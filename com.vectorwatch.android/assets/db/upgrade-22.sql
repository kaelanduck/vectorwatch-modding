ALTER TABLE stream_data ADD COLUMN auth_invalidated integer default 0;
ALTER TABLE cloud_app ADD COLUMN auth_invalidated integer default 0;