create table resources (
_id integer primary key autoincrement,
resource_global_id integer unique,
resource_size integer not null,
resource_compressed integer,
resource_compressed_size integer,
resource_content blob
);

create table apps_resources (
_id integer primary key autoincrement,
apps_resources_app_id integer not null,
apps_resources_res_id integer
);