ALTER TABLE stream_data ADD COLUMN stream_auth_credentials blob;
ALTER TABLE stream_data ADD COLUMN needs_auth integer;