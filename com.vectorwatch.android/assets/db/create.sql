create table activity_data (
timestamp integer,
_id integer primary key autoincrement,
type text,
value integer,
timezone integer,
active_time integer,
amplitude integer,
avg_period integer,
calories integer,
distance integer,
dirty_field integer,
offset integer
);

create table calendar_data (
timestamp integer,
_id integer primary key autoincrement,
title text,
start_time integer,
stop_time integer,
timezone text,
location text,
description text,
alarm_enabled integer,
alarm_time integer
);

create table contextual_data (
timestamp integer,
_id integer primary key autoincrement,
period_of_day text,
type text,
area text
);
create table sleep_data (
timestamp integer,
_id integer primary key autoincrement,
start_time integer,
stop_time integer,
restless_count integer,
awake_count integer
);
create table step_log (
timestamp integer,
_id integer primary key autoincrement,
watch_steps integer,
phone_steps integer,
start_count_timestamp integer,
stop_count_timestamp integer
);
create table log_data (
timestamp integer,
_id integer primary key autoincrement,
log_notification integer,
log_glance_count integer,
log_received_packets integer,
log_transmitted_packets integer,
log_connects integer,
log_disconnects integer,
log_secs_disconnected integer,
log_button_presses	integer,
log_backlight_count integer,
log_shaker_count integer,
log_battery_voltage integer,
log_offset integer,
dirty_field integer
);
create table stream_data (
_id integer primary key autoincrement,
stream_id integer,
stream_uuid integer,
stream_update_value text,
stream_name text,
stream_label text,
stream_description text,
stream_img  text,
stream_cont_version integer,
stream_type text,
stream_supported_type text,
stream_possible_settings blob,
stream_settings blob,
stream_active_app integer,
stream_active_face integer,
stream_active_field integer,
stream_channel_label text,
stream_state text,
stream_dirty integer,
UNIQUE( stream_uuid, stream_active_app, stream_active_face, stream_active_field ) ON CONFLICT REPLACE
);

create table cloud_app (
_id integer primary key autoincrement,
app_global_id integer unique,
app_watchfaces blob,
app_name text,
app_description text,
app_image text,
app_rating integer,
app_state text,
cloud_app_modified integer,
cloud_app_order_index integer,
app_uuid text,
app_edit_image text,
app_type text
);

create table alarm_data (
_id integer primary key autoincrement,
alarm_name text,
alarm_hour integer,
alarm_minute integer,
alarm_time_of_day integer,
alarm_angle real,
alarm_status integer
);