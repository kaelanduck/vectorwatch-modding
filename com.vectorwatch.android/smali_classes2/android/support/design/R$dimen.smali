.class public final Landroid/support/design/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/design/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f0b000f

.field public static final abc_action_bar_default_height_material:I = 0x7f0b0004

.field public static final abc_action_bar_default_padding_material:I = 0x7f0b0005

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f0b001d

.field public static final abc_action_bar_navigation_padding_start_material:I = 0x7f0b0010

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f0b0011

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f0b001e

.field public static final abc_action_bar_progress_bar_size:I = 0x7f0b0006

.field public static final abc_action_bar_stacked_max_height:I = 0x7f0b001f

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f0b0020

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f0b0021

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f0b0022

.field public static final abc_action_button_min_height_material:I = 0x7f0b0023

.field public static final abc_action_button_min_width_material:I = 0x7f0b0024

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f0b0025

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f0b0000

.field public static final abc_button_inset_horizontal_material:I = 0x7f0b0026

.field public static final abc_button_inset_vertical_material:I = 0x7f0b0027

.field public static final abc_button_padding_horizontal_material:I = 0x7f0b0028

.field public static final abc_button_padding_vertical_material:I = 0x7f0b0029

.field public static final abc_config_prefDialogWidth:I = 0x7f0b0009

.field public static final abc_control_corner_material:I = 0x7f0b002a

.field public static final abc_control_inset_material:I = 0x7f0b002b

.field public static final abc_control_padding_material:I = 0x7f0b002c

.field public static final abc_dialog_list_padding_vertical_material:I = 0x7f0b002d

.field public static final abc_dialog_min_width_major:I = 0x7f0b002e

.field public static final abc_dialog_min_width_minor:I = 0x7f0b002f

.field public static final abc_dialog_padding_material:I = 0x7f0b0030

.field public static final abc_dialog_padding_top_material:I = 0x7f0b0031

.field public static final abc_disabled_alpha_material_dark:I = 0x7f0b0032

.field public static final abc_disabled_alpha_material_light:I = 0x7f0b0033

.field public static final abc_dropdownitem_icon_width:I = 0x7f0b0034

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f0b0035

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f0b0036

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f0b0037

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f0b0038

.field public static final abc_edit_text_inset_top_material:I = 0x7f0b0039

.field public static final abc_floating_window_z:I = 0x7f0b003a

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f0b003b

.field public static final abc_panel_menu_list_width:I = 0x7f0b003c

.field public static final abc_search_view_preferred_width:I = 0x7f0b003d

.field public static final abc_search_view_text_min_width:I = 0x7f0b000a

.field public static final abc_switch_padding:I = 0x7f0b001a

.field public static final abc_text_size_body_1_material:I = 0x7f0b003e

.field public static final abc_text_size_body_2_material:I = 0x7f0b003f

.field public static final abc_text_size_button_material:I = 0x7f0b0040

.field public static final abc_text_size_caption_material:I = 0x7f0b0041

.field public static final abc_text_size_display_1_material:I = 0x7f0b0042

.field public static final abc_text_size_display_2_material:I = 0x7f0b0043

.field public static final abc_text_size_display_3_material:I = 0x7f0b0044

.field public static final abc_text_size_display_4_material:I = 0x7f0b0045

.field public static final abc_text_size_headline_material:I = 0x7f0b0046

.field public static final abc_text_size_large_material:I = 0x7f0b0047

.field public static final abc_text_size_medium_material:I = 0x7f0b0048

.field public static final abc_text_size_menu_material:I = 0x7f0b0049

.field public static final abc_text_size_small_material:I = 0x7f0b004a

.field public static final abc_text_size_subhead_material:I = 0x7f0b004b

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f0b0007

.field public static final abc_text_size_title_material:I = 0x7f0b004c

.field public static final abc_text_size_title_material_toolbar:I = 0x7f0b0008

.field public static final appbar_elevation:I = 0x7f0b0050

.field public static final dialog_fixed_height_major:I = 0x7f0b000b

.field public static final dialog_fixed_height_minor:I = 0x7f0b000c

.field public static final dialog_fixed_width_major:I = 0x7f0b000d

.field public static final dialog_fixed_width_minor:I = 0x7f0b000e

.field public static final disabled_alpha_material_dark:I = 0x7f0b0079

.field public static final disabled_alpha_material_light:I = 0x7f0b007a

.field public static final fab_border_width:I = 0x7f0b007b

.field public static final fab_content_size:I = 0x7f0b007c

.field public static final fab_elevation:I = 0x7f0b007d

.field public static final fab_size_mini:I = 0x7f0b007e

.field public static final fab_size_normal:I = 0x7f0b007f

.field public static final fab_translation_z_pressed:I = 0x7f0b0080

.field public static final navigation_elevation:I = 0x7f0b0095

.field public static final navigation_icon_padding:I = 0x7f0b0096

.field public static final navigation_icon_size:I = 0x7f0b0097

.field public static final navigation_max_width:I = 0x7f0b0098

.field public static final navigation_padding_bottom:I = 0x7f0b0099

.field public static final navigation_padding_top_default:I = 0x7f0b001b

.field public static final navigation_separator_vertical_padding:I = 0x7f0b009a

.field public static final notification_large_icon_height:I = 0x7f0b009b

.field public static final notification_large_icon_width:I = 0x7f0b009c

.field public static final notification_subtext_size:I = 0x7f0b009d

.field public static final snackbar_action_inline_max_width:I = 0x7f0b0013

.field public static final snackbar_background_corner_radius:I = 0x7f0b0014

.field public static final snackbar_elevation:I = 0x7f0b00b1

.field public static final snackbar_extra_spacing_horizontal:I = 0x7f0b0015

.field public static final snackbar_max_width:I = 0x7f0b0016

.field public static final snackbar_min_width:I = 0x7f0b0017

.field public static final snackbar_padding_horizontal:I = 0x7f0b00b2

.field public static final snackbar_padding_vertical:I = 0x7f0b00b3

.field public static final snackbar_padding_vertical_2lines:I = 0x7f0b0018

.field public static final snackbar_text_size:I = 0x7f0b00b4

.field public static final tab_max_width:I = 0x7f0b00b8

.field public static final tab_min_width:I = 0x7f0b0019


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
