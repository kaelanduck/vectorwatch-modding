.class public Lio/realm/CloudActivityDayRealmProxy;
.super Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
.source "CloudActivityDayRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/CloudActivityDayRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

.field private final proxyState:Lio/realm/ProxyState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "timestamp"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    const-string v1, "effTime"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v1, "avgAmpl"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    const-string v1, "avgPeriod"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    const-string v1, "cal"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v1, "dist"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v1, "timezone"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    const-string v1, "val"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    const-string v1, "offset"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    const-string v1, "activityType"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    const-string v1, "dirtySteps"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    const-string v1, "dirtyCalories"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    const-string v1, "dirtyDistance"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    const-string v1, "dirtyCloud"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/CloudActivityDayRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 117
    return-void
.end method

.method constructor <init>(Lio/realm/internal/ColumnInfo;)V
    .locals 2
    .param p1, "columnInfo"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;-><init>()V

    .line 120
    check-cast p1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    .end local p1    # "columnInfo":Lio/realm/internal/ColumnInfo;
    iput-object p1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    .line 121
    new-instance v0, Lio/realm/ProxyState;

    const-class v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-direct {v0, v1, p0}, Lio/realm/ProxyState;-><init>(Ljava/lang/Class;Lio/realm/RealmModel;)V

    iput-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 122
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;"
        }
    .end annotation

    .prologue
    .line 738
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    const-class v2, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    move-object v1, p1

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$timestamp()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .local v0, "realmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    move-object v1, v0

    .line 739
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 740
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$timestamp()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$timestamp(J)V

    move-object v1, v0

    .line 741
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$effTime()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$effTime(I)V

    move-object v1, v0

    .line 742
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$avgAmpl()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgAmpl(I)V

    move-object v1, v0

    .line 743
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$avgPeriod()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgPeriod(I)V

    move-object v1, v0

    .line 744
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$cal()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$cal(I)V

    move-object v1, v0

    .line 745
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dist()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dist(I)V

    move-object v1, v0

    .line 746
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$timezone()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$timezone(I)V

    move-object v1, v0

    .line 747
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$val()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$val(I)V

    move-object v1, v0

    .line 748
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$offset()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$offset(I)V

    move-object v1, v0

    .line 749
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$activityType()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    move-object v1, v0

    .line 750
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtySteps()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtySteps(Z)V

    move-object v1, v0

    .line 751
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtyCalories()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCalories(Z)V

    move-object v1, v0

    .line 752
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtyDistance()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyDistance(Z)V

    move-object v1, v0

    .line 753
    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    check-cast p1, Lio/realm/CloudActivityDayRealmProxyInterface;

    .end local p1    # "newObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    invoke-interface {p1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtyCloud()Z

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCloud(Z)V

    .line 754
    return-object v0
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .locals 12
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;"
        }
    .end annotation

    .prologue
    .line 708
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    instance-of v7, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v7, :cond_0

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    if-eqz v7, :cond_0

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    iget-wide v8, v7, Lio/realm/BaseRealm;->threadId:J

    iget-wide v10, p0, Lio/realm/Realm;->threadId:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 709
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 711
    :cond_0
    instance-of v7, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v7, :cond_1

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    if-eqz v7, :cond_1

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 733
    .end local p1    # "object":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :goto_0
    return-object p1

    .line 714
    .restart local p1    # "object":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :cond_1
    const/4 v1, 0x0

    .line 715
    .local v1, "realmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    move v0, p2

    .line 716
    .local v0, "canUpdate":Z
    if-eqz v0, :cond_2

    .line 717
    const-class v7, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p0, v7}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v6

    .line 718
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .local v2, "pkColumnIndex":J
    move-object v7, p1

    .line 719
    check-cast v7, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$timestamp()J

    move-result-wide v8

    invoke-virtual {v6, v2, v3, v8, v9}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v4

    .line 720
    .local v4, "rowIndex":J
    const-wide/16 v8, -0x1

    cmp-long v7, v4, v8

    if-eqz v7, :cond_3

    .line 721
    new-instance v1, Lio/realm/CloudActivityDayRealmProxy;

    .end local v1    # "realmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    iget-object v7, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v8, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v7, v8}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    invoke-direct {v1, v7}, Lio/realm/CloudActivityDayRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v1    # "realmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    move-object v7, v1

    .line 722
    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v7, v1

    .line 723
    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v8

    invoke-virtual {v7, v8}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    move-object v7, v1

    .line 724
    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 730
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    .line 731
    invoke-static {p0, v1, p1, p3}, Lio/realm/CloudActivityDayRealmProxy;->update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;Ljava/util/Map;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    move-result-object p1

    goto :goto_0

    .line 726
    .restart local v2    # "pkColumnIndex":J
    .restart local v4    # "rowIndex":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 733
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_4
    invoke-static {p0, p1, p2, p3}, Lio/realm/CloudActivityDayRealmProxy;->copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    move-result-object p1

    goto :goto_0
.end method

.method public static createDetachedCopy(Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;IILjava/util/Map;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .locals 6
    .param p0, "realmObject"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;"
        }
    .end annotation

    .prologue
    .line 758
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 759
    :cond_0
    const/4 v2, 0x0

    .line 789
    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :goto_0
    return-object v2

    .line 761
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 763
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 765
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 766
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    goto :goto_0

    .line 768
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 769
    .local v1, "unmanagedObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 775
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$timestamp()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$timestamp(J)V

    move-object v2, v1

    .line 776
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$effTime()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$effTime(I)V

    move-object v2, v1

    .line 777
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$avgAmpl()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgAmpl(I)V

    move-object v2, v1

    .line 778
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$avgPeriod()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgPeriod(I)V

    move-object v2, v1

    .line 779
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$cal()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$cal(I)V

    move-object v2, v1

    .line 780
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dist()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dist(I)V

    move-object v2, v1

    .line 781
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$timezone()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$timezone(I)V

    move-object v2, v1

    .line 782
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$val()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$val(I)V

    move-object v2, v1

    .line 783
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$offset()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$offset(I)V

    move-object v2, v1

    .line 784
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$activityType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    move-object v2, v1

    .line 785
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtySteps()Z

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtySteps(Z)V

    move-object v2, v1

    .line 786
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtyCalories()Z

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCalories(Z)V

    move-object v2, v1

    .line 787
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtyDistance()Z

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyDistance(Z)V

    move-object v2, v1

    .line 788
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    check-cast p0, Lio/realm/CloudActivityDayRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    invoke-interface {p0}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtyCloud()Z

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCloud(Z)V

    move-object v2, v1

    .line 789
    goto/16 :goto_0

    .line 772
    .end local v1    # "unmanagedObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :cond_3
    new-instance v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;-><init>()V

    .line 773
    .restart local v1    # "unmanagedObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .locals 11
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 468
    const/4 v0, 0x0

    .line 469
    .local v0, "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    if-eqz p2, :cond_1

    .line 470
    const-class v6, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v1

    .line 471
    .local v1, "table":Lio/realm/internal/Table;
    invoke-virtual {v1}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .line 472
    .local v2, "pkColumnIndex":J
    const-wide/16 v4, -0x1

    .line 473
    .local v4, "rowIndex":J
    const-string v6, "timestamp"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 474
    const-string v6, "timestamp"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v1, v2, v3, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v4

    .line 476
    :cond_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_1

    .line 477
    new-instance v0, Lio/realm/CloudActivityDayRealmProxy;

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    iget-object v6, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v6

    invoke-direct {v0, v6}, Lio/realm/CloudActivityDayRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    move-object v6, v0

    .line 478
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v6, v0

    .line 479
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v1, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v7

    invoke-virtual {v6, v7}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 482
    .end local v1    # "table":Lio/realm/internal/Table;
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    :cond_1
    if-nez v0, :cond_2

    .line 483
    const-string v6, "timestamp"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 484
    const-string v6, "timestamp"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 485
    const-class v6, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p0, v6, v10}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    check-cast v0, Lio/realm/CloudActivityDayRealmProxy;

    .line 493
    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :cond_2
    :goto_0
    const-string v6, "timestamp"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 494
    const-string v6, "timestamp"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 495
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field timestamp to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 487
    :cond_3
    const-class v6, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    const-string v7, "timestamp"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    check-cast v0, Lio/realm/CloudActivityDayRealmProxy;

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    goto :goto_0

    .line 490
    :cond_4
    const-class v6, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p0, v6}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    check-cast v0, Lio/realm/CloudActivityDayRealmProxy;

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    goto :goto_0

    :cond_5
    move-object v6, v0

    .line 497
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "timestamp"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-interface {v6, v8, v9}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$timestamp(J)V

    .line 500
    :cond_6
    const-string v6, "effTime"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 501
    const-string v6, "effTime"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 502
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field effTime to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_7
    move-object v6, v0

    .line 504
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "effTime"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$effTime(I)V

    .line 507
    :cond_8
    const-string v6, "avgAmpl"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 508
    const-string v6, "avgAmpl"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 509
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field avgAmpl to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_9
    move-object v6, v0

    .line 511
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "avgAmpl"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgAmpl(I)V

    .line 514
    :cond_a
    const-string v6, "avgPeriod"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 515
    const-string v6, "avgPeriod"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 516
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field avgPeriod to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_b
    move-object v6, v0

    .line 518
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "avgPeriod"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgPeriod(I)V

    .line 521
    :cond_c
    const-string v6, "cal"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 522
    const-string v6, "cal"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 523
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field cal to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_d
    move-object v6, v0

    .line 525
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "cal"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$cal(I)V

    .line 528
    :cond_e
    const-string v6, "dist"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 529
    const-string v6, "dist"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 530
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field dist to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_f
    move-object v6, v0

    .line 532
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "dist"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dist(I)V

    .line 535
    :cond_10
    const-string v6, "timezone"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 536
    const-string v6, "timezone"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 537
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field timezone to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_11
    move-object v6, v0

    .line 539
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "timezone"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$timezone(I)V

    .line 542
    :cond_12
    const-string v6, "val"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 543
    const-string v6, "val"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 544
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field val to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_13
    move-object v6, v0

    .line 546
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "val"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$val(I)V

    .line 549
    :cond_14
    const-string v6, "offset"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 550
    const-string v6, "offset"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 551
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field offset to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_15
    move-object v6, v0

    .line 553
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "offset"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$offset(I)V

    .line 556
    :cond_16
    const-string v6, "activityType"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 557
    const-string v6, "activityType"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_18

    move-object v6, v0

    .line 558
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v6, v10}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    .line 563
    :cond_17
    :goto_1
    const-string v6, "dirtySteps"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 564
    const-string v6, "dirtySteps"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 565
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field dirtySteps to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_18
    move-object v6, v0

    .line 560
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "activityType"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    goto :goto_1

    :cond_19
    move-object v6, v0

    .line 567
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "dirtySteps"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtySteps(Z)V

    .line 570
    :cond_1a
    const-string v6, "dirtyCalories"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 571
    const-string v6, "dirtyCalories"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 572
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field dirtyCalories to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_1b
    move-object v6, v0

    .line 574
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "dirtyCalories"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCalories(Z)V

    .line 577
    :cond_1c
    const-string v6, "dirtyDistance"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 578
    const-string v6, "dirtyDistance"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 579
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field dirtyDistance to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_1d
    move-object v6, v0

    .line 581
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "dirtyDistance"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyDistance(Z)V

    .line 584
    :cond_1e
    const-string v6, "dirtyCloud"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_20

    .line 585
    const-string v6, "dirtyCloud"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1f

    .line 586
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field dirtyCloud to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_1f
    move-object v6, v0

    .line 588
    check-cast v6, Lio/realm/CloudActivityDayRealmProxyInterface;

    const-string v7, "dirtyCloud"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCloud(Z)V

    .line 591
    :cond_20
    return-object v0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 597
    const-class v2, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p0, v2}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 598
    .local v1, "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 599
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 600
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 601
    .local v0, "name":Ljava/lang/String;
    const-string v2, "timestamp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 602
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_0

    .line 603
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 604
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field timestamp to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object v2, v1

    .line 606
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$timestamp(J)V

    goto :goto_0

    .line 608
    :cond_1
    const-string v2, "effTime"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 609
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_2

    .line 610
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 611
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field effTime to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    move-object v2, v1

    .line 613
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$effTime(I)V

    goto :goto_0

    .line 615
    :cond_3
    const-string v2, "avgAmpl"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 616
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_4

    .line 617
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 618
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field avgAmpl to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    move-object v2, v1

    .line 620
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgAmpl(I)V

    goto :goto_0

    .line 622
    :cond_5
    const-string v2, "avgPeriod"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 623
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_6

    .line 624
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 625
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field avgPeriod to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    move-object v2, v1

    .line 627
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgPeriod(I)V

    goto/16 :goto_0

    .line 629
    :cond_7
    const-string v2, "cal"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 630
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_8

    .line 631
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 632
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field cal to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_8
    move-object v2, v1

    .line 634
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$cal(I)V

    goto/16 :goto_0

    .line 636
    :cond_9
    const-string v2, "dist"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 637
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_a

    .line 638
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 639
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field dist to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_a
    move-object v2, v1

    .line 641
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dist(I)V

    goto/16 :goto_0

    .line 643
    :cond_b
    const-string v2, "timezone"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 644
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_c

    .line 645
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 646
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field timezone to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_c
    move-object v2, v1

    .line 648
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$timezone(I)V

    goto/16 :goto_0

    .line 650
    :cond_d
    const-string v2, "val"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 651
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_e

    .line 652
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 653
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field val to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_e
    move-object v2, v1

    .line 655
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$val(I)V

    goto/16 :goto_0

    .line 657
    :cond_f
    const-string v2, "offset"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 658
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_10

    .line 659
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 660
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field offset to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_10
    move-object v2, v1

    .line 662
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$offset(I)V

    goto/16 :goto_0

    .line 664
    :cond_11
    const-string v2, "activityType"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 665
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_12

    .line 666
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v2, v1

    .line 667
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_12
    move-object v2, v1

    .line 669
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 671
    :cond_13
    const-string v2, "dirtySteps"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 672
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_14

    .line 673
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 674
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field dirtySteps to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_14
    move-object v2, v1

    .line 676
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtySteps(Z)V

    goto/16 :goto_0

    .line 678
    :cond_15
    const-string v2, "dirtyCalories"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 679
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_16

    .line 680
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 681
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field dirtyCalories to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_16
    move-object v2, v1

    .line 683
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCalories(Z)V

    goto/16 :goto_0

    .line 685
    :cond_17
    const-string v2, "dirtyDistance"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 686
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_18

    .line 687
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 688
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field dirtyDistance to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_18
    move-object v2, v1

    .line 690
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyDistance(Z)V

    goto/16 :goto_0

    .line 692
    :cond_19
    const-string v2, "dirtyCloud"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 693
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_1a

    .line 694
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 695
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field dirtyCloud to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1a
    move-object v2, v1

    .line 697
    check-cast v2, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCloud(Z)V

    goto/16 :goto_0

    .line 700
    :cond_1b
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 703
    .end local v0    # "name":Ljava/lang/String;
    :cond_1c
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 704
    return-object v1
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 462
    sget-object v0, Lio/realm/CloudActivityDayRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 458
    const-string v0, "class_CloudActivityDay"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;
    .locals 5
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    const/4 v4, 0x0

    .line 283
    const-string v1, "class_CloudActivityDay"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 284
    const-string v1, "class_CloudActivityDay"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 285
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 286
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "effTime"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 287
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "avgAmpl"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 288
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "avgPeriod"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 289
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "cal"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 290
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "dist"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 291
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "timezone"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 292
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "val"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 293
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "offset"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 294
    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v2, "activityType"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 295
    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    const-string v2, "dirtySteps"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 296
    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    const-string v2, "dirtyCalories"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 297
    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    const-string v2, "dirtyDistance"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 298
    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    const-string v2, "dirtyCloud"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 299
    const-string v1, "timestamp"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 300
    const-string v1, "timestamp"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 303
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "class_CloudActivityDay"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method static update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;Ljava/util/Map;)Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .locals 2
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .param p2, "newObject"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;"
        }
    .end annotation

    .prologue
    .line 793
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v0, p1

    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$effTime()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$effTime(I)V

    move-object v0, p1

    .line 794
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$avgAmpl()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgAmpl(I)V

    move-object v0, p1

    .line 795
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$avgPeriod()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$avgPeriod(I)V

    move-object v0, p1

    .line 796
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$cal()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$cal(I)V

    move-object v0, p1

    .line 797
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dist()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dist(I)V

    move-object v0, p1

    .line 798
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$timezone()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$timezone(I)V

    move-object v0, p1

    .line 799
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$val()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$val(I)V

    move-object v0, p1

    .line 800
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$offset()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$offset(I)V

    move-object v0, p1

    .line 801
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$activityType()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    move-object v0, p1

    .line 802
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtySteps()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtySteps(Z)V

    move-object v0, p1

    .line 803
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtyCalories()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCalories(Z)V

    move-object v0, p1

    .line 804
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/CloudActivityDayRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtyDistance()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyDistance(Z)V

    move-object v0, p1

    .line 805
    check-cast v0, Lio/realm/CloudActivityDayRealmProxyInterface;

    check-cast p2, Lio/realm/CloudActivityDayRealmProxyInterface;

    .end local p2    # "newObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    invoke-interface {p2}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmGet$dirtyCloud()Z

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/CloudActivityDayRealmProxyInterface;->realmSet$dirtyCloud(Z)V

    .line 806
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;
    .locals 10
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    const-wide/16 v8, 0xe

    .line 307
    const-string v5, "class_CloudActivityDay"

    invoke-virtual {p0, v5}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2d

    .line 308
    const-string v5, "class_CloudActivityDay"

    invoke-virtual {p0, v5}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v4

    .line 309
    .local v4, "table":Lio/realm/internal/Table;
    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 310
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Field count does not match - expected 14 but was "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 312
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 313
    .local v1, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v2, 0x0

    .local v2, "i":J
    :goto_0
    cmp-long v5, v2, v8

    if-gez v5, :cond_1

    .line 314
    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    goto :goto_0

    .line 317
    :cond_1
    new-instance v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5, v4}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 319
    .local v0, "columnInfo":Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;
    const-string v5, "timestamp"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 320
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'timestamp\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 322
    :cond_2
    const-string v5, "timestamp"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_3

    .line 323
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'long\' for field \'timestamp\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 325
    :cond_3
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timestampIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timestampIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_4

    .line 326
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Cannot migrate an object with null value in field \'timestamp\'. Either maintain the same type for primary key field \'timestamp\', or remove the object with null value before migration."

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 328
    :cond_4
    invoke-virtual {v4}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    const-string v5, "timestamp"

    invoke-virtual {v4, v5}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-eqz v5, :cond_5

    .line 329
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Primary key not defined for field \'timestamp\' in existing Realm file. Add @PrimaryKey."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 331
    :cond_5
    const-string v5, "timestamp"

    invoke-virtual {v4, v5}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v5

    if-nez v5, :cond_6

    .line 332
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Index not defined for field \'timestamp\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 334
    :cond_6
    const-string v5, "effTime"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 335
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'effTime\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 337
    :cond_7
    const-string v5, "effTime"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_8

    .line 338
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'effTime\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 340
    :cond_8
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->effTimeIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 341
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'effTime\' does support null values in the existing Realm file. Use corresponding boxed type for field \'effTime\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 343
    :cond_9
    const-string v5, "avgAmpl"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 344
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'avgAmpl\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 346
    :cond_a
    const-string v5, "avgAmpl"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_b

    .line 347
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'avgAmpl\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 349
    :cond_b
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgAmplIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 350
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'avgAmpl\' does support null values in the existing Realm file. Use corresponding boxed type for field \'avgAmpl\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 352
    :cond_c
    const-string v5, "avgPeriod"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 353
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'avgPeriod\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 355
    :cond_d
    const-string v5, "avgPeriod"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_e

    .line 356
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'avgPeriod\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 358
    :cond_e
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgPeriodIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 359
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'avgPeriod\' does support null values in the existing Realm file. Use corresponding boxed type for field \'avgPeriod\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 361
    :cond_f
    const-string v5, "cal"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 362
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'cal\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 364
    :cond_10
    const-string v5, "cal"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_11

    .line 365
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'cal\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 367
    :cond_11
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->calIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 368
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'cal\' does support null values in the existing Realm file. Use corresponding boxed type for field \'cal\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 370
    :cond_12
    const-string v5, "dist"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_13

    .line 371
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'dist\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 373
    :cond_13
    const-string v5, "dist"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_14

    .line 374
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'dist\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 376
    :cond_14
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->distIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 377
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'dist\' does support null values in the existing Realm file. Use corresponding boxed type for field \'dist\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 379
    :cond_15
    const-string v5, "timezone"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_16

    .line 380
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'timezone\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 382
    :cond_16
    const-string v5, "timezone"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_17

    .line 383
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'timezone\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 385
    :cond_17
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timezoneIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 386
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'timezone\' does support null values in the existing Realm file. Use corresponding boxed type for field \'timezone\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 388
    :cond_18
    const-string v5, "val"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_19

    .line 389
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'val\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 391
    :cond_19
    const-string v5, "val"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_1a

    .line 392
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'val\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 394
    :cond_1a
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->valIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 395
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'val\' does support null values in the existing Realm file. Use corresponding boxed type for field \'val\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 397
    :cond_1b
    const-string v5, "offset"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1c

    .line 398
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'offset\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 400
    :cond_1c
    const-string v5, "offset"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_1d

    .line 401
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'offset\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 403
    :cond_1d
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->offsetIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 404
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'offset\' does support null values in the existing Realm file. Use corresponding boxed type for field \'offset\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 406
    :cond_1e
    const-string v5, "activityType"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1f

    .line 407
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'activityType\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 409
    :cond_1f
    const-string v5, "activityType"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_20

    .line 410
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'String\' for field \'activityType\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 412
    :cond_20
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->activityTypeIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-nez v5, :cond_21

    .line 413
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'activityType\' is required. Either set @Required to field \'activityType\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 415
    :cond_21
    const-string v5, "dirtySteps"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_22

    .line 416
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'dirtySteps\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 418
    :cond_22
    const-string v5, "dirtySteps"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_23

    .line 419
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'boolean\' for field \'dirtySteps\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 421
    :cond_23
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyStepsIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 422
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'dirtySteps\' does support null values in the existing Realm file. Use corresponding boxed type for field \'dirtySteps\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 424
    :cond_24
    const-string v5, "dirtyCalories"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_25

    .line 425
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'dirtyCalories\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 427
    :cond_25
    const-string v5, "dirtyCalories"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_26

    .line 428
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'boolean\' for field \'dirtyCalories\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 430
    :cond_26
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCaloriesIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_27

    .line 431
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'dirtyCalories\' does support null values in the existing Realm file. Use corresponding boxed type for field \'dirtyCalories\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 433
    :cond_27
    const-string v5, "dirtyDistance"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_28

    .line 434
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'dirtyDistance\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 436
    :cond_28
    const-string v5, "dirtyDistance"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_29

    .line 437
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'boolean\' for field \'dirtyDistance\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 439
    :cond_29
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyDistanceIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 440
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'dirtyDistance\' does support null values in the existing Realm file. Use corresponding boxed type for field \'dirtyDistance\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 442
    :cond_2a
    const-string v5, "dirtyCloud"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2b

    .line 443
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'dirtyCloud\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 445
    :cond_2b
    const-string v5, "dirtyCloud"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_2c

    .line 446
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'boolean\' for field \'dirtyCloud\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 448
    :cond_2c
    iget-wide v6, v0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCloudIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_2e

    .line 449
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'dirtyCloud\' does support null values in the existing Realm file. Use corresponding boxed type for field \'dirtyCloud\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 453
    .end local v0    # "columnInfo":Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;
    .end local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v2    # "i":J
    .end local v4    # "table":Lio/realm/internal/Table;
    :cond_2d
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "The CloudActivityDay class is missing from the schema for this Realm."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 451
    .restart local v0    # "columnInfo":Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;
    .restart local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v2    # "i":J
    .restart local v4    # "table":Lio/realm/internal/Table;
    :cond_2e
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 894
    if-ne p0, p1, :cond_1

    .line 908
    :cond_0
    :goto_0
    return v5

    .line 895
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 896
    check-cast v0, Lio/realm/CloudActivityDayRealmProxy;

    .line 898
    .local v0, "aCloudActivityDay":Lio/realm/CloudActivityDayRealmProxy;
    iget-object v7, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 899
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 900
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 902
    :cond_6
    iget-object v7, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 903
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 904
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 906
    :cond_9
    iget-object v7, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 881
    iget-object v6, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 882
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 883
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 885
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 886
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 887
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 888
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 889
    return v1

    :cond_1
    move v6, v5

    .line 886
    goto :goto_0
.end method

.method public realmGet$activityType()Ljava/lang/String;
    .locals 4

    .prologue
    .line 225
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 226
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->activityTypeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public realmGet$avgAmpl()I
    .locals 4

    .prologue
    .line 148
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 149
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgAmplIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$avgPeriod()I
    .locals 4

    .prologue
    .line 159
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 160
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgPeriodIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$cal()I
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 171
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->calIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$dirtyCalories()Z
    .locals 4

    .prologue
    .line 251
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 252
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCaloriesIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getBoolean(J)Z

    move-result v0

    return v0
.end method

.method public realmGet$dirtyCloud()Z
    .locals 4

    .prologue
    .line 273
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 274
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCloudIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getBoolean(J)Z

    move-result v0

    return v0
.end method

.method public realmGet$dirtyDistance()Z
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 263
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyDistanceIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getBoolean(J)Z

    move-result v0

    return v0
.end method

.method public realmGet$dirtySteps()Z
    .locals 4

    .prologue
    .line 240
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 241
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyStepsIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getBoolean(J)Z

    move-result v0

    return v0
.end method

.method public realmGet$dist()I
    .locals 4

    .prologue
    .line 181
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 182
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->distIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$effTime()I
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 138
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->effTimeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$offset()I
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 215
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->offsetIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$timestamp()J
    .locals 4

    .prologue
    .line 126
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 127
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timestampIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$timezone()I
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 193
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timezoneIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$val()I
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 204
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->valIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmSet$activityType(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 230
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 231
    if-nez p1, :cond_0

    .line 232
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->activityTypeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->setNull(J)V

    .line 236
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->activityTypeIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setString(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public realmSet$avgAmpl(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 153
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 154
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgAmplIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 155
    return-void
.end method

.method public realmSet$avgPeriod(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 165
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgPeriodIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 166
    return-void
.end method

.method public realmSet$cal(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 175
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 176
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->calIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 177
    return-void
.end method

.method public realmSet$dirtyCalories(Z)V
    .locals 4
    .param p1, "value"    # Z

    .prologue
    .line 256
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 257
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCaloriesIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setBoolean(JZ)V

    .line 258
    return-void
.end method

.method public realmSet$dirtyCloud(Z)V
    .locals 4
    .param p1, "value"    # Z

    .prologue
    .line 278
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 279
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCloudIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setBoolean(JZ)V

    .line 280
    return-void
.end method

.method public realmSet$dirtyDistance(Z)V
    .locals 4
    .param p1, "value"    # Z

    .prologue
    .line 267
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 268
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyDistanceIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setBoolean(JZ)V

    .line 269
    return-void
.end method

.method public realmSet$dirtySteps(Z)V
    .locals 4
    .param p1, "value"    # Z

    .prologue
    .line 245
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 246
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyStepsIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setBoolean(JZ)V

    .line 247
    return-void
.end method

.method public realmSet$dist(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 186
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 187
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->distIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 188
    return-void
.end method

.method public realmSet$effTime(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 143
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->effTimeIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 144
    return-void
.end method

.method public realmSet$offset(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 219
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 220
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->offsetIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 221
    return-void
.end method

.method public realmSet$timestamp(J)V
    .locals 5
    .param p1, "value"    # J

    .prologue
    .line 131
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 132
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timestampIndex:J

    invoke-interface {v0, v2, v3, p1, p2}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 133
    return-void
.end method

.method public realmSet$timezone(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 197
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 198
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timezoneIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 199
    return-void
.end method

.method public realmSet$val(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 208
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 209
    iget-object v0, p0, Lio/realm/CloudActivityDayRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/CloudActivityDayRealmProxy;->columnInfo:Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;

    iget-wide v2, v1, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->valIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 210
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 811
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 812
    const-string v1, "Invalid object"

    .line 871
    :goto_0
    return-object v1

    .line 814
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CloudActivityDay = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 815
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{timestamp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 816
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$timestamp()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 817
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 818
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 819
    const-string v1, "{effTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$effTime()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 821
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 822
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 823
    const-string v1, "{avgAmpl:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 824
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$avgAmpl()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 825
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 826
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 827
    const-string v1, "{avgPeriod:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 828
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$avgPeriod()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 829
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 830
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    const-string v1, "{cal:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$cal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 833
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 834
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 835
    const-string v1, "{dist:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 836
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$dist()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 837
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 838
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 839
    const-string v1, "{timezone:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 840
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$timezone()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 841
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 842
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 843
    const-string v1, "{val:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 844
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$val()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 845
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 846
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    const-string v1, "{offset:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 848
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$offset()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 849
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 850
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 851
    const-string v1, "{activityType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 852
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$activityType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$activityType()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 853
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 854
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 855
    const-string v1, "{dirtySteps:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 856
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$dirtySteps()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 857
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 858
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 859
    const-string v1, "{dirtyCalories:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 860
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$dirtyCalories()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 861
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 862
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 863
    const-string v1, "{dirtyDistance:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 864
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$dirtyDistance()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 865
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 866
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 867
    const-string v1, "{dirtyCloud:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 868
    invoke-virtual {p0}, Lio/realm/CloudActivityDayRealmProxy;->realmGet$dirtyCloud()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 869
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 870
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 871
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 852
    :cond_1
    const-string v1, "null"

    goto :goto_1
.end method
