.class public Lio/realm/StringObjectRealmProxy;
.super Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
.source "StringObjectRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/StringObjectRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final columnInfo:Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;

.field private final proxyState:Lio/realm/ProxyState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "string"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/StringObjectRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 52
    return-void
.end method

.method constructor <init>(Lio/realm/internal/ColumnInfo;)V
    .locals 2
    .param p1, "columnInfo"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;-><init>()V

    .line 55
    check-cast p1, Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;

    .end local p1    # "columnInfo":Lio/realm/internal/ColumnInfo;
    iput-object p1, p0, Lio/realm/StringObjectRealmProxy;->columnInfo:Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;

    .line 56
    new-instance v0, Lio/realm/ProxyState;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-direct {v0, v1, p0}, Lio/realm/ProxyState;-><init>(Ljava/lang/Class;Lio/realm/RealmModel;)V

    iput-object v0, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 57
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .locals 3
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;"
        }
    .end annotation

    .prologue
    .line 226
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-object v1, p1

    check-cast v1, Lio/realm/StringObjectRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/StringObjectRealmProxyInterface;->realmGet$string()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    .local v0, "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    move-object v1, v0

    .line 227
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 228
    check-cast v1, Lio/realm/StringObjectRealmProxyInterface;

    check-cast p1, Lio/realm/StringObjectRealmProxyInterface;

    .end local p1    # "newObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    invoke-interface {p1}, Lio/realm/StringObjectRealmProxyInterface;->realmGet$string()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/StringObjectRealmProxyInterface;->realmSet$string(Ljava/lang/String;)V

    .line 229
    return-object v0
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .locals 12
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;"
        }
    .end annotation

    .prologue
    .line 190
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    instance-of v8, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_0

    move-object v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_0

    move-object v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    iget-wide v8, v8, Lio/realm/BaseRealm;->threadId:J

    iget-wide v10, p0, Lio/realm/Realm;->threadId:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 191
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 193
    :cond_0
    instance-of v8, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 221
    .end local p1    # "object":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    :goto_0
    return-object p1

    .line 196
    .restart local p1    # "object":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    :cond_1
    const/4 v1, 0x0

    .line 197
    .local v1, "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    move v0, p2

    .line 198
    .local v0, "canUpdate":Z
    if-eqz v0, :cond_2

    .line 199
    const-class v8, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v6

    .line 200
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .local v2, "pkColumnIndex":J
    move-object v8, p1

    .line 201
    check-cast v8, Lio/realm/StringObjectRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/StringObjectRealmProxyInterface;->realmGet$string()Ljava/lang/String;

    move-result-object v7

    .line 202
    .local v7, "value":Ljava/lang/String;
    const-wide/16 v4, -0x1

    .line 203
    .local v4, "rowIndex":J
    if-nez v7, :cond_3

    .line 204
    invoke-virtual {v6, v2, v3}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v4

    .line 208
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v8, v4, v8

    if-eqz v8, :cond_4

    .line 209
    new-instance v1, Lio/realm/StringObjectRealmProxy;

    .end local v1    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    iget-object v8, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v8

    invoke-direct {v1, v8}, Lio/realm/StringObjectRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v1    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    move-object v8, v1

    .line 210
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v8, v1

    .line 211
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v9

    invoke-virtual {v8, v9}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    move-object v8, v1

    .line 212
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    .end local v7    # "value":Ljava/lang/String;
    :cond_2
    :goto_2
    if-eqz v0, :cond_5

    .line 219
    invoke-static {p0, v1, p1, p3}, Lio/realm/StringObjectRealmProxy;->update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;Ljava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object p1

    goto :goto_0

    .line 206
    .restart local v2    # "pkColumnIndex":J
    .restart local v4    # "rowIndex":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    .restart local v7    # "value":Ljava/lang/String;
    :cond_3
    invoke-virtual {v6, v2, v3, v7}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v4

    goto :goto_1

    .line 214
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 221
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    .end local v7    # "value":Ljava/lang/String;
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/StringObjectRealmProxy;->copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object p1

    goto :goto_0
.end method

.method public static createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .locals 4
    .param p0, "realmObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;"
        }
    .end annotation

    .prologue
    .line 233
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 234
    :cond_0
    const/4 v2, 0x0

    .line 251
    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    :goto_0
    return-object v2

    .line 236
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 238
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 240
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 241
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    goto :goto_0

    .line 243
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    .line 244
    .local v1, "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 250
    check-cast v2, Lio/realm/StringObjectRealmProxyInterface;

    check-cast p0, Lio/realm/StringObjectRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    invoke-interface {p0}, Lio/realm/StringObjectRealmProxyInterface;->realmGet$string()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/StringObjectRealmProxyInterface;->realmSet$string(Ljava/lang/String;)V

    move-object v2, v1

    .line 251
    goto :goto_0

    .line 247
    .end local v1    # "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    :cond_3
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;-><init>()V

    .line 248
    .restart local v1    # "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .locals 9
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    if-eqz p2, :cond_0

    .line 132
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v1

    .line 133
    .local v1, "table":Lio/realm/internal/Table;
    invoke-virtual {v1}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .line 134
    .local v2, "pkColumnIndex":J
    const-wide/16 v4, -0x1

    .line 135
    .local v4, "rowIndex":J
    const-string v6, "string"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 136
    invoke-virtual {v1, v2, v3}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v4

    .line 140
    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_0

    .line 141
    new-instance v0, Lio/realm/StringObjectRealmProxy;

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    iget-object v6, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v6

    invoke-direct {v0, v6}, Lio/realm/StringObjectRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    move-object v6, v0

    .line 142
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v6, v0

    .line 143
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v1, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v7

    invoke-virtual {v6, v7}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 146
    .end local v1    # "table":Lio/realm/internal/Table;
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    :cond_0
    if-nez v0, :cond_1

    .line 147
    const-string v6, "string"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 148
    const-string v6, "string"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 149
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p0, v6, v8}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    check-cast v0, Lio/realm/StringObjectRealmProxy;

    .line 157
    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    :cond_1
    :goto_1
    const-string v6, "string"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 158
    const-string v6, "string"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    move-object v6, v0

    .line 159
    check-cast v6, Lio/realm/StringObjectRealmProxyInterface;

    invoke-interface {v6, v8}, Lio/realm/StringObjectRealmProxyInterface;->realmSet$string(Ljava/lang/String;)V

    .line 164
    :cond_2
    :goto_2
    return-object v0

    .line 138
    .restart local v1    # "table":Lio/realm/internal/Table;
    .restart local v2    # "pkColumnIndex":J
    .restart local v4    # "rowIndex":J
    :cond_3
    const-string v6, "string"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v2, v3, v6}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v4

    goto :goto_0

    .line 151
    .end local v1    # "table":Lio/realm/internal/Table;
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    :cond_4
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    const-string v7, "string"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    check-cast v0, Lio/realm/StringObjectRealmProxy;

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    goto :goto_1

    .line 154
    :cond_5
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p0, v6}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    check-cast v0, Lio/realm/StringObjectRealmProxy;

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    goto :goto_1

    :cond_6
    move-object v6, v0

    .line 161
    check-cast v6, Lio/realm/StringObjectRealmProxyInterface;

    const-string v7, "string"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lio/realm/StringObjectRealmProxyInterface;->realmSet$string(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .locals 4
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {p0, v2}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    .line 171
    .local v1, "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 172
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 173
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "name":Ljava/lang/String;
    const-string v2, "string"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_0

    .line 176
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v2, v1

    .line 177
    check-cast v2, Lio/realm/StringObjectRealmProxyInterface;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lio/realm/StringObjectRealmProxyInterface;->realmSet$string(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move-object v2, v1

    .line 179
    check-cast v2, Lio/realm/StringObjectRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/StringObjectRealmProxyInterface;->realmSet$string(Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 185
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 186
    return-object v1
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lio/realm/StringObjectRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const-string v0, "class_StringObject"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    .line 75
    const-string v1, "class_StringObject"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    const-string v1, "class_StringObject"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 77
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v2, "string"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 78
    const-string v1, "string"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 79
    const-string v1, "string"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 82
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "class_StringObject"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method static update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;Ljava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .locals 0
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .param p2, "newObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;"
        }
    .end annotation

    .prologue
    .line 255
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;
    .locals 10
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    const-wide/16 v8, 0x1

    .line 86
    const-string v5, "class_StringObject"

    invoke-virtual {p0, v5}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 87
    const-string v5, "class_StringObject"

    invoke-virtual {p0, v5}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v4

    .line 88
    .local v4, "table":Lio/realm/internal/Table;
    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 89
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Field count does not match - expected 1 but was "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 91
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 92
    .local v1, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v2, 0x0

    .local v2, "i":J
    :goto_0
    cmp-long v5, v2, v8

    if-gez v5, :cond_1

    .line 93
    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    add-long/2addr v2, v8

    goto :goto_0

    .line 96
    :cond_1
    new-instance v0, Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5, v4}, Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 98
    .local v0, "columnInfo":Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;
    const-string v5, "string"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 99
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'string\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 101
    :cond_2
    const-string v5, "string"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_3

    .line 102
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'String\' for field \'string\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 104
    :cond_3
    iget-wide v6, v0, Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;->stringIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-nez v5, :cond_4

    .line 105
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "@PrimaryKey field \'string\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 107
    :cond_4
    invoke-virtual {v4}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    const-string v5, "string"

    invoke-virtual {v4, v5}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-eqz v5, :cond_5

    .line 108
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Primary key not defined for field \'string\' in existing Realm file. Add @PrimaryKey."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 110
    :cond_5
    const-string v5, "string"

    invoke-virtual {v4, v5}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v5

    if-nez v5, :cond_7

    .line 111
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Index not defined for field \'string\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 115
    .end local v0    # "columnInfo":Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;
    .end local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v2    # "i":J
    .end local v4    # "table":Lio/realm/internal/Table;
    :cond_6
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "The StringObject class is missing from the schema for this Realm."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 113
    .restart local v0    # "columnInfo":Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;
    .restart local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v2    # "i":J
    .restart local v4    # "table":Lio/realm/internal/Table;
    :cond_7
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 291
    if-ne p0, p1, :cond_1

    .line 305
    :cond_0
    :goto_0
    return v5

    .line 292
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 293
    check-cast v0, Lio/realm/StringObjectRealmProxy;

    .line 295
    .local v0, "aStringObject":Lio/realm/StringObjectRealmProxy;
    iget-object v7, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 296
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 297
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 299
    :cond_6
    iget-object v7, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 300
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 301
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 303
    :cond_9
    iget-object v7, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 278
    iget-object v6, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 280
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 282
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 283
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 284
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 285
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 286
    return v1

    :cond_1
    move v6, v5

    .line 283
    goto :goto_0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$string()Ljava/lang/String;
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 62
    iget-object v0, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/StringObjectRealmProxy;->columnInfo:Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;

    iget-wide v2, v1, Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;->stringIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public realmSet$string(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v0, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 67
    if-nez p1, :cond_0

    .line 68
    iget-object v0, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/StringObjectRealmProxy;->columnInfo:Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;

    iget-wide v2, v1, Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;->stringIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->setNull(J)V

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lio/realm/StringObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/StringObjectRealmProxy;->columnInfo:Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;

    iget-wide v2, v1, Lio/realm/StringObjectRealmProxy$StringObjectColumnInfo;->stringIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setString(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 260
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 261
    const-string v1, "Invalid object"

    .line 268
    :goto_0
    return-object v1

    .line 263
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "StringObject = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 264
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{string:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    invoke-virtual {p0}, Lio/realm/StringObjectRealmProxy;->realmGet$string()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lio/realm/StringObjectRealmProxy;->realmGet$string()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 265
    :cond_1
    const-string v1, "null"

    goto :goto_1
.end method
