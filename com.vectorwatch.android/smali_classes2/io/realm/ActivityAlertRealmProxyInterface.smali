.class public interface abstract Lio/realm/ActivityAlertRealmProxyInterface;
.super Ljava/lang/Object;
.source "ActivityAlertRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$activityType()Ljava/lang/String;
.end method

.method public abstract realmGet$longValues()Lio/realm/RealmList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            ">;"
        }
    .end annotation
.end method

.method public abstract realmGet$message()Ljava/lang/String;
.end method

.method public abstract realmGet$stringValues()Lio/realm/RealmList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            ">;"
        }
    .end annotation
.end method

.method public abstract realmGet$timestamp()J
.end method

.method public abstract realmGet$type()I
.end method

.method public abstract realmGet$uuid()Ljava/lang/String;
.end method

.method public abstract realmSet$activityType(Ljava/lang/String;)V
.end method

.method public abstract realmSet$longValues(Lio/realm/RealmList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract realmSet$message(Ljava/lang/String;)V
.end method

.method public abstract realmSet$stringValues(Lio/realm/RealmList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract realmSet$timestamp(J)V
.end method

.method public abstract realmSet$type(I)V
.end method

.method public abstract realmSet$uuid(Ljava/lang/String;)V
.end method
