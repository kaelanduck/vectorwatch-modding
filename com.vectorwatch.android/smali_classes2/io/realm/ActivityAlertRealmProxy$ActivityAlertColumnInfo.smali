.class final Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "ActivityAlertRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/ActivityAlertRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ActivityAlertColumnInfo"
.end annotation


# instance fields
.field public final activityTypeIndex:J

.field public final longValuesIndex:J

.field public final messageIndex:J

.field public final stringValuesIndex:J

.field public final timestampIndex:J

.field public final typeIndex:J

.field public final uuidIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 44
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 46
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "ActivityAlert"

    const-string v2, "activityType"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->activityTypeIndex:J

    .line 47
    const-string v1, "activityType"

    iget-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->activityTypeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "ActivityAlert"

    const-string v2, "stringValues"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->stringValuesIndex:J

    .line 50
    const-string v1, "stringValues"

    iget-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->stringValuesIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    const-string v1, "ActivityAlert"

    const-string v2, "longValues"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->longValuesIndex:J

    .line 53
    const-string v1, "longValues"

    iget-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->longValuesIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string v1, "ActivityAlert"

    const-string v2, "message"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->messageIndex:J

    .line 56
    const-string v1, "message"

    iget-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->messageIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const-string v1, "ActivityAlert"

    const-string v2, "timestamp"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->timestampIndex:J

    .line 59
    const-string v1, "timestamp"

    iget-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->timestampIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v1, "ActivityAlert"

    const-string v2, "uuid"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->uuidIndex:J

    .line 62
    const-string v1, "uuid"

    iget-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->uuidIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const-string v1, "ActivityAlert"

    const-string v2, "type"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->typeIndex:J

    .line 65
    const-string v1, "type"

    iget-wide v2, p0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->typeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    invoke-virtual {p0, v0}, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 68
    return-void
.end method
