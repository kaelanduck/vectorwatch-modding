.class final Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "CloudActivityDayRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/CloudActivityDayRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CloudActivityDayColumnInfo"
.end annotation


# instance fields
.field public final activityTypeIndex:J

.field public final avgAmplIndex:J

.field public final avgPeriodIndex:J

.field public final calIndex:J

.field public final dirtyCaloriesIndex:J

.field public final dirtyCloudIndex:J

.field public final dirtyDistanceIndex:J

.field public final dirtyStepsIndex:J

.field public final distIndex:J

.field public final effTimeIndex:J

.field public final offsetIndex:J

.field public final timestampIndex:J

.field public final timezoneIndex:J

.field public final valIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 49
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 50
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xe

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 51
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "CloudActivityDay"

    const-string v2, "timestamp"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timestampIndex:J

    .line 52
    const-string v1, "timestamp"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timestampIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v1, "CloudActivityDay"

    const-string v2, "effTime"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->effTimeIndex:J

    .line 55
    const-string v1, "effTime"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->effTimeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v1, "CloudActivityDay"

    const-string v2, "avgAmpl"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgAmplIndex:J

    .line 58
    const-string v1, "avgAmpl"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgAmplIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-string v1, "CloudActivityDay"

    const-string v2, "avgPeriod"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgPeriodIndex:J

    .line 61
    const-string v1, "avgPeriod"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->avgPeriodIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v1, "CloudActivityDay"

    const-string v2, "cal"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->calIndex:J

    .line 64
    const-string v1, "cal"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->calIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-string v1, "CloudActivityDay"

    const-string v2, "dist"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->distIndex:J

    .line 67
    const-string v1, "dist"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->distIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const-string v1, "CloudActivityDay"

    const-string v2, "timezone"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timezoneIndex:J

    .line 70
    const-string v1, "timezone"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->timezoneIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string v1, "CloudActivityDay"

    const-string v2, "val"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->valIndex:J

    .line 73
    const-string v1, "val"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->valIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const-string v1, "CloudActivityDay"

    const-string v2, "offset"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->offsetIndex:J

    .line 76
    const-string v1, "offset"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->offsetIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    const-string v1, "CloudActivityDay"

    const-string v2, "activityType"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->activityTypeIndex:J

    .line 79
    const-string v1, "activityType"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->activityTypeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    const-string v1, "CloudActivityDay"

    const-string v2, "dirtySteps"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyStepsIndex:J

    .line 82
    const-string v1, "dirtySteps"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyStepsIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    const-string v1, "CloudActivityDay"

    const-string v2, "dirtyCalories"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCaloriesIndex:J

    .line 85
    const-string v1, "dirtyCalories"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCaloriesIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    const-string v1, "CloudActivityDay"

    const-string v2, "dirtyDistance"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyDistanceIndex:J

    .line 88
    const-string v1, "dirtyDistance"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyDistanceIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const-string v1, "CloudActivityDay"

    const-string v2, "dirtyCloud"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCloudIndex:J

    .line 91
    const-string v1, "dirtyCloud"

    iget-wide v2, p0, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->dirtyCloudIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    invoke-virtual {p0, v0}, Lio/realm/CloudActivityDayRealmProxy$CloudActivityDayColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 94
    return-void
.end method
