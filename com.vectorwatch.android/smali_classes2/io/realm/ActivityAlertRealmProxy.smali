.class public Lio/realm/ActivityAlertRealmProxy;
.super Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
.source "ActivityAlertRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/ActivityAlertRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

.field private longValuesRealmList:Lio/realm/RealmList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            ">;"
        }
    .end annotation
.end field

.field private final proxyState:Lio/realm/ProxyState;

.field private stringValuesRealmList:Lio/realm/RealmList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "activityType"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    const-string v1, "stringValues"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    const-string v1, "longValues"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    const-string v1, "message"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    const-string v1, "timestamp"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    const-string v1, "uuid"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    const-string v1, "type"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/ActivityAlertRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 86
    return-void
.end method

.method constructor <init>(Lio/realm/internal/ColumnInfo;)V
    .locals 2
    .param p1, "columnInfo"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;-><init>()V

    .line 89
    check-cast p1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    .end local p1    # "columnInfo":Lio/realm/internal/ColumnInfo;
    iput-object p1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    .line 90
    new-instance v0, Lio/realm/ProxyState;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-direct {v0, v1, p0}, Lio/realm/ProxyState;-><init>(Ljava/lang/Class;Lio/realm/RealmModel;)V

    iput-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 91
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;"
        }
    .end annotation

    .prologue
    .line 542
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    const-class v15, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    move-object/from16 v14, p1

    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v14}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$timestamp()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v14}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v10

    check-cast v10, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    .local v10, "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    move-object v14, v10

    .line 543
    check-cast v14, Lio/realm/internal/RealmObjectProxy;

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v14, v10

    .line 544
    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object/from16 v15, p1

    check-cast v15, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v15}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$activityType()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    move-object/from16 v14, p1

    .line 546
    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v14}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v12

    .line 547
    .local v12, "stringValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    if-eqz v12, :cond_1

    move-object v14, v10

    .line 548
    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v14}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v13

    .line 549
    .local v13, "stringValuesRealmList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v12}, Lio/realm/RealmList;->size()I

    move-result v14

    if-ge v6, v14, :cond_1

    .line 550
    invoke-virtual {v12, v6}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v11

    check-cast v11, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    .line 551
    .local v11, "stringValuesItem":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    .line 552
    .local v5, "cachestringValues":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    if-eqz v5, :cond_0

    .line 553
    invoke-virtual {v13, v5}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 549
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 555
    :cond_0
    invoke-virtual {v12, v6}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v14

    check-cast v14, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/StringObjectRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object v14

    invoke-virtual {v13, v14}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    goto :goto_1

    .end local v5    # "cachestringValues":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .end local v6    # "i":I
    .end local v11    # "stringValuesItem":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .end local v13    # "stringValuesRealmList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    :cond_1
    move-object/from16 v14, p1

    .line 561
    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v14}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v8

    .line 562
    .local v8, "longValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    if-eqz v8, :cond_3

    move-object v14, v10

    .line 563
    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v14}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v9

    .line 564
    .local v9, "longValuesRealmList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_2
    invoke-virtual {v8}, Lio/realm/RealmList;->size()I

    move-result v14

    if-ge v6, v14, :cond_3

    .line 565
    invoke-virtual {v8, v6}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    .line 566
    .local v7, "longValuesItem":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    .line 567
    .local v4, "cachelongValues":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    if-eqz v4, :cond_2

    .line 568
    invoke-virtual {v9, v4}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 564
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 570
    :cond_2
    invoke-virtual {v8, v6}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v14

    check-cast v14, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/LongObjectRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object v14

    invoke-virtual {v9, v14}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    goto :goto_3

    .end local v4    # "cachelongValues":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .end local v6    # "i":I
    .end local v7    # "longValuesItem":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .end local v9    # "longValuesRealmList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    :cond_3
    move-object v14, v10

    .line 575
    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object/from16 v15, p1

    check-cast v15, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v15}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$message()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$message(Ljava/lang/String;)V

    move-object v14, v10

    .line 576
    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object/from16 v15, p1

    check-cast v15, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v15}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$timestamp()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-interface {v14, v0, v1}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$timestamp(J)V

    move-object v14, v10

    .line 577
    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object/from16 v15, p1

    check-cast v15, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v15}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$uuid()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$uuid(Ljava/lang/String;)V

    move-object v14, v10

    .line 578
    check-cast v14, Lio/realm/ActivityAlertRealmProxyInterface;

    check-cast p1, Lio/realm/ActivityAlertRealmProxyInterface;

    .end local p1    # "newObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    invoke-interface/range {p1 .. p1}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$type()I

    move-result v15

    invoke-interface {v14, v15}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$type(I)V

    .line 579
    return-object v10
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .locals 12
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;"
        }
    .end annotation

    .prologue
    .line 512
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    instance-of v7, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v7, :cond_0

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    if-eqz v7, :cond_0

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    iget-wide v8, v7, Lio/realm/BaseRealm;->threadId:J

    iget-wide v10, p0, Lio/realm/Realm;->threadId:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 513
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 515
    :cond_0
    instance-of v7, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v7, :cond_1

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    if-eqz v7, :cond_1

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 537
    .end local p1    # "object":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    :goto_0
    return-object p1

    .line 518
    .restart local p1    # "object":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    :cond_1
    const/4 v1, 0x0

    .line 519
    .local v1, "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    move v0, p2

    .line 520
    .local v0, "canUpdate":Z
    if-eqz v0, :cond_2

    .line 521
    const-class v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p0, v7}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v6

    .line 522
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .local v2, "pkColumnIndex":J
    move-object v7, p1

    .line 523
    check-cast v7, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v7}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$timestamp()J

    move-result-wide v8

    invoke-virtual {v6, v2, v3, v8, v9}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v4

    .line 524
    .local v4, "rowIndex":J
    const-wide/16 v8, -0x1

    cmp-long v7, v4, v8

    if-eqz v7, :cond_3

    .line 525
    new-instance v1, Lio/realm/ActivityAlertRealmProxy;

    .end local v1    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    iget-object v7, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v8, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {v7, v8}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    invoke-direct {v1, v7}, Lio/realm/ActivityAlertRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v1    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    move-object v7, v1

    .line 526
    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v7, v1

    .line 527
    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v8

    invoke-virtual {v7, v8}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    move-object v7, v1

    .line 528
    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    .line 535
    invoke-static {p0, v1, p1, p3}, Lio/realm/ActivityAlertRealmProxy;->update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;Ljava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    move-result-object p1

    goto :goto_0

    .line 530
    .restart local v2    # "pkColumnIndex":J
    .restart local v4    # "rowIndex":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 537
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_4
    invoke-static {p0, p1, p2, p3}, Lio/realm/ActivityAlertRealmProxy;->copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    move-result-object p1

    goto :goto_0
.end method

.method public static createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .locals 16
    .param p0, "realmObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;"
        }
    .end annotation

    .prologue
    .line 583
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    move/from16 v0, p1

    move/from16 v1, p2

    if-gt v0, v1, :cond_0

    if-nez p0, :cond_1

    .line 584
    :cond_0
    const/4 v12, 0x0

    .line 635
    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    :goto_0
    return-object v12

    .line 586
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    :cond_1
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 588
    .local v2, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v2, :cond_5

    .line 590
    iget v12, v2, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    move/from16 v0, p1

    if-lt v0, v12, :cond_2

    .line 591
    iget-object v12, v2, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v12, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    goto :goto_0

    .line 593
    :cond_2
    iget-object v9, v2, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    .line 594
    .local v9, "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    move/from16 v0, p1

    iput v0, v2, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v12, v9

    .line 600
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object/from16 v13, p0

    check-cast v13, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$activityType()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    .line 603
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_6

    move-object v12, v9

    .line 604
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    const/4 v13, 0x0

    invoke-interface {v12, v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$stringValues(Lio/realm/RealmList;)V

    .line 618
    :cond_3
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_7

    move-object v12, v9

    .line 619
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    const/4 v13, 0x0

    invoke-interface {v12, v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$longValues(Lio/realm/RealmList;)V

    :cond_4
    move-object v12, v9

    .line 631
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object/from16 v13, p0

    check-cast v13, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$message()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$message(Ljava/lang/String;)V

    move-object v12, v9

    .line 632
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object/from16 v13, p0

    check-cast v13, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$timestamp()J

    move-result-wide v14

    invoke-interface {v12, v14, v15}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$timestamp(J)V

    move-object v12, v9

    .line 633
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object/from16 v13, p0

    check-cast v13, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$uuid()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$uuid(Ljava/lang/String;)V

    move-object v12, v9

    .line 634
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    check-cast p0, Lio/realm/ActivityAlertRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    invoke-interface/range {p0 .. p0}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$type()I

    move-result v13

    invoke-interface {v12, v13}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$type(I)V

    move-object v12, v9

    .line 635
    goto :goto_0

    .line 597
    .end local v9    # "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    :cond_5
    new-instance v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-direct {v9}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;-><init>()V

    .line 598
    .restart local v9    # "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    new-instance v12, Lio/realm/internal/RealmObjectProxy$CacheData;

    move/from16 v0, p1

    invoke-direct {v12, v0, v9}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    invoke-interface {v0, v1, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_6
    move-object/from16 v12, p0

    .line 606
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v12}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v6

    .line 607
    .local v6, "managedstringValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    new-instance v11, Lio/realm/RealmList;

    invoke-direct {v11}, Lio/realm/RealmList;-><init>()V

    .local v11, "unmanagedstringValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    move-object v12, v9

    .line 608
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v12, v11}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$stringValues(Lio/realm/RealmList;)V

    .line 609
    add-int/lit8 v7, p1, 0x1

    .line 610
    .local v7, "nextDepth":I
    invoke-virtual {v6}, Lio/realm/RealmList;->size()I

    move-result v8

    .line 611
    .local v8, "size":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v8, :cond_3

    .line 612
    invoke-virtual {v6, v3}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v12

    check-cast v12, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v12, v7, v0, v1}, Lio/realm/StringObjectRealmProxy;->createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object v4

    .line 613
    .local v4, "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    invoke-virtual {v11, v4}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 611
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v3    # "i":I
    .end local v4    # "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .end local v6    # "managedstringValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    .end local v7    # "nextDepth":I
    .end local v8    # "size":I
    .end local v11    # "unmanagedstringValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    :cond_7
    move-object/from16 v12, p0

    .line 621
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v12}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v5

    .line 622
    .local v5, "managedlongValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    new-instance v10, Lio/realm/RealmList;

    invoke-direct {v10}, Lio/realm/RealmList;-><init>()V

    .local v10, "unmanagedlongValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    move-object v12, v9

    .line 623
    check-cast v12, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v12, v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$longValues(Lio/realm/RealmList;)V

    .line 624
    add-int/lit8 v7, p1, 0x1

    .line 625
    .restart local v7    # "nextDepth":I
    invoke-virtual {v5}, Lio/realm/RealmList;->size()I

    move-result v8

    .line 626
    .restart local v8    # "size":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    if-ge v3, v8, :cond_4

    .line 627
    invoke-virtual {v5, v3}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v12

    check-cast v12, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v12, v7, v0, v1}, Lio/realm/LongObjectRealmProxy;->createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object v4

    .line 628
    .local v4, "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    invoke-virtual {v10, v4}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 626
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .locals 13
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 350
    const/4 v3, 0x0

    .line 351
    .local v3, "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    if-eqz p2, :cond_1

    .line 352
    const-class v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p0, v9}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v8

    .line 353
    .local v8, "table":Lio/realm/internal/Table;
    invoke-virtual {v8}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 354
    .local v4, "pkColumnIndex":J
    const-wide/16 v6, -0x1

    .line 355
    .local v6, "rowIndex":J
    const-string v9, "timestamp"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 356
    const-string v9, "timestamp"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v8, v4, v5, v10, v11}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v6

    .line 358
    :cond_0
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-eqz v9, :cond_1

    .line 359
    new-instance v3, Lio/realm/ActivityAlertRealmProxy;

    .end local v3    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    iget-object v9, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v10, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {v9, v10}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v9

    invoke-direct {v3, v9}, Lio/realm/ActivityAlertRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v3    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    move-object v9, v3

    .line 360
    check-cast v9, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v9}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v9

    invoke-virtual {v9, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v9, v3

    .line 361
    check-cast v9, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v9}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v9

    invoke-virtual {v8, v6, v7}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v10

    invoke-virtual {v9, v10}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 364
    .end local v4    # "pkColumnIndex":J
    .end local v6    # "rowIndex":J
    .end local v8    # "table":Lio/realm/internal/Table;
    :cond_1
    if-nez v3, :cond_2

    .line 365
    const-string v9, "timestamp"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 366
    const-string v9, "timestamp"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 367
    const-class v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p0, v9, v12}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v3

    .end local v3    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    check-cast v3, Lio/realm/ActivityAlertRealmProxy;

    .line 375
    .restart local v3    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    :cond_2
    :goto_0
    const-string v9, "activityType"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 376
    const-string v9, "activityType"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    move-object v9, v3

    .line 377
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9, v12}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    .line 382
    :cond_3
    :goto_1
    const-string v9, "stringValues"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 383
    const-string v9, "stringValues"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    move-object v9, v3

    .line 384
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9, v12}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$stringValues(Lio/realm/RealmList;)V

    .line 394
    :cond_4
    const-string v9, "longValues"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 395
    const-string v9, "longValues"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    move-object v9, v3

    .line 396
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9, v12}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$longValues(Lio/realm/RealmList;)V

    .line 406
    :cond_5
    const-string v9, "message"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 407
    const-string v9, "message"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    move-object v9, v3

    .line 408
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9, v12}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$message(Ljava/lang/String;)V

    .line 413
    :cond_6
    :goto_2
    const-string v9, "timestamp"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 414
    const-string v9, "timestamp"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 415
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string v10, "Trying to set non-nullable field timestamp to null."

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 369
    :cond_7
    const-class v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    const-string v10, "timestamp"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {p0, v9, v10}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v3

    .end local v3    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    check-cast v3, Lio/realm/ActivityAlertRealmProxy;

    .restart local v3    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    goto/16 :goto_0

    .line 372
    :cond_8
    const-class v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p0, v9}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v3

    .end local v3    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    check-cast v3, Lio/realm/ActivityAlertRealmProxy;

    .restart local v3    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    goto/16 :goto_0

    :cond_9
    move-object v9, v3

    .line 379
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    const-string v10, "activityType"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    move-object v9, v3

    .line 386
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v9

    invoke-virtual {v9}, Lio/realm/RealmList;->clear()V

    .line 387
    const-string v9, "stringValues"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 388
    .local v0, "array":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v1, v9, :cond_4

    .line 389
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    invoke-static {p0, v9, p2}, Lio/realm/StringObjectRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object v2

    .local v2, "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    move-object v9, v3

    .line 390
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v9

    invoke-virtual {v9, v2}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 388
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v1    # "i":I
    .end local v2    # "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    :cond_b
    move-object v9, v3

    .line 398
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v9

    invoke-virtual {v9}, Lio/realm/RealmList;->clear()V

    .line 399
    const-string v9, "longValues"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 400
    .restart local v0    # "array":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v1, v9, :cond_5

    .line 401
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    invoke-static {p0, v9, p2}, Lio/realm/LongObjectRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object v2

    .local v2, "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    move-object v9, v3

    .line 402
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v9

    invoke-virtual {v9, v2}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 400
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v1    # "i":I
    .end local v2    # "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    :cond_c
    move-object v9, v3

    .line 410
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    const-string v10, "message"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$message(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_d
    move-object v9, v3

    .line 417
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    const-string v10, "timestamp"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-interface {v9, v10, v11}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$timestamp(J)V

    .line 420
    :cond_e
    const-string v9, "uuid"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 421
    const-string v9, "uuid"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_10

    move-object v9, v3

    .line 422
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9, v12}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$uuid(Ljava/lang/String;)V

    .line 427
    :cond_f
    :goto_5
    const-string v9, "type"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 428
    const-string v9, "type"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 429
    new-instance v9, Ljava/lang/IllegalArgumentException;

    const-string v10, "Trying to set non-nullable field type to null."

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_10
    move-object v9, v3

    .line 424
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    const-string v10, "uuid"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$uuid(Ljava/lang/String;)V

    goto :goto_5

    :cond_11
    move-object v9, v3

    .line 431
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    const-string v10, "type"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v9, v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$type(I)V

    .line 434
    :cond_12
    return-object v3
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .locals 7
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 440
    const-class v3, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p0, v3}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    .line 441
    .local v2, "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 442
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 443
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 444
    .local v1, "name":Ljava/lang/String;
    const-string v3, "activityType"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 445
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_0

    .line 446
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v3, v2

    .line 447
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v3, v6}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move-object v3, v2

    .line 449
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    goto :goto_0

    .line 451
    :cond_1
    const-string v3, "stringValues"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 452
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_2

    .line 453
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v3, v2

    .line 454
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v3, v6}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$stringValues(Lio/realm/RealmList;)V

    goto :goto_0

    .line 456
    :cond_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 457
    :goto_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 458
    invoke-static {p0, p1}, Lio/realm/StringObjectRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object v0

    .local v0, "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    move-object v3, v2

    .line 459
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v3

    invoke-virtual {v3, v0}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    goto :goto_1

    .line 461
    .end local v0    # "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    :cond_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    goto :goto_0

    .line 463
    :cond_4
    const-string v3, "longValues"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 464
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_5

    .line 465
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v3, v2

    .line 466
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v3, v6}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$longValues(Lio/realm/RealmList;)V

    goto :goto_0

    .line 468
    :cond_5
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 469
    :goto_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 470
    invoke-static {p0, p1}, Lio/realm/LongObjectRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object v0

    .local v0, "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    move-object v3, v2

    .line 471
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v3

    invoke-virtual {v3, v0}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    goto :goto_2

    .line 473
    .end local v0    # "item":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    :cond_6
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    goto/16 :goto_0

    .line 475
    :cond_7
    const-string v3, "message"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 476
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_8

    .line 477
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v3, v2

    .line 478
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v3, v6}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$message(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    move-object v3, v2

    .line 480
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$message(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 482
    :cond_9
    const-string v3, "timestamp"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 483
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_a

    .line 484
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 485
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field timestamp to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_a
    move-object v3, v2

    .line 487
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$timestamp(J)V

    goto/16 :goto_0

    .line 489
    :cond_b
    const-string v3, "uuid"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 490
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_c

    .line 491
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v3, v2

    .line 492
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v3, v6}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$uuid(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    move-object v3, v2

    .line 494
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$uuid(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 496
    :cond_d
    const-string v3, "type"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 497
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_e

    .line 498
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 499
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field type to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_e
    move-object v3, v2

    .line 501
    check-cast v3, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$type(I)V

    goto/16 :goto_0

    .line 504
    :cond_f
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 507
    .end local v1    # "name":Ljava/lang/String;
    :cond_10
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 508
    return-object v2
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344
    sget-object v0, Lio/realm/ActivityAlertRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    const-string v0, "class_ActivityAlert"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;
    .locals 6
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 221
    const-string v1, "class_ActivityAlert"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 222
    const-string v1, "class_ActivityAlert"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 223
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v2, "activityType"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 224
    const-string v1, "class_StringObject"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 225
    invoke-static {p0}, Lio/realm/StringObjectRealmProxy;->initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;

    .line 227
    :cond_0
    sget-object v1, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    const-string v2, "stringValues"

    const-string v3, "class_StringObject"

    invoke-virtual {p0, v3}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumnLink(Lio/realm/RealmFieldType;Ljava/lang/String;Lio/realm/internal/Table;)J

    .line 228
    const-string v1, "class_LongObject"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 229
    invoke-static {p0}, Lio/realm/LongObjectRealmProxy;->initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;

    .line 231
    :cond_1
    sget-object v1, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    const-string v2, "longValues"

    const-string v3, "class_LongObject"

    invoke-virtual {p0, v3}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumnLink(Lio/realm/RealmFieldType;Ljava/lang/String;Lio/realm/internal/Table;)J

    .line 232
    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v2, "message"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 233
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2, v5}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 234
    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v2, "uuid"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 235
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "type"

    invoke-virtual {v0, v1, v2, v5}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 236
    const-string v1, "timestamp"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 237
    const-string v1, "timestamp"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 240
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_2
    const-string v1, "class_ActivityAlert"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method static update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;Ljava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .locals 12
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .param p2, "newObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;"
        }
    .end annotation

    .prologue
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    const/4 v11, 0x1

    .line 639
    move-object v9, p1

    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object v10, p2

    check-cast v10, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$activityType()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$activityType(Ljava/lang/String;)V

    move-object v9, p2

    .line 640
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v7

    .local v7, "stringValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    move-object v9, p1

    .line 641
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v8

    .line 642
    .local v8, "stringValuesRealmList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    invoke-virtual {v8}, Lio/realm/RealmList;->clear()V

    .line 643
    if-eqz v7, :cond_1

    .line 644
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v7}, Lio/realm/RealmList;->size()I

    move-result v9

    if-ge v2, v9, :cond_1

    .line 645
    invoke-virtual {v7, v2}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    .line 646
    .local v6, "stringValuesItem":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    invoke-interface {p3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    .line 647
    .local v1, "cachestringValues":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    if-eqz v1, :cond_0

    .line 648
    invoke-virtual {v8, v1}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 644
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 650
    :cond_0
    invoke-virtual {v7, v2}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v9

    check-cast v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-static {p0, v9, v11, p3}, Lio/realm/StringObjectRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    move-result-object v9

    invoke-virtual {v8, v9}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    goto :goto_1

    .end local v1    # "cachestringValues":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    .end local v2    # "i":I
    .end local v6    # "stringValuesItem":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
    :cond_1
    move-object v9, p2

    .line 654
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v4

    .local v4, "longValuesList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    move-object v9, p1

    .line 655
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v9}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v5

    .line 656
    .local v5, "longValuesRealmList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    invoke-virtual {v5}, Lio/realm/RealmList;->clear()V

    .line 657
    if-eqz v4, :cond_3

    .line 658
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    invoke-virtual {v4}, Lio/realm/RealmList;->size()I

    move-result v9

    if-ge v2, v9, :cond_3

    .line 659
    invoke-virtual {v4, v2}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    .line 660
    .local v3, "longValuesItem":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    invoke-interface {p3, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    .line 661
    .local v0, "cachelongValues":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    if-eqz v0, :cond_2

    .line 662
    invoke-virtual {v5, v0}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 658
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 664
    :cond_2
    invoke-virtual {v4, v2}, Lio/realm/RealmList;->get(I)Lio/realm/RealmModel;

    move-result-object v9

    check-cast v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-static {p0, v9, v11, p3}, Lio/realm/LongObjectRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object v9

    invoke-virtual {v5, v9}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    goto :goto_3

    .end local v0    # "cachelongValues":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .end local v2    # "i":I
    .end local v3    # "longValuesItem":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    :cond_3
    move-object v9, p1

    .line 668
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object v10, p2

    check-cast v10, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$message()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$message(Ljava/lang/String;)V

    move-object v9, p1

    .line 669
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    move-object v10, p2

    check-cast v10, Lio/realm/ActivityAlertRealmProxyInterface;

    invoke-interface {v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$uuid()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$uuid(Ljava/lang/String;)V

    move-object v9, p1

    .line 670
    check-cast v9, Lio/realm/ActivityAlertRealmProxyInterface;

    check-cast p2, Lio/realm/ActivityAlertRealmProxyInterface;

    .end local p2    # "newObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    invoke-interface {p2}, Lio/realm/ActivityAlertRealmProxyInterface;->realmGet$type()I

    move-result v10

    invoke-interface {v9, v10}, Lio/realm/ActivityAlertRealmProxyInterface;->realmSet$type(I)V

    .line 671
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;
    .locals 12
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    const-wide/16 v10, 0x7

    .line 244
    const-string v7, "class_ActivityAlert"

    invoke-virtual {p0, v7}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1a

    .line 245
    const-string v7, "class_ActivityAlert"

    invoke-virtual {p0, v7}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v4

    .line 246
    .local v4, "table":Lio/realm/internal/Table;
    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v8

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 247
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count does not match - expected 7 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 249
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 250
    .local v1, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v2, 0x0

    .local v2, "i":J
    :goto_0
    cmp-long v7, v2, v10

    if-gez v7, :cond_1

    .line 251
    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v8

    invoke-interface {v1, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    const-wide/16 v8, 0x1

    add-long/2addr v2, v8

    goto :goto_0

    .line 254
    :cond_1
    new-instance v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7, v4}, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 256
    .local v0, "columnInfo":Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;
    const-string v7, "activityType"

    invoke-interface {v1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 257
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'activityType\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 259
    :cond_2
    const-string v7, "activityType"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_3

    .line 260
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'String\' for field \'activityType\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 262
    :cond_3
    iget-wide v8, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->activityTypeIndex:J

    invoke-virtual {v4, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-nez v7, :cond_4

    .line 263
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'activityType\' is required. Either set @Required to field \'activityType\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 265
    :cond_4
    const-string v7, "stringValues"

    invoke-interface {v1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 266
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'stringValues\'"

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 268
    :cond_5
    const-string v7, "stringValues"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_6

    .line 269
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'StringObject\' for field \'stringValues\'"

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 271
    :cond_6
    const-string v7, "class_StringObject"

    invoke-virtual {p0, v7}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 272
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing class \'class_StringObject\' for field \'stringValues\'"

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 274
    :cond_7
    const-string v7, "class_StringObject"

    invoke-virtual {p0, v7}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v5

    .line 275
    .local v5, "table_1":Lio/realm/internal/Table;
    iget-wide v8, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->stringValuesIndex:J

    invoke-virtual {v4, v8, v9}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7, v5}, Lio/realm/internal/Table;->hasSameSchema(Lio/realm/internal/Table;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 276
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid RealmList type for field \'stringValues\': \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->stringValuesIndex:J

    invoke-virtual {v4, v10, v11}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v10

    invoke-virtual {v10}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\' expected - was \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 278
    :cond_8
    const-string v7, "longValues"

    invoke-interface {v1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 279
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'longValues\'"

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 281
    :cond_9
    const-string v7, "longValues"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_a

    .line 282
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'LongObject\' for field \'longValues\'"

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 284
    :cond_a
    const-string v7, "class_LongObject"

    invoke-virtual {p0, v7}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 285
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing class \'class_LongObject\' for field \'longValues\'"

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 287
    :cond_b
    const-string v7, "class_LongObject"

    invoke-virtual {p0, v7}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v6

    .line 288
    .local v6, "table_2":Lio/realm/internal/Table;
    iget-wide v8, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->longValuesIndex:J

    invoke-virtual {v4, v8, v9}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7, v6}, Lio/realm/internal/Table;->hasSameSchema(Lio/realm/internal/Table;)Z

    move-result v7

    if-nez v7, :cond_c

    .line 289
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid RealmList type for field \'longValues\': \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->longValuesIndex:J

    invoke-virtual {v4, v10, v11}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v10

    invoke-virtual {v10}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\' expected - was \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 291
    :cond_c
    const-string v7, "message"

    invoke-interface {v1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 292
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'message\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 294
    :cond_d
    const-string v7, "message"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_e

    .line 295
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'String\' for field \'message\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 297
    :cond_e
    iget-wide v8, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->messageIndex:J

    invoke-virtual {v4, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-nez v7, :cond_f

    .line 298
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'message\' is required. Either set @Required to field \'message\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 300
    :cond_f
    const-string v7, "timestamp"

    invoke-interface {v1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_10

    .line 301
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'timestamp\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 303
    :cond_10
    const-string v7, "timestamp"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_11

    .line 304
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'long\' for field \'timestamp\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 306
    :cond_11
    iget-wide v8, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->timestampIndex:J

    invoke-virtual {v4, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_12

    iget-wide v8, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->timestampIndex:J

    invoke-virtual {v4, v8, v9}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_12

    .line 307
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Cannot migrate an object with null value in field \'timestamp\'. Either maintain the same type for primary key field \'timestamp\', or remove the object with null value before migration."

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 309
    :cond_12
    invoke-virtual {v4}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    const-string v7, "timestamp"

    invoke-virtual {v4, v7}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_13

    .line 310
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Primary key not defined for field \'timestamp\' in existing Realm file. Add @PrimaryKey."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 312
    :cond_13
    const-string v7, "timestamp"

    invoke-virtual {v4, v7}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v7

    if-nez v7, :cond_14

    .line 313
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Index not defined for field \'timestamp\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 315
    :cond_14
    const-string v7, "uuid"

    invoke-interface {v1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_15

    .line 316
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'uuid\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 318
    :cond_15
    const-string v7, "uuid"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_16

    .line 319
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'String\' for field \'uuid\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 321
    :cond_16
    iget-wide v8, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->uuidIndex:J

    invoke-virtual {v4, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-nez v7, :cond_17

    .line 322
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'uuid\' is required. Either set @Required to field \'uuid\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 324
    :cond_17
    const-string v7, "type"

    invoke-interface {v1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_18

    .line 325
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'type\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 327
    :cond_18
    const-string v7, "type"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_19

    .line 328
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'type\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 330
    :cond_19
    iget-wide v8, v0, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->typeIndex:J

    invoke-virtual {v4, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_1b

    .line 331
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'type\' does support null values in the existing Realm file. Use corresponding boxed type for field \'type\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 335
    .end local v0    # "columnInfo":Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;
    .end local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v2    # "i":J
    .end local v4    # "table":Lio/realm/internal/Table;
    .end local v5    # "table_1":Lio/realm/internal/Table;
    .end local v6    # "table_2":Lio/realm/internal/Table;
    :cond_1a
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "The ActivityAlert class is missing from the schema for this Realm."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 333
    .restart local v0    # "columnInfo":Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;
    .restart local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v2    # "i":J
    .restart local v4    # "table":Lio/realm/internal/Table;
    .restart local v5    # "table_1":Lio/realm/internal/Table;
    .restart local v6    # "table_2":Lio/realm/internal/Table;
    :cond_1b
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 731
    if-ne p0, p1, :cond_1

    .line 745
    :cond_0
    :goto_0
    return v5

    .line 732
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 733
    check-cast v0, Lio/realm/ActivityAlertRealmProxy;

    .line 735
    .local v0, "aActivityAlert":Lio/realm/ActivityAlertRealmProxy;
    iget-object v7, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 736
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 737
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 739
    :cond_6
    iget-object v7, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 740
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 741
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 743
    :cond_9
    iget-object v7, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 718
    iget-object v6, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 719
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 720
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 722
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 723
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 724
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 725
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 726
    return v1

    :cond_1
    move v6, v5

    .line 723
    goto :goto_0
.end method

.method public realmGet$activityType()Ljava/lang/String;
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 96
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->activityTypeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public realmGet$longValues()Lio/realm/RealmList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 141
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->longValuesRealmList:Lio/realm/RealmList;

    if-eqz v1, :cond_0

    .line 142
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->longValuesRealmList:Lio/realm/RealmList;

    .line 146
    :goto_0
    return-object v1

    .line 144
    :cond_0
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v2, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->longValuesIndex:J

    invoke-interface {v1, v2, v3}, Lio/realm/internal/Row;->getLinkList(J)Lio/realm/internal/LinkView;

    move-result-object v0

    .line 145
    .local v0, "linkView":Lio/realm/internal/LinkView;
    new-instance v1, Lio/realm/RealmList;

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    iget-object v3, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lio/realm/RealmList;-><init>(Ljava/lang/Class;Lio/realm/internal/LinkView;Lio/realm/BaseRealm;)V

    iput-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->longValuesRealmList:Lio/realm/RealmList;

    .line 146
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->longValuesRealmList:Lio/realm/RealmList;

    goto :goto_0
.end method

.method public realmGet$message()Ljava/lang/String;
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 171
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->messageIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$stringValues()Lio/realm/RealmList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 111
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->stringValuesRealmList:Lio/realm/RealmList;

    if-eqz v1, :cond_0

    .line 112
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->stringValuesRealmList:Lio/realm/RealmList;

    .line 116
    :goto_0
    return-object v1

    .line 114
    :cond_0
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v2, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->stringValuesIndex:J

    invoke-interface {v1, v2, v3}, Lio/realm/internal/Row;->getLinkList(J)Lio/realm/internal/LinkView;

    move-result-object v0

    .line 115
    .local v0, "linkView":Lio/realm/internal/LinkView;
    new-instance v1, Lio/realm/RealmList;

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    iget-object v3, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v3}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lio/realm/RealmList;-><init>(Ljava/lang/Class;Lio/realm/internal/LinkView;Lio/realm/BaseRealm;)V

    iput-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->stringValuesRealmList:Lio/realm/RealmList;

    .line 116
    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->stringValuesRealmList:Lio/realm/RealmList;

    goto :goto_0
.end method

.method public realmGet$timestamp()J
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 186
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->timestampIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$type()I
    .locals 4

    .prologue
    .line 211
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 212
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->typeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$uuid()Ljava/lang/String;
    .locals 4

    .prologue
    .line 196
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 197
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->uuidIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public realmSet$activityType(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 100
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 101
    if-nez p1, :cond_0

    .line 102
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->activityTypeIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->setNull(J)V

    .line 106
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->activityTypeIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setString(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public realmSet$longValues(Lio/realm/RealmList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "value":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    iget-object v2, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 152
    iget-object v2, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    iget-object v3, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v4, v3, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->longValuesIndex:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/Row;->getLinkList(J)Lio/realm/internal/LinkView;

    move-result-object v1

    .line 153
    .local v1, "links":Lio/realm/internal/LinkView;
    invoke-virtual {v1}, Lio/realm/internal/LinkView;->clear()V

    .line 154
    if-nez p1, :cond_1

    .line 166
    :cond_0
    return-void

    .line 157
    :cond_1
    invoke-virtual {p1}, Lio/realm/RealmList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 158
    .local v0, "linkedObject":Lio/realm/RealmModel;
    invoke-static {v0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 159
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Each element of \'value\' must be a valid managed object."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    move-object v2, v0

    .line 161
    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    iget-object v4, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v4

    if-eq v2, v4, :cond_3

    .line 162
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Each element of \'value\' must belong to the same Realm."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 164
    :cond_3
    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .end local v0    # "linkedObject":Lio/realm/RealmModel;
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lio/realm/internal/LinkView;->add(J)V

    goto :goto_0
.end method

.method public realmSet$message(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 175
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 176
    if-nez p1, :cond_0

    .line 177
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->messageIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->setNull(J)V

    .line 181
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->messageIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setString(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public realmSet$stringValues(Lio/realm/RealmList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "value":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    iget-object v2, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 122
    iget-object v2, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    iget-object v3, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v4, v3, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->stringValuesIndex:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/Row;->getLinkList(J)Lio/realm/internal/LinkView;

    move-result-object v1

    .line 123
    .local v1, "links":Lio/realm/internal/LinkView;
    invoke-virtual {v1}, Lio/realm/internal/LinkView;->clear()V

    .line 124
    if-nez p1, :cond_1

    .line 136
    :cond_0
    return-void

    .line 127
    :cond_1
    invoke-virtual {p1}, Lio/realm/RealmList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 128
    .local v0, "linkedObject":Lio/realm/RealmModel;
    invoke-static {v0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 129
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Each element of \'value\' must be a valid managed object."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    move-object v2, v0

    .line 131
    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v2}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    iget-object v4, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v4}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v4

    if-eq v2, v4, :cond_3

    .line 132
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Each element of \'value\' must belong to the same Realm."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 134
    :cond_3
    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .end local v0    # "linkedObject":Lio/realm/RealmModel;
    invoke-interface {v0}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    invoke-interface {v2}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lio/realm/internal/LinkView;->add(J)V

    goto :goto_0
.end method

.method public realmSet$timestamp(J)V
    .locals 5
    .param p1, "value"    # J

    .prologue
    .line 190
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 191
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->timestampIndex:J

    invoke-interface {v0, v2, v3, p1, p2}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 192
    return-void
.end method

.method public realmSet$type(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 216
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 217
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->typeIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 218
    return-void
.end method

.method public realmSet$uuid(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 202
    if-nez p1, :cond_0

    .line 203
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->uuidIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->setNull(J)V

    .line 207
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lio/realm/ActivityAlertRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ActivityAlertRealmProxy;->columnInfo:Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;

    iget-wide v2, v1, Lio/realm/ActivityAlertRealmProxy$ActivityAlertColumnInfo;->uuidIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setString(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 676
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 677
    const-string v1, "Invalid object"

    .line 708
    :goto_0
    return-object v1

    .line 679
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ActivityAlert = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 680
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{activityType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$activityType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$activityType()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 683
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 684
    const-string v1, "{stringValues:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 685
    const-string v1, "RealmList<StringObject>["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/RealmList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 686
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 687
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 688
    const-string v1, "{longValues:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    const-string v1, "RealmList<LongObject>["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/RealmList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 690
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 692
    const-string v1, "{message:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 693
    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$message()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$message()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 696
    const-string v1, "{timestamp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$timestamp()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 698
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    const-string v1, "{uuid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$uuid()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$uuid()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 703
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 704
    const-string v1, "{type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 705
    invoke-virtual {p0}, Lio/realm/ActivityAlertRealmProxy;->realmGet$type()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 706
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 707
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 708
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 681
    :cond_1
    const-string v1, "null"

    goto/16 :goto_1

    .line 693
    :cond_2
    const-string v1, "null"

    goto :goto_2

    .line 701
    :cond_3
    const-string v1, "null"

    goto :goto_3
.end method
