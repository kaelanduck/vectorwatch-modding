.class final Lio/realm/AlarmRealmProxy$AlarmColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "AlarmRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/AlarmRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "AlarmColumnInfo"
.end annotation


# instance fields
.field public final idIndex:J

.field public final isEnabledIndex:J

.field public final nameIndex:J

.field public final numberOfHoursIndex:J

.field public final numberOfMinutesIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 40
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 41
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 42
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "Alarm"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->idIndex:J

    .line 43
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const-string v1, "Alarm"

    const-string v2, "numberOfHours"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfHoursIndex:J

    .line 46
    const-string v1, "numberOfHours"

    iget-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfHoursIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v1, "Alarm"

    const-string v2, "numberOfMinutes"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfMinutesIndex:J

    .line 49
    const-string v1, "numberOfMinutes"

    iget-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfMinutesIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "Alarm"

    const-string v2, "name"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->nameIndex:J

    .line 52
    const-string v1, "name"

    iget-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->nameIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v1, "Alarm"

    const-string v2, "isEnabled"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->isEnabledIndex:J

    .line 55
    const-string v1, "isEnabled"

    iget-wide v2, p0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->isEnabledIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    invoke-virtual {p0, v0}, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 58
    return-void
.end method
