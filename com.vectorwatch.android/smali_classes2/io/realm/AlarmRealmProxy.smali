.class public Lio/realm/AlarmRealmProxy;
.super Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
.source "AlarmRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/AlarmRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/AlarmRealmProxy$AlarmColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

.field private final proxyState:Lio/realm/ProxyState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    const-string v1, "numberOfHours"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    const-string v1, "numberOfMinutes"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    const-string v1, "name"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    const-string v1, "isEnabled"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/AlarmRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 72
    return-void
.end method

.method constructor <init>(Lio/realm/internal/ColumnInfo;)V
    .locals 2
    .param p1, "columnInfo"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;-><init>()V

    .line 75
    check-cast p1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    .end local p1    # "columnInfo":Lio/realm/internal/ColumnInfo;
    iput-object p1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    .line 76
    new-instance v0, Lio/realm/ProxyState;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-direct {v0, v1, p0}, Lio/realm/ProxyState;-><init>(Ljava/lang/Class;Lio/realm/RealmModel;)V

    iput-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 77
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;"
        }
    .end annotation

    .prologue
    .line 378
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    move-object v1, p1

    check-cast v1, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/AlarmRealmProxyInterface;->realmGet$id()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .local v0, "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    move-object v1, v0

    .line 379
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 380
    check-cast v1, Lio/realm/AlarmRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/AlarmRealmProxyInterface;->realmGet$id()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$id(J)V

    move-object v1, v0

    .line 381
    check-cast v1, Lio/realm/AlarmRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/AlarmRealmProxyInterface;->realmGet$numberOfHours()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfHours(I)V

    move-object v1, v0

    .line 382
    check-cast v1, Lio/realm/AlarmRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/AlarmRealmProxyInterface;->realmGet$numberOfMinutes()I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfMinutes(I)V

    move-object v1, v0

    .line 383
    check-cast v1, Lio/realm/AlarmRealmProxyInterface;

    move-object v2, p1

    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/AlarmRealmProxyInterface;->realmGet$name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/AlarmRealmProxyInterface;->realmSet$name(Ljava/lang/String;)V

    move-object v1, v0

    .line 384
    check-cast v1, Lio/realm/AlarmRealmProxyInterface;

    check-cast p1, Lio/realm/AlarmRealmProxyInterface;

    .end local p1    # "newObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    invoke-interface {p1}, Lio/realm/AlarmRealmProxyInterface;->realmGet$isEnabled()B

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/AlarmRealmProxyInterface;->realmSet$isEnabled(B)V

    .line 385
    return-object v0
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .locals 12
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;"
        }
    .end annotation

    .prologue
    .line 348
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    instance-of v7, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v7, :cond_0

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    if-eqz v7, :cond_0

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    iget-wide v8, v7, Lio/realm/BaseRealm;->threadId:J

    iget-wide v10, p0, Lio/realm/Realm;->threadId:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 349
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 351
    :cond_0
    instance-of v7, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v7, :cond_1

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    if-eqz v7, :cond_1

    move-object v7, p1

    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 373
    .end local p1    # "object":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    :goto_0
    return-object p1

    .line 354
    .restart local p1    # "object":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    :cond_1
    const/4 v1, 0x0

    .line 355
    .local v1, "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    move v0, p2

    .line 356
    .local v0, "canUpdate":Z
    if-eqz v0, :cond_2

    .line 357
    const-class v7, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p0, v7}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v6

    .line 358
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .local v2, "pkColumnIndex":J
    move-object v7, p1

    .line 359
    check-cast v7, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v7}, Lio/realm/AlarmRealmProxyInterface;->realmGet$id()J

    move-result-wide v8

    invoke-virtual {v6, v2, v3, v8, v9}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v4

    .line 360
    .local v4, "rowIndex":J
    const-wide/16 v8, -0x1

    cmp-long v7, v4, v8

    if-eqz v7, :cond_3

    .line 361
    new-instance v1, Lio/realm/AlarmRealmProxy;

    .end local v1    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    iget-object v7, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v8, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {v7, v8}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    invoke-direct {v1, v7}, Lio/realm/AlarmRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v1    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    move-object v7, v1

    .line 362
    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v7, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v7, v1

    .line 363
    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v7}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v7

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v8

    invoke-virtual {v7, v8}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    move-object v7, v1

    .line 364
    check-cast v7, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    .line 371
    invoke-static {p0, v1, p1, p3}, Lio/realm/AlarmRealmProxy;->update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;Ljava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    move-result-object p1

    goto :goto_0

    .line 366
    .restart local v2    # "pkColumnIndex":J
    .restart local v4    # "rowIndex":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 373
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_4
    invoke-static {p0, p1, p2, p3}, Lio/realm/AlarmRealmProxy;->copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    move-result-object p1

    goto :goto_0
.end method

.method public static createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .locals 6
    .param p0, "realmObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;"
        }
    .end annotation

    .prologue
    .line 389
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 390
    :cond_0
    const/4 v2, 0x0

    .line 411
    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    :goto_0
    return-object v2

    .line 392
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 394
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 396
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 397
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    goto :goto_0

    .line 399
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .line 400
    .local v1, "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 406
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/AlarmRealmProxyInterface;->realmGet$id()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/AlarmRealmProxyInterface;->realmSet$id(J)V

    move-object v2, v1

    .line 407
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/AlarmRealmProxyInterface;->realmGet$numberOfHours()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfHours(I)V

    move-object v2, v1

    .line 408
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/AlarmRealmProxyInterface;->realmGet$numberOfMinutes()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfMinutes(I)V

    move-object v2, v1

    .line 409
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/AlarmRealmProxyInterface;->realmGet$name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$name(Ljava/lang/String;)V

    move-object v2, v1

    .line 410
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    check-cast p0, Lio/realm/AlarmRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    invoke-interface {p0}, Lio/realm/AlarmRealmProxyInterface;->realmGet$isEnabled()B

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$isEnabled(B)V

    move-object v2, v1

    .line 411
    goto :goto_0

    .line 403
    .end local v1    # "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    :cond_3
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;-><init>()V

    .line 404
    .restart local v1    # "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .locals 11
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 234
    const/4 v0, 0x0

    .line 235
    .local v0, "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    if-eqz p2, :cond_1

    .line 236
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v1

    .line 237
    .local v1, "table":Lio/realm/internal/Table;
    invoke-virtual {v1}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .line 238
    .local v2, "pkColumnIndex":J
    const-wide/16 v4, -0x1

    .line 239
    .local v4, "rowIndex":J
    const-string v6, "id"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 240
    const-string v6, "id"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v1, v2, v3, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v4

    .line 242
    :cond_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_1

    .line 243
    new-instance v0, Lio/realm/AlarmRealmProxy;

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    iget-object v6, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v6

    invoke-direct {v0, v6}, Lio/realm/AlarmRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    move-object v6, v0

    .line 244
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v6, v0

    .line 245
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v1, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v7

    invoke-virtual {v6, v7}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 248
    .end local v1    # "table":Lio/realm/internal/Table;
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    :cond_1
    if-nez v0, :cond_2

    .line 249
    const-string v6, "id"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 250
    const-string v6, "id"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 251
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p0, v6, v10}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    check-cast v0, Lio/realm/AlarmRealmProxy;

    .line 259
    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    :cond_2
    :goto_0
    const-string v6, "id"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 260
    const-string v6, "id"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 261
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field id to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 253
    :cond_3
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    const-string v7, "id"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    check-cast v0, Lio/realm/AlarmRealmProxy;

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    goto :goto_0

    .line 256
    :cond_4
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p0, v6}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    check-cast v0, Lio/realm/AlarmRealmProxy;

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    goto :goto_0

    :cond_5
    move-object v6, v0

    .line 263
    check-cast v6, Lio/realm/AlarmRealmProxyInterface;

    const-string v7, "id"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-interface {v6, v8, v9}, Lio/realm/AlarmRealmProxyInterface;->realmSet$id(J)V

    .line 266
    :cond_6
    const-string v6, "numberOfHours"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 267
    const-string v6, "numberOfHours"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 268
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field numberOfHours to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_7
    move-object v6, v0

    .line 270
    check-cast v6, Lio/realm/AlarmRealmProxyInterface;

    const-string v7, "numberOfHours"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfHours(I)V

    .line 273
    :cond_8
    const-string v6, "numberOfMinutes"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 274
    const-string v6, "numberOfMinutes"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 275
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field numberOfMinutes to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_9
    move-object v6, v0

    .line 277
    check-cast v6, Lio/realm/AlarmRealmProxyInterface;

    const-string v7, "numberOfMinutes"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfMinutes(I)V

    .line 280
    :cond_a
    const-string v6, "name"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 281
    const-string v6, "name"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    move-object v6, v0

    .line 282
    check-cast v6, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v6, v10}, Lio/realm/AlarmRealmProxyInterface;->realmSet$name(Ljava/lang/String;)V

    .line 287
    :cond_b
    :goto_1
    const-string v6, "isEnabled"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 288
    const-string v6, "isEnabled"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 289
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Trying to set non-nullable field isEnabled to null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_c
    move-object v6, v0

    .line 284
    check-cast v6, Lio/realm/AlarmRealmProxyInterface;

    const-string v7, "name"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lio/realm/AlarmRealmProxyInterface;->realmSet$name(Ljava/lang/String;)V

    goto :goto_1

    :cond_d
    move-object v6, v0

    .line 291
    check-cast v6, Lio/realm/AlarmRealmProxyInterface;

    const-string v7, "isEnabled"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    int-to-byte v7, v7

    invoke-interface {v6, v7}, Lio/realm/AlarmRealmProxyInterface;->realmSet$isEnabled(B)V

    .line 294
    :cond_e
    return-object v0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 300
    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p0, v2}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .line 301
    .local v1, "obj":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 302
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 303
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "name":Ljava/lang/String;
    const-string v2, "id"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 305
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_0

    .line 306
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 307
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field id to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move-object v2, v1

    .line 309
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lio/realm/AlarmRealmProxyInterface;->realmSet$id(J)V

    goto :goto_0

    .line 311
    :cond_1
    const-string v2, "numberOfHours"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 312
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_2

    .line 313
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 314
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field numberOfHours to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    move-object v2, v1

    .line 316
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfHours(I)V

    goto :goto_0

    .line 318
    :cond_3
    const-string v2, "numberOfMinutes"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 319
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_4

    .line 320
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 321
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field numberOfMinutes to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    move-object v2, v1

    .line 323
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfMinutes(I)V

    goto :goto_0

    .line 325
    :cond_5
    const-string v2, "name"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 326
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_6

    .line 327
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v2, v1

    .line 328
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$name(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move-object v2, v1

    .line 330
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$name(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 332
    :cond_7
    const-string v2, "isEnabled"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 333
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_8

    .line 334
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 335
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Trying to set non-nullable field isEnabled to null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_8
    move-object v2, v1

    .line 337
    check-cast v2, Lio/realm/AlarmRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v3

    int-to-byte v3, v3

    invoke-interface {v2, v3}, Lio/realm/AlarmRealmProxyInterface;->realmSet$isEnabled(B)V

    goto/16 :goto_0

    .line 340
    :cond_9
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 343
    .end local v0    # "name":Ljava/lang/String;
    :cond_a
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 344
    return-object v1
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    sget-object v0, Lio/realm/AlarmRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    const-string v0, "class_Alarm"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;
    .locals 5
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    const/4 v4, 0x0

    .line 139
    const-string v1, "class_Alarm"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 140
    const-string v1, "class_Alarm"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 141
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "id"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 142
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "numberOfHours"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 143
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "numberOfMinutes"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 144
    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v2, "name"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 145
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "isEnabled"

    invoke-virtual {v0, v1, v2, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 146
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 147
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 150
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "class_Alarm"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method static update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;Ljava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .locals 2
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .param p2, "newObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;"
        }
    .end annotation

    .prologue
    .line 415
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v0, p1

    check-cast v0, Lio/realm/AlarmRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/AlarmRealmProxyInterface;->realmGet$numberOfHours()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfHours(I)V

    move-object v0, p1

    .line 416
    check-cast v0, Lio/realm/AlarmRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/AlarmRealmProxyInterface;->realmGet$numberOfMinutes()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/AlarmRealmProxyInterface;->realmSet$numberOfMinutes(I)V

    move-object v0, p1

    .line 417
    check-cast v0, Lio/realm/AlarmRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/AlarmRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/AlarmRealmProxyInterface;->realmGet$name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/realm/AlarmRealmProxyInterface;->realmSet$name(Ljava/lang/String;)V

    move-object v0, p1

    .line 418
    check-cast v0, Lio/realm/AlarmRealmProxyInterface;

    check-cast p2, Lio/realm/AlarmRealmProxyInterface;

    .end local p2    # "newObject":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    invoke-interface {p2}, Lio/realm/AlarmRealmProxyInterface;->realmGet$isEnabled()B

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/AlarmRealmProxyInterface;->realmSet$isEnabled(B)V

    .line 419
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/AlarmRealmProxy$AlarmColumnInfo;
    .locals 10
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    const-wide/16 v8, 0x5

    .line 154
    const-string v5, "class_Alarm"

    invoke-virtual {p0, v5}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 155
    const-string v5, "class_Alarm"

    invoke-virtual {p0, v5}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v4

    .line 156
    .local v4, "table":Lio/realm/internal/Table;
    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 157
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Field count does not match - expected 5 but was "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 159
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 160
    .local v1, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v2, 0x0

    .local v2, "i":J
    :goto_0
    cmp-long v5, v2, v8

    if-gez v5, :cond_1

    .line 161
    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    goto :goto_0

    .line 164
    :cond_1
    new-instance v0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5, v4}, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 166
    .local v0, "columnInfo":Lio/realm/AlarmRealmProxy$AlarmColumnInfo;
    const-string v5, "id"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 167
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 169
    :cond_2
    const-string v5, "id"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_3

    .line 170
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'long\' for field \'id\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 172
    :cond_3
    iget-wide v6, v0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->idIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-wide v6, v0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->idIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_4

    .line 173
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Cannot migrate an object with null value in field \'id\'. Either maintain the same type for primary key field \'id\', or remove the object with null value before migration."

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 175
    :cond_4
    invoke-virtual {v4}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    const-string v5, "id"

    invoke-virtual {v4, v5}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-eqz v5, :cond_5

    .line 176
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Primary key not defined for field \'id\' in existing Realm file. Add @PrimaryKey."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 178
    :cond_5
    const-string v5, "id"

    invoke-virtual {v4, v5}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v5

    if-nez v5, :cond_6

    .line 179
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 181
    :cond_6
    const-string v5, "numberOfHours"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 182
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'numberOfHours\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 184
    :cond_7
    const-string v5, "numberOfHours"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_8

    .line 185
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'numberOfHours\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 187
    :cond_8
    iget-wide v6, v0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfHoursIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 188
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'numberOfHours\' does support null values in the existing Realm file. Use corresponding boxed type for field \'numberOfHours\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 190
    :cond_9
    const-string v5, "numberOfMinutes"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 191
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'numberOfMinutes\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 193
    :cond_a
    const-string v5, "numberOfMinutes"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_b

    .line 194
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'int\' for field \'numberOfMinutes\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 196
    :cond_b
    iget-wide v6, v0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfMinutesIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 197
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'numberOfMinutes\' does support null values in the existing Realm file. Use corresponding boxed type for field \'numberOfMinutes\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 199
    :cond_c
    const-string v5, "name"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 200
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'name\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 202
    :cond_d
    const-string v5, "name"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_e

    .line 203
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'String\' for field \'name\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 205
    :cond_e
    iget-wide v6, v0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->nameIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-nez v5, :cond_f

    .line 206
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'name\' is required. Either set @Required to field \'name\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 208
    :cond_f
    const-string v5, "isEnabled"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 209
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'isEnabled\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 211
    :cond_10
    const-string v5, "isEnabled"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_11

    .line 212
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'byte\' for field \'isEnabled\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 214
    :cond_11
    iget-wide v6, v0, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->isEnabledIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 215
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Field \'isEnabled\' does support null values in the existing Realm file. Use corresponding boxed type for field \'isEnabled\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 219
    .end local v0    # "columnInfo":Lio/realm/AlarmRealmProxy$AlarmColumnInfo;
    .end local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v2    # "i":J
    .end local v4    # "table":Lio/realm/internal/Table;
    :cond_12
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "The Alarm class is missing from the schema for this Realm."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 217
    .restart local v0    # "columnInfo":Lio/realm/AlarmRealmProxy$AlarmColumnInfo;
    .restart local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v2    # "i":J
    .restart local v4    # "table":Lio/realm/internal/Table;
    :cond_13
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 471
    if-ne p0, p1, :cond_1

    .line 485
    :cond_0
    :goto_0
    return v5

    .line 472
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 473
    check-cast v0, Lio/realm/AlarmRealmProxy;

    .line 475
    .local v0, "aAlarm":Lio/realm/AlarmRealmProxy;
    iget-object v7, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 476
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 477
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 479
    :cond_6
    iget-object v7, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 480
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 481
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 483
    :cond_9
    iget-object v7, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 458
    iget-object v6, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 459
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 460
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 462
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 463
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 464
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 465
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 466
    return v1

    :cond_1
    move v6, v5

    .line 463
    goto :goto_0
.end method

.method public realmGet$id()J
    .locals 4

    .prologue
    .line 81
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 82
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$isEnabled()B
    .locals 4

    .prologue
    .line 129
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 130
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->isEnabledIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-byte v0, v0

    return v0
.end method

.method public realmGet$name()Ljava/lang/String;
    .locals 4

    .prologue
    .line 114
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 115
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->nameIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public realmGet$numberOfHours()I
    .locals 4

    .prologue
    .line 92
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 93
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfHoursIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$numberOfMinutes()I
    .locals 4

    .prologue
    .line 103
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 104
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfMinutesIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmSet$id(J)V
    .locals 5
    .param p1, "value"    # J

    .prologue
    .line 86
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 87
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3, p1, p2}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 88
    return-void
.end method

.method public realmSet$isEnabled(B)V
    .locals 6
    .param p1, "value"    # B

    .prologue
    .line 134
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 135
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->isEnabledIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 136
    return-void
.end method

.method public realmSet$name(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 119
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 120
    if-nez p1, :cond_0

    .line 121
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->nameIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->setNull(J)V

    .line 125
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->nameIndex:J

    invoke-interface {v0, v2, v3, p1}, Lio/realm/internal/Row;->setString(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public realmSet$numberOfHours(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 98
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfHoursIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 99
    return-void
.end method

.method public realmSet$numberOfMinutes(I)V
    .locals 6
    .param p1, "value"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 109
    iget-object v0, p0, Lio/realm/AlarmRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/AlarmRealmProxy;->columnInfo:Lio/realm/AlarmRealmProxy$AlarmColumnInfo;

    iget-wide v2, v1, Lio/realm/AlarmRealmProxy$AlarmColumnInfo;->numberOfMinutesIndex:J

    int-to-long v4, p1

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    .line 110
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 424
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 425
    const-string v1, "Invalid object"

    .line 448
    :goto_0
    return-object v1

    .line 427
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Alarm = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 428
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    invoke-virtual {p0}, Lio/realm/AlarmRealmProxy;->realmGet$id()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 430
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    const-string v1, "{numberOfHours:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    invoke-virtual {p0}, Lio/realm/AlarmRealmProxy;->realmGet$numberOfHours()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 434
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 435
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 436
    const-string v1, "{numberOfMinutes:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    invoke-virtual {p0}, Lio/realm/AlarmRealmProxy;->realmGet$numberOfMinutes()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 438
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 440
    const-string v1, "{name:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    invoke-virtual {p0}, Lio/realm/AlarmRealmProxy;->realmGet$name()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lio/realm/AlarmRealmProxy;->realmGet$name()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    const-string v1, "{isEnabled:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    invoke-virtual {p0}, Lio/realm/AlarmRealmProxy;->realmGet$isEnabled()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 446
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 441
    :cond_1
    const-string v1, "null"

    goto :goto_1
.end method
