.class public Lio/realm/LongObjectRealmProxy;
.super Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
.source "LongObjectRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/LongObjectRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final columnInfo:Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;

.field private final proxyState:Lio/realm/ProxyState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "longValue"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/LongObjectRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 52
    return-void
.end method

.method constructor <init>(Lio/realm/internal/ColumnInfo;)V
    .locals 2
    .param p1, "columnInfo"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;-><init>()V

    .line 55
    check-cast p1, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;

    .end local p1    # "columnInfo":Lio/realm/internal/ColumnInfo;
    iput-object p1, p0, Lio/realm/LongObjectRealmProxy;->columnInfo:Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;

    .line 56
    new-instance v0, Lio/realm/ProxyState;

    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-direct {v0, v1, p0}, Lio/realm/ProxyState;-><init>(Ljava/lang/Class;Lio/realm/RealmModel;)V

    iput-object v0, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 57
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .locals 3
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;"
        }
    .end annotation

    .prologue
    .line 229
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-object v1, p1

    check-cast v1, Lio/realm/LongObjectRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/LongObjectRealmProxyInterface;->realmGet$longValue()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    .local v0, "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    move-object v1, v0

    .line 230
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 231
    check-cast v1, Lio/realm/LongObjectRealmProxyInterface;

    check-cast p1, Lio/realm/LongObjectRealmProxyInterface;

    .end local p1    # "newObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    invoke-interface {p1}, Lio/realm/LongObjectRealmProxyInterface;->realmGet$longValue()Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/realm/LongObjectRealmProxyInterface;->realmSet$longValue(Ljava/lang/Long;)V

    .line 232
    return-object v0
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .locals 12
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;"
        }
    .end annotation

    .prologue
    .line 193
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    instance-of v8, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_0

    move-object v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_0

    move-object v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    iget-wide v8, v8, Lio/realm/BaseRealm;->threadId:J

    iget-wide v10, p0, Lio/realm/Realm;->threadId:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 194
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 196
    :cond_0
    instance-of v8, p1, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 224
    .end local p1    # "object":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    :goto_0
    return-object p1

    .line 199
    .restart local p1    # "object":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    :cond_1
    const/4 v1, 0x0

    .line 200
    .local v1, "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    move v0, p2

    .line 201
    .local v0, "canUpdate":Z
    if-eqz v0, :cond_2

    .line 202
    const-class v8, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v6

    .line 203
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .local v2, "pkColumnIndex":J
    move-object v8, p1

    .line 204
    check-cast v8, Lio/realm/LongObjectRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/LongObjectRealmProxyInterface;->realmGet$longValue()Ljava/lang/Long;

    move-result-object v7

    .line 205
    .local v7, "value":Ljava/lang/Number;
    const-wide/16 v4, -0x1

    .line 206
    .local v4, "rowIndex":J
    if-nez v7, :cond_3

    .line 207
    invoke-virtual {v6, v2, v3}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v4

    .line 211
    :goto_1
    const-wide/16 v8, -0x1

    cmp-long v8, v4, v8

    if-eqz v8, :cond_4

    .line 212
    new-instance v1, Lio/realm/LongObjectRealmProxy;

    .end local v1    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    iget-object v8, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v8

    invoke-direct {v1, v8}, Lio/realm/LongObjectRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v1    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    move-object v8, v1

    .line 213
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v8, v1

    .line 214
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v9

    invoke-virtual {v8, v9}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    move-object v8, v1

    .line 215
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    .end local v7    # "value":Ljava/lang/Number;
    :cond_2
    :goto_2
    if-eqz v0, :cond_5

    .line 222
    invoke-static {p0, v1, p1, p3}, Lio/realm/LongObjectRealmProxy;->update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;Ljava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object p1

    goto :goto_0

    .line 209
    .restart local v2    # "pkColumnIndex":J
    .restart local v4    # "rowIndex":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    .restart local v7    # "value":Ljava/lang/Number;
    :cond_3
    invoke-virtual {v7}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v2, v3, v8, v9}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v4

    goto :goto_1

    .line 217
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 224
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    .end local v6    # "table":Lio/realm/internal/Table;
    .end local v7    # "value":Ljava/lang/Number;
    :cond_5
    invoke-static {p0, p1, p2, p3}, Lio/realm/LongObjectRealmProxy;->copy(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;ZLjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    move-result-object p1

    goto :goto_0
.end method

.method public static createDetachedCopy(Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;IILjava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .locals 4
    .param p0, "realmObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;"
        }
    .end annotation

    .prologue
    .line 236
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 237
    :cond_0
    const/4 v2, 0x0

    .line 254
    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    :goto_0
    return-object v2

    .line 239
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 241
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 243
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 244
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    goto :goto_0

    .line 246
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    .line 247
    .local v1, "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 253
    check-cast v2, Lio/realm/LongObjectRealmProxyInterface;

    check-cast p0, Lio/realm/LongObjectRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    invoke-interface {p0}, Lio/realm/LongObjectRealmProxyInterface;->realmGet$longValue()Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/LongObjectRealmProxyInterface;->realmSet$longValue(Ljava/lang/Long;)V

    move-object v2, v1

    .line 254
    goto :goto_0

    .line 250
    .end local v1    # "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .restart local p0    # "realmObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    :cond_3
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;-><init>()V

    .line 251
    .restart local v1    # "unmanagedObject":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .locals 11
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 133
    const/4 v0, 0x0

    .line 134
    .local v0, "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    if-eqz p2, :cond_0

    .line 135
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v1

    .line 136
    .local v1, "table":Lio/realm/internal/Table;
    invoke-virtual {v1}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .line 137
    .local v2, "pkColumnIndex":J
    const-wide/16 v4, -0x1

    .line 138
    .local v4, "rowIndex":J
    const-string v6, "longValue"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 139
    invoke-virtual {v1, v2, v3}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v4

    .line 143
    :goto_0
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_0

    .line 144
    new-instance v0, Lio/realm/LongObjectRealmProxy;

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    iget-object v6, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v6

    invoke-direct {v0, v6}, Lio/realm/LongObjectRealmProxy;-><init>(Lio/realm/internal/ColumnInfo;)V

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    move-object v6, v0

    .line 145
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6, p0}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    move-object v6, v0

    .line 146
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v1, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v7

    invoke-virtual {v6, v7}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 149
    .end local v1    # "table":Lio/realm/internal/Table;
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    :cond_0
    if-nez v0, :cond_1

    .line 150
    const-string v6, "longValue"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 151
    const-string v6, "longValue"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 152
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p0, v6, v10}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    check-cast v0, Lio/realm/LongObjectRealmProxy;

    .line 160
    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    :cond_1
    :goto_1
    const-string v6, "longValue"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 161
    const-string v6, "longValue"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    move-object v6, v0

    .line 162
    check-cast v6, Lio/realm/LongObjectRealmProxyInterface;

    invoke-interface {v6, v10}, Lio/realm/LongObjectRealmProxyInterface;->realmSet$longValue(Ljava/lang/Long;)V

    .line 167
    :cond_2
    :goto_2
    return-object v0

    .line 141
    .restart local v1    # "table":Lio/realm/internal/Table;
    .restart local v2    # "pkColumnIndex":J
    .restart local v4    # "rowIndex":J
    :cond_3
    const-string v6, "longValue"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v1, v2, v3, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v4

    goto :goto_0

    .line 154
    .end local v1    # "table":Lio/realm/internal/Table;
    .end local v2    # "pkColumnIndex":J
    .end local v4    # "rowIndex":J
    :cond_4
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    const-string v7, "longValue"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lio/realm/Realm;->createObject(Ljava/lang/Class;Ljava/lang/Object;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    check-cast v0, Lio/realm/LongObjectRealmProxy;

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    goto :goto_1

    .line 157
    :cond_5
    const-class v6, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p0, v6}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v0

    .end local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    check-cast v0, Lio/realm/LongObjectRealmProxy;

    .restart local v0    # "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    goto :goto_1

    :cond_6
    move-object v6, v0

    .line 164
    check-cast v6, Lio/realm/LongObjectRealmProxyInterface;

    const-string v7, "longValue"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Lio/realm/LongObjectRealmProxyInterface;->realmSet$longValue(Ljava/lang/Long;)V

    goto :goto_2
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {p0, v2}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    .line 174
    .local v1, "obj":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 175
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 176
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "name":Ljava/lang/String;
    const-string v2, "longValue"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 178
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v2

    sget-object v3, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v2, v3, :cond_0

    .line 179
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v2, v1

    .line 180
    check-cast v2, Lio/realm/LongObjectRealmProxyInterface;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lio/realm/LongObjectRealmProxyInterface;->realmSet$longValue(Ljava/lang/Long;)V

    goto :goto_0

    :cond_0
    move-object v2, v1

    .line 182
    check-cast v2, Lio/realm/LongObjectRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/LongObjectRealmProxyInterface;->realmSet$longValue(Ljava/lang/Long;)V

    goto :goto_0

    .line 185
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 188
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 189
    return-object v1
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    sget-object v0, Lio/realm/LongObjectRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    const-string v0, "class_LongObject"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    .line 78
    const-string v1, "class_LongObject"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 79
    const-string v1, "class_LongObject"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 80
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "longValue"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 81
    const-string v1, "longValue"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 82
    const-string v1, "longValue"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 85
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "class_LongObject"

    invoke-virtual {p0, v1}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method static update(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;Ljava/util/Map;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .locals 0
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .param p2, "newObject"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;"
        }
    .end annotation

    .prologue
    .line 258
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/ImplicitTransaction;)Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;
    .locals 10
    .param p0, "transaction"    # Lio/realm/internal/ImplicitTransaction;

    .prologue
    const-wide/16 v8, 0x1

    .line 89
    const-string v5, "class_LongObject"

    invoke-virtual {p0, v5}, Lio/realm/internal/ImplicitTransaction;->hasTable(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 90
    const-string v5, "class_LongObject"

    invoke-virtual {p0, v5}, Lio/realm/internal/ImplicitTransaction;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v4

    .line 91
    .local v4, "table":Lio/realm/internal/Table;
    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 92
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Field count does not match - expected 1 but was "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 94
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 95
    .local v1, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v2, 0x0

    .local v2, "i":J
    :goto_0
    cmp-long v5, v2, v8

    if-gez v5, :cond_1

    .line 96
    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v3}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    add-long/2addr v2, v8

    goto :goto_0

    .line 99
    :cond_1
    new-instance v0, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5, v4}, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 101
    .local v0, "columnInfo":Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;
    const-string v5, "longValue"

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 102
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Missing field \'longValue\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 104
    :cond_2
    const-string v5, "longValue"

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v5, v6, :cond_3

    .line 105
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Invalid type \'Long\' for field \'longValue\' in existing Realm file."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 107
    :cond_3
    iget-wide v6, v0, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;->longValueIndex:J

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v5

    if-nez v5, :cond_4

    .line 108
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "@PrimaryKey field \'longValue\' does not support null values in the existing Realm file. Migrate using RealmObjectSchema.setNullable(), or mark the field as @Required."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 110
    :cond_4
    invoke-virtual {v4}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    const-string v5, "longValue"

    invoke-virtual {v4, v5}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-eqz v5, :cond_5

    .line 111
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Primary key not defined for field \'longValue\' in existing Realm file. Add @PrimaryKey."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 113
    :cond_5
    const-string v5, "longValue"

    invoke-virtual {v4, v5}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v5

    if-nez v5, :cond_7

    .line 114
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Index not defined for field \'longValue\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 118
    .end local v0    # "columnInfo":Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;
    .end local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v2    # "i":J
    .end local v4    # "table":Lio/realm/internal/Table;
    :cond_6
    new-instance v5, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/ImplicitTransaction;->getPath()Ljava/lang/String;

    move-result-object v6

    const-string v7, "The LongObject class is missing from the schema for this Realm."

    invoke-direct {v5, v6, v7}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v5

    .line 116
    .restart local v0    # "columnInfo":Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;
    .restart local v1    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v2    # "i":J
    .restart local v4    # "table":Lio/realm/internal/Table;
    :cond_7
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 294
    if-ne p0, p1, :cond_1

    .line 308
    :cond_0
    :goto_0
    return v5

    .line 295
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 296
    check-cast v0, Lio/realm/LongObjectRealmProxy;

    .line 298
    .local v0, "aLongObject":Lio/realm/LongObjectRealmProxy;
    iget-object v7, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 299
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 300
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 302
    :cond_6
    iget-object v7, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 303
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 304
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 306
    :cond_9
    iget-object v7, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 281
    iget-object v6, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 282
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 283
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 285
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 286
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 287
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 288
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 289
    return v1

    :cond_1
    move v6, v5

    .line 286
    goto :goto_0
.end method

.method public realmGet$longValue()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 62
    iget-object v0, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/LongObjectRealmProxy;->columnInfo:Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;

    iget-wide v2, v1, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;->longValueIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->isNull(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/LongObjectRealmProxy;->columnInfo:Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;

    iget-wide v2, v1, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;->longValueIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmSet$longValue(Ljava/lang/Long;)V
    .locals 6
    .param p1, "value"    # Ljava/lang/Long;

    .prologue
    .line 69
    iget-object v0, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 70
    if-nez p1, :cond_0

    .line 71
    iget-object v0, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/LongObjectRealmProxy;->columnInfo:Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;

    iget-wide v2, v1, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;->longValueIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->setNull(J)V

    .line 75
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lio/realm/LongObjectRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/LongObjectRealmProxy;->columnInfo:Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;

    iget-wide v2, v1, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;->longValueIndex:J

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v0, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 263
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 264
    const-string v1, "Invalid object"

    .line 271
    :goto_0
    return-object v1

    .line 266
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LongObject = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 267
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{longValue:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    invoke-virtual {p0}, Lio/realm/LongObjectRealmProxy;->realmGet$longValue()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lio/realm/LongObjectRealmProxy;->realmGet$longValue()Ljava/lang/Long;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 269
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 268
    :cond_1
    const-string v1, "null"

    goto :goto_1
.end method
