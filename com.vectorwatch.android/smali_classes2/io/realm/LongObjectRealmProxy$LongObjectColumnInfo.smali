.class final Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "LongObjectRealmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/LongObjectRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "LongObjectColumnInfo"
.end annotation


# instance fields
.field public final longValueIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 36
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 38
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "LongObject"

    const-string v2, "longValue"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;->longValueIndex:J

    .line 39
    const-string v1, "longValue"

    iget-wide v2, p0, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;->longValueIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    invoke-virtual {p0, v0}, Lio/realm/LongObjectRealmProxy$LongObjectColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 42
    return-void
.end method
