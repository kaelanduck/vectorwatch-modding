.class public interface abstract Lio/realm/AlarmRealmProxyInterface;
.super Ljava/lang/Object;
.source "AlarmRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$id()J
.end method

.method public abstract realmGet$isEnabled()B
.end method

.method public abstract realmGet$name()Ljava/lang/String;
.end method

.method public abstract realmGet$numberOfHours()I
.end method

.method public abstract realmGet$numberOfMinutes()I
.end method

.method public abstract realmSet$id(J)V
.end method

.method public abstract realmSet$isEnabled(B)V
.end method

.method public abstract realmSet$name(Ljava/lang/String;)V
.end method

.method public abstract realmSet$numberOfHours(I)V
.end method

.method public abstract realmSet$numberOfMinutes(I)V
.end method
