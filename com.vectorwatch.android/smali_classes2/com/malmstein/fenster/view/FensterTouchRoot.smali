.class public final Lcom/malmstein/fenster/view/FensterTouchRoot;
.super Landroid/widget/FrameLayout;
.source "FensterTouchRoot.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;
    }
.end annotation


# static fields
.field public static final MIN_INTERCEPTION_TIME:I = 0x3e8


# instance fields
.field private lastInterception:J

.field private touchReceiver:Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 33
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterTouchRoot;->touchReceiver:Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;

    if-eqz v2, :cond_0

    .line 34
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 36
    .local v0, "timeStamp":J
    iget-wide v2, p0, Lcom/malmstein/fenster/view/FensterTouchRoot;->lastInterception:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 37
    iput-wide v0, p0, Lcom/malmstein/fenster/view/FensterTouchRoot;->lastInterception:J

    .line 38
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterTouchRoot;->touchReceiver:Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;

    invoke-interface {v2}, Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;->onControllerUiTouched()V

    .line 41
    .end local v0    # "timeStamp":J
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2
.end method

.method public setOnTouchReceiver(Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;)V
    .locals 0
    .param p1, "receiver"    # Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterTouchRoot;->touchReceiver:Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;

    .line 46
    return-void
.end method
