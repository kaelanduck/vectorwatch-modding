.class final Lcom/malmstein/fenster/view/FensterVideoView$7;
.super Ljava/lang/Object;
.source "FensterVideoView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/malmstein/fenster/view/FensterVideoView;->createErrorDialog(Landroid/content/Context;Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer;I)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field final synthetic val$mediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method constructor <init>(Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 414
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView$7;->val$completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    iput-object p2, p0, Lcom/malmstein/fenster/view/FensterVideoView$7;->val$mediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 419
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$7;->val$completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$7;->val$completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$7;->val$mediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 422
    :cond_0
    return-void
.end method
