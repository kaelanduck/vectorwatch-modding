.class Lcom/malmstein/fenster/view/FensterVideoView$3;
.super Ljava/lang/Object;
.source "FensterVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/view/FensterVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/view/FensterVideoView;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/view/FensterVideoView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v3, 0x1

    .line 283
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->access$102(Lcom/malmstein/fenster/view/FensterVideoView;I)I

    .line 285
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->access$202(Lcom/malmstein/fenster/view/FensterVideoView;Z)Z

    .line 286
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->access$302(Lcom/malmstein/fenster/view/FensterVideoView;Z)Z

    .line 287
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->access$402(Lcom/malmstein/fenster/view/FensterVideoView;Z)Z

    .line 289
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$500(Lcom/malmstein/fenster/view/FensterVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 290
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$500(Lcom/malmstein/fenster/view/FensterVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v1

    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v2}, Lcom/malmstein/fenster/view/FensterVideoView;->access$600(Lcom/malmstein/fenster/view/FensterVideoView;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 292
    :cond_0
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$700(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/controller/FensterPlayerController;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 293
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$700(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/controller/FensterPlayerController;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/malmstein/fenster/controller/FensterPlayerController;->setEnabled(Z)V

    .line 295
    :cond_1
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$000(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/view/VideoSizeCalculator;

    move-result-object v1

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/malmstein/fenster/view/VideoSizeCalculator;->setVideoSize(II)V

    .line 297
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$800(Lcom/malmstein/fenster/view/FensterVideoView;)I

    move-result v0

    .line 298
    .local v0, "seekToPosition":I
    if-eqz v0, :cond_2

    .line 299
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v1, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    .line 302
    :cond_2
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$900(Lcom/malmstein/fenster/view/FensterVideoView;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    .line 303
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->start()V

    .line 304
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$1000(Lcom/malmstein/fenster/view/FensterVideoView;)V

    .line 308
    :cond_3
    :goto_0
    return-void

    .line 305
    :cond_4
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$1100(Lcom/malmstein/fenster/view/FensterVideoView;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 306
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView$3;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$1200(Lcom/malmstein/fenster/view/FensterVideoView;)V

    goto :goto_0
.end method
