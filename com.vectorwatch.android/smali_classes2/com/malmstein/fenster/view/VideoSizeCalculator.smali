.class public Lcom/malmstein/fenster/view/VideoSizeCalculator;
.super Ljava/lang/Object;
.source "VideoSizeCalculator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;
    }
.end annotation


# instance fields
.field private dimens:Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;

.field private mVideoHeight:I

.field private mVideoWidth:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;

    invoke-direct {v0}, Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;-><init>()V

    iput-object v0, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->dimens:Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;

    .line 15
    return-void
.end method


# virtual methods
.method public currentSizeIs(II)Z
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 85
    iget v0, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasASizeYet()Z
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected measure(II)Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    .line 27
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    invoke-static {v6, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v3

    .line 28
    .local v3, "width":I
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    invoke-static {v6, p2}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 29
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/VideoSizeCalculator;->hasASizeYet()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 31
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 32
    .local v4, "widthSpecMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 33
    .local v5, "widthSpecSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 34
    .local v1, "heightSpecMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 36
    .local v2, "heightSpecSize":I
    if-ne v4, v7, :cond_2

    if-ne v1, v7, :cond_2

    .line 38
    move v3, v5

    .line 39
    move v0, v2

    .line 42
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    mul-int/2addr v6, v0

    iget v7, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    mul-int/2addr v7, v3

    if-ge v6, v7, :cond_1

    .line 43
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    mul-int/2addr v6, v0

    iget v7, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    div-int v3, v6, v7

    .line 79
    .end local v1    # "heightSpecMode":I
    .end local v2    # "heightSpecSize":I
    .end local v4    # "widthSpecMode":I
    .end local v5    # "widthSpecSize":I
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->dimens:Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;

    iput v3, v6, Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;->width:I

    .line 80
    iget-object v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->dimens:Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;

    iput v0, v6, Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;->height:I

    .line 81
    iget-object v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->dimens:Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;

    return-object v6

    .line 44
    .restart local v1    # "heightSpecMode":I
    .restart local v2    # "heightSpecSize":I
    .restart local v4    # "widthSpecMode":I
    .restart local v5    # "widthSpecSize":I
    :cond_1
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    mul-int/2addr v6, v0

    iget v7, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    mul-int/2addr v7, v3

    if-le v6, v7, :cond_0

    .line 45
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    mul-int/2addr v6, v3

    iget v7, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    div-int v0, v6, v7

    goto :goto_0

    .line 47
    :cond_2
    if-ne v4, v7, :cond_3

    .line 49
    move v3, v5

    .line 50
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    mul-int/2addr v6, v3

    iget v7, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    div-int v0, v6, v7

    .line 51
    if-ne v1, v8, :cond_0

    if-le v0, v2, :cond_0

    .line 53
    move v0, v2

    goto :goto_0

    .line 55
    :cond_3
    if-ne v1, v7, :cond_4

    .line 57
    move v0, v2

    .line 58
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    mul-int/2addr v6, v0

    iget v7, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    div-int v3, v6, v7

    .line 59
    if-ne v4, v8, :cond_0

    if-le v3, v5, :cond_0

    .line 61
    move v3, v5

    goto :goto_0

    .line 65
    :cond_4
    iget v3, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    .line 66
    iget v0, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    .line 67
    if-ne v1, v8, :cond_5

    if-le v0, v2, :cond_5

    .line 69
    move v0, v2

    .line 70
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    mul-int/2addr v6, v0

    iget v7, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    div-int v3, v6, v7

    .line 72
    :cond_5
    if-ne v4, v8, :cond_0

    if-le v3, v5, :cond_0

    .line 74
    move v3, v5

    .line 75
    iget v6, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    mul-int/2addr v6, v3

    iget v7, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    div-int v0, v6, v7

    goto :goto_0
.end method

.method public setVideoSize(II)V
    .locals 0
    .param p1, "mVideoWidth"    # I
    .param p2, "mVideoHeight"    # I

    .prologue
    .line 18
    iput p1, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    .line 19
    iput p2, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    .line 20
    return-void
.end method

.method public updateHolder(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 89
    iget v0, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoWidth:I

    iget v1, p0, Lcom/malmstein/fenster/view/VideoSizeCalculator;->mVideoHeight:I

    invoke-interface {p1, v0, v1}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 90
    return-void
.end method
