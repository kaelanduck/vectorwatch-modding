.class Lcom/malmstein/fenster/view/FensterVideoView$11;
.super Ljava/lang/Object;
.source "FensterVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/view/FensterVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/view/FensterVideoView;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/view/FensterVideoView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 714
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView$11;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v1, 0x0

    .line 718
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$11;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2300(Lcom/malmstein/fenster/view/FensterVideoView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 733
    :cond_0
    :goto_0
    return v1

    .line 722
    :cond_1
    const/4 v0, 0x3

    if-ne v0, p2, :cond_2

    .line 723
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$11;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2400(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/play/FensterVideoStateListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterVideoStateListener;->onFirstVideoFrameRendered()V

    .line 724
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$11;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2400(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/play/FensterVideoStateListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterVideoStateListener;->onPlay()V

    .line 726
    :cond_2
    const/16 v0, 0x2bd

    if-ne v0, p2, :cond_3

    .line 727
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$11;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2400(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/play/FensterVideoStateListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterVideoStateListener;->onBuffer()V

    .line 729
    :cond_3
    const/16 v0, 0x2be

    if-ne v0, p2, :cond_0

    .line 730
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$11;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2400(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/play/FensterVideoStateListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterVideoStateListener;->onPlay()V

    goto :goto_0
.end method
