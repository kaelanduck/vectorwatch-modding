.class Lcom/malmstein/fenster/view/FensterVideoView$2;
.super Ljava/lang/Object;
.source "FensterVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/view/FensterVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/view/FensterVideoView;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/view/FensterVideoView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView$2;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 273
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$2;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$000(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/view/VideoSizeCalculator;

    move-result-object v0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/malmstein/fenster/view/VideoSizeCalculator;->setVideoSize(II)V

    .line 274
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$2;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$000(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/view/VideoSizeCalculator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/VideoSizeCalculator;->hasASizeYet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$2;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->requestLayout()V

    .line 277
    :cond_0
    return-void
.end method
