.class Lcom/malmstein/fenster/view/FensterVideoView$6;
.super Ljava/lang/Object;
.source "FensterVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/view/FensterVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/view/FensterVideoView;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/view/FensterVideoView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 345
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView$6;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "frameworkError"    # I
    .param p3, "implError"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 348
    const-string v0, "TextureVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$6;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$100(Lcom/malmstein/fenster/view/FensterVideoView;)I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 365
    :cond_0
    :goto_0
    return v3

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$6;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0, v4}, Lcom/malmstein/fenster/view/FensterVideoView;->access$102(Lcom/malmstein/fenster/view/FensterVideoView;I)I

    .line 353
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$6;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0, v4}, Lcom/malmstein/fenster/view/FensterVideoView;->access$902(Lcom/malmstein/fenster/view/FensterVideoView;I)I

    .line 354
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$6;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$1300(Lcom/malmstein/fenster/view/FensterVideoView;)V

    .line 356
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$6;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0, p2}, Lcom/malmstein/fenster/view/FensterVideoView;->access$1600(Lcom/malmstein/fenster/view/FensterVideoView;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$6;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0, p2, p3}, Lcom/malmstein/fenster/view/FensterVideoView;->access$1700(Lcom/malmstein/fenster/view/FensterVideoView;II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$6;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0, p2}, Lcom/malmstein/fenster/view/FensterVideoView;->access$1800(Lcom/malmstein/fenster/view/FensterVideoView;I)V

    goto :goto_0
.end method
