.class Lcom/malmstein/fenster/view/FensterVideoView$9;
.super Ljava/lang/Object;
.source "FensterVideoView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/view/FensterVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/view/FensterVideoView;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/view/FensterVideoView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 500
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 503
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0, p1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2002(Lcom/malmstein/fenster/view/FensterVideoView;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;

    .line 504
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2100(Lcom/malmstein/fenster/view/FensterVideoView;)V

    .line 505
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 521
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2002(Lcom/malmstein/fenster/view/FensterVideoView;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;

    .line 522
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->access$1300(Lcom/malmstein/fenster/view/FensterVideoView;)V

    .line 523
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2200(Lcom/malmstein/fenster/view/FensterVideoView;Z)V

    .line 524
    const/4 v0, 0x0

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 4
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 509
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v2}, Lcom/malmstein/fenster/view/FensterVideoView;->access$900(Lcom/malmstein/fenster/view/FensterVideoView;)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    const/4 v1, 0x1

    .line 510
    .local v1, "isValidState":Z
    :goto_0
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v2}, Lcom/malmstein/fenster/view/FensterVideoView;->access$000(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/view/VideoSizeCalculator;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/malmstein/fenster/view/VideoSizeCalculator;->currentSizeIs(II)Z

    move-result v0

    .line 511
    .local v0, "hasValidSize":Z
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v2}, Lcom/malmstein/fenster/view/FensterVideoView;->access$600(Lcom/malmstein/fenster/view/FensterVideoView;)Landroid/media/MediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 512
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v2}, Lcom/malmstein/fenster/view/FensterVideoView;->access$800(Lcom/malmstein/fenster/view/FensterVideoView;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 513
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v3, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v3}, Lcom/malmstein/fenster/view/FensterVideoView;->access$800(Lcom/malmstein/fenster/view/FensterVideoView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    .line 515
    :cond_0
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v2}, Lcom/malmstein/fenster/view/FensterVideoView;->start()V

    .line 517
    :cond_1
    return-void

    .line 509
    .end local v0    # "hasValidSize":Z
    .end local v1    # "isValidState":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 529
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView$9;->this$0:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-static {v0, p1}, Lcom/malmstein/fenster/view/FensterVideoView;->access$2002(Lcom/malmstein/fenster/view/FensterVideoView;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;

    .line 530
    return-void
.end method
