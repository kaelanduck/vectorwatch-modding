.class public Lcom/malmstein/fenster/view/FensterLoadingView;
.super Landroid/widget/FrameLayout;
.source "FensterLoadingView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 30
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/view/FensterLoadingView;->setVisibility(I)V

    .line 31
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterLoadingView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/malmstein/fenster/R$layout;->view_loading:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 23
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/view/FensterLoadingView;->setVisibility(I)V

    .line 27
    return-void
.end method
