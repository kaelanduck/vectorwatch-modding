.class public interface abstract Lcom/malmstein/fenster/controller/FensterPlayerController;
.super Ljava/lang/Object;
.source "FensterPlayerController.java"


# virtual methods
.method public abstract hide()V
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract setMediaPlayer(Lcom/malmstein/fenster/play/FensterPlayer;)V
.end method

.method public abstract setVisibilityListener(Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;)V
.end method

.method public abstract show()V
.end method

.method public abstract show(I)V
.end method
