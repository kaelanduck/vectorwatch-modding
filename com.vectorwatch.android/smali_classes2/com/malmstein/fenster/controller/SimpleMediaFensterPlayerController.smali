.class public final Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;
.super Landroid/widget/FrameLayout;
.source "SimpleMediaFensterPlayerController.java"

# interfaces
.implements Lcom/malmstein/fenster/controller/FensterPlayerController;
.implements Lcom/malmstein/fenster/play/FensterVideoStateListener;
.implements Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;


# static fields
.field private static final DEFAULT_TIMEOUT:I = 0x1388

.field public static final DEFAULT_VIDEO_START:I = 0x0

.field private static final FADE_OUT:I = 0x1

.field private static final SHOW_PROGRESS:I = 0x2

.field public static final TAG:Ljava/lang/String; = "PlayerController"


# instance fields
.field private bottomControlsRoot:Landroid/view/View;

.field private controlsRoot:Landroid/view/View;

.field private lastPlayedSeconds:I

.field private loadingView:Landroid/widget/ProgressBar;

.field private mCurrentTime:Landroid/widget/TextView;

.field private mDragging:Z

.field private mEndTime:Landroid/widget/TextView;

.field private mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

.field private mFirstTimeLoading:Z

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private final mHandler:Landroid/os/Handler;

.field private mLoading:Z

.field private mNextButton:Landroid/widget/ImageButton;

.field private mPauseButton:Landroid/widget/ImageButton;

.field private final mPauseListener:Landroid/view/View$OnClickListener;

.field private mPrevButton:Landroid/widget/ImageButton;

.field private mProgress:Landroid/widget/ProgressBar;

.field private final mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mShowing:Z

.field private visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFirstTimeLoading:Z

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->lastPlayedSeconds:I

    .line 401
    new-instance v0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;-><init>(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)V

    iput-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 443
    new-instance v0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;-><init>(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)V

    iput-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    .line 469
    new-instance v0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$3;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$3;-><init>(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)V

    iput-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseListener:Landroid/view/View$OnClickListener;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mDragging:Z

    return v0
.end method

.method static synthetic access$002(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mDragging:Z

    return p1
.end method

.method static synthetic access$100(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    return-object v0
.end method

.method static synthetic access$300(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;
    .param p1, "x1"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->stringForTime(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)I
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->setProgress()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->updatePausePlay()V

    return-void
.end method

.method static synthetic access$700(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mShowing:Z

    return v0
.end method

.method static synthetic access$800(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->doPauseResume()V

    return-void
.end method

.method private doPauseResume()V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterPlayer;->pause()V

    .line 322
    :goto_0
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->updatePausePlay()V

    .line 323
    return-void

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterPlayer;->start()V

    goto :goto_0
.end method

.method private hideLoadingView()V
    .locals 2

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->hide()V

    .line 380
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->loadingView:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 382
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mLoading:Z

    .line 383
    return-void
.end method

.method private initControllerView()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    .line 98
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_pause:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    .line 99
    iget-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 100
    iget-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_next:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mNextButton:Landroid/widget/ImageButton;

    .line 103
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_previous:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPrevButton:Landroid/widget/ImageButton;

    .line 105
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_progress:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mProgress:Landroid/widget/ProgressBar;

    .line 106
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mProgress:Landroid/widget/ProgressBar;

    check-cast v0, Landroid/widget/SeekBar;

    .line 107
    .local v0, "seeker":Landroid/widget/SeekBar;
    iget-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 108
    iget-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mProgress:Landroid/widget/ProgressBar;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 110
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_time:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mEndTime:Landroid/widget/TextView;

    .line 111
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_time_current:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;

    .line 112
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFormatBuilder:Ljava/lang/StringBuilder;

    .line 113
    new-instance v2, Ljava/util/Formatter;

    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFormatter:Ljava/util/Formatter;

    .line 115
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_touch_root:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/malmstein/fenster/view/FensterTouchRoot;

    .line 116
    .local v1, "touchRoot":Lcom/malmstein/fenster/view/FensterTouchRoot;
    invoke-virtual {v1, p0}, Lcom/malmstein/fenster/view/FensterTouchRoot;->setOnTouchReceiver(Lcom/malmstein/fenster/view/FensterTouchRoot$OnTouchReceiver;)V

    .line 118
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_bottom_area:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->bottomControlsRoot:Landroid/view/View;

    .line 119
    iget-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->bottomControlsRoot:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 121
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_controls_root:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->controlsRoot:Landroid/view/View;

    .line 122
    iget-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->controlsRoot:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 124
    sget v2, Lcom/malmstein/fenster/R$id;->media_controller_loading_view:I

    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->loadingView:Landroid/widget/ProgressBar;

    .line 125
    return-void
.end method

.method private setProgress()I
    .locals 10

    .prologue
    .line 222
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    if-eqz v6, :cond_0

    iget-boolean v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mDragging:Z

    if-eqz v6, :cond_2

    .line 223
    :cond_0
    const/4 v3, 0x0

    .line 247
    :cond_1
    :goto_0
    return v3

    .line 225
    :cond_2
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v6}, Lcom/malmstein/fenster/play/FensterPlayer;->getCurrentPosition()I

    move-result v3

    .line 226
    .local v3, "position":I
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v6}, Lcom/malmstein/fenster/play/FensterPlayer;->getDuration()I

    move-result v0

    .line 227
    .local v0, "duration":I
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mProgress:Landroid/widget/ProgressBar;

    if-eqz v6, :cond_4

    .line 228
    if-lez v0, :cond_3

    .line 230
    const-wide/16 v6, 0x3e8

    int-to-long v8, v3

    mul-long/2addr v6, v8

    int-to-long v8, v0

    div-long v4, v6, v8

    .line 231
    .local v4, "pos":J
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mProgress:Landroid/widget/ProgressBar;

    long-to-int v7, v4

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 233
    .end local v4    # "pos":J
    :cond_3
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v6}, Lcom/malmstein/fenster/play/FensterPlayer;->getBufferPercentage()I

    move-result v1

    .line 234
    .local v1, "percent":I
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mProgress:Landroid/widget/ProgressBar;

    mul-int/lit8 v7, v1, 0xa

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 237
    .end local v1    # "percent":I
    :cond_4
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mEndTime:Landroid/widget/TextView;

    if-eqz v6, :cond_5

    .line 238
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->stringForTime(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    :cond_5
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v6, :cond_6

    .line 241
    iget-object v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->stringForTime(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    :cond_6
    div-int/lit16 v2, v3, 0x3e8

    .line 244
    .local v2, "playedSeconds":I
    iget v6, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->lastPlayedSeconds:I

    if-eq v6, v2, :cond_1

    .line 245
    iput v2, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->lastPlayedSeconds:I

    goto :goto_0
.end method

.method private showLoadingView()V
    .locals 2

    .prologue
    .line 386
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mLoading:Z

    .line 387
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->loadingView:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 388
    return-void
.end method

.method private stringForTime(I)Ljava/lang/String;
    .locals 11
    .param p1, "timeMs"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 207
    div-int/lit16 v3, p1, 0x3e8

    .line 209
    .local v3, "totalSeconds":I
    rem-int/lit8 v2, v3, 0x3c

    .line 210
    .local v2, "seconds":I
    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    .line 211
    .local v1, "minutes":I
    div-int/lit16 v0, v3, 0xe10

    .line 213
    .local v0, "hours":I
    iget-object v4, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 214
    if-lez v0, :cond_0

    .line 215
    iget-object v4, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFormatter:Ljava/util/Formatter;

    const-string v5, "%d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    .line 217
    :goto_0
    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFormatter:Ljava/util/Formatter;

    const-string v5, "%02d:%02d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private updatePausePlay()V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 314
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    const v1, 0x1080023

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    const v1, 0x1080024

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v4, 0x1388

    const/4 v2, 0x1

    .line 258
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 259
    .local v0, "keyCode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_2

    .line 260
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 261
    .local v1, "uniqueDown":Z
    :goto_0
    const/16 v3, 0x4f

    if-eq v0, v3, :cond_0

    const/16 v3, 0x55

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3e

    if-ne v0, v3, :cond_3

    .line 264
    :cond_0
    if-eqz v1, :cond_1

    .line 265
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->doPauseResume()V

    .line 266
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->show(I)V

    .line 267
    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_1

    .line 268
    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 301
    :cond_1
    :goto_1
    return v2

    .line 260
    .end local v1    # "uniqueDown":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 272
    .restart local v1    # "uniqueDown":Z
    :cond_3
    const/16 v3, 0x7e

    if-ne v0, v3, :cond_4

    .line 273
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v3

    if-nez v3, :cond_1

    .line 274
    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->start()V

    .line 275
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->updatePausePlay()V

    .line 276
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->show(I)V

    goto :goto_1

    .line 279
    :cond_4
    const/16 v3, 0x56

    if-eq v0, v3, :cond_5

    const/16 v3, 0x7f

    if-ne v0, v3, :cond_6

    .line 281
    :cond_5
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 282
    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->pause()V

    .line 283
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->updatePausePlay()V

    .line 284
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->show(I)V

    goto :goto_1

    .line 287
    :cond_6
    const/16 v3, 0x19

    if-eq v0, v3, :cond_7

    const/16 v3, 0x18

    if-eq v0, v3, :cond_7

    const/16 v3, 0xa4

    if-eq v0, v3, :cond_7

    const/16 v3, 0x1b

    if-ne v0, v3, :cond_8

    .line 292
    :cond_7
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_1

    .line 293
    :cond_8
    const/4 v3, 0x4

    if-eq v0, v3, :cond_9

    const/16 v3, 0x52

    if-ne v0, v3, :cond_a

    .line 294
    :cond_9
    if-eqz v1, :cond_1

    .line 295
    invoke-virtual {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->hide()V

    goto :goto_1

    .line 300
    :cond_a
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->show(I)V

    .line 301
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_1
.end method

.method public hide()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 192
    iget-boolean v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mShowing:Z

    if-eqz v1, :cond_0

    .line 194
    :try_start_0
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 195
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    iput-boolean v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mShowing:Z

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    if-eqz v1, :cond_1

    .line 202
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    invoke-interface {v1, v3}, Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;->onControlsVisibilityChange(Z)V

    .line 204
    :cond_1
    return-void

    .line 196
    :catch_0
    move-exception v0

    .line 197
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v1, "MediaController"

    const-string v2, "already removed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isFirstTimeLoading()Z
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFirstTimeLoading:Z

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mLoading:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mShowing:Z

    return v0
.end method

.method public onBuffer()V
    .locals 0

    .prologue
    .line 370
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->showLoadingView()V

    .line 371
    return-void
.end method

.method public onControllerUiTouched()V
    .locals 2

    .prologue
    .line 482
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mShowing:Z

    if-eqz v0, :cond_0

    .line 483
    const-string v0, "PlayerController"

    const-string v1, "controller ui touch received!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    invoke-virtual {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->show()V

    .line 486
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/malmstein/fenster/R$layout;->view_simple_media_controller:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 83
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->initControllerView()V

    .line 84
    return-void
.end method

.method public onFirstVideoFrameRendered()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 358
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->controlsRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->bottomControlsRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 360
    iput-boolean v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFirstTimeLoading:Z

    .line 361
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 346
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 347
    const-class v0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 348
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 352
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 353
    const-class v0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 354
    return-void
.end method

.method public onPlay()V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->hideLoadingView()V

    .line 366
    return-void
.end method

.method public onStopWithExternalError(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 375
    const/4 v0, 0x0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 252
    const/16 v0, 0x1388

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->show(I)V

    .line 253
    const/4 v0, 0x0

    return v0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 328
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mNextButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPrevButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 336
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPrevButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 338
    :cond_2
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mProgress:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_3

    .line 339
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setEnabled(Z)V

    .line 341
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 342
    return-void
.end method

.method public setMediaPlayer(Lcom/malmstein/fenster/play/FensterPlayer;)V
    .locals 0
    .param p1, "fensterPlayer"    # Lcom/malmstein/fenster/play/FensterPlayer;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    .line 89
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->updatePausePlay()V

    .line 90
    return-void
.end method

.method public setVisibilityListener(Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;)V
    .locals 0
    .param p1, "visibilityListener"    # Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    .line 95
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 134
    const/16 v0, 0x1388

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->show(I)V

    .line 135
    return-void
.end method

.method public show(I)V
    .locals 5
    .param p1, "timeInMilliSeconds"    # I

    .prologue
    const/4 v4, 0x1

    .line 146
    iget-boolean v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mShowing:Z

    if-nez v1, :cond_1

    .line 147
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->setProgress()I

    .line 148
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 149
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 151
    :cond_0
    iput-boolean v4, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mShowing:Z

    .line 152
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->setVisibility(I)V

    .line 155
    :cond_1
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->updatePausePlay()V

    .line 160
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 162
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 163
    .local v0, "msg":Landroid/os/Message;
    if-eqz p1, :cond_2

    .line 164
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 165
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 168
    :cond_2
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    if-eqz v1, :cond_3

    .line 169
    iget-object v1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    invoke-interface {v1, v4}, Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;->onControlsVisibilityChange(Z)V

    .line 172
    :cond_3
    return-void
.end method
