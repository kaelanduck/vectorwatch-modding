.class Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;
.super Landroid/os/Handler;
.source "SimpleMediaFensterPlayerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 443
    iput-object p1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 447
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 449
    :pswitch_0
    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;
    invoke-static {v3}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$200(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;

    move-result-object v3

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 450
    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    invoke-virtual {v3}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->hide()V

    goto :goto_0

    .line 453
    :cond_1
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 454
    .local v0, "fadeMessage":Landroid/os/Message;
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->removeMessages(I)V

    .line 455
    const-wide/16 v4, 0x1388

    invoke-virtual {p0, v0, v4, v5}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 459
    .end local v0    # "fadeMessage":Landroid/os/Message;
    :pswitch_1
    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # invokes: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->setProgress()I
    invoke-static {v3}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$500(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)I

    move-result v2

    .line 460
    .local v2, "pos":I
    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mDragging:Z
    invoke-static {v3}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$000(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mShowing:Z
    invoke-static {v3}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$700(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;
    invoke-static {v3}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$200(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;

    move-result-object v3

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 461
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 462
    .local v1, "message":Landroid/os/Message;
    rem-int/lit16 v3, v2, 0x3e8

    rsub-int v3, v3, 0x3e8

    int-to-long v4, v3

    invoke-virtual {p0, v1, v4, v5}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 447
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
