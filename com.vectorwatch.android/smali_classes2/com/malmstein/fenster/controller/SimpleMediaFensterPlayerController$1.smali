.class Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;
.super Ljava/lang/Object;
.source "SimpleMediaFensterPlayerController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    .prologue
    .line 401
    iput-object p1, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8
    .param p1, "bar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromuser"    # Z

    .prologue
    .line 416
    if-nez p3, :cond_1

    .line 428
    :cond_0
    :goto_0
    return-void

    .line 422
    :cond_1
    iget-object v4, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;
    invoke-static {v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$200(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;

    move-result-object v4

    invoke-interface {v4}, Lcom/malmstein/fenster/play/FensterPlayer;->getDuration()I

    move-result v4

    int-to-long v0, v4

    .line 423
    .local v0, "duration":J
    int-to-long v4, p2

    mul-long/2addr v4, v0

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 424
    .local v2, "newposition":J
    iget-object v4, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;
    invoke-static {v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$200(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;

    move-result-object v4

    long-to-int v5, v2

    invoke-interface {v4, v5}, Lcom/malmstein/fenster/play/FensterPlayer;->seekTo(I)V

    .line 425
    iget-object v4, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$300(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 426
    iget-object v4, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$300(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    long-to-int v6, v2

    # invokes: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->stringForTime(I)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$400(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    .line 403
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->show(I)V

    .line 405
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    const/4 v1, 0x1

    # setter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mDragging:Z
    invoke-static {v0, v1}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$002(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;Z)Z

    .line 412
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$100(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 413
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    .line 431
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    const/4 v1, 0x0

    # setter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mDragging:Z
    invoke-static {v0, v1}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$002(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;Z)Z

    .line 432
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # invokes: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->setProgress()I
    invoke-static {v0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$500(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)I

    .line 433
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # invokes: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->updatePausePlay()V
    invoke-static {v0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$600(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)V

    .line 434
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->show(I)V

    .line 439
    iget-object v0, p0, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController$1;->this$0:Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;->access$100(Lcom/malmstein/fenster/controller/SimpleMediaFensterPlayerController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 440
    return-void
.end method
