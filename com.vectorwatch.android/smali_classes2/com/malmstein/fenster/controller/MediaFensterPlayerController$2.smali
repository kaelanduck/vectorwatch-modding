.class Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;
.super Landroid/os/Handler;
.source "MediaFensterPlayerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/controller/MediaFensterPlayerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 70
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 72
    :pswitch_0
    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;
    invoke-static {v3}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$100(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;

    move-result-object v3

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 73
    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    invoke-virtual {v3}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->hide()V

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 77
    .local v0, "fadeMessage":Landroid/os/Message;
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->removeMessages(I)V

    .line 78
    const-wide/16 v4, 0x1388

    invoke-virtual {p0, v0, v4, v5}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 82
    .end local v0    # "fadeMessage":Landroid/os/Message;
    :pswitch_1
    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # invokes: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->setProgress()I
    invoke-static {v3}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$200(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)I

    move-result v2

    .line 83
    .local v2, "pos":I
    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z
    invoke-static {v3}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$300(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mShowing:Z
    invoke-static {v3}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$400(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;
    invoke-static {v3}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$100(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;

    move-result-object v3

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 84
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 85
    .local v1, "message":Landroid/os/Message;
    rem-int/lit16 v3, v2, 0x3e8

    rsub-int v3, v3, 0x3e8

    int-to-long v4, v3

    invoke-virtual {p0, v1, v4, v5}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
