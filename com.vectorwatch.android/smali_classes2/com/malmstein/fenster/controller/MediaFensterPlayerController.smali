.class public final Lcom/malmstein/fenster/controller/MediaFensterPlayerController;
.super Landroid/widget/RelativeLayout;
.source "MediaFensterPlayerController.java"

# interfaces
.implements Lcom/malmstein/fenster/controller/FensterPlayerController;
.implements Lcom/malmstein/fenster/gestures/FensterEventsListener;
.implements Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;
.implements Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;


# static fields
.field private static final DEFAULT_TIMEOUT:I = 0x1388

.field public static final DEFAULT_VIDEO_START:I = 0x0

.field private static final FADE_OUT:I = 0x1

.field public static final MAX_VIDEO_PROGRESS:I = 0x3e8

.field public static final ONE_FINGER:I = 0x1

.field private static final SHOW_PROGRESS:I = 0x2

.field public static final SKIP_VIDEO_PROGRESS:I = 0x64

.field public static final TAG:Ljava/lang/String; = "PlayerController"


# instance fields
.field private bottomControlsArea:Landroid/view/View;

.field private gestureControllerView:Lcom/malmstein/fenster/gestures/FensterGestureControllerView;

.field private lastPlayedSeconds:I

.field private mBrightness:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

.field private mCurrentTime:Landroid/widget/TextView;

.field private mDragging:Z

.field private mEndTime:Landroid/widget/TextView;

.field private mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

.field private mFirstTimeLoading:Z

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private final mHandler:Landroid/os/Handler;

.field private mManualDragging:Z

.field private mNextButton:Landroid/widget/ImageButton;

.field private mPauseButton:Landroid/widget/ImageButton;

.field private final mPauseListener:Landroid/view/View$OnClickListener;

.field private mPrevButton:Landroid/widget/ImageButton;

.field private mProgress:Landroid/widget/SeekBar;

.field private final mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mShowing:Z

.field private mVolume:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

.field private visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 160
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 161
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 168
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    new-instance v0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$1;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$1;-><init>(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)V

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseListener:Landroid/view/View$OnClickListener;

    .line 66
    new-instance v0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$2;-><init>(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)V

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFirstTimeLoading:Z

    .line 113
    new-instance v0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;-><init>(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)V

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 157
    const/4 v0, -0x1

    iput v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->lastPlayedSeconds:I

    .line 169
    return-void
.end method

.method static synthetic access$000(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->doPauseResume()V

    return-void
.end method

.method static synthetic access$100(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)I
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->setProgress()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z

    return v0
.end method

.method static synthetic access$302(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z

    return p1
.end method

.method static synthetic access$400(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mShowing:Z

    return v0
.end method

.method static synthetic access$500(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mManualDragging:Z

    return v0
.end method

.method static synthetic access$700(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->stringForTime(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updatePausePlay()V

    return-void
.end method

.method private backwardSkippingUnit()I
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    add-int/lit8 v0, v0, -0x64

    return v0
.end method

.method private doPauseResume()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterPlayer;->pause()V

    .line 413
    :goto_0
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updatePausePlay()V

    .line 414
    return-void

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterPlayer;->start()V

    goto :goto_0
.end method

.method private extractDeltaScale(IFLandroid/widget/SeekBar;)I
    .locals 6
    .param p1, "availableSpace"    # I
    .param p2, "deltaX"    # F
    .param p3, "seekbar"    # Landroid/widget/SeekBar;

    .prologue
    .line 547
    float-to-int v3, p2

    .line 549
    .local v3, "x":I
    invoke-virtual {p3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v4

    int-to-float v1, v4

    .line 550
    .local v1, "progress":F
    invoke-virtual {p3}, Landroid/widget/SeekBar;->getMax()I

    move-result v0

    .line 552
    .local v0, "max":I
    if-gez v3, :cond_0

    .line 553
    int-to-float v4, v3

    sub-int v5, v0, p1

    int-to-float v5, v5

    div-float v2, v4, v5

    .line 554
    .local v2, "scale":F
    mul-float v4, v2, v1

    sub-float/2addr v1, v4

    .line 560
    :goto_0
    float-to-int v4, v1

    return v4

    .line 556
    .end local v2    # "scale":F
    :cond_0
    int-to-float v4, v3

    int-to-float v5, p1

    div-float v2, v4, v5

    .line 557
    .restart local v2    # "scale":F
    int-to-float v4, v0

    mul-float/2addr v4, v2

    add-float/2addr v1, v4

    goto :goto_0
.end method

.method private extractHorizontalDeltaScale(FLandroid/widget/SeekBar;)I
    .locals 1
    .param p1, "deltaX"    # F
    .param p2, "seekbar"    # Landroid/widget/SeekBar;

    .prologue
    .line 539
    invoke-virtual {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->getWidth()I

    move-result v0

    invoke-direct {p0, v0, p1, p2}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->extractDeltaScale(IFLandroid/widget/SeekBar;)I

    move-result v0

    return v0
.end method

.method private extractVerticalDeltaScale(FLandroid/widget/SeekBar;)I
    .locals 1
    .param p1, "deltaY"    # F
    .param p2, "seekbar"    # Landroid/widget/SeekBar;

    .prologue
    .line 543
    invoke-virtual {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->getHeight()I

    move-result v0

    invoke-direct {p0, v0, p1, p2}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->extractDeltaScale(IFLandroid/widget/SeekBar;)I

    move-result v0

    return v0
.end method

.method private forwardSkippingUnit()I
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    add-int/lit8 v0, v0, 0x64

    return v0
.end method

.method private initControllerView()V
    .locals 3

    .prologue
    .line 188
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_bottom_root:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->bottomControlsArea:Landroid/view/View;

    .line 190
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_gestures_area:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/malmstein/fenster/gestures/FensterGestureControllerView;

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->gestureControllerView:Lcom/malmstein/fenster/gestures/FensterGestureControllerView;

    .line 191
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->gestureControllerView:Lcom/malmstein/fenster/gestures/FensterGestureControllerView;

    invoke-virtual {v0, p0}, Lcom/malmstein/fenster/gestures/FensterGestureControllerView;->setFensterEventsListener(Lcom/malmstein/fenster/gestures/FensterEventsListener;)V

    .line 193
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_pause:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    .line 194
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 195
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_next:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mNextButton:Landroid/widget/ImageButton;

    .line 198
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_previous:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPrevButton:Landroid/widget/ImageButton;

    .line 200
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_progress:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    .line 201
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 202
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 204
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_volume:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mVolume:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    .line 205
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mVolume:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    invoke-virtual {v0, p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->initialise(Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;)V

    .line 207
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_brightness:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mBrightness:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    .line 208
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mBrightness:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    invoke-virtual {v0, p0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->initialise(Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;)V

    .line 210
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_time:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mEndTime:Landroid/widget/TextView;

    .line 211
    sget v0, Lcom/malmstein/fenster/R$id;->media_controller_time_current:I

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;

    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFormatBuilder:Ljava/lang/StringBuilder;

    .line 213
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFormatter:Ljava/util/Formatter;

    .line 214
    return-void
.end method

.method private setProgress()I
    .locals 10

    .prologue
    .line 313
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    if-eqz v6, :cond_0

    iget-boolean v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z

    if-eqz v6, :cond_2

    .line 314
    :cond_0
    const/4 v3, 0x0

    .line 338
    :cond_1
    :goto_0
    return v3

    .line 316
    :cond_2
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v6}, Lcom/malmstein/fenster/play/FensterPlayer;->getCurrentPosition()I

    move-result v3

    .line 317
    .local v3, "position":I
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v6}, Lcom/malmstein/fenster/play/FensterPlayer;->getDuration()I

    move-result v0

    .line 318
    .local v0, "duration":I
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    if-eqz v6, :cond_4

    .line 319
    if-lez v0, :cond_3

    .line 321
    const-wide/16 v6, 0x3e8

    int-to-long v8, v3

    mul-long/2addr v6, v8

    int-to-long v8, v0

    div-long v4, v6, v8

    .line 322
    .local v4, "pos":J
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    long-to-int v7, v4

    invoke-virtual {v6, v7}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 324
    .end local v4    # "pos":J
    :cond_3
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v6}, Lcom/malmstein/fenster/play/FensterPlayer;->getBufferPercentage()I

    move-result v1

    .line 325
    .local v1, "percent":I
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    mul-int/lit8 v7, v1, 0xa

    invoke-virtual {v6, v7}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 328
    .end local v1    # "percent":I
    :cond_4
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mEndTime:Landroid/widget/TextView;

    if-eqz v6, :cond_5

    .line 329
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->stringForTime(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    :cond_5
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v6, :cond_6

    .line 332
    iget-object v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->stringForTime(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    :cond_6
    div-int/lit16 v2, v3, 0x3e8

    .line 335
    .local v2, "playedSeconds":I
    iget v6, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->lastPlayedSeconds:I

    if-eq v6, v2, :cond_1

    .line 336
    iput v2, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->lastPlayedSeconds:I

    goto :goto_0
.end method

.method private showBottomArea()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->bottomControlsArea:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 265
    return-void
.end method

.method private skipVideoBackwards()V
    .locals 4

    .prologue
    .line 535
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->backwardSkippingUnit()I

    move-result v2

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 536
    return-void
.end method

.method private skipVideoForward()V
    .locals 4

    .prologue
    .line 531
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->forwardSkippingUnit()I

    move-result v2

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 532
    return-void
.end method

.method private stringForTime(I)Ljava/lang/String;
    .locals 11
    .param p1, "timeMs"    # I

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 298
    div-int/lit16 v3, p1, 0x3e8

    .line 300
    .local v3, "totalSeconds":I
    rem-int/lit8 v2, v3, 0x3c

    .line 301
    .local v2, "seconds":I
    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    .line 302
    .local v1, "minutes":I
    div-int/lit16 v0, v3, 0xe10

    .line 304
    .local v0, "hours":I
    iget-object v4, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 305
    if-lez v0, :cond_0

    .line 306
    iget-object v4, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFormatter:Ljava/util/Formatter;

    const-string v5, "%d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    .line 308
    :goto_0
    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFormatter:Ljava/util/Formatter;

    const-string v5, "%02d:%02d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private updateBrightnessProgressBar(F)V
    .locals 2
    .param p1, "delta"    # F

    .prologue
    .line 523
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mBrightness:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    float-to-int v1, p1

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->manuallyUpdate(I)V

    .line 524
    return-void
.end method

.method private updatePausePlay()V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 405
    :goto_0
    return-void

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v0}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    const v1, 0x1080023

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 403
    :cond_1
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    const v1, 0x1080024

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method private updateVideoProgressBar(F)V
    .locals 4
    .param p1, "delta"    # F

    .prologue
    .line 527
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    invoke-direct {p0, p1, v2}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->extractHorizontalDeltaScale(FLandroid/widget/SeekBar;)I

    move-result v2

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 528
    return-void
.end method

.method private updateVolumeProgressBar(F)V
    .locals 2
    .param p1, "delta"    # F

    .prologue
    .line 519
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mVolume:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mVolume:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    invoke-direct {p0, p1, v1}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->extractVerticalDeltaScale(FLandroid/widget/SeekBar;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->manuallyUpdate(I)V

    .line 520
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v4, 0x1388

    const/4 v2, 0x1

    .line 349
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 350
    .local v0, "keyCode":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_2

    .line 351
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_2

    move v1, v2

    .line 352
    .local v1, "uniqueDown":Z
    :goto_0
    const/16 v3, 0x4f

    if-eq v0, v3, :cond_0

    const/16 v3, 0x55

    if-eq v0, v3, :cond_0

    const/16 v3, 0x3e

    if-ne v0, v3, :cond_3

    .line 355
    :cond_0
    if-eqz v1, :cond_1

    .line 356
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->doPauseResume()V

    .line 357
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->show(I)V

    .line 358
    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_1

    .line 359
    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 392
    :cond_1
    :goto_1
    return v2

    .line 351
    .end local v1    # "uniqueDown":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 363
    .restart local v1    # "uniqueDown":Z
    :cond_3
    const/16 v3, 0x7e

    if-ne v0, v3, :cond_4

    .line 364
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v3

    if-nez v3, :cond_1

    .line 365
    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->start()V

    .line 366
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updatePausePlay()V

    .line 367
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->show(I)V

    goto :goto_1

    .line 370
    :cond_4
    const/16 v3, 0x56

    if-eq v0, v3, :cond_5

    const/16 v3, 0x7f

    if-ne v0, v3, :cond_6

    .line 372
    :cond_5
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 373
    iget-object v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    invoke-interface {v3}, Lcom/malmstein/fenster/play/FensterPlayer;->pause()V

    .line 374
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updatePausePlay()V

    .line 375
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->show(I)V

    goto :goto_1

    .line 378
    :cond_6
    const/16 v3, 0x19

    if-eq v0, v3, :cond_7

    const/16 v3, 0x18

    if-eq v0, v3, :cond_7

    const/16 v3, 0xa4

    if-eq v0, v3, :cond_7

    const/16 v3, 0x1b

    if-ne v0, v3, :cond_8

    .line 383
    :cond_7
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_1

    .line 384
    :cond_8
    const/4 v3, 0x4

    if-eq v0, v3, :cond_9

    const/16 v3, 0x52

    if-ne v0, v3, :cond_a

    .line 385
    :cond_9
    if-eqz v1, :cond_1

    .line 386
    invoke-virtual {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->hide()V

    goto :goto_1

    .line 391
    :cond_a
    invoke-virtual {p0, v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->show(I)V

    .line 392
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_1
.end method

.method public hide()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 280
    iget-boolean v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z

    if-nez v1, :cond_1

    .line 281
    iget-boolean v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mShowing:Z

    if-eqz v1, :cond_0

    .line 283
    :try_start_0
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 284
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :goto_0
    iput-boolean v3, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mShowing:Z

    .line 290
    :cond_0
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    if-eqz v1, :cond_1

    .line 291
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    invoke-interface {v1, v3}, Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;->onControlsVisibilityChange(Z)V

    .line 295
    :cond_1
    return-void

    .line 285
    :catch_0
    move-exception v0

    .line 286
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v1, "MediaController"

    const-string v2, "already removed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isFirstTimeLoading()Z
    .locals 1

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFirstTimeLoading:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mShowing:Z

    return v0
.end method

.method public onBrightnessFinishedDragging()V
    .locals 1

    .prologue
    .line 515
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z

    .line 516
    return-void
.end method

.method public onBrigthnessStartedDragging()V
    .locals 1

    .prologue
    .line 510
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z

    .line 511
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/malmstein/fenster/R$layout;->view_media_controller:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 174
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->initControllerView()V

    .line 175
    return-void
.end method

.method public onHorizontalScroll(Landroid/view/MotionEvent;F)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "delta"    # F

    .prologue
    .line 458
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 459
    invoke-direct {p0, p2}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updateVideoProgressBar(F)V

    .line 467
    :goto_0
    return-void

    .line 461
    :cond_0
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    .line 462
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->skipVideoForward()V

    goto :goto_0

    .line 464
    :cond_1
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->skipVideoBackwards()V

    goto :goto_0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 441
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 442
    const-class v0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 443
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 447
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 448
    const-class v0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 449
    return-void
.end method

.method public onSwipeBottom()V
    .locals 0

    .prologue
    .line 491
    return-void
.end method

.method public onSwipeLeft()V
    .locals 0

    .prologue
    .line 485
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->skipVideoBackwards()V

    .line 486
    return-void
.end method

.method public onSwipeRight()V
    .locals 0

    .prologue
    .line 480
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->skipVideoForward()V

    .line 481
    return-void
.end method

.method public onSwipeTop()V
    .locals 0

    .prologue
    .line 496
    return-void
.end method

.method public onTap()V
    .locals 2

    .prologue
    .line 453
    const-string v0, "PlayerController"

    const-string v1, "Single Tap Up"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    return-void
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 343
    const/16 v0, 0x1388

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->show(I)V

    .line 344
    const/4 v0, 0x0

    return v0
.end method

.method public onVerticalScroll(Landroid/view/MotionEvent;F)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "delta"    # F

    .prologue
    .line 471
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 472
    neg-float v0, p2

    invoke-direct {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updateVolumeProgressBar(F)V

    .line 476
    :goto_0
    return-void

    .line 474
    :cond_0
    neg-float v0, p2

    invoke-direct {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updateBrightnessProgressBar(F)V

    goto :goto_0
.end method

.method public onVolumeFinishedDragging()V
    .locals 1

    .prologue
    .line 505
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z

    .line 506
    return-void
.end method

.method public onVolumeStartedDragging()V
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z

    .line 501
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 418
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mNextButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 424
    :cond_1
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPrevButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 425
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPrevButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 427
    :cond_2
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    if-eqz v0, :cond_3

    .line 428
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mProgress:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 430
    :cond_3
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mVolume:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    if-eqz v0, :cond_4

    .line 431
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mVolume:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    invoke-virtual {v0, p1}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->setEnabled(Z)V

    .line 433
    :cond_4
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mBrightness:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    if-eqz v0, :cond_5

    .line 434
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mBrightness:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    invoke-virtual {v0, p1}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->setEnabled(Z)V

    .line 436
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 437
    return-void
.end method

.method public setMediaPlayer(Lcom/malmstein/fenster/play/FensterPlayer;)V
    .locals 0
    .param p1, "fensterPlayer"    # Lcom/malmstein/fenster/play/FensterPlayer;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;

    .line 180
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updatePausePlay()V

    .line 181
    return-void
.end method

.method public setVisibilityListener(Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;)V
    .locals 0
    .param p1, "visibilityListener"    # Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    .line 185
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 222
    const/16 v0, 0x1388

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->show(I)V

    .line 223
    return-void
.end method

.method public show(I)V
    .locals 5
    .param p1, "timeInMilliSeconds"    # I

    .prologue
    const/4 v4, 0x1

    .line 234
    iget-boolean v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mShowing:Z

    if-nez v1, :cond_1

    .line 235
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->showBottomArea()V

    .line 236
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->setProgress()I

    .line 237
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 238
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 240
    :cond_0
    iput-boolean v4, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mShowing:Z

    .line 241
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->setVisibility(I)V

    .line 244
    :cond_1
    invoke-direct {p0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updatePausePlay()V

    .line 249
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 251
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 252
    .local v0, "msg":Landroid/os/Message;
    if-eqz p1, :cond_2

    .line 253
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 254
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mHandler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 257
    :cond_2
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    if-eqz v1, :cond_3

    .line 258
    iget-object v1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->visibilityListener:Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    invoke-interface {v1, v4}, Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;->onControlsVisibilityChange(Z)V

    .line 261
    :cond_3
    return-void
.end method
