.class Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;
.super Ljava/lang/Object;
.source "MediaFensterPlayerController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/controller/MediaFensterPlayerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 8
    .param p1, "bar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromuser"    # Z

    .prologue
    .line 128
    if-nez p3, :cond_1

    iget-object v4, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mManualDragging:Z
    invoke-static {v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$600(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    iget-object v4, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;
    invoke-static {v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$100(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;

    move-result-object v4

    invoke-interface {v4}, Lcom/malmstein/fenster/play/FensterPlayer;->getDuration()I

    move-result v4

    int-to-long v0, v4

    .line 135
    .local v0, "duration":J
    int-to-long v4, p2

    mul-long/2addr v4, v0

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    .line 136
    .local v2, "newposition":J
    iget-object v4, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mFensterPlayer:Lcom/malmstein/fenster/play/FensterPlayer;
    invoke-static {v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$100(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Lcom/malmstein/fenster/play/FensterPlayer;

    move-result-object v4

    long-to-int v5, v2

    invoke-interface {v4, v5}, Lcom/malmstein/fenster/play/FensterPlayer;->seekTo(I)V

    .line 137
    iget-object v4, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$700(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 138
    iget-object v4, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mCurrentTime:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$700(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    long-to-int v6, v2

    # invokes: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->stringForTime(I)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$800(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->show(I)V

    .line 117
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    const/4 v1, 0x1

    # setter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z
    invoke-static {v0, v1}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$302(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;Z)Z

    .line 124
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$500(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 125
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    const/4 v1, 0x0

    # setter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mDragging:Z
    invoke-static {v0, v1}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$302(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;Z)Z

    .line 144
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # invokes: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->setProgress()I
    invoke-static {v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$200(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)I

    .line 145
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # invokes: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->updatePausePlay()V
    invoke-static {v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$900(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)V

    .line 146
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->show(I)V

    .line 151
    iget-object v0, p0, Lcom/malmstein/fenster/controller/MediaFensterPlayerController$3;->this$0:Lcom/malmstein/fenster/controller/MediaFensterPlayerController;

    # getter for: Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/malmstein/fenster/controller/MediaFensterPlayerController;->access$500(Lcom/malmstein/fenster/controller/MediaFensterPlayerController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 152
    return-void
.end method
