.class public Lcom/malmstein/fenster/gestures/FensterGestureControllerView;
.super Landroid/view/View;
.source "FensterGestureControllerView.java"


# instance fields
.field private gestureDetector:Landroid/view/GestureDetector;

.field private listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method private mayNotifyGestureDetector(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/malmstein/fenster/gestures/FensterGestureControllerView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 42
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 29
    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    .line 30
    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/gestures/FensterGestureControllerView;->setClickable(Z)V

    .line 31
    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/gestures/FensterGestureControllerView;->setFocusable(Z)V

    .line 32
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/malmstein/fenster/gestures/FensterGestureControllerView;->mayNotifyGestureDetector(Landroid/view/MotionEvent;)V

    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public setFensterEventsListener(Lcom/malmstein/fenster/gestures/FensterEventsListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/malmstein/fenster/gestures/FensterEventsListener;

    .prologue
    .line 45
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/malmstein/fenster/gestures/FensterGestureControllerView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/malmstein/fenster/gestures/FensterGestureListener;

    invoke-virtual {p0}, Lcom/malmstein/fenster/gestures/FensterGestureControllerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/malmstein/fenster/gestures/FensterGestureListener;-><init>(Lcom/malmstein/fenster/gestures/FensterEventsListener;Landroid/view/ViewConfiguration;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/malmstein/fenster/gestures/FensterGestureControllerView;->gestureDetector:Landroid/view/GestureDetector;

    .line 46
    return-void
.end method
