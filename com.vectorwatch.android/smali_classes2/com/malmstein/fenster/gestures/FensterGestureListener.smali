.class public Lcom/malmstein/fenster/gestures/FensterGestureListener;
.super Ljava/lang/Object;
.source "FensterGestureListener.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# static fields
.field private static final SWIPE_THRESHOLD:I = 0x64

.field public static final TAG:Ljava/lang/String; = "FensterGestureListener"


# instance fields
.field private final listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;

.field private final minFlingVelocity:I


# direct methods
.method public constructor <init>(Lcom/malmstein/fenster/gestures/FensterEventsListener;Landroid/view/ViewConfiguration;)V
    .locals 1
    .param p1, "listener"    # Lcom/malmstein/fenster/gestures/FensterEventsListener;
    .param p2, "viewConfiguration"    # Landroid/view/ViewConfiguration;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;

    .line 18
    invoke-virtual {p2}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->minFlingVelocity:I

    .line 19
    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 102
    const-string v0, "FensterGestureListener"

    const-string v1, "Down"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/high16 v7, 0x42c80000    # 100.0f

    const/4 v6, 0x0

    .line 66
    const-string v4, "FensterGestureListener"

    const-string v5, "Fling"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const/4 v3, 0x0

    .line 69
    .local v3, "result":Z
    :try_start_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float v1, v4, v5

    .line 70
    .local v1, "diffY":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float v0, v4, v5

    .line 71
    .local v0, "diffX":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    .line 72
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, v4, v7

    if-lez v4, :cond_0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->minFlingVelocity:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 73
    cmpl-float v4, v0, v6

    if-lez v4, :cond_2

    .line 74
    iget-object v4, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;

    invoke-interface {v4}, Lcom/malmstein/fenster/gestures/FensterEventsListener;->onSwipeRight()V

    .line 79
    :cond_0
    :goto_0
    const/4 v3, 0x1

    .line 87
    :cond_1
    :goto_1
    const/4 v3, 0x1

    .line 92
    .end local v0    # "diffX":F
    .end local v1    # "diffY":F
    :goto_2
    return v3

    .line 76
    .restart local v0    # "diffX":F
    .restart local v1    # "diffY":F
    :cond_2
    iget-object v4, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;

    invoke-interface {v4}, Lcom/malmstein/fenster/gestures/FensterEventsListener;->onSwipeLeft()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    .end local v0    # "diffX":F
    .end local v1    # "diffY":F
    :catch_0
    move-exception v2

    .line 90
    .local v2, "exception":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 80
    .end local v2    # "exception":Ljava/lang/Exception;
    .restart local v0    # "diffX":F
    .restart local v1    # "diffY":F
    :cond_3
    :try_start_1
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, v4, v7

    if-lez v4, :cond_1

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->minFlingVelocity:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 81
    cmpl-float v4, v1, v6

    if-lez v4, :cond_4

    .line 82
    iget-object v4, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;

    invoke-interface {v4}, Lcom/malmstein/fenster/gestures/FensterEventsListener;->onSwipeBottom()V

    goto :goto_1

    .line 84
    :cond_4
    iget-object v4, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;

    invoke-interface {v4}, Lcom/malmstein/fenster/gestures/FensterEventsListener;->onSwipeTop()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 31
    const-string v0, "FensterGestureListener"

    const-string v1, "Long Press"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/high16 v5, 0x42c80000    # 100.0f

    const/4 v4, 0x0

    .line 36
    const-string v2, "FensterGestureListener"

    const-string v3, "Scroll"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float v1, v2, v3

    .line 39
    .local v1, "deltaY":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float v0, v2, v3

    .line 41
    .local v0, "deltaX":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 42
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v5

    if-lez v2, :cond_0

    .line 43
    iget-object v2, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;

    invoke-interface {v2, p2, v0}, Lcom/malmstein/fenster/gestures/FensterEventsListener;->onHorizontalScroll(Landroid/view/MotionEvent;F)V

    .line 44
    cmpl-float v2, v0, v4

    if-lez v2, :cond_1

    .line 45
    const-string v2, "FensterGestureListener"

    const-string v3, "Slide right"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 47
    :cond_1
    const-string v2, "FensterGestureListener"

    const-string v3, "Slide left"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 51
    :cond_2
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v5

    if-lez v2, :cond_0

    .line 52
    iget-object v2, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;

    invoke-interface {v2, p2, v1}, Lcom/malmstein/fenster/gestures/FensterEventsListener;->onVerticalScroll(Landroid/view/MotionEvent;F)V

    .line 53
    cmpl-float v2, v1, v4

    if-lez v2, :cond_3

    .line 54
    const-string v2, "FensterGestureListener"

    const-string v3, "Slide down"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 56
    :cond_3
    const-string v2, "FensterGestureListener"

    const-string v3, "Slide up"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 97
    const-string v0, "FensterGestureListener"

    const-string v1, "Show Press"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/malmstein/fenster/gestures/FensterGestureListener;->listener:Lcom/malmstein/fenster/gestures/FensterEventsListener;

    invoke-interface {v0}, Lcom/malmstein/fenster/gestures/FensterEventsListener;->onTap()V

    .line 24
    const/4 v0, 0x0

    return v0
.end method
