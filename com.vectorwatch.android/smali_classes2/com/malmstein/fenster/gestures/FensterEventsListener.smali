.class public interface abstract Lcom/malmstein/fenster/gestures/FensterEventsListener;
.super Ljava/lang/Object;
.source "FensterEventsListener.java"


# virtual methods
.method public abstract onHorizontalScroll(Landroid/view/MotionEvent;F)V
.end method

.method public abstract onSwipeBottom()V
.end method

.method public abstract onSwipeLeft()V
.end method

.method public abstract onSwipeRight()V
.end method

.method public abstract onSwipeTop()V
.end method

.method public abstract onTap()V
.end method

.method public abstract onVerticalScroll(Landroid/view/MotionEvent;F)V
.end method
