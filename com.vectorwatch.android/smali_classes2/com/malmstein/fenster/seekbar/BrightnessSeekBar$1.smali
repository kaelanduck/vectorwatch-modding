.class Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;
.super Ljava/lang/Object;
.source "BrightnessSeekBar.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;->this$0:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "brightness"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 18
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;->this$0:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    invoke-virtual {v0, p2}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->setBrightness(I)V

    .line 19
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;->this$0:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    invoke-virtual {v0, p2}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->setProgress(I)V

    .line 20
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;->this$0:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    # getter for: Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->brightnessListener:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;
    invoke-static {v0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->access$000(Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;)Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;->onBrigthnessStartedDragging()V

    .line 25
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;->this$0:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    # getter for: Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->brightnessListener:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;
    invoke-static {v0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->access$000(Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;)Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;->onBrightnessFinishedDragging()V

    .line 30
    return-void
.end method
