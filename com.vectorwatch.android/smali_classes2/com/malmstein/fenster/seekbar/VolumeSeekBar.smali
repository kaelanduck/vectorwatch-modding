.class public Lcom/malmstein/fenster/seekbar/VolumeSeekBar;
.super Landroid/widget/SeekBar;
.source "VolumeSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;
    }
.end annotation


# instance fields
.field private audioManager:Landroid/media/AudioManager;

.field private volumeListener:Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;

.field private volumeReceiver:Landroid/content/BroadcastReceiver;

.field public final volumeSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 15
    new-instance v0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;-><init>(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)V

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 34
    new-instance v0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$2;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$2;-><init>(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)V

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeReceiver:Landroid/content/BroadcastReceiver;

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    new-instance v0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;-><init>(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)V

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 34
    new-instance v0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$2;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$2;-><init>(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)V

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeReceiver:Landroid/content/BroadcastReceiver;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    new-instance v0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;-><init>(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)V

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 34
    new-instance v0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$2;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$2;-><init>(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)V

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeReceiver:Landroid/content/BroadcastReceiver;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeListener:Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->updateVolumeProgress()V

    return-void
.end method

.method private registerVolumeReceiver()V
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.media.VOLUME_CHANGED_ACTION"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 92
    return-void
.end method

.method private unregisterVolumeReceiver()V
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 96
    return-void
.end method

.method private updateVolumeProgress()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->audioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->setProgress(I)V

    .line 88
    return-void
.end method


# virtual methods
.method public initialise(Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;)V
    .locals 3
    .param p1, "volumeListener"    # Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;

    .prologue
    const/4 v2, 0x3

    .line 78
    invoke-virtual {p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->audioManager:Landroid/media/AudioManager;

    .line 79
    iput-object p1, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeListener:Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;

    .line 81
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->setMax(I)V

    .line 82
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->setProgress(I)V

    .line 83
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 84
    return-void
.end method

.method public manuallyUpdate(I)V
    .locals 2
    .param p1, "update"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, p1, v1}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 100
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0}, Landroid/widget/SeekBar;->onAttachedToWindow()V

    .line 68
    invoke-direct {p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->registerVolumeReceiver()V

    .line 69
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->unregisterVolumeReceiver()V

    .line 74
    invoke-super {p0}, Landroid/widget/SeekBar;->onDetachedFromWindow()V

    .line 75
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 56
    const-class v0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 62
    const-class v0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 63
    return-void
.end method
