.class public Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;
.super Landroid/widget/SeekBar;
.source "BrightnessSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;
    }
.end annotation


# static fields
.field public static final MAX_BRIGHTNESS:I = 0xff

.field public static final MIN_BRIGHTNESS:I


# instance fields
.field private brightnessListener:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;

.field public final brightnessSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    .line 15
    new-instance v0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;-><init>(Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;)V

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->brightnessSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    new-instance v0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;-><init>(Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;)V

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->brightnessSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    new-instance v0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$1;-><init>(Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;)V

    iput-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->brightnessSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;)Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->brightnessListener:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;

    return-object v0
.end method


# virtual methods
.method public initialise(Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;)V
    .locals 1
    .param p1, "brightnessListener"    # Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;

    .prologue
    .line 59
    const/16 v0, 0xff

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->setMax(I)V

    .line 60
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->brightnessSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 61
    iput-object p1, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->brightnessListener:Lcom/malmstein/fenster/seekbar/BrightnessSeekBar$Listener;

    .line 62
    invoke-virtual {p0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/malmstein/fenster/helper/BrightnessHelper;->getBrightness(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->manuallyUpdate(I)V

    .line 63
    return-void
.end method

.method public manuallyUpdate(I)V
    .locals 2
    .param p1, "update"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->brightnessSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, p1, v1}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 77
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 49
    const-class v0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 50
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 55
    const-class v0, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 56
    return-void
.end method

.method public setBrightness(I)V
    .locals 1
    .param p1, "brightness"    # I

    .prologue
    .line 66
    if-gez p1, :cond_1

    .line 67
    const/4 p1, 0x0

    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/malmstein/fenster/seekbar/BrightnessSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/malmstein/fenster/helper/BrightnessHelper;->setBrightness(Landroid/content/Context;I)V

    .line 73
    return-void

    .line 68
    :cond_1
    const/16 v0, 0xff

    if-le p1, v0, :cond_0

    .line 69
    const/16 p1, 0xff

    goto :goto_0
.end method
