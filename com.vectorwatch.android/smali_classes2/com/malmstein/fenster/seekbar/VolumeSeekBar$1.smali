.class Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;
.super Ljava/lang/Object;
.source "VolumeSeekBar.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/malmstein/fenster/seekbar/VolumeSeekBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;


# direct methods
.method constructor <init>(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;->this$0:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "vol"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 18
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;->this$0:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    # getter for: Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->access$000(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 19
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;->this$0:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    # getter for: Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeListener:Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;
    invoke-static {v0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->access$100(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;->onVolumeStartedDragging()V

    .line 24
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$1;->this$0:Lcom/malmstein/fenster/seekbar/VolumeSeekBar;

    # getter for: Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->volumeListener:Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;
    invoke-static {v0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar;->access$100(Lcom/malmstein/fenster/seekbar/VolumeSeekBar;)Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/malmstein/fenster/seekbar/VolumeSeekBar$Listener;->onVolumeFinishedDragging()V

    .line 29
    return-void
.end method
