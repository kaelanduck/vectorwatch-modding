.class public Lcom/malmstein/fenster/play/FensterVideoFragment;
.super Landroid/app/Fragment;
.source "FensterVideoFragment.java"

# interfaces
.implements Lcom/malmstein/fenster/play/FensterVideoStateListener;


# instance fields
.field private contentView:Landroid/view/View;

.field private fensterLoadingView:Lcom/malmstein/fenster/view/FensterLoadingView;

.field private fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

.field private textureView:Lcom/malmstein/fenster/view/FensterVideoView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 24
    return-void
.end method

.method private initVideo()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->textureView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setMediaController(Lcom/malmstein/fenster/controller/FensterPlayerController;)V

    .line 43
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->textureView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, p0}, Lcom/malmstein/fenster/view/FensterVideoView;->setOnPlayStateListener(Lcom/malmstein/fenster/play/FensterVideoStateListener;)V

    .line 44
    return-void
.end method

.method private showLoadingView()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->fensterLoadingView:Lcom/malmstein/fenster/view/FensterLoadingView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterLoadingView;->show()V

    .line 63
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-interface {v0}, Lcom/malmstein/fenster/controller/FensterPlayerController;->hide()V

    .line 64
    return-void
.end method


# virtual methods
.method public onBuffer()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/malmstein/fenster/play/FensterVideoFragment;->showLoadingView()V

    .line 79
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    sget v0, Lcom/malmstein/fenster/R$layout;->fragment_fenster_video:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->contentView:Landroid/view/View;

    .line 29
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->contentView:Landroid/view/View;

    sget v1, Lcom/malmstein/fenster/R$id;->play_video_texture:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/malmstein/fenster/view/FensterVideoView;

    iput-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->textureView:Lcom/malmstein/fenster/view/FensterVideoView;

    .line 30
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->contentView:Landroid/view/View;

    sget v1, Lcom/malmstein/fenster/R$id;->play_video_controller:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/malmstein/fenster/controller/FensterPlayerController;

    iput-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    .line 31
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->contentView:Landroid/view/View;

    sget v1, Lcom/malmstein/fenster/R$id;->play_video_loading:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/malmstein/fenster/view/FensterLoadingView;

    iput-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->fensterLoadingView:Lcom/malmstein/fenster/view/FensterLoadingView;

    .line 32
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->contentView:Landroid/view/View;

    return-object v0
.end method

.method public onFirstVideoFrameRendered()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-interface {v0}, Lcom/malmstein/fenster/controller/FensterPlayerController;->show()V

    .line 69
    return-void
.end method

.method public onPlay()V
    .locals 0

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/malmstein/fenster/play/FensterVideoFragment;->showFensterController()V

    .line 74
    return-void
.end method

.method public onStopWithExternalError(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 38
    invoke-direct {p0}, Lcom/malmstein/fenster/play/FensterVideoFragment;->initVideo()V

    .line 39
    return-void
.end method

.method public playExampleVideo()V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->textureView:Lcom/malmstein/fenster/view/FensterVideoView;

    const-string v1, "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setVideo(Ljava/lang/String;I)V

    .line 49
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->textureView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->start()V

    .line 50
    return-void
.end method

.method public setVisibilityListener(Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;)V
    .locals 1
    .param p1, "visibilityListener"    # Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-interface {v0, p1}, Lcom/malmstein/fenster/controller/FensterPlayerController;->setVisibilityListener(Lcom/malmstein/fenster/controller/FensterPlayerControllerVisibilityListener;)V

    .line 54
    return-void
.end method

.method public showFensterController()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->fensterLoadingView:Lcom/malmstein/fenster/view/FensterLoadingView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterLoadingView;->hide()V

    .line 58
    iget-object v0, p0, Lcom/malmstein/fenster/play/FensterVideoFragment;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-interface {v0}, Lcom/malmstein/fenster/controller/FensterPlayerController;->show()V

    .line 59
    return-void
.end method
