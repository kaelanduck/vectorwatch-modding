.class public Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "UpdateWatchActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder;, "Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;Ljava/lang/Object;)V
    .locals 4
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder;, "Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;, "TT;"
    const v3, 0x7f10016e

    const v2, 0x7f100168

    .line 11
    const-string v1, "field \'mUpdateTextScroll\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mUpdateTextScroll\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p2, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    .line 13
    const-string v1, "field \'mSkipButton\' and method \'skipButton\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mSkipButton\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mSkipButton:Landroid/widget/Button;

    .line 15
    new-instance v1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder$1;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder;Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder;, "Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder;, "Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;, "TT;"
    const/4 v0, 0x0

    .line 26
    iput-object v0, p1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    .line 27
    iput-object v0, p1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mSkipButton:Landroid/widget/Button;

    .line 28
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder;, "Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$$ViewBinder;->unbind(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    return-void
.end method
