.class public Lcom/vectorwatch/com/android/vos/update/Crc;
.super Ljava/lang/Object;
.source "Crc.java"


# static fields
.field static final POLYNOMIAL:B = -0x28t

.field static final TOPBIT:B = -0x80t

.field static final WIDTH:B = 0x8t

.field static crcTable:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/16 v0, 0x100

    new-array v0, v0, [B

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/Crc;->crcTable:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static crcFast([BI)B
    .locals 5
    .param p0, "message"    # [B
    .param p1, "nBytes"    # I

    .prologue
    .line 92
    const/4 v2, 0x0

    .line 97
    .local v2, "remainder":B
    const/4 v0, 0x0

    .local v0, "b":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 98
    aget-byte v3, p0, v0

    shr-int/lit8 v4, v2, 0x0

    xor-int/2addr v3, v4

    int-to-byte v1, v3

    .line 99
    .local v1, "data":B
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/Crc;->crcTable:[B

    invoke-static {v1}, Lcom/vectorwatch/com/android/vos/update/Crc;->getValue(B)I

    move-result v4

    aget-byte v3, v3, v4

    shl-int/lit8 v4, v2, 0x8

    xor-int/2addr v3, v4

    int-to-byte v2, v3

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    .end local v1    # "data":B
    :cond_0
    return v2
.end method

.method static crcInit()V
    .locals 4

    .prologue
    .line 18
    const/4 v1, 0x0

    .local v1, "dividend":I
    :goto_0
    const/16 v3, 0x100

    if-ge v1, v3, :cond_2

    .line 22
    shl-int/lit8 v3, v1, 0x0

    int-to-byte v2, v3

    .line 28
    .local v2, "remainder":B
    const/16 v0, 0x8

    .local v0, "bit":I
    :goto_1
    if-lez v0, :cond_1

    .line 32
    and-int/lit8 v3, v2, -0x80

    if-eqz v3, :cond_0

    .line 33
    shl-int/lit8 v3, v2, 0x1

    xor-int/lit8 v3, v3, -0x28

    int-to-byte v2, v3

    .line 28
    :goto_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 35
    :cond_0
    shl-int/lit8 v3, v2, 0x1

    int-to-byte v2, v3

    goto :goto_2

    .line 42
    :cond_1
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/Crc;->crcTable:[B

    aput-byte v2, v3, v1

    .line 18
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 45
    .end local v0    # "bit":I
    .end local v2    # "remainder":B
    :cond_2
    return-void
.end method

.method static crcSlow([BI)B
    .locals 4
    .param p0, "message"    # [B
    .param p1, "nBytes"    # I

    .prologue
    .line 48
    const/4 v2, 0x0

    .line 54
    .local v2, "remainder":B
    const/4 v0, 0x0

    .local v0, "b":I
    :goto_0
    if-ge v0, p1, :cond_2

    .line 58
    aget-byte v3, p0, v0

    shl-int/lit8 v3, v3, 0x0

    xor-int/2addr v3, v2

    int-to-byte v2, v3

    .line 63
    const/16 v1, 0x8

    .local v1, "bit":I
    :goto_1
    if-lez v1, :cond_1

    .line 67
    and-int/lit8 v3, v2, -0x80

    if-eqz v3, :cond_0

    .line 68
    shl-int/lit8 v3, v2, 0x1

    xor-int/lit8 v3, v3, -0x28

    int-to-byte v2, v3

    .line 63
    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 70
    :cond_0
    shl-int/lit8 v3, v2, 0x1

    int-to-byte v2, v3

    goto :goto_2

    .line 54
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    .end local v1    # "bit":I
    :cond_2
    return v2
.end method

.method static getValue(B)I
    .locals 0
    .param p0, "b"    # B

    .prologue
    .line 84
    if-gez p0, :cond_0

    .line 85
    add-int/lit16 p0, p0, 0x100

    .line 87
    .end local p0    # "b":B
    :cond_0
    return p0
.end method
