.class final Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;
.super Landroid/os/AsyncTask;
.source "WatchCloudUpdateEndpoints.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;->confirmUpdateDoneOnWatchWithCpuId(Landroid/content/Context;Ljava/lang/Long;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$model:Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

.field final synthetic val$type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

.field final synthetic val$versionId:Ljava/lang/Long;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Ljava/lang/Long;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    iput-object p3, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$versionId:Ljava/lang/Long;

    iput-object p4, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$model:Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    .line 83
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$context:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;->access$000(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "authToken":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 86
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$context:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->goToLogin(Landroid/content/Context;)V

    .line 87
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 116
    :goto_0
    return-object v4

    .line 90
    :cond_0
    const/4 v1, 0x0

    .line 92
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    const/4 v4, 0x2

    invoke-static {v0, v4}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v1

    .line 95
    :try_start_0
    invoke-static {}, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;->access$100()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UPDATE: Confirm update for VOS with cpu id for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    invoke-virtual {v6}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 96
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$versionId:Ljava/lang/Long;

    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$model:Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

    invoke-interface {v1, v4, v5}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->confirmUpdateWithCpuId(Ljava/lang/Long;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Lcom/vectorwatch/android/models/ServerResponseModel;

    move-result-object v3

    .line 97
    .local v3, "response":Lcom/vectorwatch/android/models/ServerResponseModel;
    invoke-static {}, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;->access$100()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "CLOUD CALLS: confirm_update_with_cpu_id"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 100
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v5, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v4, v5, :cond_1

    .line 101
    const-string v4, "update_id"

    const-wide/16 v6, -0x1

    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$context:Landroid/content/Context;

    invoke-static {v4, v6, v7, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 103
    const-string v4, "flag_confirm_update"

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$context:Landroid/content/Context;

    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_1
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0

    .line 106
    :cond_1
    :try_start_1
    const-string v4, "update_id_bootloader"

    const-wide/16 v6, -0x1

    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$context:Landroid/content/Context;

    invoke-static {v4, v6, v7, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 108
    const-string v4, "flag_confirm_update"

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->val$context:Landroid/content/Context;

    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V
    :try_end_1
    .catch Lretrofit/RetrofitError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 111
    .end local v3    # "response":Lcom/vectorwatch/android/models/ServerResponseModel;
    :catch_0
    move-exception v2

    .line 112
    .local v2, "error":Lretrofit/RetrofitError;
    invoke-static {v2}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    .line 113
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
