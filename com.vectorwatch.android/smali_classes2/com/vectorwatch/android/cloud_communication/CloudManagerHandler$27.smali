.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->syncLogs(Landroid/content/Context;Landroid/accounts/Account;Ljava/util/List;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$listLogs:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/util/List;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 1513
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$listLogs:Ljava/util/List;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1517
    :try_start_0
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$account:Landroid/accounts/Account;

    const-string v6, "Full access"

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1519
    .local v0, "authToken":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1520
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "The user should be authenticated before data is being synced"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1523
    :cond_0
    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 1524
    .local v1, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$listLogs:Ljava/util/List;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v4, v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->syncLogs(Ljava/util/List;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1535
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    :goto_0
    return-void

    .line 1525
    :catch_0
    move-exception v2

    .line 1526
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    const-string v4, "Account"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1527
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$cb:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1528
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 1529
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    const-string v4, "Canceled"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1530
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$cb:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1531
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 1532
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "Unexpected"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1533
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;->val$cb:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
