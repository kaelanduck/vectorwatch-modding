.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getStreamAuthMethod(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$uuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 689
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$uuid:Ljava/lang/String;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$context:Landroid/content/Context;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 693
    :try_start_0
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$account:Landroid/accounts/Account;

    const-string v8, "Full access"

    const/4 v9, 0x1

    invoke-virtual {v6, v7, v8, v9}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 695
    .local v0, "authToken":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-static {v0, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 697
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$uuid:Ljava/lang/String;

    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$context:Landroid/content/Context;

    invoke-static {v6, v7}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v5

    .line 699
    .local v5, "stream":Lcom/vectorwatch/android/models/Stream;
    new-instance v4, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v4}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 700
    .local v4, "model":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 701
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$uuid:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v7

    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v6, v7, v4, v8}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->getStreamAuthMethod(Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 712
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "model":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    .end local v5    # "stream":Lcom/vectorwatch/android/models/Stream;
    :goto_0
    return-void

    .line 702
    :catch_0
    move-exception v2

    .line 703
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    const-string v6, "Account"

    invoke-static {v6, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 704
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$cb:Lretrofit/Callback;

    invoke-interface {v6, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 705
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 706
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    const-string v6, "Canceled"

    invoke-static {v6, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 707
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$cb:Lretrofit/Callback;

    invoke-interface {v6, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 708
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 709
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "Unexpected"

    invoke-static {v6, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 710
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;->val$cb:Lretrofit/Callback;

    invoke-interface {v6, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
