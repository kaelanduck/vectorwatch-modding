.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->subscribeStream(Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPlacementModel;Lcom/vectorwatch/android/models/StreamChannelSettings;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$channelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

.field final synthetic val$uuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/models/StreamPlacementModel;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 993
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$channelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$uuid:Ljava/lang/String;

    iput-object p6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$context:Landroid/content/Context;

    iput-object p7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p8, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 997
    :try_start_0
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$account:Landroid/accounts/Account;

    const-string v3, "Full access"

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v3, v5}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 1000
    .local v6, "authToken":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {v6, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v0

    .line 1002
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1003
    .local v14, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1005
    iget-object v12, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$channelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 1007
    .local v12, "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SUBSCRIBE: on app with uuid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppUuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1008
    invoke-virtual {v12, v14}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setSubscriptions(Ljava/util/List;)V

    .line 1009
    new-instance v4, Lcom/vectorwatch/android/models/ChannelSettingsChange;

    invoke-direct {v4}, Lcom/vectorwatch/android/models/ChannelSettingsChange;-><init>()V

    .line 1010
    .local v4, "changeChannelSettings":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    invoke-virtual {v4, v12}, Lcom/vectorwatch/android/models/ChannelSettingsChange;->setNewChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 1012
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$uuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$context:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v11

    .line 1014
    .local v11, "stream":Lcom/vectorwatch/android/models/Stream;
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 1018
    const/4 v13, 0x0

    .line 1019
    .local v13, "streamResponse":Lcom/vectorwatch/android/models/StreamDownloadResponse;
    new-instance v9, Lcom/google/gson/Gson;

    invoke-direct {v9}, Lcom/google/gson/Gson;-><init>()V

    .line 1020
    .local v9, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v9, v4}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 1021
    .local v10, "settingsToString":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SUBSCRIBE stream - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " auth = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " uuid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$uuid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1022
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$uuid:Ljava/lang/String;

    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "SUBSCRIBED"

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->changeStreamState(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1035
    .end local v0    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "changeChannelSettings":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    .end local v6    # "authToken":Ljava/lang/String;
    .end local v9    # "gson":Lcom/google/gson/Gson;
    .end local v10    # "settingsToString":Ljava/lang/String;
    .end local v11    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v12    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v13    # "streamResponse":Lcom/vectorwatch/android/models/StreamDownloadResponse;
    .end local v14    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    :goto_0
    return-void

    .line 1025
    :catch_0
    move-exception v7

    .line 1026
    .local v7, "e":Landroid/accounts/AuthenticatorException;
    const-string v1, "Account"

    invoke-static {v1, v7}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v8

    .line 1027
    .local v8, "error":Lretrofit/RetrofitError;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$cb:Lretrofit/Callback;

    invoke-interface {v1, v8}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1028
    .end local v7    # "e":Landroid/accounts/AuthenticatorException;
    .end local v8    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v7

    .line 1029
    .local v7, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "Canceled"

    invoke-static {v1, v7}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v8

    .line 1030
    .restart local v8    # "error":Lretrofit/RetrofitError;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$cb:Lretrofit/Callback;

    invoke-interface {v1, v8}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1031
    .end local v7    # "e":Landroid/accounts/OperationCanceledException;
    .end local v8    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v7

    .line 1032
    .local v7, "e":Ljava/lang/Exception;
    const-string v1, "Unexpected"

    invoke-static {v1, v7}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v8

    .line 1033
    .restart local v8    # "error":Lretrofit/RetrofitError;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;->val$cb:Lretrofit/Callback;

    invoke-interface {v1, v8}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
