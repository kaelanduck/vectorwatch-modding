.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->syncOrder(Landroid/content/Context;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 223
    :try_start_0
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$account:Landroid/accounts/Account;

    const-string v8, "Full access"

    const/4 v9, 0x1

    invoke-virtual {v6, v7, v8, v9}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, "authToken":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-static {v0, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 227
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$context:Landroid/content/Context;

    .line 228
    invoke-static {v6}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppUuidsInRunningOrder(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 230
    .local v4, "runningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$context:Landroid/content/Context;

    invoke-static {v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "round"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 231
    sget-object v6, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/WatchType;->name()Ljava/lang/String;

    move-result-object v5

    .line 236
    .local v5, "watchType":Ljava/lang/String;
    :goto_0
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v5, v4, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->syncOrderToCloud(Ljava/lang/String;Ljava/util/ArrayList;Lretrofit/Callback;)V

    .line 247
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "runningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "watchType":Ljava/lang/String;
    :goto_1
    return-void

    .line 233
    .restart local v0    # "authToken":Ljava/lang/String;
    .restart local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .restart local v4    # "runningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    sget-object v6, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/WatchType;->name()Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v5

    .restart local v5    # "watchType":Ljava/lang/String;
    goto :goto_0

    .line 237
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "runningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "watchType":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 238
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    const-string v6, "Account"

    invoke-static {v6, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 239
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$cb:Lretrofit/Callback;

    invoke-interface {v6, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_1

    .line 240
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 241
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    const-string v6, "Canceled"

    invoke-static {v6, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 242
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$cb:Lretrofit/Callback;

    invoke-interface {v6, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_1

    .line 243
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 244
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "Unexpected"

    invoke-static {v6, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 245
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;->val$cb:Lretrofit/Callback;

    invoke-interface {v6, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_1
.end method
