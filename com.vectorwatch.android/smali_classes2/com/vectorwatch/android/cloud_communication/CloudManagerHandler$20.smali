.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getDefaultApps(Landroid/content/Context;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 1172
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 1176
    :try_start_0
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v10, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$account:Landroid/accounts/Account;

    const-string v11, "Full access"

    const/4 v12, 0x1

    invoke-virtual {v9, v10, v11, v12}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 1178
    .local v1, "authToken":Ljava/lang/String;
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$context:Landroid/content/Context;

    invoke-static {v9}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 1181
    .local v7, "shape":Ljava/lang/String;
    invoke-static {v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$300(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1183
    .local v0, "andFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Filter;>;"
    new-instance v6, Lcom/vectorwatch/android/models/CloudRequestModel;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v6, v9, v10, v11, v0}, Lcom/vectorwatch/android/models/CloudRequestModel;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;)V

    .line 1184
    .local v6, "model":Lcom/vectorwatch/android/models/CloudRequestModel;
    new-instance v5, Lcom/google/gson/Gson;

    invoke-direct {v5}, Lcom/google/gson/Gson;-><init>()V

    .line 1186
    .local v5, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v5, v6}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1187
    .local v8, "stuff":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Default filters sent to cloud: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1190
    const/4 v9, 0x1

    invoke-static {v1, v9}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v2

    .line 1192
    .local v2, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    invoke-static {v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 1193
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v2, v6, v9}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->getAppDefaults(Lcom/vectorwatch/android/models/CloudRequestModel;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1204
    .end local v0    # "andFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Filter;>;"
    .end local v1    # "authToken":Ljava/lang/String;
    .end local v2    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v5    # "gson":Lcom/google/gson/Gson;
    .end local v6    # "model":Lcom/vectorwatch/android/models/CloudRequestModel;
    .end local v7    # "shape":Ljava/lang/String;
    .end local v8    # "stuff":Ljava/lang/String;
    :goto_0
    return-void

    .line 1194
    :catch_0
    move-exception v3

    .line 1195
    .local v3, "e":Landroid/accounts/AuthenticatorException;
    const-string v9, "Account"

    invoke-static {v9, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 1196
    .local v4, "error":Lretrofit/RetrofitError;
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$cb:Lretrofit/Callback;

    invoke-interface {v9, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1197
    .end local v3    # "e":Landroid/accounts/AuthenticatorException;
    .end local v4    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v3

    .line 1198
    .local v3, "e":Landroid/accounts/OperationCanceledException;
    const-string v9, "Canceled"

    invoke-static {v9, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 1199
    .restart local v4    # "error":Lretrofit/RetrofitError;
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$cb:Lretrofit/Callback;

    invoke-interface {v9, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1200
    .end local v3    # "e":Landroid/accounts/OperationCanceledException;
    .end local v4    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v3

    .line 1201
    .local v3, "e":Ljava/lang/Exception;
    const-string v9, "Unexpected"

    invoke-static {v9, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 1202
    .restart local v4    # "error":Lretrofit/RetrofitError;
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;->val$cb:Lretrofit/Callback;

    invoke-interface {v9, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
