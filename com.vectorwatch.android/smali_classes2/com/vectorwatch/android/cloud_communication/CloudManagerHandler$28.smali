.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->syncStreamTtlExpiredToCloud(Landroid/content/Context;Ljava/util/List;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$streamLogItemList:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/util/List;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;)V
    .locals 0

    .prologue
    .line 1564
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;->val$streamLogItemList:Ljava/util/List;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 1568
    :try_start_0
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;->val$account:Landroid/accounts/Account;

    const-string v7, "Full access"

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1570
    .local v0, "authToken":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-static {v0, v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 1572
    .local v1, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    new-instance v4, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v4}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 1573
    .local v4, "model":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 1574
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$200(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Ljava/lang/String;

    move-result-object v2

    .line 1576
    .local v2, "defaultHeader":Ljava/lang/String;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;->val$streamLogItemList:Ljava/util/List;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v2, v5, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->syncStreamLogExpired(Ljava/lang/String;Ljava/util/List;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1584
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v2    # "defaultHeader":Ljava/lang/String;
    .end local v4    # "model":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    :goto_0
    return-void

    .line 1577
    :catch_0
    move-exception v3

    .line 1578
    .local v3, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v3}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 1579
    .end local v3    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v3

    .line 1580
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1581
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 1582
    .local v3, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v3}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_0
.end method
