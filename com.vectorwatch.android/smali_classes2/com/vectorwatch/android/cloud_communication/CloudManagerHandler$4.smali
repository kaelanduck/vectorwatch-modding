.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->downloadAppFromLoadedAppList(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$uuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$uuid:Ljava/lang/String;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 285
    :try_start_0
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$account:Landroid/accounts/Account;

    const-string v8, "Full access"

    const/4 v9, 0x1

    invoke-virtual {v6, v7, v8, v9}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 287
    .local v0, "authToken":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-static {v0, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 289
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$context:Landroid/content/Context;

    sget-object v7, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-static {v6, v7}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppsWithGivenState(Landroid/content/Context;Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)Ljava/util/ArrayList;

    move-result-object v5

    .line 291
    .local v5, "runningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/16 v7, 0xc

    if-ge v6, v7, :cond_0

    .line 295
    new-instance v4, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v4}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 297
    .local v4, "extraInfo":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 299
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$uuid:Ljava/lang/String;

    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v6, v4, v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->getApp(Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Lretrofit/Callback;)V

    .line 300
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "CLOUD CALL - APP to RUNNING - uuid = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$uuid:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ". Details "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "2.0.2"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 314
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "extraInfo":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    .end local v5    # "runningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    :goto_0
    return-void

    .line 302
    .restart local v0    # "authToken":Ljava/lang/String;
    .restart local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .restart local v5    # "runningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    :cond_0
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$cb:Lretrofit/Callback;

    const-string v7, "Fail"

    new-instance v8, Ljava/lang/Throwable;

    invoke-direct {v8}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v7, v8}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v7

    invoke-interface {v6, v7}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 304
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v5    # "runningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    :catch_0
    move-exception v2

    .line 305
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    const-string v6, "Account"

    invoke-static {v6, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 306
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$cb:Lretrofit/Callback;

    invoke-interface {v6, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 307
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 308
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    const-string v6, "Canceled"

    invoke-static {v6, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 309
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$cb:Lretrofit/Callback;

    invoke-interface {v6, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 310
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 311
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "Unexpected"

    invoke-static {v6, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 312
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;->val$cb:Lretrofit/Callback;

    invoke-interface {v6, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
