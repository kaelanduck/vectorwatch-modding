.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5$1;
.super Ljava/lang/Object;
.source "SyncUpdatesService.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->success(Lcom/vectorwatch/android/models/DefaultStreamsModel;Lretrofit/client/Response;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;

    .prologue
    .line 480
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5$1;->this$1:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 502
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5$1;->this$1:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;

    iget-object v0, v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-static {v0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$200(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Lretrofit/RetrofitError;)V

    .line 503
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;Lretrofit/client/Response;)V
    .locals 5
    .param p1, "retrievedFullAppsContainer"    # Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    const/4 v3, 0x0

    .line 483
    iget-object v0, p1, Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;->data:Lcom/vectorwatch/android/models/RetrievedFullAppsContainer$FullCloudAppsList;

    iget-object v0, v0, Lcom/vectorwatch/android/models/RetrievedFullAppsContainer$FullCloudAppsList;->apps:Ljava/util/List;

    sput-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    .line 485
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sput v0, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    .line 487
    sput v3, Lcom/vectorwatch/android/VectorApplication;->sCorrectlyInstalledApps:I

    .line 488
    sput v3, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    .line 490
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/InstallAppEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v2}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 492
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/RefreshWatchmakerUi;

    invoke-direct {v1}, Lcom/vectorwatch/android/events/RefreshWatchmakerUi;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 498
    :goto_0
    return-void

    .line 494
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vectorwatch/android/VectorApplication;->hasError:Z

    .line 495
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/InstallAppEvent;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5$1;->this$1:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;

    iget-object v3, v3, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .line 496
    invoke-virtual {v3}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090116

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 495
    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 480
    check-cast p1, Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5$1;->success(Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;Lretrofit/client/Response;)V

    return-void
.end method
