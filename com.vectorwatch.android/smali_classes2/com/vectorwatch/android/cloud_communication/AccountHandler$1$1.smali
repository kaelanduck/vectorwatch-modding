.class Lcom/vectorwatch/android/cloud_communication/AccountHandler$1$1;
.super Ljava/lang/Object;
.source "AccountHandler.java"

# interfaces
.implements Lretrofit/RequestInterceptor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;

.field final synthetic val$authToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1$1;->this$0:Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1$1;->val$authToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public intercept(Lretrofit/RequestInterceptor$RequestFacade;)V
    .locals 2
    .param p1, "request"    # Lretrofit/RequestInterceptor$RequestFacade;

    .prologue
    .line 57
    const-string v0, "Content-Type"

    const-string v1, "application/json"

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1$1;->val$authToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1$1;->val$authToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "No token saved in shared preferences"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 63
    :cond_1
    const-string v0, "Authorization"

    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1$1;->val$authToken:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method
