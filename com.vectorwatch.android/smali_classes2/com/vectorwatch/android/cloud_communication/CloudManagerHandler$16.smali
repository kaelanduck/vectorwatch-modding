.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->updateAccountInfo(Landroid/content/Context;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 937
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 941
    :try_start_0
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$account:Landroid/accounts/Account;

    const-string v7, "Full access"

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 944
    .local v0, "authToken":Ljava/lang/String;
    new-instance v4, Lcom/vectorwatch/android/models/UserModel;

    invoke-direct {v4}, Lcom/vectorwatch/android/models/UserModel;-><init>()V

    .line 945
    .local v4, "user":Lcom/vectorwatch/android/models/UserModel;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$context:Landroid/content/Context;

    invoke-static {v5}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->getEndpointArn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/models/UserModel;->setEndpointArn(Ljava/lang/String;)V

    .line 947
    const/4 v5, 0x1

    invoke-static {v0, v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 948
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v4, v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->updateInfo(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 959
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "user":Lcom/vectorwatch/android/models/UserModel;
    :goto_0
    return-void

    .line 949
    :catch_0
    move-exception v2

    .line 950
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    const-string v5, "Account"

    invoke-static {v5, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 951
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 952
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 953
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    const-string v5, "Canceled"

    invoke-static {v5, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 954
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 955
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 956
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "Unexpected"

    invoke-static {v5, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 957
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
