.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->uploadUserContextualSettings(Landroid/content/Context;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 1737
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 1742
    :try_start_0
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$account:Landroid/accounts/Account;

    const-string v7, "Full access"

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1743
    .local v0, "authToken":Ljava/lang/String;
    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 1744
    .local v1, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/vectorwatch/android/utils/Helpers;->packContextualSettingsForUpload(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 1746
    .local v2, "contextualItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/ContextualItem;>;"
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v2, v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->syncContextualInCloud(Ljava/util/List;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1758
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v2    # "contextualItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/ContextualItem;>;"
    :goto_0
    return-void

    .line 1748
    :catch_0
    move-exception v3

    .line 1749
    .local v3, "e":Landroid/accounts/AuthenticatorException;
    const-string v5, "Account"

    invoke-static {v5, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 1750
    .local v4, "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1751
    .end local v3    # "e":Landroid/accounts/AuthenticatorException;
    .end local v4    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v3

    .line 1752
    .local v3, "e":Landroid/accounts/OperationCanceledException;
    const-string v5, "Canceled"

    invoke-static {v5, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 1753
    .restart local v4    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1754
    .end local v3    # "e":Landroid/accounts/OperationCanceledException;
    .end local v4    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v3

    .line 1755
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "Unexpected"

    invoke-static {v5, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 1756
    .restart local v4    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
