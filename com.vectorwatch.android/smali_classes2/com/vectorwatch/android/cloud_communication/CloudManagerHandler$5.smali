.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->downloadStream(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$uuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$uuid:Ljava/lang/String;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 348
    :try_start_0
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$account:Landroid/accounts/Account;

    const-string v7, "Full access"

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 350
    .local v0, "authToken":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-static {v0, v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v2

    .line 352
    .local v2, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    new-instance v1, Lcom/vectorwatch/android/models/ChannelSettingsChange;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/ChannelSettingsChange;-><init>()V

    .line 353
    .local v1, "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 354
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$uuid:Ljava/lang/String;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v2, v5, v1, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->downloadStream(Ljava/lang/String;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 365
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    .end local v2    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    :goto_0
    return-void

    .line 355
    :catch_0
    move-exception v3

    .line 356
    .local v3, "e":Landroid/accounts/AuthenticatorException;
    const-string v5, "Account"

    invoke-static {v5, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 357
    .local v4, "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 358
    .end local v3    # "e":Landroid/accounts/AuthenticatorException;
    .end local v4    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v3

    .line 359
    .local v3, "e":Landroid/accounts/OperationCanceledException;
    const-string v5, "Canceled"

    invoke-static {v5, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 360
    .restart local v4    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 361
    .end local v3    # "e":Landroid/accounts/OperationCanceledException;
    .end local v4    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v3

    .line 362
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "Unexpected"

    invoke-static {v5, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 363
    .restart local v4    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
