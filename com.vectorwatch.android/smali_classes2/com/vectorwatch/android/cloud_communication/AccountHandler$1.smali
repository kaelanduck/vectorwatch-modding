.class final Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;
.super Ljava/lang/Object;
.source "AccountHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/AccountHandler;->registerWatch(Ljava/lang/String;Landroid/accounts/Account;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$nameOfBondedWatch:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$nameOfBondedWatch:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 48
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$context:Landroid/content/Context;

    invoke-static {v9}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 51
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    :try_start_0
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$account:Landroid/accounts/Account;

    const-string v10, "Full access"

    const/4 v11, 0x1

    invoke-virtual {v0, v9, v10, v11}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "authToken":Ljava/lang/String;
    new-instance v9, Lretrofit/RestAdapter$Builder;

    invoke-direct {v9}, Lretrofit/RestAdapter$Builder;-><init>()V

    new-instance v10, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1$1;

    invoke-direct {v10, p0, v1}, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1$1;-><init>(Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Lretrofit/RestAdapter$Builder;->setRequestInterceptor(Lretrofit/RequestInterceptor;)Lretrofit/RestAdapter$Builder;

    move-result-object v2

    .line 66
    .local v2, "builder":Lretrofit/RestAdapter$Builder;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v9

    invoke-virtual {v9}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v6

    .line 67
    .local v6, "restAdapter":Lretrofit/RestAdapter;
    const-class v9, Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;

    invoke-virtual {v6, v9}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;

    .line 70
    .local v7, "syncService":Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    const/4 v8, 0x0

    .line 71
    .local v8, "token":Ljava/lang/String;
    const/4 v5, 0x0

    .line 72
    .local v5, "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Trying to register watch with name = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$nameOfBondedWatch:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " account = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$account:Landroid/accounts/Account;

    iget-object v11, v11, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "CPU id "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$context:Landroid/content/Context;

    .line 73
    invoke-static {v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchOldCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 72
    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 75
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$context:Landroid/content/Context;

    invoke-static {v9}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchOldCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_0

    .line 76
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    const-string v10, "Can\'t register watch, missing CPU id"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_3

    .line 112
    .end local v1    # "authToken":Ljava/lang/String;
    .end local v2    # "builder":Lretrofit/RestAdapter$Builder;
    .end local v5    # "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    .end local v6    # "restAdapter":Lretrofit/RestAdapter;
    .end local v7    # "syncService":Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    .end local v8    # "token":Ljava/lang/String;
    :goto_0
    return-void

    .line 81
    .restart local v1    # "authToken":Ljava/lang/String;
    .restart local v2    # "builder":Lretrofit/RestAdapter$Builder;
    .restart local v5    # "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    .restart local v6    # "restAdapter":Lretrofit/RestAdapter;
    .restart local v7    # "syncService":Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    .restart local v8    # "token":Ljava/lang/String;
    :cond_0
    :try_start_1
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$nameOfBondedWatch:Ljava/lang/String;

    new-instance v10, Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

    iget-object v11, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$context:Landroid/content/Context;

    .line 82
    invoke-static {v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchOldCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$context:Landroid/content/Context;

    .line 83
    invoke-static {v12}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {v10, v11, v12, v13}, Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-interface {v7, v9, v10}, Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;->registerWatch(Ljava/lang/String;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Lcom/vectorwatch/android/models/ServerResponseModel;

    move-result-object v5

    .line 84
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    const-string v10, "CLOUD CALLS: register_watch"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ServerResponseModel;->getToken()Ljava/lang/String;
    :try_end_1
    .catch Lretrofit/RetrofitError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v8

    .line 90
    :goto_1
    if-eqz v8, :cond_1

    :try_start_2
    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 91
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Did not manage to register watch. Token was: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 104
    .end local v1    # "authToken":Ljava/lang/String;
    .end local v2    # "builder":Lretrofit/RestAdapter$Builder;
    .end local v5    # "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    .end local v6    # "restAdapter":Lretrofit/RestAdapter;
    .end local v7    # "syncService":Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    .end local v8    # "token":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 105
    .local v3, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v3}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 86
    .end local v3    # "e":Landroid/accounts/OperationCanceledException;
    .restart local v1    # "authToken":Ljava/lang/String;
    .restart local v2    # "builder":Lretrofit/RestAdapter$Builder;
    .restart local v5    # "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    .restart local v6    # "restAdapter":Lretrofit/RestAdapter;
    .restart local v7    # "syncService":Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    .restart local v8    # "token":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 87
    .local v4, "error":Lretrofit/RetrofitError;
    :try_start_3
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    .line 106
    .end local v1    # "authToken":Ljava/lang/String;
    .end local v2    # "builder":Lretrofit/RestAdapter$Builder;
    .end local v4    # "error":Lretrofit/RetrofitError;
    .end local v5    # "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    .end local v6    # "restAdapter":Lretrofit/RestAdapter;
    .end local v7    # "syncService":Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    .end local v8    # "token":Ljava/lang/String;
    :catch_2
    move-exception v3

    .line 107
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 94
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v1    # "authToken":Ljava/lang/String;
    .restart local v2    # "builder":Lretrofit/RestAdapter$Builder;
    .restart local v5    # "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    .restart local v6    # "restAdapter":Lretrofit/RestAdapter;
    .restart local v7    # "syncService":Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    .restart local v8    # "token":Ljava/lang/String;
    :cond_2
    :try_start_4
    iget-object v9, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$account:Landroid/accounts/Account;

    const-string v10, "com.vectorwatch.android"

    invoke-virtual {v0, v9, v10, v8}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v9, "bonded_watch_dirty"

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$context:Landroid/content/Context;

    invoke-static {v9, v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 98
    const-string v9, "flag_get_cpu_id"

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$context:Landroid/content/Context;

    invoke-static {v9, v10, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 101
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Watch registered. Saved update token: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " .Serial was: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;->val$nameOfBondedWatch:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/accounts/OperationCanceledException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 108
    .end local v1    # "authToken":Ljava/lang/String;
    .end local v2    # "builder":Lretrofit/RestAdapter$Builder;
    .end local v5    # "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    .end local v6    # "restAdapter":Lretrofit/RestAdapter;
    .end local v7    # "syncService":Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    .end local v8    # "token":Ljava/lang/String;
    :catch_3
    move-exception v3

    .line 109
    .local v3, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v3}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto/16 :goto_0
.end method
