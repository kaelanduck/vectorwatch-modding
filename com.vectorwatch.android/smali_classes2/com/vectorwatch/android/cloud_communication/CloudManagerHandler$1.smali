.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$1;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->checkCloudStatus(Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cb:Lretrofit/Callback;


# direct methods
.method constructor <init>(Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$1;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 132
    :try_start_0
    const-string v3, ""

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v0

    .line 134
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$1;->val$cb:Lretrofit/Callback;

    invoke-interface {v0, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->checkCloudStatus(Lretrofit/Callback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    .end local v0    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v1

    .line 136
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Unexpected"

    invoke-static {v3, v1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v2

    .line 137
    .local v2, "error":Lretrofit/RetrofitError;
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$1;->val$cb:Lretrofit/Callback;

    invoke-interface {v3, v2}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
