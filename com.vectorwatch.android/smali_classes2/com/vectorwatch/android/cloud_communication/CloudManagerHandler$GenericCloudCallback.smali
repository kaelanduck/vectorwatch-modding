.class Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;
.super Ljava/lang/Object;
.source "CloudManagerHandler.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GenericCloudCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<TT;>;"
    }
.end annotation


# instance fields
.field callback:Lretrofit/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit/Callback",
            "<TT;>;"
        }
    .end annotation
.end field

.field context:Landroid/content/Context;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 102
    .local p0, "this":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 114
    .local p0, "this":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<TT;>;"
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    invoke-virtual {v0}, Lretrofit/client/Response;->getStatus()I

    move-result v0

    if-nez v0, :cond_1

    .line 115
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->refreshOfflineCloudStatus(Z)V

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    invoke-interface {v0, p1}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 119
    return-void
.end method

.method public success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 1
    .param p2, "response"    # Lretrofit/client/Response;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lretrofit/client/Response;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "this":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<TT;>;"
    .local p1, "t":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->refreshOfflineCloudStatus(Z)V

    .line 109
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    invoke-interface {v0, p1, p2}, Lretrofit/Callback;->success(Ljava/lang/Object;Lretrofit/client/Response;)V

    .line 110
    return-void
.end method
