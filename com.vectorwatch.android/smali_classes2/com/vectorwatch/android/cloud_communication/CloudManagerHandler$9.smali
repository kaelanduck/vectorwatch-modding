.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->addRating(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$ratingInfo:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;

.field final synthetic val$type:Ljava/lang/String;

.field final synthetic val$uuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 549
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$type:Ljava/lang/String;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$uuid:Ljava/lang/String;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$ratingInfo:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;

    iput-object p6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 553
    :try_start_0
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$account:Landroid/accounts/Account;

    const-string v3, "Full access"

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v3, v5}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 555
    .local v6, "authToken":Ljava/lang/String;
    const/4 v1, 0x2

    invoke-static {v6, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v0

    .line 556
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    new-instance v9, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v9}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 557
    .local v9, "model":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v9}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 558
    invoke-static {v9}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$200(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Ljava/lang/String;

    move-result-object v4

    .line 560
    .local v4, "defaultHeader":Ljava/lang/String;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$type:Ljava/lang/String;

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$uuid:Ljava/lang/String;

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$ratingInfo:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->addRating(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;Ljava/lang/String;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 571
    .end local v0    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "defaultHeader":Ljava/lang/String;
    .end local v6    # "authToken":Ljava/lang/String;
    .end local v9    # "model":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    :goto_0
    return-void

    .line 561
    :catch_0
    move-exception v7

    .line 562
    .local v7, "e":Landroid/accounts/AuthenticatorException;
    const-string v1, "Account"

    invoke-static {v1, v7}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v8

    .line 563
    .local v8, "error":Lretrofit/RetrofitError;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$cb:Lretrofit/Callback;

    invoke-interface {v1, v8}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 564
    .end local v7    # "e":Landroid/accounts/AuthenticatorException;
    .end local v8    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v7

    .line 565
    .local v7, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "Canceled"

    invoke-static {v1, v7}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v8

    .line 566
    .restart local v8    # "error":Lretrofit/RetrofitError;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$cb:Lretrofit/Callback;

    invoke-interface {v1, v8}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 567
    .end local v7    # "e":Landroid/accounts/OperationCanceledException;
    .end local v8    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v7

    .line 568
    .local v7, "e":Ljava/lang/Exception;
    const-string v1, "Unexpected"

    invoke-static {v1, v7}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v8

    .line 569
    .restart local v8    # "error":Lretrofit/RetrofitError;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;->val$cb:Lretrofit/Callback;

    invoke-interface {v1, v8}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
