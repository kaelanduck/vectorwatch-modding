.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->uploadUserSettings(Landroid/content/Context;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 1780
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 1784
    :try_start_0
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$account:Landroid/accounts/Account;

    const-string v7, "Full access"

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1785
    .local v0, "authToken":Ljava/lang/String;
    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 1786
    .local v1, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$context:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/vectorwatch/android/utils/Helpers;->packUserSettingsForUpload(Landroid/content/Context;)Lcom/vectorwatch/android/models/LocalSettingsModel;

    move-result-object v4

    .line 1787
    .local v4, "localSettingsModel":Lcom/vectorwatch/android/models/LocalSettingsModel;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "UPDATE SETTINGS: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Lcom/google/gson/Gson;

    invoke-direct {v7}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v7, v4}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1788
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v4, v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->putSettingsInCloud(Lcom/vectorwatch/android/models/LocalSettingsModel;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1800
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "localSettingsModel":Lcom/vectorwatch/android/models/LocalSettingsModel;
    :goto_0
    return-void

    .line 1790
    :catch_0
    move-exception v2

    .line 1791
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    const-string v5, "Account"

    invoke-static {v5, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1792
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1793
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 1794
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    const-string v5, "Canceled"

    invoke-static {v5, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1795
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1796
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 1797
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "Unexpected"

    invoke-static {v5, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1798
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
