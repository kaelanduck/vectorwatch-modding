.class Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator$1;
.super Ljava/lang/Object;
.source "Authenticator.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/LoginResponseModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator$1;->this$0:Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 2
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator$1;->this$0:Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;

    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->access$100(Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;)Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "AUTHENTICATOR: Error at login."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/LoginResponseModel;Lretrofit/client/Response;)V
    .locals 1
    .param p1, "loginResponseModel"    # Lcom/vectorwatch/android/models/LoginResponseModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 98
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/LoginResponseModel;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 99
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 95
    check-cast p1, Lcom/vectorwatch/android/models/LoginResponseModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator$1;->success(Lcom/vectorwatch/android/models/LoginResponseModel;Lretrofit/client/Response;)V

    return-void
.end method
