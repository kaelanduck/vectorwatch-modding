.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->uploadWatchFile(Landroid/content/Context;Ljava/io/File;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/io/File;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 2034
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$file:Ljava/io/File;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 2038
    :try_start_0
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$account:Landroid/accounts/Account;

    const-string v9, "Full access"

    const/4 v10, 0x1

    invoke-virtual {v7, v8, v9, v10}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 2041
    .local v0, "authToken":Ljava/lang/String;
    new-instance v5, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v5}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 2043
    .local v5, "extraInfo":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 2044
    invoke-static {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$200(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Ljava/lang/String;

    move-result-object v2

    .line 2046
    .local v2, "defaultHeader":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2047
    invoke-static {v0, v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterfaceForMultipart(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 2050
    .local v1, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    new-instance v6, Lretrofit/mime/TypedFile;

    const-string v7, "multipart/form-data"

    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$file:Ljava/io/File;

    invoke-direct {v6, v7, v8}, Lretrofit/mime/TypedFile;-><init>(Ljava/lang/String;Ljava/io/File;)V

    .line 2052
    .local v6, "typedFile":Lretrofit/mime/TypedFile;
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v6, v2, v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->uploadWatchFile(Lretrofit/mime/TypedFile;Ljava/lang/String;Lretrofit/Callback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2057
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v2    # "defaultHeader":Ljava/lang/String;
    .end local v5    # "extraInfo":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    .end local v6    # "typedFile":Lretrofit/mime/TypedFile;
    :goto_0
    return-void

    .line 2053
    :catch_0
    move-exception v3

    .line 2054
    .local v3, "e":Ljava/lang/Exception;
    const-string v7, "Unexpected"

    invoke-static {v7, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 2055
    .local v4, "error":Lretrofit/RetrofitError;
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;->val$cb:Lretrofit/Callback;

    invoke-interface {v7, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
