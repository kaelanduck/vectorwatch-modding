.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$41;
.super Ljava/lang/Object;
.source "CloudManagerHandler.java"

# interfaces
.implements Lretrofit/RequestInterceptor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$authToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2169
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$41;->val$authToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public intercept(Lretrofit/RequestInterceptor$RequestFacade;)V
    .locals 2
    .param p1, "request"    # Lretrofit/RequestInterceptor$RequestFacade;

    .prologue
    .line 2173
    const-string v0, "Content-Type"

    const-string v1, "application/json"

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2175
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$41;->val$authToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$41;->val$authToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2176
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "No token saved in shared preferences"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2181
    :cond_1
    const-string v0, "Authorization"

    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$41;->val$authToken:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 2182
    return-void
.end method
