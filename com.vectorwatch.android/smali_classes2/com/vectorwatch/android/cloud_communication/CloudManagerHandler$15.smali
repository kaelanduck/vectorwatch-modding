.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getShareLink(Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/models/ShareValuesItem;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$shareValuesItem:Lcom/vectorwatch/android/models/ShareValuesItem;

.field final synthetic val$type:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Lcom/vectorwatch/android/models/ShareValuesItem;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 894
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$type:Ljava/lang/String;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$shareValuesItem:Lcom/vectorwatch/android/models/ShareValuesItem;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 898
    :try_start_0
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$account:Landroid/accounts/Account;

    const-string v6, "Full access"

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 900
    .local v0, "authToken":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-static {v0, v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 902
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$type:Ljava/lang/String;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$shareValuesItem:Lcom/vectorwatch/android/models/ShareValuesItem;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v4, v5, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->getShareLink(Ljava/lang/String;Lcom/vectorwatch/android/models/ShareValuesItem;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 913
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    :goto_0
    return-void

    .line 903
    :catch_0
    move-exception v2

    .line 904
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    const-string v4, "Account"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 905
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$cb:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 906
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 907
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    const-string v4, "Canceled"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 908
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$cb:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 909
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 910
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "Unexpected"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 911
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;->val$cb:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
