.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->syncDefaultsToCloud(Landroid/content/Context;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;)V
    .locals 0

    .prologue
    .line 1383
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 1387
    :try_start_0
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;->val$account:Landroid/accounts/Account;

    const-string v8, "Full access"

    const/4 v9, 0x1

    invoke-virtual {v6, v7, v8, v9}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 1389
    .local v1, "authToken":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-static {v1, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v2

    .line 1391
    .local v2, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;->val$context:Landroid/content/Context;

    invoke-static {v6}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentWatchAppsState(Landroid/content/Context;)Lcom/vectorwatch/android/models/UpdateDefaultsModel;

    move-result-object v5

    .line 1393
    .local v5, "updateDefaultsModel":Lcom/vectorwatch/android/models/UpdateDefaultsModel;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;->val$context:Landroid/content/Context;

    invoke-static {v6}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppIdsInRunningOrder(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 1394
    .local v4, "runningApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1395
    .local v0, "appId":Ljava/lang/Integer;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SETUP: running apps = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 1401
    .end local v0    # "appId":Ljava/lang/Integer;
    .end local v1    # "authToken":Ljava/lang/String;
    .end local v2    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "runningApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v5    # "updateDefaultsModel":Lcom/vectorwatch/android/models/UpdateDefaultsModel;
    :catch_0
    move-exception v3

    .line 1402
    .local v3, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v3}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 1408
    .end local v3    # "e":Landroid/accounts/OperationCanceledException;
    :goto_1
    return-void

    .line 1397
    .restart local v1    # "authToken":Ljava/lang/String;
    .restart local v2    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .restart local v4    # "runningApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v5    # "updateDefaultsModel":Lcom/vectorwatch/android/models/UpdateDefaultsModel;
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SETUP: What gets synced as defaults = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1399
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v6

    const-string v7, "SETUP: Trying to sync defaults to cloud."

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1400
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v2, v5, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->updateDefaults(Lcom/vectorwatch/android/models/UpdateDefaultsModel;Lretrofit/Callback;)V
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 1403
    .end local v1    # "authToken":Ljava/lang/String;
    .end local v2    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "runningApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v5    # "updateDefaultsModel":Lcom/vectorwatch/android/models/UpdateDefaultsModel;
    :catch_1
    move-exception v3

    .line 1404
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1405
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 1406
    .local v3, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v3}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1
.end method
