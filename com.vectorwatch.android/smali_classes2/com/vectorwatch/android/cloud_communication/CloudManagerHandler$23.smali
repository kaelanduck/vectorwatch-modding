.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->setAuthCredentials(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lretrofit/Callback;

.field final synthetic val$cb:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$credentials:Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;

.field final synthetic val$element:Ljava/lang/String;

.field final synthetic val$uuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 1334
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$credentials:Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$element:Ljava/lang/String;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$uuid:Ljava/lang/String;

    iput-object p6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$cb:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$callback:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1338
    :try_start_0
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$credentials:Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;

    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 1339
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$account:Landroid/accounts/Account;

    const-string v6, "Full access"

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1341
    .local v0, "authToken":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-static {v0, v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 1343
    .local v1, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$element:Ljava/lang/String;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$uuid:Ljava/lang/String;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$credentials:Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;

    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$cb:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v4, v5, v6, v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->setAuthCredetentialsForStream(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1357
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    :goto_0
    return-void

    .line 1344
    :catch_0
    move-exception v2

    .line 1345
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v2}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 1346
    const-string v4, "Unexpected"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1347
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$callback:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1348
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 1349
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 1350
    const-string v4, "Unexpected"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1351
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$callback:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1352
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 1353
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v2}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    .line 1354
    const-string v4, "Unexpected"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1355
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;->val$callback:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
