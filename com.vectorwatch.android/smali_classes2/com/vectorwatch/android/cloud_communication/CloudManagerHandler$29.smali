.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->confirmUpdateDoneOnWatchWithCpuId(Landroid/content/Context;Ljava/lang/Long;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$model:Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

.field final synthetic val$type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

.field final synthetic val$versionId:Ljava/lang/Long;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Ljava/lang/Long;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 1610
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$versionId:Ljava/lang/Long;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$model:Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

    iput-object p6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1615
    :try_start_0
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$account:Landroid/accounts/Account;

    const-string v6, "Full access"

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1616
    .local v0, "authToken":Ljava/lang/String;
    const/4 v4, 0x2

    invoke-static {v0, v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 1618
    .local v1, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$100()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UPDATE: Confirm update for VOS with cpu id for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    invoke-virtual {v6}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 1619
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$versionId:Ljava/lang/Long;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$model:Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v4, v5, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->confirmUpdateWithCpuId(Ljava/lang/Long;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1630
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    :goto_0
    return-void

    .line 1620
    :catch_0
    move-exception v2

    .line 1621
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    const-string v4, "Account"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1622
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$cb:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1623
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 1624
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    const-string v4, "Canceled"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1625
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$cb:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1626
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 1627
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "Unexpected"

    invoke-static {v4, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 1628
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;->val$cb:Lretrofit/Callback;

    invoke-interface {v4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
