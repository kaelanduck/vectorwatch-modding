.class public Lcom/vectorwatch/android/cloud_communication/syncadapter/CloudErrorHandler;
.super Ljava/lang/Object;
.source "CloudErrorHandler.java"

# interfaces
.implements Lretrofit/ErrorHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleError(Lretrofit/RetrofitError;)Ljava/lang/Throwable;
    .locals 2
    .param p1, "cause"    # Lretrofit/RetrofitError;

    .prologue
    .line 28
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    invoke-virtual {v0}, Lretrofit/client/Response;->getStatus()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    .line 29
    new-instance p1, Lcom/vectorwatch/android/events/UnauthorizedException;

    .end local p1    # "cause":Lretrofit/RetrofitError;
    invoke-direct {p1}, Lcom/vectorwatch/android/events/UnauthorizedException;-><init>()V

    .line 32
    :cond_0
    return-object p1
.end method
