.class final Lcom/vectorwatch/android/cloud_communication/FileManager$1;
.super Ljava/lang/Object;
.source "FileManager.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/FileManager;->uploadFileToCloud(Landroid/content/Context;Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/ServerResponseModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Ljava/io/File;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/FileManager$1;->val$file:Ljava/io/File;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/FileManager$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 7
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    const/4 v6, -0x1

    .line 164
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    .line 165
    .local v0, "response":Lretrofit/client/Response;
    const/4 v2, -0x1

    .line 168
    .local v2, "statusCodeMessageResId":I
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0}, Lretrofit/client/Response;->getStatus()I

    move-result v1

    .line 170
    .local v1, "status":I
    invoke-static {v1}, Lcom/vectorwatch/android/utils/CloudStatusCodes;->getDefaultStringIdForStatus(I)I

    move-result v2

    .line 173
    .end local v1    # "status":I
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/FileManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FILE MANAGER: Failure at sync logs to cloud (with error code): "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eq v2, v6, :cond_1

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/FileManager$1;->val$context:Landroid/content/Context;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/FileManager$1;->val$context:Landroid/content/Context;

    .line 174
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/FileManager$1;->val$context:Landroid/content/Context;

    .line 175
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 173
    invoke-interface {v4, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 176
    return-void

    .line 175
    :cond_1
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_0
.end method

.method public success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V
    .locals 4
    .param p1, "serverResponseModel"    # Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 152
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/FileManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FILE MANAGER: File uploaded successfully. Will delete file  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/FileManager$1;->val$file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 154
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/FileManager$1;->val$file:Ljava/io/File;

    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/FileManager;->deleteFile(Ljava/io/File;)Z

    move-result v0

    .line 155
    .local v0, "isDone":Z
    if-eqz v0, :cond_0

    .line 156
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/FileManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "FILE MANAGER: File deleted."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/FileManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "FILE MANAGER: File not deleted."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 149
    check-cast p1, Lcom/vectorwatch/android/models/ServerResponseModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/FileManager$1;->success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V

    return-void
.end method
