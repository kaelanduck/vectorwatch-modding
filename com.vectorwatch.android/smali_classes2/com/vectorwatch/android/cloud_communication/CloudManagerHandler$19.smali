.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->uninstallStream(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$stream:Lcom/vectorwatch/android/models/Stream;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 1123
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 1127
    :try_start_0
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$account:Landroid/accounts/Account;

    const-string v3, "Full access"

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v3, v5}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 1130
    .local v6, "authToken":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-static {v6, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v0

    .line 1132
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    new-instance v4, Lcom/vectorwatch/android/models/ChannelSettingsChange;

    invoke-direct {v4}, Lcom/vectorwatch/android/models/ChannelSettingsChange;-><init>()V

    .line 1133
    .local v4, "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/vectorwatch/android/models/ChannelSettingsChange;->setOldChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 1135
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 1137
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "UNINSTALLED"

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->changeStreamState(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1149
    .end local v0    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    .end local v6    # "authToken":Ljava/lang/String;
    :goto_0
    return-void

    .line 1139
    :catch_0
    move-exception v7

    .line 1140
    .local v7, "e":Landroid/accounts/AuthenticatorException;
    const-string v1, "Account"

    invoke-static {v1, v7}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v8

    .line 1141
    .local v8, "error":Lretrofit/RetrofitError;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$cb:Lretrofit/Callback;

    invoke-interface {v1, v8}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1142
    .end local v7    # "e":Landroid/accounts/AuthenticatorException;
    .end local v8    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v7

    .line 1143
    .local v7, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "Canceled"

    invoke-static {v1, v7}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v8

    .line 1144
    .restart local v8    # "error":Lretrofit/RetrofitError;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$cb:Lretrofit/Callback;

    invoke-interface {v1, v8}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 1145
    .end local v7    # "e":Landroid/accounts/OperationCanceledException;
    .end local v8    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v7

    .line 1146
    .local v7, "e":Ljava/lang/Exception;
    const-string v1, "Unexpected"

    invoke-static {v1, v7}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v8

    .line 1147
    .restart local v8    # "error":Lretrofit/RetrofitError;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;->val$cb:Lretrofit/Callback;

    invoke-interface {v1, v8}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
