.class final Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;
.super Ljava/lang/Object;
.source "CloudCommunicationHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->notifyCloudAppStateChange(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$appIdCopy:I

.field final synthetic val$contextCopy:Landroid/content/Context;

.field final synthetic val$extraInfo:Lcom/vectorwatch/android/models/BaseCloudRequestModel;

.field final synthetic val$newState:Ljava/lang/String;

.field final synthetic val$uuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$newState:Ljava/lang/String;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$uuid:Ljava/lang/String;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$extraInfo:Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    iput-object p6, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$contextCopy:Landroid/content/Context;

    iput p7, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$appIdCopy:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 220
    :try_start_0
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$account:Landroid/accounts/Account;

    const-string v6, "Full access"

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 222
    .local v0, "authToken":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-static {v0, v4}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    .line 225
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    :try_start_1
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CLOUD CALL - APP to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$newState:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - uuid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$uuid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". Details "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$extraInfo:Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    .line 226
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getAppVersion()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$extraInfo:Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getDeviceType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$extraInfo:Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getKernelVersion()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 225
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 228
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$uuid:Ljava/lang/String;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$newState:Ljava/lang/String;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$extraInfo:Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-interface {v1, v4, v5, v6}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->notifyAppStateChange(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Lcom/vectorwatch/android/models/DownloadedAppContainer;

    .line 233
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CLOUD CALL - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$newState:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v6}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 235
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$newState:Ljava/lang/String;

    sget-object v5, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v5}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 236
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$contextCopy:Landroid/content/Context;

    if-eqz v4, :cond_0

    .line 237
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "CLOUD CALL - mark cloud as unmodified"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 238
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$uuid:Ljava/lang/String;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$contextCopy:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->markCloudAppAsUnmodified(Ljava/lang/String;Landroid/content/Context;)V

    .line 260
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    :cond_0
    :goto_0
    return-void

    .line 241
    .restart local v0    # "authToken":Ljava/lang/String;
    .restart local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$newState:Ljava/lang/String;

    sget-object v5, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v5}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 242
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$contextCopy:Landroid/content/Context;

    if-eqz v4, :cond_0

    .line 243
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->access$000()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "CLOUD CALL - uninstall -> delete app"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 245
    iget v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$appIdCopy:I

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;->val$contextCopy:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->hardDeleteApp(ILandroid/content/Context;)V
    :try_end_1
    .catch Lretrofit/RetrofitError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 249
    :catch_0
    move-exception v3

    .line 250
    .local v3, "error":Lretrofit/RetrofitError;
    :try_start_2
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;
    :try_end_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 253
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 254
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v2}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 255
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    :catch_2
    move-exception v2

    .line 256
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 257
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 258
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v2}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_0
.end method
