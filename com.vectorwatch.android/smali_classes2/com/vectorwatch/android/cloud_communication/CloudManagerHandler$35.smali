.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->queryStore(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;ILjava/lang/String;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$appOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$from:I

.field final synthetic val$query:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;ILcom/vectorwatch/android/models/cloud/AppsOption;Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 1863
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$account:Landroid/accounts/Account;

    iput p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$from:I

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$appOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$context:Landroid/content/Context;

    iput-object p6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$query:Ljava/lang/String;

    iput-object p7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p8, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 1867
    :try_start_0
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$account:Landroid/accounts/Account;

    const-string v9, "Full access"

    const/4 v10, 0x1

    invoke-virtual {v7, v8, v9, v10}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1868
    .local v0, "authToken":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-static {v0, v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 1870
    .local v1, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1871
    .local v6, "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "from"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget v9, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$from:I

    mul-int/lit8 v9, v9, 0x5

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1872
    const-string v7, "size"

    const-string v8, "5"

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1873
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$appOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    sget-object v8, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    if-ne v7, v8, :cond_2

    .line 1874
    const-string v7, "type"

    const-string v8, "watchface"

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1880
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$context:Landroid/content/Context;

    invoke-static {v7}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "round"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1881
    const-string v7, "compatibility"

    sget-object v8, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/WatchType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1885
    :goto_1
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$query:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1886
    const-string v7, "q"

    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$query:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1889
    :cond_1
    new-instance v5, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v5}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 1890
    .local v5, "model":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 1891
    invoke-static {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$200(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Ljava/lang/String;

    move-result-object v2

    .line 1893
    .local v2, "defaultHeader":Ljava/lang/String;
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v2, v6, v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->queryStore(Ljava/lang/String;Ljava/util/HashMap;Lretrofit/Callback;)V

    .line 1904
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v2    # "defaultHeader":Ljava/lang/String;
    .end local v5    # "model":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    .end local v6    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_2
    return-void

    .line 1875
    .restart local v0    # "authToken":Ljava/lang/String;
    .restart local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .restart local v6    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$appOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    sget-object v8, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    if-ne v7, v8, :cond_3

    .line 1876
    const-string v7, "type"

    const-string v8, "app"

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 1894
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v6    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    .line 1895
    .local v3, "e":Landroid/accounts/AuthenticatorException;
    const-string v7, "Account"

    invoke-static {v7, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 1896
    .local v4, "error":Lretrofit/RetrofitError;
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$cb:Lretrofit/Callback;

    invoke-interface {v7, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_2

    .line 1877
    .end local v3    # "e":Landroid/accounts/AuthenticatorException;
    .end local v4    # "error":Lretrofit/RetrofitError;
    .restart local v0    # "authToken":Ljava/lang/String;
    .restart local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .restart local v6    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    :try_start_1
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$appOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    sget-object v8, Lcom/vectorwatch/android/models/cloud/AppsOption;->STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

    if-ne v7, v8, :cond_0

    .line 1878
    const-string v7, "type"

    const-string v8, "stream"

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 1897
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v6    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_1
    move-exception v3

    .line 1898
    .local v3, "e":Landroid/accounts/OperationCanceledException;
    const-string v7, "Canceled"

    invoke-static {v7, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 1899
    .restart local v4    # "error":Lretrofit/RetrofitError;
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$cb:Lretrofit/Callback;

    invoke-interface {v7, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_2

    .line 1883
    .end local v3    # "e":Landroid/accounts/OperationCanceledException;
    .end local v4    # "error":Lretrofit/RetrofitError;
    .restart local v0    # "authToken":Ljava/lang/String;
    .restart local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .restart local v6    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    :try_start_2
    const-string v7, "compatibility"

    sget-object v8, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/WatchType;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 1900
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v6    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_2
    move-exception v3

    .line 1901
    .local v3, "e":Ljava/lang/Exception;
    const-string v7, "Unexpected"

    invoke-static {v7, v3}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    .line 1902
    .restart local v4    # "error":Lretrofit/RetrofitError;
    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;->val$cb:Lretrofit/Callback;

    invoke-interface {v7, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_2
.end method
