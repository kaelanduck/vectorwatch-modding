.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$38;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->accountSignUp(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$userInfo:Lcom/vectorwatch/android/models/UserModel;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/models/UserModel;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 1992
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$38;->val$userInfo:Lcom/vectorwatch/android/models/UserModel;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$38;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$38;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1996
    const/4 v3, 0x1

    :try_start_0
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v0

    .line 1998
    .local v0, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$38;->val$userInfo:Lcom/vectorwatch/android/models/UserModel;

    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$38;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v0, v3, v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->createAccount(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2003
    .end local v0    # "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    :goto_0
    return-void

    .line 1999
    :catch_0
    move-exception v1

    .line 2000
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "Unexpected"

    invoke-static {v3, v1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v2

    .line 2001
    .local v2, "error":Lretrofit/RetrofitError;
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$38;->val$cb:Lretrofit/Callback;

    invoke-interface {v3, v2}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
