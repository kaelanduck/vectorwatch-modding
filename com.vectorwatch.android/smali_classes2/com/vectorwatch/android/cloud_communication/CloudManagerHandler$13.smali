.class final Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;
.super Ljava/lang/Thread;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getStreamSettings(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lretrofit/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

.field final synthetic val$cb:Lretrofit/Callback;

.field final synthetic val$stream:Lcom/vectorwatch/android/models/Stream;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V
    .locals 0

    .prologue
    .line 786
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$cb:Lretrofit/Callback;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 790
    :try_start_0
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$account:Landroid/accounts/Account;

    const-string v7, "Full access"

    const/4 v8, 0x1

    invoke-virtual {v5, v6, v7, v8}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 792
    .local v0, "authToken":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-static {v0, v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v1

    .line 793
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    new-instance v4, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v4}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 794
    .local v4, "request":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 795
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$callback:Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-interface {v1, v5, v6, v4, v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->getStreamSettings(Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Lretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 806
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v4    # "request":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    :goto_0
    return-void

    .line 796
    :catch_0
    move-exception v2

    .line 797
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    const-string v5, "Account"

    invoke-static {v5, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 798
    .local v3, "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 799
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v2

    .line 800
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    const-string v5, "Canceled"

    invoke-static {v5, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 801
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 802
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .end local v3    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v2

    .line 803
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "Unexpected"

    invoke-static {v5, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    .line 804
    .restart local v3    # "error":Lretrofit/RetrofitError;
    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;->val$cb:Lretrofit/Callback;

    invoke-interface {v5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method
