.class public final Lcom/vectorwatch/android/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f0b000f

.field public static final abc_action_bar_default_height_material:I = 0x7f0b0004

.field public static final abc_action_bar_default_padding_material:I = 0x7f0b0005

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f0b001d

.field public static final abc_action_bar_navigation_padding_start_material:I = 0x7f0b0010

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f0b0011

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f0b001e

.field public static final abc_action_bar_progress_bar_size:I = 0x7f0b0006

.field public static final abc_action_bar_stacked_max_height:I = 0x7f0b001f

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f0b0020

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f0b0021

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f0b0022

.field public static final abc_action_button_min_height_material:I = 0x7f0b0023

.field public static final abc_action_button_min_width_material:I = 0x7f0b0024

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f0b0025

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f0b0000

.field public static final abc_button_inset_horizontal_material:I = 0x7f0b0026

.field public static final abc_button_inset_vertical_material:I = 0x7f0b0027

.field public static final abc_button_padding_horizontal_material:I = 0x7f0b0028

.field public static final abc_button_padding_vertical_material:I = 0x7f0b0029

.field public static final abc_config_prefDialogWidth:I = 0x7f0b0009

.field public static final abc_control_corner_material:I = 0x7f0b002a

.field public static final abc_control_inset_material:I = 0x7f0b002b

.field public static final abc_control_padding_material:I = 0x7f0b002c

.field public static final abc_dialog_list_padding_vertical_material:I = 0x7f0b002d

.field public static final abc_dialog_min_width_major:I = 0x7f0b002e

.field public static final abc_dialog_min_width_minor:I = 0x7f0b002f

.field public static final abc_dialog_padding_material:I = 0x7f0b0030

.field public static final abc_dialog_padding_top_material:I = 0x7f0b0031

.field public static final abc_disabled_alpha_material_dark:I = 0x7f0b0032

.field public static final abc_disabled_alpha_material_light:I = 0x7f0b0033

.field public static final abc_dropdownitem_icon_width:I = 0x7f0b0034

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f0b0035

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f0b0036

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f0b0037

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f0b0038

.field public static final abc_edit_text_inset_top_material:I = 0x7f0b0039

.field public static final abc_floating_window_z:I = 0x7f0b003a

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f0b003b

.field public static final abc_panel_menu_list_width:I = 0x7f0b003c

.field public static final abc_search_view_preferred_width:I = 0x7f0b003d

.field public static final abc_search_view_text_min_width:I = 0x7f0b000a

.field public static final abc_switch_padding:I = 0x7f0b001a

.field public static final abc_text_size_body_1_material:I = 0x7f0b003e

.field public static final abc_text_size_body_2_material:I = 0x7f0b003f

.field public static final abc_text_size_button_material:I = 0x7f0b0040

.field public static final abc_text_size_caption_material:I = 0x7f0b0041

.field public static final abc_text_size_display_1_material:I = 0x7f0b0042

.field public static final abc_text_size_display_2_material:I = 0x7f0b0043

.field public static final abc_text_size_display_3_material:I = 0x7f0b0044

.field public static final abc_text_size_display_4_material:I = 0x7f0b0045

.field public static final abc_text_size_headline_material:I = 0x7f0b0046

.field public static final abc_text_size_large_material:I = 0x7f0b0047

.field public static final abc_text_size_medium_material:I = 0x7f0b0048

.field public static final abc_text_size_menu_material:I = 0x7f0b0049

.field public static final abc_text_size_small_material:I = 0x7f0b004a

.field public static final abc_text_size_subhead_material:I = 0x7f0b004b

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f0b0007

.field public static final abc_text_size_title_material:I = 0x7f0b004c

.field public static final abc_text_size_title_material_toolbar:I = 0x7f0b0008

.field public static final activity_horizontal_margin:I = 0x7f0b001c

.field public static final activity_stats_padding:I = 0x7f0b004d

.field public static final activity_vertical_margin:I = 0x7f0b004e

.field public static final alarm_time_text_width:I = 0x7f0b004f

.field public static final appbar_elevation:I = 0x7f0b0050

.field public static final clock_margin:I = 0x7f0b0051

.field public static final com_facebook_likeboxcountview_border_radius:I = 0x7f0b0052

.field public static final com_facebook_likeboxcountview_border_width:I = 0x7f0b0053

.field public static final com_facebook_likeboxcountview_caret_height:I = 0x7f0b0054

.field public static final com_facebook_likeboxcountview_caret_width:I = 0x7f0b0055

.field public static final com_facebook_likeboxcountview_text_padding:I = 0x7f0b0056

.field public static final com_facebook_likeboxcountview_text_size:I = 0x7f0b0057

.field public static final com_facebook_likeview_edge_padding:I = 0x7f0b0058

.field public static final com_facebook_likeview_internal_padding:I = 0x7f0b0059

.field public static final com_facebook_likeview_text_size:I = 0x7f0b005a

.field public static final com_facebook_profilepictureview_preset_size_large:I = 0x7f0b005b

.field public static final com_facebook_profilepictureview_preset_size_normal:I = 0x7f0b005c

.field public static final com_facebook_profilepictureview_preset_size_small:I = 0x7f0b005d

.field public static final com_facebook_share_button_compound_drawable_padding:I = 0x7f0b005e

.field public static final com_facebook_share_button_padding_bottom:I = 0x7f0b005f

.field public static final com_facebook_share_button_padding_left:I = 0x7f0b0060

.field public static final com_facebook_share_button_padding_right:I = 0x7f0b0061

.field public static final com_facebook_share_button_padding_top:I = 0x7f0b0062

.field public static final com_facebook_share_button_text_size:I = 0x7f0b0063

.field public static final com_facebook_tooltip_horizontal_padding:I = 0x7f0b0064

.field public static final cpb_default_stroke_width:I = 0x7f0b0065

.field public static final cwac_cam2_fragment_drawer_offset_distance:I = 0x7f0b0066

.field public static final cwac_cam2_fragment_drawer_shadow_size:I = 0x7f0b0067

.field public static final cwac_cam2_fragment_drawer_width:I = 0x7f0b0068

.field public static final cwac_cam2_fragment_panel_item_height:I = 0x7f0b0069

.field public static final cwac_cam2_fragment_panel_item_width:I = 0x7f0b006a

.field public static final cwac_cam2_fragment_panel_padding:I = 0x7f0b006b

.field public static final default_circle_indicator_radius:I = 0x7f0b006c

.field public static final default_circle_indicator_stroke_width:I = 0x7f0b006d

.field public static final default_line_indicator_gap_width:I = 0x7f0b006e

.field public static final default_line_indicator_line_width:I = 0x7f0b006f

.field public static final default_line_indicator_stroke_width:I = 0x7f0b0070

.field public static final default_title_indicator_clip_padding:I = 0x7f0b0071

.field public static final default_title_indicator_footer_indicator_height:I = 0x7f0b0072

.field public static final default_title_indicator_footer_indicator_underline_padding:I = 0x7f0b0073

.field public static final default_title_indicator_footer_line_height:I = 0x7f0b0074

.field public static final default_title_indicator_footer_padding:I = 0x7f0b0075

.field public static final default_title_indicator_text_size:I = 0x7f0b0076

.field public static final default_title_indicator_title_padding:I = 0x7f0b0077

.field public static final default_title_indicator_top_padding:I = 0x7f0b0078

.field public static final dialog_fixed_height_major:I = 0x7f0b000b

.field public static final dialog_fixed_height_minor:I = 0x7f0b000c

.field public static final dialog_fixed_width_major:I = 0x7f0b000d

.field public static final dialog_fixed_width_minor:I = 0x7f0b000e

.field public static final disabled_alpha_material_dark:I = 0x7f0b0079

.field public static final disabled_alpha_material_light:I = 0x7f0b007a

.field public static final fab_border_width:I = 0x7f0b007b

.field public static final fab_content_size:I = 0x7f0b007c

.field public static final fab_elevation:I = 0x7f0b007d

.field public static final fab_margin:I = 0x7f0b0012

.field public static final fab_size_mini:I = 0x7f0b007e

.field public static final fab_size_normal:I = 0x7f0b007f

.field public static final fab_translation_z_pressed:I = 0x7f0b0080

.field public static final height_link_lost:I = 0x7f0b0081

.field public static final horizontal_page_margin:I = 0x7f0b0082

.field public static final item_preferences_margin_extra_extra_large:I = 0x7f0b0083

.field public static final item_preferences_margin_extra_large:I = 0x7f0b0084

.field public static final item_preferences_margin_large:I = 0x7f0b0085

.field public static final item_preferences_margin_small:I = 0x7f0b0086

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f0b0087

.field public static final labels_text_size:I = 0x7f0b0088

.field public static final margin_huge:I = 0x7f0b0089

.field public static final margin_large:I = 0x7f0b008a

.field public static final margin_medium:I = 0x7f0b008b

.field public static final margin_small:I = 0x7f0b008c

.field public static final margin_tiny:I = 0x7f0b008d

.field public static final media_controller_bottom_margin:I = 0x7f0b008e

.field public static final media_controller_button_height:I = 0x7f0b008f

.field public static final media_controller_button_width:I = 0x7f0b0090

.field public static final media_controller_seekbar_height:I = 0x7f0b0091

.field public static final media_controller_seekbar_width:I = 0x7f0b0092

.field public static final media_controller_text_size:I = 0x7f0b0093

.field public static final media_controller_top_margin:I = 0x7f0b0094

.field public static final navigation_elevation:I = 0x7f0b0095

.field public static final navigation_icon_padding:I = 0x7f0b0096

.field public static final navigation_icon_size:I = 0x7f0b0097

.field public static final navigation_max_width:I = 0x7f0b0098

.field public static final navigation_padding_bottom:I = 0x7f0b0099

.field public static final navigation_padding_top_default:I = 0x7f0b001b

.field public static final navigation_separator_vertical_padding:I = 0x7f0b009a

.field public static final notification_large_icon_height:I = 0x7f0b009b

.field public static final notification_large_icon_width:I = 0x7f0b009c

.field public static final notification_subtext_size:I = 0x7f0b009d

.field public static final padding:I = 0x7f0b009e

.field public static final padding_extra_large:I = 0x7f0b009f

.field public static final padding_medium:I = 0x7f0b00a0

.field public static final padding_none:I = 0x7f0b00a1

.field public static final padding_small:I = 0x7f0b00a2

.field public static final padding_time_zones_icons:I = 0x7f0b00a3

.field public static final page_width:I = 0x7f0b0001

.field public static final place_autocomplete_button_padding:I = 0x7f0b00a4

.field public static final place_autocomplete_powered_by_google_height:I = 0x7f0b00a5

.field public static final place_autocomplete_powered_by_google_start:I = 0x7f0b00a6

.field public static final place_autocomplete_prediction_height:I = 0x7f0b00a7

.field public static final place_autocomplete_prediction_horizontal_margin:I = 0x7f0b00a8

.field public static final place_autocomplete_prediction_primary_text:I = 0x7f0b00a9

.field public static final place_autocomplete_prediction_secondary_text:I = 0x7f0b00aa

.field public static final place_autocomplete_progress_horizontal_margin:I = 0x7f0b00ab

.field public static final place_autocomplete_progress_size:I = 0x7f0b00ac

.field public static final place_autocomplete_separator_start:I = 0x7f0b00ad

.field public static final prb_drawable_tick_default_spacing:I = 0x7f0b00ae

.field public static final prb_symbolic_tick_default_text_size:I = 0x7f0b00af

.field public static final settings_alerts_toggle:I = 0x7f0b00b0

.field public static final side_margin:I = 0x7f0b0002

.field public static final snackbar_action_inline_max_width:I = 0x7f0b0013

.field public static final snackbar_background_corner_radius:I = 0x7f0b0014

.field public static final snackbar_elevation:I = 0x7f0b00b1

.field public static final snackbar_extra_spacing_horizontal:I = 0x7f0b0015

.field public static final snackbar_max_width:I = 0x7f0b0016

.field public static final snackbar_min_width:I = 0x7f0b0017

.field public static final snackbar_padding_horizontal:I = 0x7f0b00b2

.field public static final snackbar_padding_vertical:I = 0x7f0b00b3

.field public static final snackbar_padding_vertical_2lines:I = 0x7f0b0018

.field public static final snackbar_text_size:I = 0x7f0b00b4

.field public static final status_bar_padding:I = 0x7f0b00b5

.field public static final stream_remove_button_margin:I = 0x7f0b00b6

.field public static final stream_remove_button_size:I = 0x7f0b00b7

.field public static final tab_height:I = 0x7f0b0003

.field public static final tab_max_width:I = 0x7f0b00b8

.field public static final tab_min_width:I = 0x7f0b0019

.field public static final update_watch_image_margin:I = 0x7f0b00b9

.field public static final update_watch_logo_image_size_x:I = 0x7f0b00ba

.field public static final update_watch_logo_image_size_y:I = 0x7f0b00bb

.field public static final update_watch_margin_for_progress_status:I = 0x7f0b00bc

.field public static final update_watch_progress_size_x:I = 0x7f0b00bd

.field public static final update_watch_progress_size_y:I = 0x7f0b00be

.field public static final update_watch_text_element_margin:I = 0x7f0b00bf

.field public static final update_watch_update_details_height:I = 0x7f0b00c0

.field public static final update_watch_update_details_width:I = 0x7f0b00c1

.field public static final update_watch_vector_image_size_x:I = 0x7f0b00c2

.field public static final update_watch_vector_image_size_y:I = 0x7f0b00c3

.field public static final vertical_page_margin:I = 0x7f0b00c4

.field public static final watchface_details_size:I = 0x7f0b00c5

.field public static final watchface_details_size_2:I = 0x7f0b00c6

.field public static final watchface_size:I = 0x7f0b00c7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
