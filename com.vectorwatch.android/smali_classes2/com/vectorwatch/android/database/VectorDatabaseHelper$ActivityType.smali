.class public final enum Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;
.super Ljava/lang/Enum;
.source "VectorDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/database/VectorDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

.field public static final enum ACTIVITY:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

.field public static final enum HINT:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

.field public static final enum NO_MOVEMENT:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

.field public static final enum RUN:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

.field public static final enum SLEEP:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 229
    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    const-string v1, "ACTIVITY"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->ACTIVITY:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    const-string v1, "RUN"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->RUN:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    const-string v1, "SLEEP"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->SLEEP:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    const-string v1, "HINT"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->HINT:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    const-string v1, "NO_MOVEMENT"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->NO_MOVEMENT:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    .line 228
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->ACTIVITY:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->RUN:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->SLEEP:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->HINT:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->NO_MOVEMENT:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 228
    const-class v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    return-object v0
.end method
