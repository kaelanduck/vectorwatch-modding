.class public final enum Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;
.super Ljava/lang/Enum;
.source "VectorDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/database/VectorDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContextualArea"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

.field public static final enum HOME:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

.field public static final enum TRAVEL:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

.field public static final enum WORK:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 241
    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;->HOME:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    const-string v1, "WORK"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;->WORK:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    const-string v1, "TRAVEL"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;->TRAVEL:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    .line 240
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;->HOME:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;->WORK:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;->TRAVEL:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 240
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 240
    const-class v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;
    .locals 1

    .prologue
    .line 240
    sget-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;

    return-object v0
.end method
