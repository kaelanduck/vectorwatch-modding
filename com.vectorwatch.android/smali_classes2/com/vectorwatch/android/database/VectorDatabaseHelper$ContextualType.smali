.class public final enum Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;
.super Ljava/lang/Enum;
.source "VectorDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/database/VectorDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContextualType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

.field public static final enum GLANCE:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

.field public static final enum RUNNING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

.field public static final enum TRANSPORTATION:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

.field public static final enum WALKING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 237
    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    const-string v1, "WALKING"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->WALKING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->RUNNING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    const-string v1, "TRANSPORTATION"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->TRANSPORTATION:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    const-string v1, "GLANCE"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->GLANCE:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    .line 236
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->WALKING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->RUNNING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->TRANSPORTATION:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->GLANCE:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 236
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 236
    const-class v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;
    .locals 1

    .prologue
    .line 236
    sget-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;

    return-object v0
.end method
