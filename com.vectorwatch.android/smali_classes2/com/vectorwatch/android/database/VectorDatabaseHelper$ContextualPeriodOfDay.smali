.class public final enum Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;
.super Ljava/lang/Enum;
.source "VectorDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/database/VectorDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContextualPeriodOfDay"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

.field public static final enum EVENING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

.field public static final enum MORNING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

.field public static final enum OFFICE:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 233
    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    const-string v1, "MORNING"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;->MORNING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    const-string v1, "EVENING"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;->EVENING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    const-string v1, "OFFICE"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;->OFFICE:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    .line 232
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;->MORNING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;->EVENING:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;->OFFICE:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 232
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 232
    const-class v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;

    return-object v0
.end method
