.class public final enum Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;
.super Ljava/lang/Enum;
.source "VectorDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/database/VectorDatabaseHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LogType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

.field public static final enum BATTERY:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

.field public static final enum BLE_COM:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

.field public static final enum BUTTON_PRESS:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

.field public static final enum NOTIF_COUNT:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 245
    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    const-string v1, "BATTERY"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->BATTERY:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    const-string v1, "BLE_COM"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->BLE_COM:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    const-string v1, "BUTTON_PRESS"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->BUTTON_PRESS:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    new-instance v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    const-string v1, "NOTIF_COUNT"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->NOTIF_COUNT:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    .line 244
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->BATTERY:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->BLE_COM:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->BUTTON_PRESS:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->NOTIF_COUNT:Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 244
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 244
    const-class v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->$VALUES:[Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;

    return-object v0
.end method
