.class public final enum Lcom/vectorwatch/android/utils/Constants$SettingsType;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SettingsType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$SettingsType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_DND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_GLANCE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_LOCALIZATION:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_ACTIVITY_ALERTS:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_AF:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_AGE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_AUTO_DISCREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_AUTO_SLEEP_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_BACKLIGHT_LEVEL:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_BACKLIGHT_TIMEOUT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_DROP_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_GENDER:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_HEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_HOUR_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_MORNING_GREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_NAME:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_NOTIFICATIONS_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_SHOW_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_UNIT_SYSTEM:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_WARNING_OPTIONS:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field public static final enum SETTING_TYPE_WEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;


# instance fields
.field private final val:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 327
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_NAME"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_NAME:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 328
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_AGE"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AGE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 329
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_GENDER"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_GENDER:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 330
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_WEIGHT"

    invoke-direct {v0, v1, v7, v7}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_WEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 331
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_HEIGHT"

    invoke-direct {v0, v1, v8, v8}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_HEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 332
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_AF"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AF:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 333
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_UNIT_SYSTEM"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_UNIT_SYSTEM:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 334
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_HOUR_MODE"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_HOUR_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 335
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_MORNING_GREET_ENABLE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_MORNING_GREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 336
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_AUTO_DISCREET_ENABLE"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_DISCREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 337
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_AUTO_SLEEP_ENABLE"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_SLEEP_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 338
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_DROP_MODE"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_DROP_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 339
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_DND"

    const/16 v2, 0xc

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_DND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 340
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_GLANCE"

    const/16 v2, 0xd

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_GLANCE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 341
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_BACKLIGHT_LEVEL"

    const/16 v2, 0xe

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_BACKLIGHT_LEVEL:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 342
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_BACKLIGHT_TIMEOUT"

    const/16 v2, 0xf

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_BACKLIGHT_TIMEOUT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 343
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_SHOW_SECOND_HAND"

    const/16 v2, 0x10

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_SHOW_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 344
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_LOCALIZATION"

    const/16 v2, 0x11

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_LOCALIZATION:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 345
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_WARNING_OPTIONS"

    const/16 v2, 0x12

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_WARNING_OPTIONS:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 346
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_ACTIVITY_ALERTS"

    const/16 v2, 0x13

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_ACTIVITY_ALERTS:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 347
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    const-string v1, "SETTING_TYPE_NOTIFICATIONS_MODE"

    const/16 v2, 0x14

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_NOTIFICATIONS_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 326
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$SettingsType;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_NAME:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AGE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_GENDER:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_WEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_HEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AF:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_UNIT_SYSTEM:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_HOUR_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_MORNING_GREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_DISCREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_SLEEP_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_DROP_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_DND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_GLANCE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_BACKLIGHT_LEVEL:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_BACKLIGHT_TIMEOUT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_SHOW_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_LOCALIZATION:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_WARNING_OPTIONS:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_ACTIVITY_ALERTS:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_NOTIFICATIONS_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$SettingsType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 352
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->val:I

    .line 353
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$SettingsType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 326
    const-class v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$SettingsType;
    .locals 1

    .prologue
    .line 326
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$SettingsType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$SettingsType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->val:I

    return v0
.end method
