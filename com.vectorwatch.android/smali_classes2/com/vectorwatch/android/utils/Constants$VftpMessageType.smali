.class public final enum Lcom/vectorwatch/android/utils/Constants$VftpMessageType;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VftpMessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$VftpMessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

.field public static final enum VFTP_MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

.field public static final enum VFTP_MESSAGE_TYPE_PUT:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

.field public static final enum VFTP_MESSAGE_TYPE_REQ:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

.field public static final enum VFTP_MESSAGE_TYPE_STATUS:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

.field public static final enum VFTP_MESSAGE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 716
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    const-string v1, "VFTP_MESSAGE_TYPE_REQ"

    invoke-direct {v0, v1, v3, v3}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_REQ:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 717
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    const-string v1, "VFTP_MESSAGE_TYPE_PUT"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_PUT:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 718
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    const-string v1, "VFTP_MESSAGE_TYPE_DATA"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 719
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    const-string v1, "VFTP_MESSAGE_TYPE_STATUS"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_STATUS:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 720
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    const-string v1, "VFTP_MESSAGE_TYPE_UNKNOWN"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 715
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_REQ:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_PUT:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_STATUS:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 724
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 725
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->val:I

    .line 726
    return-void
.end method

.method public static getType(I)Lcom/vectorwatch/android/utils/Constants$VftpMessageType;
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 733
    packed-switch p0, :pswitch_data_0

    .line 743
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    :goto_0
    return-object v0

    .line 735
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_REQ:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    goto :goto_0

    .line 737
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_PUT:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    goto :goto_0

    .line 739
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    goto :goto_0

    .line 741
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_STATUS:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    goto :goto_0

    .line 733
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$VftpMessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 715
    const-class v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$VftpMessageType;
    .locals 1

    .prologue
    .line 715
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 729
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->val:I

    return v0
.end method
