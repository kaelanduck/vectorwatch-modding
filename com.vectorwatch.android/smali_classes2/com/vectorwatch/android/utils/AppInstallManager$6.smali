.class Lcom/vectorwatch/android/utils/AppInstallManager$6;
.super Ljava/lang/Object;
.source "AppInstallManager.java"

# interfaces
.implements Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/AppInstallManager;->startConfiguringAppSettings(Lcom/vectorwatch/android/models/CloudElementSummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

.field final synthetic val$app:Lcom/vectorwatch/android/models/CloudElementSummary;

.field final synthetic val$locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

.field final synthetic val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/LocationHelper;Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/utils/AppInstallManager;

    .prologue
    .line 626
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iput-object p2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$app:Lcom/vectorwatch/android/models/CloudElementSummary;

    iput-object p3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    iput-object p4, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccurateLocation(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 629
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - config app settings - onAccurateLoc app = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " loc"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 630
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 629
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 631
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 632
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask$Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    invoke-static {p1}, Lcom/vectorwatch/android/models/VectorLocation;->fromLocation(Landroid/location/Location;)Lcom/vectorwatch/android/models/VectorLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    .line 634
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 636
    :cond_0
    return-void
.end method

.method public onLocation(Landroid/location/Location;)V
    .locals 3
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 640
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - config app settings - onLocation app = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " loc"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 641
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 640
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 644
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 645
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask$Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    invoke-static {p1}, Lcom/vectorwatch/android/models/VectorLocation;->fromLocation(Landroid/location/Location;)Lcom/vectorwatch/android/models/VectorLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    .line 647
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 649
    :cond_0
    return-void
.end method

.method public onTimeoutError()V
    .locals 3

    .prologue
    .line 653
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - config app settings - onTimeout app = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 654
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 656
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->PENDING:Landroid/os/AsyncTask$Status;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask$Status;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$6;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 659
    :cond_0
    return-void
.end method
