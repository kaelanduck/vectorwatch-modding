.class Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1$1;
.super Ljava/lang/Object;
.source "ScannerApi18.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1;->onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1;

.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;

.field final synthetic val$scanRecord:[B


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1;[BLandroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1$1;->this$1:Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1;

    iput-object p2, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1$1;->val$scanRecord:[B

    iput-object p3, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 31
    const/4 v1, 0x0

    .line 33
    .local v1, "found":Z
    :try_start_0
    iget-object v5, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1$1;->val$scanRecord:[B

    invoke-static {v5}, Lcom/vectorwatch/android/utils/Helpers;->parseFromBytes([B)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 37
    .local v3, "mServiceUuidList":Ljava/util/List;, "Ljava/util/List<Landroid/os/ParcelUuid;>;"
    :goto_0
    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 38
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 39
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/ParcelUuid;

    invoke-virtual {v5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "uuid":Ljava/lang/String;
    const-string v5, "81a50000-9ebd-0436-3358-bb370c7da4c5"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "9e3b0000-d7ab-adf3-f683-baa2a0e81612"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 41
    :cond_0
    const/4 v1, 0x1

    .line 48
    .end local v2    # "i":I
    .end local v4    # "uuid":Ljava/lang/String;
    :cond_1
    :goto_2
    const-string v5, "BLE"

    const-string v6, "Device"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    if-eqz v1, :cond_2

    iget-object v5, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 50
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v5

    new-instance v6, Lcom/vectorwatch/android/events/FoundBleDeviceEvent;

    iget-object v7, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {v6, v7}, Lcom/vectorwatch/android/events/FoundBleDeviceEvent;-><init>(Landroid/bluetooth/BluetoothDevice;)V

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 51
    const-string v5, "BLE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Device found"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :cond_2
    return-void

    .line 34
    .end local v3    # "mServiceUuidList":Ljava/util/List;, "Ljava/util/List<Landroid/os/ParcelUuid;>;"
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .restart local v3    # "mServiceUuidList":Ljava/util/List;, "Ljava/util/List<Landroid/os/ParcelUuid;>;"
    goto :goto_0

    .line 38
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "i":I
    .restart local v4    # "uuid":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 46
    .end local v2    # "i":I
    .end local v4    # "uuid":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x1

    goto :goto_2
.end method
