.class final Lcom/vectorwatch/android/utils/LocationHelper$3;
.super Ljava/lang/Object;
.source "LocationHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/LocationHelper;->triggerGPSDisabledDialogWithApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/vectorwatch/android/utils/LocationHelper$3;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 127
    const/4 v1, 0x1

    sput-boolean v1, Lcom/vectorwatch/android/utils/LocationHelper;->STARTED_LOCATION_SETTINGS_ACTIVITY:Z

    .line 129
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 130
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$3;->val$activity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 131
    return-void
.end method
