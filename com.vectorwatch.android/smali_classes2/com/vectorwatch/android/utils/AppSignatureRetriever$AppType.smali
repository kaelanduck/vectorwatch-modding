.class public final enum Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;
.super Ljava/lang/Enum;
.source "AppSignatureRetriever.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/AppSignatureRetriever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

.field public static final enum ALL_APPS:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

.field public static final enum SYSTEM_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

.field public static final enum USER_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 109
    new-instance v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    const-string v1, "SYSTEM_APP"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->SYSTEM_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    new-instance v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    const-string v1, "USER_APP"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->USER_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    new-instance v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    const-string v1, "ALL_APPS"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->ALL_APPS:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    .line 108
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    sget-object v1, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->SYSTEM_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->USER_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->ALL_APPS:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->$VALUES:[Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 108
    const-class v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->$VALUES:[Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    return-object v0
.end method
