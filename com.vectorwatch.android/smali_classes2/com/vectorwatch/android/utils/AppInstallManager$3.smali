.class Lcom/vectorwatch/android/utils/AppInstallManager$3;
.super Ljava/lang/Object;
.source "AppInstallManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/AppInstallManager;->installAppFromStore(Lcom/vectorwatch/android/models/StoreElement;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

.field final synthetic val$element:Lcom/vectorwatch/android/models/StoreElement;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/utils/AppInstallManager;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$3;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iput-object p2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$3;->val$element:Lcom/vectorwatch/android/models/StoreElement;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x0

    .line 254
    new-instance v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$3;->val$element:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$3;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;-><init>(Ljava/lang/String;Landroid/app/Activity;)V

    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 255
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$3;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$3;->val$element:Lcom/vectorwatch/android/models/StoreElement;

    invoke-static {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->fromStoreElement(Lcom/vectorwatch/android/models/StoreElement;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 256
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 257
    return-void
.end method
