.class public final enum Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SystemButtonCallbackIndex"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ANIMATE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ANIMATE_DEAD_END:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_CHANGE_TO_NEXT_APP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_CHANGE_TO_NEXT_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_CHANGE_TO_PREVIOUS_APP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_CHANGE_TO_PREVIOUS_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_CHANGE_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_DECREMENT_GAUGE_CONTROL:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_GAUGE_CONTROL_CHANGE_STOP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_GAUGE_CONTROL_SEND_VALUE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_INCREMENT_GAUGE_CONTROL:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_LIST_DECREMENT_SELECTION:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_LIST_INCREMENT_SELECTION:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_LIST_SELECT:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_LIST_SEND_VALUE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_NONE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_NUM:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_POP_APP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_REFRESH_ELEMENT:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_SEND_DOUBLE_PRESS_TO_CLOUD:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_SEND_LONG_PRESS_TO_CLOUD:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_SEND_PRESS_TO_CLOUD:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_SHOW_NOTIFICATIONS:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_TOGGLE_ANALOG_DAY:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 494
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_NONE"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_NONE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 495
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_CHANGE_TO_NEXT_APP"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_TO_NEXT_APP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 496
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_CHANGE_TO_PREVIOUS_APP"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_TO_PREVIOUS_APP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 497
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_POP_APP"

    invoke-direct {v0, v1, v7, v7}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_POP_APP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 498
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_CHANGE_TO_NEXT_WATCHFACE"

    invoke-direct {v0, v1, v8, v8}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_TO_NEXT_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 499
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_CHANGE_TO_PREVIOUS_WATCHFACE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_TO_PREVIOUS_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 500
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_CHANGE_WATCHFACE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 501
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ANIMATE"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_ANIMATE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 502
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ANIMATE_DEAD_END"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_ANIMATE_DEAD_END:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 503
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_SEND_PRESS_TO_CLOUD"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_SEND_PRESS_TO_CLOUD:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 504
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_SEND_LONG_PRESS_TO_CLOUD"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_SEND_LONG_PRESS_TO_CLOUD:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 505
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_SEND_DOUBLE_PRESS_TO_CLOUD"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_SEND_DOUBLE_PRESS_TO_CLOUD:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 506
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_TOGGLE_ANALOG_DAY"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_TOGGLE_ANALOG_DAY:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 507
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_SHOW_NOTIFICATIONS"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_SHOW_NOTIFICATIONS:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 508
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_LIST_INCREMENT_SELECTION"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_LIST_INCREMENT_SELECTION:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 509
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_LIST_DECREMENT_SELECTION"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_LIST_DECREMENT_SELECTION:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 510
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_LIST_SELECT"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_LIST_SELECT:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 511
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_INCREMENT_GAUGE_CONTROL"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_INCREMENT_GAUGE_CONTROL:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 512
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_DECREMENT_GAUGE_CONTROL"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_DECREMENT_GAUGE_CONTROL:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 513
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_GAUGE_CONTROL_CHANGE_STOP"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_GAUGE_CONTROL_CHANGE_STOP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 514
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_GAUGE_CONTROL_SEND_VALUE"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_GAUGE_CONTROL_SEND_VALUE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 515
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_LIST_SEND_VALUE"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_LIST_SEND_VALUE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 516
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_REFRESH_ELEMENT"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_REFRESH_ELEMENT:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 517
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_NUM"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_NUM:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    .line 493
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_NONE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_TO_NEXT_APP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_TO_PREVIOUS_APP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_POP_APP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_TO_NEXT_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_TO_PREVIOUS_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_CHANGE_WATCHFACE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_ANIMATE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_ANIMATE_DEAD_END:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_SEND_PRESS_TO_CLOUD:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_SEND_LONG_PRESS_TO_CLOUD:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_SEND_DOUBLE_PRESS_TO_CLOUD:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_TOGGLE_ANALOG_DAY:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_SHOW_NOTIFICATIONS:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_LIST_INCREMENT_SELECTION:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_LIST_DECREMENT_SELECTION:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_LIST_SELECT:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_INCREMENT_GAUGE_CONTROL:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_DECREMENT_GAUGE_CONTROL:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_GAUGE_CONTROL_CHANGE_STOP:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_GAUGE_CONTROL_SEND_VALUE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_LIST_SEND_VALUE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_REFRESH_ELEMENT:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_NUM:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 521
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 522
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->value:I

    .line 523
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 526
    if-eqz p0, :cond_1

    .line 527
    invoke-static {}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->values()[Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 528
    .local v0, "s":Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;
    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 533
    .end local v0    # "s":Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;
    :goto_1
    return-object v0

    .line 527
    .restart local v0    # "s":Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 533
    .end local v0    # "s":Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->BUTTON_CALLBACK_NONE:Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 493
    const-class v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;
    .locals 1

    .prologue
    .line 493
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;

    return-object v0
.end method
