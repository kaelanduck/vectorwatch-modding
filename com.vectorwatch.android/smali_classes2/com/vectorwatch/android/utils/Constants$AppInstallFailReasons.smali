.class public final enum Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppInstallFailReasons"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

.field public static final enum INTERNAL_SERVER_ERROR:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

.field public static final enum MOBILE_APPLICATION_ERROR:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

.field public static final enum NOT_ENOUGH_SPACE:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

.field public static final enum UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;


# instance fields
.field private final val:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 577
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    .line 578
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    const-string v1, "NOT_ENOUGH_SPACE"

    invoke-direct {v0, v1, v3, v3}, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->NOT_ENOUGH_SPACE:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    .line 579
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    const-string v1, "MOBILE_APPLICATION_ERROR"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->MOBILE_APPLICATION_ERROR:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    .line 580
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    const-string v1, "INTERNAL_SERVER_ERROR"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->INTERNAL_SERVER_ERROR:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    .line 576
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->NOT_ENOUGH_SPACE:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->MOBILE_APPLICATION_ERROR:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->INTERNAL_SERVER_ERROR:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 584
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 585
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->val:I

    .line 586
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 576
    const-class v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;
    .locals 1

    .prologue
    .line 576
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 589
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->val:I

    return v0
.end method
