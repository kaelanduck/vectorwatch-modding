.class Lcom/vectorwatch/android/utils/AppInstallManager$5;
.super Ljava/lang/Object;
.source "AppInstallManager.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/AppInstallManager;->startConfiguringStreamCredentials(Lcom/vectorwatch/android/models/Stream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/AuthMethodResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

.field final synthetic val$listener:Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/utils/AppInstallManager;

    .prologue
    .line 543
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$5;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iput-object p2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$5;->val$listener:Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 552
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$5;->val$listener:Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->onFailure()V

    .line 553
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$5;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$5;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/vectorwatch/android/utils/Helpers;->handleRetrofitError(Landroid/content/Context;Lretrofit/RetrofitError;)V

    .line 556
    :cond_0
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/AuthMethodResponse;Lretrofit/client/Response;)V
    .locals 2
    .param p1, "authMethodResponse"    # Lcom/vectorwatch/android/models/AuthMethodResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 546
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$5;->val$listener:Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/AuthMethodResponse;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V

    .line 547
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$5;->val$listener:Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->onSuccess()V

    .line 548
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 543
    check-cast p1, Lcom/vectorwatch/android/models/AuthMethodResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/utils/AppInstallManager$5;->success(Lcom/vectorwatch/android/models/AuthMethodResponse;Lretrofit/client/Response;)V

    return-void
.end method
