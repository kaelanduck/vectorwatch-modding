.class final Lcom/vectorwatch/android/utils/OfflineUtils$6;
.super Ljava/lang/Object;
.source "OfflineUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/OfflineUtils;->switchToOfflineMode(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 543
    iput-object p1, p0, Lcom/vectorwatch/android/utils/OfflineUtils$6;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 546
    sget-object v3, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    iget-object v4, p0, Lcom/vectorwatch/android/utils/OfflineUtils$6;->val$context:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getAppsWithGivenState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    .line 548
    .local v2, "mActiveWatchFaces":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 550
    .local v0, "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 551
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 550
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 554
    :cond_0
    iget-object v3, p0, Lcom/vectorwatch/android/utils/OfflineUtils$6;->val$context:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->removeBlacklistedApps(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 555
    iget-object v3, p0, Lcom/vectorwatch/android/utils/OfflineUtils$6;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/OfflineUtils;->removeStreamsOffline(Landroid/content/Context;)V

    .line 556
    return-void
.end method
