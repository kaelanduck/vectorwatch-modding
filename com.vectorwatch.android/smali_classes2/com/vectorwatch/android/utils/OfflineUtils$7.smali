.class synthetic Lcom/vectorwatch/android/utils/OfflineUtils$7;
.super Ljava/lang/Object;
.source "OfflineUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/OfflineUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$vectorwatch$android$models$cloud$AppsOption:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 91
    invoke-static {}, Lcom/vectorwatch/android/models/cloud/AppsOption;->values()[Lcom/vectorwatch/android/models/cloud/AppsOption;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vectorwatch/android/utils/OfflineUtils$7;->$SwitchMap$com$vectorwatch$android$models$cloud$AppsOption:[I

    :try_start_0
    sget-object v0, Lcom/vectorwatch/android/utils/OfflineUtils$7;->$SwitchMap$com$vectorwatch$android$models$cloud$AppsOption:[I

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    sget-object v0, Lcom/vectorwatch/android/utils/OfflineUtils$7;->$SwitchMap$com$vectorwatch$android$models$cloud$AppsOption:[I

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    sget-object v0, Lcom/vectorwatch/android/utils/OfflineUtils$7;->$SwitchMap$com$vectorwatch$android$models$cloud$AppsOption:[I

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_0
.end method
