.class final Lcom/vectorwatch/android/utils/OfflineUtils$4;
.super Ljava/lang/Object;
.source "OfflineUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineSoftwareUpdateModel(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$type:Ljava/lang/String;

.field final synthetic val$watchOsDetailsModel:Lcom/vectorwatch/android/models/WatchOsDetailsModel;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/vectorwatch/android/utils/OfflineUtils$4;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/vectorwatch/android/utils/OfflineUtils$4;->val$watchOsDetailsModel:Lcom/vectorwatch/android/models/WatchOsDetailsModel;

    iput-object p3, p0, Lcom/vectorwatch/android/utils/OfflineUtils$4;->val$type:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 381
    new-instance v1, Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;-><init>()V

    .line 383
    .local v1, "softwareUpdateModel":Lcom/vectorwatch/android/models/SoftwareUpdateModel;
    new-instance v2, Lcom/vectorwatch/android/models/UpdateData;

    iget-object v3, p0, Lcom/vectorwatch/android/utils/OfflineUtils$4;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/vectorwatch/android/utils/OfflineUtils$4;->val$watchOsDetailsModel:Lcom/vectorwatch/android/models/WatchOsDetailsModel;

    iget-object v5, p0, Lcom/vectorwatch/android/utils/OfflineUtils$4;->val$type:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineWatchVersionList(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/models/UpdateData;-><init>(Ljava/util/List;)V

    .line 386
    .local v2, "updateData":Lcom/vectorwatch/android/models/UpdateData;
    new-instance v0, Lcom/vectorwatch/android/events/CloudMessageModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/events/CloudMessageModel;-><init>()V

    .line 387
    .local v0, "cloudMessage":Lcom/vectorwatch/android/events/CloudMessageModel;
    sget-object v3, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-virtual {v3}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/events/CloudMessageModel;->setCode(Ljava/lang/Integer;)V

    .line 388
    iget-object v3, p0, Lcom/vectorwatch/android/utils/OfflineUtils$4;->val$context:Landroid/content/Context;

    const v4, 0x7f0901b7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/events/CloudMessageModel;->setText(Ljava/lang/String;)V

    .line 390
    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->setData(Lcom/vectorwatch/android/models/UpdateData;)V

    .line 391
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->setMessage(Lcom/vectorwatch/android/events/CloudMessageModel;)V

    .line 393
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;

    invoke-direct {v4, v1}, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;-><init>(Lcom/vectorwatch/android/models/SoftwareUpdateModel;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 394
    return-void
.end method
