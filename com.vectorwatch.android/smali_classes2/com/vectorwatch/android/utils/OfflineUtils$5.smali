.class final Lcom/vectorwatch/android/utils/OfflineUtils$5;
.super Ljava/lang/Object;
.source "OfflineUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/OfflineUtils;->installAppsToDb(Landroid/content/Context;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$appList:Ljava/util/List;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 424
    iput-object p1, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$appList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 427
    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    check-cast v9, Lcom/vectorwatch/android/VectorApplication;

    .line 428
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    .line 430
    .local v0, "appInstallManager":Lcom/vectorwatch/android/utils/AppInstallManager;
    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$context:Landroid/content/Context;

    sget-object v10, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-static {v9, v10}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineApps(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;)Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v3

    .line 431
    .local v3, "getLocalAppWatchFaceList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$context:Landroid/content/Context;

    sget-object v10, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-static {v9, v10}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineApps(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;)Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v2

    .line 433
    .local v2, "getLocalAppList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    if-ge v4, v9, :cond_0

    .line 434
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 438
    :cond_0
    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$appList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v4, v9, -0x1

    :goto_1
    if-ltz v4, :cond_5

    .line 439
    const/4 v1, 0x0

    .line 440
    .local v1, "found":Z
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    if-ge v5, v9, :cond_2

    .line 441
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vectorwatch/android/models/StoreElement;

    .line 442
    .local v8, "storeElement":Lcom/vectorwatch/android/models/StoreElement;
    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$appList:Ljava/util/List;

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v10

    if-ne v9, v10, :cond_4

    .line 443
    sget-object v6, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 444
    .local v6, "option":Lcom/vectorwatch/android/models/cloud/AppsOption;
    invoke-virtual {v8}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 445
    sget-object v6, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 448
    :cond_1
    const/4 v1, 0x1

    .line 449
    invoke-virtual {v0, v8, v6}, Lcom/vectorwatch/android/utils/AppInstallManager;->installAppOffline(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 454
    .end local v6    # "option":Lcom/vectorwatch/android/models/cloud/AppsOption;
    .end local v8    # "storeElement":Lcom/vectorwatch/android/models/StoreElement;
    :cond_2
    if-nez v1, :cond_3

    .line 455
    new-instance v7, Lcom/vectorwatch/android/models/StoreElement;

    invoke-direct {v7}, Lcom/vectorwatch/android/models/StoreElement;-><init>()V

    .line 456
    .local v7, "placeholderElement":Lcom/vectorwatch/android/models/StoreElement;
    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$appList:Ljava/util/List;

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v7, v9}, Lcom/vectorwatch/android/models/StoreElement;->setId(I)V

    .line 457
    sget-object v9, Lcom/vectorwatch/android/models/cloud/AppsOption;->PLACEHOLDER:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/vectorwatch/android/models/StoreElement;->setType(Ljava/lang/String;)V

    .line 458
    const-string v9, ""

    invoke-virtual {v7, v9}, Lcom/vectorwatch/android/models/StoreElement;->setImage(Ljava/lang/String;)V

    .line 459
    const-string v9, ""

    invoke-virtual {v7, v9}, Lcom/vectorwatch/android/models/StoreElement;->setEditImage(Ljava/lang/String;)V

    .line 460
    const-string v9, ""

    invoke-virtual {v7, v9}, Lcom/vectorwatch/android/models/StoreElement;->setDescription(Ljava/lang/String;)V

    .line 461
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$appList:Ljava/util/List;

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$context:Landroid/content/Context;

    const v12, 0x7f09029b

    .line 462
    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$appList:Ljava/util/List;

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v11, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$context:Landroid/content/Context;

    .line 461
    invoke-static {v10, v9, v11}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/vectorwatch/android/models/StoreElement;->setName(Ljava/lang/String;)V

    .line 464
    sget-object v9, Lcom/vectorwatch/android/models/cloud/AppsOption;->PLACEHOLDER:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v0, v7, v9}, Lcom/vectorwatch/android/utils/AppInstallManager;->installAppOffline(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 438
    .end local v7    # "placeholderElement":Lcom/vectorwatch/android/models/StoreElement;
    :cond_3
    add-int/lit8 v4, v4, -0x1

    goto/16 :goto_1

    .line 440
    .restart local v8    # "storeElement":Lcom/vectorwatch/android/models/StoreElement;
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 468
    .end local v1    # "found":Z
    .end local v5    # "j":I
    .end local v8    # "storeElement":Lcom/vectorwatch/android/models/StoreElement;
    :cond_5
    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$context:Landroid/content/Context;

    iget-object v10, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$appList:Ljava/util/List;

    invoke-static {v9, v10}, Lcom/vectorwatch/android/utils/OfflineUtils;->removeBlacklistedApps(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 469
    iget-object v9, p0, Lcom/vectorwatch/android/utils/OfflineUtils$5;->val$context:Landroid/content/Context;

    invoke-static {v9}, Lcom/vectorwatch/android/utils/OfflineUtils;->removeStreamsOffline(Landroid/content/Context;)V

    .line 471
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v9

    new-instance v10, Lcom/vectorwatch/android/events/OfflineInstallCompleteEvent;

    invoke-direct {v10}, Lcom/vectorwatch/android/events/OfflineInstallCompleteEvent;-><init>()V

    invoke-virtual {v9, v10}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 472
    return-void
.end method
