.class public Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;
.super Ljava/lang/Object;
.source "StreamAuthCredentialsStatusListener.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;


# static fields
.field private static final REQUEST_CODE_STREAM_AUTHENTICATION:I = 0xca


# instance fields
.field private elemId:I

.field private mActivity:Landroid/app/Activity;

.field private stream:Lcom/vectorwatch/android/models/Stream;

.field private streamPosInList:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/vectorwatch/android/models/Stream;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->elemId:I

    .line 22
    iput-object p2, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->mActivity:Landroid/app/Activity;

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/vectorwatch/android/models/Stream;II)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p3, "streamPosInList"    # I
    .param p4, "elemId"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->elemId:I

    .line 27
    iput-object p2, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 28
    iput p3, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->streamPosInList:I

    .line 29
    iput p4, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->elemId:I

    .line 30
    iput-object p1, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->mActivity:Landroid/app/Activity;

    .line 31
    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public onSuccess()V
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AuthMethod;->getLoginUrl()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->streamPosInList:I

    iget v3, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->elemId:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->startOauthActivity(Lcom/vectorwatch/android/models/Stream;Ljava/lang/String;II)V

    .line 36
    return-void
.end method

.method public setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V
    .locals 1
    .param p1, "authMethod"    # Lcom/vectorwatch/android/models/AuthMethod;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/Stream;->setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V

    .line 45
    return-void
.end method

.method public startOauthActivity(Lcom/vectorwatch/android/models/Stream;Ljava/lang/String;II)V
    .locals 7
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "loginUrl"    # Ljava/lang/String;
    .param p3, "streamPosInList"    # I
    .param p4, "elementId"    # I

    .prologue
    .line 48
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/webview/OauthActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    .local v0, "loginActivity":Landroid/content/Intent;
    const-string v1, "login_url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const-string v1, "stream_place_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 51
    const-string v1, "watchface_elem_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 52
    const-string v1, "title"

    iget-object v2, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->mActivity:Landroid/app/Activity;

    const v3, 0x7f090169

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string v1, "element_uuid"

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    const-string v1, "element_type"

    const-string v2, "stream"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    iget-object v1, p0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->mActivity:Landroid/app/Activity;

    const/16 v2, 0xca

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 56
    return-void
.end method
