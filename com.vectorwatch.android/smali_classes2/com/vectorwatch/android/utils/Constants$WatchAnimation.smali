.class public final enum Lcom/vectorwatch/android/utils/Constants$WatchAnimation;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WatchAnimation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$WatchAnimation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

.field public static final enum DOWN_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

.field public static final enum DOWN_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

.field public static final enum LEFT_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

.field public static final enum LEFT_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

.field public static final enum NONE:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

.field public static final enum RIGHT_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

.field public static final enum RIGHT_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

.field public static final enum UP_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

.field public static final enum UP_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;


# instance fields
.field private final text:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 437
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    const-string v1, "NONE"

    const-string v2, "NONE"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->NONE:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    .line 438
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    const-string v1, "UP_IN"

    const-string v2, "UP_IN"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->UP_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    .line 439
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    const-string v1, "UP_OUT"

    const-string v2, "UP_OUT"

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->UP_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    .line 440
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    const-string v1, "DOWN_IN"

    const-string v2, "DOWN_IN"

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->DOWN_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    .line 441
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    const-string v1, "DOWN_OUT"

    const-string v2, "DOWN_OUT"

    invoke-direct {v0, v1, v8, v2}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->DOWN_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    .line 442
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    const-string v1, "RIGHT_IN"

    const/4 v2, 0x5

    const-string v3, "RIGHT_IN"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->RIGHT_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    .line 443
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    const-string v1, "RIGHT_OUT"

    const/4 v2, 0x6

    const-string v3, "RIGHT_OUT"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->RIGHT_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    .line 444
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    const-string v1, "LEFT_IN"

    const/4 v2, 0x7

    const-string v3, "LEFT_IN"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->LEFT_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    .line 445
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    const-string v1, "LEFT_OUT"

    const/16 v2, 0x8

    const-string v3, "LEFT_OUT"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->LEFT_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    .line 436
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->NONE:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->UP_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->UP_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->DOWN_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->DOWN_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->RIGHT_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->RIGHT_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->LEFT_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->LEFT_OUT:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 449
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 450
    iput-object p3, p0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->text:Ljava/lang/String;

    .line 451
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$WatchAnimation;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 482
    if-eqz p0, :cond_1

    .line 483
    invoke-static {}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->values()[Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 484
    .local v0, "s":Lcom/vectorwatch/android/utils/Constants$WatchAnimation;
    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 489
    .end local v0    # "s":Lcom/vectorwatch/android/utils/Constants$WatchAnimation;
    :goto_1
    return-object v0

    .line 483
    .restart local v0    # "s":Lcom/vectorwatch/android/utils/Constants$WatchAnimation;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 489
    .end local v0    # "s":Lcom/vectorwatch/android/utils/Constants$WatchAnimation;
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->NONE:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$WatchAnimation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 436
    const-class v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$WatchAnimation;
    .locals 1

    .prologue
    .line 436
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    return-object v0
.end method


# virtual methods
.method public getWatchValue()B
    .locals 2

    .prologue
    .line 454
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$WatchAnimation:[I

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 472
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 456
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 458
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 460
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 462
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 464
    :pswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 466
    :pswitch_5
    const/4 v0, 0x6

    goto :goto_0

    .line 468
    :pswitch_6
    const/4 v0, 0x7

    goto :goto_0

    .line 470
    :pswitch_7
    const/16 v0, 0x8

    goto :goto_0

    .line 454
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->text:Ljava/lang/String;

    return-object v0
.end method
