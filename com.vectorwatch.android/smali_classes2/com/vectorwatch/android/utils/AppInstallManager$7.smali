.class Lcom/vectorwatch/android/utils/AppInstallManager$7;
.super Ljava/lang/Object;
.source "AppInstallManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/AppInstallManager;->startConfiguringAppSettings(Lcom/vectorwatch/android/models/CloudElementSummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/utils/AppInstallManager;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/utils/AppInstallManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/utils/AppInstallManager;

    .prologue
    .line 663
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$7;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "i"    # I

    .prologue
    .line 666
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$7;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$7;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$7;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$500()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$7;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$7;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$600(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 668
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0901a5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xbb8

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$7;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 670
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$7;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$802(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 671
    return-void
.end method
