.class public final enum Lcom/vectorwatch/android/utils/Constants$VftpFileType;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VftpFileType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$VftpFileType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$VftpFileType;

.field public static final enum FILE_TYPE_ANY:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

.field public static final enum FILE_TYPE_APPLICATION:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

.field public static final enum FILE_TYPE_LOCALE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

.field public static final enum FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

.field public static final enum FILE_TYPE_RESOURCE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

.field public static final enum FILE_TYPE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

.field public static final enum FILE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpFileType;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 673
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const-string v1, "FILE_TYPE_ANY"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_ANY:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 674
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const-string v1, "FILE_TYPE_RESOURCE"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_RESOURCE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 675
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const-string v1, "FILE_TYPE_APPLICATION"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_APPLICATION:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 676
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const-string v1, "FILE_TYPE_LOCALE_TMP"

    invoke-direct {v0, v1, v7, v7}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 677
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const-string v1, "FILE_TYPE_LOCALE"

    invoke-direct {v0, v1, v8, v8}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 678
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const-string v1, "FILE_TYPE_TMP"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 680
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const-string v1, "FILE_TYPE_UNKNOWN"

    const/4 v2, 0x6

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 672
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_ANY:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_RESOURCE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_APPLICATION:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 684
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 685
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->val:I

    .line 686
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 672
    const-class v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .locals 1

    .prologue
    .line 672
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$VftpFileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 689
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->val:I

    return v0
.end method
