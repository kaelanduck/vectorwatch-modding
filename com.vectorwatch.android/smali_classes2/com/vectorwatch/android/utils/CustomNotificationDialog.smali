.class public Lcom/vectorwatch/android/utils/CustomNotificationDialog;
.super Ljava/lang/Object;
.source "CustomNotificationDialog.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Landroid/content/Context;Landroid/view/LayoutInflater;Ljava/lang/String;)Landroid/support/v7/app/AlertDialog$Builder;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "layoutInflater"    # Landroid/view/LayoutInflater;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 17
    const v1, 0x7f030040

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 18
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f100196

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 19
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    return-object v1
.end method
