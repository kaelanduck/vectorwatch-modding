.class public final enum Lcom/vectorwatch/android/utils/Constants$VftpStatus;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VftpStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$VftpStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$VftpStatus;

.field public static final enum VFTP_STATUS_ERROR:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

.field public static final enum VFTP_STATUS_FILE_EXISTS:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

.field public static final enum VFTP_STATUS_FILE_NOT_FOUND:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

.field public static final enum VFTP_STATUS_NO_SPACE:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

.field public static final enum VFTP_STATUS_SUCCESS:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

.field public static final enum VFTP_STATUS_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpStatus;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 634
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    const-string v1, "VFTP_STATUS_SUCCESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$VftpStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_SUCCESS:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .line 635
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    const-string v1, "VFTP_STATUS_NO_SPACE"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$VftpStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_NO_SPACE:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .line 636
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    const-string v1, "VFTP_STATUS_FILE_NOT_FOUND"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$VftpStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_FILE_NOT_FOUND:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .line 637
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    const-string v1, "VFTP_STATUS_FILE_EXISTS"

    invoke-direct {v0, v1, v7, v7}, Lcom/vectorwatch/android/utils/Constants$VftpStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_FILE_EXISTS:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .line 638
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    const-string v1, "VFTP_STATUS_ERROR"

    invoke-direct {v0, v1, v8, v8}, Lcom/vectorwatch/android/utils/Constants$VftpStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_ERROR:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .line 639
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    const-string v1, "VFTP_STATUS_UNKNOWN"

    const/4 v2, 0x5

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$VftpStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .line 633
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_SUCCESS:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_NO_SPACE:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_FILE_NOT_FOUND:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_FILE_EXISTS:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_ERROR:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 643
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 644
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->val:I

    .line 645
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$VftpStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 633
    const-class v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$VftpStatus;
    .locals 1

    .prologue
    .line 633
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$VftpStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 648
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->val:I

    return v0
.end method
