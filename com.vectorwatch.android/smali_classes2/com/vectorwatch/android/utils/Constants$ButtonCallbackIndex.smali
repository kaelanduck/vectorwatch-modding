.class public final enum Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ButtonCallbackIndex"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACKS_NUM:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_DOWN_DOUBLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_DOWN_LONG_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_DOWN_LONG_PRESS_REPEAT:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_DOWN_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_DOWN_RELEASE:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_DOWN_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_MIDDLE_DOUBLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_MIDDLE_LONG_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_MIDDLE_LONG_PRESS_REPEAT:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_MIDDLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_MIDDLE_RELEASE:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_MIDDLE_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_UP_DOUBLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_UP_LONG_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_UP_LONG_PRESS_REPEAT:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_UP_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_UP_RELEASE:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

.field public static final enum BUTTON_CALLBACK_ON_UP_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 538
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_UP_START_PRESS"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 539
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_UP_RELEASE"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_RELEASE:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 540
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_UP_PRESS"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 541
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_UP_LONG_PRESS"

    invoke-direct {v0, v1, v7, v7}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_LONG_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 542
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_UP_DOUBLE_PRESS"

    invoke-direct {v0, v1, v8, v8}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_DOUBLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 543
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_UP_LONG_PRESS_REPEAT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_LONG_PRESS_REPEAT:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 544
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_MIDDLE_START_PRESS"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 545
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_MIDDLE_RELEASE"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_RELEASE:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 546
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_MIDDLE_PRESS"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 547
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_MIDDLE_LONG_PRESS"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_LONG_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 548
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_MIDDLE_DOUBLE_PRESS"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_DOUBLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 549
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_MIDDLE_LONG_PRESS_REPEAT"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_LONG_PRESS_REPEAT:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 550
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_DOWN_START_PRESS"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 551
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_DOWN_RELEASE"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_RELEASE:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 552
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_DOWN_PRESS"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 553
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_DOWN_LONG_PRESS"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_LONG_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 554
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_DOWN_DOUBLE_PRESS"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_DOUBLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 555
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACK_ON_DOWN_LONG_PRESS_REPEAT"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_LONG_PRESS_REPEAT:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 556
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    const-string v1, "BUTTON_CALLBACKS_NUM"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACKS_NUM:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    .line 537
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_RELEASE:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_LONG_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_DOUBLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_LONG_PRESS_REPEAT:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_RELEASE:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_LONG_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_DOUBLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_MIDDLE_LONG_PRESS_REPEAT:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_RELEASE:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_LONG_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_DOUBLE_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_DOWN_LONG_PRESS_REPEAT:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACKS_NUM:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 560
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 561
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->value:I

    .line 562
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 565
    if-eqz p0, :cond_1

    .line 566
    invoke-static {}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->values()[Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 567
    .local v0, "s":Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;
    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 572
    .end local v0    # "s":Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;
    :goto_1
    return-object v0

    .line 566
    .restart local v0    # "s":Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 572
    .end local v0    # "s":Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->BUTTON_CALLBACK_ON_UP_START_PRESS:Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 537
    const-class v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;
    .locals 1

    .prologue
    .line 537
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;

    return-object v0
.end method
