.class public final enum Lcom/vectorwatch/android/utils/Helpers$TableType;
.super Ljava/lang/Enum;
.source "Helpers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Helpers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TableType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Helpers$TableType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Helpers$TableType;

.field public static final enum ACTIVITY:Lcom/vectorwatch/android/utils/Helpers$TableType;

.field public static final enum ALARMS:Lcom/vectorwatch/android/utils/Helpers$TableType;

.field public static final enum CLOUD:Lcom/vectorwatch/android/utils/Helpers$TableType;

.field public static final enum LOGS:Lcom/vectorwatch/android/utils/Helpers$TableType;

.field public static final enum STREAM:Lcom/vectorwatch/android/utils/Helpers$TableType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1369
    new-instance v0, Lcom/vectorwatch/android/utils/Helpers$TableType;

    const-string v1, "ACTIVITY"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers$TableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/Helpers$TableType;->ACTIVITY:Lcom/vectorwatch/android/utils/Helpers$TableType;

    new-instance v0, Lcom/vectorwatch/android/utils/Helpers$TableType;

    const-string v1, "CLOUD"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/utils/Helpers$TableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/Helpers$TableType;->CLOUD:Lcom/vectorwatch/android/utils/Helpers$TableType;

    new-instance v0, Lcom/vectorwatch/android/utils/Helpers$TableType;

    const-string v1, "STREAM"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/utils/Helpers$TableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/Helpers$TableType;->STREAM:Lcom/vectorwatch/android/utils/Helpers$TableType;

    new-instance v0, Lcom/vectorwatch/android/utils/Helpers$TableType;

    const-string v1, "ALARMS"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/utils/Helpers$TableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/Helpers$TableType;->ALARMS:Lcom/vectorwatch/android/utils/Helpers$TableType;

    new-instance v0, Lcom/vectorwatch/android/utils/Helpers$TableType;

    const-string v1, "LOGS"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/utils/Helpers$TableType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/Helpers$TableType;->LOGS:Lcom/vectorwatch/android/utils/Helpers$TableType;

    .line 1368
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Helpers$TableType;

    sget-object v1, Lcom/vectorwatch/android/utils/Helpers$TableType;->ACTIVITY:Lcom/vectorwatch/android/utils/Helpers$TableType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/utils/Helpers$TableType;->CLOUD:Lcom/vectorwatch/android/utils/Helpers$TableType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/utils/Helpers$TableType;->STREAM:Lcom/vectorwatch/android/utils/Helpers$TableType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Helpers$TableType;->ALARMS:Lcom/vectorwatch/android/utils/Helpers$TableType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Helpers$TableType;->LOGS:Lcom/vectorwatch/android/utils/Helpers$TableType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vectorwatch/android/utils/Helpers$TableType;->$VALUES:[Lcom/vectorwatch/android/utils/Helpers$TableType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1368
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Helpers$TableType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1368
    const-class v0, Lcom/vectorwatch/android/utils/Helpers$TableType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Helpers$TableType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Helpers$TableType;
    .locals 1

    .prologue
    .line 1368
    sget-object v0, Lcom/vectorwatch/android/utils/Helpers$TableType;->$VALUES:[Lcom/vectorwatch/android/utils/Helpers$TableType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Helpers$TableType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Helpers$TableType;

    return-object v0
.end method
