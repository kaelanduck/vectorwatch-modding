.class public final enum Lcom/vectorwatch/android/utils/Constants$ElementType;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ElementType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$ElementType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_ANALOG_DAY:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_BACKGROUND:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_BITMAP:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_CLOUD_LONG_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_CLOUD_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_DIGITAL_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_HOUR_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_LINE:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_LIVE_STREAM:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_MINUTE_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_NORMAL_HOUR_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_NORMAL_MINUTE_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_NORMAL_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_STATIC_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_SYSTEM_VARS:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field public static final enum PROT_ELEMENT_TYPE_TIMEZONE:Lcom/vectorwatch/android/utils/Constants$ElementType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 110
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_NONE"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 111
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_TEXT"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 112
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_HOUR_HAND"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_HOUR_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 113
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_MINUTE_HAND"

    invoke-direct {v0, v1, v7, v7}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_MINUTE_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 114
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_BITMAP"

    invoke-direct {v0, v1, v8, v8}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_BITMAP:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 115
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_BACKGROUND"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_BACKGROUND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 116
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_DIGITAL_TIME"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_DIGITAL_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 117
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_CLOUD_LONG_TEXT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_LONG_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 118
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_LINE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_LINE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 119
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_NORMAL_SECOND_HAND"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NORMAL_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 120
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_NORMAL_MINUTE_HAND"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NORMAL_MINUTE_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 121
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_SYSTEM_VARS"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_SYSTEM_VARS:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 122
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_NORMAL_HOUR_HAND"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NORMAL_HOUR_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 123
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_SECOND_HAND"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 124
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_STATIC_TIME"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_STATIC_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 125
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_CLOUD_TEXT"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 126
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_ANALOG_DAY"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_ANALOG_DAY:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 127
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_DYNAMIC_IMAGE"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 128
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_TIMEZONE"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_TIMEZONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 129
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    const-string v1, "PROT_ELEMENT_TYPE_LIVE_STREAM"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_LIVE_STREAM:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 109
    const/16 v0, 0x14

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$ElementType;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_HOUR_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_MINUTE_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_BITMAP:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_BACKGROUND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_DIGITAL_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_LONG_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_LINE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NORMAL_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NORMAL_MINUTE_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_SYSTEM_VARS:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NORMAL_HOUR_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_STATIC_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_ANALOG_DAY:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_TIMEZONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_LIVE_STREAM:Lcom/vectorwatch/android/utils/Constants$ElementType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$ElementType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 133
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$ElementType;->value:I

    .line 134
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$ElementType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 109
    const-class v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$ElementType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$ElementType;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$ElementType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$ElementType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$ElementType;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$ElementType;->value:I

    return v0
.end method
