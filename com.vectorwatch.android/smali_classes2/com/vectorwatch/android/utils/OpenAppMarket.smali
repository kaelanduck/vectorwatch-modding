.class public Lcom/vectorwatch/android/utils/OpenAppMarket;
.super Ljava/lang/Object;
.source "OpenAppMarket.java"


# static fields
.field public static final MARKET_AMAZON_URL:Ljava/lang/String; = "amzn://apps/android?p="

.field public static final MARKET_GOOGLE_URL:Ljava/lang/String; = "market://details?id="

.field public static final MARKET_SAMSUNG_URL:Ljava/lang/String; = "samsungapps://ProductDetail/"

.field public static final WEB_AMAZON_URL:Ljava/lang/String; = "http://www.amazon.com/gp/mas/dl/android?p="

.field public static final WEB_GOOGLE_URL:Ljava/lang/String; = "http://play.google.com/store/apps/details?id="

.field public static final WEB_SAMSUNG_URL:Ljava/lang/String; = "http://www.samsungapps.com/appquery/appDetail.as?mAppId="


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static openOnAmazonMarket(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 40
    const-string v0, "amzn://apps/android?p="

    const-string v1, "http://www.amazon.com/gp/mas/dl/android?p="

    invoke-static {v0, v1, p0, p1}, Lcom/vectorwatch/android/utils/OpenAppMarket;->openOnMarket(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public static openOnGooglePlayMarket(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 32
    const-string v0, "market://details?id="

    const-string v1, "http://play.google.com/store/apps/details?id="

    invoke-static {v0, v1, p0, p1}, Lcom/vectorwatch/android/utils/OpenAppMarket;->openOnMarket(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public static openOnMarket(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "market"    # Ljava/lang/String;
    .param p1, "web"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    const/high16 v4, 0x10000000

    .line 21
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 22
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 23
    invoke-virtual {p2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    :goto_0
    return-void

    .line 24
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 25
    .local v0, "anfe":Landroid/content/ActivityNotFoundException;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 26
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 27
    invoke-virtual {p2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static openOnSamsungMarket(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 36
    const-string v0, "samsungapps://ProductDetail/"

    const-string v1, "http://www.samsungapps.com/appquery/appDetail.as?mAppId="

    invoke-static {v0, v1, p0, p1}, Lcom/vectorwatch/android/utils/OpenAppMarket;->openOnMarket(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 37
    return-void
.end method
