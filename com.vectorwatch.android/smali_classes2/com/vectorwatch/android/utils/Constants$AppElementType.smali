.class public final enum Lcom/vectorwatch/android/utils/Constants$AppElementType;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppElementType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$AppElementType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_BITMAP_HAND:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_GAUGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_LINE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_LIST:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_NORMAL_HAND:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_NUM:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_RECTANGLE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_TEXT:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_TEXT_COMPLICATION:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field public static final enum ELEMENT_TYPE_TEXT_MULTIPLE_LINES:Lcom/vectorwatch/android/utils/Constants$AppElementType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 190
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_NONE"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 191
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_TEXT"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 192
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_LINE"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_LINE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 193
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_IMAGE"

    invoke-direct {v0, v1, v7, v7}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 194
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_RECTANGLE"

    invoke-direct {v0, v1, v8, v8}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_RECTANGLE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 195
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_NORMAL_HAND"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NORMAL_HAND:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 196
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_BITMAP_HAND"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_BITMAP_HAND:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 197
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_TEXT_MULTIPLE_LINES"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_MULTIPLE_LINES:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 198
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_TEXT_COMPLICATION"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_COMPLICATION:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 199
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_LIST"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_LIST:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 200
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_GAUGE"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_GAUGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 201
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_DYNAMIC_IMAGE"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 202
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    const-string v1, "ELEMENT_TYPE_NUM"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$AppElementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NUM:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 189
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$AppElementType;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_LINE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_RECTANGLE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NORMAL_HAND:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_BITMAP_HAND:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_MULTIPLE_LINES:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_COMPLICATION:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_LIST:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_GAUGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NUM:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$AppElementType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 206
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->value:I

    .line 207
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$AppElementType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 189
    const-class v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$AppElementType;
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$AppElementType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$AppElementType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$AppElementType;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->value:I

    return v0
.end method
