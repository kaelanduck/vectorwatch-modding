.class public final enum Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;
.super Ljava/lang/Enum;
.source "LocationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/LocationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RequestAccuracy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

.field public static final enum ACCURATE_GPS:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

.field public static final enum NORMAL_WIFI:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;


# instance fields
.field private locationProvider:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    const-string v1, "NORMAL_WIFI"

    const-string v2, "network"

    invoke-direct {v0, v1, v3, v2}, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->NORMAL_WIFI:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    .line 32
    new-instance v0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    const-string v1, "ACCURATE_GPS"

    const-string v2, "gps"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->ACCURATE_GPS:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    sget-object v1, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->NORMAL_WIFI:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->ACCURATE_GPS:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->$VALUES:[Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "locationProvider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput-object p3, p0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->locationProvider:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->$VALUES:[Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    return-object v0
.end method


# virtual methods
.method getLocationProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->locationProvider:Ljava/lang/String;

    return-object v0
.end method
