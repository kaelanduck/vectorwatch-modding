.class final Lcom/vectorwatch/android/utils/MigrationHelper$1;
.super Ljava/lang/Object;
.source "MigrationHelper.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/MigrationHelper;->executeMigration(ILandroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/LoginResponseModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$accountManager:Landroid/accounts/AccountManager;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/vectorwatch/android/utils/MigrationHelper$1;->val$accountManager:Landroid/accounts/AccountManager;

    iput-object p2, p0, Lcom/vectorwatch/android/utils/MigrationHelper$1;->val$account:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/vectorwatch/android/utils/MigrationHelper$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 2
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 163
    invoke-static {}, Lcom/vectorwatch/android/utils/MigrationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "AUTHENTICATOR: Error at login."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/LoginResponseModel;Lretrofit/client/Response;)V
    .locals 4
    .param p1, "loginResponseModel"    # Lcom/vectorwatch/android/models/LoginResponseModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 147
    invoke-static {}, Lcom/vectorwatch/android/utils/MigrationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "MIGRATION: RE-LOGIN: Relogin success"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/LoginResponseModel;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "authToken":Ljava/lang/String;
    const-string v1, "Full access"

    .line 151
    .local v1, "authTokenType":Ljava/lang/String;
    iget-object v2, p0, Lcom/vectorwatch/android/utils/MigrationHelper$1;->val$accountManager:Landroid/accounts/AccountManager;

    if-eqz v2, :cond_0

    .line 152
    invoke-static {}, Lcom/vectorwatch/android/utils/MigrationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "MIGRATION: RE-LOGIN: Save new token"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 153
    iget-object v2, p0, Lcom/vectorwatch/android/utils/MigrationHelper$1;->val$accountManager:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lcom/vectorwatch/android/utils/MigrationHelper$1;->val$account:Landroid/accounts/Account;

    invoke-virtual {v2, v3, v1, v0}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :goto_0
    return-void

    .line 155
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/utils/MigrationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "MIGRATION: RE-LOGIN: No account manager to save in"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 157
    iget-object v2, p0, Lcom/vectorwatch/android/utils/MigrationHelper$1;->val$context:Landroid/content/Context;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->logoutDrivenResets(Landroid/content/Context;)Z

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 144
    check-cast p1, Lcom/vectorwatch/android/models/LoginResponseModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/utils/MigrationHelper$1;->success(Lcom/vectorwatch/android/models/LoginResponseModel;Lretrofit/client/Response;)V

    return-void
.end method
