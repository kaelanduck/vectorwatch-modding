.class Lcom/vectorwatch/android/utils/LocationHelper$4;
.super Ljava/lang/Object;
.source "LocationHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/LocationHelper;->queryLocationManagerWithDelay()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/utils/LocationHelper;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/utils/LocationHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/utils/LocationHelper;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 182
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    if-eqz v1, :cond_4

    .line 183
    invoke-static {}, Lcom/vectorwatch/android/utils/LocationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "LOCATION HELPER: Location query"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 184
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/LocationHelper;->access$100(Lcom/vectorwatch/android/utils/LocationHelper;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/LocationHelper;->access$102(Lcom/vectorwatch/android/utils/LocationHelper;J)J

    .line 186
    invoke-static {}, Lcom/vectorwatch/android/utils/LocationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "LOCATION HELPER: First location query"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/LocationHelper;->access$200(Lcom/vectorwatch/android/utils/LocationHelper;)Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->getLocationProvider()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 189
    .local v0, "location":Landroid/location/Location;
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    const-string v2, "Requesting location..."

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/LocationHelper;->access$300(Lcom/vectorwatch/android/utils/LocationHelper;Ljava/lang/String;)V

    .line 190
    if-eqz v0, :cond_7

    .line 191
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v2, v2, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    invoke-virtual {v1, v0}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_6

    .line 192
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/utils/LocationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "LOCATION HELPER: Last known location is retrieved"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 193
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    if-eqz v1, :cond_2

    .line 195
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    invoke-interface {v1, v0}, Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;->onLocation(Landroid/location/Location;)V

    .line 198
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v6, :cond_5

    sget-boolean v1, Lcom/vectorwatch/android/utils/LocationHelper;->USE_LOCATION_CACHE_IF_AVAILABLE:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    .line 200
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/utils/LocationHelper;->isAccurate(Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 201
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    if-eqz v1, :cond_3

    .line 202
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    invoke-interface {v1, v0}, Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;->onAccurateLocation(Landroid/location/Location;)V

    .line 221
    :cond_3
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/LocationHelper;->access$100(Lcom/vectorwatch/android/utils/LocationHelper;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v1, v2

    sget v2, Lcom/vectorwatch/android/utils/LocationHelper;->LOCATION_TIMEOUT_SECONDS:F

    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_8

    .line 222
    invoke-static {}, Lcom/vectorwatch/android/utils/LocationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "LOCATION HELPER: Location query will run again next time"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/LocationHelper;->access$500(Lcom/vectorwatch/android/utils/LocationHelper;)V

    .line 237
    .end local v0    # "location":Landroid/location/Location;
    :cond_4
    :goto_1
    return-void

    .line 205
    .restart local v0    # "location":Landroid/location/Location;
    :cond_5
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/LocationHelper;->onNewLocationAdded()V

    goto :goto_0

    .line 208
    :cond_6
    invoke-static {}, Lcom/vectorwatch/android/utils/LocationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "LOCATION HELPER: Last known location is the same as last one"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_7
    invoke-static {}, Lcom/vectorwatch/android/utils/LocationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "LOCATION HELPER: Last known location is null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 212
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/LocationHelper;->access$400(Lcom/vectorwatch/android/utils/LocationHelper;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 213
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-static {v1, v6}, Lcom/vectorwatch/android/utils/LocationHelper;->access$402(Lcom/vectorwatch/android/utils/LocationHelper;Z)Z

    .line 214
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v2, "network"

    iget-object v3, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v1, v2, v3, v7}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 215
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/LocationHelper;->access$200(Lcom/vectorwatch/android/utils/LocationHelper;)Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->ACCURATE_GPS:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    if-ne v1, v2, :cond_3

    .line 216
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v2, "gps"

    iget-object v3, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v1, v2, v3, v7}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    goto :goto_0

    .line 225
    :cond_8
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    if-eqz v1, :cond_4

    .line 226
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    .line 227
    invoke-static {}, Lcom/vectorwatch/android/utils/LocationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "LOCATION HELPER: TIMEOUT - Sent last location retrieved"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 228
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v2, v1, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    iget-object v3, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v3, v3, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    invoke-interface {v2, v1}, Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;->onAccurateLocation(Landroid/location/Location;)V

    goto :goto_1

    .line 230
    :cond_9
    invoke-static {}, Lcom/vectorwatch/android/utils/LocationHelper;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "LOCATION HELPER: TIMEOUT - No location retrieved"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    const-string v2, "Location timeout"

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/LocationHelper;->access$300(Lcom/vectorwatch/android/utils/LocationHelper;Ljava/lang/String;)V

    .line 232
    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper$4;->this$0:Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, v1, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    invoke-interface {v1}, Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;->onTimeoutError()V

    goto/16 :goto_1
.end method
