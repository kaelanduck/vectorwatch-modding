.class public final enum Lcom/vectorwatch/android/utils/Constants$InstallStatus;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InstallStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$InstallStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$InstallStatus;

.field public static final enum INSTALL_STATUS_NO_SPACE:Lcom/vectorwatch/android/utils/Constants$InstallStatus;

.field public static final enum INSTALL_STATUS_SUCCESS:Lcom/vectorwatch/android/utils/Constants$InstallStatus;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 616
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    const-string v1, "INSTALL_STATUS_SUCCESS"

    invoke-direct {v0, v1, v2, v2}, Lcom/vectorwatch/android/utils/Constants$InstallStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->INSTALL_STATUS_SUCCESS:Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    .line 617
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    const-string v1, "INSTALL_STATUS_NO_SPACE"

    invoke-direct {v0, v1, v3, v3}, Lcom/vectorwatch/android/utils/Constants$InstallStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->INSTALL_STATUS_NO_SPACE:Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    .line 615
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->INSTALL_STATUS_SUCCESS:Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->INSTALL_STATUS_NO_SPACE:Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 621
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 622
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->val:I

    .line 623
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$InstallStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 615
    const-class v0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$InstallStatus;
    .locals 1

    .prologue
    .line 615
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$InstallStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 626
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->val:I

    return v0
.end method
