.class Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;
.super Ljava/lang/Object;
.source "AppDataRequesterHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/AppDataRequesterHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueueManager"
.end annotation


# instance fields
.field private queues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Queue",
            "<",
            "Lcom/vectorwatch/android/utils/AppDataRequesterHelper;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 554
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->queues:Ljava/util/Map;

    .line 555
    return-void
.end method


# virtual methods
.method public addRequestToQueue(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;
    .locals 3
    .param p1, "request"    # Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    .prologue
    .line 560
    iget v1, p1, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 561
    .local v0, "appId":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->queues:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 562
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->queues:Ljava/util/Map;

    new-instance v2, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v2}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->queues:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :goto_0
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->handleAppQueue(Ljava/lang/Integer;)V

    .line 572
    return-object p0

    .line 567
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public contains(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)Z
    .locals 2
    .param p1, "request"    # Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    .prologue
    .line 600
    iget v1, p1, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 601
    .local v0, "appId":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->queues:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 602
    const/4 v1, 0x0

    .line 604
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->queues:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public handleAppQueue(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "appId"    # Ljava/lang/Integer;

    .prologue
    .line 576
    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->queues:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    .line 577
    .local v0, "appQueue":Ljava/util/Queue;
    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    .line 578
    .local v1, "topRequest":Lcom/vectorwatch/android/utils/AppDataRequesterHelper;
    if-eqz v1, :cond_0

    .line 579
    iget-boolean v2, v1, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callWasLaunched:Z

    if-nez v2, :cond_0

    .line 580
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->performDataRequest()V

    .line 583
    :cond_0
    return-void
.end method

.method public removeRequestFromQueue(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;
    .locals 3
    .param p1, "request"    # Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    .prologue
    .line 586
    iget v2, p1, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 587
    .local v0, "appId":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->queues:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 588
    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->queues:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Queue;

    .line 589
    .local v1, "appQueue":Ljava/util/Queue;
    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_2

    .line 590
    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 594
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->handleAppQueue(Ljava/lang/Integer;)V

    .line 596
    .end local v1    # "appQueue":Ljava/util/Queue;
    :cond_1
    return-object p0

    .line 591
    .restart local v1    # "appQueue":Ljava/util/Queue;
    :cond_2
    invoke-interface {v1, p1}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 592
    invoke-interface {v1, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
