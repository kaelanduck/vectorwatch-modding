.class final Lcom/vectorwatch/android/utils/Helpers$1;
.super Ljava/lang/Object;
.source "Helpers.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$alert:Landroid/support/v7/app/AlertDialog;


# direct methods
.method constructor <init>(Landroid/support/v7/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 1238
    iput-object p1, p0, Lcom/vectorwatch/android/utils/Helpers$1;->val$alert:Landroid/support/v7/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1242
    :try_start_0
    iget-object v1, p0, Lcom/vectorwatch/android/utils/Helpers$1;->val$alert:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1243
    iget-object v1, p0, Lcom/vectorwatch/android/utils/Helpers$1;->val$alert:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1250
    :cond_0
    :goto_0
    return-void

    .line 1245
    :catch_0
    move-exception v0

    .line 1248
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Dismiss alert error"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method
