.class public Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;
.super Ljava/lang/Object;
.source "AppInstallManager.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/AppInstallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "StreamAuthMethodStatusListener"
.end annotation


# instance fields
.field private stream:Lcom/vectorwatch/android/models/Stream;

.field final synthetic this$0:Lcom/vectorwatch/android/utils/AppInstallManager;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/Stream;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p2, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 477
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478
    iput-object p2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 479
    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 513
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - stream auth method status listener - on failure "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 514
    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$900(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$900(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 513
    invoke-interface {v1, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$1000(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/Stream;)V

    .line 517
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->handleStreamTutorial(Lcom/vectorwatch/android/models/Stream;)V

    .line 518
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v3, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 520
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v0, v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$902(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/Stream;)Lcom/vectorwatch/android/models/Stream;

    .line 521
    return-void

    .line 514
    :cond_1
    const-string v0, "null"

    goto :goto_0
.end method

.method public onSuccess()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 483
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$300(Lcom/vectorwatch/android/utils/AppInstallManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 509
    :goto_0
    return-void

    .line 487
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 488
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "INSTALL MNGR - Activity context is null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 489
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090102

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 490
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 494
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AuthMethod;->getLoginUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AuthMethod;->getLoginUrl()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 495
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "INSTALL MNGR - auth login url empty or null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 496
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$600(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 497
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090115

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 498
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$802(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)Lcom/vectorwatch/android/models/CloudElementSummary;

    goto :goto_0

    .line 502
    :cond_3
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "INSTALL MNGR - stream auth method status listener - on success "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 503
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/ui/webview/OauthActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 504
    .local v0, "loginActivity":Landroid/content/Intent;
    const-string v1, "login_url"

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Stream;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/AuthMethod;->getLoginUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 505
    const-string v1, "element_uuid"

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 506
    const-string v1, "element_type"

    const-string v2, "stream"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 507
    const-string v1, "title"

    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090169

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 508
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x67

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method public setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V
    .locals 1
    .param p1, "authMethod"    # Lcom/vectorwatch/android/models/AuthMethod;

    .prologue
    .line 524
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/Stream;->setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V

    .line 525
    return-void
.end method
