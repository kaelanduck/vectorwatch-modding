.class public final enum Lcom/vectorwatch/android/utils/Constants$RepeatOption;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RepeatOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$RepeatOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$RepeatOption;

.field public static final enum FRIDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

.field public static final enum MONDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

.field public static final enum SATURDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

.field public static final enum SUNDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

.field public static final enum THURSDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

.field public static final enum TUESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

.field public static final enum WEDNESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 602
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    const-string v1, "SUNDAY"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SUNDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    const-string v1, "MONDAY"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->MONDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    const-string v1, "TUESDAY"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->TUESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    const-string v1, "WEDNESDAY"

    invoke-direct {v0, v1, v7, v7}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->WEDNESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    const-string v1, "THURSDAY"

    invoke-direct {v0, v1, v8, v8}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->THURSDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    const-string v1, "FRIDAY"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->FRIDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    const-string v1, "SATURDAY"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SATURDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    .line 601
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SUNDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->MONDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->TUESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->WEDNESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->THURSDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->FRIDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SATURDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 606
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 607
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->val:I

    .line 608
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$RepeatOption;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 601
    const-class v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$RepeatOption;
    .locals 1

    .prologue
    .line 601
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$RepeatOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 611
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->val:I

    return v0
.end method
