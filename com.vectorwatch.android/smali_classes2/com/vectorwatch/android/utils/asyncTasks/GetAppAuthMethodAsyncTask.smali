.class public Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;
.super Landroid/os/AsyncTask;
.source "GetAppAuthMethodAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field app:Lcom/vectorwatch/android/models/CloudElementSummary;

.field callIsOk:Z

.field private listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

.field private mProgressBar:Landroid/view/View;

.field pContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;Landroid/view/View;)V
    .locals 1
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "listener"    # Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;
    .param p4, "progressBar"    # Landroid/view/View;

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 38
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    .line 39
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 40
    iput-object p4, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->mProgressBar:Landroid/view/View;

    .line 41
    iput-object p3, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

    .line 42
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/content/Intent;
    .locals 11
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x1

    .line 48
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    const-string v9, "AT_GetAppAuthMethodAsyncTask"

    invoke-virtual {v8, v9}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 50
    iget-object v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-object v7

    .line 56
    :cond_1
    :try_start_0
    iget-object v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v8}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 57
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v8, "com.vectorwatch.android"

    invoke-virtual {v1, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 59
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v8, v2

    if-ge v8, v10, :cond_2

    .line 60
    sget-object v8, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v9, "There should be at least one account in Accounts & sync"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 62
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 63
    .local v7, "res":Landroid/content/Intent;
    const-string v8, "ERR_MSG"

    const v9, 0x7f09010d

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 89
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v7    # "res":Landroid/content/Intent;
    :catch_0
    move-exception v6

    .line 90
    .local v6, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v6}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 97
    .end local v6    # "e":Landroid/accounts/OperationCanceledException;
    :goto_1
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 98
    .restart local v7    # "res":Landroid/content/Intent;
    goto :goto_0

    .line 68
    .end local v7    # "res":Landroid/content/Intent;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_2
    const/4 v8, 0x0

    :try_start_1
    aget-object v0, v2, v8

    .line 70
    .local v0, "account":Landroid/accounts/Account;
    const-string v8, "Full access"

    const/4 v9, 0x1

    invoke-virtual {v1, v0, v8, v9}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 72
    .local v4, "authToken":Ljava/lang/String;
    iget-object v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    .line 73
    .local v5, "context":Landroid/content/Context;
    if-eqz v5, :cond_0

    .line 76
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->callIsOk:Z
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_3

    .line 78
    :try_start_2
    iget-object v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v4}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->getAppAuthMethod(Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v3

    .line 81
    .local v3, "authMethod":Lcom/vectorwatch/android/models/AuthMethod;
    iget-object v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v8, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V

    .line 83
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->callIsOk:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 84
    .end local v3    # "authMethod":Lcom/vectorwatch/android/models/AuthMethod;
    :catch_1
    move-exception v6

    .line 85
    .local v6, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v8, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v9, "Error in getting app auth method "

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 86
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->callIsOk:Z
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    .line 91
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v4    # "authToken":Ljava/lang/String;
    .end local v5    # "context":Landroid/content/Context;
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v6

    .line 92
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 93
    .end local v6    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v6

    .line 94
    .local v6, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v6}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->doInBackground([Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->callIsOk:Z

    if-eqz v0, :cond_1

    .line 114
    const-string v0, "ERR_MSG"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 115
    sget-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error at downloading system apps: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ERR_MSG"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;->onFailure()V

    .line 128
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->callIsOk:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 129
    sget-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v1, "Get Stream Auth Method call did not end as expected. Exception has been caught."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;->onFailure()V

    .line 133
    :cond_2
    return-void

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;->onSuccess()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 106
    :cond_0
    return-void
.end method
