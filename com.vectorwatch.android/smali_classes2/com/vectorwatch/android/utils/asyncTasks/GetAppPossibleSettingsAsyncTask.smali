.class public Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;
.super Landroid/os/AsyncTask;
.source "GetAppPossibleSettingsAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field app:Lcom/vectorwatch/android/models/CloudElementSummary;

.field callIsOk:Z

.field private hasAuthError:Z

.field private listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

.field private mProgressBar:Landroid/view/View;

.field private model:Lcom/vectorwatch/android/models/AuthCredentialsRequest;

.field pContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;Landroid/view/View;)V
    .locals 1
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "listener"    # Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;
    .param p4, "progressBar"    # Landroid/view/View;

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->hasAuthError:Z

    .line 46
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    .line 47
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 48
    iput-object p4, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    .line 49
    new-instance v0, Lcom/vectorwatch/android/models/AuthCredentialsRequest;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/AuthCredentialsRequest;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->model:Lcom/vectorwatch/android/models/AuthCredentialsRequest;

    .line 50
    iput-object p3, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

    .line 51
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/content/Intent;
    .locals 12
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x1

    .line 63
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    const-string v10, "AT_GetAppPossibleSettingsAsyncTask"

    invoke-virtual {v9, v10}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 65
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-object v7

    .line 71
    :cond_1
    :try_start_0
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {v9}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 72
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v9, "com.vectorwatch.android"

    invoke-virtual {v1, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 74
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v9, v2

    if-ge v9, v11, :cond_3

    .line 75
    sget-object v9, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v10, "There should be at least one account in Accounts & sync"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 77
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 78
    .local v7, "res":Landroid/content/Intent;
    const-string v9, "ERR_MSG"

    const v10, 0x7f09010d

    invoke-virtual {v7, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 106
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v7    # "res":Landroid/content/Intent;
    :catch_0
    move-exception v5

    .line 107
    .local v5, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v5}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 114
    .end local v5    # "e":Landroid/accounts/OperationCanceledException;
    :cond_2
    :goto_1
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 115
    .restart local v7    # "res":Landroid/content/Intent;
    goto :goto_0

    .line 83
    .end local v7    # "res":Landroid/content/Intent;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_3
    const/4 v9, 0x0

    :try_start_1
    aget-object v0, v2, v9

    .line 85
    .local v0, "account":Landroid/accounts/Account;
    const-string v9, "Full access"

    const/4 v10, 0x1

    invoke-virtual {v1, v0, v9, v10}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 87
    .local v3, "authToken":Ljava/lang/String;
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    .line 88
    .local v4, "context":Landroid/content/Context;
    if-eqz v4, :cond_0

    .line 91
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->callIsOk:Z
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_4

    .line 94
    :try_start_2
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->model:Lcom/vectorwatch/android/models/AuthCredentialsRequest;

    invoke-static {v9, v10, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->getAppSettings(Ljava/lang/String;Lcom/vectorwatch/android/models/AuthCredentialsRequest;Ljava/lang/String;)Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v6

    .line 95
    .local v6, "possibleSettings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v9, v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAppPossibleSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V

    .line 96
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->callIsOk:Z
    :try_end_2
    .catch Lretrofit/RetrofitError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 97
    .end local v6    # "possibleSettings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    :catch_1
    move-exception v8

    .line 98
    .local v8, "retrofitError":Lretrofit/RetrofitError;
    :try_start_3
    invoke-virtual {v8}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v8}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v9

    invoke-virtual {v9}, Lretrofit/client/Response;->getStatus()I

    move-result v9

    const/16 v10, 0x385

    if-ne v9, v10, :cond_2

    .line 99
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->hasAuthError:Z
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_1

    .line 108
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v8    # "retrofitError":Lretrofit/RetrofitError;
    :catch_2
    move-exception v5

    .line 109
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 101
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    .restart local v3    # "authToken":Ljava/lang/String;
    .restart local v4    # "context":Landroid/content/Context;
    :catch_3
    move-exception v5

    .line 102
    .local v5, "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v9, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v10, "Error in getting app settings"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 103
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->callIsOk:Z
    :try_end_4
    .catch Landroid/accounts/OperationCanceledException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_1

    .line 110
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    .line 111
    .local v5, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v5}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->doInBackground([Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->callIsOk:Z

    if-eqz v0, :cond_1

    .line 131
    const-string v0, "ERR_MSG"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 132
    sget-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error at downloading system apps: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ERR_MSG"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;->onFailure()V

    .line 140
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->callIsOk:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 142
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;->onFailure()V

    .line 143
    sget-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v1, "Get App Settings call did not end as expected. Exception has been caught."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 147
    :cond_2
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->hasAuthError:Z

    if-eqz v0, :cond_3

    .line 148
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 150
    :cond_3
    return-void

    .line 136
    :cond_4
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->listener:Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;->onSuccess()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    :cond_0
    return-void
.end method

.method public setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;
    .locals 1
    .param p1, "location"    # Lcom/vectorwatch/android/models/VectorLocation;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->model:Lcom/vectorwatch/android/models/AuthCredentialsRequest;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/AuthCredentialsRequest;->setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/models/AuthCredentialsRequest;

    .line 56
    return-object p0
.end method
