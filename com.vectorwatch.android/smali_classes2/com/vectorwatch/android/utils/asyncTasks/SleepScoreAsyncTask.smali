.class public Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;
.super Landroid/os/AsyncTask;
.source "SleepScoreAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final HOUR_IN_SEC:I

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 31
    const/16 v0, 0xe10

    iput v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;->HOUR_IN_SEC:I

    .line 34
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;->mContext:Landroid/content/Context;

    .line 35
    return-void
.end method

.method private getScore(Ljava/util/List;)I
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p1, "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    const/high16 v13, 0x40e00000    # 7.0f

    const/high16 v12, 0x40c00000    # 6.0f

    const/16 v11, 0xa

    const/high16 v10, 0x40a00000    # 5.0f

    const/16 v9, 0x64

    .line 80
    const/4 v1, 0x0

    .line 81
    .local v1, "DSM":I
    const/4 v3, 0x0

    .line 82
    .local v3, "TSM":I
    const/4 v2, 0x0

    .line 83
    .local v2, "SCY":I
    const/4 v0, 0x0

    .line 85
    .local v0, "AWS":I
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_5

    .line 86
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getEffTime()I

    move-result v6

    .line 88
    .local v6, "value":I
    if-gt v6, v11, :cond_3

    .line 90
    add-int/lit8 v7, v4, -0x1

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getEffTime()I

    move-result v7

    if-le v7, v11, :cond_0

    add-int/lit8 v7, v4, -0x1

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getEffTime()I

    move-result v7

    const/16 v8, 0x63

    if-gt v7, v8, :cond_0

    .line 91
    add-int/lit8 v2, v2, 0x1

    .line 95
    :cond_0
    add-int/lit8 v7, v4, -0x1

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getEffTime()I

    move-result v7

    if-gt v7, v11, :cond_1

    .line 96
    add-int/lit8 v1, v1, 0xf

    .line 100
    :cond_1
    add-int/lit8 v7, v4, -0x1

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getEffTime()I

    move-result v7

    if-ge v7, v9, :cond_2

    .line 101
    add-int/lit8 v3, v3, 0xf

    .line 85
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 103
    :cond_3
    if-lt v6, v9, :cond_4

    .line 105
    add-int/lit8 v7, v4, -0x1

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getEffTime()I

    move-result v7

    if-ge v7, v9, :cond_2

    if-lez v3, :cond_2

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 110
    :cond_4
    add-int/lit8 v7, v4, -0x1

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getEffTime()I

    move-result v7

    if-ge v7, v9, :cond_2

    .line 111
    add-int/lit8 v3, v3, 0xf

    goto :goto_1

    .line 117
    .end local v6    # "value":I
    :cond_5
    add-int/lit8 v3, v3, 0xf

    .line 119
    const/16 v7, 0xc8

    if-ge v1, v7, :cond_6

    .line 120
    int-to-float v7, v3

    div-float/2addr v7, v13

    int-to-float v8, v2

    mul-float/2addr v8, v12

    add-float/2addr v7, v8

    int-to-float v8, v0

    mul-float/2addr v8, v10

    sub-float/2addr v7, v8

    rsub-int v8, v1, 0xc8

    int-to-float v8, v8

    div-float/2addr v8, v10

    sub-float v5, v7, v8

    .line 125
    .local v5, "score":F
    :goto_2
    const/4 v7, 0x5

    const/high16 v8, 0x41a00000    # 20.0f

    div-float v8, v5, v8

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    int-to-float v5, v7

    .line 127
    float-to-int v7, v5

    return v7

    .line 122
    .end local v5    # "score":F
    :cond_6
    const/high16 v7, 0x40000000    # 2.0f

    int-to-float v8, v3

    div-float/2addr v8, v13

    add-float/2addr v7, v8

    int-to-float v8, v2

    mul-float/2addr v8, v12

    add-float/2addr v7, v8

    int-to-float v8, v0

    mul-float/2addr v8, v10

    sub-float v5, v7, v8

    .restart local v5    # "score":F
    goto :goto_2
.end method

.method private getSleepForInterval(Landroid/content/ContentResolver;JJ)Ljava/util/List;
    .locals 10
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "startTime"    # J
    .param p4, "stopTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SleepInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 147
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v8, "sleepList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "start_time"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "stop_time"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "timestamp"

    aput-object v3, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop_time>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ">"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "stop_time"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " OR "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ">"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "start_time"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "<"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "start_time"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 157
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    new-instance v7, Lcom/vectorwatch/android/models/SleepInfo;

    const-string v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v1, "stop_time"

    .line 159
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v2, "timestamp"

    .line 161
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 160
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-direct {v7, v0, v1, v2}, Lcom/vectorwatch/android/models/SleepInfo;-><init>(III)V

    .line 162
    .local v7, "sleep":Lcom/vectorwatch/android/models/SleepInfo;
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 165
    .end local v7    # "sleep":Lcom/vectorwatch/android/models/SleepInfo;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 167
    return-object v8
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 20
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 39
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 40
    .local v9, "cal":Ljava/util/Calendar;
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    div-long v6, v16, v18

    .line 41
    .local v6, "endDate":J
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v15

    .line 42
    .local v15, "tz":Ljava/util/TimeZone;
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11}, Ljava/util/Date;-><init>()V

    .line 43
    .local v11, "now":Ljava/util/Date;
    invoke-virtual {v11}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    div-int/lit16 v12, v2, 0x3e8

    .line 44
    .local v12, "offsetFromUtc":I
    int-to-long v0, v12

    move-wide/from16 v16, v0

    sub-long v6, v6, v16

    .line 46
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 47
    .local v10, "calSleep":Ljava/util/Calendar;
    const/4 v2, 0x6

    const/16 v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v10, v2, v0}, Ljava/util/Calendar;->add(II)V

    .line 48
    const/16 v2, 0xb

    const/16 v16, 0x11

    move/from16 v0, v16

    invoke-virtual {v10, v2, v0}, Ljava/util/Calendar;->set(II)V

    .line 49
    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    div-long v4, v16, v18

    .line 51
    .local v4, "startDate":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;->getSleepForInterval(Landroid/content/ContentResolver;JJ)Ljava/util/List;

    move-result-object v14

    .line 52
    .local v14, "sleepInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 53
    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 68
    :goto_0
    return-object v2

    .line 55
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStart()I

    move-result v2

    add-int/lit16 v2, v2, -0xe10

    add-int/lit16 v2, v2, -0x384

    int-to-long v4, v2

    .line 56
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v14, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v2

    add-int/lit16 v2, v2, 0x1c20

    int-to-long v6, v2

    .line 58
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    .line 59
    .local v3, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v2

    invoke-virtual/range {v2 .. v7}, Lcom/vectorwatch/android/database/DatabaseManager;->getActivity(Lio/realm/Realm;JJ)Ljava/util/List;

    move-result-object v8

    .line 60
    .local v8, "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 61
    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 62
    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    .line 65
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;->getScore(Ljava/util/List;)I

    move-result v13

    .line 67
    .local v13, "score":I
    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 68
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "sleepQuality"    # Ljava/lang/Integer;

    .prologue
    .line 132
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 134
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/SleepQualityEvent;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/SleepQualityEvent;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 135
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/SleepScoreAsyncTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
