.class public Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;
.super Landroid/os/AsyncTask;
.source "GetAppAdditionalDataAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/vectorwatch/android/models/AppCallbackProxyResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

.field private callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;

.field cloudElementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

.field private mAlertMessage:Lcom/vectorwatch/android/models/AlertMessage;

.field private mProgressBar:Landroid/view/View;

.field model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

.field pContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private receivedAuthError:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/DownloadedAppDetails;Lcom/vectorwatch/android/models/CloudElementSummary;IIILcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "app"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .param p2, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p3, "watchface_id"    # I
    .param p4, "element_id"    # I
    .param p5, "param"    # I
    .param p6, "callback"    # Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;
    .param p7, "context"    # Landroid/content/Context;
    .param p8, "progressBar"    # Landroid/view/View;
    .param p9, "buttonType"    # Ljava/lang/String;
    .param p10, "buttonEvent"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 47
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->receivedAuthError:Ljava/lang/Boolean;

    .line 61
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p7}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    .line 62
    iput-object p8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mProgressBar:Landroid/view/View;

    .line 63
    iput-object p6, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;

    .line 65
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 66
    iput-object p2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->cloudElementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    if-nez v0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, p3, :cond_0

    .line 71
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, p4, :cond_0

    .line 74
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getDatasource()Lcom/vectorwatch/android/models/Element$DataSource;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    new-instance v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    .line 78
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->setSingleValueInArgs(Ljava/lang/Integer;)V

    .line 79
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    new-instance v1, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;-><init>()V

    iput-object v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    .line 80
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iget-object v1, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->appId:I

    .line 81
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iput p4, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->elementId:I

    .line 82
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iput p3, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->watchfaceId:I

    .line 83
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->elementType:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getDatasource()Lcom/vectorwatch/android/models/Element$DataSource;

    move-result-object v0

    iget-object v0, v0, Lcom/vectorwatch/android/models/Element$DataSource;->remoteMethod:Lcom/vectorwatch/android/models/Element$RemoteMethod;

    iput-object v0, v1, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->remoteMethod:Lcom/vectorwatch/android/models/Element$RemoteMethod;

    .line 85
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getUserSettings()Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->userSettings:Ljava/util/Map;

    .line 86
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iput-object p9, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->button:Ljava/lang/String;

    .line 87
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iput-object p10, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->buttonAction:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/DownloadedAppDetails;Lcom/vectorwatch/android/models/CloudElementSummary;ILcom/vectorwatch/android/models/EventDestination$ButtonWatch;IILcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "app"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .param p2, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p3, "watchface_id"    # I
    .param p4, "button"    # Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;
    .param p5, "identifier"    # I
    .param p6, "param"    # I
    .param p7, "callback"    # Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;
    .param p8, "context"    # Landroid/content/Context;
    .param p9, "progressBar"    # Landroid/view/View;
    .param p10, "buttonType"    # Ljava/lang/String;
    .param p11, "buttonEvent"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 47
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->receivedAuthError:Ljava/lang/Boolean;

    .line 92
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p8}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    .line 93
    iput-object p9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mProgressBar:Landroid/view/View;

    .line 94
    iput-object p7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;

    .line 96
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 97
    iput-object p2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->cloudElementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 98
    new-instance v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    .line 99
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->setValuePairsInArgs(Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 100
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    new-instance v1, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;-><init>()V

    iput-object v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iget-object v1, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->appId:I

    .line 102
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iget-object v1, p4, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->elementId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->elementId:I

    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iput p3, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->watchfaceId:I

    .line 104
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iget-object v1, p4, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->elementType:Ljava/lang/String;

    iput-object v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->elementType:Ljava/lang/String;

    .line 105
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iput-object p10, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->button:Ljava/lang/String;

    .line 106
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

    iput-object p11, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;->buttonAction:Ljava/lang/String;

    .line 108
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v1, p4, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->remoteMethod:Lcom/vectorwatch/android/models/Element$RemoteMethod;

    iput-object v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->remoteMethod:Lcom/vectorwatch/android/models/Element$RemoteMethod;

    .line 109
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getUserSettings()Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->userSettings:Ljava/util/Map;

    .line 110
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    .locals 11
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 121
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    const-string v9, "AT_GetAppAdditionalDataAsyncTask"

    invoke-virtual {v7, v9}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 123
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_0

    move-object v7, v8

    .line 165
    :goto_0
    return-object v7

    .line 129
    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v7}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 130
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v7, "com.vectorwatch.android"

    invoke-virtual {v1, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 132
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v7, v2

    if-ge v7, v10, :cond_1

    .line 133
    sget-object v7, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v9, "There should be at least one account in Accounts & sync"

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    move-object v7, v8

    .line 135
    goto :goto_0

    .line 138
    :cond_1
    const/4 v7, 0x0

    aget-object v0, v2, v7

    .line 140
    .local v0, "account":Landroid/accounts/Account;
    const-string v7, "Full access"

    const/4 v9, 0x1

    invoke-virtual {v1, v0, v7, v9}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 142
    .local v3, "authToken":Ljava/lang/String;
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    .line 143
    .local v4, "context":Landroid/content/Context;
    if-eqz v4, :cond_2

    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_4

    if-nez v7, :cond_3

    :cond_2
    move-object v7, v8

    .line 144
    goto :goto_0

    .line 147
    :cond_3
    :try_start_1
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v7, v7, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v9, v9, Lcom/vectorwatch/android/models/DownloadedAppDetails;->contentPVersion:Ljava/lang/String;

    iget-object v10, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    invoke-static {v7, v9, v10, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->getAppAdditionalData(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;Ljava/lang/String;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    :try_end_1
    .catch Lretrofit/RetrofitError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v7

    goto :goto_0

    .line 148
    :catch_0
    move-exception v6

    .line 149
    .local v6, "retrofitError":Lretrofit/RetrofitError;
    :try_start_2
    invoke-virtual {v6}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v6}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v7

    invoke-virtual {v7}, Lretrofit/client/Response;->getStatus()I

    move-result v7

    const/16 v9, 0x385

    if-ne v7, v9, :cond_4

    .line 150
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->receivedAuthError:Ljava/lang/Boolean;

    .line 153
    :cond_4
    invoke-static {v6}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->getAlertFromRetrofitError(Lretrofit/RetrofitError;)Lcom/vectorwatch/android/models/AlertMessage;

    move-result-object v7

    iput-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mAlertMessage:Lcom/vectorwatch/android/models/AlertMessage;

    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v6    # "retrofitError":Lretrofit/RetrofitError;
    :goto_1
    move-object v7, v8

    .line 165
    goto :goto_0

    .line 154
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    .restart local v3    # "authToken":Ljava/lang/String;
    .restart local v4    # "context":Landroid/content/Context;
    :catch_1
    move-exception v5

    .line 155
    .local v5, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error in getting additional data for app "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 157
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v5

    .line 158
    .local v5, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v5}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_1

    .line 159
    .end local v5    # "e":Landroid/accounts/OperationCanceledException;
    :catch_3
    move-exception v5

    .line 160
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 161
    .end local v5    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 162
    .local v5, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v5}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->doInBackground([Ljava/lang/String;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/vectorwatch/android/models/AppCallbackProxyResponse;)V
    .locals 3
    .param p1, "data"    # Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

    .prologue
    const/4 v2, 0x0

    .line 177
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 181
    if-eqz p1, :cond_2

    .line 182
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->args:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    .line 183
    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->args:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, p1, v0}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;->onSuccess(Lcom/vectorwatch/android/models/AppCallbackProxyResponse;I)V

    .line 205
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;

    invoke-interface {v0, p1, v2}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;->onSuccess(Lcom/vectorwatch/android/models/AppCallbackProxyResponse;I)V

    goto :goto_0

    .line 189
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->receivedAuthError:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mAlertMessage:Lcom/vectorwatch/android/models/AlertMessage;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;->onAuthError(Lcom/vectorwatch/android/models/AlertMessage;)V

    .line 191
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->cloudElementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 193
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mAlertMessage:Lcom/vectorwatch/android/models/AlertMessage;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;->onError(Lcom/vectorwatch/android/models/AlertMessage;)V

    goto :goto_0

    .line 198
    :cond_4
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->receivedAuthError:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 199
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mAlertMessage:Lcom/vectorwatch/android/models/AlertMessage;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;->onAuthError(Lcom/vectorwatch/android/models/AlertMessage;)V

    .line 200
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->cloudElementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 202
    :cond_5
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mAlertMessage:Lcom/vectorwatch/android/models/AlertMessage;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;->onError(Lcom/vectorwatch/android/models/AlertMessage;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->onPostExecute(Lcom/vectorwatch/android/models/AppCallbackProxyResponse;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 173
    :cond_0
    return-void
.end method

.method public setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;
    .locals 1
    .param p1, "location"    # Lcom/vectorwatch/android/models/VectorLocation;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->model:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;

    iput-object p1, v0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 114
    return-object p0
.end method
