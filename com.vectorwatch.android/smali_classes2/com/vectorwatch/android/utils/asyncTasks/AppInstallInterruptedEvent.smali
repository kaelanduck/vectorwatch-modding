.class public Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;
.super Ljava/lang/Object;
.source "AppInstallInterruptedEvent.java"


# instance fields
.field private crtInstalling:Ljava/lang/String;

.field private type:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V
    .locals 0
    .param p1, "crtInstallingElementUuid"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;->crtInstalling:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;->type:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    .line 16
    return-void
.end method


# virtual methods
.method public getCrtInstalling()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;->crtInstalling:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;->type:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    return-object v0
.end method
