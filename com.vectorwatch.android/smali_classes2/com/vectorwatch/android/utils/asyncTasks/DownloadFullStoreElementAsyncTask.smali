.class public Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;
.super Landroid/os/AsyncTask;
.source "DownloadFullStoreElementAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# static fields
.field private static final RUNNING_APPS_LIMIT:I = 0xc

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private COUNT_APPS:I

.field private mApp:Lcom/vectorwatch/android/models/DownloadedAppContainer;

.field private mRunningApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation
.end field

.field private mStoreElementsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation
.end field

.field private mUuid:Ljava/lang/String;

.field pActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/app/Activity;)V
    .locals 1
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 41
    const/16 v0, 0x14

    iput v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->COUNT_APPS:I

    .line 49
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    .line 50
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mUuid:Ljava/lang/String;

    .line 51
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/content/Intent;
    .locals 10
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 57
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    const-string v8, "AT_DownloadAppThread"

    invoke-virtual {v7, v8}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 58
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    .line 59
    .local v3, "activity":Landroid/app/Activity;
    if-nez v3, :cond_0

    .line 60
    const/4 v6, 0x0

    .line 96
    :goto_0
    return-object v6

    .line 62
    :cond_0
    sget-object v7, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v8, "Started download of full element from store"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 67
    :try_start_0
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v7}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 68
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v7, "com.vectorwatch.android"

    invoke-virtual {v1, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 70
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v7, v2

    if-ge v7, v9, :cond_1

    .line 71
    const-string v7, "Store-Apps:"

    const-string v8, "There should be at least one account in Accounts & sync"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 74
    .local v6, "res":Landroid/content/Intent;
    const-string v7, "ERR_MSG"

    const v8, 0x7f09010d

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 85
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v6    # "res":Landroid/content/Intent;
    :catch_0
    move-exception v5

    .line 86
    .local v5, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v5}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 95
    .end local v5    # "e":Landroid/accounts/OperationCanceledException;
    :goto_1
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 96
    .restart local v6    # "res":Landroid/content/Intent;
    goto :goto_0

    .line 79
    .end local v6    # "res":Landroid/content/Intent;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const/4 v7, 0x0

    :try_start_1
    aget-object v0, v2, v7

    .line 81
    .local v0, "account":Landroid/accounts/Account;
    const-string v7, "Full access"

    const/4 v8, 0x1

    invoke-virtual {v1, v0, v7, v8}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 83
    .local v4, "authToken":Ljava/lang/String;
    sget-object v7, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v8, "STORE: Download full tore element async task"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 84
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mUuid:Ljava/lang/String;

    invoke-static {v7, v4}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->downloadAppFromLoadedAppList(Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/DownloadedAppContainer;

    move-result-object v7

    iput-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mApp:Lcom/vectorwatch/android/models/DownloadedAppContainer;
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lretrofit/RetrofitError; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    .line 87
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v4    # "authToken":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 88
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 89
    .end local v5    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v5

    .line 90
    .local v5, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v5}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1

    .line 91
    .end local v5    # "e":Landroid/accounts/AuthenticatorException;
    :catch_3
    move-exception v5

    .line 92
    .local v5, "e":Lretrofit/RetrofitError;
    invoke-virtual {v5}, Lretrofit/RetrofitError;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->doInBackground([Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 106
    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 107
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 108
    const-string v2, "ERR_MSG"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 109
    sget-object v2, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error at downloading system apps: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ERR_MSG"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mApp:Lcom/vectorwatch/android/models/DownloadedAppContainer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mApp:Lcom/vectorwatch/android/models/DownloadedAppContainer;

    iget-object v2, v2, Lcom/vectorwatch/android/models/DownloadedAppContainer;->data:Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mApp:Lcom/vectorwatch/android/models/DownloadedAppContainer;

    iget-object v2, v2, Lcom/vectorwatch/android/models/DownloadedAppContainer;->data:Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;

    iget-object v2, v2, Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mApp:Lcom/vectorwatch/android/models/DownloadedAppContainer;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->checkAppStreams(Lcom/vectorwatch/android/models/DownloadedAppContainer;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 113
    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mApp:Lcom/vectorwatch/android/models/DownloadedAppContainer;

    iget-object v2, v2, Lcom/vectorwatch/android/models/DownloadedAppContainer;->data:Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;

    iget-object v1, v2, Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 115
    .local v1, "app":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mApp:Lcom/vectorwatch/android/models/DownloadedAppContainer;

    iget-object v2, v2, Lcom/vectorwatch/android/models/DownloadedAppContainer;->data:Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;

    iget-object v2, v2, Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v2, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 116
    sget-object v2, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DOWNLOAD: Complete download for app = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mApp:Lcom/vectorwatch/android/models/DownloadedAppContainer;

    iget-object v4, v4, Lcom/vectorwatch/android/models/DownloadedAppContainer;->data:Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;

    iget-object v4, v4, Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v4, v4, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 119
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/managers/CloudAppsManager;->saveAppToDatabase(Lcom/vectorwatch/android/models/DownloadedAppDetails;ILandroid/content/Context;)V

    goto :goto_0

    .line 122
    .end local v1    # "app":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    :cond_3
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;

    iget-object v4, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->mUuid:Ljava/lang/String;

    sget-object v5, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-direct {v3, v4, v5}, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;-><init>(Ljava/lang/String;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method
