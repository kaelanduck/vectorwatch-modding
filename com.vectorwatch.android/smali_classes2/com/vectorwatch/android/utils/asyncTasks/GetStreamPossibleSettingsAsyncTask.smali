.class public Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;
.super Landroid/os/AsyncTask;
.source "GetStreamPossibleSettingsAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private authExpired:Z

.field callIsOk:Z

.field elemId:I

.field private mProgressBar:Landroid/view/View;

.field pContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field stream:Lcom/vectorwatch/android/models/Stream;

.field streamPosInList:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;IILandroid/view/View;)V
    .locals 1
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "streamPosInList"    # I
    .param p4, "elemId"    # I
    .param p5, "progressBar"    # Landroid/view/View;

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->authExpired:Z

    .line 43
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    .line 44
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 45
    iput p3, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->streamPosInList:I

    .line 46
    iput p4, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->elemId:I

    .line 47
    iput-object p5, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    .line 48
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/content/Intent;
    .locals 12
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x1

    .line 54
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    const-string v10, "AT_GetStreamPossibleSettingsAsyncTask"

    invoke-virtual {v9, v10}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 56
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_1

    .line 107
    :cond_0
    :goto_0
    return-object v7

    .line 62
    :cond_1
    :try_start_0
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static {v9}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 63
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v9, "com.vectorwatch.android"

    invoke-virtual {v1, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 65
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v9, v2

    if-ge v9, v11, :cond_3

    .line 66
    sget-object v9, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v10, "There should be at least one account in Accounts & sync"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 68
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 69
    .local v7, "res":Landroid/content/Intent;
    const-string v9, "ERR_MSG"

    const v10, 0x7f09010d

    invoke-virtual {v7, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 98
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v7    # "res":Landroid/content/Intent;
    :catch_0
    move-exception v5

    .line 99
    .local v5, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v5}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 106
    .end local v5    # "e":Landroid/accounts/OperationCanceledException;
    :cond_2
    :goto_1
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 107
    .restart local v7    # "res":Landroid/content/Intent;
    goto :goto_0

    .line 74
    .end local v7    # "res":Landroid/content/Intent;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_3
    const/4 v9, 0x0

    :try_start_1
    aget-object v0, v2, v9

    .line 76
    .local v0, "account":Landroid/accounts/Account;
    const-string v9, "Full access"

    const/4 v10, 0x1

    invoke-virtual {v1, v0, v9, v10}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 78
    .local v3, "authToken":Ljava/lang/String;
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    .line 79
    .local v4, "context":Landroid/content/Context;
    if-eqz v4, :cond_0

    .line 82
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->callIsOk:Z
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_4

    .line 84
    :try_start_2
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-static {v9, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->getStreamSettings(Lcom/vectorwatch/android/models/Stream;Ljava/lang/String;)Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v8

    .line 86
    .local v8, "streamPossibleSettings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v9, v8}, Lcom/vectorwatch/android/models/Stream;->setStreamSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V

    .line 88
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->callIsOk:Z
    :try_end_2
    .catch Lretrofit/RetrofitError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 89
    .end local v8    # "streamPossibleSettings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    :catch_1
    move-exception v6

    .line 90
    .local v6, "error":Lretrofit/RetrofitError;
    :try_start_3
    invoke-virtual {v6}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v6}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v9

    invoke-virtual {v9}, Lretrofit/client/Response;->getStatus()I

    move-result v9

    sget v10, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->STREAM_APP_AUTH_ERROR_STATUS_CODE:I

    if-ne v9, v10, :cond_2

    .line 91
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->authExpired:Z
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_1

    .line 100
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v6    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v5

    .line 101
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 93
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    .restart local v3    # "authToken":Ljava/lang/String;
    .restart local v4    # "context":Landroid/content/Context;
    :catch_3
    move-exception v5

    .line 94
    .local v5, "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v9, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v10, "Error in getting stream auth method "

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 95
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->callIsOk:Z
    :try_end_4
    .catch Landroid/accounts/OperationCanceledException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_1

    .line 102
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    .line 103
    .local v5, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v5}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->doInBackground([Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    :cond_0
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->authExpired:Z

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    if-eqz v0, :cond_1

    .line 125
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 142
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->callIsOk:Z

    if-eqz v0, :cond_3

    .line 129
    const-string v0, "ERR_MSG"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130
    sget-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error at downloading system apps: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ERR_MSG"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 137
    :cond_3
    :goto_1
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->callIsOk:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 138
    sget-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v1, "Get Stream Auth Method call did not end as expected. Exception has been caught."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_4
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    iget v3, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->streamPosInList:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->elemId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;-><init>(Lcom/vectorwatch/android/models/Stream;Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    :cond_0
    return-void
.end method
