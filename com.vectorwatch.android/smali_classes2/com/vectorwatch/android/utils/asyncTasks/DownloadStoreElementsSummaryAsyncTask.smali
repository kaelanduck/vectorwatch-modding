.class public Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;
.super Landroid/os/AsyncTask;
.source "DownloadStoreElementsSummaryAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# static fields
.field private static final COUNT_ELEMENTS:I = 0x5

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

.field private mQuery:Ljava/lang/String;

.field private option:Lcom/vectorwatch/android/models/cloud/AppsOption;

.field pActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private storeElements:Lcom/vectorwatch/android/models/StoreQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Landroid/app/Activity;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;Ljava/lang/String;)V
    .locals 1
    .param p1, "option"    # Lcom/vectorwatch/android/models/cloud/AppsOption;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "progressBar"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .param p4, "query"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 61
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    .line 62
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->option:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 63
    iput-object p3, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 64
    iput-object p4, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->mQuery:Ljava/lang/String;

    .line 65
    return-void
.end method

.method private static getJSONFromSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Ljava/lang/String;
    .locals 5
    .param p0, "model"    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    .prologue
    .line 183
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 185
    .local v1, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "deviceType"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 186
    const-string v2, "appVersion"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getAppVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 187
    const-string v2, "kernelVersion"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getKernelVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 188
    const-string v2, "endpointArn"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getEndpointArn()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 189
    const-string v2, "deviceCompatibility"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getDeviceCompatibility()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 190
    const-string v2, "cpuId"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getCpuId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 192
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 195
    :goto_0
    return-object v2

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "exception":Lorg/json/JSONException;
    sget-object v2, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CloudManagerHandler: Exception while formatting the JSON "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 195
    const-string v2, ""

    goto :goto_0
.end method

.method private static populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V
    .locals 6
    .param p0, "model"    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    .prologue
    .line 205
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 208
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "last_known_system_info"

    const-class v5, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 209
    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 211
    .local v1, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-eqz v1, :cond_0

    .line 212
    const-string v3, "kernel"

    invoke-static {v1, v3}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setKernelVersion(Ljava/lang/String;)V

    .line 215
    :cond_0
    const-string v3, "android"

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setDeviceType(Ljava/lang/String;)V

    .line 216
    const-string v3, "2.0.2"

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setAppVersion(Ljava/lang/String;)V

    .line 217
    invoke-static {v0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->getEndpointArn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setEndpointArn(Ljava/lang/String;)V

    .line 219
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 221
    .local v2, "shape":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 222
    sget-object v3, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v4, "populateSystemData: Shape is null!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 229
    :goto_0
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 230
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setCpuId(Ljava/lang/String;)V

    .line 232
    :cond_1
    return-void

    .line 223
    :cond_2
    const-string v3, "round"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 224
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setDeviceCompatibility(Ljava/lang/String;)V

    goto :goto_0

    .line 226
    :cond_3
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setDeviceCompatibility(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Integer;)Landroid/content/Intent;
    .locals 17
    .param p1, "params"    # [Ljava/lang/Integer;

    .prologue
    .line 71
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v14

    const-string v15, "AT_DownloadAppThread"

    invoke-virtual {v14, v15}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 72
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v14}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    .line 73
    .local v4, "activity":Landroid/app/Activity;
    if-nez v4, :cond_0

    .line 74
    const/4 v12, 0x0

    .line 148
    :goto_0
    return-object v12

    .line 76
    :cond_0
    sget-object v14, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Started download of elements summary from store "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->option:Lcom/vectorwatch/android/models/cloud/AppsOption;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 81
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v14}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/Context;

    invoke-static {v14}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 82
    .local v2, "accountManager":Landroid/accounts/AccountManager;
    const-string v14, "com.vectorwatch.android"

    invoke-virtual {v2, v14}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 84
    .local v3, "accounts":[Landroid/accounts/Account;
    array-length v14, v3

    const/4 v15, 0x1

    if-ge v14, v15, :cond_1

    .line 85
    const-string v14, "Store-Apps:"

    const-string v15, "There should be at least one account in Accounts & sync"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12}, Landroid/content/Intent;-><init>()V

    .line 88
    .local v12, "res":Landroid/content/Intent;
    const-string v14, "ERR_MSG"

    const v15, 0x7f09010d

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 135
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v12    # "res":Landroid/content/Intent;
    :catch_0
    move-exception v7

    .line 136
    .local v7, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v7}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 147
    .end local v7    # "e":Landroid/accounts/OperationCanceledException;
    :goto_1
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12}, Landroid/content/Intent;-><init>()V

    .line 148
    .restart local v12    # "res":Landroid/content/Intent;
    goto :goto_0

    .line 93
    .end local v12    # "res":Landroid/content/Intent;
    .restart local v2    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v3    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const/4 v14, 0x0

    :try_start_1
    aget-object v1, v3, v14

    .line 95
    .local v1, "account":Landroid/accounts/Account;
    const-string v14, "Full access"

    const/4 v15, 0x1

    invoke-virtual {v2, v1, v14, v15}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 97
    .local v5, "authToken":Ljava/lang/String;
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 98
    .local v9, "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v14, "from"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v16, 0x0

    aget-object v16, p1, v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v16

    mul-int/lit8 v16, v16, 0x5

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    const-string v14, "size"

    const-string v15, "5"

    invoke-virtual {v9, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->option:Lcom/vectorwatch/android/models/cloud/AppsOption;

    sget-object v15, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    if-ne v14, v15, :cond_4

    .line 101
    const-string v14, "type"

    const-string v15, "watchface"

    invoke-virtual {v9, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    :cond_2
    :goto_2
    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "round"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 106
    const-string v14, "compatibility"

    sget-object v15, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v15}, Lcom/vectorwatch/android/models/WatchType;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->mQuery:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 111
    const-string v14, "q"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->mQuery:Ljava/lang/String;

    invoke-virtual {v9, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :cond_3
    const-string v14, "Options"

    invoke-virtual {v9}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const/4 v14, 0x2

    invoke-static {v5, v14}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v6

    .line 118
    .local v6, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    new-instance v13, Lcom/vectorwatch/android/models/VInfo;

    invoke-direct {v13}, Lcom/vectorwatch/android/models/VInfo;-><init>()V

    .line 119
    .local v13, "vinfo":Lcom/vectorwatch/android/models/VInfo;
    const-string v14, "android"

    invoke-virtual {v13, v14}, Lcom/vectorwatch/android/models/VInfo;->setDeviceType(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v4}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 121
    .local v10, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v4}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lretrofit/RetrofitError; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v11

    .line 124
    .local v11, "packageName":Ljava/lang/String;
    const/4 v14, 0x0

    :try_start_2
    invoke-virtual {v10, v11, v14}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v14

    iget-object v14, v14, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Lcom/vectorwatch/android/models/VInfo;->setAppVersion(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lretrofit/RetrofitError; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    .line 130
    :goto_4
    :try_start_3
    new-instance v8, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v8}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 131
    .local v8, "extraInfo":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v8}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 133
    invoke-static {v8}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->getJSONFromSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v6, v14, v9}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->queryStore(Ljava/lang/String;Ljava/util/HashMap;)Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->storeElements:Lcom/vectorwatch/android/models/StoreQuery;
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lretrofit/RetrofitError; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_1

    .line 137
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v6    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .end local v8    # "extraInfo":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    .end local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v13    # "vinfo":Lcom/vectorwatch/android/models/VInfo;
    :catch_1
    move-exception v7

    .line 138
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 102
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v1    # "account":Landroid/accounts/Account;
    .restart local v2    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v3    # "accounts":[Landroid/accounts/Account;
    .restart local v5    # "authToken":Ljava/lang/String;
    .restart local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    :try_start_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->option:Lcom/vectorwatch/android/models/cloud/AppsOption;

    sget-object v15, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    if-ne v14, v15, :cond_2

    .line 103
    const-string v14, "type"

    const-string v15, "app"

    invoke-virtual {v9, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Landroid/accounts/OperationCanceledException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lretrofit/RetrofitError; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    goto/16 :goto_2

    .line 139
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_2
    move-exception v7

    .line 140
    .local v7, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v7}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto/16 :goto_1

    .line 108
    .end local v7    # "e":Landroid/accounts/AuthenticatorException;
    .restart local v1    # "account":Landroid/accounts/Account;
    .restart local v2    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v3    # "accounts":[Landroid/accounts/Account;
    .restart local v5    # "authToken":Ljava/lang/String;
    .restart local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_5
    :try_start_5
    const-string v14, "compatibility"

    sget-object v15, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v15}, Lcom/vectorwatch/android/models/WatchType;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Landroid/accounts/OperationCanceledException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lretrofit/RetrofitError; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_3

    .line 141
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_3
    move-exception v7

    .line 142
    .local v7, "e":Lretrofit/RetrofitError;
    invoke-virtual {v7}, Lretrofit/RetrofitError;->printStackTrace()V

    goto/16 :goto_1

    .line 125
    .end local v7    # "e":Lretrofit/RetrofitError;
    .restart local v1    # "account":Landroid/accounts/Account;
    .restart local v2    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v3    # "accounts":[Landroid/accounts/Account;
    .restart local v5    # "authToken":Ljava/lang/String;
    .restart local v6    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .restart local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v10    # "packageManager":Landroid/content/pm/PackageManager;
    .restart local v11    # "packageName":Ljava/lang/String;
    .restart local v13    # "vinfo":Lcom/vectorwatch/android/models/VInfo;
    :catch_4
    move-exception v7

    .line 126
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_6
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_6
    .catch Landroid/accounts/OperationCanceledException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lretrofit/RetrofitError; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_4

    .line 143
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v6    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v13    # "vinfo":Lcom/vectorwatch/android/models/VInfo;
    :catch_5
    move-exception v7

    .line 144
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 49
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->doInBackground([Ljava/lang/Integer;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 160
    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 161
    .local v0, "activity":Landroid/app/Activity;
    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 164
    :cond_0
    if-eqz v0, :cond_1

    .line 165
    const-string v1, "ERR_MSG"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 166
    sget-object v1, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error at downloading system apps: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ERR_MSG"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 174
    :cond_1
    :goto_0
    return-void

    .line 168
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->storeElements:Lcom/vectorwatch/android/models/StoreQuery;

    if-nez v1, :cond_3

    .line 169
    sget-object v1, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v2, "Problem at loading store apps."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :cond_3
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;

    iget-object v3, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->option:Lcom/vectorwatch/android/models/cloud/AppsOption;

    iget-object v4, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->storeElements:Lcom/vectorwatch/android/models/StoreQuery;

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;-><init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Lcom/vectorwatch/android/models/StoreQuery;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 49
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 156
    :cond_0
    return-void
.end method
