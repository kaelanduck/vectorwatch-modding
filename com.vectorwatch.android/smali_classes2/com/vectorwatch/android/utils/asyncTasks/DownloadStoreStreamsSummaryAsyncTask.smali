.class public Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;
.super Landroid/os/AsyncTask;
.source "DownloadStoreStreamsSummaryAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private COUNT_STREAMS:I

.field private mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

.field pActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private storeElements:Lcom/vectorwatch/android/models/StoreQuery;

.field private streamType:Lcom/vectorwatch/android/models/cloud/AppsOption;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Landroid/app/Activity;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;)V
    .locals 1
    .param p1, "streamType"    # Lcom/vectorwatch/android/models/cloud/AppsOption;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "progressBar"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 48
    const/4 v0, 0x5

    iput v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->COUNT_STREAMS:I

    .line 54
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    .line 55
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->streamType:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 56
    iput-object p3, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 57
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Integer;)Landroid/content/Intent;
    .locals 19
    .param p1, "params"    # [Ljava/lang/Integer;

    .prologue
    .line 63
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v15

    const-string v16, "AT_DownloadAppThread"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 64
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v15}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    .line 65
    .local v4, "activity":Landroid/app/Activity;
    if-nez v4, :cond_0

    .line 66
    const/4 v12, 0x0

    .line 138
    :goto_0
    return-object v12

    .line 68
    :cond_0
    sget-object v15, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Started download of elements summary from store "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->streamType:Lcom/vectorwatch/android/models/cloud/AppsOption;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 69
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12}, Landroid/content/Intent;-><init>()V

    .line 73
    .local v12, "res":Landroid/content/Intent;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v15}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/Context;

    invoke-static {v15}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 74
    .local v2, "accountManager":Landroid/accounts/AccountManager;
    const-string v15, "com.vectorwatch.android"

    invoke-virtual {v2, v15}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 76
    .local v3, "accounts":[Landroid/accounts/Account;
    array-length v15, v3

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ge v15, v0, :cond_1

    .line 77
    const-string v15, "Store-Streams:"

    const-string v16, "There should be at least one account in Accounts & sync"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const-string v15, "ERR_MSG"

    const v16, 0x7f09010d

    move/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 121
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    :catch_0
    move-exception v7

    .line 122
    .local v7, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v7}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 123
    const-string v15, "ERR_MSG"

    const v16, 0x7f090114

    move/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 83
    .end local v7    # "e":Landroid/accounts/OperationCanceledException;
    .restart local v2    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v3    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const/4 v15, 0x0

    :try_start_1
    aget-object v1, v3, v15

    .line 85
    .local v1, "account":Landroid/accounts/Account;
    const-string v15, "Full access"

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v2, v1, v15, v0}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 87
    .local v5, "authToken":Ljava/lang/String;
    const-string v15, "Full access"

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v2, v1, v15, v0}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    .line 89
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 90
    .local v9, "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v15, "from"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->COUNT_STREAMS:I

    move/from16 v18, v0

    mul-int v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    const-string v15, "size"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->COUNT_STREAMS:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    const-string v15, "type"

    const-string v16, "stream"

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "round"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 94
    const-string v15, "compatibility"

    sget-object v16, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/WatchType;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    :goto_1
    const/4 v15, 0x2

    invoke-static {v5, v15}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v6

    .line 101
    .local v6, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    new-instance v13, Lcom/vectorwatch/android/models/VInfo;

    invoke-direct {v13}, Lcom/vectorwatch/android/models/VInfo;-><init>()V

    .line 102
    .local v13, "vinfo":Lcom/vectorwatch/android/models/VInfo;
    const-string v15, "android"

    invoke-virtual {v13, v15}, Lcom/vectorwatch/android/models/VInfo;->setDeviceType(Ljava/lang/String;)V

    .line 103
    invoke-virtual {v4}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 104
    .local v10, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v4}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lretrofit/RetrofitError; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v11

    .line 107
    .local v11, "packageName":Ljava/lang/String;
    const/4 v15, 0x0

    :try_start_2
    invoke-virtual {v10, v11, v15}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    iget-object v15, v15, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v13, v15}, Lcom/vectorwatch/android/models/VInfo;->setAppVersion(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lretrofit/RetrofitError; {:try_start_2 .. :try_end_2} :catch_5

    .line 112
    :goto_2
    :try_start_3
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v15

    const-string v16, "last_known_system_info"

    const-class v17, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 113
    invoke-virtual/range {v15 .. v17}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 114
    .local v8, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    const-string v15, "kernel"

    invoke-static {v8, v15}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lcom/vectorwatch/android/models/VInfo;->setKernelVersion(Ljava/lang/String;)V

    .line 116
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 117
    .local v14, "vinfoJson":Lorg/json/JSONObject;
    const-string v15, "deviceType"

    invoke-virtual {v13}, Lcom/vectorwatch/android/models/VInfo;->getDeviceType()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 118
    const-string v15, "appVersion"

    invoke-virtual {v13}, Lcom/vectorwatch/android/models/VInfo;->getAppVersion()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 119
    const-string v15, "kernelVersion"

    invoke-virtual {v13}, Lcom/vectorwatch/android/models/VInfo;->getKernelVersion()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 120
    invoke-virtual {v14}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v6, v15, v9}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->queryStore(Ljava/lang/String;Ljava/util/HashMap;)Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->storeElements:Lcom/vectorwatch/android/models/StoreQuery;
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lretrofit/RetrofitError; {:try_start_3 .. :try_end_3} :catch_5

    goto/16 :goto_0

    .line 124
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v6    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .end local v8    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    .end local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v13    # "vinfo":Lcom/vectorwatch/android/models/VInfo;
    .end local v14    # "vinfoJson":Lorg/json/JSONObject;
    :catch_1
    move-exception v7

    .line 125
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    .line 126
    const-string v15, "ERR_MSG"

    const v16, 0x7f090114

    move/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    .line 96
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v1    # "account":Landroid/accounts/Account;
    .restart local v2    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v3    # "accounts":[Landroid/accounts/Account;
    .restart local v5    # "authToken":Ljava/lang/String;
    .restart local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    :try_start_4
    const-string v15, "compatibility"

    sget-object v16, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/WatchType;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Landroid/accounts/OperationCanceledException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lretrofit/RetrofitError; {:try_start_4 .. :try_end_4} :catch_5

    goto/16 :goto_1

    .line 127
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_2
    move-exception v7

    .line 128
    .local v7, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v7}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    .line 129
    const-string v15, "ERR_MSG"

    const v16, 0x7f090114

    move/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    .line 108
    .end local v7    # "e":Landroid/accounts/AuthenticatorException;
    .restart local v1    # "account":Landroid/accounts/Account;
    .restart local v2    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v3    # "accounts":[Landroid/accounts/Account;
    .restart local v5    # "authToken":Ljava/lang/String;
    .restart local v6    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .restart local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v10    # "packageManager":Landroid/content/pm/PackageManager;
    .restart local v11    # "packageName":Ljava/lang/String;
    .restart local v13    # "vinfo":Lcom/vectorwatch/android/models/VInfo;
    :catch_3
    move-exception v7

    .line 109
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_5
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_5
    .catch Landroid/accounts/OperationCanceledException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Lretrofit/RetrofitError; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_2

    .line 130
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v5    # "authToken":Ljava/lang/String;
    .end local v6    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v9    # "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v13    # "vinfo":Lcom/vectorwatch/android/models/VInfo;
    :catch_4
    move-exception v7

    .line 131
    .local v7, "e":Lorg/json/JSONException;
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    .line 132
    const-string v15, "ERR_MSG"

    const v16, 0x7f090114

    move/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    .line 133
    .end local v7    # "e":Lorg/json/JSONException;
    :catch_5
    move-exception v7

    .line 134
    .local v7, "e":Lretrofit/RetrofitError;
    invoke-virtual {v7}, Lretrofit/RetrofitError;->printStackTrace()V

    .line 135
    const-string v15, "ERR_MSG"

    const v16, 0x7f090114

    move/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->doInBackground([Ljava/lang/Integer;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 148
    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->pActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 149
    .local v0, "activity":Landroid/app/Activity;
    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 150
    if-eqz v0, :cond_0

    .line 151
    const-string v1, "ERR_MSG"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 152
    sget-object v1, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error at downloading system apps: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ERR_MSG"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->storeElements:Lcom/vectorwatch/android/models/StoreQuery;

    if-nez v1, :cond_2

    .line 155
    sget-object v1, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v2, "Problem at loading store apps."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/LoadedStoreStreamsEvent;

    iget-object v3, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->streamType:Lcom/vectorwatch/android/models/cloud/AppsOption;

    iget-object v4, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->storeElements:Lcom/vectorwatch/android/models/StoreQuery;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v4

    .line 158
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/events/LoadedStoreStreamsEvent;-><init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Ljava/util/List;)V

    .line 157
    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreStreamsSummaryAsyncTask;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 144
    return-void
.end method
