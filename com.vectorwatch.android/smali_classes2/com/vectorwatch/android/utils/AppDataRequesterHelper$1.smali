.class Lcom/vectorwatch/android/utils/AppDataRequesterHelper$1;
.super Ljava/lang/Object;
.source "AppDataRequesterHelper.java"

# interfaces
.implements Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->_doPerformRequestWithLocation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/utils/AppDataRequesterHelper;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$1;->this$0:Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccurateLocation(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$1;->this$0:Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-static {p1}, Lcom/vectorwatch/android/models/VectorLocation;->fromLocation(Landroid/location/Location;)Lcom/vectorwatch/android/models/VectorLocation;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->access$002(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/models/VectorLocation;

    .line 254
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$1;->this$0:Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->getLocationHelper()Lcom/vectorwatch/android/utils/LocationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 255
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$1;->this$0:Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->access$100(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)V

    .line 256
    return-void
.end method

.method public onLocation(Landroid/location/Location;)V
    .locals 0
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 261
    return-void
.end method

.method public onTimeoutError()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$1;->this$0:Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->getLocationHelper()Lcom/vectorwatch/android/utils/LocationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 266
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$1;->this$0:Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->onGpsTimeoutError()V

    .line 267
    return-void
.end method
