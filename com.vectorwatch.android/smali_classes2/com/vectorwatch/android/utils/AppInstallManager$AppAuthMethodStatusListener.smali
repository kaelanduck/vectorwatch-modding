.class public Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;
.super Ljava/lang/Object;
.source "AppInstallManager.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/AppInstallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppAuthMethodStatusListener"
.end annotation


# instance fields
.field private app:Lcom/vectorwatch/android/models/CloudElementSummary;

.field final synthetic this$0:Lcom/vectorwatch/android/utils/AppInstallManager;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p2, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 414
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415
    iput-object p2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 416
    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 3

    .prologue
    .line 447
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$500()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 449
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - auth method status listener - on failure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$600(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 454
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090114

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xbb8

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 455
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$702(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 456
    return-void
.end method

.method public onSuccess()V
    .locals 7

    .prologue
    .line 420
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$300(Lcom/vectorwatch/android/utils/AppInstallManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 443
    :goto_0
    return-void

    .line 423
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 424
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "INSTALL MNGR - auth method status listener - on success "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 425
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/ui/webview/OauthActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 426
    .local v0, "loginActivity":Landroid/content/Intent;
    const-string v1, "login_url"

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/AuthMethod;->getLoginUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 427
    const-string v1, "element_uuid"

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    const-string v1, "element_type"

    const-string v2, "app"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 429
    const-string v1, "title"

    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090169

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 430
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 432
    .end local v0    # "loginActivity":Landroid/content/Intent;
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v1, :cond_2

    .line 433
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$500()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 434
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "INSTALL MNGR - auth method listener - auth method == null:  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "cancel install"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 436
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$600(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 440
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090114

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 441
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$702(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)Lcom/vectorwatch/android/models/CloudElementSummary;

    goto/16 :goto_0
.end method
