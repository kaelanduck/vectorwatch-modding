.class public final enum Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CurrentAppOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

.field public static final enum ID:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

.field public static final enum POSITION:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 768
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    const-string v1, "POSITION"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;->POSITION:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    const-string v1, "ID"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;->ID:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    .line 767
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;->POSITION:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;->ID:Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 767
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 767
    const-class v0, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;
    .locals 1

    .prologue
    .line 767
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;

    return-object v0
.end method
