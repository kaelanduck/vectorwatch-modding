.class public final enum Lcom/vectorwatch/android/utils/Constants$NotificationMode;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotificationMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$NotificationMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$NotificationMode;

.field public static final enum NOTIFICATIONS_MODE_OFF:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

.field public static final enum NOTIFICATIONS_MODE_SHOW_ALERT:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

.field public static final enum NOTIFICATIONS_MODE_SHOW_CONTENTS:Lcom/vectorwatch/android/utils/Constants$NotificationMode;


# instance fields
.field private final val:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 404
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    const-string v1, "NOTIFICATIONS_MODE_OFF"

    invoke-direct {v0, v1, v2, v2}, Lcom/vectorwatch/android/utils/Constants$NotificationMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_OFF:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    .line 405
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    const-string v1, "NOTIFICATIONS_MODE_SHOW_CONTENTS"

    invoke-direct {v0, v1, v3, v3}, Lcom/vectorwatch/android/utils/Constants$NotificationMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_SHOW_CONTENTS:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    .line 406
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    const-string v1, "NOTIFICATIONS_MODE_SHOW_ALERT"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$NotificationMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_SHOW_ALERT:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    .line 403
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_OFF:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_SHOW_CONTENTS:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_SHOW_ALERT:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 410
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 411
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->val:I

    .line 412
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$NotificationMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 403
    const-class v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$NotificationMode;
    .locals 1

    .prologue
    .line 403
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$NotificationMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 415
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->val:I

    return v0
.end method
