.class public Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;
.super Ljava/lang/Object;
.source "AppInstallManager.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/AppInstallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppPossibleSettingsStatusListener"
.end annotation


# instance fields
.field private app:Lcom/vectorwatch/android/models/CloudElementSummary;

.field final synthetic this$0:Lcom/vectorwatch/android/utils/AppInstallManager;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p2, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 568
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 569
    iput-object p2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 570
    return-void
.end method


# virtual methods
.method public onFailure()V
    .locals 3

    .prologue
    .line 597
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 598
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->hasDynamicSettings()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 600
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - app possible settings on failre = cancel app install "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 601
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$600(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 602
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090115

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xbb8

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 608
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$802(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 609
    return-void

    .line 604
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - app possible settings on failure = trigger app install "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 605
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    goto :goto_0
.end method

.method public onSuccess()V
    .locals 7

    .prologue
    const/16 v6, 0xbb8

    .line 574
    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getAppPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getAppPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getRenderOptions()Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getAppPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getRenderOptions()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 575
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "INSTALL MNGR - app possible settings status list -success- not ok: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 576
    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 577
    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 578
    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$600(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 579
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090115

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v4

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 583
    :goto_0
    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$802(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 593
    :goto_1
    return-void

    .line 581
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090061

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v4

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 585
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$200()Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "INSTALL MNGR - app possible settings status list -success- ok: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 586
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 587
    .local v1, "gson":Lcom/google/gson/Gson;
    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getAppPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 588
    .local v2, "json":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$400()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 589
    .local v0, "appSettings":Landroid/content/Intent;
    const-string v3, "settings_json"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 590
    const-string v3, "app_uuid"

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 591
    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;->this$0:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;

    move-result-object v3

    const/16 v4, 0x65

    invoke-virtual {v3, v0, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1
.end method
