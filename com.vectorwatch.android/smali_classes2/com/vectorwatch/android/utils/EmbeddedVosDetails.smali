.class public final Lcom/vectorwatch/android/utils/EmbeddedVosDetails;
.super Ljava/lang/Object;
.source "EmbeddedVosDetails.java"


# static fields
.field public static final TYPE_BOOTLOADER:I = 0x1

.field public static final TYPE_KERNEL:I

.field private static bootloaderSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

.field private static id:J

.field private static kernelSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

.field private static summary:Ljava/lang/String;

.field private static type:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    const/4 v1, 0x1

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    sput-object v0, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->bootloaderSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 15
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    sput-object v0, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->kernelSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 18
    sput v3, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->type:I

    .line 19
    const-string v0, "This is a new update"

    sput-object v0, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->summary:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBootloaderSystemInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->bootloaderSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    return-object v0
.end method

.method public static getBootloaderVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "v"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->bootloaderSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->bootloaderSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->bootloaderSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 48
    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->bootloaderSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getId()J
    .locals 2

    .prologue
    .line 38
    sget-wide v0, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->id:J

    return-wide v0
.end method

.method public static getKernelSystemInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->kernelSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    return-object v0
.end method

.method public static getKernelVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "v"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->kernelSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->kernelSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->kernelSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 43
    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->kernelSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public static getType()I
    .locals 1

    .prologue
    .line 22
    sget v0, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->type:I

    return v0
.end method
