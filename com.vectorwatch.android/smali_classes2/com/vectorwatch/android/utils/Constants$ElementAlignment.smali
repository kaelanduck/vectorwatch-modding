.class public final enum Lcom/vectorwatch/android/utils/Constants$ElementAlignment;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ElementAlignment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$ElementAlignment;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

.field public static final enum BOTTOM_CENTER:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

.field public static final enum BOTTOM_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

.field public static final enum BOTTOM_RIGHT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

.field public static final enum CENTER_CENTER:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

.field public static final enum CENTER_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

.field public static final enum CENTER_RIGHT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

.field public static final enum TOP_CENTER:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

.field public static final enum TOP_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

.field public static final enum TOP_RIGHT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 241
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    const-string v1, "TOP_LEFT"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->TOP_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    .line 242
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    const-string v1, "TOP_CENTER"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->TOP_CENTER:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    .line 243
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    const-string v1, "TOP_RIGHT"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->TOP_RIGHT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    .line 245
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    const-string v1, "CENTER_LEFT"

    invoke-direct {v0, v1, v7, v7}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->CENTER_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    .line 246
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    const-string v1, "CENTER_CENTER"

    invoke-direct {v0, v1, v8, v8}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->CENTER_CENTER:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    .line 247
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    const-string v1, "CENTER_RIGHT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->CENTER_RIGHT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    .line 249
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    const-string v1, "BOTTOM_LEFT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->BOTTOM_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    .line 250
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    const-string v1, "BOTTOM_CENTER"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->BOTTOM_CENTER:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    .line 251
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    const-string v1, "BOTTOM_RIGHT"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->BOTTOM_RIGHT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    .line 240
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->TOP_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->TOP_CENTER:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->TOP_RIGHT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->CENTER_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->CENTER_CENTER:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->CENTER_RIGHT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->BOTTOM_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->BOTTOM_CENTER:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->BOTTOM_RIGHT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 255
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 256
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->value:I

    .line 257
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$ElementAlignment;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 240
    const-class v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    return-object v0
.end method

.method public static valueOfString(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$ElementAlignment;
    .locals 5
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 270
    invoke-static {}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->values()[Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 271
    .local v0, "elementAlignment":Lcom/vectorwatch/android/utils/Constants$ElementAlignment;
    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 277
    .end local v0    # "elementAlignment":Lcom/vectorwatch/android/utils/Constants$ElementAlignment;
    :goto_1
    return-object v0

    .line 270
    .restart local v0    # "elementAlignment":Lcom/vectorwatch/android/utils/Constants$ElementAlignment;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 277
    .end local v0    # "elementAlignment":Lcom/vectorwatch/android/utils/Constants$ElementAlignment;
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->TOP_LEFT:Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    goto :goto_1
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$ElementAlignment;
    .locals 1

    .prologue
    .line 240
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$ElementAlignment;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$ElementAlignment;->value:I

    return v0
.end method
