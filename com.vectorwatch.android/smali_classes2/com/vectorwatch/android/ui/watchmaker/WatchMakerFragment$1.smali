.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;
.super Landroid/os/Handler;
.source "WatchMakerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 256
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->getCurrentItem()I

    move-result v0

    .line 257
    .local v0, "currentItem":I
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 273
    .end local v0    # "currentItem":I
    :goto_0
    return-void

    .line 259
    .restart local v0    # "currentItem":I
    :pswitch_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->addEmptyItem(I)I

    .line 260
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->notifyDataSetChanged()V

    .line 261
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget v2, p1, Landroid/os/Message;->arg1:I

    add-int/2addr v2, v0

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$200(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;IZ)V

    goto :goto_0

    .line 264
    :pswitch_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->cleanEmptyItems()V

    .line 265
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->notifyDataSetChanged()V

    .line 266
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-lez v1, :cond_1

    .line 267
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .end local v0    # "currentItem":I
    :goto_1
    invoke-static {v1, v0, v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$200(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;IZ)V

    goto :goto_0

    .restart local v0    # "currentItem":I
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    goto :goto_1

    .line 269
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    if-lez v0, :cond_2

    add-int/lit8 v1, v0, -0x1

    :goto_2
    invoke-static {v2, v1, v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$200(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;IZ)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 257
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
