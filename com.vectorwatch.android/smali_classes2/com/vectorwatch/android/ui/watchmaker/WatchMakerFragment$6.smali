.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->triggerSwitchToOnlineDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 441
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v7, 0x0

    .line 443
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setOfflineModeStatus(Landroid/content/Context;Z)V

    .line 444
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAccountAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 445
    .local v2, "username":Ljava/lang/String;
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->userLogOut(Ljava/lang/String;Landroid/accounts/AccountManager;)Z

    move-result v0

    .line 447
    .local v0, "isLoggedOut":Z
    const-string v3, "prefs_show_online"

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 448
    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 447
    invoke-static {v3, v4, v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 450
    if-eqz v0, :cond_0

    .line 452
    const-string v3, "account_update_type"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 453
    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 452
    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 455
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/vectorwatch/android/utils/Helpers;->logoutDrivenResets(Landroid/content/Context;)Z

    .line 457
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/managers/LiveStreamManager;->clearLiveStreamManager()V

    .line 459
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/LoginStateChangedEvent;

    invoke-direct {v4, v7}, Lcom/vectorwatch/android/events/LoginStateChangedEvent;-><init>(Z)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 461
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 462
    .local v1, "loginIntent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 464
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    .line 466
    .end local v1    # "loginIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
