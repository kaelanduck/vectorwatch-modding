.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnClickStreamListener"
.end annotation


# instance fields
.field private element:Lcom/vectorwatch/android/models/Element;

.field private placeHolder:Landroid/widget/EditText;

.field private stream:Lcom/vectorwatch/android/models/Stream;

.field private streamPosition:I

.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;ILcom/vectorwatch/android/models/Element;Landroid/widget/EditText;)V
    .locals 0
    .param p2, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p3, "streamPosition"    # I
    .param p4, "element"    # Lcom/vectorwatch/android/models/Element;
    .param p5, "placeHolder"    # Landroid/widget/EditText;

    .prologue
    .line 1656
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1657
    iput-object p2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 1658
    iput p3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->streamPosition:I

    .line 1659
    iput-object p4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->element:Lcom/vectorwatch/android/models/Element;

    .line 1660
    iput-object p5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->placeHolder:Landroid/widget/EditText;

    .line 1661
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1665
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getRenderOptions()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 1666
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getRenderOptions()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1667
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$1102(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Z)Z

    .line 1668
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$1202(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;)Lcom/vectorwatch/android/models/Stream;

    .line 1669
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->streamPosition:I

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$1302(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;I)I

    .line 1670
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->placeHolder:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$1402(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 1672
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->stream:Lcom/vectorwatch/android/models/Stream;

    iget v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->streamPosition:I

    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;->element:Lcom/vectorwatch/android/models/Element;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$1500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;ILcom/vectorwatch/android/models/Element;Landroid/view/View;)V

    .line 1674
    :cond_1
    return-void
.end method
