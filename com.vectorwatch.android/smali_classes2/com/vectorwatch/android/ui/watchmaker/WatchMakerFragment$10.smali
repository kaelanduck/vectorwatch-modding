.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

.field final synthetic val$currentApp:Lcom/vectorwatch/android/models/CloudElementSummary;

.field final synthetic val$input:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/widget/EditText;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 739
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->val$currentApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    iput-object p3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->val$input:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 742
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->val$currentApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->val$input:Landroid/widget/EditText;

    .line 743
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 742
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 744
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->getCurrentItem()I

    move-result v0

    .line 745
    .local v0, "position":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 746
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setCurrentItem(I)V

    .line 747
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 748
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 749
    return-void
.end method
