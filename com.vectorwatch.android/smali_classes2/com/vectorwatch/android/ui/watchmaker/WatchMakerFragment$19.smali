.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->replaceActiveStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

.field final synthetic val$stream:Lcom/vectorwatch/android/models/Stream;

.field final synthetic val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 1474
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iput-object p3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 8
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    const v7, 0x7f090114

    const/16 v6, 0xbb8

    .line 1487
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/vectorwatch/android/utils/Helpers;->handleRetrofitError(Landroid/content/Context;Lretrofit/RetrofitError;)V

    .line 1488
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    .line 1489
    .local v0, "response":Lretrofit/client/Response;
    if-eqz v0, :cond_2

    .line 1490
    invoke-virtual {v0}, Lretrofit/client/Response;->getStatus()I

    move-result v1

    .line 1491
    .local v1, "status":I
    invoke-static {v1}, Lcom/vectorwatch/android/utils/CloudStatusCodes;->getDefaultStringIdForStatus(I)I

    move-result v2

    .line 1492
    .local v2, "statusCodeMessageResId":I
    invoke-static {}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$300()Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SIGNUP: error = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1494
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 1495
    const/16 v3, 0x385

    if-ne v1, v3, :cond_0

    .line 1496
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-direct {v4, v5}, Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 1498
    :cond_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 1499
    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v4

    .line 1498
    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1510
    .end local v1    # "status":I
    .end local v2    # "statusCodeMessageResId":I
    :goto_0
    return-void

    .line 1502
    .restart local v1    # "status":I
    .restart local v2    # "statusCodeMessageResId":I
    :cond_1
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 1503
    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v4

    .line 1502
    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 1507
    .end local v1    # "status":I
    .end local v2    # "statusCodeMessageResId":I
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 1508
    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v4

    .line 1507
    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0
.end method

.method public success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V
    .locals 3
    .param p1, "streamDownloadResponse"    # Lcom/vectorwatch/android/models/StreamDownloadResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 1477
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamDownloadResponse;->getData()Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamDownloadResponse;->getData()Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;

    move-result-object v0

    .line 1478
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1479
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-static {v0, p1, v1, v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$1000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/StreamDownloadResponse;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    .line 1483
    :goto_0
    return-void

    .line 1481
    :cond_0
    const-string v0, "Unexpected"

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 1474
    check-cast p1, Lcom/vectorwatch/android/models/StreamDownloadResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;->success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V

    return-void
.end method
