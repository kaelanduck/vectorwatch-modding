.class public Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "CustomScrollView.java"


# static fields
.field private static final SWIPE_MAX_OFF_PATH:I = 0xfa

.field private static final SWIPE_MIN_DISTANCE:I = 0x78

.field private static final SWIPE_THRESHOLD_VELOCITY:I = 0x32


# instance fields
.field context:Landroid/content/Context;

.field private currIndex:I

.field private mAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

.field private mScrollable:Z

.field prevIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x1

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    iput v0, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->prevIndex:I

    .line 25
    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->mScrollable:Z

    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->context:Landroid/content/Context;

    .line 33
    return-void
.end method

.method private fillViewWithAdapter(Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;)V
    .locals 7
    .param p1, "mAdapter"    # Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vectorwatch/android/events/ZeroChildException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 50
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 51
    new-instance v2, Lcom/vectorwatch/android/events/ZeroChildException;

    const-string v3, "CenterLockHorizontalScrollView must have one child"

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/events/ZeroChildException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_1

    if-nez p1, :cond_2

    .line 69
    :cond_1
    :goto_0
    return-void

    .line 57
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 59
    .local v1, "parent":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 61
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 62
    instance-of v2, v1, Landroid/widget/LinearLayout;

    if-eqz v2, :cond_3

    .line 63
    invoke-virtual {p1, v0, v6, v1}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 65
    :cond_3
    invoke-virtual {p1, v0, v6, v1}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 68
    :cond_4
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->redrawViews()V

    goto :goto_0
.end method


# virtual methods
.method public getAdapter()Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->mAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    return-object v0
.end method

.method public getCurrIndex()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->currIndex:I

    return v0
.end method

.method public isScrollable()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->mScrollable:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->mScrollable:Z

    if-nez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 108
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onWindowFocusChanged(Z)V

    .line 88
    if-eqz p1, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->redrawViews()V

    .line 91
    :cond_0
    return-void
.end method

.method public redrawViews()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 74
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 75
    .local v1, "parent":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->mAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 77
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 78
    .local v2, "view":Landroid/view/View;
    new-instance v3, Lcom/vectorwatch/android/ui/custom/MyLongClickListener;

    invoke-direct {v3}, Lcom/vectorwatch/android/ui/custom/MyLongClickListener;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    iput v4, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->currIndex:I

    .line 82
    iput v4, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->prevIndex:I

    .line 83
    return-void
.end method

.method public setAdapter(Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;)V
    .locals 1
    .param p1, "mAdapter"    # Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->mAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    .line 42
    :try_start_0
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->fillViewWithAdapter(Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;)V
    :try_end_0
    .catch Lcom/vectorwatch/android/events/ZeroChildException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Lcom/vectorwatch/android/events/ZeroChildException;
    invoke-virtual {v0}, Lcom/vectorwatch/android/events/ZeroChildException;->printStackTrace()V

    goto :goto_0
.end method

.method public setScrollingEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->mScrollable:Z

    .line 95
    return-void
.end method
