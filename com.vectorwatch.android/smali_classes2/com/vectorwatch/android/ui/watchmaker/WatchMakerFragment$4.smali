.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 355
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    const/16 v6, 0x5dc

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 358
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 360
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v2}, Lcom/software/shell/fab/ActionButton;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 361
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0900ae

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 366
    .local v0, "notificationText":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v5, v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v2}, Lcom/software/shell/fab/ActionButton;->isSelected()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    invoke-virtual {v5, v2}, Lcom/software/shell/fab/ActionButton;->setSelected(Z)V

    .line 368
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v6, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 370
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v2}, Lcom/software/shell/fab/ActionButton;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 371
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    const v5, 0x7f020181

    invoke-virtual {v2, v5}, Lcom/software/shell/fab/ActionButton;->setImageResource(I)V

    .line 373
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v2

    sget-object v5, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_VIBRATION:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v6, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DISABLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v5, v6, v1}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 386
    :goto_2
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v1}, Lcom/software/shell/fab/ActionButton;->isSelected()Z

    move-result v1

    if-nez v1, :cond_0

    move v4, v3

    :cond_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDndState(Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 387
    const-string v1, "dnd_dirty"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 388
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSettings(Landroid/content/Context;)Ljava/util/UUID;

    .line 389
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 395
    .end local v0    # "notificationText":Ljava/lang/String;
    :goto_3
    return-void

    .line 363
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0900ad

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "notificationText":Ljava/lang/String;
    goto/16 :goto_0

    :cond_2
    move v2, v4

    .line 366
    goto :goto_1

    .line 378
    :cond_3
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    const v5, 0x7f020180

    invoke-virtual {v2, v5}, Lcom/software/shell/fab/ActionButton;->setImageResource(I)V

    .line 380
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v2

    sget-object v5, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_VIBRATION:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v6, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_ENABLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v5, v6, v1}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    goto :goto_2

    .line 392
    .end local v0    # "notificationText":Ljava/lang/String;
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 393
    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 392
    invoke-static {v1, v6, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_3
.end method
