.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$25;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->addFacePlaceholders(Lcom/vectorwatch/android/models/CloudElementSummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 2027
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$25;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2030
    const v2, 0x7f10000d

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2031
    .local v0, "editTextId":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 2032
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 2033
    .local v1, "etPlaceholder":Landroid/widget/EditText;
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2034
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$25;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v2, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$2000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Landroid/view/View;)V

    .line 2035
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2037
    .end local v1    # "etPlaceholder":Landroid/widget/EditText;
    :cond_0
    return-void
.end method
