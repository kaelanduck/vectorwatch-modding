.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/ServerResponseModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 323
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 6
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 336
    invoke-static {}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$300()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "Cloud check status - Cloud is not running"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 337
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/NetworkUtils;->isOnline(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 338
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "prefs_cloud_status"

    const-class v4, Lcom/vectorwatch/android/models/CloudStatus;

    invoke-virtual {v2, v3, v4}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/CloudStatus;

    .line 340
    .local v1, "oldCloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    new-instance v0, Lcom/vectorwatch/android/models/CloudStatus;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/vectorwatch/android/models/CloudStatus;-><init>(J)V

    .line 342
    .local v0, "cloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    if-nez v1, :cond_1

    .line 343
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "prefs_cloud_status"

    invoke-virtual {v2, v3, v0}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 352
    .end local v0    # "cloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    .end local v1    # "oldCloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    :cond_0
    :goto_0
    return-void

    .line 346
    .restart local v0    # "cloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    .restart local v1    # "oldCloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudStatus;->getTimestamp()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudStatus;->getTimestamp()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 347
    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 348
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$600(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    goto :goto_0
.end method

.method public success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V
    .locals 3
    .param p1, "serverResponseModel"    # Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 326
    invoke-static {}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$300()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Cloud check status - Cloud is running"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 327
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v0

    const-string v1, "prefs_cloud_status"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 329
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$400(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 332
    :cond_0
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 323
    check-cast p1, Lcom/vectorwatch/android/models/ServerResponseModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;->success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V

    return-void
.end method
