.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$5;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->triggerSwitchToOnlineDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

.field final synthetic val$currentTimestamp:J


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 470
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$5;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iput-wide p2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$5;->val$currentTimestamp:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 473
    const-string v0, "prefs_show_online"

    iget-wide v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$5;->val$currentTimestamp:J

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$5;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 474
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 473
    invoke-static {v0, v2, v3, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 475
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 476
    return-void
.end method
