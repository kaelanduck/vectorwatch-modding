.class public Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;
.super Ljava/lang/Object;
.source "WatchMakerInteractionListener.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/helper/OnWatchMakerFragmentInteractionListener;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;->mContext:Landroid/content/Context;

    .line 24
    return-void
.end method


# virtual methods
.method public appPositionChangedInCarousel(Ljava/util/List;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "cloudElementSummaryList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;->log:Lorg/slf4j/Logger;

    const-string v1, "WMListener: Requested app position change in carousel"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 43
    invoke-static {p1, p2}, Lcom/vectorwatch/android/managers/CloudAppsManager;->updateAppPosition(Ljava/util/List;Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public appRemovedFromCarousel(Lcom/vectorwatch/android/models/CloudElementSummary;I)V
    .locals 1
    .param p1, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p2, "oldPositionInAppsList"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;->mContext:Landroid/content/Context;

    invoke-static {p1, p2, v0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->uninstallAppFromWatch(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V

    .line 39
    return-void
.end method

.method public appSelected(Lcom/vectorwatch/android/models/CloudElementSummary;I)V
    .locals 1
    .param p1, "watchFace"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p2, "position"    # I

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;->mContext:Landroid/content/Context;

    invoke-static {p1, p2, v0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->sendActiveWatchFace(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V

    .line 29
    return-void
.end method
