.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->updateStreamCredentials(Lcom/vectorwatch/android/models/Stream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/AuthMethodResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

.field final synthetic val$listener:Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 2446
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;->val$listener:Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 2455
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;->val$listener:Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->onFailure()V

    .line 2456
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/vectorwatch/android/utils/Helpers;->handleRetrofitError(Landroid/content/Context;Lretrofit/RetrofitError;)V

    .line 2457
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/AuthMethodResponse;Lretrofit/client/Response;)V
    .locals 2
    .param p1, "authMethodResponse"    # Lcom/vectorwatch/android/models/AuthMethodResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 2449
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;->val$listener:Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/AuthMethodResponse;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V

    .line 2450
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;->val$listener:Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;->onSuccess()V

    .line 2451
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 2446
    check-cast p1, Lcom/vectorwatch/android/models/AuthMethodResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;->success(Lcom/vectorwatch/android/models/AuthMethodResponse;Lretrofit/client/Response;)V

    return-void
.end method
