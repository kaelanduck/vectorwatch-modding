.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$13;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->triggerAuthCredentialsChangeForApp(Lcom/vectorwatch/android/models/CloudElementSummary;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

.field final synthetic val$app:Lcom/vectorwatch/android/models/CloudElementSummary;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 785
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$13;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$13;->val$app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 788
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$13;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$13;->val$app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->startConfiguringAppCredentials(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 789
    return-void
.end method
