.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->removeStreamFromAllFaces(Lcom/vectorwatch/android/models/Stream;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

.field final synthetic val$stream:Lcom/vectorwatch/android/models/Stream;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 2413
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 4
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 2432
    invoke-static {}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$300()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error at downloading stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2434
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->MOBILE_APPLICATION_ERROR:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;-><init>(Ljava/lang/String;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 2437
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/vectorwatch/android/utils/Helpers;->handleRetrofitError(Landroid/content/Context;Lretrofit/RetrofitError;)V

    .line 2438
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V
    .locals 4
    .param p1, "streamDownloadResponse"    # Lcom/vectorwatch/android/models/StreamDownloadResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 2416
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamDownloadResponse;->getData()Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    .line 2417
    .local v0, "stream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getDynamic()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2419
    :cond_0
    const-string v1, "Unexpected"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;->failure(Lretrofit/RetrofitError;)V

    .line 2421
    invoke-static {}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$300()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Stream is not dynamic but stream possible settings is null!"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 2428
    :cond_1
    :goto_0
    return-void

    .line 2423
    :cond_2
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2424
    invoke-static {}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$300()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "REINSTALL STREAM "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2425
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/StreamDownloadCompletedEvent;

    invoke-direct {v2, v0}, Lcom/vectorwatch/android/events/StreamDownloadCompletedEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 2413
    check-cast p1, Lcom/vectorwatch/android/models/StreamDownloadResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;->success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V

    return-void
.end method
