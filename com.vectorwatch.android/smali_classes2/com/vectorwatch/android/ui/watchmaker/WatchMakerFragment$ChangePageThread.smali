.class Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;
.super Ljava/lang/Object;
.source "WatchMakerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChangePageThread"
.end annotation


# instance fields
.field private final mDirection:I

.field final synthetic this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;I)V
    .locals 0
    .param p2, "direction"    # I

    .prologue
    .line 2286
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2287
    iput p2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->mDirection:I

    .line 2288
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2291
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->getCurrentItem()I

    move-result v1

    .line 2292
    .local v1, "currentItemPosition":I
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCount()I

    move-result v3

    .line 2293
    .local v3, "numberOfFaces":I
    const/4 v0, 0x0

    .line 2294
    .local v0, "canSwap":Z
    const/4 v2, -0x1

    .line 2296
    .local v2, "newPosition":I
    iget v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->mDirection:I

    if-ne v4, v7, :cond_0

    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 2297
    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v4

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->isEmptyItemAtIndex(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2299
    const/4 v0, 0x1

    .line 2300
    add-int/lit8 v2, v1, 0x1

    .line 2302
    :cond_0
    iget v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->mDirection:I

    if-nez v4, :cond_1

    if-ltz v1, :cond_1

    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 2303
    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->isEmptyItemAtIndex(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2305
    const/4 v0, 0x1

    .line 2306
    add-int/lit8 v2, v1, -0x1

    .line 2308
    :cond_1
    if-eqz v0, :cond_3

    .line 2309
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$2100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2310
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v6}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$2200(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->removeView(Landroid/support/v4/view/ViewPager;I)I

    .line 2312
    add-int/lit8 v2, v2, -0x1

    .line 2314
    :cond_2
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v4, v7}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$2102(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Z)Z

    .line 2318
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->notifyDataSetChanged()V

    .line 2319
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v4, v2, v7}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->access$200(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;IZ)V

    .line 2321
    :cond_3
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;->this$0:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iget-object v4, v4, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mChangePageHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x2bc

    invoke-virtual {v4, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2322
    return-void
.end method
