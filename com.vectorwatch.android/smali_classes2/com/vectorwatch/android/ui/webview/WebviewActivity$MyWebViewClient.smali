.class Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "WebviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/webview/WebviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/webview/WebviewActivity;


# direct methods
.method private constructor <init>(Lcom/vectorwatch/android/ui/webview/WebviewActivity;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vectorwatch/android/ui/webview/WebviewActivity;Lcom/vectorwatch/android/ui/webview/WebviewActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vectorwatch/android/ui/webview/WebviewActivity;
    .param p2, "x1"    # Lcom/vectorwatch/android/ui/webview/WebviewActivity$1;

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;-><init>(Lcom/vectorwatch/android/ui/webview/WebviewActivity;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    # getter for: Lcom/vectorwatch/android/ui/webview/WebviewActivity;->isVisibile:Z
    invoke-static {v0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->access$400(Lcom/vectorwatch/android/ui/webview/WebviewActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const-string v0, "javascript:window.VectorWatchInterface.processContent(document.documentElement.outerHTML);"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 159
    :cond_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 143
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    # getter for: Lcom/vectorwatch/android/ui/webview/WebviewActivity;->isVisibile:Z
    invoke-static {v1}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->access$400(Lcom/vectorwatch/android/ui/webview/WebviewActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 145
    .local v0, "uriResource":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->startActivity(Landroid/content/Intent;)V

    .line 147
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 148
    invoke-virtual {p1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    invoke-virtual {p1}, Landroid/webkit/WebView;->goBack()V

    .line 153
    .end local v0    # "uriResource":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    # getter for: Lcom/vectorwatch/android/ui/webview/WebviewActivity;->isVisibile:Z
    invoke-static {v0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->access$400(Lcom/vectorwatch/android/ui/webview/WebviewActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Oh no! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 140
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 162
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    invoke-virtual {v1, p2}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->parseLoadedUrl(Ljava/lang/String;)Z

    move-result v0

    .line 163
    .local v0, "shouldOverrideUrlLoading":Z
    if-eqz v0, :cond_0

    .line 164
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->startActivity(Landroid/content/Intent;)V

    .line 166
    :cond_0
    return v0
.end method
