.class public Lcom/vectorwatch/android/ui/webview/WebviewActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "WebviewActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;,
        Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyJavaScriptInterface;,
        Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebChromeClient;
    }
.end annotation


# static fields
.field public static final EXTRA_KEY_ERROR_MSG:Ljava/lang/String; = "error_msg"

.field public static final EXTRA_KEY_PARAMS_SEARCHING:Ljava/lang/String; = "params"

.field public static final EXTRA_KEY_RESULT:Ljava/lang/String; = "result"

.field public static final EXTRA_KEY_URL:Ljava/lang/String; = "url"


# instance fields
.field private isVisibile:Z

.field private mWebView:Landroid/webkit/WebView;

.field private paramsToSearchFor:[Ljava/lang/String;

.field private progressBar:Landroid/view/View;

.field private result:Landroid/os/Bundle;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->isVisibile:Z

    .line 33
    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->mWebView:Landroid/webkit/WebView;

    .line 34
    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->url:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->progressBar:Landroid/view/View;

    .line 135
    return-void
.end method

.method public static GetWebViewActivityIntent(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "paramsToSearchFor"    # [Ljava/lang/String;

    .prologue
    .line 263
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 264
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    const-string v1, "params"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/webview/WebviewActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->progressBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/webview/WebviewActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/webview/WebviewActivity;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->isVisibile:Z

    return v0
.end method

.method private allParametersFound()Z
    .locals 6

    .prologue
    .line 91
    const/4 v0, 0x1

    .line 92
    .local v0, "allFound":Z
    iget-object v3, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->paramsToSearchFor:[Ljava/lang/String;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v1, v3, v2

    .line 94
    .local v1, "keyToSearch":Ljava/lang/String;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 95
    const/4 v0, 0x0

    .line 92
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 99
    .end local v1    # "keyToSearch":Ljava/lang/String;
    :cond_1
    return v0
.end method

.method private initWebView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 63
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebChromeClient;

    invoke-direct {v1, p0, v2}, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebChromeClient;-><init>(Lcom/vectorwatch/android/ui/webview/WebviewActivity;Lcom/vectorwatch/android/ui/webview/WebviewActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;

    invoke-direct {v1, p0, v2}, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyWebViewClient;-><init>(Lcom/vectorwatch/android/ui/webview/WebviewActivity;Lcom/vectorwatch/android/ui/webview/WebviewActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyJavaScriptInterface;

    invoke-direct {v1, p0, v2}, Lcom/vectorwatch/android/ui/webview/WebviewActivity$MyJavaScriptInterface;-><init>(Lcom/vectorwatch/android/ui/webview/WebviewActivity;Lcom/vectorwatch/android/ui/webview/WebviewActivity$1;)V

    const-string v2, "VectorWatchInterface"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method private returnErrorResult(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 71
    .local v0, "returnIntent":Landroid/content/Intent;
    const-string v1, "error_msg"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 73
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->finish()V

    .line 74
    return-void
.end method

.method private returnSuccessResult()V
    .locals 3

    .prologue
    .line 77
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 78
    .local v0, "returnIntent":Landroid/content/Intent;
    const-string v1, "result"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 79
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 80
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->finish()V

    .line 81
    return-void
.end method

.method private splitQuery(Ljava/net/URL;)Ljava/util/Map;
    .locals 11
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URL;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 217
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 218
    .local v4, "query_pairs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v3

    .line 219
    .local v3, "query":Ljava/lang/String;
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 220
    .local v2, "pairs":[Ljava/lang/String;
    array-length v7, v2

    move v5, v6

    :goto_0
    if-ge v5, v7, :cond_1

    aget-object v1, v2, v5

    .line 221
    .local v1, "pair":Ljava/lang/String;
    const-string v8, "="

    invoke-virtual {v1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 222
    .local v0, "idx":I
    if-lez v0, :cond_0

    .line 223
    invoke-virtual {v1, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const-string v9, "UTF-8"

    invoke-static {v8, v9}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "UTF-8"

    invoke-static {v9, v10}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 225
    :cond_0
    const-string v8, "UTF-8"

    invoke-static {v1, v8}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-interface {v4, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 228
    .end local v0    # "idx":I
    .end local v1    # "pair":Ljava/lang/String;
    :cond_1
    return-object v4
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f03002a

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->setContentView(I)V

    .line 48
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->url:Ljava/lang/String;

    .line 49
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->paramsToSearchFor:[Ljava/lang/String;

    .line 50
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->result:Landroid/os/Bundle;

    .line 51
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->url:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->url:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    :cond_0
    const-string v0, "No URL"

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->returnErrorResult(Ljava/lang/String;)V

    .line 59
    :goto_0
    return-void

    .line 56
    :cond_1
    const v0, 0x7f100111

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->mWebView:Landroid/webkit/WebView;

    .line 57
    const v0, 0x7f1000f8

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->progressBar:Landroid/view/View;

    .line 58
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->initWebView()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 249
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onPause()V

    .line 250
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->isVisibile:Z

    .line 251
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 243
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onResume()V

    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->isVisibile:Z

    .line 245
    return-void
.end method

.method public parseLoadedUrl(Ljava/lang/String;)Z
    .locals 11
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 178
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 179
    .local v3, "urlResource":Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v4

    const-string v7, "http"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 181
    invoke-direct {p0, v3}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->splitQuery(Ljava/net/URL;)Ljava/util/Map;

    move-result-object v2

    .line 183
    .local v2, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->paramsToSearchFor:[Ljava/lang/String;

    array-length v9, v8

    move v7, v5

    :goto_0
    if-ge v7, v9, :cond_1

    aget-object v1, v8, v7

    .line 185
    .local v1, "keyToSearch":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 187
    iget-object v10, p0, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->result:Landroid/os/Bundle;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v10, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    :cond_0
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_0

    .line 191
    .end local v1    # "keyToSearch":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->allParametersFound()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 193
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/webview/WebviewActivity;->returnSuccessResult()V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    move v4, v5

    .line 205
    .end local v2    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "urlResource":Ljava/net/URL;
    :goto_1
    return v4

    .restart local v3    # "urlResource":Ljava/net/URL;
    :cond_3
    move v4, v6

    .line 197
    goto :goto_1

    .line 199
    .end local v3    # "urlResource":Ljava/net/URL;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    move v4, v6

    .line 201
    goto :goto_1

    .line 202
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v0

    .line 204
    .local v0, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    move v4, v6

    .line 205
    goto :goto_1
.end method
