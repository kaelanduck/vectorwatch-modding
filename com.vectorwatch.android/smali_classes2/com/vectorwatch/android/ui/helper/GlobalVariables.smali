.class public Lcom/vectorwatch/android/ui/helper/GlobalVariables;
.super Ljava/lang/Object;
.source "GlobalVariables.java"


# static fields
.field private static instance:Lcom/vectorwatch/android/ui/helper/GlobalVariables;


# instance fields
.field private percentage:I

.field private shape:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/vectorwatch/android/ui/helper/GlobalVariables;

    invoke-direct {v0}, Lcom/vectorwatch/android/ui/helper/GlobalVariables;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/ui/helper/GlobalVariables;->instance:Lcom/vectorwatch/android/ui/helper/GlobalVariables;

    .line 24
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, "square"

    iput-object v0, p0, Lcom/vectorwatch/android/ui/helper/GlobalVariables;->shape:Ljava/lang/String;

    .line 18
    const/16 v0, 0x32

    iput v0, p0, Lcom/vectorwatch/android/ui/helper/GlobalVariables;->percentage:I

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/vectorwatch/android/ui/helper/GlobalVariables;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/vectorwatch/android/ui/helper/GlobalVariables;->instance:Lcom/vectorwatch/android/ui/helper/GlobalVariables;

    return-object v0
.end method


# virtual methods
.method public getPercentage()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/vectorwatch/android/ui/helper/GlobalVariables;->percentage:I

    return v0
.end method

.method public getShape()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/ui/helper/GlobalVariables;->shape:Ljava/lang/String;

    return-object v0
.end method

.method public setPercentage(I)V
    .locals 0
    .param p1, "percentage"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/vectorwatch/android/ui/helper/GlobalVariables;->percentage:I

    .line 61
    return-void
.end method

.method public setShape(Ljava/lang/String;)V
    .locals 0
    .param p1, "shape"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/vectorwatch/android/ui/helper/GlobalVariables;->shape:Ljava/lang/String;

    .line 49
    return-void
.end method
