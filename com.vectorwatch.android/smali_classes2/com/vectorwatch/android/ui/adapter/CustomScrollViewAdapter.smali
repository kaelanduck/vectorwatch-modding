.class public Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CustomScrollViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/vectorwatch/android/models/Stream;",
        ">;"
    }
.end annotation


# instance fields
.field public currPosition:I

.field private mContext:Landroid/content/Context;

.field mLayoutId:I

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation
.end field

.field private mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Landroid/util/LruCache;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "elementsTypeResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .local p4, "cache":Landroid/util/LruCache;, "Landroid/util/LruCache<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->currPosition:I

    .line 35
    iput-object p1, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mContext:Landroid/content/Context;

    .line 36
    iput-object p3, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mList:Ljava/util/List;

    .line 37
    iput-object p4, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mMemoryCache:Landroid/util/LruCache;

    .line 40
    iput p2, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mLayoutId:I

    .line 41
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->currPosition:I

    return v0
.end method

.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mList:Ljava/util/List;

    return-object v0
.end method

.method public getItem(I)Lcom/vectorwatch/android/models/Stream;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Stream;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->getItem(I)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 83
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->getItem(I)Lcom/vectorwatch/android/models/Stream;

    move-result-object v3

    .line 84
    .local v3, "stream":Lcom/vectorwatch/android/models/Stream;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mContext:Landroid/content/Context;

    iget v5, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mLayoutId:I

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 86
    .local v2, "layout":Landroid/view/ViewGroup;
    const v4, 0x7f100223

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 87
    .local v1, "image":Landroid/widget/ImageView;
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getImg()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getImg()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 88
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mMemoryCache:Landroid/util/LruCache;

    invoke-static {v4, v3}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecodeStream(Landroid/util/LruCache;Lcom/vectorwatch/android/models/Stream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 89
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 92
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 93
    return-object v2
.end method

.method public updateList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->mList:Ljava/util/List;

    .line 45
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->notifyDataSetChanged()V

    .line 46
    return-void
.end method
