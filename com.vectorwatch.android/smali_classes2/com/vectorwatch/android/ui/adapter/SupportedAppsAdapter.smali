.class public Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;
.super Landroid/widget/BaseAdapter;
.source "SupportedAppsAdapter.java"


# instance fields
.field mContext:Landroid/content/Context;

.field private mSupportedAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AppInfoModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AppInfoModel;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "supportedAppList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AppInfoModel;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;->mSupportedAppList:Ljava/util/List;

    .line 22
    iput-object p2, p0, Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;->mContext:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;->mSupportedAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/vectorwatch/android/models/AppInfoModel;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;->mSupportedAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/AppInfoModel;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;->getItem(I)Lcom/vectorwatch/android/models/AppInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 37
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 42
    if-nez p2, :cond_0

    .line 43
    new-instance p2, Lcom/vectorwatch/android/ui/view/SupportedAppView;

    .end local p2    # "convertView":Landroid/view/View;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p2, v0, p0}, Lcom/vectorwatch/android/ui/view/SupportedAppView;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;)V

    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    move-object v0, p2

    .line 46
    check-cast v0, Lcom/vectorwatch/android/ui/view/SupportedAppView;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;->getItem(I)Lcom/vectorwatch/android/models/AppInfoModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->bind(Lcom/vectorwatch/android/models/AppInfoModel;)V

    .line 48
    return-object p2
.end method
