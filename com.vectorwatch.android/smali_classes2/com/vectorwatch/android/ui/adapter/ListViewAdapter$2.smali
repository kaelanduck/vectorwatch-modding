.class Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;
.super Ljava/lang/Object;
.source "ListViewAdapter.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    iput p2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppOperation(Lcom/vectorwatch/android/models/StoreElement;)Ljava/lang/String;
    .locals 1
    .param p1, "item"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->getAppOperation(Lcom/vectorwatch/android/models/StoreElement;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onSaveAction(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;Landroid/widget/ImageView;)V
    .locals 8
    .param p1, "item"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "progressBar"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .param p3, "actionButton"    # Landroid/widget/ImageView;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 109
    new-instance v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 110
    .local v0, "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 111
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v5

    if-nez v5, :cond_4

    .line 115
    iget-object v5, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->access$000(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Landroid/app/Activity;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/vectorwatch/android/managers/CloudAppsManager;->isAppInstalled(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)Z

    move-result v1

    .line 116
    .local v1, "isAppSaved":Z
    if-eqz v1, :cond_0

    iget-object v5, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->access$100(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .line 117
    invoke-static {v5}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->access$100(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 124
    .end local v1    # "isAppSaved":Z
    .local v2, "isInstalled":Z
    :cond_1
    :goto_0
    if-nez v2, :cond_8

    .line 125
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v4, p1, p2, p3}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->startInstallApp(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;Landroid/widget/ImageView;)V

    .line 129
    :goto_1
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->access$300(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 130
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->access$300(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;

    move-result-object v4

    iget v5, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->val$position:I

    invoke-interface {v4, p1, v5}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;->onSaveAction(Lcom/vectorwatch/android/models/StoreElement;I)V

    .line 132
    :cond_2
    return-void

    .end local v2    # "isInstalled":Z
    .restart local v1    # "isAppSaved":Z
    :cond_3
    move v2, v4

    .line 117
    goto :goto_0

    .line 120
    .end local v1    # "isAppSaved":Z
    :cond_4
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-static {v6}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->access$000(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-static {v7}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->access$200(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Ljava/util/List;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/vectorwatch/android/managers/StreamsManager;->isStreamSaved(Ljava/lang/String;Landroid/content/Context;Ljava/util/List;)Z

    move-result v3

    .line 121
    .local v3, "isStreamSaved":Z
    if-eqz v3, :cond_5

    iget-object v5, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->access$100(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_5
    if-eqz v3, :cond_7

    iget-object v5, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .line 122
    invoke-static {v5}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->access$100(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    .restart local v2    # "isInstalled":Z
    :cond_6
    :goto_2
    goto :goto_0

    .end local v2    # "isInstalled":Z
    :cond_7
    move v2, v4

    goto :goto_2

    .line 127
    .end local v3    # "isStreamSaved":Z
    .restart local v2    # "isInstalled":Z
    :cond_8
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;->this$0:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    invoke-virtual {v4, p1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->startUninstallApp(Lcom/vectorwatch/android/models/StoreElement;)V

    goto :goto_1
.end method
