.class public Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "CarouselViewPagerAdapter.java"


# static fields
.field public static EMPTY_CLOUD_APP:I

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field clickListener:Landroid/view/View$OnClickListener;

.field private emptyItemIndex:I

.field longClickListener:Landroid/view/View$OnLongClickListener;

.field public mCloudElementSummaryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field mPager:Landroid/support/v4/view/ViewPager;

.field private mViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final pageWidth:I

.field previousIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/view/ViewPager;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Landroid/content/Context;ILandroid/util/LruCache;)V
    .locals 6
    .param p1, "pager"    # Landroid/support/v4/view/ViewPager;
    .param p3, "clickListener"    # Landroid/view/View$OnClickListener;
    .param p4, "longClickListener"    # Landroid/view/View$OnLongClickListener;
    .param p5, "context"    # Landroid/content/Context;
    .param p6, "pageWidth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/view/ViewPager;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnLongClickListener;",
            "Landroid/content/Context;",
            "I",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "activeWatchFaces":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    .local p7, "cache":Landroid/util/LruCache;, "Landroid/util/LruCache<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x0

    .line 54
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 47
    const/4 v4, -0x1

    iput v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->emptyItemIndex:I

    .line 55
    iput-object p1, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    .line 56
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    .line 57
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    .line 58
    iput-object p7, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mMemoryCache:Landroid/util/LruCache;

    .line 59
    iput-object p5, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mContext:Landroid/content/Context;

    .line 61
    iput-object p3, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->clickListener:Landroid/view/View$OnClickListener;

    .line 62
    iput-object p4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->longClickListener:Landroid/view/View$OnLongClickListener;

    .line 64
    iput p6, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->pageWidth:I

    .line 68
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 70
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 72
    invoke-static {p5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 73
    .local v2, "s":Ljava/lang/String;
    const-string v4, "round"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 74
    const v4, 0x7f0300bc

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 78
    .local v3, "v":Landroid/view/View;
    :goto_1
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    .end local v3    # "v":Landroid/view/View;
    :cond_0
    const v4, 0x7f03007c

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .restart local v3    # "v":Landroid/view/View;
    goto :goto_1

    .line 82
    .end local v2    # "s":Ljava/lang/String;
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 83
    return-void
.end method

.method private setCenter(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 212
    return-void
.end method


# virtual methods
.method public addEmptyItem(I)I
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 100
    iget v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->emptyItemIndex:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    .line 101
    new-instance v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 102
    .local v0, "emptyApp":Lcom/vectorwatch/android/models/CloudElementSummary;
    sget v4, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->EMPTY_CLOUD_APP:I

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 104
    new-instance v1, Landroid/view/View;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 105
    .local v1, "emptyView":Landroid/view/View;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 106
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v4, p1, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 117
    .end local v0    # "emptyApp":Lcom/vectorwatch/android/models/CloudElementSummary;
    .end local v1    # "emptyView":Landroid/view/View;
    :cond_0
    :goto_0
    iput p1, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->emptyItemIndex:I

    .line 119
    return p1

    .line 107
    :cond_1
    iget v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->emptyItemIndex:I

    if-eq v4, p1, :cond_0

    .line 108
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    iget v5, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->emptyItemIndex:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 109
    .local v3, "swapView":Landroid/view/View;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    iget v5, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->emptyItemIndex:I

    iget-object v6, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v4, p1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 112
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->emptyItemIndex:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 113
    .local v2, "swapApp":Lcom/vectorwatch/android/models/CloudElementSummary;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->emptyItemIndex:I

    iget-object v6, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public addViewAt(Landroid/view/View;Lcom/vectorwatch/android/models/CloudElementSummary;I)I
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p3, "position"    # I

    .prologue
    .line 217
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p3, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 219
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v0, p3, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 221
    return p3
.end method

.method public cleanEmptyItems()V
    .locals 6

    .prologue
    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v0, "dummyIndexes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 131
    iget-object v3, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v3

    sget v4, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->EMPTY_CLOUD_APP:I

    if-ne v3, v4, :cond_0

    .line 132
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 135
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 136
    iget-object v3, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 137
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 138
    .local v1, "dummyItemIndex":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 139
    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 141
    .end local v1    # "dummyItemIndex":Ljava/lang/Integer;
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, p0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 143
    :cond_3
    const/4 v3, -0x1

    iput v3, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->emptyItemIndex:I

    .line 144
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .param p1, "collection"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "view"    # Ljava/lang/Object;

    .prologue
    .line 348
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 349
    return-void
.end method

.method public getAppPosition(I)I
    .locals 2
    .param p1, "appId"    # I

    .prologue
    .line 158
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 160
    iget-object v1, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 166
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 158
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getAppsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method public getCurrentWatchFace()Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    return-object v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 2
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 338
    iget-object v1, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 339
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 340
    const/4 v0, -0x2

    .line 342
    .end local v0    # "index":I
    :cond_0
    return v0
.end method

.method public getView(I)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 150
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public getWatchFace(I)Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 154
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 13
    .param p1, "collection"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 256
    iget-object v10, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v10, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 257
    .local v9, "v":Landroid/view/View;
    invoke-virtual {p1, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 258
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 260
    if-ltz p2, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCount()I

    move-result v10

    if-ge p2, v10, :cond_0

    .line 263
    iget-object v10, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v10, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 265
    .local v3, "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v10

    sget v11, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->EMPTY_CLOUD_APP:I

    if-eq v10, v11, :cond_0

    .line 268
    const v10, 0x7f10022f

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 269
    .local v6, "imageView":Landroid/widget/ImageView;
    const v10, 0x7f100231

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 271
    .local v7, "placeholderTextView":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getType()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/vectorwatch/android/models/cloud/AppsOption;->PLACEHOLDER:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v11}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 272
    iget-object v10, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mMemoryCache:Landroid/util/LruCache;

    sget-object v11, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v10, v3, v11}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 273
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 274
    const-string v10, ""

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    const v10, 0x7f100230

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 288
    .local v1, "attentionIcon":Landroid/widget/ImageView;
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getAuthInvalidated()Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 289
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 290
    new-instance v4, Landroid/view/animation/AlphaAnimation;

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-direct {v4, v10, v11}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 291
    .local v4, "fadeIn":Landroid/view/animation/Animation;
    new-instance v10, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v10}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v4, v10}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 292
    const-wide/16 v10, 0x3e8

    invoke-virtual {v4, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 294
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    invoke-direct {v5, v10, v11}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 295
    .local v5, "fadeOut":Landroid/view/animation/Animation;
    new-instance v10, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v10}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v5, v10}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 296
    const-wide/16 v10, 0x3e8

    invoke-virtual {v5, v10, v11}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 297
    const-wide/16 v10, 0x3e8

    invoke-virtual {v5, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 299
    new-instance v0, Landroid/view/animation/AnimationSet;

    const/4 v10, 0x0

    invoke-direct {v0, v10}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 300
    .local v0, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 301
    invoke-virtual {v0, v5}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 303
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 304
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Landroid/view/animation/AnimationSet;->setFillBefore(Z)V

    .line 305
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Landroid/view/animation/AnimationSet;->setFillEnabled(Z)V

    .line 306
    new-instance v10, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter$1;

    invoke-direct {v10, p0, v0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter$1;-><init>(Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;Landroid/view/animation/AnimationSet;)V

    invoke-virtual {v0, v10}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 323
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 324
    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->start()V

    .line 333
    .end local v0    # "animationSet":Landroid/view/animation/AnimationSet;
    .end local v1    # "attentionIcon":Landroid/widget/ImageView;
    .end local v3    # "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    .end local v4    # "fadeIn":Landroid/view/animation/Animation;
    .end local v5    # "fadeOut":Landroid/view/animation/Animation;
    .end local v6    # "imageView":Landroid/widget/ImageView;
    .end local v7    # "placeholderTextView":Landroid/widget/TextView;
    :cond_0
    :goto_1
    return-object v9

    .line 276
    .restart local v3    # "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    .restart local v6    # "imageView":Landroid/widget/ImageView;
    .restart local v7    # "placeholderTextView":Landroid/widget/TextView;
    :cond_1
    iget-object v10, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 277
    .local v8, "s":Ljava/lang/String;
    const-string v10, "round"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 278
    const v10, 0x7f020130

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 283
    :goto_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 284
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mContext:Landroid/content/Context;

    .line 283
    invoke-static {v10, v11, v12}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 280
    :cond_2
    const v10, 0x7f020131

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 328
    .end local v8    # "s":Ljava/lang/String;
    .restart local v1    # "attentionIcon":Landroid/widget/ImageView;
    :cond_3
    const/16 v10, 0x8

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public isEmptyItemAtIndex(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 124
    iget-object v1, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 125
    .local v0, "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v1

    sget v2, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->EMPTY_CLOUD_APP:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 359
    check-cast p2, Landroid/view/View;

    .end local p2    # "arg1":Ljava/lang/Object;
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeClickListeners()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 196
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->longClickListener:Landroid/view/View$OnLongClickListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->clickListener:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    .line 197
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 198
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 202
    .local v1, "view":Landroid/view/View;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 204
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208
    .end local v0    # "index":I
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public removeView(Landroid/support/v4/view/ViewPager;I)I
    .locals 1
    .param p1, "pager"    # Landroid/support/v4/view/ViewPager;
    .param p2, "position"    # I

    .prologue
    .line 246
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 247
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 248
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 250
    invoke-virtual {p1, p0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 251
    return p2
.end method

.method public removeView(Landroid/support/v4/view/ViewPager;Landroid/view/View;)I
    .locals 2
    .param p1, "pager"    # Landroid/support/v4/view/ViewPager;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    const/4 v0, -0x1

    .line 229
    iget-object v1, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->removeView(Landroid/support/v4/view/ViewPager;I)I

    move-result v0

    .line 232
    :cond_0
    return v0
.end method

.method public resetClickListeners()V
    .locals 3

    .prologue
    .line 174
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->longClickListener:Landroid/view/View$OnLongClickListener;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->clickListener:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_1

    .line 175
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 176
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 180
    .local v1, "view":Landroid/view/View;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 182
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentItem()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 184
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->longClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 175
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_1

    .line 193
    .end local v0    # "index":I
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public updateList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->mCloudElementSummaryList:Ljava/util/ArrayList;

    .line 91
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->notifyDataSetChanged()V

    .line 92
    return-void
.end method
