.class Lcom/vectorwatch/android/ui/view/SupportedAppView$1;
.super Ljava/lang/Object;
.source "SupportedAppView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/view/SupportedAppView;->bind(Lcom/vectorwatch/android/models/AppInfoModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/view/SupportedAppView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/SupportedAppView;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/SupportedAppView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->access$000(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/SupportedAppView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->access$000(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setChecked(Z)V

    .line 70
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/SupportedAppView;->log:Lorg/slf4j/Logger;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->access$100(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Preferences - Supported app enabler - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/SupportedAppView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->access$000(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/SupportedAppView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->access$200(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lcom/vectorwatch/android/models/AppInfoModel;

    move-result-object v0

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/SupportedAppView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->access$000(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/SupportedAppView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->access$300(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 73
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->setTaskAppearence()V

    .line 74
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/SupportedAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/SupportedAppView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->access$000(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setChecked(Z)V

    goto :goto_0
.end method
