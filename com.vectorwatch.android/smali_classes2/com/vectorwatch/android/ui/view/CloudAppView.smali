.class public Lcom/vectorwatch/android/ui/view/CloudAppView;
.super Landroid/widget/RelativeLayout;
.source "CloudAppView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;
    }
.end annotation


# instance fields
.field private appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

.field private log:Lorg/slf4j/Logger;

.field private mActionButton:Landroid/widget/ImageView;

.field private mCallbacks:Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;

.field private mCloudAppDescription:Landroid/widget/TextView;

.field private mCloudAppImage:Landroid/widget/ImageView;

.field private mCloudAppName:Landroid/widget/TextView;

.field private mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

.field private mContext:Landroid/content/Context;

.field private mImgInstalled:Landroid/widget/ImageView;

.field private mIsInstalled:Z

.field private mIsSelected:Z

.field private mIsStream:Z

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mNewView:Landroid/widget/RelativeLayout;

.field private final mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

.field private mTypeText:Landroid/widget/TextView;

.field private ratingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;)V
    .locals 4
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "resourceId"    # I
    .param p3, "callbacks"    # Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 40
    const-class v2, Lcom/vectorwatch/android/ui/view/CloudAppView;

    invoke-static {v2}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->log:Lorg/slf4j/Logger;

    .line 69
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    .line 70
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 71
    iput-object p3, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCallbacks:Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;

    .line 73
    const-string v2, "layout_inflater"

    .line 74
    invoke-virtual {p1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 75
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x1

    invoke-virtual {v0, p2, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 77
    const v2, 0x7f1000f8

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 78
    const v2, 0x7f100239

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppName:Landroid/widget/TextView;

    .line 79
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppName:Landroid/widget/TextView;

    if-nez v2, :cond_0

    .line 80
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->log:Lorg/slf4j/Logger;

    const-string v3, "NULL"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 82
    :cond_0
    const v2, 0x7f1000ec

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppDescription:Landroid/widget/TextView;

    .line 83
    const v2, 0x7f1001ec

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    .line 84
    const v2, 0x7f10023b

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mImgInstalled:Landroid/widget/ImageView;

    .line 85
    const v2, 0x7f10023d

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mNewView:Landroid/widget/RelativeLayout;

    .line 86
    const v2, 0x7f1000eb

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->ratingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    .line 87
    const v2, 0x7f10023e

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mTypeText:Landroid/widget/TextView;

    .line 89
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "square"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 91
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x42ac0000    # 86.0f

    invoke-static {v2, v3}, Lcom/github/lzyzsd/circleprogress/Utils;->dp2px(Landroid/content/res/Resources;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 92
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 95
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    const v2, 0x7f10023a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;

    .line 98
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;

    new-instance v3, Lcom/vectorwatch/android/ui/view/CloudAppView$1;

    invoke-direct {v3, p0, p1}, Lcom/vectorwatch/android/ui/view/CloudAppView$1;-><init>(Lcom/vectorwatch/android/ui/view/CloudAppView;Landroid/app/Activity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lorg/slf4j/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/utils/AppInstallManager;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/view/CloudAppView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/view/CloudAppView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mIsStream:Z

    return v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/view/CloudAppView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mIsInstalled:Z

    return v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/view/CloudAppView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCallbacks:Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/models/StoreElement;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    return-object v0
.end method

.method private updateSelectedState(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4
    .param p1, "listType"    # Ljava/lang/String;
    .param p2, "s"    # Ljava/lang/String;
    .param p3, "isInstalled"    # Z
    .param p4, "isTransmitted"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 199
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mIsSelected:Z

    if-eqz v0, :cond_6

    .line 200
    if-eqz p4, :cond_1

    .line 201
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 213
    :goto_0
    const-string v0, "store"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 214
    const-string v0, "round"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 215
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020113

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 246
    :cond_0
    :goto_1
    return-void

    .line 204
    :cond_1
    if-eqz p3, :cond_2

    .line 205
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;

    const v1, 0x7f0200e8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 209
    :goto_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 210
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;

    const v1, 0x7f0200ee

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 218
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020112

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 222
    :cond_4
    const-string v0, "round"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 223
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020110

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 226
    :cond_5
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v0

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02010f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 231
    :cond_6
    if-eqz p4, :cond_7

    .line 232
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 238
    :goto_3
    const-string v0, "round"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 239
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020111

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 235
    :cond_7
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    goto :goto_3

    .line 242
    :cond_8
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public bind(Lcom/vectorwatch/android/models/StoreElement;ZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Landroid/util/LruCache;)V
    .locals 9
    .param p1, "cloudElementSummary"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "isSelected"    # Z
    .param p3, "listType"    # Ljava/lang/String;
    .param p4, "shape"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/StoreElement;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .local p6, "mMemoryCache":Landroid/util/LruCache;, "Landroid/util/LruCache<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 135
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    .line 136
    iput-boolean p2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mIsSelected:Z

    .line 137
    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v7

    const-string v8, "STREAM"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mIsStream:Z

    .line 139
    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppName:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppDescription:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/StoreElement;->getDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 143
    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    sget-object v8, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {p6, p1, v8}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 151
    :goto_0
    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v8}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    move v2, v5

    .line 152
    .local v2, "isBeingTransmitted":Z
    :goto_1
    const/4 v3, 0x0

    .line 154
    .local v3, "isInstalled":Z
    new-instance v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 155
    .local v0, "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 156
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 158
    iget-boolean v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mIsStream:Z

    if-nez v7, :cond_6

    .line 161
    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    invoke-static {v0, v7}, Lcom/vectorwatch/android/managers/CloudAppsManager;->isAppInstalled(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)Z

    move-result v1

    .line 162
    .local v1, "isAppSaved":Z
    if-eqz v1, :cond_0

    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    if-eqz v1, :cond_5

    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 163
    invoke-virtual {v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    :cond_1
    move v3, v5

    .line 174
    .end local v1    # "isAppSaved":Z
    :goto_2
    iput-boolean v3, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mIsInstalled:Z

    .line 177
    if-eqz v3, :cond_a

    .line 178
    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mImgInstalled:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 183
    :goto_3
    invoke-direct {p0, p3, p4, v3, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->updateSelectedState(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 185
    const-string v5, "store_search"

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 186
    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mTypeText:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    :cond_2
    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/StoreElement;->getNewInStore()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 190
    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mNewView:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 195
    :goto_4
    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->ratingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getRating()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    .line 196
    return-void

    .line 145
    .end local v0    # "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    .end local v2    # "isBeingTransmitted":Z
    .end local v3    # "isInstalled":Z
    :cond_3
    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v7

    .line 146
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getImgURL()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v7

    .line 147
    invoke-virtual {v7}, Lcom/bumptech/glide/DrawableTypeRequest;->crossFade()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudAppImage:Landroid/widget/ImageView;

    .line 148
    invoke-virtual {v7, v8}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    goto/16 :goto_0

    :cond_4
    move v2, v6

    .line 151
    goto/16 :goto_1

    .restart local v0    # "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    .restart local v1    # "isAppSaved":Z
    .restart local v2    # "isBeingTransmitted":Z
    .restart local v3    # "isInstalled":Z
    :cond_5
    move v3, v6

    .line 163
    goto :goto_2

    .line 169
    .end local v1    # "isAppSaved":Z
    :cond_6
    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;

    invoke-static {v7, v8, p5}, Lcom/vectorwatch/android/managers/StreamsManager;->isStreamSaved(Ljava/lang/String;Landroid/content/Context;Ljava/util/List;)Z

    move-result v4

    .line 170
    .local v4, "isStreamSaved":Z
    if-eqz v4, :cond_7

    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v7

    if-eqz v7, :cond_8

    :cond_7
    if-eqz v4, :cond_9

    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 171
    invoke-virtual {v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_8
    move v3, v5

    :goto_5
    goto/16 :goto_2

    :cond_9
    move v3, v6

    goto :goto_5

    .line 180
    .end local v4    # "isStreamSaved":Z
    :cond_a
    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mImgInstalled:Landroid/widget/ImageView;

    const/4 v7, 0x4

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 192
    :cond_b
    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mNewView:Landroid/widget/RelativeLayout;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_4
.end method

.method public getCallbacks()Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCallbacks:Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;

    return-object v0
.end method

.method public setCallbacks(Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView;->mCallbacks:Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;

    .line 255
    return-void
.end method
