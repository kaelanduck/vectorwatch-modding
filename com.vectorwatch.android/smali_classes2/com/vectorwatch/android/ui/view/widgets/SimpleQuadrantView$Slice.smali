.class public Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;
.super Ljava/lang/Object;
.source "SimpleQuadrantView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Slice"
.end annotation


# instance fields
.field public fillPaint:Landroid/graphics/Paint;

.field public path:Landroid/graphics/Path;

.field public pathOutline:Landroid/graphics/Path;

.field public pathSide:Landroid/graphics/Path;

.field public strokePaint:Landroid/graphics/Paint;

.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

.field public value:F


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;FIILandroid/graphics/Paint$Style;)V
    .locals 1
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;
    .param p2, "value"    # F
    .param p3, "fillColor"    # I
    .param p4, "strokeColor"    # I
    .param p5, "style"    # Landroid/graphics/Paint$Style;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->path:Landroid/graphics/Path;

    .line 183
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    .line 188
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->pathSide:Landroid/graphics/Path;

    .line 191
    iput p2, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->value:F

    .line 192
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    # invokes: Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->buildStrokePaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    invoke-static {p4, v0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->access$000(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->strokePaint:Landroid/graphics/Paint;

    .line 193
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    # invokes: Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->buildFillPaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    invoke-static {p3, v0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->access$100(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->fillPaint:Landroid/graphics/Paint;

    .line 194
    return-void
.end method
