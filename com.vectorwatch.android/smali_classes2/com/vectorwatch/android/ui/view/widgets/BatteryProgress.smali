.class public Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;
.super Landroid/widget/ImageView;
.source "BatteryProgress.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;
    }
.end annotation


# static fields
.field private static final FILL_GRADIENT:Z = false

.field public static final LOGD:Z = false

.field public static final TAG:Ljava/lang/String; = "SleepQuadrantView"


# instance fields
.field private backgroundImageView:Landroid/widget/ImageView;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mOriginAngle:I

.field private mSlices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;",
            ">;"
        }
    .end annotation
.end field

.field private maxValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mSlices:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mMatrix:Landroid/graphics/Matrix;

    .line 31
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    .line 46
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->setWillNotDraw(Z)V

    .line 47
    return-void
.end method

.method static synthetic access$000(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # Landroid/content/res/Resources;

    .prologue
    .line 21
    invoke-static {p0, p1}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->buildStrokePaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # Landroid/content/res/Resources;

    .prologue
    .line 21
    invoke-static {p0, p1}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->buildFillPaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method

.method private static buildFillPaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 2
    .param p0, "color"    # I
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 61
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 63
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 64
    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 67
    return-object v0
.end method

.method private static buildStrokePaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 3
    .param p0, "color"    # I
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 50
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 52
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 53
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 55
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 57
    return-object v0
.end method

.method public static darken(I)I
    .locals 4
    .param p0, "color"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 71
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 72
    .local v0, "hsv":[F
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 73
    const/4 v1, 0x2

    aget v2, v0, v1

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 74
    const/4 v1, 0x1

    aget v2, v0, v1

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 75
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    return v1
.end method


# virtual methods
.method public addSlice(FIILandroid/graphics/Paint$Style;)V
    .locals 7
    .param p1, "value"    # F
    .param p2, "fillColor"    # I
    .param p3, "strokeColor"    # I
    .param p4, "style"    # Landroid/graphics/Paint$Style;

    .prologue
    .line 90
    iget-object v6, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mSlices:Ljava/util/ArrayList;

    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;-><init>(Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;FIILandroid/graphics/Paint$Style;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method public generatePath()V
    .locals 18

    .prologue
    .line 114
    move-object/from16 v0, p0

    iget v13, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-nez v13, :cond_0

    .line 115
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->invalidate()V

    .line 164
    :goto_0
    return-void

    .line 119
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;

    .line 120
    .local v6, "slice":Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->path:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->reset()V

    .line 121
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->pathSide:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->reset()V

    .line 122
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->pathOutline:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->reset()V

    goto :goto_1

    .line 125
    .end local v6    # "slice":Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->getWidth()I

    move-result v12

    .line 126
    .local v12, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->getHeight()I

    move-result v3

    .line 128
    .local v3, "height":I
    new-instance v4, Landroid/graphics/RectF;

    const/4 v13, 0x0

    const/4 v14, 0x0

    int-to-float v15, v12

    int-to-float v0, v3

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-direct {v4, v13, v14, v15, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 129
    .local v4, "rect":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 130
    .local v5, "rectSide":Landroid/graphics/RectF;
    invoke-virtual {v5, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 132
    move-object/from16 v0, p0

    iget v7, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mOriginAngle:I

    .line 133
    .local v7, "startAngle":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;

    .line 134
    .restart local v6    # "slice":Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;
    const/4 v11, 0x0

    .line 135
    .local v11, "tempMax":F
    move-object/from16 v0, p0

    iget v14, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    iget v15, v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->value:F

    cmpg-float v14, v14, v15

    if-gtz v14, :cond_4

    .line 136
    iget v11, v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->value:F

    .line 140
    :goto_3
    iget v14, v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->value:F

    const v15, 0x43b3ffdf    # 359.999f

    mul-float/2addr v14, v15

    div-float v9, v14, v11

    .line 141
    .local v9, "sweepAngle":F
    int-to-float v14, v7

    add-float v1, v14, v9

    .line 145
    .local v1, "endAngle":F
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->path:Landroid/graphics/Path;

    int-to-float v15, v7

    invoke-virtual {v14, v4, v15, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 149
    int-to-float v8, v7

    .line 150
    .local v8, "startAngleSide":F
    move v2, v1

    .line 151
    .local v2, "endAngleSide":F
    sub-float v10, v2, v8

    .line 155
    .local v10, "sweepAngleSide":F
    move-object/from16 v0, p0

    iget v14, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    const/high16 v15, 0x43b40000    # 360.0f

    cmpl-float v14, v14, v15

    if-lez v14, :cond_2

    .line 156
    const/high16 v14, 0x43b40000    # 360.0f

    move-object/from16 v0, p0

    iput v14, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    .line 157
    :cond_2
    move-object/from16 v0, p0

    iget v14, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    const/4 v15, 0x0

    cmpg-float v14, v14, v15

    if-gez v14, :cond_3

    .line 158
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    .line 159
    :cond_3
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->pathOutline:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    const/high16 v16, 0x43b40000    # 360.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    move/from16 v0, v16

    invoke-virtual {v14, v5, v15, v0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto :goto_2

    .line 138
    .end local v1    # "endAngle":F
    .end local v2    # "endAngleSide":F
    .end local v8    # "startAngleSide":F
    .end local v9    # "sweepAngle":F
    .end local v10    # "sweepAngleSide":F
    :cond_4
    move-object/from16 v0, p0

    iget v11, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    goto :goto_3

    .line 163
    .end local v6    # "slice":Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;
    .end local v11    # "tempMax":F
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->invalidate()V

    goto/16 :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 169
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 171
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;

    .line 173
    .local v0, "slice":Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;
    iget-object v2, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->pathOutline:Landroid/graphics/Path;

    iget-object v3, v0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 176
    .end local v0    # "slice":Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress$Slice;
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const v3, 0x3f70a3d7    # 0.94f

    .line 99
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v0, v2

    .line 100
    .local v0, "centerX":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v1, v2

    .line 103
    .local v1, "centerY":F
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 104
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 105
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mMatrix:Landroid/graphics/Matrix;

    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-virtual {v2, v3, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 107
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->generatePath()V

    .line 108
    return-void
.end method

.method public removeAllSlices()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 95
    return-void
.end method

.method public setValues(F)V
    .locals 4
    .param p1, "maxValue"    # F

    .prologue
    .line 79
    const/high16 v1, 0x42100000    # 36.0f

    mul-float/2addr v1, p1

    const/high16 v2, 0x41200000    # 10.0f

    div-float p1, v1, v2

    .line 80
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->maxValue:F

    .line 81
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->removeAllSlices()V

    .line 82
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00af

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 84
    .local v0, "yellowThemeColor":I
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, -0x1000000

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->addSlice(FIILandroid/graphics/Paint$Style;)V

    .line 85
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/BatteryProgress;->invalidate()V

    .line 86
    return-void
.end method
