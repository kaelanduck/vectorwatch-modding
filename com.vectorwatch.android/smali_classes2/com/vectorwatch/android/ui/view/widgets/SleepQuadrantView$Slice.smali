.class public Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;
.super Ljava/lang/Object;
.source "SleepQuadrantView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Slice"
.end annotation


# instance fields
.field public fillPaint:Landroid/graphics/Paint;

.field public path:Landroid/graphics/Path;

.field public pathOutline:Landroid/graphics/Path;

.field public pathSide:Landroid/graphics/Path;

.field public strokePaint:Landroid/graphics/Paint;

.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;

.field public value:F


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;FIILandroid/graphics/Paint$Style;)V
    .locals 1
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;
    .param p2, "value"    # F
    .param p3, "fillColor"    # I
    .param p4, "strokeColor"    # I
    .param p5, "style"    # Landroid/graphics/Paint$Style;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->path:Landroid/graphics/Path;

    .line 198
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    .line 203
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->pathSide:Landroid/graphics/Path;

    .line 206
    iput p2, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->value:F

    .line 207
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    # invokes: Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->buildStrokePaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    invoke-static {p4, v0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->access$000(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->strokePaint:Landroid/graphics/Paint;

    .line 208
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    # invokes: Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->buildFillPaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    invoke-static {p3, v0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->access$100(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->fillPaint:Landroid/graphics/Paint;

    .line 209
    return-void
.end method
