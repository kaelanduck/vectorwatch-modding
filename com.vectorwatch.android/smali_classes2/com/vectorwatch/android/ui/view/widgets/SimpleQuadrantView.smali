.class public Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;
.super Landroid/widget/ImageView;
.source "SimpleQuadrantView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;
    }
.end annotation


# static fields
.field private static final FILL_GRADIENT:Z = false

.field public static final LOGD:Z = false

.field public static final TAG:Ljava/lang/String; = "SleepQuadrantView"


# instance fields
.field private backgroundImageView:Landroid/widget/ImageView;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mOriginAngle:I

.field private mSlices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;",
            ">;"
        }
    .end annotation
.end field

.field private maxValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mSlices:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    .line 35
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->maxValue:F

    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->setWillNotDraw(Z)V

    .line 51
    return-void
.end method

.method static synthetic access$000(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # Landroid/content/res/Resources;

    .prologue
    .line 25
    invoke-static {p0, p1}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->buildStrokePaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # Landroid/content/res/Resources;

    .prologue
    .line 25
    invoke-static {p0, p1}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->buildFillPaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method

.method private static buildFillPaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 2
    .param p0, "color"    # I
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 65
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 67
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 68
    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 69
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 71
    return-object v0
.end method

.method private static buildStrokePaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 3
    .param p0, "color"    # I
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 54
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 56
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 58
    const/high16 v1, 0x41100000    # 9.0f

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 59
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 61
    return-object v0
.end method

.method public static darken(I)I
    .locals 4
    .param p0, "color"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 75
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 76
    .local v0, "hsv":[F
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 77
    const/4 v1, 0x2

    aget v2, v0, v1

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 78
    const/4 v1, 0x1

    aget v2, v0, v1

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 79
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    return v1
.end method


# virtual methods
.method public addSlice(FIILandroid/graphics/Paint$Style;)V
    .locals 7
    .param p1, "value"    # F
    .param p2, "fillColor"    # I
    .param p3, "strokeColor"    # I
    .param p4, "style"    # Landroid/graphics/Paint$Style;

    .prologue
    .line 95
    iget-object v6, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mSlices:Ljava/util/ArrayList;

    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;-><init>(Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;FIILandroid/graphics/Paint$Style;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    return-void
.end method

.method public generatePath()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 119
    iget v12, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->maxValue:F

    cmpl-float v12, v12, v14

    if-nez v12, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->invalidate()V

    .line 165
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;

    .line 125
    .local v5, "slice":Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;
    iget-object v13, v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->path:Landroid/graphics/Path;

    invoke-virtual {v13}, Landroid/graphics/Path;->reset()V

    .line 126
    iget-object v13, v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->pathSide:Landroid/graphics/Path;

    invoke-virtual {v13}, Landroid/graphics/Path;->reset()V

    .line 127
    iget-object v13, v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    invoke-virtual {v13}, Landroid/graphics/Path;->reset()V

    goto :goto_1

    .line 130
    .end local v5    # "slice":Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->getWidth()I

    move-result v11

    .line 131
    .local v11, "width":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->getHeight()I

    move-result v2

    .line 133
    .local v2, "height":I
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v12, v11

    int-to-float v13, v2

    invoke-direct {v3, v14, v14, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 134
    .local v3, "rect":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 135
    .local v4, "rectSide":Landroid/graphics/RectF;
    invoke-virtual {v4, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 137
    iget v6, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mOriginAngle:I

    .line 138
    .local v6, "startAngle":I
    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;

    .line 139
    .restart local v5    # "slice":Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;
    const/4 v10, 0x0

    .line 140
    .local v10, "tempMax":F
    iget v13, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->maxValue:F

    iget v14, v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->value:F

    cmpg-float v13, v13, v14

    if-gtz v13, :cond_2

    .line 141
    iget v10, v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->value:F

    .line 145
    :goto_3
    iget v13, v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->value:F

    const v14, 0x43b3ffdf    # 359.999f

    mul-float/2addr v13, v14

    div-float v8, v13, v10

    .line 146
    .local v8, "sweepAngle":F
    int-to-float v13, v6

    add-float v0, v13, v8

    .line 150
    .local v0, "endAngle":F
    iget-object v13, v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->path:Landroid/graphics/Path;

    int-to-float v14, v6

    invoke-virtual {v13, v3, v14, v8}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 154
    int-to-float v7, v6

    .line 155
    .local v7, "startAngleSide":F
    move v1, v0

    .line 156
    .local v1, "endAngleSide":F
    sub-float v9, v1, v7

    .line 160
    .local v9, "sweepAngleSide":F
    iget-object v13, v5, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    invoke-virtual {v13, v4, v7, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto :goto_2

    .line 143
    .end local v0    # "endAngle":F
    .end local v1    # "endAngleSide":F
    .end local v7    # "startAngleSide":F
    .end local v8    # "sweepAngle":F
    .end local v9    # "sweepAngleSide":F
    :cond_2
    iget v10, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->maxValue:F

    goto :goto_3

    .line 164
    .end local v5    # "slice":Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;
    .end local v10    # "tempMax":F
    :cond_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->invalidate()V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 170
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 172
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;

    .line 174
    .local v0, "slice":Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;
    iget-object v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    iget-object v3, v0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 177
    .end local v0    # "slice":Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView$Slice;
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const v3, 0x3f70a3d7    # 0.94f

    .line 104
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v0, v2

    .line 105
    .local v0, "centerX":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v1, v2

    .line 108
    .local v1, "centerY":F
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 109
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 110
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-virtual {v2, v3, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 112
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->generatePath()V

    .line 113
    return-void
.end method

.method public removeAllSlices()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 100
    return-void
.end method

.method public setOriginAngle(I)V
    .locals 0
    .param p1, "originAngle"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->mOriginAngle:I

    .line 92
    return-void
.end method

.method public setValues(FFI)V
    .locals 2
    .param p1, "value"    # F
    .param p2, "maxValue"    # F
    .param p3, "color"    # I

    .prologue
    .line 83
    iput p2, p0, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->maxValue:F

    .line 84
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->removeAllSlices()V

    .line 85
    const/high16 v0, -0x1000000

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p0, p1, v0, p3, v1}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->addSlice(FIILandroid/graphics/Paint$Style;)V

    .line 86
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->invalidate()V

    .line 87
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SimpleQuadrantView;->generatePath()V

    .line 88
    return-void
.end method
