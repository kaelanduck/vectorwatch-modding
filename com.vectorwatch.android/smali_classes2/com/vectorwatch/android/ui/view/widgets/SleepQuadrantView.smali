.class public Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;
.super Landroid/widget/ImageView;
.source "SleepQuadrantView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;
    }
.end annotation


# static fields
.field private static final FILL_GRADIENT:Z = false

.field public static final LOGD:Z = false

.field public static final TAG:Ljava/lang/String; = "SleepQuadrantView"


# instance fields
.field private backgroundImageView:Landroid/widget/ImageView;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mOriginAngle:I

.field private mSlices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;",
            ">;"
        }
    .end annotation
.end field

.field private maxValue:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mSlices:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    .line 48
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->maxValue:F

    .line 63
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->setWillNotDraw(Z)V

    .line 64
    return-void
.end method

.method static synthetic access$000(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # Landroid/content/res/Resources;

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->buildStrokePaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # I
    .param p1, "x1"    # Landroid/content/res/Resources;

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->buildFillPaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;

    move-result-object v0

    return-object v0
.end method

.method private static buildFillPaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 2
    .param p0, "color"    # I
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 78
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 80
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 84
    return-object v0
.end method

.method private static buildStrokePaint(ILandroid/content/res/Resources;)Landroid/graphics/Paint;
    .locals 3
    .param p0, "color"    # I
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 67
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 69
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 71
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 72
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 74
    return-object v0
.end method

.method public static darken(I)I
    .locals 4
    .param p0, "color"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 88
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 89
    .local v0, "hsv":[F
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 90
    const/4 v1, 0x2

    aget v2, v0, v1

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 91
    const/4 v1, 0x1

    aget v2, v0, v1

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 92
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    return v1
.end method


# virtual methods
.method public addSlice(FIILandroid/graphics/Paint$Style;)V
    .locals 7
    .param p1, "value"    # F
    .param p2, "fillColor"    # I
    .param p3, "strokeColor"    # I
    .param p4, "style"    # Landroid/graphics/Paint$Style;

    .prologue
    .line 110
    iget-object v6, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mSlices:Ljava/util/ArrayList;

    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;-><init>(Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;FIILandroid/graphics/Paint$Style;)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public generatePath()V
    .locals 17

    .prologue
    .line 134
    move-object/from16 v0, p0

    iget v13, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->maxValue:F

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-nez v13, :cond_0

    .line 135
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->invalidate()V

    .line 180
    :goto_0
    return-void

    .line 139
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;

    .line 140
    .local v6, "slice":Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->path:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->reset()V

    .line 141
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->pathSide:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->reset()V

    .line 142
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->reset()V

    goto :goto_1

    .line 145
    .end local v6    # "slice":Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->getWidth()I

    move-result v12

    .line 146
    .local v12, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->getHeight()I

    move-result v3

    .line 148
    .local v3, "height":I
    new-instance v4, Landroid/graphics/RectF;

    const/4 v13, 0x0

    const/4 v14, 0x0

    int-to-float v15, v12

    int-to-float v0, v3

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-direct {v4, v13, v14, v15, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 149
    .local v4, "rect":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 150
    .local v5, "rectSide":Landroid/graphics/RectF;
    invoke-virtual {v5, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 152
    move-object/from16 v0, p0

    iget v7, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mOriginAngle:I

    .line 153
    .local v7, "startAngle":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;

    .line 154
    .restart local v6    # "slice":Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;
    const/4 v11, 0x0

    .line 155
    .local v11, "tempMax":F
    move-object/from16 v0, p0

    iget v14, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->maxValue:F

    iget v15, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->value:F

    cmpg-float v14, v14, v15

    if-gtz v14, :cond_2

    .line 156
    iget v11, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->value:F

    .line 160
    :goto_3
    iget v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->value:F

    const v15, 0x43b3ffdf    # 359.999f

    mul-float/2addr v14, v15

    div-float v9, v14, v11

    .line 161
    .local v9, "sweepAngle":F
    int-to-float v14, v7

    add-float v1, v14, v9

    .line 164
    .local v1, "endAngle":F
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->path:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v15

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v16

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/Path;->moveTo(FF)V

    .line 165
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->path:Landroid/graphics/Path;

    int-to-float v15, v7

    invoke-virtual {v14, v4, v15, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 166
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->path:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v15

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v16

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    int-to-float v8, v7

    .line 170
    .local v8, "startAngleSide":F
    move v2, v1

    .line 171
    .local v2, "endAngleSide":F
    sub-float v10, v2, v8

    .line 174
    .local v10, "sweepAngleSide":F
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v15

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v16

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/Path;->moveTo(FF)V

    .line 175
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    invoke-virtual {v14, v5, v8, v10}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    .line 176
    iget-object v14, v6, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v15

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v16

    invoke-virtual/range {v14 .. v16}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_2

    .line 158
    .end local v1    # "endAngle":F
    .end local v2    # "endAngleSide":F
    .end local v8    # "startAngleSide":F
    .end local v9    # "sweepAngle":F
    .end local v10    # "sweepAngleSide":F
    :cond_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->maxValue:F

    goto :goto_3

    .line 179
    .end local v6    # "slice":Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;
    .end local v11    # "tempMax":F
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->invalidate()V

    goto/16 :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 185
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 187
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;

    .line 188
    .local v0, "slice":Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;
    iget-object v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->path:Landroid/graphics/Path;

    iget-object v3, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 189
    iget-object v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->pathOutline:Landroid/graphics/Path;

    iget-object v3, v0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 192
    .end local v0    # "slice":Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView$Slice;
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const v3, 0x3f7d70a4    # 0.99f

    .line 119
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v0, v2

    .line 120
    .local v0, "centerX":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v1, v2

    .line 123
    .local v1, "centerY":F
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 124
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 125
    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mMatrix:Landroid/graphics/Matrix;

    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-virtual {v2, v3, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 127
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->generatePath()V

    .line 128
    return-void
.end method

.method public removeAllSlices()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mSlices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 115
    return-void
.end method

.method public setOriginAngle(I)V
    .locals 0
    .param p1, "originAngle"    # I

    .prologue
    .line 106
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->mOriginAngle:I

    .line 107
    return-void
.end method

.method public setValues(FF)V
    .locals 4
    .param p1, "value"    # F
    .param p2, "maxValue"    # F

    .prologue
    .line 96
    iput p2, p0, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->maxValue:F

    .line 97
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->removeAllSlices()V

    .line 98
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 99
    .local v1, "yellowThemeColor":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00af

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 100
    .local v0, "backgroundColor":I
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->addSlice(FIILandroid/graphics/Paint$Style;)V

    .line 101
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->invalidate()V

    .line 102
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SleepQuadrantView;->generatePath()V

    .line 103
    return-void
.end method
