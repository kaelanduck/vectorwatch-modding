.class Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;
.super Ljava/lang/Object;
.source "SpokeView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/view/widgets/SpokeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 204
    :try_start_0
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$000(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 224
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$408(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    .line 226
    :goto_0
    return-void

    .line 207
    :cond_0
    const/16 v4, 0x168

    :try_start_1
    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$100(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    move-result v5

    div-int/2addr v4, v5

    int-to-float v2, v4

    .line 208
    .local v2, "spokeAngle":F
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$200(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    move-result v4

    mul-int/lit16 v4, v4, 0x168

    div-int/lit8 v4, v4, 0x64

    int-to-float v1, v4

    .line 209
    .local v1, "finalAngle":F
    div-float v3, v1, v2

    .line 210
    .local v3, "steps":F
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$300(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v3

    float-to-int v0, v4

    .line 212
    .local v0, "animStepTime":I
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$400(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v4, v0

    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$300(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 213
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$200(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v3

    iget-object v6, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v6}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$400(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBPercent(I)V

    .line 215
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    iget-object v4, v4, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$500(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)Ljava/lang/Runnable;

    move-result-object v5

    int-to-long v6, v0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 216
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->postInvalidate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 224
    :goto_1
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$408(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    goto :goto_0

    .line 218
    :cond_1
    :try_start_2
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$200(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBPercent(I)V

    .line 219
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$002(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Z)Z

    .line 220
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->postInvalidate()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 222
    .end local v0    # "animStepTime":I
    .end local v1    # "finalAngle":F
    .end local v2    # "spokeAngle":F
    .end local v3    # "steps":F
    :catch_0
    move-exception v4

    .line 224
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$408(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    goto :goto_0

    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;->this$0:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->access$408(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I

    throw v4
.end method
