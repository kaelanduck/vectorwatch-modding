.class Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;
.super Ljava/lang/Object;
.source "CircularProgressDrawable.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setupAnimations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field cancelled:Z

.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V
    .locals 1
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;->cancelled:Z

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;->cancelled:Z

    .line 214
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;->cancelled:Z

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    const/4 v1, 0x0

    # setter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mFirstSweepAnimation:Z
    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$002(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;Z)Z

    .line 206
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # invokes: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setDisappearing()V
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$400(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    .line 207
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepDisappearingAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$500(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 209
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 218
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 198
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;->cancelled:Z

    .line 199
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    const/4 v1, 0x1

    # setter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mModeAppearing:Z
    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$302(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;Z)Z

    .line 200
    return-void
.end method
