.class Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$8;
.super Ljava/lang/Object;
.source "CircularProgressDrawable.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->progressiveStop(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 339
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$8;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 354
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 347
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$8;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1400(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 348
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$8;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mOnEndListener:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1500(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$8;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mOnEndListener:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1500(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$8;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;->onEnd(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    .line 349
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 359
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 343
    return-void
.end method
