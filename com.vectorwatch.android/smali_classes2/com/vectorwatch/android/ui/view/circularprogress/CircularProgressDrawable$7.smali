.class Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$7;
.super Ljava/lang/Object;
.source "CircularProgressDrawable.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setupAnimations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private cancelled:Z

.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 279
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$7;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 295
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$7;->cancelled:Z

    .line 296
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 289
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$7;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    const/4 v1, 0x0

    # invokes: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setEndRatio(F)V
    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1300(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;F)V

    .line 290
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$7;->cancelled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$7;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->stop()V

    .line 291
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 301
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 284
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$7;->cancelled:Z

    .line 285
    return-void
.end method
