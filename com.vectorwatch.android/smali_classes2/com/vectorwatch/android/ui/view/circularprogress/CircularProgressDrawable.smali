.class public Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "CircularProgressDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;,
        Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;,
        Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;
    }
.end annotation


# static fields
.field private static final COLOR_EVALUATOR:Landroid/animation/ArgbEvaluator;

.field private static final DEFAULT_ROTATION_INTERPOLATOR:Landroid/view/animation/Interpolator;

.field private static final DEFAULT_SWEEP_INTERPOLATOR:Landroid/view/animation/Interpolator;

.field private static final END_ANIMATOR_DURATION:I = 0xc8

.field public static final END_INTERPOLATOR:Landroid/view/animation/Interpolator;

.field private static final ROTATION_ANIMATOR_DURATION:I = 0x7d0

.field private static final SWEEP_ANIMATOR_DURATION:I = 0x258


# instance fields
.field private final fBounds:Landroid/graphics/RectF;

.field private mAngleInterpolator:Landroid/view/animation/Interpolator;

.field private mBorderWidth:F

.field private mColors:[I

.field private mCurrentColor:I

.field private mCurrentEndRatio:F

.field private mCurrentIndexColor:I

.field private mCurrentRotationAngle:F

.field private mCurrentRotationAngleOffset:F

.field private mCurrentSweepAngle:F

.field private mEndAnimator:Landroid/animation/ValueAnimator;

.field private mFirstSweepAnimation:Z

.field private mMaxSweepAngle:I

.field private mMinSweepAngle:I

.field private mModeAppearing:Z

.field private mOnEndListener:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;

.field private mPaint:Landroid/graphics/Paint;

.field private mRotationAnimator:Landroid/animation/ValueAnimator;

.field private mRotationSpeed:F

.field private mRunning:Z

.field private mSweepAppearingAnimator:Landroid/animation/ValueAnimator;

.field private mSweepDisappearingAnimator:Landroid/animation/ValueAnimator;

.field private mSweepInterpolator:Landroid/view/animation/Interpolator;

.field private mSweepSpeed:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->COLOR_EVALUATOR:Landroid/animation/ArgbEvaluator;

    .line 39
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->END_INTERPOLATOR:Landroid/view/animation/Interpolator;

    .line 40
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->DEFAULT_ROTATION_INTERPOLATOR:Landroid/view/animation/Interpolator;

    .line 41
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->DEFAULT_SWEEP_INTERPOLATOR:Landroid/view/animation/Interpolator;

    return-void
.end method

.method private constructor <init>([IFFFIILcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V
    .locals 3
    .param p1, "colors"    # [I
    .param p2, "borderWidth"    # F
    .param p3, "sweepSpeed"    # F
    .param p4, "rotationSpeed"    # F
    .param p5, "minSweepAngle"    # I
    .param p6, "maxSweepAngle"    # I
    .param p7, "style"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;
    .param p8, "angleInterpolator"    # Landroid/view/animation/Interpolator;
    .param p9, "sweepInterpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 46
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->fBounds:Landroid/graphics/RectF;

    .line 59
    iput v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentRotationAngleOffset:F

    .line 60
    iput v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentRotationAngle:F

    .line 61
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentEndRatio:F

    .line 83
    iput-object p9, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepInterpolator:Landroid/view/animation/Interpolator;

    .line 84
    iput-object p8, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mAngleInterpolator:Landroid/view/animation/Interpolator;

    .line 85
    iput p2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mBorderWidth:F

    .line 86
    iput v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentIndexColor:I

    .line 87
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mColors:[I

    .line 88
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mColors:[I

    aget v0, v0, v2

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentColor:I

    .line 89
    iput p3, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepSpeed:F

    .line 90
    iput p4, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationSpeed:F

    .line 91
    iput p5, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMinSweepAngle:I

    .line 92
    iput p6, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMaxSweepAngle:I

    .line 94
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    .line 95
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 96
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 97
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 98
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;->ROUNDED:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;

    if-ne p7, v0, :cond_0

    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 99
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mColors:[I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 101
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setupAnimations()V

    .line 102
    return-void

    .line 98
    :cond_0
    sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    goto :goto_0
.end method

.method synthetic constructor <init>([IFFFIILcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$1;)V
    .locals 0
    .param p1, "x0"    # [I
    .param p2, "x1"    # F
    .param p3, "x2"    # F
    .param p4, "x3"    # F
    .param p5, "x4"    # I
    .param p6, "x5"    # I
    .param p7, "x6"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;
    .param p8, "x7"    # Landroid/view/animation/Interpolator;
    .param p9, "x8"    # Landroid/view/animation/Interpolator;
    .param p10, "x9"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$1;

    .prologue
    .line 29
    invoke-direct/range {p0 .. p9}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;-><init>([IFFFIILcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;)V

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mFirstSweepAnimation:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mFirstSweepAnimation:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMaxSweepAngle:I

    return v0
.end method

.method static synthetic access$1000(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setAppearing()V

    return-void
.end method

.method static synthetic access$1200(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepAppearingAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;F)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;
    .param p1, "x1"    # F

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setEndRatio(F)V

    return-void
.end method

.method static synthetic access$1400(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mOnEndListener:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;

    return-object v0
.end method

.method static synthetic access$1600()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->DEFAULT_SWEEP_INTERPOLATOR:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$1700()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->DEFAULT_ROTATION_INTERPOLATOR:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMinSweepAngle:I

    return v0
.end method

.method static synthetic access$302(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mModeAppearing:Z

    return p1
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setDisappearing()V

    return-void
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepDisappearingAnimator:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)[I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mColors:[I

    return-object v0
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentColor:I

    return v0
.end method

.method static synthetic access$702(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentColor:I

    return p1
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 29
    iget v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentIndexColor:I

    return v0
.end method

.method static synthetic access$802(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentIndexColor:I

    return p1
.end method

.method static synthetic access$900()Landroid/animation/ArgbEvaluator;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->COLOR_EVALUATOR:Landroid/animation/ArgbEvaluator;

    return-object v0
.end method

.method private reinitValues()V
    .locals 2

    .prologue
    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mFirstSweepAnimation:Z

    .line 106
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentEndRatio:F

    .line 107
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 108
    return-void
.end method

.method private setAppearing()V
    .locals 2

    .prologue
    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mModeAppearing:Z

    .line 152
    iget v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentRotationAngleOffset:F

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMinSweepAngle:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentRotationAngleOffset:F

    .line 153
    return-void
.end method

.method private setDisappearing()V
    .locals 2

    .prologue
    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mModeAppearing:Z

    .line 157
    iget v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentRotationAngleOffset:F

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMaxSweepAngle:I

    rsub-int v1, v1, 0x168

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentRotationAngleOffset:F

    .line 158
    return-void
.end method

.method private setEndRatio(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 384
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentEndRatio:F

    .line 385
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->invalidateSelf()V

    .line 386
    return-void
.end method

.method private setupAnimations()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x44160000    # 600.0f

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 164
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationAnimator:Landroid/animation/ValueAnimator;

    .line 165
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mAngleInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 166
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationAnimator:Landroid/animation/ValueAnimator;

    const/high16 v1, 0x44fa0000    # 2000.0f

    iget v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationSpeed:F

    div-float/2addr v1, v2

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 167
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$1;-><init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 174
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 175
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 177
    new-array v0, v4, [F

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMinSweepAngle:I

    int-to-float v1, v1

    aput v1, v0, v7

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMaxSweepAngle:I

    int-to-float v1, v1

    aput v1, v0, v5

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepAppearingAnimator:Landroid/animation/ValueAnimator;

    .line 178
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepAppearingAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 179
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepAppearingAnimator:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepSpeed:F

    div-float v1, v6, v1

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 180
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepAppearingAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$2;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$2;-><init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 193
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepAppearingAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$3;-><init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 221
    new-array v0, v4, [F

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMaxSweepAngle:I

    int-to-float v1, v1

    aput v1, v0, v7

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mMinSweepAngle:I

    int-to-float v1, v1

    aput v1, v0, v5

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepDisappearingAnimator:Landroid/animation/ValueAnimator;

    .line 222
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepDisappearingAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 223
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepDisappearingAnimator:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepSpeed:F

    div-float v1, v6, v1

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 224
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepDisappearingAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$4;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$4;-><init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 241
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepDisappearingAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;-><init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 269
    new-array v0, v4, [F

    fill-array-data v0, :array_1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    .line 270
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    sget-object v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->END_INTERPOLATOR:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 271
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 272
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$6;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$6;-><init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 279
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$7;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$7;-><init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 303
    return-void

    .line 164
    nop

    :array_0
    .array-data 4
        0x0
        0x43b40000    # 360.0f
    .end array-data

    .line 269
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private stopAnimators()V
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 329
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepAppearingAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 330
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepDisappearingAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 331
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 332
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x43b40000    # 360.0f

    .line 112
    iget v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentRotationAngle:F

    iget v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentRotationAngleOffset:F

    sub-float v2, v0, v1

    .line 113
    .local v2, "startAngle":F
    iget v3, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentSweepAngle:F

    .line 114
    .local v3, "sweepAngle":F
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mModeAppearing:Z

    if-nez v0, :cond_0

    .line 115
    sub-float v0, v4, v3

    add-float/2addr v2, v0

    .line 117
    :cond_0
    rem-float/2addr v2, v4

    .line 118
    iget v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentEndRatio:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 119
    iget v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentEndRatio:F

    mul-float v6, v3, v0

    .line 120
    .local v6, "newSweepAngle":F
    sub-float v0, v3, v6

    add-float/2addr v0, v2

    rem-float v2, v0, v4

    .line 121
    move v3, v6

    .line 123
    .end local v6    # "newSweepAngle":F
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->fBounds:Landroid/graphics/RectF;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 124
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 138
    const/4 v0, -0x3

    return v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 370
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRunning:Z

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 5
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x3f000000    # 0.5f

    .line 143
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 144
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->fBounds:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mBorderWidth:F

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    add-float/2addr v1, v3

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->fBounds:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mBorderWidth:F

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    sub-float/2addr v1, v3

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 146
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->fBounds:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iget v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mBorderWidth:F

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    add-float/2addr v1, v3

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 147
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->fBounds:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iget v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mBorderWidth:F

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    sub-float/2addr v1, v3

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 148
    return-void
.end method

.method public progressiveStop()V
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->progressiveStop(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;)V

    .line 366
    return-void
.end method

.method public progressiveStop(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mOnEndListener:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;

    .line 339
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$8;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$8;-><init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 361
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mEndAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 129
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 134
    return-void
.end method

.method public setCurrentRotationAngle(F)V
    .locals 0
    .param p1, "currentRotationAngle"    # F

    .prologue
    .line 374
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentRotationAngle:F

    .line 375
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->invalidateSelf()V

    .line 376
    return-void
.end method

.method public setCurrentSweepAngle(F)V
    .locals 0
    .param p1, "currentSweepAngle"    # F

    .prologue
    .line 379
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentSweepAngle:F

    .line 380
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->invalidateSelf()V

    .line 381
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    :goto_0
    return-void

    .line 310
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRunning:Z

    .line 311
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->reinitValues()V

    .line 312
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRotationAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 313
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepAppearingAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 314
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->invalidateSelf()V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 325
    :goto_0
    return-void

    .line 322
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mRunning:Z

    .line 323
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->stopAnimators()V

    .line 324
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->invalidateSelf()V

    goto :goto_0
.end method
