.class public Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
.super Ljava/lang/Object;
.source "CircularProgressDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mAngleInterpolator:Landroid/view/animation/Interpolator;

.field private mColors:[I

.field private mMaxSweepAngle:I

.field private mMinSweepAngle:I

.field private mRotationSpeed:F

.field private mStrokeWidth:F

.field private mStyle:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;

.field private mSweepInterpolator:Landroid/view/animation/Interpolator;

.field private mSweepSpeed:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 400
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;-><init>(Landroid/content/Context;Z)V

    .line 401
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "editMode"    # Z

    .prologue
    .line 403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->DEFAULT_SWEEP_INTERPOLATOR:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1600()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mSweepInterpolator:Landroid/view/animation/Interpolator;

    .line 397
    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->DEFAULT_ROTATION_INTERPOLATOR:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1700()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mAngleInterpolator:Landroid/view/animation/Interpolator;

    .line 404
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->initValues(Landroid/content/Context;Z)V

    .line 405
    return-void
.end method

.method private initValues(Landroid/content/Context;Z)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "editMode"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 408
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mStrokeWidth:F

    .line 409
    iput v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mSweepSpeed:F

    .line 410
    iput v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mRotationSpeed:F

    .line 411
    if-eqz p2, :cond_0

    .line 412
    new-array v0, v4, [I

    const v1, -0xffff01

    aput v1, v0, v3

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mColors:[I

    .line 413
    const/16 v0, 0x14

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mMinSweepAngle:I

    .line 414
    const/16 v0, 0x12c

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mMaxSweepAngle:I

    .line 420
    :goto_0
    sget-object v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;->ROUNDED:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mStyle:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;

    .line 421
    return-void

    .line 416
    :cond_0
    new-array v0, v4, [I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v3

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mColors:[I

    .line 417
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mMinSweepAngle:I

    .line 418
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mMaxSweepAngle:I

    goto :goto_0
.end method


# virtual methods
.method public angleInterpolator(Landroid/view/animation/Interpolator;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 1
    .param p1, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 477
    const-string v0, "Angle interpolator"

    invoke-static {p1, v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 478
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mAngleInterpolator:Landroid/view/animation/Interpolator;

    .line 479
    return-object p0
.end method

.method public build()Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;
    .locals 11

    .prologue
    .line 483
    new-instance v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mColors:[I

    iget v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mStrokeWidth:F

    iget v3, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mSweepSpeed:F

    iget v4, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mRotationSpeed:F

    iget v5, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mMinSweepAngle:I

    iget v6, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mMaxSweepAngle:I

    iget-object v7, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mStyle:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;

    iget-object v8, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mAngleInterpolator:Landroid/view/animation/Interpolator;

    iget-object v9, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mSweepInterpolator:Landroid/view/animation/Interpolator;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;-><init>([IFFFIILcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$1;)V

    return-object v0
.end method

.method public color(I)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 424
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mColors:[I

    .line 425
    return-object p0
.end method

.method public colors([I)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 0
    .param p1, "colors"    # [I

    .prologue
    .line 429
    invoke-static {p1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->checkColors([I)V

    .line 430
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mColors:[I

    .line 431
    return-object p0
.end method

.method public maxSweepAngle(I)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 0
    .param p1, "maxSweepAngle"    # I

    .prologue
    .line 453
    invoke-static {p1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->checkAngle(I)V

    .line 454
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mMaxSweepAngle:I

    .line 455
    return-object p0
.end method

.method public minSweepAngle(I)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 0
    .param p1, "minSweepAngle"    # I

    .prologue
    .line 447
    invoke-static {p1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->checkAngle(I)V

    .line 448
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mMinSweepAngle:I

    .line 449
    return-object p0
.end method

.method public rotationSpeed(F)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 0
    .param p1, "rotationSpeed"    # F

    .prologue
    .line 441
    invoke-static {p1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->checkSpeed(F)V

    .line 442
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mRotationSpeed:F

    .line 443
    return-object p0
.end method

.method public strokeWidth(F)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 1
    .param p1, "strokeWidth"    # F

    .prologue
    .line 459
    const-string v0, "StrokeWidth"

    invoke-static {p1, v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->checkPositiveOrZero(FLjava/lang/String;)V

    .line 460
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mStrokeWidth:F

    .line 461
    return-object p0
.end method

.method public style(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 1
    .param p1, "style"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;

    .prologue
    .line 465
    const-string v0, "Style"

    invoke-static {p1, v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 466
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mStyle:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Style;

    .line 467
    return-object p0
.end method

.method public sweepInterpolator(Landroid/view/animation/Interpolator;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 1
    .param p1, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 471
    const-string v0, "Sweep interpolator"

    invoke-static {p1, v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 472
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mSweepInterpolator:Landroid/view/animation/Interpolator;

    .line 473
    return-object p0
.end method

.method public sweepSpeed(F)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    .locals 0
    .param p1, "sweepSpeed"    # F

    .prologue
    .line 435
    invoke-static {p1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->checkSpeed(F)V

    .line 436
    iput p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->mSweepSpeed:F

    .line 437
    return-object p0
.end method
