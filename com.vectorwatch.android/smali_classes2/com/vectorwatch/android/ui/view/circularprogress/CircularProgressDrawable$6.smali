.class Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$6;
.super Ljava/lang/Object;
.source "CircularProgressDrawable.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setupAnimations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$6;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$6;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBarUtils;->getAnimatedFraction(Landroid/animation/ValueAnimator;)F

    move-result v2

    sub-float/2addr v1, v2

    # invokes: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setEndRatio(F)V
    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1300(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;F)V

    .line 277
    return-void
.end method
