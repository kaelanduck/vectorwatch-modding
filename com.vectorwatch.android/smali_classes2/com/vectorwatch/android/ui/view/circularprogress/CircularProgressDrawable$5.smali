.class Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;
.super Ljava/lang/Object;
.source "CircularProgressDrawable.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setupAnimations()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field cancelled:Z

.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 262
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->cancelled:Z

    .line 263
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->cancelled:Z

    if-nez v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # invokes: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->setAppearing()V
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1100(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)V

    .line 253
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentIndexColor:I
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$800(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mColors:[I
    invoke-static {v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$600(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)[I

    move-result-object v2

    array-length v2, v2

    rem-int/2addr v1, v2

    # setter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentIndexColor:I
    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$802(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;I)I

    .line 254
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mColors:[I
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$600(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentIndexColor:I
    invoke-static {v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$800(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)I

    move-result v2

    aget v1, v1, v2

    # setter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentColor:I
    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$702(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;I)I

    .line 255
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1000(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Landroid/graphics/Paint;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mCurrentColor:I
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$700(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 256
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->this$0:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    # getter for: Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->mSweepAppearingAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->access$1200(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 258
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 267
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$5;->cancelled:Z

    .line 247
    return-void
.end method
