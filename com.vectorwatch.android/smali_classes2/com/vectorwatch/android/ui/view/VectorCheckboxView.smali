.class public Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
.super Landroid/widget/ImageView;
.source "VectorCheckboxView.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mChecked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 23
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setChecked(Z)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setChecked(Z)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setChecked(Z)V

    .line 29
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->mChecked:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->mChecked:Z

    .line 42
    if-eqz p1, :cond_0

    .line 43
    const v0, 0x7f0200e5

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setImageResource(I)V

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    const v0, 0x7f0200fc

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setImageResource(I)V

    goto :goto_0
.end method
