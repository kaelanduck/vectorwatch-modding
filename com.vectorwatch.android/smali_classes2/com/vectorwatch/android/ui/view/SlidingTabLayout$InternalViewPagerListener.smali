.class Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;
.super Ljava/lang/Object;
.source "SlidingTabLayout.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/view/SlidingTabLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalViewPagerListener"
.end annotation


# instance fields
.field private mScrollState:I

.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;


# direct methods
.method private constructor <init>(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;Lcom/vectorwatch/android/ui/view/SlidingTabLayout$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vectorwatch/android/ui/view/SlidingTabLayout;
    .param p2, "x1"    # Lcom/vectorwatch/android/ui/view/SlidingTabLayout$1;

    .prologue
    .line 241
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;-><init>(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 267
    iput p1, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->mScrollState:I

    .line 269
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mViewPagerPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$400(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mViewPagerPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$400(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 272
    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 246
    iget-object v3, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mTabStrip:Lcom/vectorwatch/android/ui/view/SlidingTabStrip;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$200(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Lcom/vectorwatch/android/ui/view/SlidingTabStrip;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/view/SlidingTabStrip;->getChildCount()I

    move-result v2

    .line 247
    .local v2, "tabStripChildCount":I
    if-eqz v2, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v2, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v3, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mTabStrip:Lcom/vectorwatch/android/ui/view/SlidingTabStrip;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$200(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Lcom/vectorwatch/android/ui/view/SlidingTabStrip;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Lcom/vectorwatch/android/ui/view/SlidingTabStrip;->onViewPagerPageChanged(IF)V

    .line 253
    iget-object v3, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mTabStrip:Lcom/vectorwatch/android/ui/view/SlidingTabStrip;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$200(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Lcom/vectorwatch/android/ui/view/SlidingTabStrip;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/vectorwatch/android/ui/view/SlidingTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 254
    .local v1, "selectedTitle":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 255
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p2

    float-to-int v0, v3

    .line 257
    .local v0, "extraOffset":I
    :goto_1
    iget-object v3, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # invokes: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->scrollToTab(II)V
    invoke-static {v3, p1, v0}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$300(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;II)V

    .line 259
    iget-object v3, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mViewPagerPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$400(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 260
    iget-object v3, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mViewPagerPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$400(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v3

    invoke-interface {v3, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    goto :goto_0

    .line 255
    .end local v0    # "extraOffset":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onPageSelected(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 276
    iget v0, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->mScrollState:I

    if-nez v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mTabStrip:Lcom/vectorwatch/android/ui/view/SlidingTabStrip;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$200(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Lcom/vectorwatch/android/ui/view/SlidingTabStrip;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/vectorwatch/android/ui/view/SlidingTabStrip;->onViewPagerPageChanged(IF)V

    .line 278
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    const/4 v1, 0x0

    # invokes: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->scrollToTab(II)V
    invoke-static {v0, p1, v1}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$300(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;II)V

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mViewPagerPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$400(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SlidingTabLayout$InternalViewPagerListener;->this$0:Lcom/vectorwatch/android/ui/view/SlidingTabLayout;

    # getter for: Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->mViewPagerPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/SlidingTabLayout;->access$400(Lcom/vectorwatch/android/ui/view/SlidingTabLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 284
    :cond_1
    return-void
.end method
