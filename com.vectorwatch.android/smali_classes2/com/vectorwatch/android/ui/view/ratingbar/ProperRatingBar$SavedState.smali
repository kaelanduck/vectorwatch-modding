.class Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "ProperRatingBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field rating:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 237
    new-instance v0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 227
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;->rating:I

    .line 229
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$1;

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 223
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 224
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 233
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 234
    iget v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;->rating:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 235
    return-void
.end method
