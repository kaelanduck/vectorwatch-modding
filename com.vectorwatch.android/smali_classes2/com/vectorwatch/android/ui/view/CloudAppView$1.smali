.class Lcom/vectorwatch/android/ui/view/CloudAppView$1;
.super Ljava/lang/Object;
.source "CloudAppView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/view/CloudAppView;-><init>(Landroid/app/Activity;ILcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

.field final synthetic val$context:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/view/CloudAppView;Landroid/app/Activity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/CloudAppView;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->val$context:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v4, 0x5dc

    .line 101
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 102
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->log:Lorg/slf4j/Logger;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$000(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "STORE: Click on store button."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$100(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    .line 107
    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$200(Lcom/vectorwatch/android/ui/view/CloudAppView;)Landroid/content/Context;

    move-result-object v2

    .line 106
    invoke-static {v1, v4, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->val$context:Landroid/app/Activity;

    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppsWithGivenState(Landroid/content/Context;Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)Ljava/util/ArrayList;

    move-result-object v0

    .line 113
    .local v0, "mRunningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mIsStream:Z
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$300(Lcom/vectorwatch/android/ui/view/CloudAppView;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mIsInstalled:Z
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$400(Lcom/vectorwatch/android/ui/view/CloudAppView;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_2

    .line 115
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->val$context:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090118

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->val$context:Landroid/app/Activity;

    invoke-static {v1, v4, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 118
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$500(Lcom/vectorwatch/android/ui/view/CloudAppView;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$600(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 120
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mCallbacks:Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$700(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mCallbacks:Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$700(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mCloudElementSummary:Lcom/vectorwatch/android/models/StoreElement;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$800(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/models/StoreElement;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$600(Lcom/vectorwatch/android/ui/view/CloudAppView;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mActionButton:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$500(Lcom/vectorwatch/android/ui/view/CloudAppView;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;->onSaveAction(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 126
    .end local v0    # "mRunningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/CloudAppView$1;->this$0:Lcom/vectorwatch/android/ui/view/CloudAppView;

    .line 127
    # getter for: Lcom/vectorwatch/android/ui/view/CloudAppView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;->access$200(Lcom/vectorwatch/android/ui/view/CloudAppView;)Landroid/content/Context;

    move-result-object v2

    .line 126
    invoke-static {v1, v4, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto/16 :goto_0
.end method
