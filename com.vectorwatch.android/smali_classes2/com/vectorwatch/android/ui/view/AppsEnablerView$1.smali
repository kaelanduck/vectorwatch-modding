.class Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;
.super Ljava/lang/Object;
.source "AppsEnablerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/view/AppsEnablerView;-><init>(Landroid/widget/RelativeLayout;Lcom/vectorwatch/android/ui/view/VectorCheckboxView;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/view/AppsEnablerView;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    # getter for: Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->access$000(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    # getter for: Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->access$000(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setChecked(Z)V

    .line 46
    :goto_0
    # getter for: Lcom/vectorwatch/android/ui/view/AppsEnablerView;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->access$100()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Preferences - App enabler - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    # getter for: Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->access$000(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    # getter for: Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->access$000(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    # getter for: Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->access$200(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAppNotification(ZLandroid/content/Context;)V

    .line 49
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/SupportedAppsListVisibilityChangeEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    # getter for: Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->access$000(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->isChecked()Z

    move-result v2

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/SupportedAppsListVisibilityChangeEvent;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 50
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    # getter for: Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->access$000(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setChecked(Z)V

    goto :goto_0
.end method
