.class public Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;
.super Landroid/view/View;
.source "LinePageIndicator.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/view/viewpagerindicator/PageIndicator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator$SavedState;
    }
.end annotation


# static fields
.field private static final INVALID_POINTER:I = -0x1


# instance fields
.field private mActivePointerId:I

.field private mCentered:Z

.field private mCurrentPage:I

.field private mGapWidth:F

.field private mIsDragging:Z

.field private mLastMotionX:F

.field private mLineWidth:F

.field private mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private final mPaintSelected:Landroid/graphics/Paint;

.field private final mPaintUnselected:Landroid/graphics/Paint;

.field private mTouchSlop:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 65
    const v0, 0x7f0101ce

    invoke-direct {p0, p1, p2, v0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v10, Landroid/graphics/Paint;

    const/4 v11, 0x1

    invoke-direct {v10, v11}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    .line 46
    new-instance v10, Landroid/graphics/Paint;

    const/4 v11, 0x1

    invoke-direct {v10, v11}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    .line 55
    const/high16 v10, -0x40800000    # -1.0f

    iput v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    .line 56
    const/4 v10, -0x1

    iput v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    .line 70
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->isInEditMode()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 101
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 75
    .local v9, "res":Landroid/content/res/Resources;
    const v10, 0x7f0f003d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 76
    .local v6, "defaultSelectedColor":I
    const v10, 0x7f0f003e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 77
    .local v8, "defaultUnselectedColor":I
    const v10, 0x7f0b006f

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 78
    .local v5, "defaultLineWidth":F
    const v10, 0x7f0b006e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 79
    .local v4, "defaultGapWidth":F
    const v10, 0x7f0b0070

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    .line 80
    .local v7, "defaultStrokeWidth":F
    const v10, 0x7f0d0009

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 83
    .local v3, "defaultCentered":Z
    sget-object v10, Lcom/vectorwatch/android/R$styleable;->LinePageIndicator:[I

    const/4 v11, 0x0

    invoke-virtual {p1, p2, v10, p3, v11}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 85
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v10, 0x1

    invoke-virtual {v0, v10, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v10

    iput-boolean v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCentered:Z

    .line 86
    const/4 v10, 0x5

    invoke-virtual {v0, v10, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v10

    iput v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    .line 87
    const/4 v10, 0x6

    invoke-virtual {v0, v10, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v10

    iput v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    .line 88
    const/4 v10, 0x3

    invoke-virtual {v0, v10, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v10

    invoke-virtual {p0, v10}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->setStrokeWidth(F)V

    .line 89
    iget-object v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    const/4 v11, 0x4

    invoke-virtual {v0, v11, v8}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 90
    iget-object v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    const/4 v11, 0x2

    invoke-virtual {v0, v11, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 92
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 93
    .local v1, "background":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_1

    .line 94
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 97
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 99
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    .line 100
    .local v2, "configuration":Landroid/view/ViewConfiguration;
    invoke-static {v2}, Landroid/support/v4/view/ViewConfigurationCompat;->getScaledPagingTouchSlop(Landroid/view/ViewConfiguration;)I

    move-result v10

    iput v10, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mTouchSlop:I

    goto :goto_0
.end method

.method private measureHeight(I)I
    .locals 6
    .param p1, "measureSpec"    # I

    .prologue
    .line 387
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 388
    .local v1, "specMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 390
    .local v2, "specSize":I
    const/high16 v3, 0x40000000    # 2.0f

    if-ne v1, v3, :cond_1

    .line 392
    int-to-float v0, v2

    .line 401
    .local v0, "result":F
    :cond_0
    :goto_0
    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    return v3

    .line 395
    .end local v0    # "result":F
    :cond_1
    iget-object v3, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getPaddingTop()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getPaddingBottom()I

    move-result v4

    int-to-float v4, v4

    add-float v0, v3, v4

    .line 397
    .restart local v0    # "result":F
    const/high16 v3, -0x80000000

    if-ne v1, v3, :cond_0

    .line 398
    int-to-float v3, v2

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method private measureWidth(I)I
    .locals 7
    .param p1, "measureSpec"    # I

    .prologue
    .line 360
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 361
    .local v2, "specMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 363
    .local v3, "specSize":I
    const/high16 v4, 0x40000000    # 2.0f

    if-eq v2, v4, :cond_0

    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v4, :cond_2

    .line 365
    :cond_0
    int-to-float v1, v3

    .line 375
    .local v1, "result":F
    :cond_1
    :goto_0
    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    return v4

    .line 368
    .end local v1    # "result":F
    :cond_2
    iget-object v4, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    .line 369
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    int-to-float v5, v0

    iget v6, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    add-int/lit8 v5, v0, -0x1

    int-to-float v5, v5

    iget v6, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    mul-float/2addr v5, v6

    add-float v1, v4, v5

    .line 371
    .restart local v1    # "result":F
    const/high16 v4, -0x80000000

    if-ne v2, v4, :cond_1

    .line 372
    int-to-float v4, v3

    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public getGapWidth()F
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    return v0
.end method

.method public getLineWidth()F
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    return v0
.end method

.method public getSelectedColor()I
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method public getUnselectedColor()I
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public isCentered()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCentered:Z

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 315
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 161
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 163
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v6

    .line 167
    .local v6, "count":I
    if-eqz v6, :cond_0

    .line 171
    iget v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    if-lt v0, v6, :cond_2

    .line 172
    add-int/lit8 v0, v6, -0x1

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->setCurrentItem(I)V

    goto :goto_0

    .line 176
    :cond_2
    iget v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    iget v4, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    add-float v10, v0, v4

    .line 177
    .local v10, "lineWidthAndGap":F
    int-to-float v0, v6

    mul-float/2addr v0, v10

    iget v4, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    sub-float v9, v0, v4

    .line 178
    .local v9, "indicatorWidth":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getPaddingTop()I

    move-result v0

    int-to-float v13, v0

    .line 179
    .local v13, "paddingTop":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getPaddingLeft()I

    move-result v0

    int-to-float v11, v0

    .line 180
    .local v11, "paddingLeft":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getPaddingRight()I

    move-result v0

    int-to-float v12, v0

    .line 182
    .local v12, "paddingRight":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v13

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getPaddingBottom()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v0, v4

    div-float/2addr v0, v5

    add-float v2, v13, v0

    .line 183
    .local v2, "verticalOffset":F
    move v7, v11

    .line 184
    .local v7, "horizontalOffset":F
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCentered:Z

    if-eqz v0, :cond_3

    .line 185
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v11

    sub-float/2addr v0, v12

    div-float/2addr v0, v5

    div-float v4, v9, v5

    sub-float/2addr v0, v4

    add-float/2addr v7, v0

    .line 189
    :cond_3
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v6, :cond_0

    .line 190
    int-to-float v0, v8

    mul-float/2addr v0, v10

    add-float v1, v7, v0

    .line 191
    .local v1, "dx1":F
    iget v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    add-float v3, v1, v0

    .line 192
    .local v3, "dx2":F
    iget v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    if-ne v8, v0, :cond_4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    :goto_2
    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 189
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 192
    :cond_4
    iget-object v5, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 348
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->measureWidth(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->measureHeight(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->setMeasuredDimension(II)V

    .line 349
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 319
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 322
    :cond_0
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 326
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 329
    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 333
    iput p1, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    .line 334
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 336
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 339
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 406
    move-object v0, p1

    check-cast v0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator$SavedState;

    .line 407
    .local v0, "savedState":Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator$SavedState;
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 408
    iget v1, v0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator$SavedState;->currentPage:I

    iput v1, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    .line 409
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->requestLayout()V

    .line 410
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 414
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 415
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator$SavedState;

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 416
    .local v0, "savedState":Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator$SavedState;
    iget v2, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    iput v2, v0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator$SavedState;->currentPage:I

    .line 417
    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 197
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 198
    const/4 v12, 0x1

    .line 276
    :goto_0
    return v12

    .line 200
    :cond_0
    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v12, :cond_1

    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v12}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v12

    invoke-virtual {v12}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v12

    if-nez v12, :cond_2

    .line 201
    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    .line 204
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v12

    and-int/lit16 v0, v12, 0xff

    .line 205
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 276
    :cond_3
    :goto_1
    :pswitch_0
    const/4 v12, 0x1

    goto :goto_0

    .line 207
    :pswitch_1
    const/4 v12, 0x0

    invoke-static {p1, v12}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v12

    iput v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    .line 208
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    iput v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    goto :goto_1

    .line 212
    :pswitch_2
    iget v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    invoke-static {p1, v12}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 213
    .local v1, "activePointerIndex":I
    invoke-static {p1, v1}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v11

    .line 214
    .local v11, "x":F
    iget v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    sub-float v3, v11, v12

    .line 216
    .local v3, "deltaX":F
    iget-boolean v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    if-nez v12, :cond_4

    .line 217
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v12

    iget v13, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mTouchSlop:I

    int-to-float v13, v13

    cmpl-float v12, v12, v13

    if-lez v12, :cond_4

    .line 218
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    .line 222
    :cond_4
    iget-boolean v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    if-eqz v12, :cond_3

    .line 223
    iput v11, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    .line 224
    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v12}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v12

    if-nez v12, :cond_5

    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v12}, Landroid/support/v4/view/ViewPager;->beginFakeDrag()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 225
    :cond_5
    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v12, v3}, Landroid/support/v4/view/ViewPager;->fakeDragBy(F)V

    goto :goto_1

    .line 234
    .end local v1    # "activePointerIndex":I
    .end local v3    # "deltaX":F
    .end local v11    # "x":F
    :pswitch_3
    iget-boolean v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    if-nez v12, :cond_9

    .line 235
    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v12}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v12

    invoke-virtual {v12}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v2

    .line 236
    .local v2, "count":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->getWidth()I

    move-result v10

    .line 237
    .local v10, "width":I
    int-to-float v12, v10

    const/high16 v13, 0x40000000    # 2.0f

    div-float v4, v12, v13

    .line 238
    .local v4, "halfWidth":F
    int-to-float v12, v10

    const/high16 v13, 0x40c00000    # 6.0f

    div-float v9, v12, v13

    .line 240
    .local v9, "sixthWidth":F
    iget v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    if-lez v12, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    sub-float v13, v4, v9

    cmpg-float v12, v12, v13

    if-gez v12, :cond_7

    .line 241
    const/4 v12, 0x3

    if-eq v0, v12, :cond_6

    .line 242
    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v13, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    add-int/lit8 v13, v13, -0x1

    invoke-virtual {v12, v13}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 244
    :cond_6
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 245
    :cond_7
    iget v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    add-int/lit8 v13, v2, -0x1

    if-ge v12, v13, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    add-float v13, v4, v9

    cmpl-float v12, v12, v13

    if-lez v12, :cond_9

    .line 246
    const/4 v12, 0x3

    if-eq v0, v12, :cond_8

    .line 247
    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v13, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v12, v13}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 249
    :cond_8
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 253
    .end local v2    # "count":I
    .end local v4    # "halfWidth":F
    .end local v9    # "sixthWidth":F
    .end local v10    # "width":I
    :cond_9
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    .line 254
    const/4 v12, -0x1

    iput v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    .line 255
    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v12}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v12

    if-eqz v12, :cond_3

    iget-object v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v12}, Landroid/support/v4/view/ViewPager;->endFakeDrag()V

    goto/16 :goto_1

    .line 259
    :pswitch_4
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v5

    .line 260
    .local v5, "index":I
    invoke-static {p1, v5}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v12

    iput v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    .line 261
    invoke-static {p1, v5}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v12

    iput v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    goto/16 :goto_1

    .line 266
    .end local v5    # "index":I
    :pswitch_5
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v8

    .line 267
    .local v8, "pointerIndex":I
    invoke-static {p1, v8}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v7

    .line 268
    .local v7, "pointerId":I
    iget v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    if-ne v7, v12, :cond_a

    .line 269
    if-nez v8, :cond_b

    const/4 v6, 0x1

    .line 270
    .local v6, "newPointerIndex":I
    :goto_2
    invoke-static {p1, v6}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v12

    iput v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    .line 272
    .end local v6    # "newPointerIndex":I
    :cond_a
    iget v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    invoke-static {p1, v12}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v12

    invoke-static {p1, v12}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v12

    iput v12, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    goto/16 :goto_1

    .line 269
    :cond_b
    const/4 v6, 0x0

    goto :goto_2

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public setCentered(Z)V
    .locals 0
    .param p1, "centered"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCentered:Z

    .line 106
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 107
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 2
    .param p1, "item"    # I

    .prologue
    .line 304
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 305
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager has not been bound."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 308
    iput p1, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    .line 309
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 310
    return-void
.end method

.method public setGapWidth(F)V
    .locals 0
    .param p1, "gapWidth"    # F

    .prologue
    .line 151
    iput p1, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    .line 152
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 153
    return-void
.end method

.method public setLineWidth(F)V
    .locals 0
    .param p1, "lineWidth"    # F

    .prologue
    .line 132
    iput p1, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    .line 133
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 134
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    .line 343
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 344
    return-void
.end method

.method public setSelectedColor(I)V
    .locals 1
    .param p1, "selectedColor"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 124
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 125
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 1
    .param p1, "lineHeight"    # F

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 142
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 143
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 144
    return-void
.end method

.method public setUnselectedColor(I)V
    .locals 1
    .param p1, "unselectedColor"    # I

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 115
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 116
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 2
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 281
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-ne v0, p1, :cond_0

    .line 294
    :goto_0
    return-void

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 288
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-nez v0, :cond_2

    .line 289
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager does not have adapter instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_2
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 292
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 293
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->invalidate()V

    goto :goto_0
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;I)V
    .locals 0
    .param p1, "view"    # Landroid/support/v4/view/ViewPager;
    .param p2, "initialPosition"    # I

    .prologue
    .line 298
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 299
    invoke-virtual {p0, p2}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/LinePageIndicator;->setCurrentItem(I)V

    .line 300
    return-void
.end method
