.class public Lcom/vectorwatch/android/ui/view/SupportedAppView;
.super Landroid/widget/RelativeLayout;
.source "SupportedAppView.java"


# instance fields
.field private adapter:Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;

.field private log:Lorg/slf4j/Logger;

.field private mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

.field private mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

.field private mContext:Landroid/content/Context;

.field private mIcon:Landroid/widget/ImageView;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mSummary:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adapter"    # Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 21
    const-class v1, Lcom/vectorwatch/android/ui/view/SupportedAppView;

    invoke-static {v1}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->log:Lorg/slf4j/Logger;

    .line 34
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mContext:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->adapter:Lcom/vectorwatch/android/ui/adapter/SupportedAppsAdapter;

    .line 37
    const-string v1, "layout_inflater"

    .line 38
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 39
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03006d

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 41
    const v1, 0x7f100207

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mLayout:Landroid/widget/RelativeLayout;

    .line 42
    const v1, 0x7f100208

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    .line 43
    const v1, 0x7f100178

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mTitle:Landroid/widget/TextView;

    .line 44
    const v1, 0x7f100179

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mSummary:Landroid/widget/TextView;

    .line 45
    const v1, 0x7f100209

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mIcon:Landroid/widget/ImageView;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/SupportedAppView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lorg/slf4j/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/SupportedAppView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Lcom/vectorwatch/android/models/AppInfoModel;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/SupportedAppView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/view/SupportedAppView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/SupportedAppView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/vectorwatch/android/models/AppInfoModel;)V
    .locals 3
    .param p1, "app"    # Lcom/vectorwatch/android/models/AppInfoModel;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getSupportedAppNotification(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setChecked(Z)V

    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppInfoModel;->appName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mSummary:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppInfoModel;->versionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppInfoModel;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 59
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/SupportedAppView;->setTaskAppearence()V

    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/SupportedAppView;->mLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/SupportedAppView$1;-><init>(Lcom/vectorwatch/android/ui/view/SupportedAppView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method public setTaskAppearence()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method
