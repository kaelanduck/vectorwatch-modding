.class public Lcom/vectorwatch/android/ui/view/AppsEnablerView;
.super Landroid/widget/RelativeLayout;
.source "AppsEnablerView.java"


# static fields
.field public static final NOTIFICATION_RESULT_CODE:I = 0x12d5f8

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

.field private mContext:Landroid/content/Context;

.field private mLayout:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/widget/RelativeLayout;Lcom/vectorwatch/android/ui/view/VectorCheckboxView;Landroid/content/Context;)V
    .locals 3
    .param p1, "layout"    # Landroid/widget/RelativeLayout;
    .param p2, "checkbox"    # Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 28
    iput-object p3, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mContext:Landroid/content/Context;

    .line 30
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mLayout:Landroid/widget/RelativeLayout;

    .line 31
    iput-object p2, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAppNotification(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->setChecked(Z)V

    .line 34
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/SupportedAppsListVisibilityChangeEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/view/VectorCheckboxView;->isChecked()Z

    move-result v2

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/SupportedAppsListVisibilityChangeEvent;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/view/AppsEnablerView$1;-><init>(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)Lcom/vectorwatch/android/ui/view/VectorCheckboxView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mCheckBox:Lcom/vectorwatch/android/ui/view/VectorCheckboxView;

    return-object v0
.end method

.method static synthetic access$100()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/view/AppsEnablerView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/AppsEnablerView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/AppsEnablerView;->mContext:Landroid/content/Context;

    return-object v0
.end method
