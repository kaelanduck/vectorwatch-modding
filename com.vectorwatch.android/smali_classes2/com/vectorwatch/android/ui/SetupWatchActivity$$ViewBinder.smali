.class public Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "SetupWatchActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/SetupWatchActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/SetupWatchActivity;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/SetupWatchActivity;, "TT;"
    const v6, 0x7f100149

    const v5, 0x7f100148

    const v4, 0x7f100147

    const v3, 0x7f100145

    const v2, 0x7f100143

    .line 11
    const-string v1, "field \'mInfoText\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mInfoText\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInfoText:Landroid/widget/TextView;

    .line 13
    const-string v1, "field \'mDownloadingProgress\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mDownloadingProgress\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/github/lzyzsd/circleprogress/DonutProgress;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    .line 15
    const-string v1, "field \'mUpdateProgress\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 16
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mUpdateProgress\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mUpdateProgress:Landroid/widget/TextView;

    .line 17
    const-string v1, "field \'mWaitingProgress\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 18
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mWaitingProgress\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mWaitingProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 19
    const-string v1, "field \'mButtonRetry\' and method \'retryButton\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 20
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mButtonRetry\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mButtonRetry:Landroid/widget/Button;

    .line 21
    new-instance v1, Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder;Lcom/vectorwatch/android/ui/SetupWatchActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/SetupWatchActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/SetupWatchActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/SetupWatchActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/SetupWatchActivity;, "TT;"
    const/4 v0, 0x0

    .line 32
    iput-object v0, p1, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mInfoText:Landroid/widget/TextView;

    .line 33
    iput-object v0, p1, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mDownloadingProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    .line 34
    iput-object v0, p1, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mUpdateProgress:Landroid/widget/TextView;

    .line 35
    iput-object v0, p1, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mWaitingProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 36
    iput-object v0, p1, Lcom/vectorwatch/android/ui/SetupWatchActivity;->mButtonRetry:Landroid/widget/Button;

    .line 37
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/SetupWatchActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/SetupWatchActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/SetupWatchActivity;)V

    return-void
.end method
