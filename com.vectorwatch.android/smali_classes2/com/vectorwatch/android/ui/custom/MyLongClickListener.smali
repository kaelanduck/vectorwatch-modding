.class public Lcom/vectorwatch/android/ui/custom/MyLongClickListener;
.super Ljava/lang/Object;
.source "MyLongClickListener.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# static fields
.field static final TAG:Ljava/lang/String; = "MyLongClickListener"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 15
    const-string v2, "MyLongClickListener"

    const-string v3, "onLongClick"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 16
    const-string v2, ""

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 17
    .local v0, "data":Landroid/content/ClipData;
    new-instance v1, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v1, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 18
    .local v1, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    invoke-virtual {p1, v0, v1, p1, v4}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 19
    return v4
.end method
