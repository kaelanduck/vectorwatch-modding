.class public Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;
.super Ljava/lang/Object;
.source "ActivityNeedleTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field back:Landroid/view/View;

.field private blockRotation:Z

.field infiniteRotation:Z

.field mContext:Landroid/content/Context;

.field private maxValueField:F

.field mode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

.field model:Lcom/vectorwatch/android/models/QuadrantModel;

.field needle:Landroid/widget/ImageView;

.field value:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/view/View;Lcom/vectorwatch/android/models/QuadrantModel;ZLcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "needle"    # Landroid/widget/ImageView;
    .param p3, "value"    # Landroid/widget/TextView;
    .param p4, "back"    # Landroid/view/View;
    .param p5, "model"    # Lcom/vectorwatch/android/models/QuadrantModel;
    .param p6, "infiniteRotation"    # Z
    .param p7, "mode"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p2, p0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    .line 32
    iput-object p3, p0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->value:Landroid/widget/TextView;

    .line 33
    iput-object p4, p0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->back:Landroid/view/View;

    .line 34
    iput-object p5, p0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    .line 35
    iput-boolean p6, p0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->infiniteRotation:Z

    .line 36
    iput-object p7, p0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mContext:Landroid/content/Context;

    .line 38
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    if-ne p7, v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-static {p1, v0}, Lcom/vectorwatch/android/utils/Helpers;->getMaxValueForField(Landroid/content/Context;I)F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->maxValueField:F

    .line 40
    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 44
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->back:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v3, v9

    .line 45
    .local v3, "centerXOfImageOnScreen":F
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->back:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v4, v9

    .line 47
    .local v4, "centerYOfImageOnScreen":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 49
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getVisibility()I

    move-result v9

    if-eqz v9, :cond_0

    .line 50
    const/4 v9, 0x1

    .line 155
    :goto_0
    return v9

    .line 52
    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/QuadrantModel;->getAngle()F

    move-result v14

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setRotation(F)V

    .line 54
    new-instance v8, Landroid/graphics/PointF;

    invoke-direct {v8, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 56
    .local v8, "viewCenter":Landroid/graphics/PointF;
    new-instance v7, Landroid/graphics/PointF;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v14

    invoke-direct {v7, v9, v14}, Landroid/graphics/PointF;-><init>(FF)V

    .line 59
    .local v7, "touchPoint":Landroid/graphics/PointF;
    iget v9, v7, Landroid/graphics/PointF;->x:F

    iget v14, v8, Landroid/graphics/PointF;->x:F

    sub-float/2addr v9, v14

    float-to-double v10, v9

    .line 60
    .local v10, "xDist":D
    iget v9, v7, Landroid/graphics/PointF;->y:F

    iget v14, v8, Landroid/graphics/PointF;->y:F

    sub-float/2addr v9, v14

    float-to-double v12, v9

    .line 62
    .local v12, "yDist":D
    div-double v14, v10, v12

    invoke-static {v14, v15}, Ljava/lang/Math;->atan(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    double-to-float v2, v14

    .line 64
    .local v2, "angle":F
    const-wide/16 v14, 0x0

    cmpg-double v9, v10, v14

    if-gez v9, :cond_1

    const-wide/16 v14, 0x0

    cmpl-double v9, v12, v14

    if-ltz v9, :cond_1

    .line 65
    const-wide v14, 0x400921fb54442d18L    # Math.PI

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    .line 67
    :cond_1
    const-wide/16 v14, 0x0

    cmpl-double v9, v10, v14

    if-ltz v9, :cond_2

    const-wide/16 v14, 0x0

    cmpl-double v9, v12, v14

    if-lez v9, :cond_2

    .line 68
    const-wide v14, 0x400921fb54442d18L    # Math.PI

    float-to-double v0, v2

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    double-to-float v2, v14

    .line 70
    :cond_2
    const-wide/16 v14, 0x0

    cmpl-double v9, v10, v14

    if-lez v9, :cond_3

    const-wide/16 v14, 0x0

    cmpg-double v9, v12, v14

    if-gtz v9, :cond_3

    .line 71
    const-wide v14, 0x401921fb54442d18L    # 6.283185307179586

    float-to-double v0, v2

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v2, v14

    .line 74
    :cond_3
    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v14

    double-to-float v9, v14

    neg-float v2, v9

    .line 75
    const/high16 v9, 0x43b40000    # 360.0f

    add-float/2addr v2, v9

    .line 77
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->blockRotation:Z

    if-eqz v9, :cond_4

    .line 78
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/QuadrantModel;->getAngle()F

    move-result v9

    sub-float v9, v2, v9

    const/4 v14, 0x0

    cmpl-float v9, v9, v14

    if-lez v9, :cond_9

    const/high16 v9, 0x40800000    # 4.0f

    cmpg-float v9, v2, v9

    if-gez v9, :cond_9

    .line 79
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->blockRotation:Z

    .line 85
    :cond_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->value:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/QuadrantModel;->getMaxValue()F

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->back:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getWidth()I

    move-result v14

    div-int/lit8 v14, v14, 0x2

    int-to-float v14, v14

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 88
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->back:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getHeight()I

    move-result v14

    div-int/lit8 v14, v14, 0x2

    int-to-float v14, v14

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 89
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    invoke-virtual {v9, v2}, Landroid/widget/ImageView;->setRotation(F)V

    .line 91
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/QuadrantModel;->getAngle()F

    move-result v6

    .line 93
    .local v6, "oldAngle":F
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->infiniteRotation:Z

    if-eqz v9, :cond_6

    .line 94
    sub-float v9, v6, v2

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v14, 0x43340000    # 180.0f

    cmpl-float v9, v9, v14

    if-lez v9, :cond_6

    .line 95
    cmpl-float v9, v2, v6

    if-lez v9, :cond_5

    .line 96
    sub-float v9, v6, v2

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    sub-float v9, v6, v9

    float-to-double v14, v9

    const-wide/16 v16, 0x0

    cmpg-double v9, v14, v16

    if-gez v9, :cond_5

    .line 97
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/QuadrantModel;->getFullRotation()I

    move-result v9

    if-eqz v9, :cond_5

    .line 98
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/QuadrantModel;->getFullRotation()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 103
    :cond_5
    cmpg-float v9, v2, v6

    if-gtz v9, :cond_6

    .line 104
    sub-float v9, v6, v2

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    add-float/2addr v9, v6

    const/high16 v14, 0x43b40000    # 360.0f

    cmpl-float v9, v9, v14

    if-lez v9, :cond_6

    .line 105
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/QuadrantModel;->getFullRotation()I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 111
    :cond_6
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/QuadrantModel;->getFullRotation()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/QuadrantModel;->getMaxRotation()I

    move-result v14

    mul-int/2addr v9, v14

    int-to-float v9, v9

    const/high16 v14, 0x43b40000    # 360.0f

    div-float v14, v2, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v15}, Lcom/vectorwatch/android/models/QuadrantModel;->getMaxRotation()I

    move-result v15

    int-to-float v15, v15

    mul-float/2addr v14, v15

    add-float/2addr v9, v14

    float-to-int v5, v9

    .line 112
    .local v5, "finalValue":I
    const/high16 v9, 0x42480000    # 50.0f

    cmpg-float v9, v6, v9

    if-gez v9, :cond_a

    const/high16 v9, 0x43960000    # 300.0f

    cmpl-float v9, v2, v9

    if-lez v9, :cond_a

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/QuadrantModel;->getMaxValue()F

    move-result v9

    int-to-float v14, v5

    sub-float/2addr v9, v14

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v14, 0x457a0000    # 4000.0f

    cmpl-float v9, v9, v14

    if-lez v9, :cond_a

    sget-object v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    if-ne v9, v14, :cond_a

    .line 113
    const/4 v5, 0x0

    .line 114
    const/4 v2, 0x0

    .line 115
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 116
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setRotation(F)V

    .line 117
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->blockRotation:Z

    .line 138
    :cond_7
    :goto_1
    sget-object v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    if-ne v9, v14, :cond_e

    .line 139
    int-to-double v14, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v16

    cmpg-double v9, v14, v16

    if-gez v9, :cond_d

    .line 140
    int-to-double v14, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v16

    add-double v14, v14, v16

    double-to-int v5, v14

    .line 152
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->value:Landroid/widget/TextView;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v9, v2}, Lcom/vectorwatch/android/models/QuadrantModel;->setAngle(F)V

    .line 154
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->value:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/QuadrantModel;->setMaxValue(I)V

    .line 155
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 81
    .end local v5    # "finalValue":I
    .end local v6    # "oldAngle":F
    :cond_9
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 118
    .restart local v5    # "finalValue":I
    .restart local v6    # "oldAngle":F
    :cond_a
    const/high16 v9, 0x42480000    # 50.0f

    cmpg-float v9, v6, v9

    if-gez v9, :cond_b

    const/high16 v9, 0x43960000    # 300.0f

    cmpl-float v9, v2, v9

    if-lez v9, :cond_b

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/QuadrantModel;->getMaxValue()F

    move-result v9

    int-to-float v14, v5

    sub-float/2addr v9, v14

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-double v14, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v16

    cmpg-double v9, v14, v16

    if-gez v9, :cond_b

    sget-object v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    if-ne v9, v14, :cond_b

    .line 119
    const/4 v5, 0x0

    .line 120
    const/4 v2, 0x0

    .line 121
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 122
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setRotation(F)V

    .line 123
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->blockRotation:Z

    goto/16 :goto_1

    .line 124
    :cond_b
    const/high16 v9, 0x42480000    # 50.0f

    cmpg-float v9, v6, v9

    if-gez v9, :cond_c

    const/high16 v9, 0x43960000    # 300.0f

    cmpl-float v9, v2, v9

    if-lez v9, :cond_c

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/QuadrantModel;->getMaxValue()F

    move-result v9

    int-to-float v14, v5

    sub-float/2addr v9, v14

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v14, 0x41100000    # 9.0f

    cmpl-float v9, v9, v14

    if-lez v9, :cond_c

    sget-object v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->SLEEP:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    if-ne v9, v14, :cond_c

    .line 125
    const/4 v5, 0x0

    .line 126
    const/4 v2, 0x0

    .line 127
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 128
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setRotation(F)V

    .line 129
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->blockRotation:Z

    goto/16 :goto_1

    .line 130
    :cond_c
    const/high16 v9, 0x42480000    # 50.0f

    cmpg-float v9, v6, v9

    if-gez v9, :cond_7

    const/high16 v9, 0x43960000    # 300.0f

    cmpl-float v9, v2, v9

    if-lez v9, :cond_7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/QuadrantModel;->getMaxValue()F

    move-result v9

    int-to-float v14, v5

    sub-float/2addr v9, v14

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v14, 0x40e00000    # 7.0f

    cmpl-float v9, v9, v14

    if-lez v9, :cond_7

    sget-object v9, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mode:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    if-ne v9, v14, :cond_7

    .line 131
    const/4 v5, 0x0

    .line 132
    const/4 v2, 0x0

    .line 133
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    .line 134
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->needle:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Landroid/widget/ImageView;->setRotation(F)V

    .line 135
    const/4 v9, 0x1

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->blockRotation:Z

    goto/16 :goto_1

    .line 141
    :cond_d
    int-to-double v14, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v16

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    mul-double v16, v16, v18

    cmpl-double v9, v14, v16

    if-lez v9, :cond_8

    .line 142
    int-to-double v14, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v16

    sub-double v14, v14, v16

    double-to-int v5, v14

    .line 143
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    goto/16 :goto_2

    .line 146
    :cond_e
    int-to-float v9, v5

    move-object/from16 v0, p0

    iget v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->maxValueField:F

    cmpl-float v9, v9, v14

    if-lez v9, :cond_8

    .line 147
    int-to-float v9, v5

    move-object/from16 v0, p0

    iget v14, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->maxValueField:F

    sub-float/2addr v9, v14

    float-to-int v5, v9

    .line 148
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    const/4 v14, 0x0

    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/QuadrantModel;->setFullRotation(I)V

    goto/16 :goto_2
.end method

.method public sendValues()Lcom/vectorwatch/android/models/QuadrantModel;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/vectorwatch/android/ui/custom/ActivityNeedleTouchListener;->model:Lcom/vectorwatch/android/models/QuadrantModel;

    return-object v0
.end method
