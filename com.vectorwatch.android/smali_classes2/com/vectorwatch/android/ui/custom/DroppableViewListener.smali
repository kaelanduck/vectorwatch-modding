.class public interface abstract Lcom/vectorwatch/android/ui/custom/DroppableViewListener;
.super Ljava/lang/Object;
.source "DroppableViewListener.java"


# virtual methods
.method public abstract viewBeganDragging(Landroid/view/View;)V
.end method

.method public abstract viewDidMove(Landroid/view/View;)V
.end method

.method public abstract viewDroppedOnTarget(Landroid/view/View;Landroid/view/View;)V
.end method

.method public abstract viewEndedDragging(Landroid/view/View;)V
.end method

.method public abstract viewEnteredTarget(Landroid/view/View;Landroid/view/View;)V
.end method

.method public abstract viewLeftTarget(Landroid/view/View;Landroid/view/View;)V
.end method
