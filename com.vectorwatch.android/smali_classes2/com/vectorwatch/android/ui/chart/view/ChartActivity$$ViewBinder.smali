.class public Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "ChartActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/chart/view/ChartActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/chart/view/ChartActivity;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/chart/view/ChartActivity;, "TT;"
    const v6, 0x7f1000aa

    const v2, 0x7f1000a9

    const v5, 0x7f1000a8

    const v4, 0x7f1000a7

    const v3, 0x7f1000a6

    .line 11
    const-string v1, "field \'mActivityChart\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mActivityChart\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/github/mikephil/charting/charts/LineChart;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    .line 13
    const-string v1, "field \'mTabLayout\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mTabLayout\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/TabLayout;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    .line 15
    const v1, 0x7f1000af

    const-string v2, "field \'mBtPageLeft\' and method \'buttonLeft\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 16
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1000af

    const-string v2, "field \'mBtPageLeft\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mBtPageLeft:Landroid/widget/ImageView;

    .line 17
    new-instance v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;Lcom/vectorwatch/android/ui/chart/view/ChartActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    const v1, 0x7f1000b0

    const-string v2, "field \'mBtPageRight\' and method \'buttonRight\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 26
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1000b0

    const-string v2, "field \'mBtPageRight\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mBtPageRight:Landroid/widget/ImageView;

    .line 27
    new-instance v1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder$2;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder$2;-><init>(Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;Lcom/vectorwatch/android/ui/chart/view/ChartActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    const-string v1, "field \'leftDate\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 36
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'leftDate\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->leftDate:Landroid/widget/TextView;

    .line 37
    const-string v1, "field \'centerDate\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 38
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'centerDate\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->centerDate:Landroid/widget/TextView;

    .line 39
    const-string v1, "field \'rightDate\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 40
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'rightDate\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    .line 41
    const v1, 0x7f1000ac

    const-string v2, "field \'txtTotalInfo\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 42
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1000ac

    const-string v2, "field \'txtTotalInfo\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    .line 43
    const v1, 0x7f1000b1

    const-string v2, "field \'mNoDataTextView\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 44
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1000b1

    const-string v2, "field \'mNoDataTextView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mNoDataTextView:Landroid/widget/TextView;

    .line 45
    const/4 v1, 0x4

    new-array v2, v1, [Landroid/widget/RadioButton;

    const/4 v3, 0x0

    const v1, 0x7f1000b2

    const-string v4, "field \'radioButtons\'"

    .line 46
    invoke-virtual {p1, p3, v1, v4}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    aput-object v1, v2, v3

    const/4 v3, 0x1

    const v1, 0x7f1000b4

    const-string v4, "field \'radioButtons\'"

    .line 47
    invoke-virtual {p1, p3, v1, v4}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    aput-object v1, v2, v3

    const/4 v3, 0x2

    const v1, 0x7f1000b3

    const-string v4, "field \'radioButtons\'"

    .line 48
    invoke-virtual {p1, p3, v1, v4}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    aput-object v1, v2, v3

    const/4 v3, 0x3

    const v1, 0x7f1000b5

    const-string v4, "field \'radioButtons\'"

    .line 49
    invoke-virtual {p1, p3, v1, v4}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    aput-object v1, v2, v3

    .line 45
    invoke-static {v2}, Lbutterknife/ButterKnife$Finder;->arrayOf([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    .line 51
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/chart/view/ChartActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/chart/view/ChartActivity;, "TT;"
    const/4 v0, 0x0

    .line 54
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mActivityChart:Lcom/github/mikephil/charting/charts/LineChart;

    .line 55
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mTabLayout:Landroid/support/design/widget/TabLayout;

    .line 56
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mBtPageLeft:Landroid/widget/ImageView;

    .line 57
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mBtPageRight:Landroid/widget/ImageView;

    .line 58
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->leftDate:Landroid/widget/TextView;

    .line 59
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->centerDate:Landroid/widget/TextView;

    .line 60
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->rightDate:Landroid/widget/TextView;

    .line 61
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->txtTotalInfo:Landroid/widget/TextView;

    .line 62
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->mNoDataTextView:Landroid/widget/TextView;

    .line 63
    iput-object v0, p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->radioButtons:[Landroid/widget/RadioButton;

    .line 64
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/chart/view/ChartActivity;)V

    return-void
.end method
