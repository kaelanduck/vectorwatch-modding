.class public interface abstract Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractor;
.super Ljava/lang/Object;
.source "ChartInteractor.java"


# virtual methods
.method public abstract fetchData(ZLjava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudSleep;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onStop()V
.end method
