.class public Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;
.super Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;
.source "GraphDataLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader",
        "<",
        "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field mCalendar:Ljava/util/Calendar;

.field private mCallback:Lretrofit/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mGoToCloud:Z

.field private mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

.field private mSleep:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudSleep;",
            ">;"
        }
    .end annotation
.end field

.field private mSummary:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;",
            ">;"
        }
    .end annotation
.end field

.field private sCaloriesBaseline:D

.field private sHasBase:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lretrofit/Callback;Lio/realm/Realm;Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "realm"    # Lio/realm/Realm;
    .param p4, "presenter"    # Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;",
            ">;",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    .local p2, "cbActivity":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;>;"
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;-><init>(Landroid/content/Context;)V

    .line 44
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    .line 60
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCallback:Lretrofit/Callback;

    .line 62
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    .line 63
    iget-wide v0, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    const-wide v2, 0x408f400000000000L    # 1000.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    const-wide v2, 0x40ab580000000000L    # 3500.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    .line 65
    iput-object p4, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    .line 66
    return-void

    .line 63
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private adjustSleepValue(J)J
    .locals 9
    .param p1, "value"    # J

    .prologue
    const-wide/16 v6, 0x1e

    const-wide/16 v4, 0xa

    .line 576
    cmp-long v2, p1, v4

    if-gtz v2, :cond_0

    .line 577
    mul-long v2, p1, v6

    div-long v0, v2, v4

    .line 584
    .local v0, "newValue":J
    :goto_0
    return-wide v0

    .line 578
    .end local v0    # "newValue":J
    :cond_0
    cmp-long v2, p1, v4

    if-lez v2, :cond_1

    const-wide/16 v2, 0x63

    cmp-long v2, p1, v2

    if-gtz v2, :cond_1

    .line 579
    const-wide/16 v2, 0xb

    sub-long v2, p1, v2

    mul-long/2addr v2, v6

    const-wide/16 v4, 0x58

    div-long/2addr v2, v4

    add-long v0, v2, v6

    .restart local v0    # "newValue":J
    goto :goto_0

    .line 581
    .end local v0    # "newValue":J
    :cond_1
    const-wide/16 v0, 0x3c

    .restart local v0    # "newValue":J
    goto :goto_0
.end method

.method private getActivityValuesForSleep(Ljava/util/List;JJIILjava/util/List;)Ljava/util/List;
    .locals 24
    .param p2, "startDate"    # J
    .param p4, "endDate"    # J
    .param p6, "toDivideTo"    # I
    .param p7, "intervalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SleepInfo;",
            ">;JJII",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 436
    .local p1, "sleepInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    .local p8, "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    if-gtz p7, :cond_0

    .line 437
    const/16 v19, 0x0

    .line 544
    :goto_0
    return-object v19

    .line 440
    :cond_0
    move/from16 v0, p7

    new-array v0, v0, [Ljava/lang/Float;

    move-object/from16 v18, v0

    .line 441
    .local v18, "values":[Ljava/lang/Float;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_1

    .line 442
    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v18, v11

    .line 441
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 448
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v19

    packed-switch v19, :pswitch_data_0

    .line 544
    :goto_2
    new-instance v19, Ljava/util/ArrayList;

    invoke-static/range {v18 .. v18}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 450
    :pswitch_0
    const/4 v11, 0x0

    :goto_3
    invoke-interface/range {p8 .. p8}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_4

    .line 451
    move-object/from16 v0, p8

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v16

    .line 454
    .local v16, "timestamp":J
    sub-long v4, v16, p2

    .line 456
    .local v4, "difference":J
    move/from16 v0, p6

    int-to-long v0, v0

    move-wide/from16 v20, v0

    div-long v20, v4, v20

    move-wide/from16 v0, v20

    long-to-int v12, v0

    .line 458
    .local v12, "interval":I
    move/from16 v0, p7

    if-ge v12, v0, :cond_2

    .line 460
    move-object/from16 v0, p8

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getEffTime()I

    move-result v19

    move/from16 v0, v19

    int-to-long v8, v0

    .line 462
    .local v8, "effTime":J
    move-wide/from16 v0, v16

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->isSleepTime(JLjava/util/List;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 463
    aget-object v19, v18, v12

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v19

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->adjustSleepValue(J)J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v20, v0

    add-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v18, v12

    .line 450
    .end local v8    # "effTime":J
    :cond_2
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 465
    .restart local v8    # "effTime":J
    :cond_3
    aget-object v19, v18, v12

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v19

    const/high16 v20, 0x42b40000    # 90.0f

    add-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v18, v12

    goto :goto_4

    .line 469
    .end local v4    # "difference":J
    .end local v8    # "effTime":J
    .end local v12    # "interval":I
    .end local v16    # "timestamp":J
    :cond_4
    const/4 v15, 0x0

    .line 470
    .local v15, "totalHours":F
    const/4 v11, 0x0

    :goto_5
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_5

    .line 471
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v20

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    .line 472
    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStart()I

    move-result v19

    sub-int v19, v20, v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x45610000    # 3600.0f

    div-float v19, v19, v20

    add-float v15, v15, v19

    .line 470
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 474
    :cond_5
    invoke-static {}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object v10

    .line 476
    .local v10, "format":Ljava/text/DecimalFormat;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/text/DecimalFormat;->setGroupingUsed(Z)V

    .line 477
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/text/DecimalFormat;->setGroupingSize(I)V

    .line 478
    const-string v19, "#00"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 479
    move v7, v15

    .line 480
    .local v7, "finalValue":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f09013d

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f090277

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-interface {v0, v10, v7, v1, v2}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 484
    .end local v7    # "finalValue":F
    .end local v10    # "format":Ljava/text/DecimalFormat;
    .end local v15    # "totalHours":F
    :pswitch_1
    const/4 v11, 0x0

    :goto_6
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_7

    .line 485
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 486
    .restart local v16    # "timestamp":J
    sub-long v4, v16, p2

    .line 487
    .restart local v4    # "difference":J
    move/from16 v0, p6

    int-to-long v0, v0

    move-wide/from16 v20, v0

    div-long v20, v4, v20

    move-wide/from16 v0, v20

    long-to-int v12, v0

    .line 488
    .restart local v12    # "interval":I
    move/from16 v0, p7

    if-ge v12, v0, :cond_6

    .line 490
    aget-object v19, v18, v12

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v20

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v21

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    .line 491
    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStart()I

    move-result v19

    sub-int v19, v21, v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v21, 0x45610000    # 3600.0f

    div-float v19, v19, v21

    add-float v19, v19, v20

    .line 490
    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v18, v12

    .line 484
    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 494
    .end local v4    # "difference":J
    .end local v12    # "interval":I
    .end local v16    # "timestamp":J
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/high16 v21, -0x40000000    # -2.0f

    const-string v22, ""

    const-string v23, ""

    invoke-interface/range {v19 .. v23}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 498
    :pswitch_2
    const/4 v11, 0x0

    :goto_7
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_9

    .line 499
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 500
    .restart local v16    # "timestamp":J
    sub-long v4, v16, p2

    .line 501
    .restart local v4    # "difference":J
    move/from16 v0, p6

    int-to-long v0, v0

    move-wide/from16 v20, v0

    div-long v20, v4, v20

    move-wide/from16 v0, v20

    long-to-int v12, v0

    .line 502
    .restart local v12    # "interval":I
    move/from16 v0, p7

    if-ge v12, v0, :cond_8

    .line 504
    aget-object v19, v18, v12

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v20

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v21

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    .line 505
    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStart()I

    move-result v19

    sub-int v19, v21, v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v21, 0x45610000    # 3600.0f

    div-float v19, v19, v21

    add-float v19, v19, v20

    .line 504
    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v18, v12

    .line 498
    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 508
    .end local v4    # "difference":J
    .end local v12    # "interval":I
    .end local v16    # "timestamp":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/high16 v21, -0x40000000    # -2.0f

    const-string v22, ""

    const-string v23, ""

    invoke-interface/range {v19 .. v23}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 512
    :pswitch_3
    move/from16 v0, p7

    new-array v6, v0, [Ljava/lang/Long;

    .line 513
    .local v6, "divider":[Ljava/lang/Long;
    const/4 v11, 0x0

    :goto_8
    array-length v0, v6

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_a

    .line 514
    const-wide/16 v20, 0x0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v6, v11

    .line 513
    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    .line 516
    :cond_a
    const/4 v13, 0x0

    .line 517
    .local v13, "lastTimestamp":I
    const/4 v11, 0x0

    :goto_9
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_d

    .line 518
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 519
    .restart local v16    # "timestamp":J
    sub-long v4, v16, p2

    .line 520
    .restart local v4    # "difference":J
    move/from16 v0, p6

    int-to-long v0, v0

    move-wide/from16 v20, v0

    div-long v20, v4, v20

    move-wide/from16 v0, v20

    long-to-int v12, v0

    .line 521
    .restart local v12    # "interval":I
    move/from16 v0, p7

    if-ge v12, v0, :cond_c

    .line 523
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v20

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    .line 524
    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStart()I

    move-result v19

    sub-int v19, v20, v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x45610000    # 3600.0f

    div-float v14, v19, v20

    .line 526
    .local v14, "sleep":F
    const/16 v19, 0x0

    cmpl-float v19, v14, v19

    if-lez v19, :cond_c

    .line 527
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getTimestamp()I

    move-result v19

    move/from16 v0, v19

    if-eq v0, v13, :cond_b

    .line 528
    aget-object v19, v6, v12

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    const-wide/16 v22, 0x1

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v6, v12

    .line 529
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual/range {v19 .. v19}, Lcom/vectorwatch/android/models/SleepInfo;->getTimestamp()I

    move-result v13

    .line 531
    :cond_b
    aget-object v19, v18, v12

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v19

    add-float v19, v19, v14

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v18, v12

    .line 517
    .end local v14    # "sleep":F
    :cond_c
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_9

    .line 535
    .end local v4    # "difference":J
    .end local v12    # "interval":I
    .end local v16    # "timestamp":J
    :cond_d
    const/4 v11, 0x0

    :goto_a
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v11, v0, :cond_f

    .line 536
    aget-object v19, v6, v11

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-eqz v19, :cond_e

    .line 537
    aget-object v19, v18, v11

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v19

    aget-object v20, v6, v11

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v20, v0

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v18, v11

    .line 535
    :cond_e
    add-int/lit8 v11, v11, 0x1

    goto :goto_a

    .line 540
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/high16 v21, -0x40000000    # -2.0f

    const-string v22, ""

    const-string v23, ""

    invoke-interface/range {v19 .. v23}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 448
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getActivityValuesPerInterval(Ljava/util/List;JII)Ljava/util/List;
    .locals 24
    .param p2, "startDate"    # J
    .param p4, "toDivideTo"    # I
    .param p5, "intervalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            ">;JII)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1047
    .local p1, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    if-gtz p5, :cond_0

    .line 1048
    const-string v20, "Charts"

    const-string v21, "Invalid interval count."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    const/16 v20, 0x0

    .line 1203
    :goto_0
    return-object v20

    .line 1052
    :cond_0
    move/from16 v0, p5

    new-array v0, v0, [Ljava/lang/Long;

    move-object/from16 v17, v0

    .line 1053
    .local v17, "values":[Ljava/lang/Long;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_1

    .line 1054
    const-wide/16 v20, 0x0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v17, v12

    .line 1053
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 1056
    :cond_1
    move/from16 v0, p5

    new-array v8, v0, [Ljava/lang/Long;

    .line 1057
    .local v8, "divider":[Ljava/lang/Long;
    const/4 v12, 0x0

    :goto_2
    array-length v0, v8

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_2

    .line 1058
    const-wide/16 v20, 0x0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v8, v12

    .line 1057
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 1061
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v20

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v20

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    .line 1062
    if-eqz p1, :cond_8

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v20

    if-lez v20, :cond_8

    .line 1063
    const/4 v12, 0x0

    :goto_3
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v12, v0, :cond_8

    .line 1064
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v14

    .line 1067
    .local v14, "timestamp":J
    sub-long v4, v14, p2

    .line 1069
    .local v4, "difference":J
    move/from16 v0, p4

    int-to-long v0, v0

    move-wide/from16 v20, v0

    div-long v20, v4, v20

    move-wide/from16 v0, v20

    long-to-int v13, v0

    .line 1071
    .local v13, "interval":I
    move/from16 v0, p5

    if-ge v13, v0, :cond_3

    .line 1073
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v20

    packed-switch v20, :pswitch_data_0

    .line 1063
    :cond_3
    :goto_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 1075
    :pswitch_0
    aget-object v20, v17, v13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getCal()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v17, v13

    goto :goto_4

    .line 1079
    :pswitch_1
    aget-object v20, v17, v13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getDist()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v17, v13

    goto :goto_4

    .line 1083
    :pswitch_2
    aget-object v20, v17, v13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getVal()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v17, v13

    goto :goto_4

    .line 1090
    .end local v4    # "difference":J
    .end local v13    # "interval":I
    .end local v14    # "timestamp":J
    :cond_4
    const/4 v12, 0x0

    :goto_5
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v12, v0, :cond_6

    .line 1091
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v14

    .line 1094
    .restart local v14    # "timestamp":J
    sub-long v4, v14, p2

    .line 1096
    .restart local v4    # "difference":J
    move/from16 v0, p4

    int-to-long v0, v0

    move-wide/from16 v20, v0

    div-long v20, v4, v20

    move-wide/from16 v0, v20

    long-to-int v13, v0

    .line 1098
    .restart local v13    # "interval":I
    move/from16 v0, p5

    if-ge v13, v0, :cond_5

    .line 1100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v20

    packed-switch v20, :pswitch_data_1

    .line 1090
    :cond_5
    :goto_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    .line 1102
    :pswitch_3
    aget-object v20, v17, v13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getCal()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v17, v13

    .line 1103
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getCal()I

    move-result v20

    if-lez v20, :cond_5

    .line 1104
    aget-object v20, v8, v13

    aget-object v20, v8, v13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    const-wide/16 v22, 0x1

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v8, v13

    goto :goto_6

    .line 1109
    :pswitch_4
    aget-object v20, v17, v13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getDist()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v17, v13

    .line 1110
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getDist()I

    move-result v20

    if-lez v20, :cond_5

    .line 1111
    aget-object v20, v8, v13

    aget-object v20, v8, v13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    const-wide/16 v22, 0x1

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v8, v13

    goto/16 :goto_6

    .line 1116
    :pswitch_5
    aget-object v20, v17, v13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getVal()I

    move-result v20

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v17, v13

    .line 1117
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getVal()I

    move-result v20

    if-lez v20, :cond_5

    .line 1118
    aget-object v20, v8, v13

    aget-object v20, v8, v13

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    const-wide/16 v22, 0x1

    add-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v8, v13

    goto/16 :goto_6

    .line 1125
    .end local v4    # "difference":J
    .end local v13    # "interval":I
    .end local v14    # "timestamp":J
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v20

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_8

    .line 1126
    const/4 v12, 0x0

    :goto_7
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_8

    .line 1127
    aget-object v20, v8, v12

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-eqz v20, :cond_7

    .line 1128
    aget-object v20, v17, v12

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    aget-object v22, v8, v12

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    div-long v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v17, v12

    .line 1126
    :cond_7
    add-int/lit8 v12, v12, 0x1

    goto :goto_7

    .line 1134
    :cond_8
    invoke-static {}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object v11

    .line 1136
    .local v11, "format":Ljava/text/DecimalFormat;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v20

    packed-switch v20, :pswitch_data_2

    .line 1203
    :goto_8
    new-instance v20, Ljava/util/ArrayList;

    invoke-static/range {v17 .. v17}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/16 :goto_0

    .line 1138
    :pswitch_6
    const/16 v16, 0x0

    .line 1139
    .local v16, "total":I
    const/4 v12, 0x0

    :goto_9
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_9

    .line 1140
    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    aget-object v22, v17, v12

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v16, v0

    .line 1139
    add-int/lit8 v12, v12, 0x1

    goto :goto_9

    .line 1146
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v20

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_a

    .line 1147
    const v20, 0x186a0

    div-int v20, v16, v20

    move/from16 v0, v20

    int-to-float v9, v0

    .line 1148
    .local v9, "finalValue":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f090087

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1162
    .local v6, "distanceInd":Ljava/lang/String;
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f090277

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v11, v9, v6, v1}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 1149
    .end local v6    # "distanceInd":Ljava/lang/String;
    .end local v9    # "finalValue":F
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    .line 1150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 1151
    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Lcom/vectorwatch/android/utils/Helpers;->convertCmToKm(J)F

    move-result v9

    .line 1152
    .restart local v9    # "finalValue":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f090146

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "distanceInd":Ljava/lang/String;
    goto :goto_a

    .line 1154
    .end local v6    # "distanceInd":Ljava/lang/String;
    .end local v9    # "finalValue":F
    :cond_b
    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Lcom/vectorwatch/android/utils/Helpers;->convertCmToMiles(J)F

    move-result v9

    .line 1155
    .restart local v9    # "finalValue":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0901ab

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "distanceInd":Ljava/lang/String;
    goto :goto_a

    .line 1158
    .end local v6    # "distanceInd":Ljava/lang/String;
    .end local v9    # "finalValue":F
    :cond_c
    move/from16 v0, v16

    int-to-float v9, v0

    .line 1159
    .restart local v9    # "finalValue":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f090235

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "distanceInd":Ljava/lang/String;
    goto :goto_a

    .line 1167
    .end local v6    # "distanceInd":Ljava/lang/String;
    .end local v9    # "finalValue":F
    .end local v16    # "total":I
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v20

    if-eqz v20, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_11

    .line 1168
    :cond_d
    const-wide/16 v2, 0x0

    .line 1169
    .local v2, "best":J
    const/4 v12, 0x0

    :goto_b
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_e

    .line 1170
    aget-object v20, v17, v12

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 1169
    add-int/lit8 v12, v12, 0x1

    goto :goto_b

    .line 1173
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_10

    .line 1176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 1177
    invoke-static {v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->convertCmToKm(J)F

    move-result v10

    .line 1178
    .local v10, "finalValue2":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f090146

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1184
    .local v7, "distanceInd2":Ljava/lang/String;
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f090073

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v11, v10, v7, v1}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 1180
    .end local v7    # "distanceInd2":Ljava/lang/String;
    .end local v10    # "finalValue2":F
    :cond_f
    invoke-static {v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->convertCmToMiles(J)F

    move-result v10

    .line 1181
    .restart local v10    # "finalValue2":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0901ab

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "distanceInd2":Ljava/lang/String;
    goto :goto_c

    .line 1186
    .end local v7    # "distanceInd2":Ljava/lang/String;
    .end local v10    # "finalValue2":F
    :cond_10
    long-to-float v10, v2

    .line 1187
    .restart local v10    # "finalValue2":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f090235

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1189
    .restart local v7    # "distanceInd2":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f090073

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v11, v10, v7, v1}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 1192
    .end local v2    # "best":J
    .end local v7    # "distanceInd2":Ljava/lang/String;
    .end local v10    # "finalValue2":F
    :cond_11
    const-wide/16 v18, 0x0

    .line 1193
    .local v18, "total2":J
    const/4 v12, 0x0

    :goto_d
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_12

    .line 1194
    aget-object v20, v17, v12

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    add-long v18, v18, v20

    .line 1193
    add-int/lit8 v12, v12, 0x1

    goto :goto_d

    .line 1196
    :cond_12
    const-wide/32 v20, 0x186a0

    div-long v20, v18, v20

    move-wide/from16 v0, v20

    long-to-float v10, v0

    .line 1197
    .restart local v10    # "finalValue2":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f090087

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1198
    .restart local v7    # "distanceInd2":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f090277

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v11, v10, v7, v1}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 1073
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 1100
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    .line 1136
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method private getSleepForInterval(Landroid/content/ContentResolver;JJ)Ljava/util/List;
    .locals 10
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "startTime"    # J
    .param p4, "stopTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SleepInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1216
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1218
    .local v8, "sleepList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "start_time"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "stop_time"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "timestamp"

    aput-object v3, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop_time>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ">"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "stop_time"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " OR "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ">"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "start_time"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "<"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "start_time"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1226
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1227
    new-instance v7, Lcom/vectorwatch/android/models/SleepInfo;

    const-string v0, "start_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v1, "stop_time"

    .line 1228
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-string v2, "timestamp"

    .line 1230
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1229
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-direct {v7, v0, v1, v2}, Lcom/vectorwatch/android/models/SleepInfo;-><init>(III)V

    .line 1231
    .local v7, "sleep":Lcom/vectorwatch/android/models/SleepInfo;
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 1234
    .end local v7    # "sleep":Lcom/vectorwatch/android/models/SleepInfo;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1236
    return-object v8
.end method

.method private static isSleepTime(JLjava/util/List;)Z
    .locals 4
    .param p0, "timestamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SleepInfo;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 555
    .local p2, "sleepInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    const/4 v1, 0x0

    .line 557
    .local v1, "isSleep":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 558
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStart()I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, p0, v2

    if-ltz v2, :cond_1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, p0, v2

    if-gtz v2, :cond_1

    .line 559
    const/4 v1, 0x1

    .line 564
    :cond_0
    return v1

    .line 557
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private populateChartData(Ljava/util/List;Landroid/util/SparseArray;JII)V
    .locals 19
    .param p3, "startDate"    # J
    .param p5, "toDivideTo"    # I
    .param p6, "currentInterval"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;JII)V"
        }
    .end annotation

    .prologue
    .line 598
    .local p1, "valuesPerIntervalList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .local p2, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    const/4 v2, 0x0

    .line 599
    .local v2, "average":F
    const/4 v3, 0x0

    .line 601
    .local v3, "divider":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    if-ge v5, v13, :cond_7

    .line 602
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    .line 603
    .local v10, "value":Ljava/lang/Float;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v12, v14

    .line 606
    .local v12, "zeroValue":F
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v13}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_3

    .line 607
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v13

    const v14, 0x47c35000    # 100000.0f

    div-float v4, v13, v14

    .line 608
    .local v4, "finalValue":F
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v13

    const/4 v14, 0x0

    cmpg-float v13, v13, v14

    if-gtz v13, :cond_2

    move v11, v12

    .line 613
    .end local v4    # "finalValue":F
    .local v11, "valueToPlotOnY":F
    :goto_2
    mul-int v13, v5, p5

    int-to-long v14, v13

    add-long v14, v14, p3

    const-wide/16 v16, 0x1

    add-long v8, v14, v16

    .line 614
    .local v8, "timestamp":J
    sget-object v13, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Timestamp on X-axis: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 616
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v13}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v13

    const/4 v14, 0x3

    if-ne v13, v14, :cond_5

    .line 617
    add-float/2addr v2, v11

    .line 618
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_0

    .line 619
    add-int/lit8 v3, v3, 0x1

    .line 601
    :cond_0
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 603
    .end local v8    # "timestamp":J
    .end local v11    # "valueToPlotOnY":F
    .end local v12    # "zeroValue":F
    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    .restart local v4    # "finalValue":F
    .restart local v12    # "zeroValue":F
    :cond_2
    move v11, v4

    .line 608
    goto :goto_2

    .line 610
    .end local v4    # "finalValue":F
    :cond_3
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v13

    const/4 v14, 0x0

    cmpg-float v13, v13, v14

    if-gez v13, :cond_4

    const/4 v11, 0x0

    .restart local v11    # "valueToPlotOnY":F
    :goto_4
    goto :goto_2

    .end local v11    # "valueToPlotOnY":F
    :cond_4
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v11

    goto :goto_4

    .line 622
    .restart local v8    # "timestamp":J
    .restart local v11    # "valueToPlotOnY":F
    :cond_5
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v13

    if-lez v13, :cond_6

    .line 623
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 624
    .local v7, "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    int-to-float v13, v5

    invoke-virtual {v7, v13, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 625
    const-wide/16 v14, 0x3e8

    mul-long/2addr v14, v8

    invoke-virtual {v7, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v13

    invoke-virtual {v13, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 626
    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v7}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_3

    .line 628
    .end local v7    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_6
    new-instance v6, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v13, v5

    invoke-direct {v6, v13, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 629
    .local v6, "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    const-wide/16 v14, 0x3e8

    mul-long/2addr v14, v8

    invoke-virtual {v6, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v13

    invoke-virtual {v13, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 630
    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_3

    .line 635
    .end local v6    # "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .end local v8    # "timestamp":J
    .end local v10    # "value":Ljava/lang/Float;
    .end local v11    # "valueToPlotOnY":F
    .end local v12    # "zeroValue":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v13}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v13

    const/4 v14, 0x3

    if-ne v13, v14, :cond_a

    .line 636
    if-lez v3, :cond_8

    .line 637
    int-to-float v13, v3

    div-float/2addr v2, v13

    .line 639
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v13, :cond_9

    const/4 v13, 0x0

    cmpl-float v13, v2, v13

    if-nez v13, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v13}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_9

    .line 640
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v2, v14

    .line 642
    :cond_9
    move-object/from16 v0, p2

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v13

    if-lez v13, :cond_b

    .line 643
    move-object/from16 v0, p2

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 644
    .restart local v7    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    move/from16 v0, p6

    int-to-float v13, v0

    invoke-virtual {v7, v13, v2}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 645
    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    invoke-virtual {v7, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v13

    invoke-virtual {v13, v2}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 646
    move-object/from16 v0, p2

    move/from16 v1, p6

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 653
    .end local v7    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_a
    :goto_5
    return-void

    .line 648
    :cond_b
    new-instance v6, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move/from16 v0, p6

    int-to-float v13, v0

    invoke-direct {v6, v13, v2}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 649
    .restart local v6    # "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    invoke-virtual {v6, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v13

    invoke-virtual {v13, v2}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 650
    move-object/from16 v0, p2

    move/from16 v1, p6

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_5
.end method

.method private populateChartData(Ljava/util/List;Landroid/util/SparseArray;JIII)V
    .locals 19
    .param p3, "startDate"    # J
    .param p5, "toDivideTo"    # I
    .param p6, "dataType"    # I
    .param p7, "currentInterval"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;JIII)V"
        }
    .end annotation

    .prologue
    .line 667
    .local p1, "valuesPerIntervalList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p2, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    const/4 v2, 0x0

    .line 668
    .local v2, "average":F
    const/4 v3, 0x0

    .line 670
    .local v3, "divider":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v12

    if-ge v5, v12, :cond_5

    .line 671
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    .line 672
    .local v10, "value":Ljava/lang/Long;
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-gez v12, :cond_1

    const/4 v11, 0x0

    .line 674
    .local v11, "valueToPlotOnY":F
    :goto_1
    packed-switch p6, :pswitch_data_0

    .line 699
    :goto_2
    mul-int v12, v5, p5

    int-to-long v12, v12

    add-long v12, v12, p3

    const-wide/16 v14, 0x1

    add-long v8, v12, v14

    .line 700
    .local v8, "timestamp":J
    sget-object v12, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Timestamp on X-axis: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 702
    const/4 v12, 0x3

    move/from16 v0, p7

    if-ne v0, v12, :cond_3

    .line 703
    add-float/2addr v2, v11

    .line 704
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-lez v12, :cond_0

    .line 705
    add-int/lit8 v3, v3, 0x1

    .line 670
    :cond_0
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 672
    .end local v8    # "timestamp":J
    .end local v11    # "valueToPlotOnY":F
    :cond_1
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    long-to-float v11, v12

    goto :goto_1

    .line 676
    .restart local v11    # "valueToPlotOnY":F
    :pswitch_0
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    const-wide/32 v14, 0x186a0

    div-long/2addr v12, v14

    long-to-float v11, v12

    .line 677
    goto :goto_2

    .line 680
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    invoke-static {v12}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 681
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/vectorwatch/android/utils/Helpers;->convertCmToKm(J)F

    move-result v11

    goto :goto_2

    .line 683
    :cond_2
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/vectorwatch/android/utils/Helpers;->convertCmToMiles(J)F

    move-result v11

    .line 685
    goto :goto_2

    .line 688
    :pswitch_2
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    long-to-float v11, v12

    .line 689
    goto :goto_2

    .line 692
    :pswitch_3
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    long-to-float v11, v12

    .line 693
    goto :goto_2

    .line 708
    .restart local v8    # "timestamp":J
    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v12

    if-lez v12, :cond_4

    .line 709
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 710
    .local v7, "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    int-to-float v12, v5

    invoke-virtual {v7, v12, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 711
    const-wide/16 v12, 0x3e8

    mul-long/2addr v12, v8

    invoke-virtual {v7, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    invoke-virtual {v12, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 712
    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v7}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_3

    .line 714
    .end local v7    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_4
    new-instance v6, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v12, v5

    invoke-direct {v6, v12, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 715
    .local v6, "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    const-wide/16 v12, 0x3e8

    mul-long/2addr v12, v8

    invoke-virtual {v6, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    invoke-virtual {v12, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 716
    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_3

    .line 721
    .end local v6    # "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .end local v8    # "timestamp":J
    .end local v10    # "value":Ljava/lang/Long;
    .end local v11    # "valueToPlotOnY":F
    :cond_5
    const/4 v12, 0x3

    move/from16 v0, p7

    if-ne v0, v12, :cond_7

    .line 722
    if-lez v3, :cond_6

    .line 723
    int-to-float v12, v3

    div-float/2addr v2, v12

    .line 725
    :cond_6
    move-object/from16 v0, p2

    move/from16 v1, p7

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v12

    if-lez v12, :cond_a

    .line 726
    move-object/from16 v0, p2

    move/from16 v1, p7

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 727
    .restart local v7    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    move/from16 v0, p7

    int-to-float v12, v0

    invoke-virtual {v7, v12, v2}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 728
    const-wide/16 v12, 0x3e8

    mul-long v12, v12, p3

    invoke-virtual {v7, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    invoke-virtual {v12, v2}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 729
    move-object/from16 v0, p2

    move/from16 v1, p7

    invoke-virtual {v0, v1, v7}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 737
    .end local v7    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_7
    :goto_4
    const/4 v12, 0x3

    move/from16 v0, p6

    if-ne v0, v12, :cond_c

    .line 738
    if-nez p7, :cond_b

    .line 739
    const/4 v12, 0x0

    new-instance v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v14, 0x0

    const/high16 v15, 0x42b40000    # 90.0f

    invoke-direct {v13, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    invoke-virtual {v13, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 769
    :goto_5
    const/4 v4, 0x0

    .line 770
    .local v4, "endIndex":I
    packed-switch p7, :pswitch_data_1

    .line 781
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 786
    :goto_6
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v12

    if-gez v12, :cond_8

    .line 787
    const/4 v12, 0x2

    move/from16 v0, p6

    if-ne v0, v12, :cond_16

    .line 788
    packed-switch p7, :pswitch_data_2

    .line 818
    :cond_8
    :goto_7
    const/4 v12, 0x2

    move/from16 v0, p6

    if-ne v0, v12, :cond_9

    .line 819
    packed-switch p7, :pswitch_data_3

    .line 843
    :cond_9
    :goto_8
    return-void

    .line 731
    .end local v4    # "endIndex":I
    :cond_a
    new-instance v6, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move/from16 v0, p7

    int-to-float v12, v0

    invoke-direct {v6, v12, v2}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 732
    .restart local v6    # "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    const-wide/16 v12, 0x3e8

    mul-long v12, v12, p3

    invoke-virtual {v6, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    invoke-virtual {v12, v2}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 733
    move-object/from16 v0, p2

    move/from16 v1, p7

    invoke-virtual {v0, v1, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_4

    .line 741
    .end local v6    # "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_b
    const/4 v12, 0x0

    new-instance v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct {v13, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    invoke-virtual {v13, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_5

    .line 743
    :cond_c
    const/4 v12, 0x2

    move/from16 v0, p6

    if-ne v0, v12, :cond_11

    .line 744
    packed-switch p7, :pswitch_data_4

    goto :goto_5

    .line 746
    :pswitch_4
    const/4 v13, 0x0

    new-instance v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_d

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    double-to-float v12, v0

    const/high16 v16, 0x42c80000    # 100.0f

    div-float v12, v12, v16

    :goto_9
    invoke-direct {v14, v15, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v16, 0x3e8

    mul-long v16, v16, p3

    .line 747
    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    .line 746
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_5

    :cond_d
    const/4 v12, 0x0

    goto :goto_9

    .line 751
    :pswitch_5
    const/4 v13, 0x0

    new-instance v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_e

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    double-to-float v12, v0

    :goto_a
    invoke-direct {v14, v15, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v16, 0x3e8

    mul-long v16, v16, p3

    .line 752
    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    .line 751
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_5

    :cond_e
    const/4 v12, 0x0

    goto :goto_a

    .line 756
    :pswitch_6
    const/4 v13, 0x0

    new-instance v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_f

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    double-to-float v12, v0

    :goto_b
    invoke-direct {v14, v15, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v16, 0x3e8

    mul-long v16, v16, p3

    .line 757
    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    .line 756
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_5

    :cond_f
    const/4 v12, 0x0

    goto :goto_b

    .line 761
    :pswitch_7
    const/4 v13, 0x0

    new-instance v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_10

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    double-to-float v12, v0

    :goto_c
    invoke-direct {v14, v15, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v16, 0x3e8

    mul-long v16, v16, p3

    .line 762
    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    .line 761
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_5

    :cond_10
    const/4 v12, 0x0

    goto :goto_c

    .line 766
    :cond_11
    const/4 v12, 0x0

    new-instance v13, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct {v13, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    invoke-virtual {v13, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_5

    .line 772
    .restart local v4    # "endIndex":I
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v13, 0xb

    invoke-virtual {v12, v13}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 773
    goto/16 :goto_6

    .line 775
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v13, 0x7

    invoke-virtual {v12, v13}, Ljava/util/Calendar;->get(I)I

    move-result v12

    add-int/lit8 v4, v12, -0x1

    .line 776
    goto/16 :goto_6

    .line 778
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Ljava/util/Calendar;->get(I)I

    move-result v12

    add-int/lit8 v4, v12, -0x1

    .line 779
    goto/16 :goto_6

    .line 790
    :pswitch_b
    const/4 v13, 0x0

    new-instance v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v15, v4

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_12

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    double-to-float v12, v0

    const/high16 v16, 0x42c80000    # 100.0f

    div-float v12, v12, v16

    :goto_d
    invoke-direct {v14, v15, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v16, 0x3e8

    mul-long v16, v16, p3

    .line 791
    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    .line 790
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    :cond_12
    const/4 v12, 0x0

    goto :goto_d

    .line 795
    :pswitch_c
    const/4 v13, 0x0

    new-instance v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v15, v4

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_13

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    double-to-float v12, v0

    :goto_e
    invoke-direct {v14, v15, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v16, 0x3e8

    mul-long v16, v16, p3

    .line 796
    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    .line 795
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    :cond_13
    const/4 v12, 0x0

    goto :goto_e

    .line 800
    :pswitch_d
    const/4 v13, 0x0

    new-instance v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v15, v4

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    double-to-float v12, v0

    :goto_f
    invoke-direct {v14, v15, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v16, 0x3e8

    mul-long v16, v16, p3

    .line 801
    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    .line 800
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    :cond_14
    const/4 v12, 0x0

    goto :goto_f

    .line 805
    :pswitch_e
    const/4 v13, 0x0

    new-instance v14, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v15, v4

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_15

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    double-to-float v12, v0

    :goto_10
    invoke-direct {v14, v15, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v16, 0x3e8

    mul-long v16, v16, p3

    .line 806
    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    .line 805
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    :cond_15
    const/4 v12, 0x0

    goto :goto_10

    .line 810
    :cond_16
    const/4 v12, 0x3

    move/from16 v0, p6

    if-ne v0, v12, :cond_17

    .line 811
    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v13, v4

    const/high16 v14, 0x42b40000    # 90.0f

    invoke-direct {v12, v13, v14}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    .line 813
    :cond_17
    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v13, v4

    const/4 v14, 0x0

    invoke-direct {v12, v13, v14}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v12}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    .line 821
    :pswitch_f
    const/4 v5, 0x0

    :goto_11
    invoke-virtual/range {p2 .. p2}, Landroid/util/SparseArray;->size()I

    move-result v12

    if-ge v5, v12, :cond_9

    .line 822
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 823
    .restart local v7    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v12

    float-to-double v12, v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    const-wide/high16 v16, 0x4059000000000000L    # 100.0

    div-double v14, v14, v16

    cmpg-double v12, v12, v14

    if-gez v12, :cond_18

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_18

    .line 824
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    div-double/2addr v12, v14

    double-to-float v12, v12

    invoke-virtual {v7, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 825
    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    const-wide/high16 v16, 0x4059000000000000L    # 100.0

    div-double v14, v14, v16

    double-to-float v13, v14

    invoke-virtual {v7, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 826
    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v7}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 821
    :cond_18
    add-int/lit8 v5, v5, 0x1

    goto :goto_11

    .line 834
    .end local v7    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :pswitch_10
    invoke-virtual/range {p2 .. p2}, Landroid/util/SparseArray;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 835
    .restart local v7    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v12

    float-to-double v12, v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    cmpg-double v12, v12, v14

    if-gez v12, :cond_9

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v12, :cond_9

    .line 836
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v12, v12

    invoke-virtual {v7, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 837
    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v13, v14

    invoke-virtual {v7, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 838
    invoke-virtual/range {p2 .. p2}, Landroid/util/SparseArray;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v7}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    goto/16 :goto_8

    .line 674
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 770
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 788
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 819
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_f
        :pswitch_10
        :pswitch_10
        :pswitch_10
    .end packed-switch

    .line 744
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private populateChartDataWithSummary(Ljava/util/List;Landroid/util/SparseArray;JIII)V
    .locals 17
    .param p3, "startDate"    # J
    .param p5, "toDivideTo"    # I
    .param p6, "dataType"    # I
    .param p7, "currentInterval"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;JIII)V"
        }
    .end annotation

    .prologue
    .line 858
    .local p1, "valuesPerIntervalList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;>;"
    .local p2, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    if-nez p1, :cond_1

    .line 859
    sget-object v10, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    const-string v11, "Activity value list - null"

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1033
    :cond_0
    :goto_0
    return-void

    .line 863
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v10

    if-ge v3, v10, :cond_9

    .line 864
    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 865
    .local v8, "value":Ljava/lang/Long;
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-gez v10, :cond_2

    const/4 v9, 0x0

    .line 866
    .local v9, "valueToPlotOnY":F
    :goto_2
    packed-switch p6, :pswitch_data_0

    .line 894
    :goto_3
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;

    invoke-virtual {v10}, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->getTs()J

    move-result-wide v6

    .line 896
    .local v6, "timestamp":J
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v10}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v10

    const/4 v11, 0x3

    if-eq v10, v11, :cond_6

    .line 897
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v10

    if-lez v10, :cond_4

    .line 898
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 899
    .local v5, "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    int-to-float v10, v3

    invoke-virtual {v5, v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 900
    invoke-virtual {v5, v6, v7}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    invoke-virtual {v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 901
    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v5}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 863
    .end local v5    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 865
    .end local v6    # "timestamp":J
    .end local v9    # "valueToPlotOnY":F
    :cond_2
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    long-to-float v9, v10

    goto :goto_2

    .line 868
    .restart local v9    # "valueToPlotOnY":F
    :pswitch_0
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;

    invoke-virtual {v10}, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->getCal()I

    move-result v10

    int-to-long v10, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 869
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/32 v12, 0x186a0

    div-long/2addr v10, v12

    long-to-float v9, v10

    .line 870
    goto :goto_3

    .line 873
    :pswitch_1
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;

    invoke-virtual {v10}, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->getDist()I

    move-result v10

    int-to-long v10, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 874
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 875
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/vectorwatch/android/utils/Helpers;->convertCmToKm(J)F

    move-result v9

    goto :goto_3

    .line 877
    :cond_3
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/vectorwatch/android/utils/Helpers;->convertCmToMiles(J)F

    move-result v9

    .line 879
    goto/16 :goto_3

    .line 882
    :pswitch_2
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    long-to-float v9, v10

    .line 883
    goto/16 :goto_3

    .line 886
    :pswitch_3
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;

    invoke-virtual {v10}, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->getSteps()I

    move-result v10

    int-to-long v10, v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 887
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    long-to-float v9, v10

    .line 888
    goto/16 :goto_3

    .line 903
    .restart local v6    # "timestamp":J
    :cond_4
    new-instance v4, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v10, v3

    invoke-direct {v4, v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 904
    .local v4, "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v10}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v10

    const/4 v11, 0x3

    if-ne v10, v11, :cond_5

    .line 905
    invoke-virtual {v4, v6, v7}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    invoke-virtual {v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 909
    :goto_5
    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_4

    .line 907
    :cond_5
    invoke-virtual {v4, v6, v7}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    invoke-virtual {v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    goto :goto_5

    .line 912
    .end local v4    # "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_6
    add-int/lit8 v10, v3, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v10

    if-lez v10, :cond_7

    .line 913
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 914
    .restart local v5    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    add-int/lit8 v10, v3, 0x1

    int-to-float v10, v10

    invoke-virtual {v5, v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 915
    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v6

    invoke-virtual {v5, v10, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    invoke-virtual {v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 916
    add-int/lit8 v10, v3, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v5}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_4

    .line 918
    .end local v5    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_7
    new-instance v4, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    add-int/lit8 v10, v3, 0x1

    int-to-float v10, v10

    invoke-direct {v4, v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    .line 919
    .restart local v4    # "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v10}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v10

    const/4 v11, 0x3

    if-ne v10, v11, :cond_8

    .line 920
    invoke-virtual {v4, v6, v7}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    invoke-virtual {v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 924
    :goto_6
    add-int/lit8 v10, v3, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_4

    .line 922
    :cond_8
    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v6

    invoke-virtual {v4, v10, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    invoke-virtual {v10, v9}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    goto :goto_6

    .line 929
    .end local v4    # "newPointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    .end local v6    # "timestamp":J
    .end local v8    # "value":Ljava/lang/Long;
    .end local v9    # "valueToPlotOnY":F
    :cond_9
    const/4 v10, 0x3

    move/from16 v0, p6

    if-ne v0, v10, :cond_d

    .line 930
    if-nez p7, :cond_c

    .line 931
    const/4 v10, 0x0

    new-instance v11, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v12, 0x0

    const/high16 v13, 0x42b40000    # 90.0f

    invoke-direct {v11, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v12, 0x3e8

    mul-long v12, v12, p3

    invoke-virtual {v11, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 958
    :goto_7
    const/4 v2, 0x0

    .line 959
    .local v2, "endIndex":I
    packed-switch p7, :pswitch_data_1

    .line 973
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 978
    :goto_8
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v10

    if-gez v10, :cond_a

    .line 979
    const/4 v10, 0x2

    move/from16 v0, p6

    if-ne v0, v10, :cond_17

    .line 980
    packed-switch p7, :pswitch_data_2

    .line 1007
    :cond_a
    :goto_9
    const/4 v10, 0x2

    move/from16 v0, p6

    if-ne v0, v10, :cond_0

    .line 1008
    packed-switch p7, :pswitch_data_3

    goto/16 :goto_0

    .line 1010
    :pswitch_4
    const/4 v3, 0x0

    :goto_a
    invoke-virtual/range {p2 .. p2}, Landroid/util/SparseArray;->size()I

    move-result v10

    if-ge v3, v10, :cond_0

    .line 1011
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 1012
    .restart local v5    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v10

    float-to-double v10, v10

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    div-double/2addr v12, v14

    cmpg-double v10, v10, v12

    if-gez v10, :cond_b

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_b

    .line 1013
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    div-double/2addr v10, v12

    double-to-float v10, v10

    invoke-virtual {v5, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 1014
    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v10

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    div-double/2addr v12, v14

    double-to-float v11, v12

    invoke-virtual {v5, v10, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 1015
    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v5}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 1010
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 933
    .end local v2    # "endIndex":I
    .end local v5    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    :cond_c
    const/4 v10, 0x0

    new-instance v11, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct {v11, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v12, 0x3e8

    mul-long v12, v12, p3

    invoke-virtual {v11, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    .line 935
    :cond_d
    const/4 v10, 0x2

    move/from16 v0, p6

    if-ne v0, v10, :cond_12

    .line 936
    packed-switch p7, :pswitch_data_4

    goto/16 :goto_7

    .line 938
    :pswitch_5
    const/4 v11, 0x0

    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_e

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v10, v14

    const/high16 v14, 0x42c80000    # 100.0f

    div-float/2addr v10, v14

    :goto_b
    invoke-direct {v12, v13, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    .line 939
    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    .line 938
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    :cond_e
    const/4 v10, 0x0

    goto :goto_b

    .line 942
    :pswitch_6
    const/4 v11, 0x0

    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_f

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v10, v14

    :goto_c
    invoke-direct {v12, v13, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    .line 943
    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    .line 942
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    :cond_f
    const/4 v10, 0x0

    goto :goto_c

    .line 946
    :pswitch_7
    const/4 v11, 0x0

    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_10

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v10, v14

    :goto_d
    invoke-direct {v12, v13, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    .line 947
    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    .line 946
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    :cond_10
    const/4 v10, 0x0

    goto :goto_d

    .line 950
    :pswitch_8
    const/4 v11, 0x0

    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_11

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v10, v14

    :goto_e
    invoke-direct {v12, v13, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    .line 951
    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    .line 950
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    :cond_11
    const/4 v10, 0x0

    goto :goto_e

    .line 955
    :cond_12
    const/4 v10, 0x0

    new-instance v11, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct {v11, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v12, 0x3e8

    mul-long v12, v12, p3

    invoke-virtual {v11, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_7

    .line 961
    .restart local v2    # "endIndex":I
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v11, 0xb

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 962
    goto/16 :goto_8

    .line 965
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v11, 0x7

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v10

    add-int/lit8 v2, v10, -0x1

    .line 966
    goto/16 :goto_8

    .line 969
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v10

    add-int/lit8 v2, v10, -0x1

    .line 970
    goto/16 :goto_8

    .line 982
    :pswitch_c
    const/4 v11, 0x0

    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v13, v2

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_13

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v10, v14

    const/high16 v14, 0x42c80000    # 100.0f

    div-float/2addr v10, v14

    :goto_f
    invoke-direct {v12, v13, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    .line 983
    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    .line 982
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_9

    :cond_13
    const/4 v10, 0x0

    goto :goto_f

    .line 987
    :pswitch_d
    const/4 v11, 0x0

    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v13, v2

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_14

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v10, v14

    :goto_10
    invoke-direct {v12, v13, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    .line 988
    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    .line 987
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_9

    :cond_14
    const/4 v10, 0x0

    goto :goto_10

    .line 992
    :pswitch_e
    const/4 v11, 0x0

    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v13, v2

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_15

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v10, v14

    :goto_11
    invoke-direct {v12, v13, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    .line 993
    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    .line 992
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_9

    :cond_15
    const/4 v10, 0x0

    goto :goto_11

    .line 997
    :pswitch_f
    const/4 v11, 0x0

    new-instance v12, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v13, v2

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_16

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v10, v14

    :goto_12
    invoke-direct {v12, v13, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    const-wide/16 v14, 0x3e8

    mul-long v14, v14, p3

    .line 998
    invoke-virtual {v12, v14, v15}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    .line 997
    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_9

    :cond_16
    const/4 v10, 0x0

    goto :goto_12

    .line 1002
    :cond_17
    new-instance v10, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    int-to-float v11, v2

    const/4 v12, 0x0

    invoke-direct {v10, v11, v12}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;-><init>(FF)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    .line 1003
    invoke-virtual {v11}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    .line 1002
    invoke-virtual {v10, v12, v13}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setTimestamp(J)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_9

    .line 1023
    :pswitch_10
    invoke-virtual/range {p2 .. p2}, Landroid/util/SparseArray;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 1024
    .restart local v5    # "pointValueWithTimestamp":Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;
    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v10

    float-to-double v10, v10

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    cmpg-double v10, v10, v12

    if-gez v10, :cond_0

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sHasBase:Z

    if-eqz v10, :cond_0

    .line 1025
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v10, v10

    invoke-virtual {v5, v10}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->setYValue(F)Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    .line 1026
    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getX()F

    move-result v10

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->sCaloriesBaseline:D

    double-to-float v11, v12

    invoke-virtual {v5, v10, v11}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->set(FF)Lcom/vectorwatch/android/ui/chart/model/PointValue;

    .line 1027
    invoke-virtual/range {p2 .. p2}, Landroid/util/SparseArray;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v5}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 866
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 959
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 980
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    .line 1008
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_4
        :pswitch_10
        :pswitch_10
        :pswitch_10
    .end packed-switch

    .line 936
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public fetchLoaderData()Landroid/util/SparseArray;
    .locals 67
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    const-string v10, "CHARTS_LOAD"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "fetch_data "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v12}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v12}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 93
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 94
    const/16 v20, 0x1

    .line 95
    .local v20, "toDivideTo":I
    const/16 v21, 0x0

    .line 96
    .local v21, "intervalCount":I
    new-instance v25, Landroid/util/SparseArray;

    invoke-direct/range {v25 .. v25}, Landroid/util/SparseArray;-><init>()V

    .line 98
    .local v25, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    const-wide/16 v6, 0x0

    .line 99
    .local v6, "startDate":J
    const-wide/16 v8, 0x0

    .line 101
    .local v8, "endDate":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long v8, v10, v12

    .line 102
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v66

    .line 103
    .local v66, "tz":Ljava/util/TimeZone;
    new-instance v63, Ljava/util/Date;

    invoke-direct/range {v63 .. v63}, Ljava/util/Date;-><init>()V

    .line 104
    .local v63, "now":Ljava/util/Date;
    invoke-virtual/range {v63 .. v63}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    move-object/from16 v0, v66

    invoke-virtual {v0, v10, v11}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    div-int/lit16 v14, v4, 0x3e8

    .line 106
    .local v14, "offsetFromUtc":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 305
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    const/4 v10, 0x3

    if-ne v4, v10, :cond_18

    .line 306
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v33

    move-object/from16 v32, p0

    move-wide/from16 v34, v6

    move-wide/from16 v36, v8

    invoke-direct/range {v32 .. v37}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getSleepForInterval(Landroid/content/ContentResolver;JJ)Ljava/util/List;

    move-result-object v33

    .line 307
    .local v33, "sleepInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v4

    if-nez v4, :cond_1

    .line 308
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 309
    const/4 v4, 0x0

    move-object/from16 v0, v33

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStart()I

    move-result v4

    add-int/lit16 v4, v4, -0xe10

    add-int/lit16 v4, v4, -0x5dc

    int-to-long v6, v4

    .line 310
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, v33

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/SleepInfo;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/SleepInfo;->getSleepStop()I

    move-result v4

    add-int/lit16 v4, v4, 0x1c20

    add-int/lit16 v4, v4, -0x7d0

    int-to-long v8, v4

    .line 311
    sub-long v10, v8, v6

    const-wide/16 v12, 0xe10

    div-long/2addr v10, v12

    long-to-int v4, v10

    mul-int/lit8 v21, v4, 0x4

    .line 315
    :cond_1
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v5

    .line 316
    .local v5, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v4

    invoke-virtual/range {v4 .. v9}, Lcom/vectorwatch/android/database/DatabaseManager;->getActivity(Lio/realm/Realm;JJ)Ljava/util/List;

    move-result-object v17

    .line 317
    .local v17, "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "get_activity: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 318
    new-instance v36, Ljava/util/ArrayList;

    invoke-direct/range {v36 .. v36}, Ljava/util/ArrayList;-><init>()V

    .line 320
    .local v36, "valuesPerIntervalList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    .line 370
    :cond_2
    :goto_1
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 420
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    .end local v25    # "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    .end local v33    # "sleepInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    .end local v36    # "valuesPerIntervalList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    :goto_2
    :pswitch_0
    return-object v25

    .line 108
    .restart local v25    # "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    :pswitch_1
    const/16 v20, 0x384

    .line 109
    const/16 v21, 0x60

    .line 110
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xc

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 111
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xd

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 112
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v11}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v11

    mul-int/lit8 v11, v11, -0x1

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->add(II)V

    .line 113
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v4

    if-eqz v4, :cond_4

    .line 114
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    const/4 v10, 0x3

    if-eq v4, v10, :cond_3

    .line 115
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xb

    const/16 v11, 0x18

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 121
    :cond_3
    :goto_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v52

    .line 122
    .local v52, "cal":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    move-object/from16 v0, v52

    invoke-virtual {v0, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 124
    new-instance v59, Ljava/text/SimpleDateFormat;

    const-string v4, "E - dd"

    move-object/from16 v0, v59

    invoke-direct {v0, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 125
    .local v59, "formater":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v0, v59

    move-object/from16 v1, v52

    invoke-interface {v4, v0, v1}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setInterval(Ljava/text/SimpleDateFormat;Ljava/util/Calendar;)V

    .line 126
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    int-to-long v12, v14

    sub-long v8, v10, v12

    .line 128
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    const/4 v10, 0x3

    if-ne v4, v10, :cond_5

    .line 129
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v53

    .line 130
    .local v53, "calSleep":Ljava/util/Calendar;
    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v10}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v10

    mul-int/lit8 v10, v10, -0x1

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v10}, Ljava/util/Calendar;->add(II)V

    .line 131
    const/16 v4, 0xb

    const/16 v10, 0x11

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v10}, Ljava/util/Calendar;->set(II)V

    .line 132
    invoke-virtual/range {v53 .. v53}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long v6, v10, v12

    .line 144
    .end local v53    # "calSleep":Ljava/util/Calendar;
    :goto_4
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "YEAR=========start "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 145
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "YEAR=========end "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 147
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    const/4 v10, 0x3

    if-eq v4, v10, :cond_0

    .line 148
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v5

    .line 149
    .restart local v5    # "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v4

    invoke-virtual/range {v4 .. v9}, Lcom/vectorwatch/android/database/DatabaseManager;->getActivity(Lio/realm/Realm;JJ)Ljava/util/List;

    move-result-object v17

    .line 150
    .restart local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "get_activity: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 151
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v4

    const/16 v10, 0x5a

    if-ge v4, v10, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    if-eqz v4, :cond_7

    .line 152
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const-string v11, "DAY"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCallback:Lretrofit/Callback;

    move-wide v12, v8

    invoke-static/range {v10 .. v15}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getActivityFromCloud(Landroid/content/Context;Ljava/lang/String;JILretrofit/Callback;)V

    .line 153
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 154
    const/16 v25, 0x0

    goto/16 :goto_2

    .line 118
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    .end local v52    # "cal":Ljava/util/Calendar;
    .end local v59    # "formater":Ljava/text/SimpleDateFormat;
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xa

    const/4 v11, 0x3

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->add(II)V

    goto/16 :goto_3

    .line 134
    .restart local v52    # "cal":Ljava/util/Calendar;
    .restart local v59    # "formater":Ljava/text/SimpleDateFormat;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v4

    if-nez v4, :cond_6

    .line 135
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v54

    .line 136
    .local v54, "calStart":Ljava/util/Calendar;
    const/16 v4, 0xb

    const/4 v10, 0x0

    move-object/from16 v0, v54

    invoke-virtual {v0, v4, v10}, Ljava/util/Calendar;->set(II)V

    .line 137
    const/16 v4, 0xc

    const/4 v10, 0x0

    move-object/from16 v0, v54

    invoke-virtual {v0, v4, v10}, Ljava/util/Calendar;->set(II)V

    .line 138
    invoke-virtual/range {v54 .. v54}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long v6, v10, v12

    .line 139
    goto/16 :goto_4

    .line 140
    .end local v54    # "calStart":Ljava/util/Calendar;
    :cond_6
    const-wide/32 v10, 0x15180

    sub-long v6, v8, v10

    goto/16 :goto_4

    .restart local v5    # "realm":Lio/realm/Realm;
    .restart local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    :cond_7
    move-object/from16 v16, p0

    move-wide/from16 v18, v6

    .line 156
    invoke-direct/range {v16 .. v21}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getActivityValuesPerInterval(Ljava/util/List;JII)Ljava/util/List;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v29

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v30

    move-object/from16 v23, p0

    move-wide/from16 v26, v6

    move/from16 v28, v20

    invoke-direct/range {v23 .. v30}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartData(Ljava/util/List;Landroid/util/SparseArray;JIII)V

    .line 157
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    goto/16 :goto_0

    .line 161
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    .end local v52    # "cal":Ljava/util/Calendar;
    .end local v59    # "formater":Ljava/text/SimpleDateFormat;
    :pswitch_2
    const v20, 0x15180

    .line 162
    const/16 v21, 0x8

    .line 164
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const-string v10, "UTC"

    invoke-static {v10}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x2

    invoke-virtual {v4, v10}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 166
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xb

    const/16 v11, 0x17

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 167
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xc

    const/16 v11, 0x3b

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 168
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xd

    const/16 v11, 0x3b

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 169
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v11}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v11

    mul-int/lit8 v11, v11, -0x1

    mul-int/lit8 v11, v11, 0x7

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->add(II)V

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v4

    if-eqz v4, :cond_8

    .line 171
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x7

    const/4 v11, 0x1

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 173
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    int-to-long v12, v14

    sub-long v8, v10, v12

    .line 175
    const-wide/32 v10, 0xa8c00

    sub-long v10, v8, v10

    const-wide/16 v12, 0x1

    add-long v6, v10, v12

    .line 176
    const-wide/16 v10, 0x3e8

    mul-long v28, v6, v10

    .line 177
    .local v28, "fStartDate":J
    const-wide/16 v10, 0x3e8

    mul-long v30, v8, v10

    .line 178
    .local v30, "fEndDate":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    move-object/from16 v27, v0

    invoke-interface/range {v26 .. v31}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setInterval(Ljava/util/Calendar;JJ)V

    .line 180
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "YEAR=========start "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 181
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "YEAR=========end "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 183
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    const/4 v10, 0x3

    if-eq v4, v10, :cond_b

    .line 184
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v5

    .line 185
    .restart local v5    # "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v4

    invoke-virtual/range {v4 .. v9}, Lcom/vectorwatch/android/database/DatabaseManager;->getActivity(Lio/realm/Realm;JJ)Ljava/util/List;

    move-result-object v17

    .line 187
    .restart local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "get_activity: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 189
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v4

    const/16 v10, 0x276

    if-ge v4, v10, :cond_9

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    if-eqz v4, :cond_9

    .line 190
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const-string v11, "WEEK"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCallback:Lretrofit/Callback;

    move-wide v12, v8

    invoke-static/range {v10 .. v15}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getActivityFromCloud(Landroid/content/Context;Ljava/lang/String;JILretrofit/Callback;)V

    .line 191
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 192
    const/16 v25, 0x0

    goto/16 :goto_2

    .line 195
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    if-eqz v4, :cond_a

    move-object/from16 v16, p0

    move-wide/from16 v18, v6

    .line 196
    invoke-direct/range {v16 .. v21}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getActivityValuesPerInterval(Ljava/util/List;JII)Ljava/util/List;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v40

    move-object/from16 v33, p0

    move-object/from16 v35, v25

    move-wide/from16 v36, v6

    move/from16 v38, v20

    invoke-direct/range {v33 .. v40}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartData(Ljava/util/List;Landroid/util/SparseArray;JIII)V

    .line 200
    :goto_5
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    goto/16 :goto_0

    .line 198
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSummary:Ljava/util/List;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v40

    move-object/from16 v33, p0

    move-object/from16 v35, v25

    move-wide/from16 v36, v6

    move/from16 v38, v20

    invoke-direct/range {v33 .. v40}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartDataWithSummary(Ljava/util/List;Landroid/util/SparseArray;JIII)V

    goto :goto_5

    .line 202
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    if-eqz v4, :cond_c

    .line 203
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const-string v11, "WEEK"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCallback:Lretrofit/Callback;

    move-wide v12, v8

    invoke-static/range {v10 .. v15}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getActivityFromCloud(Landroid/content/Context;Ljava/lang/String;JILretrofit/Callback;)V

    .line 204
    const/16 v25, 0x0

    goto/16 :goto_2

    .line 206
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSummary:Ljava/util/List;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v40

    move-object/from16 v33, p0

    move-object/from16 v35, v25

    move-wide/from16 v36, v6

    move/from16 v38, v20

    invoke-direct/range {v33 .. v40}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartDataWithSummary(Ljava/util/List;Landroid/util/SparseArray;JIII)V

    goto/16 :goto_0

    .line 211
    .end local v28    # "fStartDate":J
    .end local v30    # "fEndDate":J
    :pswitch_3
    const v20, 0x15180

    .line 212
    const/16 v21, 0x1e

    .line 214
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Month actual maximum count days = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const-string v10, "UTC"

    invoke-static {v10}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 217
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xb

    const/16 v11, 0x17

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 218
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xc

    const/16 v11, 0x3b

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 219
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xd

    const/16 v11, 0x3b

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 220
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v11}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v11

    mul-int/lit8 v11, v11, -0x1

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->add(II)V

    .line 221
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v4

    if-eqz v4, :cond_d

    .line 222
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v12, 0x5

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v11

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 225
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    int-to-long v12, v14

    sub-long v8, v10, v12

    .line 226
    int-to-long v10, v14

    sub-long/2addr v8, v10

    .line 228
    const-wide/32 v10, 0x278d00

    sub-long v6, v8, v10

    .line 229
    new-instance v60, Ljava/text/SimpleDateFormat;

    const-string v4, "MMM yyyy"

    move-object/from16 v0, v60

    invoke-direct {v0, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 230
    .local v60, "formater2":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    move-object/from16 v0, v60

    invoke-interface {v4, v0, v10}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setInterval(Ljava/text/SimpleDateFormat;Ljava/util/Calendar;)V

    .line 232
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "YEAR=========start "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 233
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "YEAR=========end "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 235
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    const/4 v10, 0x3

    if-eq v4, v10, :cond_f

    .line 236
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v5

    .line 237
    .restart local v5    # "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v4

    invoke-virtual/range {v4 .. v9}, Lcom/vectorwatch/android/database/DatabaseManager;->getActivity(Lio/realm/Realm;JJ)Ljava/util/List;

    move-result-object v17

    .line 238
    .restart local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "get_activity: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 239
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v4

    const/16 v10, 0x49d4

    if-ge v4, v10, :cond_e

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    if-eqz v4, :cond_e

    .line 240
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const-string v11, "MONTH"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCallback:Lretrofit/Callback;

    move-wide v12, v8

    invoke-static/range {v10 .. v15}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getActivityFromCloud(Landroid/content/Context;Ljava/lang/String;JILretrofit/Callback;)V

    .line 241
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 242
    const/16 v25, 0x0

    goto/16 :goto_2

    .line 244
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    if-eqz v4, :cond_10

    move-object/from16 v16, p0

    move-wide/from16 v18, v6

    .line 245
    invoke-direct/range {v16 .. v21}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getActivityValuesPerInterval(Ljava/util/List;JII)Ljava/util/List;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v40

    move-object/from16 v33, p0

    move-object/from16 v35, v25

    move-wide/from16 v36, v6

    move/from16 v38, v20

    invoke-direct/range {v33 .. v40}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartData(Ljava/util/List;Landroid/util/SparseArray;JIII)V

    .line 249
    :goto_6
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 252
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    :cond_f
    const/16 v22, 0x0

    .line 253
    .local v22, "best":F
    const/16 v62, 0x0

    .local v62, "i":I
    :goto_7
    invoke-virtual/range {v25 .. v25}, Landroid/util/SparseArray;->size()I

    move-result v4

    move/from16 v0, v62

    if-ge v0, v4, :cond_0

    .line 254
    move-object/from16 v0, v25

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v4

    move/from16 v0, v22

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v22

    .line 253
    add-int/lit8 v62, v62, 0x1

    goto :goto_7

    .line 247
    .end local v22    # "best":F
    .end local v62    # "i":I
    .restart local v5    # "realm":Lio/realm/Realm;
    .restart local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSummary:Ljava/util/List;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v40

    move-object/from16 v33, p0

    move-object/from16 v35, v25

    move-wide/from16 v36, v6

    move/from16 v38, v20

    invoke-direct/range {v33 .. v40}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartDataWithSummary(Ljava/util/List;Landroid/util/SparseArray;JIII)V

    goto :goto_6

    .line 259
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    .end local v60    # "formater2":Ljava/text/SimpleDateFormat;
    :pswitch_4
    const v20, 0x278d00

    .line 260
    const/16 v21, 0xd

    .line 262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const-string v10, "UTC"

    invoke-static {v10}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 263
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xb

    const/16 v11, 0x17

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 264
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xc

    const/16 v11, 0x3b

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 265
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/16 v10, 0xd

    const/16 v11, 0x3b

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 266
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v11}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v11

    mul-int/lit8 v11, v11, -0x1

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->add(II)V

    .line 267
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getNumberOfPagesScrolled()I

    move-result v4

    if-eqz v4, :cond_11

    .line 268
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 269
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->add(II)V

    .line 270
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    const/4 v10, 0x6

    const/4 v11, 0x1

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->set(II)V

    .line 272
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    int-to-long v12, v14

    sub-long v8, v10, v12

    .line 273
    int-to-long v10, v14

    sub-long/2addr v8, v10

    .line 275
    const-wide/32 v10, 0x1e13380

    sub-long v6, v8, v10

    .line 277
    new-instance v61, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy"

    move-object/from16 v0, v61

    invoke-direct {v0, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 279
    .local v61, "formater3":Ljava/text/SimpleDateFormat;
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "YEAR=========start "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 280
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "YEAR=========end "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 282
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    const/4 v10, 0x3

    if-eq v4, v10, :cond_13

    .line 283
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v5

    .line 284
    .restart local v5    # "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v4

    invoke-virtual/range {v4 .. v9}, Lcom/vectorwatch/android/database/DatabaseManager;->getActivity(Lio/realm/Realm;JJ)Ljava/util/List;

    move-result-object v17

    .line 285
    .restart local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "get_activity: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 286
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v4

    const v10, 0x375f0

    if-ge v4, v10, :cond_12

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    if-eqz v4, :cond_12

    .line 287
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const-string v11, "YEAR"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCallback:Lretrofit/Callback;

    move-wide v12, v8

    invoke-static/range {v10 .. v15}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getActivityFromCloud(Landroid/content/Context;Ljava/lang/String;JILretrofit/Callback;)V

    .line 288
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 289
    const/16 v25, 0x0

    goto/16 :goto_2

    .line 291
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    if-eqz v4, :cond_14

    move-object/from16 v16, p0

    move-wide/from16 v18, v6

    .line 292
    invoke-direct/range {v16 .. v21}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getActivityValuesPerInterval(Ljava/util/List;JII)Ljava/util/List;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v40

    move-object/from16 v33, p0

    move-object/from16 v35, v25

    move-wide/from16 v36, v6

    move/from16 v38, v20

    invoke-direct/range {v33 .. v40}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartData(Ljava/util/List;Landroid/util/SparseArray;JIII)V

    .line 296
    :goto_8
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 298
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCalendar:Ljava/util/Calendar;

    move-object/from16 v0, v61

    invoke-interface {v4, v0, v10}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setInterval(Ljava/text/SimpleDateFormat;Ljava/util/Calendar;)V

    goto/16 :goto_0

    .line 294
    .restart local v5    # "realm":Lio/realm/Realm;
    .restart local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSummary:Ljava/util/List;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v39

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v40

    move-object/from16 v33, p0

    move-object/from16 v35, v25

    move-wide/from16 v36, v6

    move/from16 v38, v20

    invoke-direct/range {v33 .. v40}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartDataWithSummary(Ljava/util/List;Landroid/util/SparseArray;JIII)V

    goto :goto_8

    .line 322
    .end local v61    # "formater3":Ljava/text/SimpleDateFormat;
    .restart local v33    # "sleepInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    .restart local v36    # "valuesPerIntervalList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    :pswitch_5
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v4

    const/16 v10, 0x5a

    if-ge v4, v10, :cond_16

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    if-eqz v4, :cond_16

    if-eqz v33, :cond_15

    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_16

    .line 323
    :cond_15
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const-string v11, "DAY"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mCallback:Lretrofit/Callback;

    move-wide v12, v8

    invoke-static/range {v10 .. v15}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getActivityFromCloud(Landroid/content/Context;Ljava/lang/String;JILretrofit/Callback;)V

    .line 324
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 325
    const/16 v25, 0x0

    goto/16 :goto_2

    .line 327
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSleep:Ljava/util/List;

    if-eqz v4, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSleep:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_17

    .line 328
    new-instance v33, Ljava/util/ArrayList;

    .end local v33    # "sleepInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 329
    .restart local v33    # "sleepInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    const/16 v62, 0x0

    .restart local v62    # "i":I
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSleep:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v62

    if-ge v0, v4, :cond_17

    .line 330
    new-instance v10, Lcom/vectorwatch/android/models/SleepInfo;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSleep:Ljava/util/List;

    move/from16 v0, v62

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getId()Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->getStartTs()J

    move-result-wide v12

    const-wide/16 v18, 0x3e8

    div-long v12, v12, v18

    long-to-int v11, v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSleep:Ljava/util/List;

    move/from16 v0, v62

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getEndTs()J

    move-result-wide v12

    const-wide/16 v18, 0x3e8

    div-long v12, v12, v18

    long-to-int v12, v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSleep:Ljava/util/List;

    move/from16 v0, v62

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getId()Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->getStartTs()J

    move-result-wide v18

    const-wide/16 v26, 0x3e8

    div-long v18, v18, v26

    move-wide/from16 v0, v18

    long-to-int v4, v0

    invoke-direct {v10, v11, v12, v4}, Lcom/vectorwatch/android/models/SleepInfo;-><init>(III)V

    move-object/from16 v0, v33

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    add-int/lit8 v62, v62, 0x1

    goto :goto_9

    .end local v62    # "i":I
    :cond_17
    move-object/from16 v32, p0

    move-wide/from16 v34, v6

    move-wide/from16 v36, v8

    move/from16 v38, v20

    move/from16 v39, v21

    move-object/from16 v40, v17

    .line 334
    invoke-direct/range {v32 .. v40}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getActivityValuesForSleep(Ljava/util/List;JJIILjava/util/List;)Ljava/util/List;

    .end local v36    # "valuesPerIntervalList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    move-result-object v36

    .line 336
    .restart local v36    # "valuesPerIntervalList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    const/16 v41, 0x0

    move-object/from16 v35, p0

    move-object/from16 v37, v25

    move-wide/from16 v38, v6

    move/from16 v40, v20

    invoke-direct/range {v35 .. v41}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartData(Ljava/util/List;Landroid/util/SparseArray;JII)V

    goto/16 :goto_1

    :pswitch_6
    move-object/from16 v38, p0

    move-object/from16 v39, v33

    move-wide/from16 v40, v6

    move-wide/from16 v42, v8

    move/from16 v44, v20

    move/from16 v45, v21

    move-object/from16 v46, v17

    .line 340
    invoke-direct/range {v38 .. v46}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getActivityValuesForSleep(Ljava/util/List;JJIILjava/util/List;)Ljava/util/List;

    move-result-object v36

    .line 342
    const/16 v41, 0x0

    move-object/from16 v35, p0

    move-object/from16 v37, v25

    move-wide/from16 v38, v6

    move/from16 v40, v20

    invoke-direct/range {v35 .. v41}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartData(Ljava/util/List;Landroid/util/SparseArray;JII)V

    goto/16 :goto_1

    :pswitch_7
    move-object/from16 v38, p0

    move-object/from16 v39, v33

    move-wide/from16 v40, v6

    move-wide/from16 v42, v8

    move/from16 v44, v20

    move/from16 v45, v21

    move-object/from16 v46, v17

    .line 346
    invoke-direct/range {v38 .. v46}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getActivityValuesForSleep(Ljava/util/List;JJIILjava/util/List;)Ljava/util/List;

    move-result-object v36

    .line 348
    const/16 v41, 0x0

    move-object/from16 v35, p0

    move-object/from16 v37, v25

    move-wide/from16 v38, v6

    move/from16 v40, v20

    invoke-direct/range {v35 .. v41}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartData(Ljava/util/List;Landroid/util/SparseArray;JII)V

    goto/16 :goto_1

    .line 352
    :pswitch_8
    move-wide/from16 v40, v6

    .line 353
    .local v40, "currentStart":J
    const-wide/32 v10, 0x278d00

    add-long v42, v6, v10

    .line 354
    .local v42, "currentEnd":J
    const/16 v51, 0x0

    .line 355
    .local v51, "currentInterval":I
    const v20, 0x15180

    .line 356
    :goto_a
    cmp-long v4, v40, v8

    if-gez v4, :cond_2

    .line 357
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v39

    move-object/from16 v38, p0

    invoke-direct/range {v38 .. v43}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getSleepForInterval(Landroid/content/ContentResolver;JJ)Ljava/util/List;

    move-result-object v39

    .line 359
    .local v39, "sleepInfoList2":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    const/16 v21, 0x1e

    move-object/from16 v38, p0

    move/from16 v44, v20

    move/from16 v45, v21

    move-object/from16 v46, v17

    .line 360
    invoke-direct/range {v38 .. v46}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->getActivityValuesForSleep(Ljava/util/List;JJIILjava/util/List;)Ljava/util/List;

    move-result-object v36

    move-object/from16 v45, p0

    move-object/from16 v46, v36

    move-object/from16 v47, v25

    move-wide/from16 v48, v40

    move/from16 v50, v20

    .line 362
    invoke-direct/range {v45 .. v51}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->populateChartData(Ljava/util/List;Landroid/util/SparseArray;JII)V

    .line 363
    move-wide/from16 v40, v42

    .line 364
    const-wide/32 v10, 0x278d00

    add-long v42, v42, v10

    .line 365
    add-int/lit8 v51, v51, 0x1

    .line 366
    goto :goto_a

    .line 372
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v17    # "activityDayList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    .end local v33    # "sleepInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    .end local v36    # "valuesPerIntervalList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .end local v39    # "sleepInfoList2":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepInfo;>;"
    .end local v40    # "currentStart":J
    .end local v42    # "currentEnd":J
    .end local v51    # "currentInterval":I
    :cond_18
    invoke-static {}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object v57

    .line 373
    .local v57, "format":Ljava/text/DecimalFormat;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v4

    packed-switch v4, :pswitch_data_2

    goto/16 :goto_2

    .line 380
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    if-eqz v4, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    const/4 v10, 0x1

    if-ne v4, v10, :cond_1d

    .line 381
    :cond_19
    const/16 v22, 0x0

    .line 382
    .restart local v22    # "best":F
    const/16 v62, 0x0

    .restart local v62    # "i":I
    :goto_b
    invoke-virtual/range {v25 .. v25}, Landroid/util/SparseArray;->size()I

    move-result v4

    move/from16 v0, v62

    if-ge v0, v4, :cond_1a

    .line 383
    move-object/from16 v0, v25

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v4

    move/from16 v0, v22

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v22

    .line 382
    add-int/lit8 v62, v62, 0x1

    goto :goto_b

    .line 385
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v4}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v4

    const/4 v10, 0x1

    if-ne v4, v10, :cond_1c

    .line 387
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 388
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const v10, 0x7f090146

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v55

    .line 392
    .local v55, "distanceInd2":Ljava/lang/String;
    :goto_c
    sget-object v4, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "SET_TOTAL: W/M "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v55

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v4, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 393
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const v11, 0x7f090073

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v57

    move/from16 v1, v22

    move-object/from16 v2, v55

    invoke-interface {v4, v0, v1, v2, v10}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 390
    .end local v55    # "distanceInd2":Ljava/lang/String;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const v10, 0x7f0901ab

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v55

    .restart local v55    # "distanceInd2":Ljava/lang/String;
    goto :goto_c

    .line 395
    .end local v55    # "distanceInd2":Ljava/lang/String;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const v10, 0x7f090235

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v55

    .line 396
    .restart local v55    # "distanceInd2":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const v11, 0x7f090073

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v57

    move/from16 v1, v22

    move-object/from16 v2, v55

    invoke-interface {v4, v0, v1, v2, v10}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 399
    .end local v22    # "best":F
    .end local v55    # "distanceInd2":Ljava/lang/String;
    .end local v62    # "i":I
    :cond_1d
    const-wide/16 v64, 0x0

    .line 400
    .local v64, "total2":J
    const/16 v62, 0x0

    .restart local v62    # "i":I
    :goto_d
    invoke-virtual/range {v25 .. v25}, Landroid/util/SparseArray;->size()I

    move-result v4

    move/from16 v0, v62

    if-ge v0, v4, :cond_1e

    .line 401
    move-wide/from16 v0, v64

    long-to-float v10, v0

    move-object/from16 v0, v25

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v4

    add-float/2addr v4, v10

    float-to-long v0, v4

    move-wide/from16 v64, v0

    .line 400
    add-int/lit8 v62, v62, 0x1

    goto :goto_d

    .line 404
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    move-wide/from16 v0, v64

    long-to-float v10, v0

    const-string v11, ""

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const v13, 0x7f090277

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v57

    invoke-interface {v4, v0, v10, v11, v12}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 409
    .end local v62    # "i":I
    .end local v64    # "total2":J
    :pswitch_a
    const/16 v22, 0x0

    .line 410
    .restart local v22    # "best":F
    const/16 v62, 0x0

    .restart local v62    # "i":I
    :goto_e
    invoke-virtual/range {v25 .. v25}, Landroid/util/SparseArray;->size()I

    move-result v4

    move/from16 v0, v62

    if-ge v0, v4, :cond_1f

    .line 411
    move-object/from16 v0, v25

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;->getY()F

    move-result v4

    move/from16 v0, v22

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v22

    .line 410
    add-int/lit8 v62, v62, 0x1

    goto :goto_e

    .line 413
    :cond_1f
    move/from16 v56, v22

    .line 414
    .local v56, "finalValue3":F
    invoke-static {}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorDecimalFormat()Ljava/text/DecimalFormat;

    move-result-object v58

    .line 415
    .local v58, "format3":Ljava/text/DecimalFormat;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    const-string v10, ""

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mContext:Landroid/content/Context;

    const v12, 0x7f090073

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-interface {v4, v0, v1, v10, v11}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setTotal(Ljava/text/DecimalFormat;FLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 320
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 373
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_0
        :pswitch_9
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method protected onLoadInBackground()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->fetchLoaderData()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onLoadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->onLoadInBackground()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method public setParameters(ZLjava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1, "goToCloud"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudSleep;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p2, "summary":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;>;"
    .local p3, "sleep":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudSleep;>;"
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mGoToCloud:Z

    .line 82
    iput-object p2, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSummary:Ljava/util/List;

    .line 83
    iput-object p3, p0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->mSleep:Ljava/util/List;

    .line 84
    return-void
.end method
