.class public Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;
.super Ljava/lang/Object;
.source "ChartInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractor;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractor;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/util/SparseArray",
        "<",
        "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
        ">;>;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p2, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    .line 44
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mContext:Landroid/content/Context;

    .line 45
    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 3
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 150
    sget-object v0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get_activity_from_cloud: fail "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setNoData()V

    .line 152
    return-void
.end method

.method public fetchData(ZLjava/util/List;Ljava/util/List;)V
    .locals 6
    .param p1, "goToCloud"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudSleep;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "summary":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;>;"
    .local p3, "sleep":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudSleep;>;"
    const/4 v5, 0x0

    .line 73
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v0

    .line 74
    .local v0, "hasPersonalInfo":Z
    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v3}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    :cond_0
    if-nez v0, :cond_2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    .line 75
    invoke-interface {v3}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataType()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 76
    :cond_1
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v3}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->setNoPersonalData()V

    .line 91
    :goto_0
    return-void

    .line 80
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/view/ChartActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    .line 81
    .local v2, "loaderManager":Landroid/support/v4/app/LoaderManager;
    invoke-virtual {v2, v5}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v1

    .line 82
    .local v1, "loader":Landroid/support/v4/content/Loader;
    if-eqz v1, :cond_3

    .line 83
    invoke-virtual {v1}, Landroid/support/v4/content/Loader;->abandon()V

    .line 84
    invoke-virtual {v1}, Landroid/support/v4/content/Loader;->reset()V

    move-object v3, v1

    .line 85
    check-cast v3, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;

    invoke-virtual {v3, p1, p2, p3}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;->setParameters(ZLjava/util/List;Ljava/util/List;)V

    .line 86
    invoke-virtual {v1}, Landroid/support/v4/content/Loader;->onContentChanged()V

    .line 87
    invoke-virtual {v1}, Landroid/support/v4/content/Loader;->startLoading()V

    goto :goto_0

    .line 89
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v2, v5, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/vectorwatch/android/ui/chart/interactor/GraphDataLoader;-><init>(Landroid/content/Context;Lretrofit/Callback;Lio/realm/Realm;Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;>;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;>;"
    .local p2, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;"
    if-eqz p2, :cond_0

    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/util/SparseArray;)V

    .line 57
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p2, Landroid/util/SparseArray;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/util/SparseArray;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/util/SparseArray<Lcom/vectorwatch/android/ui/chart/model/PointValueWithTimestamp;>;>;"
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 96
    return-void
.end method

.method public success(Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;Lretrofit/client/Response;)V
    .locals 9
    .param p1, "cloudActivityResponse"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    const/4 v8, 0x0

    .line 106
    iget-object v5, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;

    invoke-interface {v5}, Lcom/vectorwatch/android/ui/chart/presenter/ChartPresenter$Interactor;->getDataInterval()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 138
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->getData()Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->getActivity()Lcom/vectorwatch/android/ui/chart/model/CloudActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->getDataSummary()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->getData()Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->getSleep()Ljava/util/List;

    move-result-object v6

    invoke-virtual {p0, v8, v5, v6}, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->fetchData(ZLjava/util/List;Ljava/util/List;)V

    .line 141
    :goto_0
    return-void

    .line 108
    :pswitch_0
    sget-object v5, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_activity_from_cloud: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->getData()Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->getActivity()Lcom/vectorwatch/android/ui/chart/model/CloudActivity;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->getDataPointsForDays()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " activity from cloud size"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 109
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v4

    .line 110
    .local v4, "realm":Lio/realm/Realm;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v1, "activityDayRealmList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->getData()Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->getActivity()Lcom/vectorwatch/android/ui/chart/model/CloudActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->getDataPointsForDays()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 112
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->getData()Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->getActivity()Lcom/vectorwatch/android/ui/chart/model/CloudActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->getDataPointsForDays()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;

    .line 113
    .local v3, "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;
    new-instance v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-direct {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;-><init>()V

    .line 114
    .local v0, "activDayRealmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->getAvgAmpl()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setAvgAmpl(I)V

    .line 115
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->getAvgPeriod()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setAvgPeriod(I)V

    .line 116
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->getCal()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setCal(I)V

    .line 117
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->getDist()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDist(I)V

    .line 118
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->getEffTime()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setEffTime(I)V

    .line 119
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->getOffset()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setOffset(I)V

    .line 120
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->getId()Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->getTs()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setTimestamp(J)V

    .line 121
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->getTimezone()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setTimezone(I)V

    .line 122
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->getVal()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setVal(I)V

    .line 123
    sget-object v5, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->ACTIVITY:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    invoke-virtual {v5}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setActivityType(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v0, v8}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyCloud(Z)V

    .line 126
    invoke-virtual {v0, v8}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyCalories(Z)V

    .line 127
    invoke-virtual {v0, v8}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyDistance(Z)V

    .line 128
    invoke-virtual {v0, v8}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtySteps(Z)V

    .line 130
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 132
    .end local v0    # "activDayRealmObject":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .end local v3    # "obj":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v5

    invoke-virtual {v5, v4, v1}, Lcom/vectorwatch/android/database/DatabaseManager;->insertActivityDataList(Lio/realm/Realm;Ljava/util/List;)V

    .line 133
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->getData()Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->getSleep()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/vectorwatch/android/database/DatabaseManager;->insertSleepList(Landroid/content/Context;Ljava/util/List;)V

    .line 134
    invoke-virtual {v4}, Lio/realm/Realm;->close()V

    .line 135
    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->getData()Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->getSleep()Ljava/util/List;

    move-result-object v6

    invoke-virtual {p0, v8, v5, v6}, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->fetchData(ZLjava/util/List;Ljava/util/List;)V

    goto/16 :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/chart/interactor/ChartInteractorImpl;->success(Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;Lretrofit/client/Response;)V

    return-void
.end method
