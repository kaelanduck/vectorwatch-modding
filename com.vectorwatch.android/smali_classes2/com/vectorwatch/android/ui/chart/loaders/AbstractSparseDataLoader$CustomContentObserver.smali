.class public Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;
.super Landroid/database/ContentObserver;
.source "AbstractSparseDataLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CustomContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;
    .param p2, "h"    # Landroid/os/Handler;

    .prologue
    .line 140
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>.CustomContentObserver;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;->this$0:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;

    .line 141
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 142
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .prologue
    .line 147
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>.CustomContentObserver;"
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 148
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;->this$0:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->onContentChanged()V

    .line 149
    return-void
.end method
