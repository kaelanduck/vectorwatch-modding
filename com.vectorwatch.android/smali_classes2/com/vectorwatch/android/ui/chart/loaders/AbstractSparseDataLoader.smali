.class public abstract Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;
.super Landroid/support/v4/content/AsyncTaskLoader;
.source "AbstractSparseDataLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Landroid/util/SparseArray",
        "<TD;>;>;"
    }
.end annotation


# instance fields
.field private data:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<TD;>;"
        }
    .end annotation
.end field

.field private mObserver:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader",
            "<TD;>.CustomContentObserver;"
        }
    .end annotation
.end field

.field private observerUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 25
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    invoke-direct {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method


# virtual methods
.method public deliverResult(Landroid/util/SparseArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    .local p1, "_data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<TD;>;"
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->isReset()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->data:Landroid/util/SparseArray;

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->onReleaseResource(Landroid/util/SparseArray;)V

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->data:Landroid/util/SparseArray;

    .line 59
    .local v0, "oldData":Landroid/util/SparseArray;, "Landroid/util/SparseArray<TD;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->data:Landroid/util/SparseArray;

    .line 61
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63
    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->data:Landroid/util/SparseArray;

    invoke-super {p0, v1}, Landroid/support/v4/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 66
    :cond_1
    if-eqz v0, :cond_2

    if-eq v0, p1, :cond_2

    .line 68
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->onReleaseResource(Landroid/util/SparseArray;)V

    .line 70
    :cond_2
    return-void
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    check-cast p1, Landroid/util/SparseArray;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->deliverResult(Landroid/util/SparseArray;)V

    return-void
.end method

.method public getObserver()Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader",
            "<TD;>.CustomContentObserver;"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->mObserver:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;

    return-object v0
.end method

.method public getObserverUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 44
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->observerUri:Landroid/net/Uri;

    return-object v0
.end method

.method public loadInBackground()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<TD;>;"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->loadInBackground()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method public onCanceled(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    .local p1, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<TD;>;"
    invoke-super {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;->onCanceled(Ljava/lang/Object;)V

    .line 33
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->onReleaseResource(Landroid/util/SparseArray;)V

    .line 34
    return-void
.end method

.method public bridge synthetic onCanceled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    check-cast p1, Landroid/util/SparseArray;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->onCanceled(Landroid/util/SparseArray;)V

    return-void
.end method

.method public onReleaseResource(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 154
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    .local p1, "data":Landroid/util/SparseArray;, "Landroid/util/SparseArray<TD;>;"
    if-eqz p1, :cond_0

    .line 156
    invoke-virtual {p1}, Landroid/util/SparseArray;->clear()V

    .line 157
    const/4 p1, 0x0

    .line 159
    :cond_0
    return-void
.end method

.method public onReset()V
    .locals 2

    .prologue
    .line 119
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    invoke-super {p0}, Landroid/support/v4/content/AsyncTaskLoader;->onReset()V

    .line 121
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->onStopLoading()V

    .line 124
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->isReset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->data:Landroid/util/SparseArray;

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->onReleaseResource(Landroid/util/SparseArray;)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->mObserver:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;

    if-eqz v0, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->mObserver:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->mObserver:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;

    .line 135
    :cond_1
    return-void
.end method

.method protected onStartLoading()V
    .locals 4

    .prologue
    .line 81
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->data:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->data:Landroid/util/SparseArray;

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->deliverResult(Landroid/util/SparseArray;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->mObserver:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->observerUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 88
    new-instance v0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;-><init>(Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->mObserver:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;

    .line 89
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->observerUri:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->mObserver:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 93
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->takeContentChanged()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->data:Landroid/util/SparseArray;

    if-nez v0, :cond_3

    .line 95
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->forceLoad()V

    .line 97
    :cond_3
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    .prologue
    .line 107
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    invoke-super {p0}, Landroid/support/v4/content/AsyncTaskLoader;->onStopLoading()V

    .line 108
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->cancelLoad()Z

    .line 109
    return-void
.end method

.method public setObserver(Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader",
            "<TD;>.CustomContentObserver;)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    .local p1, "mObserver":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>.CustomContentObserver;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->mObserver:Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader$CustomContentObserver;

    .line 114
    return-void
.end method

.method public setObserverUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "observerUri"    # Landroid/net/Uri;

    .prologue
    .line 74
    .local p0, "this":Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;, "Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader<TD;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/loaders/AbstractSparseDataLoader;->observerUri:Landroid/net/Uri;

    .line 75
    return-void
.end method
