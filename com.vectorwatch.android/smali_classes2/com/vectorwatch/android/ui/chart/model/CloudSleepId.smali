.class public Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;
.super Ljava/lang/Object;
.source "CloudSleepId.java"


# instance fields
.field private startTs:J

.field private uID:I

.field private wID:I


# direct methods
.method public constructor <init>(IIJ)V
    .locals 1
    .param p1, "wID"    # I
    .param p2, "uID"    # I
    .param p3, "startTs"    # J

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->wID:I

    .line 13
    iput p2, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->uID:I

    .line 14
    iput-wide p3, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->startTs:J

    .line 15
    return-void
.end method


# virtual methods
.method public getStartTs()J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->startTs:J

    return-wide v0
.end method

.method public getuID()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->uID:I

    return v0
.end method

.method public getwID()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->wID:I

    return v0
.end method

.method public setStartTs(J)V
    .locals 1
    .param p1, "startTs"    # J

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->startTs:J

    .line 39
    return-void
.end method

.method public setuID(I)V
    .locals 0
    .param p1, "uID"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->uID:I

    .line 31
    return-void
.end method

.method public setwID(I)V
    .locals 0
    .param p1, "wID"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->wID:I

    .line 23
    return-void
.end method
