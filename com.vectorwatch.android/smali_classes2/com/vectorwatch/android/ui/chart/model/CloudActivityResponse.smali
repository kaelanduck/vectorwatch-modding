.class public Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;
.super Ljava/lang/Object;
.source "CloudActivityResponse.java"


# instance fields
.field private data:Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "data"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->message:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->data:Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    .line 14
    return-void
.end method


# virtual methods
.method public getData()Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->data:Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->message:Ljava/lang/String;

    return-object v0
.end method

.method public setData(Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;)V
    .locals 0
    .param p1, "data"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->data:Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;

    .line 30
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;->message:Ljava/lang/String;

    .line 22
    return-void
.end method
