.class public Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;
.super Ljava/lang/Object;
.source "CloudActivitySummary.java"


# instance fields
.field private cal:I

.field private dist:I

.field private steps:I

.field private ts:J

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIIJLjava/lang/String;)V
    .locals 0
    .param p1, "steps"    # I
    .param p2, "cal"    # I
    .param p3, "dist"    # I
    .param p4, "ts"    # J
    .param p6, "type"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->steps:I

    .line 15
    iput p2, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->cal:I

    .line 16
    iput p3, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->dist:I

    .line 17
    iput-wide p4, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->ts:J

    .line 18
    iput-object p6, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->type:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public getCal()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->cal:I

    return v0
.end method

.method public getDist()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->dist:I

    return v0
.end method

.method public getSteps()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->steps:I

    return v0
.end method

.method public getTs()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->ts:J

    return-wide v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setCal(I)V
    .locals 0
    .param p1, "cal"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->cal:I

    .line 35
    return-void
.end method

.method public setDist(I)V
    .locals 0
    .param p1, "dist"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->dist:I

    .line 43
    return-void
.end method

.method public setSteps(I)V
    .locals 0
    .param p1, "steps"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->steps:I

    .line 27
    return-void
.end method

.method public setTs(J)V
    .locals 1
    .param p1, "ts"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->ts:J

    .line 51
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;->type:Ljava/lang/String;

    .line 59
    return-void
.end method
