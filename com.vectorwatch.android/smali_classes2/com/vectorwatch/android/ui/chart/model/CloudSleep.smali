.class public Lcom/vectorwatch/android/ui/chart/model/CloudSleep;
.super Ljava/lang/Object;
.source "CloudSleep.java"


# instance fields
.field private endTs:J

.field private id:Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;J)V
    .locals 0
    .param p1, "id"    # Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;
    .param p2, "endTs"    # J

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->id:Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    .line 12
    iput-wide p2, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->endTs:J

    .line 13
    return-void
.end method


# virtual methods
.method public getEndTs()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->endTs:J

    return-wide v0
.end method

.method public getId()Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->id:Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    return-object v0
.end method

.method public setEndTs(J)V
    .locals 1
    .param p1, "endTs"    # J

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->endTs:J

    .line 29
    return-void
.end method

.method public setId(Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;)V
    .locals 0
    .param p1, "id"    # Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->id:Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    .line 21
    return-void
.end method
