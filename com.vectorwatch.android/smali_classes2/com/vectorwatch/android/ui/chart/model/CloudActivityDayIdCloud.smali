.class public Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;
.super Ljava/lang/Object;
.source "CloudActivityDayIdCloud.java"


# instance fields
.field private ts:J

.field private type:Ljava/lang/String;

.field private uID:I

.field private wID:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IIJLjava/lang/String;)V
    .locals 1
    .param p1, "wID"    # I
    .param p2, "uID"    # I
    .param p3, "ts"    # J
    .param p5, "type"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->wID:I

    .line 20
    iput p2, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->uID:I

    .line 21
    iput-wide p3, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->ts:J

    .line 22
    iput-object p5, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->type:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public getTs()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->ts:J

    return-wide v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getuID()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->uID:I

    return v0
.end method

.method public getwID()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->wID:I

    return v0
.end method

.method public setTs(J)V
    .locals 1
    .param p1, "ts"    # J

    .prologue
    .line 46
    iput-wide p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->ts:J

    .line 47
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->type:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setuID(I)V
    .locals 0
    .param p1, "uID"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->uID:I

    .line 39
    return-void
.end method

.method public setwID(I)V
    .locals 0
    .param p1, "wID"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;->wID:I

    .line 31
    return-void
.end method
