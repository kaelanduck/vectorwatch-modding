.class public Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;
.super Ljava/lang/Object;
.source "CloudActivityDayCloud.java"


# instance fields
.field private avgAmpl:I

.field private avgPeriod:I

.field private cal:I

.field private dist:I

.field private effTime:I

.field private id:Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;

.field private offset:I

.field private timestamp:J

.field private timezone:I

.field private val:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(JLcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;IIIIIIII)V
    .locals 1
    .param p1, "timestamp"    # J
    .param p3, "id"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;
    .param p4, "effTime"    # I
    .param p5, "avgAmpl"    # I
    .param p6, "avgPeriod"    # I
    .param p7, "cal"    # I
    .param p8, "dist"    # I
    .param p9, "timezone"    # I
    .param p10, "val"    # I
    .param p11, "offset"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->timestamp:J

    .line 28
    iput-object p3, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->id:Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;

    .line 29
    iput p4, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->effTime:I

    .line 30
    iput p5, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->avgAmpl:I

    .line 31
    iput p6, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->avgPeriod:I

    .line 32
    iput p7, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->cal:I

    .line 33
    iput p8, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->dist:I

    .line 34
    iput p9, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->timezone:I

    .line 35
    iput p10, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->val:I

    .line 36
    iput p11, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->offset:I

    .line 37
    return-void
.end method


# virtual methods
.method public getAvgAmpl()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->avgAmpl:I

    return v0
.end method

.method public getAvgPeriod()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->avgPeriod:I

    return v0
.end method

.method public getCal()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->cal:I

    return v0
.end method

.method public getDist()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->dist:I

    return v0
.end method

.method public getEffTime()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->effTime:I

    return v0
.end method

.method public getId()Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->id:Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;

    return-object v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->offset:I

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 112
    iget-wide v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->timestamp:J

    return-wide v0
.end method

.method public getTimezone()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->timezone:I

    return v0
.end method

.method public getVal()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->val:I

    return v0
.end method

.method public setAvgAmpl(I)V
    .locals 0
    .param p1, "avgAmpl"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->avgAmpl:I

    .line 61
    return-void
.end method

.method public setAvgPeriod(I)V
    .locals 0
    .param p1, "avgPeriod"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->avgPeriod:I

    .line 69
    return-void
.end method

.method public setCal(I)V
    .locals 0
    .param p1, "cal"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->cal:I

    .line 77
    return-void
.end method

.method public setDist(I)V
    .locals 0
    .param p1, "dist"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->dist:I

    .line 85
    return-void
.end method

.method public setEffTime(I)V
    .locals 0
    .param p1, "effTime"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->effTime:I

    .line 53
    return-void
.end method

.method public setId(Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;)V
    .locals 0
    .param p1, "id"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->id:Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayIdCloud;

    .line 45
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->offset:I

    .line 109
    return-void
.end method

.method public setTimestamp(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 116
    iput-wide p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->timestamp:J

    .line 117
    return-void
.end method

.method public setTimezone(I)V
    .locals 0
    .param p1, "timezone"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->timezone:I

    .line 93
    return-void
.end method

.method public setVal(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;->val:I

    .line 101
    return-void
.end method
