.class public Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;
.super Ljava/lang/Object;
.source "CloudActivityData.java"


# instance fields
.field private activity:Lcom/vectorwatch/android/ui/chart/model/CloudActivity;

.field private sleep:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudSleep;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/vectorwatch/android/ui/chart/model/CloudActivity;)V
    .locals 0
    .param p2, "activity"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudSleep;",
            ">;",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12
    .local p1, "sleep":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudSleep;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->sleep:Ljava/util/List;

    .line 14
    iput-object p2, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->activity:Lcom/vectorwatch/android/ui/chart/model/CloudActivity;

    .line 15
    return-void
.end method


# virtual methods
.method public getActivity()Lcom/vectorwatch/android/ui/chart/model/CloudActivity;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->activity:Lcom/vectorwatch/android/ui/chart/model/CloudActivity;

    return-object v0
.end method

.method public getSleep()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudSleep;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->sleep:Ljava/util/List;

    return-object v0
.end method

.method public setActivity(Lcom/vectorwatch/android/ui/chart/model/CloudActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivity;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->activity:Lcom/vectorwatch/android/ui/chart/model/CloudActivity;

    .line 31
    return-void
.end method

.method public setSleep(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudSleep;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "sleep":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudSleep;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityData;->sleep:Ljava/util/List;

    .line 23
    return-void
.end method
