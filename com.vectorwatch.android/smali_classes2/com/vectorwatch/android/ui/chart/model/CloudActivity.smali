.class public Lcom/vectorwatch/android/ui/chart/model/CloudActivity;
.super Ljava/lang/Object;
.source "CloudActivity.java"


# instance fields
.field private dataPointsForDays:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;",
            ">;"
        }
    .end annotation
.end field

.field private dataSummary:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 12
    .local p1, "dataPointsForDays":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;>;"
    .local p2, "dataSummary":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->dataPointsForDays:Ljava/util/List;

    .line 14
    iput-object p2, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->dataSummary:Ljava/util/List;

    .line 15
    return-void
.end method


# virtual methods
.method public getDataPointsForDays()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->dataPointsForDays:Ljava/util/List;

    return-object v0
.end method

.method public getDataSummary()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->dataSummary:Ljava/util/List;

    return-object v0
.end method

.method public setDataPointsForDays(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "dataPointsForDays":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDayCloud;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->dataPointsForDays:Ljava/util/List;

    .line 23
    return-void
.end method

.method public setDataSummary(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "dataSummary":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivitySummary;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/chart/model/CloudActivity;->dataSummary:Ljava/util/List;

    .line 31
    return-void
.end method
