.class public Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "OnboardingActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;, "TT;"
    const v6, 0x7f100117

    const v5, 0x7f100116

    const v4, 0x7f100115

    const v3, 0x7f100114

    const v2, 0x7f100112

    .line 11
    const-string v1, "field \'mFirstLayout\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mFirstLayout\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mFirstLayout:Landroid/widget/RelativeLayout;

    .line 13
    const-string v1, "field \'mVideoLayout\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mVideoLayout\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoLayout:Landroid/widget/RelativeLayout;

    .line 15
    const v1, 0x7f100118

    const-string v2, "field \'mVideoView\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 16
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100118

    const-string v2, "field \'mVideoView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/malmstein/fenster/view/FensterVideoView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    .line 17
    const-string v1, "field \'mTxtWelcome\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 18
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mTxtWelcome\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mTxtWelcome:Landroid/widget/TextView;

    .line 19
    const-string v1, "field \'mBtnGetStarted\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 20
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mBtnGetStarted\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mBtnGetStarted:Landroid/widget/Button;

    .line 21
    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    const-string v1, "field \'mImgLogo\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 30
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mImgLogo\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mImgLogo:Landroid/widget/ImageView;

    .line 31
    const v1, 0x7f10011a

    const/4 v2, 0x0

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 32
    .restart local v0    # "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 33
    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder$2;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder$2;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    :cond_0
    const v1, 0x7f10011b

    const/4 v2, 0x0

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findOptionalView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 43
    .restart local v0    # "view":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 44
    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder$3;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder$3;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    :cond_1
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;, "TT;"
    const/4 v0, 0x0

    .line 56
    iput-object v0, p1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mFirstLayout:Landroid/widget/RelativeLayout;

    .line 57
    iput-object v0, p1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoLayout:Landroid/widget/RelativeLayout;

    .line 58
    iput-object v0, p1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    .line 59
    iput-object v0, p1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mTxtWelcome:Landroid/widget/TextView;

    .line 60
    iput-object v0, p1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mBtnGetStarted:Landroid/widget/Button;

    .line 61
    iput-object v0, p1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mImgLogo:Landroid/widget/ImageView;

    .line 62
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V

    return-void
.end method
