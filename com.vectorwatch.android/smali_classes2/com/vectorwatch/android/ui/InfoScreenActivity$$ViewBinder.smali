.class public Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "InfoScreenActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/InfoScreenActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/InfoScreenActivity;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/InfoScreenActivity;, "TT;"
    const v6, 0x7f1001ff

    const v5, 0x7f1001fe

    const v4, 0x7f1001fd

    const v3, 0x7f1001fc

    const v2, 0x7f100142

    .line 11
    const-string v1, "field \'layout\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'layout\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/InfoScreenActivity;->layout:Landroid/widget/RelativeLayout;

    .line 13
    const-string v1, "field \'mVectorLogo\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mVectorLogo\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mVectorLogo:Landroid/widget/ImageView;

    .line 15
    const-string v1, "field \'mVectorTextLogo\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 16
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mVectorTextLogo\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mVectorTextLogo:Landroid/widget/ImageView;

    .line 17
    const-string v1, "field \'mInfoText\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 18
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mInfoText\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    .line 19
    const-string v1, "field \'mRetryButton\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 20
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mRetryButton\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    .line 21
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/InfoScreenActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/InfoScreenActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/InfoScreenActivity;, "TT;"
    const/4 v0, 0x0

    .line 24
    iput-object v0, p1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->layout:Landroid/widget/RelativeLayout;

    .line 25
    iput-object v0, p1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mVectorLogo:Landroid/widget/ImageView;

    .line 26
    iput-object v0, p1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mVectorTextLogo:Landroid/widget/ImageView;

    .line 27
    iput-object v0, p1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    .line 28
    iput-object v0, p1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    .line 29
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/InfoScreenActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/InfoScreenActivity;)V

    return-void
.end method
