.class Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$2;
.super Ljava/lang/Object;
.source "ActivityListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 364
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 367
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 368
    .local v0, "exportIntent":Landroid/content/Intent;
    const-string v1, "https://developer.vectorwatch.com/platform/export/activity"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 369
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 370
    return-void
.end method
