.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;
.super Lio/realm/RealmObject;
.source "StringObject.java"

# interfaces
.implements Lio/realm/StringObjectRealmProxyInterface;


# instance fields
.field private string:Ljava/lang/String;
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;->string:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;->realmGet$string()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public realmGet$string()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;->string:Ljava/lang/String;

    return-object v0
.end method

.method public realmSet$string(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;->string:Ljava/lang/String;

    return-void
.end method

.method public setString(Ljava/lang/String;)V
    .locals 0
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;->realmSet$string(Ljava/lang/String;)V

    .line 26
    return-void
.end method
