.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders$RowViewHolder;
.super Ljava/lang/Object;
.source "NotificationAlertListAdapterHolders.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ResourceType"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RowViewHolder"
.end annotation


# instance fields
.field public alert:Landroid/widget/Button;

.field public dateTag:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const v0, 0x7f1001fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders$RowViewHolder;->alert:Landroid/widget/Button;

    .line 21
    const v0, 0x7f100238

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterHolders$RowViewHolder;->dateTag:Landroid/widget/TextView;

    .line 22
    return-void
.end method
