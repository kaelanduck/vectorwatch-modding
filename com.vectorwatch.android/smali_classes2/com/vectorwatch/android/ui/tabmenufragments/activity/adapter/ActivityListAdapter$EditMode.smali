.class public final enum Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;
.super Ljava/lang/Enum;
.source "ActivityListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EditMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

.field public static final enum CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

.field public static final enum DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

.field public static final enum NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

.field public static final enum SLEEP:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

.field public static final enum STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 637
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    const-string v1, "STEPS"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    const-string v1, "CALORIES"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    const-string v1, "SLEEP"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->SLEEP:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    const-string v1, "DISTANCE"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->SLEEP:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->$VALUES:[Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 637
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 637
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;
    .locals 1

    .prologue
    .line 637
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->$VALUES:[Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    return-object v0
.end method
