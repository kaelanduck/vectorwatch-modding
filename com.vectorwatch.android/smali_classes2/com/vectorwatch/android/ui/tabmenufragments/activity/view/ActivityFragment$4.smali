.class Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$4;
.super Ljava/lang/Object;
.source "ActivityFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->handleActivityTabSelectedEvent(Lcom/vectorwatch/android/events/ActivityTabSelectedEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 213
    const-string v0, "flag_show_sync_to_fit_popup"

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    .line 214
    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 213
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 215
    const-string v0, "flag_sync_to_google_fit"

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    .line 216
    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 215
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 217
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/TriggerBuildFitnessClientEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/events/TriggerBuildFitnessClientEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 218
    return-void
.end method
