.class Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;
.super Ljava/lang/Object;
.source "ActivityListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 532
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 536
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 537
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03003f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 538
    .local v1, "view":Landroid/view/View;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c00c5

    invoke-direct {v2, v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 540
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0900b3

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9$2;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;)V

    .line 541
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f09008a

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9$1;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;)V

    .line 546
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 550
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 551
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 555
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-void

    .line 554
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$9;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    sget-object v3, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->changeEditMode(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V

    goto :goto_0
.end method
