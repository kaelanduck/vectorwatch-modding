.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "NotificationsAlertActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;, "TT;"
    const v6, 0x7f100110

    const v5, 0x7f10010f

    const v4, 0x7f10010e

    const v3, 0x7f10010d

    const v2, 0x7f10010c

    .line 11
    const-string v1, "field \'mainLayout\' and method \'onMainViewClicked\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mainLayout\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mainLayout:Landroid/widget/RelativeLayout;

    .line 13
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    const-string v1, "field \'mListView\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 22
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mListView\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mListView:Landroid/widget/ListView;

    .line 23
    const-string v1, "field \'mBtnClearAll\' and method \'onClearAllButtonClicked\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 24
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mBtnClearAll\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mBtnClearAll:Landroid/widget/Button;

    .line 25
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder$2;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    const-string v1, "field \'mBtnClear\' and method \'onClearButtonClicked\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 34
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mBtnClear\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mBtnClear:Landroid/widget/Button;

    .line 35
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder$3;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    const-string v1, "field \'mEmptyText\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 44
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mEmptyText\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mEmptyText:Landroid/widget/TextView;

    .line 45
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;, "TT;"
    const/4 v0, 0x0

    .line 48
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mainLayout:Landroid/widget/RelativeLayout;

    .line 49
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mListView:Landroid/widget/ListView;

    .line 50
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mBtnClearAll:Landroid/widget/Button;

    .line 51
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mBtnClear:Landroid/widget/Button;

    .line 52
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;->mEmptyText:Landroid/widget/TextView;

    .line 53
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/NotificationsAlertActivity;)V

    return-void
.end method
