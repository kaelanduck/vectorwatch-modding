.class Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;
.super Ljava/lang/Object;
.source "ActivityListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 491
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 494
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$1200(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->saveGoals()V

    .line 496
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    new-instance v1, Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->handleActivityTotalsUpdateEvent(Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;)V

    .line 498
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$1202(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;Z)Z

    .line 500
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->changeEditMode(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V

    .line 501
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$1300(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/support/v7/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$6;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$1300(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/support/v7/view/ActionMode;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/view/ActionMode;->finish()V

    .line 505
    :cond_0
    return-void
.end method
