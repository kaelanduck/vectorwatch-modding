.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;
.super Lio/realm/RealmObject;
.source "LongObject.java"

# interfaces
.implements Lio/realm/LongObjectRealmProxyInterface;


# instance fields
.field private longValue:Ljava/lang/Long;
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;)V
    .locals 0
    .param p1, "longValue"    # Ljava/lang/Long;

    .prologue
    .line 17
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;->longValue:Ljava/lang/Long;

    .line 19
    return-void
.end method


# virtual methods
.method public getLongValue()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;->realmGet$longValue()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public realmGet$longValue()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;->longValue:Ljava/lang/Long;

    return-object v0
.end method

.method public realmSet$longValue(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;->longValue:Ljava/lang/Long;

    return-void
.end method

.method public setLongValue(Ljava/lang/Long;)V
    .locals 0
    .param p1, "longValue"    # Ljava/lang/Long;

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;->realmSet$longValue(Ljava/lang/Long;)V

    .line 27
    return-void
.end method
