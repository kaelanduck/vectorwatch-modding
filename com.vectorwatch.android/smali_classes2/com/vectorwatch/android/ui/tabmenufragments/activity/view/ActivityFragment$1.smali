.class Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;
.super Ljava/lang/Object;
.source "ActivityFragment.java"

# interfaces
.implements Landroid/support/v7/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/support/v7/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 65
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->changeEditMode(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V

    .line 68
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->getAndSetExistingGoalValues()V

    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateActionMode(Landroid/support/v7/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroyActionMode(Landroid/support/v7/view/ActionMode;)V
    .locals 2
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;->NONE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->changeEditMode(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$EditMode;)V

    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->getAndSetExistingGoalValues()V

    .line 78
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 79
    return-void
.end method

.method public onPrepareActionMode(Landroid/support/v7/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/support/v7/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method
