.class Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;
.super Ljava/lang/Object;
.source "ActivityListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 400
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/NetworkUtils;->isOnline(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 401
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090053

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .line 402
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v3

    .line 401
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 447
    :goto_0
    return-void

    .line 406
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SHARED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v1, 0x0

    check-cast v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v2, v3, v4, v1}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    .line 409
    new-instance v0, Lcom/vectorwatch/android/models/ShareValuesItem;

    new-instance v1, Lcom/vectorwatch/android/models/CloudGoalValueModel;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .line 410
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/models/CloudGoalValueModel;-><init>(II)V

    new-instance v2, Lcom/vectorwatch/android/models/CloudGoalValueModel;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/models/CloudGoalValueModel;-><init>(II)V

    new-instance v3, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .line 411
    invoke-static {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$500(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F

    move-result v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v5}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$600(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;-><init>(FF)V

    new-instance v4, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .line 412
    invoke-static {v5}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$700(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F

    move-result v5

    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v6}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$800(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)F

    move-result v6

    invoke-direct {v4, v5, v6}, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;-><init>(FF)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vectorwatch/android/models/ShareValuesItem;-><init>(Lcom/vectorwatch/android/models/CloudGoalValueModel;Lcom/vectorwatch/android/models/CloudGoalValueModel;Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;)V

    .line 414
    .local v0, "shareValuesItem":Lcom/vectorwatch/android/models/ShareValuesItem;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$900(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 415
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$900(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->bringToFront()V

    .line 416
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$1000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 418
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "goalAchievement"

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;)V

    invoke-static {v1, v2, v0, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getShareLink(Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/models/ShareValuesItem;Lretrofit/Callback;)V

    goto/16 :goto_0
.end method
