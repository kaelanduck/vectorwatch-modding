.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
.super Lio/realm/RealmObject;
.source "ActivityAlert.java"

# interfaces
.implements Lio/realm/ActivityAlertRealmProxyInterface;


# instance fields
.field private activityType:Ljava/lang/String;

.field private longValues:Lio/realm/RealmList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            ">;"
        }
    .end annotation
.end field

.field private message:Ljava/lang/String;

.field private stringValues:Lio/realm/RealmList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            ">;"
        }
    .end annotation
.end field

.field private timestamp:J
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private type:I

.field private uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lio/realm/RealmList;Lio/realm/RealmList;Ljava/lang/String;JLjava/lang/String;I)V
    .locals 1
    .param p1, "activityType"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "timestamp"    # J
    .param p7, "uuid"    # Ljava/lang/String;
    .param p8, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            ">;",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p2, "stringValues":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    .local p3, "longValues":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->activityType:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->stringValues:Lio/realm/RealmList;

    .line 26
    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->longValues:Lio/realm/RealmList;

    .line 27
    iput-object p4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->message:Ljava/lang/String;

    .line 28
    iput-wide p5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->timestamp:J

    .line 29
    iput-object p7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->uuid:Ljava/lang/String;

    .line 30
    iput p8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->type:I

    .line 31
    return-void
.end method


# virtual methods
.method public getActivityType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmGet$activityType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLongValues()Lio/realm/RealmList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmGet$longValues()Lio/realm/RealmList;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmGet$message()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringValues()Lio/realm/RealmList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmGet$stringValues()Lio/realm/RealmList;

    move-result-object v0

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmGet$timestamp()J

    move-result-wide v0

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmGet$type()I

    move-result v0

    return v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmGet$uuid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public realmGet$activityType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->activityType:Ljava/lang/String;

    return-object v0
.end method

.method public realmGet$longValues()Lio/realm/RealmList;
    .locals 1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->longValues:Lio/realm/RealmList;

    return-object v0
.end method

.method public realmGet$message()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->message:Ljava/lang/String;

    return-object v0
.end method

.method public realmGet$stringValues()Lio/realm/RealmList;
    .locals 1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->stringValues:Lio/realm/RealmList;

    return-object v0
.end method

.method public realmGet$timestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->timestamp:J

    return-wide v0
.end method

.method public realmGet$type()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->type:I

    return v0
.end method

.method public realmGet$uuid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public realmSet$activityType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->activityType:Ljava/lang/String;

    return-void
.end method

.method public realmSet$longValues(Lio/realm/RealmList;)V
    .locals 0

    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->longValues:Lio/realm/RealmList;

    return-void
.end method

.method public realmSet$message(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->message:Ljava/lang/String;

    return-void
.end method

.method public realmSet$stringValues(Lio/realm/RealmList;)V
    .locals 0

    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->stringValues:Lio/realm/RealmList;

    return-void
.end method

.method public realmSet$timestamp(J)V
    .locals 1

    iput-wide p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->timestamp:J

    return-void
.end method

.method public realmSet$type(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->type:I

    return-void
.end method

.method public realmSet$uuid(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->uuid:Ljava/lang/String;

    return-void
.end method

.method public setActivityType(Ljava/lang/String;)V
    .locals 0
    .param p1, "activityType"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmSet$activityType(Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public setLongValues(Lio/realm/RealmList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "longValues":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmSet$longValues(Lio/realm/RealmList;)V

    .line 55
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmSet$message(Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public setStringValues(Lio/realm/RealmList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmList",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "stringValues":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmSet$stringValues(Lio/realm/RealmList;)V

    .line 47
    return-void
.end method

.method public setTimestamp(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 78
    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmSet$timestamp(J)V

    .line 79
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmSet$type(I)V

    .line 71
    return-void
.end method

.method public setUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->realmSet$uuid(Ljava/lang/String;)V

    .line 87
    return-void
.end method
