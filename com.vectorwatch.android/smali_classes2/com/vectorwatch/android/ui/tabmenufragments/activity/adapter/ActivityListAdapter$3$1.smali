.class Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;
.super Ljava/lang/Object;
.source "ActivityListAdapter.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/ShareServerResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    .prologue
    .line 419
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 3
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    const/4 v2, 0x0

    .line 439
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$1100()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Share generate link fail"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 440
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$900(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 441
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$1000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f02014e

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 443
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090127

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5dc

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .line 444
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v2

    .line 443
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 445
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/ShareServerResponse;Lretrofit/client/Response;)V
    .locals 4
    .param p1, "shareServerResponse"    # Lcom/vectorwatch/android/models/ShareServerResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    const/4 v3, 0x0

    .line 422
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$900(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 423
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$1000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f02014e

    invoke-virtual {v1, v3, v3, v2, v3}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 425
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/ShareServerResponse;->getData()Lcom/vectorwatch/android/models/ShareServerData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/ShareServerResponse;->getData()Lcom/vectorwatch/android/models/ShareServerData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/ShareServerData;->getUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 426
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 427
    .local v0, "sharingIntent":Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    const-string v1, "android.intent.extra.TEXT"

    .line 429
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/ShareServerResponse;->getData()Lcom/vectorwatch/android/models/ShareServerData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/ShareServerData;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 428
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 430
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f09022e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 435
    .end local v0    # "sharingIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 433
    :cond_0
    const-string v1, "Unexpected"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 419
    check-cast p1, Lcom/vectorwatch/android/models/ShareServerResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter$3$1;->success(Lcom/vectorwatch/android/models/ShareServerResponse;Lretrofit/client/Response;)V

    return-void
.end method
