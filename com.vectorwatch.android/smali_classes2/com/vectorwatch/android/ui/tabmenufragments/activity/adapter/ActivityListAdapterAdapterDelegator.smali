.class public Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterAdapterDelegator;
.super Ljava/lang/Object;
.source "ActivityListAdapterAdapterDelegator.java"

# interfaces
.implements Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkBinderInterfaceImplemented(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;

    .prologue
    .line 15
    instance-of v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterBinder;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The adapter class ActivityListAdapter must implement the binder interface ActivityListAdapterBinder "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    return-void
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x1

    return v0
.end method

.method public onBindViewHolder(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;Landroid/view/View;II)V
    .locals 5
    .param p1, "adapter"    # Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "viewType"    # I

    .prologue
    .line 44
    move-object v1, p1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .local v1, "castedAdapter":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;
    move-object v0, p1

    .line 45
    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterBinder;

    .line 46
    .local v0, "binder":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterBinder;
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .line 47
    .local v2, "vh":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez p4, :cond_0

    .line 48
    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;

    .end local v2    # "vh":Ljava/lang/Object;
    invoke-interface {v0, v2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterBinder;->bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;I)V

    .line 49
    return-void

    .line 51
    .restart local v2    # "vh":Ljava/lang/Object;
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Binder method not found"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public onCreateViewHolder(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 7
    .param p1, "adapter"    # Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "viewType"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceType"
        }
    .end annotation

    .prologue
    .line 27
    move-object v0, p1

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;

    .line 29
    .local v0, "ad":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez p3, :cond_0

    .line 30
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapter;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030024

    const/4 v6, 0x0

    invoke-virtual {v4, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 31
    .local v3, "view":Landroid/view/View;
    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;-><init>(Landroid/view/View;)V

    .line 32
    .local v2, "vh":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;
    invoke-virtual {v3, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, p1

    .line 33
    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterBinder;

    .line 34
    .local v1, "binder":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterBinder;
    invoke-interface {v1, v2, v3, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterBinder;->initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V

    .line 35
    return-object v3

    .line 38
    .end local v1    # "binder":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterBinder;
    .end local v2    # "vh":Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterHolders$RowOneViewHolder;
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown view type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
