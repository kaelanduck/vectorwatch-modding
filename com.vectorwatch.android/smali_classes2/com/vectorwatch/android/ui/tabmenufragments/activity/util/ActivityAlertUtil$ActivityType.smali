.class public final enum Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;
.super Ljava/lang/Enum;
.source "ActivityAlertUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum CONTEXTUAL_MOTIVATION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum CONTEXTUAL_MOTIVATION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum CONTEXTUAL_MOTIVATION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum DECREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum DECREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum DECREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum GOAL_ACHIEVEMENT_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum GOAL_ACHIEVEMENT_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum GOAL_ACHIEVEMENT_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum INCREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum INCREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum INCREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum PERSONAL_RECORD_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum PERSONAL_RECORD_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

.field public static final enum PERSONAL_RECORD_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;


# instance fields
.field private mResourceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "PERSONAL_RECORD_CALORIES"

    const v2, 0x7f02012c

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 64
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "PERSONAL_RECORD_STEPS"

    const v2, 0x7f02012e

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 65
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "PERSONAL_RECORD_DISTANCE"

    const v2, 0x7f02012d

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 66
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "INCREASE_SUGGESTION_CALORIES"

    const v2, 0x7f020108

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 67
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "INCREASE_SUGGESTION_STEPS"

    const v2, 0x7f02010a

    invoke-direct {v0, v1, v8, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 68
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "INCREASE_SUGGESTION_DISTANCE"

    const/4 v2, 0x5

    const v3, 0x7f020109

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 69
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "DECREASE_SUGGESTION_CALORIES"

    const/4 v2, 0x6

    const v3, 0x7f0200c4

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 70
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "DECREASE_SUGGESTION_STEPS"

    const/4 v2, 0x7

    const v3, 0x7f0200c6

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 71
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "DECREASE_SUGGESTION_DISTANCE"

    const/16 v2, 0x8

    const v3, 0x7f0200c5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 72
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "GOAL_ACHIEVEMENT_CALORIES"

    const/16 v2, 0x9

    const v3, 0x7f0200d2

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->GOAL_ACHIEVEMENT_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 73
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "GOAL_ACHIEVEMENT_STEPS"

    const/16 v2, 0xa

    const v3, 0x7f0200d4

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->GOAL_ACHIEVEMENT_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 74
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "GOAL_ACHIEVEMENT_DISTANCE"

    const/16 v2, 0xb

    const v3, 0x7f0200d3

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->GOAL_ACHIEVEMENT_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 75
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "CONTEXTUAL_MOTIVATION_CALORIES"

    const/16 v2, 0xc

    const v3, 0x7f0200b0

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 76
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "CONTEXTUAL_MOTIVATION_STEPS"

    const/16 v2, 0xd

    const v3, 0x7f0200b2

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 77
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    const-string v1, "CONTEXTUAL_MOTIVATION_DISTANCE"

    const/16 v2, 0xe

    const v3, 0x7f0200b1

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    .line 62
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->PERSONAL_RECORD_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->INCREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->DECREASE_SUGGESTION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->GOAL_ACHIEVEMENT_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->GOAL_ACHIEVEMENT_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->GOAL_ACHIEVEMENT_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_CALORIES:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_STEPS:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->CONTEXTUAL_MOTIVATION_DISTANCE:Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->$VALUES:[Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "resourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 82
    iput p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->mResourceId:I

    .line 83
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 62
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->$VALUES:[Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;

    return-object v0
.end method


# virtual methods
.method getResourceId()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/util/ActivityAlertUtil$ActivityType;->mResourceId:I

    return v0
.end method
