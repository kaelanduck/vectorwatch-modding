.class public Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;
.super Ljava/lang/Object;
.source "SettingsFragment$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;, "TT;"
    const v6, 0x7f1001c5

    const v5, 0x7f1001c4

    const v4, 0x7f1001c3

    const v3, 0x7f1001c2

    const v2, 0x7f1001b5

    .line 11
    const-string v1, "field \'mLinkStateImage\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mLinkStateImage\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 13
    const-string v1, "field \'mAccountProfileView\' and method \'onAccountProfileClicked\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mAccountProfileView\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAccountProfileView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 15
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    const-string v1, "field \'mActivityInfoView\' and method \'onActivityInfoClicked\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 24
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mActivityInfoView\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mActivityInfoView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 25
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$2;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    const v1, 0x7f1001c6

    const-string v2, "field \'mAlertsView\' and method \'OnAlertsClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 34
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001c6

    const-string v2, "field \'mAlertsView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAlertsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 35
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$3;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    const v1, 0x7f1001c7

    const-string v2, "field \'mContextualView\' and method \'OnContextualClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 44
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001c7

    const-string v2, "field \'mContextualView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mContextualView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 45
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$4;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    const v1, 0x7f1001c8

    const-string v2, "field \'mWatchView\' and method \'OnWatchClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 54
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001c8

    const-string v2, "field \'mWatchView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 55
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$5;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$5;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const v1, 0x7f1001c9

    const-string v2, "field \'mAlarmsView\' and method \'OnAlarmsClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 64
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001c9

    const-string v2, "field \'mAlarmsView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAlarmsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 65
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$6;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$6;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    const v1, 0x7f1001cb

    const-string v2, "field \'mSupportView\' and method \'OnSupportClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 74
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001cb

    const-string v2, "field \'mSupportView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSupportView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 75
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$7;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$7;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v1, 0x7f1001cc

    const-string v2, "field \'mNewsletterView\' and method \'OnNewsletterClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 84
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001cc

    const-string v2, "field \'mNewsletterView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 85
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$8;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$8;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    const v1, 0x7f1001cd

    const-string v2, "field \'mTourView\' and method \'OnTourClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 94
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001cd

    const-string v2, "field \'mTourView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mTourView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 95
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$9;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$9;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    const v1, 0x7f1001cf

    const-string v2, "field \'mUpdateWatchView\' and method \'OnUpdateWatchClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 104
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001cf

    const-string v2, "field \'mUpdateWatchView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mUpdateWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 105
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$10;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$10;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    const v1, 0x7f1001d0

    const-string v2, "field \'mResetView\' and method \'OnResetSettingsClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 114
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001d0

    const-string v2, "field \'mResetView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mResetView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 115
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$11;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$11;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    const v1, 0x7f1001d1

    const-string v2, "field \'mLogoutView\' and method \'OnLogoutClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 124
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001d1

    const-string v2, "field \'mLogoutView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLogoutView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 125
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$12;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$12;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    const v1, 0x7f1001d2

    const-string v2, "field \'mVersionView\' and method \'OnAboutClicked\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 134
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001d2

    const-string v2, "field \'mVersionView\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mVersionView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 135
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$13;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder$13;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    const-string v1, "field \'mSection1\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 144
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mSection1\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 145
    const-string v1, "field \'mSection2\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 146
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mSection2\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection2:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 147
    const v1, 0x7f1001ca

    const-string v2, "field \'mSection3\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 148
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001ca

    const-string v2, "field \'mSection3\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection3:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 149
    const v1, 0x7f1001ce

    const-string v2, "field \'mSection4\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 150
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001ce

    const-string v2, "field \'mSection4\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection4:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 151
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;, "TT;"
    const/4 v0, 0x0

    .line 154
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 155
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAccountProfileView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 156
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mActivityInfoView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 157
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAlertsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 158
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mContextualView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 159
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 160
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAlarmsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 161
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSupportView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 162
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 163
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mTourView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 164
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mUpdateWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 165
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mResetView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 166
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLogoutView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 167
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mVersionView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    .line 168
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 169
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection2:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 170
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection3:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 171
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection4:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 172
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)V

    return-void
.end method
