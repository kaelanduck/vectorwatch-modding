.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterAdapterDelegator;
.super Ljava/lang/Object;
.source "StorePlacesAdapterAdapterDelegator.java"

# interfaces
.implements Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkBinderInterfaceImplemented(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;

    .prologue
    .line 15
    instance-of v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The adapter class StorePlacesAdapter must implement the binder interface StorePlacesAdapterBinder "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    return-void
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x3

    return v0
.end method

.method public onBindViewHolder(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;Landroid/view/View;II)V
    .locals 5
    .param p1, "adapter"    # Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "viewType"    # I

    .prologue
    .line 62
    move-object v1, p1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;

    .local v1, "castedAdapter":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;
    move-object v0, p1

    .line 63
    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;

    .line 64
    .local v0, "binder":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .line 65
    .local v2, "vh":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez p4, :cond_0

    .line 66
    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;

    .end local v2    # "vh":Ljava/lang/Object;
    invoke-interface {v0, v2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;->bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;I)V

    .line 75
    :goto_0
    return-void

    .line 69
    .restart local v2    # "vh":Ljava/lang/Object;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x1

    if-ne v3, p4, :cond_1

    .line 70
    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;

    .end local v2    # "vh":Ljava/lang/Object;
    invoke-interface {v0, v2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;->bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;I)V

    goto :goto_0

    .line 73
    .restart local v2    # "vh":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x2

    if-ne v3, p4, :cond_2

    .line 74
    check-cast v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;

    .end local v2    # "vh":Ljava/lang/Object;
    invoke-interface {v0, v2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;->bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;I)V

    goto :goto_0

    .line 77
    .restart local v2    # "vh":Ljava/lang/Object;
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Binder method not found"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public onCreateViewHolder(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 8
    .param p1, "adapter"    # Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "viewType"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceType"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 27
    move-object v0, p1

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;

    .line 29
    .local v0, "ad":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-nez p3, :cond_0

    .line 30
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03009d

    invoke-virtual {v5, v6, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 31
    .local v3, "view":Landroid/view/View;
    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;-><init>(Landroid/view/View;)V

    .line 32
    .local v2, "vh":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;
    invoke-virtual {v3, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, p1

    .line 33
    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;

    .line 34
    .local v1, "binder":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;
    invoke-interface {v1, v2, v3, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;->initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V

    move-object v4, v3

    .line 53
    .end local v2    # "vh":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;
    .end local v3    # "view":Landroid/view/View;
    .local v4, "view":Landroid/view/View;
    :goto_0
    return-object v4

    .line 38
    .end local v1    # "binder":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;
    .end local v4    # "view":Landroid/view/View;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x1

    if-ne p3, v5, :cond_1

    .line 39
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03009e

    invoke-virtual {v5, v6, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 40
    .restart local v3    # "view":Landroid/view/View;
    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;-><init>(Landroid/view/View;)V

    .line 41
    .local v2, "vh":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;
    invoke-virtual {v3, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, p1

    .line 42
    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;

    .line 43
    .restart local v1    # "binder":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;
    invoke-interface {v1, v2, v3, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;->initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V

    move-object v4, v3

    .line 44
    .end local v3    # "view":Landroid/view/View;
    .restart local v4    # "view":Landroid/view/View;
    goto :goto_0

    .line 47
    .end local v1    # "binder":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;
    .end local v2    # "vh":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;
    .end local v4    # "view":Landroid/view/View;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x2

    if-ne p3, v5, :cond_2

    .line 48
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->getInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03009f

    invoke-virtual {v5, v6, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 49
    .restart local v3    # "view":Landroid/view/View;
    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;-><init>(Landroid/view/View;)V

    .line 50
    .local v2, "vh":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;
    invoke-virtual {v3, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, p1

    .line 51
    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;

    .line 52
    .restart local v1    # "binder":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;
    invoke-interface {v1, v2, v3, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;->initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V

    move-object v4, v3

    .line 53
    .end local v3    # "view":Landroid/view/View;
    .restart local v4    # "view":Landroid/view/View;
    goto :goto_0

    .line 56
    .end local v1    # "binder":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;
    .end local v2    # "vh":Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;
    .end local v4    # "view":Landroid/view/View;
    :cond_2
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown view type "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5
.end method
