.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;
.super Ljava/lang/Object;
.source "StoreAddRatingInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;)Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public addRating(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;F)V
    .locals 5
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "comment"    # Ljava/lang/String;
    .param p3, "rating"    # F

    .prologue
    .line 64
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;

    invoke-direct {v0, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;-><init>(Ljava/lang/String;F)V

    .line 65
    .local v0, "ratingCloudItem":Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$2;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;)V

    invoke-static {v1, v2, v3, v0, v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->addRating(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;Lretrofit/Callback;)V

    .line 80
    return-void
.end method

.method public loadOwnRating(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 4
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;)V

    invoke-static {v0, v1, v2, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getRating(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lretrofit/Callback;)V

    .line 53
    return-void
.end method
