.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;
.super Ljava/lang/Object;
.source "StorePlacesAdapterHolders.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ResourceType"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RowTopViewHolder"
.end annotation


# instance fields
.field public indicator:Lcom/vectorwatch/android/ui/view/viewpagerindicator/CirclePageIndicator;

.field public pager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const v0, 0x7f100262

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;->pager:Landroid/support/v4/view/ViewPager;

    .line 65
    const v0, 0x7f100263

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/CirclePageIndicator;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;->indicator:Lcom/vectorwatch/android/ui/view/viewpagerindicator/CirclePageIndicator;

    .line 66
    return-void
.end method
