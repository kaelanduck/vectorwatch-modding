.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;
.super Ljava/lang/Object;
.source "RatingItemData.java"


# instance fields
.field ratings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "ratings":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;->ratings:Ljava/util/List;

    .line 16
    return-void
.end method


# virtual methods
.method public getRatings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;->ratings:Ljava/util/List;

    return-object v0
.end method

.method public setRatings(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "ratings":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/RatingItem;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;->ratings:Ljava/util/List;

    .line 24
    return-void
.end method
