.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "StoreItemDetailsActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;, "TT;"
    const v6, 0x7f10014e

    const v5, 0x7f10014d

    const v2, 0x7f10014c

    const v4, 0x7f1000f8

    const v3, 0x7f1000eb

    .line 11
    const-string v1, "field \'mImageView\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mImageView\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImageView:Landroid/widget/ImageView;

    .line 13
    const-string v1, "field \'mAppName\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mAppName\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mAppName:Landroid/widget/TextView;

    .line 15
    const v1, 0x7f10014f

    const-string v2, "field \'mAppDescription\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 16
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f10014f

    const-string v2, "field \'mAppDescription\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mAppDescription:Landroid/widget/TextView;

    .line 17
    const v1, 0x7f100150

    const-string v2, "field \'mActionButton\' and method \'onActionClick\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 18
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100150

    const-string v2, "field \'mActionButton\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mActionButton:Landroid/widget/ImageView;

    .line 19
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    const-string v1, "field \'mProgressBar\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 28
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mProgressBar\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 29
    const-string v1, "field \'mImgInstalled\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 30
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mImgInstalled\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImgInstalled:Landroid/widget/ImageView;

    .line 31
    const v1, 0x7f100151

    const-string v2, "field \'mExtendedDescription\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 32
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100151

    const-string v2, "field \'mExtendedDescription\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mExtendedDescription:Landroid/widget/TextView;

    .line 33
    const-string v1, "field \'mRatingBar\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 34
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mRatingBar\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    .line 35
    const v1, 0x7f100152

    const-string v2, "field \'mReviewsLayout\' and method \'onReviewsClick\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 36
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100152

    const-string v2, "field \'mReviewsLayout\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mReviewsLayout:Landroid/widget/RelativeLayout;

    .line 37
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder$2;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    const v1, 0x7f100154

    const-string v2, "field \'mReportLayout\' and method \'onReportAbuseClick\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 46
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100154

    const-string v2, "field \'mReportLayout\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mReportLayout:Landroid/widget/RelativeLayout;

    .line 47
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder$3;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const v1, 0x7f100153

    const-string v2, "field \'mRatingLayout\' and method \'onRatingLayoutClick\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 56
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100153

    const-string v2, "field \'mRatingLayout\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mRatingLayout:Landroid/widget/RelativeLayout;

    .line 57
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder$4;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;, "TT;"
    const/4 v0, 0x0

    .line 68
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImageView:Landroid/widget/ImageView;

    .line 69
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mAppName:Landroid/widget/TextView;

    .line 70
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mAppDescription:Landroid/widget/TextView;

    .line 71
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mActionButton:Landroid/widget/ImageView;

    .line 72
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 73
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mImgInstalled:Landroid/widget/ImageView;

    .line 74
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mExtendedDescription:Landroid/widget/TextView;

    .line 75
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mRatingBar:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    .line 76
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mReviewsLayout:Landroid/widget/RelativeLayout;

    .line 77
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mReportLayout:Landroid/widget/RelativeLayout;

    .line 78
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;->mRatingLayout:Landroid/widget/RelativeLayout;

    .line 79
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreItemDetailsActivity;)V

    return-void
.end method
