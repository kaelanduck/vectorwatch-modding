.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
.super Ljava/lang/Object;
.source "StorePlace.java"


# instance fields
.field private imageUrl1:Ljava/lang/String;

.field private imageUrl2:Ljava/lang/String;

.field private imageUrl3:Ljava/lang/String;

.field private newInStore:Z

.field private summary:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "summary"    # Ljava/lang/String;
    .param p3, "imageUrl1"    # Ljava/lang/String;
    .param p4, "imageUrl2"    # Ljava/lang/String;
    .param p5, "imageUrl3"    # Ljava/lang/String;
    .param p6, "newInStore"    # Z

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->title:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->summary:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->imageUrl1:Ljava/lang/String;

    .line 19
    iput-object p4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->imageUrl2:Ljava/lang/String;

    .line 20
    iput-object p5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->imageUrl3:Ljava/lang/String;

    .line 21
    iput-boolean p6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->newInStore:Z

    .line 22
    return-void
.end method


# virtual methods
.method public getImageUrl1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->imageUrl1:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->imageUrl2:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl3()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->imageUrl3:Ljava/lang/String;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->title:Ljava/lang/String;

    return-object v0
.end method

.method public isNewInStore()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->newInStore:Z

    return v0
.end method

.method public setImageUrl1(Ljava/lang/String;)V
    .locals 0
    .param p1, "imageUrl1"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->imageUrl1:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setImageUrl2(Ljava/lang/String;)V
    .locals 0
    .param p1, "imageUrl2"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->imageUrl2:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setImageUrl3(Ljava/lang/String;)V
    .locals 0
    .param p1, "imageUrl3"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->imageUrl3:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public setNewInStore(Z)V
    .locals 0
    .param p1, "newInStore"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->newInStore:Z

    .line 70
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 0
    .param p1, "summary"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->summary:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->title:Ljava/lang/String;

    .line 30
    return-void
.end method
