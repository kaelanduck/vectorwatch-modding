.class Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$1;
.super Ljava/lang/Object;
.source "StoreAddRatingInteractorImpl.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->loadOwnRating(Lcom/vectorwatch/android/models/StoreElement;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 3
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;)Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090221

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;->displayAlert(Ljava/lang/String;I)V

    .line 51
    return-void
.end method

.method public success(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;Lretrofit/client/Response;)V
    .locals 2
    .param p1, "ratingItemGetResponse"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 41
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;->getRatings()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl;)Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;->getRatings()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreAddRatingPresenter$Interactor;->onRatingsLoaded(Ljava/util/List;)V

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    const-string v0, "Unexpected"

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$1;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreAddRatingInteractorImpl$1;->success(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;Lretrofit/client/Response;)V

    return-void
.end method
