.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;
.super Ljava/lang/Object;
.source "StoreItemDetailsRatingsInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractor;
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractor;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;->mContext:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;

    .line 32
    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 3
    .param p1, "retrofitError"    # Lretrofit/RetrofitError;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;->mContext:Landroid/content/Context;

    const v2, 0x7f090221

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;->displayAlert(Ljava/lang/String;I)V

    .line 67
    return-void
.end method

.method public loadRatingsList(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 3
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getRating(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lretrofit/Callback;)V

    .line 42
    return-void
.end method

.method public success(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;Lretrofit/client/Response;)V
    .locals 2
    .param p1, "ratingItemGetResponse"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 52
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;->getRatings()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemData;->getRatings()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsRatingsPresenter$Interactor;->onRatingsLoaded(Ljava/util/List;)V

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    const-string v0, "Unexpected"

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsRatingsInteractorImpl;->success(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;Lretrofit/client/Response;)V

    return-void
.end method
