.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;
.super Ljava/lang/Object;
.source "RatingItemPostResponse.java"


# instance fields
.field private data:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "data"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;->message:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;->data:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;

    .line 13
    return-void
.end method


# virtual methods
.method public getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;->data:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;->message:Ljava/lang/String;

    return-object v0
.end method

.method public setData(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;)V
    .locals 0
    .param p1, "data"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;->data:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostData;

    .line 21
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;->message:Ljava/lang/String;

    .line 29
    return-void
.end method
