.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;
.super Ljava/lang/Object;
.source "StoreItemDetailsInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractor;


# instance fields
.field log:Lorg/slf4j/Logger;

.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->log:Lorg/slf4j/Logger;

    .line 38
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    .line 39
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;

    .line 40
    return-void
.end method

.method private onSaveAction(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/AppInstallManager;Z)V
    .locals 7
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "appInstallManager"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p3, "isInstalled"    # Z

    .prologue
    const-wide/16 v4, 0x0

    .line 142
    new-instance v6, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 143
    .local v6, "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 144
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 147
    if-nez p3, :cond_2

    .line 149
    invoke-virtual {p2}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "DOWNLOAD: download in progress."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 152
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 216
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "DOWNLOAD: no download in progress."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    invoke-virtual {p2, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->installStreamOffline(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 161
    :goto_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 163
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 161
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto :goto_0

    .line 158
    :cond_1
    invoke-virtual {p2, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->installStreamFromStore(Lcom/vectorwatch/android/models/StoreElement;)V

    goto :goto_1

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Delete stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/StreamsManager;->deleteStreamFromDatabase(Ljava/lang/String;Landroid/content/Context;)Z

    .line 169
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 171
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 169
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto :goto_0

    .line 174
    :cond_3
    if-nez p3, :cond_8

    .line 175
    invoke-virtual {p2}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 176
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "DOWNLOAD: download in progress."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 178
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 180
    :cond_4
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "DOWNLOAD: no download in progress."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 182
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 183
    sget-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {p2, p1, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->installAppOffline(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 191
    :goto_2
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 192
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 194
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 192
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 185
    :cond_5
    sget-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {p2, p1, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->installAppOffline(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/models/cloud/AppsOption;)V

    goto :goto_2

    .line 188
    :cond_6
    invoke-virtual {p2, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->installAppFromStore(Lcom/vectorwatch/android/models/StoreElement;)V

    goto :goto_2

    .line 196
    :cond_7
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_APP:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 198
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 196
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 203
    :cond_8
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v6, v0, v1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->uninstallAppFromWatch(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V

    .line 205
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 206
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 208
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 206
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 210
    :cond_9
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_APP:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 212
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 210
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0
.end method


# virtual methods
.method public onActionClick(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 12
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    const/4 v9, 0x0

    const/16 v11, 0x5dc

    const/4 v8, 0x1

    .line 86
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v1

    .line 87
    .local v1, "appInstallManager":Lcom/vectorwatch/android/utils/AppInstallManager;
    const/4 v4, 0x0

    .line 88
    .local v4, "isInstalled":Z
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/vectorwatch/android/managers/StreamsManager;->getSavedStreams(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 90
    .local v0, "allStreams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    new-instance v2, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 91
    .local v2, "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 92
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v7

    if-nez v7, :cond_3

    .line 97
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v2, v7}, Lcom/vectorwatch/android/managers/CloudAppsManager;->isAppInstalled(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)Z

    move-result v3

    .line 98
    .local v3, "isAppSaved":Z
    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    if-eqz v3, :cond_2

    .line 99
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    move v4, v8

    .line 110
    .end local v3    # "isAppSaved":Z
    :goto_0
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 111
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v9, "STORE: Click on store button."

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 113
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 115
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f09005d

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v11}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;->onErrorReceived(Ljava/lang/String;I)V

    .line 132
    :goto_1
    return-void

    .restart local v3    # "isAppSaved":Z
    :cond_2
    move v4, v9

    .line 99
    goto :goto_0

    .line 105
    .end local v3    # "isAppSaved":Z
    :cond_3
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v7

    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v0}, Lcom/vectorwatch/android/managers/StreamsManager;->isStreamSaved(Ljava/lang/String;Landroid/content/Context;Ljava/util/List;)Z

    move-result v5

    .line 106
    .local v5, "isStreamSaved":Z
    if-eqz v5, :cond_4

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_4
    if-eqz v5, :cond_6

    .line 107
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_5
    move v4, v8

    :goto_2
    goto :goto_0

    :cond_6
    move v4, v9

    goto :goto_2

    .line 118
    .end local v5    # "isStreamSaved":Z
    :cond_7
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v9, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-static {v7, v9}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppsWithGivenState(Landroid/content/Context;Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)Ljava/util/ArrayList;

    move-result-object v6

    .line 119
    .local v6, "mRunningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v7

    if-nez v7, :cond_8

    if-eqz v4, :cond_8

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v7, v8, :cond_8

    .line 121
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;

    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090118

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v11}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;->onErrorReceived(Ljava/lang/String;I)V

    goto :goto_1

    .line 124
    :cond_8
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;

    invoke-interface {v7, v4, v8}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;->onViewUpdated(ZZ)V

    .line 125
    invoke-direct {p0, p1, v1, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->onSaveAction(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/AppInstallManager;Z)V

    goto :goto_1

    .line 130
    .end local v6    # "mRunningApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    :cond_9
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090063

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8, v11}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;->onErrorReceived(Ljava/lang/String;I)V

    goto/16 :goto_1
.end method

.method public updateViewState(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 11
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 49
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v1

    .line 51
    .local v1, "appInstallManager":Lcom/vectorwatch/android/utils/AppInstallManager;
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v4, v8

    .line 52
    .local v4, "isBeingTransmitted":Z
    :goto_0
    const/4 v5, 0x0

    .line 53
    .local v5, "isInstalled":Z
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/vectorwatch/android/managers/StreamsManager;->getSavedStreams(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 55
    .local v0, "allStreams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    new-instance v2, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 56
    .local v2, "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 57
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v7

    if-nez v7, :cond_4

    .line 62
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v2, v7}, Lcom/vectorwatch/android/managers/CloudAppsManager;->isAppInstalled(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)Z

    move-result v3

    .line 63
    .local v3, "isAppSaved":Z
    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    if-eqz v3, :cond_3

    .line 64
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    :cond_1
    move v5, v8

    .line 75
    .end local v3    # "isAppSaved":Z
    :goto_1
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Update view state - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 76
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;

    invoke-interface {v7, v5, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreItemDetailsPresenter$Interactor;->onViewUpdated(ZZ)V

    .line 77
    return-void

    .end local v0    # "allStreams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .end local v2    # "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    .end local v4    # "isBeingTransmitted":Z
    .end local v5    # "isInstalled":Z
    :cond_2
    move v4, v9

    .line 51
    goto :goto_0

    .restart local v0    # "allStreams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .restart local v2    # "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    .restart local v3    # "isAppSaved":Z
    .restart local v4    # "isBeingTransmitted":Z
    .restart local v5    # "isInstalled":Z
    :cond_3
    move v5, v9

    .line 64
    goto :goto_1

    .line 70
    .end local v3    # "isAppSaved":Z
    :cond_4
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v7

    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreItemDetailsInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v0}, Lcom/vectorwatch/android/managers/StreamsManager;->isStreamSaved(Ljava/lang/String;Landroid/content/Context;Ljava/util/List;)Z

    move-result v6

    .line 71
    .local v6, "isStreamSaved":Z
    if-eqz v6, :cond_5

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v7

    if-eqz v7, :cond_6

    :cond_5
    if-eqz v6, :cond_7

    .line 72
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_7

    :cond_6
    move v5, v8

    :goto_2
    goto :goto_1

    :cond_7
    move v5, v9

    goto :goto_2
.end method
