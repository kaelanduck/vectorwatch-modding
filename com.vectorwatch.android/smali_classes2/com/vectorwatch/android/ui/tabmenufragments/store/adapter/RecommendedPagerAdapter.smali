.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "RecommendedPagerAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;>;"
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 28
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;->mData:Ljava/util/List;

    .line 29
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 60
    check-cast p3, Landroid/widget/RelativeLayout;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 61
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;->mData:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 34
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;->mData:Ljava/util/List;

    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;

    .line 36
    .local v1, "slide":Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300ba

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 37
    .local v3, "view":Landroid/view/View;
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 39
    const v4, 0x7f1002e4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 40
    .local v0, "imageView":Landroid/widget/ImageView;
    const v4, 0x7f10014f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 42
    .local v2, "textView":Landroid/widget/TextView;
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->getBgImageUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 43
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 44
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v4

    .line 45
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->getBgImageUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v4

    .line 46
    invoke-virtual {v4}, Lcom/bumptech/glide/DrawableTypeRequest;->crossFade()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v4

    .line 47
    invoke-virtual {v4, v0}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 52
    :goto_0
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/store/listeners/SlideImageClickListener;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/listeners/SlideImageClickListener;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    return-object v3

    .line 49
    :cond_0
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 70
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
