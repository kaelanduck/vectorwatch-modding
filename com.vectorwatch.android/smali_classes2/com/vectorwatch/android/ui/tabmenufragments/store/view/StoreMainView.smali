.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainView;
.super Ljava/lang/Object;
.source "StoreMainView.java"


# virtual methods
.method public abstract handleLinkStateChanged(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
.end method

.method public abstract hideProgress()V
.end method

.method public abstract hideSlider()V
.end method

.method public abstract navigateToApps()V
.end method

.method public abstract navigateToStreams()V
.end method

.method public abstract navigateToWatchfaces()V
.end method

.method public abstract onRecommendedOfflineSlide(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;)V
.end method

.method public abstract populateImageList(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V
.end method

.method public abstract populateSlideshowViewPager(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;)V
.end method

.method public abstract populateStorePlaces(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract showProgress()V
.end method
