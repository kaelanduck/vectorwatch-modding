.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;
.super Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;
.source "StorePlacesAdapter.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterBinder;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;",
            ">;"
        }
    .end annotation
.end field

.field private mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mPagerAdaper:Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;

.field private mSlides:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;",
            ">;"
        }
    .end annotation
.end field

.field public final rowSimple:I
    .annotation build Lcom/hannesdorfmann/annotatedadapter/annotation/ViewType;
        initMethod = true
        layout = 0x7f03009d
        views = {
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f10025e
                name = "title"
                type = Landroid/widget/TextView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f10025f
                name = "summary"
                type = Landroid/widget/TextView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100260
                name = "image3"
                type = Landroid/widget/ImageView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100261
                name = "image2"
                type = Landroid/widget/ImageView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f10025d
                name = "image1"
                type = Landroid/widget/ImageView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f10023d
                name = "newView"
                type = Landroid/widget/RelativeLayout;
            .end subannotation
        }
    .end annotation
.end field

.field public final rowSimpleStream:I
    .annotation build Lcom/hannesdorfmann/annotatedadapter/annotation/ViewType;
        initMethod = true
        layout = 0x7f03009e
        views = {
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f10025e
                name = "title"
                type = Landroid/widget/TextView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f10025f
                name = "summary"
                type = Landroid/widget/TextView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100260
                name = "image3"
                type = Landroid/widget/ImageView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100261
                name = "image2"
                type = Landroid/widget/ImageView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f10025d
                name = "image1"
                type = Landroid/widget/ImageView;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f10023d
                name = "newView"
                type = Landroid/widget/RelativeLayout;
            .end subannotation
        }
    .end annotation
.end field

.field public final rowTop:I
    .annotation build Lcom/hannesdorfmann/annotatedadapter/annotation/ViewType;
        initMethod = true
        layout = 0x7f03009f
        views = {
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100262
                name = "pager"
                type = Landroid/support/v4/view/ViewPager;
            .end subannotation,
            .subannotation Lcom/hannesdorfmann/annotatedadapter/annotation/ViewField;
                id = 0x7f100263
                name = "indicator"
                type = Lcom/vectorwatch/android/ui/view/viewpagerindicator/CirclePageIndicator;
            .end subannotation
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p2, "storePlaceList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;>;"
    .local p3, "mSlideList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;>;"
    invoke-direct {p0, p1}, Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;-><init>(Landroid/content/Context;)V

    .line 39
    const/4 v2, 0x0

    iput v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->rowSimple:I

    .line 78
    const/4 v2, 0x1

    iput v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->rowSimpleStream:I

    .line 117
    const/4 v2, 0x2

    iput v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->rowTop:I

    .line 139
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mData:Ljava/util/List;

    .line 140
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    .line 141
    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mSlides:Ljava/util/List;

    .line 143
    const-string v2, "activity"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 144
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v2

    mul-int/lit16 v2, v2, 0x400

    mul-int/lit16 v1, v2, 0x400

    .line 145
    .local v1, "availableMemoryInBytes":I
    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter$1;

    div-int/lit8 v3, v1, 0xa

    invoke-direct {v2, p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;I)V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mMemoryCache:Landroid/util/LruCache;

    .line 151
    return-void
.end method


# virtual methods
.method public bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;I)V
    .locals 5
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 234
    invoke-virtual {p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->getItem(I)Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    move-result-object v1

    .line 235
    .local v1, "item":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getSummary()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->isNewInStore()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->newView:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 242
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 243
    new-instance v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/StoreElement;-><init>()V

    .line 244
    .local v0, "element":Lcom/vectorwatch/android/models/StoreElement;
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setId(I)V

    .line 245
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setImage(Ljava/lang/String;)V

    .line 246
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->image1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mMemoryCache:Landroid/util/LruCache;

    sget-object v4, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v3, v0, v4}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 248
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setId(I)V

    .line 249
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setImage(Ljava/lang/String;)V

    .line 250
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->image2:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mMemoryCache:Landroid/util/LruCache;

    sget-object v4, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v3, v0, v4}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 252
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setId(I)V

    .line 253
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl3()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setImage(Ljava/lang/String;)V

    .line 254
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->image3:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mMemoryCache:Landroid/util/LruCache;

    sget-object v4, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v3, v0, v4}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 269
    .end local v0    # "element":Lcom/vectorwatch/android/models/StoreElement;
    :goto_1
    return-void

    .line 240
    :cond_0
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->newView:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 256
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v2

    .line 257
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl1()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v2

    .line 258
    invoke-virtual {v2}, Lcom/bumptech/glide/DrawableTypeRequest;->crossFade()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->image1:Landroid/widget/ImageView;

    .line 259
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 260
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v2

    .line 261
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl2()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v2

    .line 262
    invoke-virtual {v2}, Lcom/bumptech/glide/DrawableTypeRequest;->crossFade()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->image2:Landroid/widget/ImageView;

    .line 263
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 264
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v2

    .line 265
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl3()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v2

    .line 266
    invoke-virtual {v2}, Lcom/bumptech/glide/DrawableTypeRequest;->crossFade()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->image3:Landroid/widget/ImageView;

    .line 267
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    goto :goto_1
.end method

.method public bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;I)V
    .locals 5
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 189
    invoke-virtual {p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->getItem(I)Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    move-result-object v1

    .line 190
    .local v1, "item":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getSummary()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->isNewInStore()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->newView:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 198
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 199
    new-instance v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/StoreElement;-><init>()V

    .line 200
    .local v0, "element":Lcom/vectorwatch/android/models/StoreElement;
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setId(I)V

    .line 201
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setImage(Ljava/lang/String;)V

    .line 202
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->image1:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mMemoryCache:Landroid/util/LruCache;

    sget-object v4, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v3, v0, v4}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 204
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setId(I)V

    .line 205
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setImage(Ljava/lang/String;)V

    .line 206
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->image2:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mMemoryCache:Landroid/util/LruCache;

    sget-object v4, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v3, v0, v4}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 208
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setId(I)V

    .line 209
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl3()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/StoreElement;->setImage(Ljava/lang/String;)V

    .line 210
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->image3:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mMemoryCache:Landroid/util/LruCache;

    sget-object v4, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v3, v0, v4}, Lcom/vectorwatch/android/utils/UIUtils;->getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 225
    .end local v0    # "element":Lcom/vectorwatch/android/models/StoreElement;
    :goto_1
    return-void

    .line 195
    :cond_0
    iget-object v2, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->newView:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 212
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v2

    .line 213
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl1()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v2

    .line 214
    invoke-virtual {v2}, Lcom/bumptech/glide/DrawableTypeRequest;->crossFade()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->image1:Landroid/widget/ImageView;

    .line 215
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 216
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v2

    .line 217
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl2()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v2

    .line 218
    invoke-virtual {v2}, Lcom/bumptech/glide/DrawableTypeRequest;->crossFade()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->image2:Landroid/widget/ImageView;

    .line 219
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    .line 220
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/bumptech/glide/Glide;->with(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    move-result-object v2

    .line 221
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->getImageUrl3()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->load(Ljava/lang/String;)Lcom/bumptech/glide/DrawableTypeRequest;

    move-result-object v2

    .line 222
    invoke-virtual {v2}, Lcom/bumptech/glide/DrawableTypeRequest;->crossFade()Lcom/bumptech/glide/DrawableRequestBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->image3:Landroid/widget/ImageView;

    .line 223
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/DrawableRequestBuilder;->into(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/Target;

    goto :goto_1
.end method

.method public bindViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;I)V
    .locals 3
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 279
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mSlides:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mPagerAdaper:Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;

    .line 280
    iget-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;->pager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mPagerAdaper:Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/RecommendedPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 281
    iget-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;->indicator:Lcom/vectorwatch/android/ui/view/viewpagerindicator/CirclePageIndicator;

    iget-object v1, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;->pager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/viewpagerindicator/CirclePageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 282
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mData:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 161
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->getItem(I)Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 166
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 171
    if-nez p1, :cond_0

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mSlides:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 179
    :goto_0
    return v0

    .line 174
    :cond_0
    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mSlides:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    .line 175
    goto :goto_0

    .line 176
    :cond_1
    if-ne p1, v1, :cond_2

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapter;->mSlides:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 177
    goto :goto_0

    .line 179
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 230
    return-void
.end method

.method public initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 185
    return-void
.end method

.method public initViewHolder(Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "vh"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 273
    const v0, 0x7f100262

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;->pager:Landroid/support/v4/view/ViewPager;

    .line 274
    const v0, 0x7f100263

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/view/viewpagerindicator/CirclePageIndicator;

    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowTopViewHolder;->indicator:Lcom/vectorwatch/android/ui/view/viewpagerindicator/CirclePageIndicator;

    .line 275
    return-void
.end method
