.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder;
.super Ljava/lang/Object;
.source "StoreMainFragment$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;Ljava/lang/Object;)V
    .locals 5
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;, "TT;"
    const v4, 0x7f1001d6

    const v3, 0x7f1001d5

    const v2, 0x7f1001b5

    .line 11
    const-string v1, "field \'mListView\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mListView\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mListView:Landroid/widget/ListView;

    .line 13
    const-string v1, "field \'mSearchFAB\' and method \'goToSearch\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mSearchFAB\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/software/shell/fab/ActionButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    .line 15
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    const-string v1, "field \'mLinkStateImage\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 24
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mLinkStateImage\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 25
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;, "TT;"
    const/4 v0, 0x0

    .line 28
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mListView:Landroid/widget/ListView;

    .line 29
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mSearchFAB:Lcom/software/shell/fab/ActionButton;

    .line 30
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 31
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;)V

    return-void
.end method
