.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;
.super Ljava/lang/Object;
.source "StoreReportAbuseInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractor;


# static fields
.field private static final ALPHA_MAILING_LIST:Ljava/lang/String; = "androidalpha@vectorwatch.com"

.field private static final BETA_MAILING_LIST:Ljava/lang/String; = "androidbeta@vectorwatch.com"

.field private static final PRODUCTION_MAILING_LIST:Ljava/lang/String; = "support@vectorwatch.com"

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContactEmail:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;

    .line 39
    return-void
.end method

.method private handleReportAbuse(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;)V
    .locals 18
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isAlphaBuild()Z

    move-result v13

    if-nez v13, :cond_0

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isDevBuild()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 66
    :cond_0
    const-string v13, "androidalpha@vectorwatch.com"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContactEmail:Ljava/lang/String;

    .line 73
    :goto_0
    const-string v7, "[Android] Report Abuse"

    .line 74
    .local v7, "mailTitle":Ljava/lang/String;
    move-object/from16 v6, p2

    .line 77
    .local v6, "mailText":Ljava/lang/String;
    new-instance v5, Landroid/content/Intent;

    const-string v13, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v5, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 78
    .local v5, "intent":Landroid/content/Intent;
    const-string v13, "message/rfc822"

    invoke-virtual {v5, v13}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const-string v13, "android.intent.extra.SUBJECT"

    invoke-virtual {v5, v13, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 81
    .local v8, "packageManager":Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    .line 83
    .local v9, "packageName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 86
    .local v1, "appVersion":Ljava/lang/String;
    const/4 v13, 0x0

    :try_start_0
    invoke-virtual {v8, v9, v13}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v13

    iget-object v1, v13, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :goto_1
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v13

    const-string v14, "last_known_system_info"

    const-class v15, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 92
    invoke-virtual {v13, v14, v15}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 94
    .local v4, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    const-string v12, ""

    .line 95
    .local v12, "watchOS":Ljava/lang/String;
    const-string v11, ""

    .line 96
    .local v11, "userLoginMail":Ljava/lang/String;
    const-string v10, ""

    .line 97
    .local v10, "timezone":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 98
    invoke-virtual {v4}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->toString()Ljava/lang/String;

    move-result-object v12

    .line 99
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAccountAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 100
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    .line 103
    :cond_1
    const-string v14, "android.intent.extra.TEXT"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "\n Android app version: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, " "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n Android API Level: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget v15, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Device: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v15, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Manufacturer: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v15, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Model: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v15, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, " ("

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v15, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, ") "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    if-eqz v12, :cond_4

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Watch system info: "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v16, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    :goto_2
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Watch ID: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    .line 110
    invoke-static {v15}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Watch SN: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "pref_serial_number"

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    .line 111
    invoke-static/range {v15 .. v17}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "User login: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "User type: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "account_update_type"

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    .line 113
    invoke-static/range {v15 .. v17}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "App/stream id: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 114
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "App/stream uuid: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 115
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Local timezone: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n\n------- Abuse "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "starts"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, " "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "below "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "-------\n\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 103
    invoke-virtual {v5, v14, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    const-string v13, "android.intent.extra.EMAIL"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContactEmail:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-virtual {v5, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    const-string v14, "Send mail..."

    invoke-static {v5, v14}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 125
    :goto_3
    return-void

    .line 67
    .end local v1    # "appVersion":Ljava/lang/String;
    .end local v4    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v6    # "mailText":Ljava/lang/String;
    .end local v7    # "mailTitle":Ljava/lang/String;
    .end local v8    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "timezone":Ljava/lang/String;
    .end local v11    # "userLoginMail":Ljava/lang/String;
    .end local v12    # "watchOS":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isBetaBuild()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 68
    const-string v13, "androidbeta@vectorwatch.com"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContactEmail:Ljava/lang/String;

    goto/16 :goto_0

    .line 70
    :cond_3
    const-string v13, "support@vectorwatch.com"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContactEmail:Ljava/lang/String;

    goto/16 :goto_0

    .line 87
    .restart local v1    # "appVersion":Ljava/lang/String;
    .restart local v5    # "intent":Landroid/content/Intent;
    .restart local v6    # "mailText":Ljava/lang/String;
    .restart local v7    # "mailTitle":Ljava/lang/String;
    .restart local v8    # "packageManager":Landroid/content/pm/PackageManager;
    .restart local v9    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 88
    .local v2, "e":Ljava/lang/Exception;
    sget-object v13, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v14, "Support - Can\'t retrieve appVersion: stack = "

    invoke-interface {v13, v14, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 103
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v4    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    .restart local v10    # "timezone":Ljava/lang/String;
    .restart local v11    # "userLoginMail":Ljava/lang/String;
    .restart local v12    # "watchOS":Ljava/lang/String;
    :cond_4
    const-string v13, "No watch system info"

    goto/16 :goto_2

    .line 121
    :catch_1
    move-exception v3

    .line 122
    .local v3, "ex":Landroid/content/ActivityNotFoundException;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    const v15, 0x7f0901c1

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0x5dc

    invoke-interface {v13, v14, v15}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;->displayAlert(Ljava/lang/String;I)V

    .line 123
    sget-object v13, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v14, "Support - Start activity for results: stack = "

    invoke-interface {v13, v14, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method


# virtual methods
.method public reportAbuse(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;)V
    .locals 3
    .param p1, "storeElement"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->mContext:Landroid/content/Context;

    const v2, 0x7f0901e8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x7d0

    invoke-interface {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreReportAbusePresenter$Interactor;->displayAlert(Ljava/lang/String;I)V

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreReportAbuseInteractorImpl;->handleReportAbuse(Lcom/vectorwatch/android/models/StoreElement;Ljava/lang/String;)V

    goto :goto_0
.end method
