.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;
.super Ljava/lang/Object;
.source "StoreInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractor;
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractor;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/StoreQuery;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

.field private mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 46
    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;->onStoreQueryFailure(Lretrofit/RetrofitError;)V

    .line 175
    return-void
.end method

.method public loadFacesInList(ILcom/vectorwatch/android/models/cloud/AppsOption;)V
    .locals 3
    .param p1, "page"    # I
    .param p2, "appsOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;

    .prologue
    .line 148
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 149
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineApps(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;)Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;->onStoreQuerySuccess(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;Lcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    const-string v1, ""

    invoke-static {v0, p2, p1, v1, p0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->queryStore(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;ILjava/lang/String;Lretrofit/Callback;)V

    goto :goto_0
.end method

.method public onSaveAction(Lcom/vectorwatch/android/models/StoreElement;ILcom/vectorwatch/android/models/cloud/AppsOption;)V
    .locals 10
    .param p1, "item"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "positionInList"    # I
    .param p3, "appsOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 58
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/NetworkUtils;->isOnline(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;->displayAlert(Ljava/lang/String;)V

    .line 138
    :goto_0
    return-void

    .line 63
    :cond_0
    new-instance v6, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 64
    .local v6, "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v1

    invoke-virtual {v6, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 65
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 67
    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

    if-ne p3, v1, :cond_7

    .line 68
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/managers/StreamsManager;->isStreamSaved(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v9

    .line 69
    .local v9, "isStreamSaved":Z
    if-eqz v9, :cond_1

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    if-eqz v9, :cond_3

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 70
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 71
    .local v8, "isInstalled":Z
    :cond_2
    :goto_1
    if-nez v8, :cond_6

    .line 73
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 74
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "DOWNLOAD: download in progress."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 76
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .end local v8    # "isInstalled":Z
    :cond_3
    move v8, v0

    .line 70
    goto :goto_1

    .line 78
    .restart local v8    # "isInstalled":Z
    :cond_4
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "DOWNLOAD: no download in progress."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 80
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->installStreamOffline(Lcom/vectorwatch/android/models/StoreElement;)V

    .line 85
    :goto_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 87
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 85
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 82
    :cond_5
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->installStreamFromStore(Lcom/vectorwatch/android/models/StoreElement;)V

    goto :goto_2

    .line 90
    :cond_6
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Delete stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/StreamsManager;->deleteStreamFromDatabase(Ljava/lang/String;Landroid/content/Context;)Z

    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 95
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 93
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 98
    .end local v8    # "isInstalled":Z
    .end local v9    # "isStreamSaved":Z
    :cond_7
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v6, v1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->isAppInstalled(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)Z

    move-result v7

    .line 99
    .local v7, "isAppSaved":Z
    if-eqz v7, :cond_8

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_8
    if-eqz v7, :cond_a

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 100
    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 101
    .restart local v8    # "isInstalled":Z
    :cond_9
    :goto_3
    if-nez v8, :cond_e

    .line 102
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 103
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "DOWNLOAD: download in progress."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 105
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/events/DownloadInProgressEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .end local v8    # "isInstalled":Z
    :cond_a
    move v8, v0

    .line 100
    goto :goto_3

    .line 107
    .restart local v8    # "isInstalled":Z
    :cond_b
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v1, "DOWNLOAD: no download in progress."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 109
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0, p1, p3}, Lcom/vectorwatch/android/utils/AppInstallManager;->installAppOffline(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 114
    :goto_4
    sget-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    if-ne p3, v0, :cond_d

    .line 115
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 117
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 115
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 111
    :cond_c
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->installAppFromStore(Lcom/vectorwatch/android/models/StoreElement;)V

    goto :goto_4

    .line 119
    :cond_d
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_APP:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 121
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 119
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 125
    :cond_e
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v6, p2, v0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->uninstallAppFromWatch(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V

    .line 127
    sget-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    if-ne p3, v0, :cond_f

    .line 128
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 130
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 128
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 132
    :cond_f
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_APP:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 134
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v3

    .line 132
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto/16 :goto_0
.end method

.method public success(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;)V
    .locals 2
    .param p1, "storeQuery"    # Lcom/vectorwatch/android/models/StoreQuery;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->mAppsOption:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-interface {v0, p1, p2, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StorePresenter$Interactor;->onStoreQuerySuccess(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;Lcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 165
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 33
    check-cast p1, Lcom/vectorwatch/android/models/StoreQuery;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreInteractorImpl;->success(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;)V

    return-void
.end method
