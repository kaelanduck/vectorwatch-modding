.class public interface abstract Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;
.super Ljava/lang/Object;
.source "StoreMainPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Interactor"
.end annotation


# virtual methods
.method public abstract onRecommendedFail(Lretrofit/RetrofitError;)V
.end method

.method public abstract onRecommendedSuccess(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;Lretrofit/client/Response;)V
.end method

.method public abstract onStoreQueryFailure(Lretrofit/RetrofitError;)V
.end method

.method public abstract onStoreQuerySuccess(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;Lcom/vectorwatch/android/models/cloud/AppsOption;)V
.end method

.method public abstract recommendedOfflineSlide(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;)V
.end method
