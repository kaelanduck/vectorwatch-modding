.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;
.super Ljava/lang/Object;
.source "StoreMainInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractor;
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractor;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;",
        ">;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;)Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;

    return-object v0
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;->onRecommendedFail(Lretrofit/RetrofitError;)V

    .line 142
    return-void
.end method

.method public getStorePlaces()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 42
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v8, "places":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;>;"
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    const v2, 0x7f09024e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    const v3, 0x7f090098

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    const v2, 0x7f09024f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    const v3, 0x7f090130

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    const v2, 0x7f09024d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    const v3, 0x7f09006b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getStoreSlideshow(Landroid/content/Context;Lretrofit/Callback;)V

    .line 52
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    const-string v2, ""

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;)V

    invoke-static {v0, v1, v6, v2, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->queryStore(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;ILjava/lang/String;Lretrofit/Callback;)V

    .line 68
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    const-string v2, ""

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$2;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;)V

    invoke-static {v0, v1, v6, v2, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->queryStore(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;ILjava/lang/String;Lretrofit/Callback;)V

    .line 84
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

    const-string v2, ""

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$3;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;)V

    invoke-static {v0, v1, v6, v2, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->queryStore(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;ILjava/lang/String;Lretrofit/Callback;)V

    .line 127
    :goto_0
    return-object v8

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineApps(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;)Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v9

    .line 104
    .local v9, "query":Lcom/vectorwatch/android/models/StoreQuery;
    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    .line 105
    .local v7, "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl1(Ljava/lang/String;)V

    .line 106
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl2(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl3(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineApps(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;)Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v9

    .line 111
    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    check-cast v7, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    .line 112
    .restart local v7    # "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl1(Ljava/lang/String;)V

    .line 113
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl2(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl3(Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineApps(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;)Lcom/vectorwatch/android/models/StoreQuery;

    move-result-object v9

    .line 118
    invoke-interface {v8, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    check-cast v7, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;

    .line 119
    .restart local v7    # "place":Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl1(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl2(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/model/StorePlace;->setImageUrl3(Ljava/lang/String;)V

    .line 123
    new-instance v10, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mContext:Landroid/content/Context;

    const v1, 0x7f0901d7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v10, v0, v13, v13}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;-><init>(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;Ljava/lang/String;)V

    .line 124
    .local v10, "slide":Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;

    invoke-interface {v0, v10}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;->recommendedOfflineSlide(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;)V

    goto/16 :goto_0
.end method

.method public success(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;Lretrofit/client/Response;)V
    .locals 3
    .param p1, "serverResponse"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 132
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;->getSlides()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;->getData()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;->getSlides()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;

    invoke-interface {v0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;->onRecommendedSuccess(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;Lretrofit/client/Response;)V

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;

    const-string v1, "Unexpected"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;->onRecommendedFail(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->success(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;Lretrofit/client/Response;)V

    return-void
.end method
