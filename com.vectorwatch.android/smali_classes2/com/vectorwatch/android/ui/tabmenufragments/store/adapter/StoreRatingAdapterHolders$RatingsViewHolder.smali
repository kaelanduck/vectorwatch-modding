.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;
.super Ljava/lang/Object;
.source "StoreRatingAdapterHolders.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ResourceType"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RatingsViewHolder"
.end annotation


# instance fields
.field public comment:Landroid/widget/TextView;

.field public date:Landroid/widget/TextView;

.field public from:Landroid/widget/TextView;

.field public rating:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

.field public time:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const v0, 0x7f100236

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->from:Landroid/widget/TextView;

    .line 24
    const v0, 0x7f10014b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->comment:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f1000eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->rating:Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    .line 26
    const v0, 0x7f100238

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->date:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f100237

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterHolders$RatingsViewHolder;->time:Landroid/widget/TextView;

    .line 28
    return-void
.end method
