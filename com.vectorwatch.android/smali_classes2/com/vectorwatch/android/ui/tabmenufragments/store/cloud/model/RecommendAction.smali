.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;
.super Ljava/lang/Object;
.source "RecommendAction.java"


# instance fields
.field private type:Ljava/lang/String;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;->type:Ljava/lang/String;

    .line 12
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;->value:Ljava/lang/String;

    .line 13
    return-void
.end method


# virtual methods
.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;->value:Ljava/lang/String;

    return-object v0
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;->type:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;->value:Ljava/lang/String;

    .line 29
    return-void
.end method
