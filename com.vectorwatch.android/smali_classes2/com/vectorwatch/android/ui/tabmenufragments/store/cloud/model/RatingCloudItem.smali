.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;
.super Ljava/lang/Object;
.source "RatingCloudItem.java"


# instance fields
.field private comment:Ljava/lang/String;

.field private ratingValue:F


# direct methods
.method public constructor <init>(Ljava/lang/String;F)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;
    .param p2, "ratingValue"    # F

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;->comment:Ljava/lang/String;

    .line 13
    iput p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;->ratingValue:F

    .line 14
    return-void
.end method


# virtual methods
.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getRatingValue()F
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;->ratingValue:F

    return v0
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;->comment:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setRatingValue(F)V
    .locals 0
    .param p1, "ratingValue"    # F

    .prologue
    .line 29
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;->ratingValue:F

    .line 30
    return-void
.end method
