.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;
.super Ljava/lang/Object;
.source "StorePlacesAdapterHolders.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ResourceType"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RowSimpleViewHolder"
.end annotation


# instance fields
.field public image1:Landroid/widget/ImageView;

.field public image2:Landroid/widget/ImageView;

.field public image3:Landroid/widget/ImageView;

.field public newView:Landroid/widget/RelativeLayout;

.field public summary:Landroid/widget/TextView;

.field public title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const v0, 0x7f10025e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->title:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f10025f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->summary:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f100260

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->image3:Landroid/widget/ImageView;

    .line 27
    const v0, 0x7f100261

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->image2:Landroid/widget/ImageView;

    .line 28
    const v0, 0x7f10025d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->image1:Landroid/widget/ImageView;

    .line 29
    const v0, 0x7f10023d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleViewHolder;->newView:Landroid/widget/RelativeLayout;

    .line 30
    return-void
.end method
