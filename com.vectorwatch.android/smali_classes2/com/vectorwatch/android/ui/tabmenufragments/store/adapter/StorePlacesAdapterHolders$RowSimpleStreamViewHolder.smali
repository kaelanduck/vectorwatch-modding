.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;
.super Ljava/lang/Object;
.source "StorePlacesAdapterHolders.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ResourceType"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RowSimpleStreamViewHolder"
.end annotation


# instance fields
.field public image1:Landroid/widget/ImageView;

.field public image2:Landroid/widget/ImageView;

.field public image3:Landroid/widget/ImageView;

.field public newView:Landroid/widget/RelativeLayout;

.field public summary:Landroid/widget/TextView;

.field public title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const v0, 0x7f10025e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->title:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f10025f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->summary:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f100260

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->image3:Landroid/widget/ImageView;

    .line 49
    const v0, 0x7f100261

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->image2:Landroid/widget/ImageView;

    .line 50
    const v0, 0x7f10025d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->image1:Landroid/widget/ImageView;

    .line 51
    const v0, 0x7f10023d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterHolders$RowSimpleStreamViewHolder;->newView:Landroid/widget/RelativeLayout;

    .line 52
    return-void
.end method
