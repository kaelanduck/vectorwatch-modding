.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;
.super Ljava/lang/Object;
.source "RecommendData.java"


# instance fields
.field private slides:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 12
    .local p1, "slides":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;->slides:Ljava/util/List;

    .line 14
    return-void
.end method


# virtual methods
.method public getSlides()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;->slides:Ljava/util/List;

    return-object v0
.end method

.method public setSlides(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "slides":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendData;->slides:Ljava/util/List;

    .line 22
    return-void
.end method
