.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "StoreReportAbuseActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;Ljava/lang/Object;)V
    .locals 3
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;, "TT;"
    const v2, 0x7f10014b

    .line 11
    const-string v1, "field \'mCommentEdit\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mCommentEdit\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->mCommentEdit:Landroid/widget/EditText;

    .line 13
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;, "TT;"
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;->mCommentEdit:Landroid/widget/EditText;

    .line 17
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreReportAbuseActivity;)V

    return-void
.end method
