.class public Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;
.super Ljava/lang/Object;
.source "RecommendSlide.java"


# instance fields
.field private action:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;

.field private backgroundImageUrl:Ljava/lang/String;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;
    .param p3, "bgImageUrl"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->text:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->action:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;

    .line 15
    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->backgroundImageUrl:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getAction()Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->action:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;

    return-object v0
.end method

.method public getBgImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->backgroundImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->text:Ljava/lang/String;

    return-object v0
.end method

.method public setAction(Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;)V
    .locals 0
    .param p1, "action"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->action:Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendAction;

    .line 32
    return-void
.end method

.method public setBgImageUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "bgImageUrl"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->backgroundImageUrl:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendSlide;->text:Ljava/lang/String;

    .line 24
    return-void
.end method
