.class Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$1;
.super Ljava/lang/Object;
.source "StoreMainInteractorImpl.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->getStorePlaces()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/StoreQuery;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;)Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;->onStoreQueryFailure(Lretrofit/RetrofitError;)V

    .line 65
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;)V
    .locals 2
    .param p1, "storeQuery"    # Lcom/vectorwatch/android/models/StoreQuery;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreQuery;->getData()Lcom/vectorwatch/android/models/Apps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl;)Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-interface {v0, p1, p2, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/presenter/StoreMainPresenter$Interactor;->onStoreQuerySuccess(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;Lcom/vectorwatch/android/models/cloud/AppsOption;)V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    const-string v0, "Unexpected"

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$1;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 52
    check-cast p1, Lcom/vectorwatch/android/models/StoreQuery;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/store/interactor/StoreMainInteractorImpl$1;->success(Lcom/vectorwatch/android/models/StoreQuery;Lretrofit/client/Response;)V

    return-void
.end method
