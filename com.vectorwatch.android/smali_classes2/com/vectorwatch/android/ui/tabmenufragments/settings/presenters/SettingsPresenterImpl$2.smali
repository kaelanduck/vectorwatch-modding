.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;
.super Ljava/lang/Object;
.source "SettingsPresenterImpl.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onResetSettingsClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 212
    const-string v2, "account_update_type"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    .line 213
    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v3

    .line 212
    invoke-static {v2, v4, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 216
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->resetUserSettings(Landroid/content/Context;)V

    .line 217
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->resetDatabase(Landroid/content/Context;)V

    .line 219
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAccountAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "username":Ljava/lang/String;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    .line 221
    invoke-virtual {v2}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 220
    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->userLogOut(Ljava/lang/String;Landroid/accounts/AccountManager;)Z

    move-result v0

    .line 223
    .local v0, "isLoggedOut":Z
    if-eqz v0, :cond_0

    .line 225
    const-string v2, "account_update_type"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    .line 226
    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v3

    .line 225
    invoke-static {v2, v4, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 229
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAccountAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 230
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLoggedInStatus(ZLandroid/content/Context;)V

    .line 232
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->logoutDrivenResets(Landroid/content/Context;)Z

    .line 235
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->clearLiveStreamManager()V

    .line 238
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAccountAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 239
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLoggedInStatus(ZLandroid/content/Context;)V

    .line 240
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->forgetWatch(Landroid/content/Context;)V

    .line 241
    return-void
.end method
