.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;
.super Ljava/lang/Object;
.source "ActivityInfoFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

.field final synthetic val$mainValueInputField:Landroid/widget/EditText;

.field final synthetic val$secondaryValueInputField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$mainValueInputField:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$secondaryValueInputField:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/16 v5, 0x30

    const/4 v6, 0x1

    .line 279
    :try_start_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$secondaryValueInputField:Landroid/widget/EditText;

    .line 280
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 281
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f090103

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    .line 282
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 330
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$mainValueInputField:Landroid/widget/EditText;

    .line 287
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_1

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$mainValueInputField:Landroid/widget/EditText;

    .line 288
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v6, :cond_1

    .line 289
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f090106

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    .line 290
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "exception":Ljava/lang/NumberFormatException;
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f090107

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    .line 328
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 294
    .end local v0    # "exception":Ljava/lang/NumberFormatException;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$secondaryValueInputField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$secondaryValueInputField:Landroid/widget/EditText;

    .line 295
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$secondaryValueInputField:Landroid/widget/EditText;

    .line 296
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v6, :cond_2

    .line 297
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f090106

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    .line 298
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 302
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$secondaryValueInputField:Landroid/widget/EditText;

    .line 303
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$mainValueInputField:Landroid/widget/EditText;

    .line 304
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_3

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$secondaryValueInputField:Landroid/widget/EditText;

    .line 305
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v5, :cond_3

    .line 306
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0900c0

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    .line 307
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 312
    :cond_3
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 313
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 319
    .local v1, "feet":I
    :goto_1
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$secondaryValueInputField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 320
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->val$secondaryValueInputField:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 325
    .local v2, "inches":I
    :goto_2
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v3, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$600(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;II)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 316
    .end local v1    # "feet":I
    .end local v2    # "inches":I
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "feet":I
    goto :goto_1

    .line 323
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "inches":I
    goto :goto_2
.end method
