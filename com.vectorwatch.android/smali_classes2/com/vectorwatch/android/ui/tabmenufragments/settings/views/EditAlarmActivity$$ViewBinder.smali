.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "EditAlarmActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;, "TT;"
    const v6, 0x7f1002c4

    const v5, 0x7f1002c3

    const v4, 0x7f1002c1

    const v3, 0x7f1002bf

    const v2, 0x7f1001b5

    .line 11
    const-string v1, "field \'mTimePicker\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mTimePicker\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TimePicker;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mTimePicker:Landroid/widget/TimePicker;

    .line 13
    const-string v1, "field \'mEditAlarmName\' and method \'onEditNameClicked\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mEditAlarmName\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mEditAlarmName:Landroid/widget/TextView;

    .line 15
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    const-string v1, "field \'mRepeatAlarm\' and method \'onEditRepeatPatternClicked\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 24
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mRepeatAlarm\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mRepeatAlarm:Landroid/widget/TextView;

    .line 25
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder$2;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    const-string v1, "field \'mDelete\' and method \'onDeleteAlarm\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 34
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mDelete\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mDelete:Landroid/widget/TextView;

    .line 35
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder$3;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    const-string v1, "field \'mLinkState\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 44
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mLinkState\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mLinkState:Landroid/widget/TextView;

    .line 45
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;, "TT;"
    const/4 v0, 0x0

    .line 48
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mTimePicker:Landroid/widget/TimePicker;

    .line 49
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mEditAlarmName:Landroid/widget/TextView;

    .line 50
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mRepeatAlarm:Landroid/widget/TextView;

    .line 51
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mDelete:Landroid/widget/TextView;

    .line 52
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;->mLinkState:Landroid/widget/TextView;

    .line 53
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/EditAlarmActivity;)V

    return-void
.end method
