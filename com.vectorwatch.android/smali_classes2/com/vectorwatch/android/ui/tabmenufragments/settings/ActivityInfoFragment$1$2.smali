.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$2;
.super Ljava/lang/Object;
.source "ActivityInfoFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 144
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$200()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "Gender selected and clicked ok."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)V

    .line 153
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$002(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)I

    .line 154
    return-void
.end method
