.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;
.super Ljava/lang/Object;
.source "SystemNotificationEnablerView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v0, 0x0

    .line 70
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->checkWatchConnectionWithAlert(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 73
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->access$200()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preferences - App enabler - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 74
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSystemNotification(ZLandroid/content/Context;)V

    .line 76
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    const-string v2, "system_not"

    invoke-static {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;Ljava/lang/String;)V

    .line 78
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSystemNotification(ZLandroid/content/Context;)V

    .line 81
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 84
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/SystemNotificationsEnabledEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/events/SystemNotificationsEnabledEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SystemNotificationEnablerView;)Landroid/widget/Switch;

    move-result-object v1

    if-nez p2, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_0
.end method
