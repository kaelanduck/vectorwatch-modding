.class public final enum Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;
.super Ljava/lang/Enum;
.source "SettingsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SettingsOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum ABOUT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum ACCOUNT_PROFILE:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum ACTIVITY_INFO:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum ALARMS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum ALERTS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum CONTEXTUAL:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum LOGOUT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum NEWSLETTER:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum RESET_SETTINGS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum SUPPORT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum TOUR:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum UPDATE_WATCH:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

.field public static final enum WATCH:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "ACCOUNT_PROFILE"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ACCOUNT_PROFILE:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 9
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "ACTIVITY_INFO"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ACTIVITY_INFO:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 10
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "ALERTS"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ALERTS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 11
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "CONTEXTUAL"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->CONTEXTUAL:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 12
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "WATCH"

    invoke-direct {v0, v1, v7}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->WATCH:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 13
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "ALARMS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ALARMS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 14
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "SUPPORT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->SUPPORT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 15
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "NEWSLETTER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->NEWSLETTER:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 16
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "TOUR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->TOUR:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 17
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "UPDATE_WATCH"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->UPDATE_WATCH:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 18
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "RESET_SETTINGS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->RESET_SETTINGS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 19
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "LOGOUT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->LOGOUT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 20
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    const-string v1, "ABOUT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ABOUT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .line 7
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ACCOUNT_PROFILE:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ACTIVITY_INFO:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ALERTS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->CONTEXTUAL:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->WATCH:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ALARMS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->SUPPORT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->NEWSLETTER:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->TOUR:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->UPDATE_WATCH:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->RESET_SETTINGS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->LOGOUT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ABOUT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->$VALUES:[Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->$VALUES:[Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    return-object v0
.end method
