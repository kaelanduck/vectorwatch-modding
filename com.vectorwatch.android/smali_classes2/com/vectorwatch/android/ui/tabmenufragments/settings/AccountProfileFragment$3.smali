.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;
.super Ljava/lang/Object;
.source "AccountProfileFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 166
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v3

    if-nez v3, :cond_0

    .line 167
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090063

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x5dc

    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 242
    :goto_0
    return-void

    .line 170
    :cond_0
    new-instance v2, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 171
    .local v2, "inputField":Landroid/widget/EditText;
    const/16 v3, 0x1001

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 173
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getGreetingUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "currentName":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 175
    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 180
    :goto_1
    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$1;

    invoke-direct {v3, p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;Landroid/widget/EditText;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 201
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0c00c5

    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v4, 0x7f09008b

    .line 203
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 204
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0900af

    new-instance v5, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$3;

    invoke-direct {v5, p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;Landroid/widget/EditText;)V

    .line 205
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f09008a

    new-instance v5, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$2;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;)V

    .line 214
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 218
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 220
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 224
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 225
    invoke-virtual {v2}, Landroid/widget/EditText;->setSingleLine()V

    .line 226
    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$4;

    invoke-direct {v3, p0, v2, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;Landroid/widget/EditText;Landroid/app/AlertDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 241
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    .line 177
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    :cond_1
    const v3, 0x7f0901b6

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_1
.end method
