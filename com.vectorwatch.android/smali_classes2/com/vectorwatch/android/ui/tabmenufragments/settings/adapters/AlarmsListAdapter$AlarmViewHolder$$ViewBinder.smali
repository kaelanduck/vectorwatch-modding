.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;
.super Ljava/lang/Object;
.source "AlarmsListAdapter$AlarmViewHolder$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;Ljava/lang/Object;)V
    .locals 5
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;, "TT;"
    const v4, 0x7f10018b

    const v3, 0x7f10018a

    const v2, 0x7f100189

    .line 11
    const-string v1, "field \'mToggle\' and method \'onToggleClicked\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mToggle\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mToggle:Landroid/widget/Switch;

    .line 13
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    const-string v1, "field \'mTime\' and method \'onAlarmClicked\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 22
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mTime\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mTime:Landroid/widget/TextView;

    .line 23
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder$2;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    const-string v1, "field \'mName\' and method \'onAlarmClicked\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 32
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mName\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mName:Landroid/widget/TextView;

    .line 33
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder$3;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;, "TT;"
    const/4 v0, 0x0

    .line 44
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mToggle:Landroid/widget/Switch;

    .line 45
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mTime:Landroid/widget/TextView;

    .line 46
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mName:Landroid/widget/TextView;

    .line 47
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;)V

    return-void
.end method
