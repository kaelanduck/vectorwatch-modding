.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;
.super Ljava/lang/Object;
.source "NewsletterInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractor;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mContext:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;

    return-object v0
.end method

.method private getNewsletterSettingsList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 81
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v2, "newsletterItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/NewsletterItem;>;"
    new-instance v0, Lcom/vectorwatch/android/models/NewsletterItem;

    const-string v3, "MOBILE_APP_NEWSLETTER_LIST"

    const-string v4, "pref_news_app"

    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mContext:Landroid/content/Context;

    .line 84
    invoke-static {v4, v6, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v4

    invoke-direct {v0, v3, v4}, Lcom/vectorwatch/android/models/NewsletterItem;-><init>(Ljava/lang/String;Z)V

    .line 85
    .local v0, "item":Lcom/vectorwatch/android/models/NewsletterItem;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    new-instance v1, Lcom/vectorwatch/android/models/NewsletterItem;

    const-string v3, "MOBILE_APP_MARKETING_NEWSLETTER_LIST"

    const-string v4, "pref_news_news"

    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mContext:Landroid/content/Context;

    .line 87
    invoke-static {v4, v6, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v4

    invoke-direct {v1, v3, v4}, Lcom/vectorwatch/android/models/NewsletterItem;-><init>(Ljava/lang/String;Z)V

    .line 88
    .local v1, "item2":Lcom/vectorwatch/android/models/NewsletterItem;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    return-object v2
.end method


# virtual methods
.method public changeNewsletterOptionsApp(Z)V
    .locals 3
    .param p1, "isChecked"    # Z

    .prologue
    .line 62
    const-string v1, "pref_news_app"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 63
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->getNewsletterSettingsList()Ljava/util/List;

    move-result-object v0

    .line 64
    .local v0, "newsletterItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/NewsletterItem;>;"
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl$2;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;)V

    invoke-static {v1, v0, v2}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->setNewsletterSettings(Landroid/content/Context;Ljava/util/List;Lretrofit/Callback;)V

    .line 75
    return-void
.end method

.method public changeNewsletterOptionsNews(Z)V
    .locals 3
    .param p1, "isChecked"    # Z

    .prologue
    .line 41
    const-string v1, "pref_news_news"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 42
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->getNewsletterSettingsList()Ljava/util/List;

    move-result-object v0

    .line 43
    .local v0, "newsletterItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/NewsletterItem;>;"
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl$1;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;)V

    invoke-static {v1, v0, v2}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->setNewsletterSettings(Landroid/content/Context;Ljava/util/List;Lretrofit/Callback;)V

    .line 54
    return-void
.end method
