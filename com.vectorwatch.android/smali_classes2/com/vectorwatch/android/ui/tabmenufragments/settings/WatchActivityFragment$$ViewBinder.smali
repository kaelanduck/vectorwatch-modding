.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder;
.super Ljava/lang/Object;
.source "WatchActivityFragment$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;, "TT;"
    const v6, 0x7f1001db

    const v5, 0x7f1001da

    const v4, 0x7f1001d9

    const v2, 0x7f1001d8

    const v3, 0x7f1001b5

    .line 11
    const-string v1, "field \'mSecondHand\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mSecondHand\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSecondHand:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 13
    const-string v1, "field \'mDurationBacklightText\' and method \'setBackLightDuration\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mDurationBacklightText\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mDurationBacklightText:Landroid/widget/TextView;

    .line 15
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    const-string v1, "field \'mSeekBar\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 24
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mSeekBar\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSeekBar:Landroid/widget/SeekBar;

    .line 25
    const v1, 0x7f1001dc

    const-string v2, "field \'mLinkLostIconSettings\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 26
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001dc

    const-string v2, "field \'mLinkLostIconSettings\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkLostIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 27
    const v1, 0x7f1001dd

    const-string v2, "field \'mLinkLostNotificationSettings\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 28
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001dd

    const-string v2, "field \'mLinkLostNotificationSettings\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkLostNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 29
    const v1, 0x7f1001df

    const-string v2, "field \'mLowBatteryIconSettings\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 30
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001df

    const-string v2, "field \'mLowBatteryIconSettings\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 31
    const v1, 0x7f1001e0

    const-string v2, "field \'mLowBatteryNotificationSettings\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 32
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001e0

    const-string v2, "field \'mLowBatteryNotificationSettings\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 33
    const-string v1, "field \'mLinkSection\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 34
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mLinkSection\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 35
    const v1, 0x7f1001de

    const-string v2, "field \'mBatterySection\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 36
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1001de

    const-string v2, "field \'mBatterySection\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mBatterySection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 37
    const-string v1, "field \'mLinkStateImage\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 38
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mLinkStateImage\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 39
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;, "TT;"
    const/4 v0, 0x0

    .line 42
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSecondHand:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    .line 43
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mDurationBacklightText:Landroid/widget/TextView;

    .line 44
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mSeekBar:Landroid/widget/SeekBar;

    .line 45
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkLostIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 46
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkLostNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 47
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryIconSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 48
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 49
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 50
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mBatterySection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 51
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 52
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V

    return-void
.end method
