.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;
.super Ljava/lang/Object;
.source "ActivityInfoFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

.field final synthetic val$mainValueInputField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;Landroid/widget/EditText;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v5, 0x1

    .line 215
    :try_start_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 216
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f090103

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 217
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 231
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-ne v2, v3, :cond_1

    .line 221
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f090106

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 222
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "exception":Ljava/lang/NumberFormatException;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f090107

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 229
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 225
    .end local v0    # "exception":Ljava/lang/NumberFormatException;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;->val$mainValueInputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 226
    .local v1, "height":I
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v2, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$500(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
