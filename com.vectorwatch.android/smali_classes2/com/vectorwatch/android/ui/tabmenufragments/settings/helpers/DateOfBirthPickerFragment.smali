.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/DateOfBirthPickerFragment;
.super Landroid/app/DialogFragment;
.source "DateOfBirthPickerFragment.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 30
    .local v6, "c":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/DateOfBirthPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoDateOfBirth(Landroid/content/Context;)J

    move-result-wide v8

    .line 32
    .local v8, "dob":J
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-eqz v0, :cond_0

    .line 34
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 40
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 41
    .local v3, "year":I
    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 42
    .local v4, "month":I
    const/4 v0, 0x5

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 45
    .local v5, "day":I
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/DateOfBirthPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    return-object v0

    .line 37
    .end local v3    # "year":I
    .end local v4    # "month":I
    .end local v5    # "day":I
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    goto :goto_0
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 8
    .param p1, "view"    # Landroid/widget/DatePicker;
    .param p2, "year"    # I
    .param p3, "month"    # I
    .param p4, "day"    # I

    .prologue
    const/4 v5, 0x1

    .line 50
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 51
    .local v2, "dateOfBirth":Ljava/util/Calendar;
    invoke-virtual {v2, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 53
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 55
    .local v1, "currentDate":Ljava/util/Calendar;
    invoke-static {v2, v1}, Lcom/vectorwatch/android/utils/Helpers;->getYearsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v0

    .line 58
    .local v0, "age":I
    if-ge v0, v5, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/DateOfBirthPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f090075

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 64
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/AgeChangedEvent;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-direct {v4, v0, v6, v7}, Lcom/vectorwatch/android/events/AgeChangedEvent;-><init>(IJ)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
