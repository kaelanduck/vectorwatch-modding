.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$8;
.super Ljava/lang/Object;
.source "ContextualFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->valuesSetUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$8;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 273
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$8;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->checkWatchConnectionWithAlert(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    const-string v0, "prefs_offline_badge"

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$8;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 275
    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v1

    .line 274
    invoke-static {v0, p2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 276
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$8;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$1000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    .line 280
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$8;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$1100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    move-result-object v1

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
