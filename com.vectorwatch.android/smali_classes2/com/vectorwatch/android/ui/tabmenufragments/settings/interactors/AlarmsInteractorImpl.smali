.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;
.super Ljava/lang/Object;
.source "AlarmsInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractor;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;

.field private mRealm:Lio/realm/Realm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mContext:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;

    .line 30
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mRealm:Lio/realm/Realm;

    .line 31
    return-void
.end method


# virtual methods
.method public createAlarmPressed()V
    .locals 3

    .prologue
    .line 47
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mRealm:Lio/realm/Realm;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/database/DatabaseManager;->getAllAlarms(Lio/realm/Realm;)Ljava/util/List;

    move-result-object v0

    .line 49
    .local v0, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    .line 50
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;->notifyAlarmLimitStatus(Z)V

    .line 54
    :goto_0
    return-void

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;->notifyAlarmLimitStatus(Z)V

    goto :goto_0
.end method

.method public getAllAlarms()V
    .locals 3

    .prologue
    .line 35
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mRealm:Lio/realm/Realm;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/database/DatabaseManager;->getAllAlarms(Lio/realm/Realm;)Ljava/util/List;

    move-result-object v0

    .line 36
    .local v0, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;

    invoke-interface {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/AlarmsPresenter$Interactor;->sendAlarms(Ljava/util/List;)V

    .line 37
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mRealm:Lio/realm/Realm;

    invoke-virtual {v0}, Lio/realm/Realm;->close()V

    .line 59
    return-void
.end method

.method public updateAlarmStatus(JB)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "enabledValue"    # B

    .prologue
    .line 41
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ALARM - Update status for alarm with id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/AlarmsInteractorImpl;->mRealm:Lio/realm/Realm;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/vectorwatch/android/database/DatabaseManager;->updateAlarmStatus(Lio/realm/Realm;JB)V

    .line 43
    return-void
.end method
