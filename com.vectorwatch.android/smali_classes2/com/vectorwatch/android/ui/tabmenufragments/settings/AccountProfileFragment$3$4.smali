.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$4;
.super Ljava/lang/Object;
.source "AccountProfileFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;

.field final synthetic val$alertDialog:Landroid/app/AlertDialog;

.field final synthetic val$inputField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;Landroid/widget/EditText;Landroid/app/AlertDialog;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$4;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$4;->val$inputField:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$4;->val$alertDialog:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 229
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x42

    if-ne p2, v1, :cond_0

    .line 231
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Pressed enter."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 232
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$4;->val$inputField:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$4;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-static {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$4;->val$alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 235
    const/4 v1, 0x1

    .line 237
    .end local v0    # "name":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
