.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;
.super Ljava/lang/Object;
.source "AboutFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->switchBaseUrl()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 241
    const-string v0, "flag_send_parse_installation_id"

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    .line 242
    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 241
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 244
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->setApiUri(Ljava/lang/String;)V

    .line 245
    const-string v0, "pref_api_uri_stage"

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 246
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$2;)V

    invoke-static {v0, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->updateAccountInfo(Landroid/content/Context;Lretrofit/Callback;)V

    .line 259
    return-void
.end method
