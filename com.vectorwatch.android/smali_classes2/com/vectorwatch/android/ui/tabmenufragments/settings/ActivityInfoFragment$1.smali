.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;
.super Ljava/lang/Object;
.source "ActivityInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 122
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 162
    :goto_0
    return-void

    .line 126
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0c00c5

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 129
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$002(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)I

    .line 131
    const v1, 0x7f09021e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0003

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .line 132
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)I

    move-result v3

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$3;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0901d8

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$2;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;)V

    .line 141
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09008a

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;)V

    .line 155
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 161
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
