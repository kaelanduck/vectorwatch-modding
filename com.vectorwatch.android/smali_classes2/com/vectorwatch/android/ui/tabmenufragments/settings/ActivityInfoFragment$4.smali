.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;
.super Ljava/lang/Object;
.source "ActivityInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, -0x1

    .line 352
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v4

    if-nez v4, :cond_0

    .line 353
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090063

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x5dc

    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 418
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v1

    .line 358
    .local v1, "isMetric":Z
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v7, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v4, 0x0

    check-cast v4, Ljava/lang/String;

    invoke-static {v5, v6, v7, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 362
    new-instance v0, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 366
    .local v0, "inputField":Landroid/widget/EditText;
    if-eqz v1, :cond_1

    .line 367
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v2

    .line 373
    .local v2, "previouslyRegisteredWeight":I
    :goto_1
    if-eq v2, v8, :cond_2

    .line 374
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 375
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 384
    .end local v3    # "text":Ljava/lang/String;
    :goto_2
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setInputType(I)V

    .line 386
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v6, 0x7f0c00c5

    invoke-direct {v5, v4, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    if-eqz v1, :cond_4

    const v4, 0x7f090228

    .line 388
    :goto_3
    invoke-virtual {v5, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 389
    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0900af

    new-instance v6, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;

    invoke-direct {v6, p0, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;Landroid/widget/EditText;)V

    .line 390
    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f09008a

    new-instance v6, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$1;

    invoke-direct {v6, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;)V

    .line 411
    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 415
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 417
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    .line 369
    .end local v2    # "previouslyRegisteredWeight":I
    :cond_1
    const-string v4, "activity_info_weight_lbs"

    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .line 370
    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v8, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v2

    .restart local v2    # "previouslyRegisteredWeight":I
    goto :goto_1

    .line 377
    :cond_2
    if-eqz v1, :cond_3

    .line 378
    const v4, 0x7f090122

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_2

    .line 380
    :cond_3
    const v4, 0x7f090121

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_2

    .line 386
    :cond_4
    const v4, 0x7f090227

    goto :goto_3
.end method
