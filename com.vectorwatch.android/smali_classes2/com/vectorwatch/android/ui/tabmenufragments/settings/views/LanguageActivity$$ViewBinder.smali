.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "LanguageActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;, "TT;"
    const v6, 0x7f1000fd

    const v5, 0x7f1000fc

    const v4, 0x7f1000fb

    const v2, 0x7f1000fa

    const v3, 0x7f1000f9

    .line 11
    const-string v1, "field \'mDefault\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mDefault\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mDefault:Landroid/widget/RadioButton;

    .line 13
    const-string v1, "field \'mEnglish\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mEnglish\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mEnglish:Landroid/widget/RadioButton;

    .line 15
    const-string v1, "field \'mRomanian\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 16
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mRomanian\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mRomanian:Landroid/widget/RadioButton;

    .line 17
    const-string v1, "field \'mGerman\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 18
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mGerman\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mGerman:Landroid/widget/RadioButton;

    .line 19
    const v1, 0x7f1000fe

    const-string v2, "field \'mFrench\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 20
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1000fe

    const-string v2, "field \'mFrench\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mFrench:Landroid/widget/RadioButton;

    .line 21
    const v1, 0x7f1000ff

    const-string v2, "field \'mSpanish\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 22
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f1000ff

    const-string v2, "field \'mSpanish\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mSpanish:Landroid/widget/RadioButton;

    .line 23
    const v1, 0x7f100100

    const-string v2, "field \'mTurkish\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 24
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100100

    const-string v2, "field \'mTurkish\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mTurkish:Landroid/widget/RadioButton;

    .line 25
    const v1, 0x7f100101

    const-string v2, "field \'mDutch\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 26
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100101

    const-string v2, "field \'mDutch\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mDutch:Landroid/widget/RadioButton;

    .line 27
    const-string v1, "field \'mRadioGroup\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 28
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mRadioGroup\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mRadioGroup:Landroid/widget/RadioGroup;

    .line 29
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;, "TT;"
    const/4 v0, 0x0

    .line 32
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mDefault:Landroid/widget/RadioButton;

    .line 33
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mEnglish:Landroid/widget/RadioButton;

    .line 34
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mRomanian:Landroid/widget/RadioButton;

    .line 35
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mGerman:Landroid/widget/RadioButton;

    .line 36
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mFrench:Landroid/widget/RadioButton;

    .line 37
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mSpanish:Landroid/widget/RadioButton;

    .line 38
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mTurkish:Landroid/widget/RadioButton;

    .line 39
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mDutch:Landroid/widget/RadioButton;

    .line 40
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;->mRadioGroup:Landroid/widget/RadioGroup;

    .line 41
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/LanguageActivity;)V

    return-void
.end method
