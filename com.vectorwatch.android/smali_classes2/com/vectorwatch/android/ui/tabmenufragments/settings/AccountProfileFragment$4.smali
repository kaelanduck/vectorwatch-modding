.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;
.super Ljava/lang/Object;
.source "AccountProfileFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    .prologue
    .line 245
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 248
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 249
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 285
    :goto_0
    return-void

    .line 252
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0c00c5

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 255
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$302(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;I)I

    .line 257
    const v1, 0x7f09021f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x7f0a0000

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    .line 259
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)I

    move-result v3

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4$3;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;)V

    .line 258
    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0901d8

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4$2;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;)V

    .line 268
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09008a

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$4;)V

    .line 278
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 284
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
