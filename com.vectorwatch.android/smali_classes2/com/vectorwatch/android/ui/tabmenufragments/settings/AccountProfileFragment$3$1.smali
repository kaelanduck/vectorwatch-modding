.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$1;
.super Ljava/lang/Object;
.source "AccountProfileFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;

.field final synthetic val$inputField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;Landroid/widget/EditText;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$1;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$1;->val$inputField:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    .line 198
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "i"    # I
    .param p3, "i1"    # I
    .param p4, "i2"    # I

    .prologue
    .line 184
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "i"    # I
    .param p3, "i1"    # I
    .param p4, "i2"    # I

    .prologue
    const/16 v3, 0xe

    .line 189
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 190
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$1;->val$inputField:Landroid/widget/EditText;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$3$1;->val$inputField:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 193
    :cond_0
    return-void
.end method
