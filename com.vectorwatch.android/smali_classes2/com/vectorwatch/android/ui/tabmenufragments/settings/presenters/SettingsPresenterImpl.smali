.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;
.super Ljava/lang/Object;
.source "SettingsPresenterImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;


# static fields
.field private static final CLICKS_TO_UNLOCK_HIDDEN_MENU:I = 0xa

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCounterClickOnAppVersion:I

.field private mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/SettingsView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/SettingsView;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "view"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/SettingsView;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mCounterClickOnAppVersion:I

    .line 47
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    .line 48
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/SettingsView;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private onAboutClicked()V
    .locals 5

    .prologue
    .line 294
    const-string v2, "account_update_type"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 296
    .local v1, "userUpdateType":Ljava/lang/String;
    if-eqz v1, :cond_0

    sget-object v2, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->PRODUCTION:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->getVal()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->STAGE:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    .line 297
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->getVal()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 300
    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mCounterClickOnAppVersion:I

    const/16 v3, 0xa

    if-lt v2, v3, :cond_1

    .line 301
    const/4 v2, 0x0

    iput v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mCounterClickOnAppVersion:I

    .line 302
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 303
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 308
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mCounterClickOnAppVersion:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mCounterClickOnAppVersion:I

    goto :goto_0
.end method

.method private onAccountProfileClicked()V
    .locals 3

    .prologue
    .line 106
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 108
    return-void
.end method

.method private onActivityInfoClicked()V
    .locals 3

    .prologue
    .line 114
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 116
    return-void
.end method

.method private onAlarmsClicked()V
    .locals 3

    .prologue
    .line 143
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/AlarmActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 145
    return-void
.end method

.method private onAlertsClicked()V
    .locals 3

    .prologue
    .line 122
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 123
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 124
    return-void
.end method

.method private onContextualClicked()V
    .locals 3

    .prologue
    .line 130
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 131
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 132
    return-void
.end method

.method private onLogoutClicked()V
    .locals 4

    .prologue
    .line 257
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getLoggedInStatus(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 286
    :goto_0
    return-void

    .line 262
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const v2, 0x7f0c00c5

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 264
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f09019f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0901d8

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$4;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)V

    .line 265
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09008a

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$3;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)V

    .line 280
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 285
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private onNewsletterClicked()V
    .locals 3

    .prologue
    .line 156
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NewsletterActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 158
    return-void
.end method

.method private onResetSettingsClicked()V
    .locals 5

    .prologue
    .line 198
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "bondedWatchName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 202
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/SettingsView;

    invoke-interface {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/SettingsView;->triggerPairScreen()V

    .line 251
    :goto_0
    return-void

    .line 206
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const v3, 0x7f0c00c5

    invoke-direct {v1, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 207
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f09027a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0901d8

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)V

    .line 208
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f09008a

    new-instance v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$1;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)V

    .line 243
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 249
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private onSupportClicked()V
    .locals 3

    .prologue
    .line 151
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/bug_reporting/SupportActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 153
    return-void
.end method

.method private onTourClicked()V
    .locals 3

    .prologue
    .line 164
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "simple_exit"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 166
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 167
    return-void
.end method

.method private onUpdateWatchClicked()V
    .locals 4

    .prologue
    const/16 v3, 0x5dc

    .line 173
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->isInternetEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 174
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->isMobilePairedToWatch(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "TEST - settings request update search"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 177
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 178
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 179
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 192
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 189
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const v2, 0x7f09010f

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0
.end method

.method private onWatchClicked()V
    .locals 3

    .prologue
    .line 138
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 140
    return-void
.end method


# virtual methods
.method public onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V
    .locals 2
    .param p1, "option"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    .prologue
    .line 53
    sget-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$5;->$SwitchMap$com$vectorwatch$android$ui$tabmenufragments$settings$presenters$SettingsPresenter$SettingsOption:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 94
    :goto_0
    return-void

    .line 55
    :pswitch_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onAccountProfileClicked()V

    goto :goto_0

    .line 58
    :pswitch_1
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onActivityInfoClicked()V

    goto :goto_0

    .line 61
    :pswitch_2
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onAlertsClicked()V

    goto :goto_0

    .line 64
    :pswitch_3
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onContextualClicked()V

    goto :goto_0

    .line 67
    :pswitch_4
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onWatchClicked()V

    goto :goto_0

    .line 70
    :pswitch_5
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onAlarmsClicked()V

    goto :goto_0

    .line 73
    :pswitch_6
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onSupportClicked()V

    goto :goto_0

    .line 76
    :pswitch_7
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onNewsletterClicked()V

    goto :goto_0

    .line 79
    :pswitch_8
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onTourClicked()V

    goto :goto_0

    .line 82
    :pswitch_9
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onUpdateWatchClicked()V

    goto :goto_0

    .line 85
    :pswitch_a
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onResetSettingsClicked()V

    goto :goto_0

    .line 88
    :pswitch_b
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onLogoutClicked()V

    goto :goto_0

    .line 91
    :pswitch_c
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onAboutClicked()V

    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mCounterClickOnAppVersion:I

    .line 100
    return-void
.end method
