.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;
.super Landroid/widget/RelativeLayout;
.source "GenericToggleView.java"


# instance fields
.field private log:Lorg/slf4j/Logger;

.field private mIcon:Landroid/widget/ImageView;

.field private mSummary:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mToggle:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 22
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleElementView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->log:Lorg/slf4j/Logger;

    .line 31
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->init(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleElementView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->log:Lorg/slf4j/Logger;

    .line 37
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->init(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    const-string v1, "layout_inflater"

    .line 42
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 43
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03006b

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 45
    const v1, 0x7f10006d

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->mTitle:Landroid/widget/TextView;

    .line 46
    const v1, 0x7f100205

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->mSummary:Landroid/widget/TextView;

    .line 47
    const v1, 0x7f100069

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->mIcon:Landroid/widget/ImageView;

    .line 48
    const v1, 0x7f100206

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->mToggle:Landroid/widget/Switch;

    .line 49
    return-void
.end method


# virtual methods
.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 61
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 1
    .param p1, "summary"    # Ljava/lang/String;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->mSummary:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    return-void
.end method

.method public setToggle(Z)V
    .locals 1
    .param p1, "isSet"    # Z

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/GenericToggleView;->mToggle:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 65
    return-void
.end method
