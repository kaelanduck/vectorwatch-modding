.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;
.super Ljava/lang/Object;
.source "NotificationDisplayOptionActivity$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;Ljava/lang/Object;)V
    .locals 5
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;, "TT;"
    const v4, 0x7f1002be

    const v3, 0x7f1002bd

    const v2, 0x7f1002bc

    .line 11
    const-string v1, "field \'mOptionOff\' and method \'onOffButtonClicked\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mOptionOff\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->mOptionOff:Landroid/widget/RadioButton;

    .line 13
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    const-string v1, "field \'mOptionAlert\' and method \'onAlertButtonClicked\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 22
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mOptionAlert\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->mOptionAlert:Landroid/widget/RadioButton;

    .line 23
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder$2;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    const-string v1, "field \'mOptionContent\' and method \'onContentButtonClicked\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 32
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mOptionContent\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->mOptionContent:Landroid/widget/RadioButton;

    .line 33
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder$3;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;, "TT;"
    const/4 v0, 0x0

    .line 44
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->mOptionOff:Landroid/widget/RadioButton;

    .line 45
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->mOptionAlert:Landroid/widget/RadioButton;

    .line 46
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;->mOptionContent:Landroid/widget/RadioButton;

    .line 47
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/NotificationDisplayOptionActivity;)V

    return-void
.end method
