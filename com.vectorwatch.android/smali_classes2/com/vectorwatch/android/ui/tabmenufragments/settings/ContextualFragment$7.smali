.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;
.super Ljava/lang/Object;
.source "ContextualFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->valuesSetUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v0, 0x1

    .line 249
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->checkWatchConnectionWithAlert(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    const-string v1, "glance_mode"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 251
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v2

    .line 250
    invoke-static {v1, p2, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 252
    const-string v1, "glance_dirty"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 253
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v2

    .line 252
    invoke-static {v1, v0, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 254
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$800(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V

    .line 255
    const-string v1, "flag_changed_contextual"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 256
    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 255
    invoke-static {v1, v0, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 257
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 261
    :goto_0
    return-void

    .line 259
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$900(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    move-result-object v1

    if-nez p2, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
