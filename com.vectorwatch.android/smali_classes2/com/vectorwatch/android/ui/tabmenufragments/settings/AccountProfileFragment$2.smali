.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$2;
.super Ljava/lang/Object;
.source "AccountProfileFragment.java"

# interfaces
.implements Lcom/facebook/FacebookCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/FacebookCallback",
        "<",
        "Lcom/facebook/login/LoginResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    .prologue
    .line 150
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "SOCIAL MEDIA: Login cancel."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method public onError(Lcom/facebook/FacebookException;)V
    .locals 3
    .param p1, "exception"    # Lcom/facebook/FacebookException;

    .prologue
    .line 156
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SOCIAL MEDIA: Login error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/FacebookException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method public onSuccess(Lcom/facebook/login/LoginResult;)V
    .locals 2
    .param p1, "loginResult"    # Lcom/facebook/login/LoginResult;

    .prologue
    .line 141
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "SOCIAL MEDIA: Login success."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "share"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getSocialMediaManager()Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->shareActivity()V

    .line 145
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 137
    check-cast p1, Lcom/facebook/login/LoginResult;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AccountProfileFragment$2;->onSuccess(Lcom/facebook/login/LoginResult;)V

    return-void
.end method
