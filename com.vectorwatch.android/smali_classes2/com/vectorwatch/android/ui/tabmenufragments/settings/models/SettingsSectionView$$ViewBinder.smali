.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder;
.super Ljava/lang/Object;
.source "SettingsSectionView$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;Ljava/lang/Object;)V
    .locals 4
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;, "TT;"
    const v3, 0x7f10020e

    const v2, 0x7f10020d

    .line 11
    const-string v1, "field \'mLabel\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mLabel\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->mLabel:Landroid/widget/TextView;

    .line 13
    const-string v1, "field \'mImage\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mImage\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->mImage:Landroid/widget/ImageView;

    .line 15
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;, "TT;"
    const/4 v0, 0x0

    .line 18
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->mLabel:Landroid/widget/TextView;

    .line 19
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->mImage:Landroid/widget/ImageView;

    .line 20
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;)V

    return-void
.end method
