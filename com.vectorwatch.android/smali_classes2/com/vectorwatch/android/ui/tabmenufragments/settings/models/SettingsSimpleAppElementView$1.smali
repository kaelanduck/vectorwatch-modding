.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;
.super Ljava/lang/Object;
.source "SettingsSimpleAppElementView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->bind(Lcom/vectorwatch/android/models/AppInfoModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

.field final synthetic val$appInfo:Lcom/vectorwatch/android/models/AppInfoModel;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;Lcom/vectorwatch/android/models/AppInfoModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->val$appInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 64
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->checkWatchConnectionWithAlert(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 65
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->log:Lorg/slf4j/Logger;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Lorg/slf4j/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Preferences - Supported app enabler - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 66
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "app_not_all"

    const-class v4, Ljava/util/List;

    invoke-virtual {v2, v3, v4}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 67
    .local v1, "notifs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_4

    .line 68
    if-eqz p2, :cond_2

    .line 69
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Lcom/vectorwatch/android/models/AppInfoModel;

    move-result-object v2

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 70
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Lcom/vectorwatch/android/models/AppInfoModel;

    move-result-object v2

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 87
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "app_not_all"

    invoke-virtual {v2, v3, v1}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 90
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Lcom/vectorwatch/android/models/AppInfoModel;

    move-result-object v2

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 91
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 95
    .end local v1    # "notifs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    return-void

    .line 73
    .restart local v1    # "notifs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 74
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Lcom/vectorwatch/android/models/AppInfoModel;

    move-result-object v3

    iget-object v3, v3, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 75
    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 73
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 80
    .end local v0    # "i":I
    :cond_4
    if-eqz p2, :cond_0

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "notifs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .restart local v1    # "notifs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->val$appInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 93
    .end local v1    # "notifs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mSwitch:Landroid/widget/Switch;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Landroid/widget/Switch;

    move-result-object v3

    if-nez p2, :cond_6

    const/4 v2, 0x1

    :goto_3
    invoke-virtual {v3, v2}, Landroid/widget/Switch;->setChecked(Z)V

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_3
.end method
