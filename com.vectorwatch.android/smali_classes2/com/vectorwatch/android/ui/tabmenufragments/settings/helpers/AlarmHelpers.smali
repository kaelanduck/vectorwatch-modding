.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/AlarmHelpers;
.super Ljava/lang/Object;
.source "AlarmHelpers.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAlarmStatusFromCloudInfo(Ljava/util/List;Z)B
    .locals 4
    .param p1, "isEnabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)B"
        }
    .end annotation

    .prologue
    .local p0, "weekDays":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 86
    if-eqz p1, :cond_0

    invoke-static {v0, v0}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v0

    .line 89
    .local v0, "alarmStatus":B
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 90
    .local v1, "day":Ljava/lang/String;
    if-eqz v1, :cond_2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SUNDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 91
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SUNDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    invoke-static {v0, v3}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v0

    goto :goto_0

    .line 92
    :cond_2
    if-eqz v1, :cond_3

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->MONDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 93
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->MONDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    invoke-static {v0, v3}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v0

    goto :goto_0

    .line 94
    :cond_3
    if-eqz v1, :cond_4

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->TUESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 95
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->TUESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    invoke-static {v0, v3}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v0

    goto :goto_0

    .line 96
    :cond_4
    if-eqz v1, :cond_5

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->WEDNESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 97
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->WEDNESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    invoke-static {v0, v3}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v0

    goto :goto_0

    .line 98
    :cond_5
    if-eqz v1, :cond_6

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->THURSDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 99
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->THURSDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    invoke-static {v0, v3}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v0

    goto/16 :goto_0

    .line 100
    :cond_6
    if-eqz v1, :cond_7

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->FRIDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 101
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->FRIDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    invoke-static {v0, v3}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v0

    goto/16 :goto_0

    .line 102
    :cond_7
    if-eqz v1, :cond_1

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SATURDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 103
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SATURDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    invoke-static {v0, v3}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v0

    goto/16 :goto_0

    .line 106
    .end local v1    # "day":Ljava/lang/String;
    :cond_8
    return v0
.end method

.method public static getWeekDaysRepeatPattern(B)Ljava/util/List;
    .locals 2
    .param p0, "repeatPattern"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v0, "weekDays":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SUNDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v1

    int-to-byte v1, v1

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    invoke-static {p0, v1}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    const-string v1, "SUN"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->MONDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v1

    int-to-byte v1, v1

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    invoke-static {p0, v1}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    const-string v1, "MON"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->TUESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v1

    int-to-byte v1, v1

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    invoke-static {p0, v1}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 59
    const-string v1, "TUE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_2
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->WEDNESDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v1

    int-to-byte v1, v1

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    invoke-static {p0, v1}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 62
    const-string v1, "WED"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_3
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->THURSDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v1

    int-to-byte v1, v1

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    invoke-static {p0, v1}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 65
    const-string v1, "THU"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_4
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->FRIDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v1

    int-to-byte v1, v1

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    invoke-static {p0, v1}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 68
    const-string v1, "FRI"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_5
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->SATURDAY:Lcom/vectorwatch/android/utils/Constants$RepeatOption;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$RepeatOption;->getValue()I

    move-result v1

    int-to-byte v1, v1

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    invoke-static {p0, v1}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 71
    const-string v1, "SAT"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_6
    return-object v0
.end method

.method public static syncAlarms(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v1

    .line 37
    .local v1, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vectorwatch/android/database/DatabaseManager;->getAllAlarms(Lio/realm/Realm;)Ljava/util/List;

    move-result-object v0

    .line 38
    .local v0, "alarmList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlarms(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 39
    const-string v2, "flag_changed_alarms"

    const/4 v3, 0x1

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 40
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 41
    return-void
.end method
