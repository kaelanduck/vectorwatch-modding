.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;
.super Ljava/lang/Object;
.source "ActivityInfoFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;

.field final synthetic val$inputField:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;Landroid/widget/EditText;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;

    .prologue
    .line 390
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;->val$inputField:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v5, 0x1

    .line 393
    :try_start_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;->val$inputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 394
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f090103

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 395
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 410
    :goto_0
    return-void

    .line 398
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;->val$inputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-ne v2, v3, :cond_1

    .line 399
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f090106

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 400
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 406
    :catch_0
    move-exception v0

    .line 407
    .local v0, "exception":Ljava/lang/NumberFormatException;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f090107

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 408
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 403
    .end local v0    # "exception":Ljava/lang/NumberFormatException;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;->val$inputField:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 405
    .local v1, "weight":I
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4$2;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;

    iget-object v2, v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v2, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$700(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
