.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$1;
.super Ljava/lang/Object;
.source "WatchActivityFragment.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 98
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BACKLIGHT - INTENSITY - progress changed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-static {v0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$102(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;I)I

    .line 102
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 106
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "BACKLIGHT - INTENSITY - start tracking"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 111
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "BACKLIGHT - INTENSITY - stop tracking"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;I)V

    .line 117
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$102(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;I)I

    .line 118
    return-void
.end method
