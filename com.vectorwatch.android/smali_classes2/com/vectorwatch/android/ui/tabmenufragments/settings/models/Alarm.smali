.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
.super Lio/realm/RealmObject;
.source "Alarm.java"

# interfaces
.implements Lio/realm/AlarmRealmProxyInterface;


# instance fields
.field private id:J
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private isEnabled:B

.field private name:Ljava/lang/String;

.field private numberOfHours:I

.field private numberOfMinutes:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    .line 10
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->id:J

    return-void
.end method


# virtual methods
.method public getId()J
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmGet$id()J

    move-result-wide v0

    return-wide v0
.end method

.method public getIsEnabled()B
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmGet$isEnabled()B

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmGet$name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNumberOfHours()I
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmGet$numberOfHours()I

    move-result v0

    return v0
.end method

.method public getNumberOfMinutes()I
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmGet$numberOfMinutes()I

    move-result v0

    return v0
.end method

.method public realmGet$id()J
    .locals 2

    iget-wide v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->id:J

    return-wide v0
.end method

.method public realmGet$isEnabled()B
    .locals 1

    iget-byte v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->isEnabled:B

    return v0
.end method

.method public realmGet$name()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->name:Ljava/lang/String;

    return-object v0
.end method

.method public realmGet$numberOfHours()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->numberOfHours:I

    return v0
.end method

.method public realmGet$numberOfMinutes()I
    .locals 1

    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->numberOfMinutes:I

    return v0
.end method

.method public realmSet$id(J)V
    .locals 1

    iput-wide p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->id:J

    return-void
.end method

.method public realmSet$isEnabled(B)V
    .locals 0

    iput-byte p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->isEnabled:B

    return-void
.end method

.method public realmSet$name(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->name:Ljava/lang/String;

    return-void
.end method

.method public realmSet$numberOfHours(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->numberOfHours:I

    return-void
.end method

.method public realmSet$numberOfMinutes(I)V
    .locals 0

    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->numberOfMinutes:I

    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 54
    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmSet$id(J)V

    .line 55
    return-void
.end method

.method public setIsEnabled(B)V
    .locals 0
    .param p1, "isEnabled"    # B

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmSet$isEnabled(B)V

    .line 47
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmSet$name(Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public setNumberOfHours(I)V
    .locals 0
    .param p1, "numberOfHours"    # I

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmSet$numberOfHours(I)V

    .line 23
    return-void
.end method

.method public setNumberOfMinutes(I)V
    .locals 0
    .param p1, "numberOfMinutes"    # I

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->realmSet$numberOfMinutes(I)V

    .line 31
    return-void
.end method
