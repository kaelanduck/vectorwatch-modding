.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "AlarmsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AlarmViewHolder"
.end annotation


# instance fields
.field private mAlarm:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

.field mName:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10018a
        }
    .end annotation
.end field

.field mTime:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100189
        }
    .end annotation
.end field

.field mToggle:Landroid/widget/Switch;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10018b
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    .line 89
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 90
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)V

    .line 91
    return-void
.end method


# virtual methods
.method public bind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;)V
    .locals 9
    .param p1, "alarm"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 104
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mAlarm:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .line 106
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mToggle:Landroid/widget/Switch;

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getIsEnabled()B

    move-result v3

    invoke-static {v3, v7}, Lcom/vectorwatch/android/utils/Helpers;->isBitSet(BB)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 109
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mName:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->is24hFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mTime:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%02d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfHours()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02d"

    new-array v5, v8, [Ljava/lang/Object;

    .line 111
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfMinutes()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    .line 110
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mName:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfHours()I

    move-result v1

    .line 117
    .local v1, "numberOfHours":I
    const/16 v2, 0xc

    if-le v1, v2, :cond_1

    .line 118
    add-int/lit8 v1, v1, -0xc

    .line 121
    :cond_1
    if-nez v1, :cond_2

    .line 122
    const-string v0, "AM"

    .line 123
    .local v0, "ampmValue":Ljava/lang/String;
    const/16 v1, 0xc

    .line 129
    :goto_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mTime:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%02d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02d"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfMinutes()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mName:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 125
    .end local v0    # "ampmValue":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfHours()I

    move-result v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->getTimeOfDay(I)I

    move-result v2

    if-ne v2, v8, :cond_3

    const-string v0, "PM"

    .restart local v0    # "ampmValue":Ljava/lang/String;
    :goto_2
    goto :goto_1

    .end local v0    # "ampmValue":Ljava/lang/String;
    :cond_3
    const-string v0, "AM"

    goto :goto_2
.end method

.method public onAlarmClicked()V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100189,
            0x7f10018a
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mAlarm:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getId()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;->onAlarmClicked(J)V

    .line 101
    return-void
.end method

.method public onToggleClicked()V
    .locals 5
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10018b
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mAlarm:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getId()J

    move-result-wide v2

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mAlarm:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getIsEnabled()B

    move-result v1

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmViewHolder;->mToggle:Landroid/widget/Switch;

    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-interface {v0, v2, v3, v1, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AlarmsListAdapter$AlarmsListListener;->onAlarmStatusChanged(JBZ)V

    .line 96
    return-void
.end method
