.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/AlarmView;
.super Landroid/widget/RelativeLayout;
.source "AlarmView.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field mName:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10018a
        }
    .end annotation
.end field

.field mTime:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100189
        }
    .end annotation
.end field

.field mToggle:Landroid/widget/ToggleButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10018b
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/AlarmView;->mContext:Landroid/content/Context;

    .line 32
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 33
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030069

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 34
    invoke-static {p0, p0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)V

    .line 35
    return-void
.end method
