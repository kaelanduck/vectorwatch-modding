.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;
.super Ljava/lang/Object;
.source "EditAlarmInteractorImpl.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractor;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$Interactor;

.field private mRealm:Lio/realm/Realm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$Interactor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "presenter"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$Interactor;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mContext:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$Interactor;

    .line 32
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mRealm:Lio/realm/Realm;

    .line 33
    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;IIB)V
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "hour"    # I
    .param p3, "min"    # I
    .param p4, "enabled"    # B

    .prologue
    .line 87
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mRealm:Lio/realm/Realm;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/vectorwatch/android/database/DatabaseManager;->createAlarm(Lio/realm/Realm;Ljava/lang/String;IIB)V

    .line 88
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AlarmCreatedEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/events/AlarmCreatedEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 89
    return-void
.end method

.method public delete(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 56
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mRealm:Lio/realm/Realm;

    invoke-virtual {v0, v1, p1, p2}, Lcom/vectorwatch/android/database/DatabaseManager;->deleteAlarm(Lio/realm/Realm;J)V

    .line 57
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AlarmDeletedEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/events/AlarmDeletedEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 59
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ALARM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DELETED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public getAlarmWithId(J)V
    .locals 9
    .param p1, "id"    # J

    .prologue
    .line 65
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mRealm:Lio/realm/Realm;

    invoke-virtual {v1, v2, p1, p2}, Lcom/vectorwatch/android/database/DatabaseManager;->getAlarmWithId(Lio/realm/Realm;J)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    move-result-object v0

    .line 67
    .local v0, "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$Interactor;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfHours()I

    move-result v3

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfMinutes()I

    move-result v4

    .line 68
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getIsEnabled()B

    move-result v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getId()J

    move-result-wide v6

    .line 67
    invoke-interface/range {v1 .. v7}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/EditAlarmPresenter$Interactor;->sendAlarm(Ljava/lang/String;IIBJ)V

    .line 69
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mRealm:Lio/realm/Realm;

    invoke-virtual {v0}, Lio/realm/Realm;->close()V

    .line 74
    return-void
.end method

.method public saveAlarm(Ljava/lang/String;IIBJ)V
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "hour"    # I
    .param p3, "min"    # I
    .param p4, "enabled"    # B
    .param p5, "id"    # J

    .prologue
    const/4 v0, 0x0

    .line 37
    const-wide/16 v2, -0x1

    cmp-long v1, p5, v2

    if-eqz v1, :cond_0

    .line 38
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "ALARMS - Edit alarm - trying to edit an existent alarm."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 40
    invoke-virtual/range {p0 .. p6}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->update(Ljava/lang/String;IIBJ)V

    .line 42
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ALARM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 52
    :goto_0
    return-void

    .line 45
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->log:Lorg/slf4j/Logger;

    const-string v2, "ALARMS - Add alarm - trying to create alarm."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->create(Ljava/lang/String;IIB)V

    .line 49
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ALARM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CREATED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public update(Ljava/lang/String;IIBJ)V
    .locals 9
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "hour"    # I
    .param p3, "min"    # I
    .param p4, "enabled"    # B
    .param p5, "id"    # J

    .prologue
    .line 101
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/EditAlarmInteractorImpl;->mRealm:Lio/realm/Realm;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-virtual/range {v0 .. v7}, Lcom/vectorwatch/android/database/DatabaseManager;->updateAlarm(Lio/realm/Realm;Ljava/lang/String;IIBJ)V

    .line 102
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AlarmUpdatedEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/events/AlarmUpdatedEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 103
    return-void
.end method
