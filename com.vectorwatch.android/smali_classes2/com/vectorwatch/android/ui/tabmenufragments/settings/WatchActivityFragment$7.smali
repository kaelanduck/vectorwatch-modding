.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;
.super Ljava/lang/Object;
.source "WatchActivityFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->showBacklightDurationPicker()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

.field final synthetic val$dialog:Landroid/app/Dialog;

.field final synthetic val$numberPicker:Landroid/widget/NumberPicker;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;Landroid/widget/NumberPicker;Landroid/app/Dialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->val$numberPicker:Landroid/widget/NumberPicker;

    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->val$dialog:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 272
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->val$numberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v0

    .line 273
    .local v0, "timeout":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mDurationBacklightText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_BACKLIGHT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    .line 276
    invoke-static {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v0

    .line 275
    invoke-static {v1, v2, v3, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 278
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-static {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$500(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;I)V

    .line 279
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$7;->val$dialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 280
    return-void
.end method
