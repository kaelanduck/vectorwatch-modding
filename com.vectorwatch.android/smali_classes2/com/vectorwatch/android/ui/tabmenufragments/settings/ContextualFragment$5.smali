.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;
.super Ljava/lang/Object;
.source "ContextualFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->valuesSetUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v1, 0x1

    .line 206
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->checkWatchConnectionWithAlert(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 207
    const-string v2, "settings_contextual_tentative_meetings"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 208
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v3

    .line 207
    invoke-static {v2, p2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 209
    const-string v2, "settings_contextual_tentative_meetings_DIRTY"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 211
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v3

    .line 209
    invoke-static {v2, v1, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 212
    const-string v2, "flag_changed_contextual"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 213
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 212
    invoke-static {v2, v1, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 214
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/managers/EventManager;->checkCalendarEventsForSync(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/managers/EventManager;->getCalendarEvents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 216
    .local v0, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCalendarEvents(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 218
    .end local v0    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 222
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$5;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$600(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    move-result-object v2

    if-nez p2, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
