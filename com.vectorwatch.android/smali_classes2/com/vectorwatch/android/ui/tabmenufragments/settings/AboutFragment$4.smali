.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$4;
.super Ljava/lang/Thread;
.source "AboutFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->removeTokenButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 347
    const-wide/16 v4, 0x1388

    :try_start_0
    invoke-static {v4, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$4;->sleep(J)V

    .line 348
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 349
    .local v2, "manager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 351
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v3, v0

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 352
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->access$100()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "No account found in account manager. Needed for completing registration."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 361
    .end local v0    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "manager":Landroid/accounts/AccountManager;
    :goto_0
    return-void

    .line 356
    .restart local v0    # "accounts":[Landroid/accounts/Account;
    .restart local v2    # "manager":Landroid/accounts/AccountManager;
    :cond_0
    const/4 v3, 0x0

    aget-object v3, v0, v3

    const-string v4, "Full access"

    const-string v5, "wrong_token"

    invoke-virtual {v2, v3, v4, v5}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 357
    .end local v0    # "accounts":[Landroid/accounts/Account;
    .end local v2    # "manager":Landroid/accounts/AccountManager;
    :catch_0
    move-exception v1

    .line 358
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
