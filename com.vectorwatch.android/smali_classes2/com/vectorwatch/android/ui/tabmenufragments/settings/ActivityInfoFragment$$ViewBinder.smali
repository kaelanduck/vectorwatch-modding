.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder;
.super Ljava/lang/Object;
.source "ActivityInfoFragment$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;Ljava/lang/Object;)V
    .locals 8
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;, "TT;"
    const v7, 0x7f100294

    const v6, 0x7f100293

    const v5, 0x7f100292

    const v4, 0x7f100291

    const v3, 0x7f100290

    .line 11
    const v1, 0x7f100295

    const-string v2, "field \'mWeeklyNewsletter\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f100295

    const-string v2, "field \'mWeeklyNewsletter\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeeklyNewsletter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 13
    const-string v1, "field \'mGoalAchievementAlert\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 14
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mGoalAchievementAlert\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGoalAchievementAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 15
    const-string v1, "field \'mGoalAlmostAlert\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 16
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mGoalAlmostAlert\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGoalAlmostAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 17
    const-string v1, "field \'mActivityReminder\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 18
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mActivityReminder\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mActivityReminder:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 19
    const-string v1, "field \'mAlertsSection\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 20
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mAlertsSection\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mAlertsSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 21
    const-string v1, "field \'mNewsletterSection\'"

    invoke-virtual {p1, p3, v7, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 22
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mNewsletterSection\'"

    invoke-virtual {p1, v0, v7, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mNewsletterSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 23
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;, "TT;"
    const/4 v0, 0x0

    .line 26
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mWeeklyNewsletter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 27
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGoalAchievementAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 28
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mGoalAlmostAlert:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 29
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mActivityReminder:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    .line 30
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mAlertsSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 31
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->mNewsletterSection:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    .line 32
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V

    return-void
.end method
