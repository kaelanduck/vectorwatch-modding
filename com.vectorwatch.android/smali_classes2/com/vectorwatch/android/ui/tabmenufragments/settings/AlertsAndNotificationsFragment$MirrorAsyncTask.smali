.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;
.super Landroid/os/AsyncTask;
.source "AlertsAndNotificationsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MirrorAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;


# direct methods
.method private constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;
    .param p2, "x1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$1;

    .prologue
    .line 168
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 168
    check-cast p1, [Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->doInBackground([Ljava/lang/Boolean;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Boolean;)Ljava/lang/Void;
    .locals 2
    .param p1, "booleans"    # [Ljava/lang/Boolean;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$500(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;Z)V

    .line 180
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 181
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 168
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 186
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 187
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$600(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AppsListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/adapters/AppsListAdapter;->notifyDataSetChanged()V

    .line 188
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 190
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 173
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 175
    return-void
.end method
