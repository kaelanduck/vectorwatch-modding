.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$4;
.super Ljava/lang/Object;
.source "AlertsAndNotificationsFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->setDismissFromPhoneState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "flag_dismiss_from_phone"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v2, p2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$001(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    return-void
.end method
