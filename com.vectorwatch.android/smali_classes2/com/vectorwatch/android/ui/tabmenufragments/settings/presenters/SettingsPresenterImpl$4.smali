.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$4;
.super Ljava/lang/Object;
.source "SettingsPresenterImpl.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->onLogoutClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 269
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->logoutDrivenResets(Landroid/content/Context;)Z

    move-result v1

    .line 270
    .local v1, "ok":Z
    if-eqz v1, :cond_0

    .line 271
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/LoginStateChangedEvent;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/vectorwatch/android/events/LoginStateChangedEvent;-><init>(Z)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 273
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 274
    .local v0, "loginIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 276
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl$4;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 278
    .end local v0    # "loginIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
