.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;
.super Landroid/widget/RelativeLayout;
.source "SettingsValueElementView.java"


# instance fields
.field private log:Lorg/slf4j/Logger;

.field private mContext:Landroid/content/Context;

.field private mLabel:Landroid/widget/TextView;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mValue:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 18
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->log:Lorg/slf4j/Logger;

    .line 27
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->init(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->log:Lorg/slf4j/Logger;

    .line 32
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->init(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v3, 0x7f0f00ac

    .line 36
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->mContext:Landroid/content/Context;

    .line 38
    const-string v1, "layout_inflater"

    .line 39
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 40
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030075

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 42
    const v1, 0x7f10021a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->mLayout:Landroid/widget/RelativeLayout;

    .line 44
    const v1, 0x7f10021c

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->mLabel:Landroid/widget/TextView;

    .line 45
    const v1, 0x7f10021b

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->mValue:Landroid/widget/TextView;

    .line 47
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->mLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 48
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->mValue:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 49
    return-void
.end method


# virtual methods
.method public setLabel(Ljava/lang/String;)V
    .locals 1
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->mLabel:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;->mValue:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method
