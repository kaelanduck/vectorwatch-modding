.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;
.super Ljava/lang/Object;
.source "ActivityInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 184
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v10

    if-nez v10, :cond_1

    .line 185
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v11}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090063

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x5dc

    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v12}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 346
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v10}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-static {v10}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v3

    .line 190
    .local v3, "isMetric":Z
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v10}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Landroid/content/Context;

    move-result-object v11

    sget-object v12, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v13, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v10, 0x0

    check-cast v10, Ljava/lang/String;

    invoke-static {v11, v12, v13, v10}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 194
    if-eqz v3, :cond_3

    .line 195
    new-instance v4, Landroid/widget/EditText;

    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v10}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Landroid/content/Context;

    move-result-object v10

    invoke-direct {v4, v10}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 196
    .local v4, "mainValueInputField":Landroid/widget/EditText;
    const/4 v10, 0x2

    invoke-virtual {v4, v10}, Landroid/widget/EditText;->setInputType(I)V

    .line 198
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v10}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v5

    .line 199
    .local v5, "previouslyRegisteredHeight":I
    const/4 v10, -0x1

    if-eq v5, v10, :cond_2

    .line 200
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    .line 201
    .local v9, "text":Ljava/lang/String;
    invoke-virtual {v4, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 206
    .end local v9    # "text":Ljava/lang/String;
    :goto_1
    new-instance v10, Landroid/app/AlertDialog$Builder;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v11}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 208
    .local v0, "dialog":Landroid/app/AlertDialog;
    new-instance v10, Landroid/app/AlertDialog$Builder;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v11}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const v12, 0x7f0c00c5

    invoke-direct {v10, v11, v12}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v11, 0x7f090226

    .line 210
    invoke-virtual {v10, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    .line 211
    invoke-virtual {v10, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f0900af

    new-instance v12, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;

    invoke-direct {v12, p0, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;Landroid/widget/EditText;)V

    .line 212
    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f09008a

    new-instance v12, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$1;

    invoke-direct {v12, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;)V

    .line 232
    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    .line 236
    invoke-virtual {v10}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 238
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10}, Landroid/text/Editable;->length()I

    move-result v10

    invoke-virtual {v4, v10}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 203
    .end local v0    # "dialog":Landroid/app/AlertDialog;
    :cond_2
    const v10, 0x7f090120

    invoke-virtual {v4, v10}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_1

    .line 240
    .end local v4    # "mainValueInputField":Landroid/widget/EditText;
    .end local v5    # "previouslyRegisteredHeight":I
    :cond_3
    iget-object v10, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v10}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 241
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v10, 0x7f030050

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 242
    .local v1, "dialogLayout":Landroid/view/View;
    const v10, 0x7f1001a9

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 243
    .restart local v4    # "mainValueInputField":Landroid/widget/EditText;
    const v10, 0x7f1001ab

    invoke-virtual {v1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    .line 246
    .local v8, "secondaryValueInputField":Landroid/widget/EditText;
    const/4 v10, 0x2

    invoke-virtual {v4, v10}, Landroid/widget/EditText;->setInputType(I)V

    .line 247
    const/4 v10, 0x2

    invoke-virtual {v8, v10}, Landroid/widget/EditText;->setInputType(I)V

    .line 249
    const-string v10, "activity_info_height_feet"

    const/4 v11, -0x1

    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .line 250
    invoke-virtual {v12}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v6

    .line 251
    .local v6, "previouslyRegisteredHeightFeet":I
    const-string v10, "activity_info_height_inches"

    const/4 v11, -0x1

    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .line 252
    invoke-virtual {v12}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v7

    .line 254
    .local v7, "previouslyRegisteredHeightInches":I
    const/4 v10, -0x1

    if-eq v6, v10, :cond_4

    .line 255
    if-eqz v6, :cond_7

    .line 256
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    .line 257
    .restart local v9    # "text":Ljava/lang/String;
    invoke-virtual {v4, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 262
    .end local v9    # "text":Ljava/lang/String;
    :cond_4
    :goto_2
    const/4 v10, -0x1

    if-eq v7, v10, :cond_5

    .line 263
    if-eqz v7, :cond_8

    .line 264
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    .line 265
    .restart local v9    # "text":Ljava/lang/String;
    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 271
    .end local v9    # "text":Ljava/lang/String;
    :cond_5
    :goto_3
    new-instance v10, Landroid/app/AlertDialog$Builder;

    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v11}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const v12, 0x7f0c00c5

    invoke-direct {v10, v11, v12}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v11, 0x7f090225

    .line 273
    invoke-virtual {v10, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    .line 274
    invoke-virtual {v10, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f0900af

    new-instance v12, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;

    invoke-direct {v12, p0, v4, v8}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;Landroid/widget/EditText;Landroid/widget/EditText;)V

    .line 275
    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f09008a

    new-instance v12, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$3;

    invoke-direct {v12, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$3;)V

    .line 332
    invoke-virtual {v10, v11, v12}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    .line 337
    invoke-virtual {v10}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 339
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 340
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10}, Landroid/text/Editable;->length()I

    move-result v10

    invoke-virtual {v4, v10}, Landroid/widget/EditText;->setSelection(I)V

    .line 342
    :cond_6
    invoke-virtual {v8}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 343
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10}, Landroid/text/Editable;->length()I

    move-result v10

    invoke-virtual {v8, v10}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 259
    :cond_7
    const-string v10, "0"

    invoke-virtual {v4, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 267
    :cond_8
    const-string v10, "0"

    invoke-virtual {v8, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
