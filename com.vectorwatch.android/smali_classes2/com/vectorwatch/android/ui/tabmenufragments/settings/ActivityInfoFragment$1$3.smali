.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$3;
.super Ljava/lang/Object;
.source "ActivityInfoFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$3;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 136
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$200()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Gender selected: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p2, :cond_0

    const-string v0, "female"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1$3;->this$1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$002(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;I)I

    .line 140
    return-void

    .line 136
    :cond_0
    const-string v0, "male"

    goto :goto_0
.end method
