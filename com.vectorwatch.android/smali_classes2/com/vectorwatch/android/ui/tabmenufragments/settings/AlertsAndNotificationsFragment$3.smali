.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;
.super Ljava/lang/Object;
.source "AlertsAndNotificationsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->instantiateAppsList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 205
    :try_start_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$800(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->USER_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$700(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vectorwatch/android/utils/AppSignatureRetriever;->getInstalledApps(Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 206
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$800(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->SYSTEM_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$700(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vectorwatch/android/utils/AppSignatureRetriever;->getInstalledApps(Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 208
    new-instance v1, Lcom/vectorwatch/android/models/AppInfoModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/AppInfoModel;-><init>()V

    .line 209
    .local v1, "phoneInfo":Lcom/vectorwatch/android/models/AppInfoModel;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    const v3, 0x7f0901e5

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/vectorwatch/android/models/AppInfoModel;->appName:Ljava/lang/String;

    .line 210
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/vectorwatch/android/models/AppInfoModel;->icon:Landroid/graphics/drawable/Drawable;

    .line 211
    const-string v2, "phone_call"

    iput-object v2, v1, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    .line 212
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$800(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$800(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 220
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 221
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3$2;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$3;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    .end local v1    # "phoneInfo":Lcom/vectorwatch/android/models/AppInfoModel;
    :cond_0
    :goto_0
    return-void

    .line 234
    :catch_0
    move-exception v0

    .line 235
    .local v0, "exc":Ljava/lang/IllegalStateException;
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$1000()Lorg/slf4j/Logger;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto :goto_0
.end method
