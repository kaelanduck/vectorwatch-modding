.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;
.super Ljava/lang/Object;
.source "AboutFragment$$ViewBinder.java"

# interfaces
.implements Lbutterknife/ButterKnife$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;",
        ">",
        "Ljava/lang/Object;",
        "Lbutterknife/ButterKnife$ViewBinder",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;Ljava/lang/Object;)V
    .locals 7
    .param p1, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p3, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbutterknife/ButterKnife$Finder;",
            "TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder<TT;>;"
    .local p2, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;, "TT;"
    const v6, 0x7f100097

    const v5, 0x7f100096

    const v4, 0x7f100094

    const v3, 0x7f100093

    const v2, 0x7f100092

    .line 11
    const-string v1, "field \'mCountWatchCrashes\' and method \'switchBaseUrl\'"

    invoke-virtual {p1, p3, v2, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 12
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'mCountWatchCrashes\'"

    invoke-virtual {p1, v0, v2, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mCountWatchCrashes:Landroid/widget/Button;

    .line 13
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    const-string v1, "field \'mStopBleButton\' and method \'sendBleStopCommand\'"

    invoke-virtual {p1, p3, v3, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 22
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mStopBleButton\'"

    invoke-virtual {p1, v0, v3, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mStopBleButton:Landroid/widget/Button;

    .line 23
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$2;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    const-string v1, "field \'mReconnect\' and method \'sendBleReconnectCommand\'"

    invoke-virtual {p1, p3, v4, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 32
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mReconnect\'"

    invoke-virtual {p1, v0, v4, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mReconnect:Landroid/widget/Button;

    .line 33
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$3;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    const-string v1, "field \'mSendLocalization\' and method \'sendLocalizationFile\'"

    invoke-virtual {p1, p3, v5, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 42
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mSendLocalization\'"

    invoke-virtual {p1, v0, v5, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mSendLocalization:Landroid/widget/Button;

    .line 43
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$4;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$4;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    const-string v1, "field \'mReqLocalization\' and method \'reqLocalizationFile\'"

    invoke-virtual {p1, p3, v6, v1}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 52
    .restart local v0    # "view":Landroid/view/View;
    const-string v1, "field \'mReqLocalization\'"

    invoke-virtual {p1, v0, v6, v1}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mReqLocalization:Landroid/widget/Button;

    .line 53
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$5;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$5;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    const v1, 0x7f100098

    const-string v2, "field \'mAddDummySteps\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 62
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100098

    const-string v2, "field \'mAddDummySteps\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mAddDummySteps:Landroid/widget/Button;

    .line 63
    const v1, 0x7f100099

    const-string v2, "field \'mReqSyncToFit\' and method \'reqSyncToFit\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 64
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f100099

    const-string v2, "field \'mReqSyncToFit\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mReqSyncToFit:Landroid/widget/Button;

    .line 65
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$6;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$6;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    const v1, 0x7f10009a

    const-string v2, "field \'mEraseDb\' and method \'eraseDb\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 74
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f10009a

    const-string v2, "field \'mEraseDb\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mEraseDb:Landroid/widget/Button;

    .line 75
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$7;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$7;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v1, 0x7f10009b

    const-string v2, "field \'mConnectBle\' and method \'sendBleConnectCommand\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 84
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f10009b

    const-string v2, "field \'mConnectBle\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mConnectBle:Landroid/widget/Button;

    .line 85
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$8;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$8;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    const v1, 0x7f10009d

    const-string v2, "field \'mReqSynRealmActivity\' and method \'reqSyncRealmActivityToCloud\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 94
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f10009d

    const-string v2, "field \'mReqSynRealmActivity\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mReqSynRealmActivity:Landroid/widget/Button;

    .line 95
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$9;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$9;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    const v1, 0x7f10009c

    const-string v2, "field \'mDisplayRealmActivityDb\' and method \'displayRealmActivityDb\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 104
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f10009c

    const-string v2, "field \'mDisplayRealmActivityDb\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mDisplayRealmActivityDb:Landroid/widget/Button;

    .line 105
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$10;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$10;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    const v1, 0x7f10009e

    const-string v2, "field \'mSwitchOfflineMode\' and method \'switchOfflineMode\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 114
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f10009e

    const-string v2, "field \'mSwitchOfflineMode\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mSwitchOfflineMode:Landroid/widget/Button;

    .line 115
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$11;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$11;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    const v1, 0x7f10009f

    const-string v2, "field \'mRemoveTokenButton\' and method \'removeTokenButton\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 124
    .restart local v0    # "view":Landroid/view/View;
    const v1, 0x7f10009f

    const-string v2, "field \'mRemoveTokenButton\'"

    invoke-virtual {p1, v0, v1, v2}, Lbutterknife/ButterKnife$Finder;->castView(Landroid/view/View;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mRemoveTokenButton:Landroid/widget/Button;

    .line 125
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$12;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$12;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    const v1, 0x7f100095

    const-string v2, "method \'switchLanguage\'"

    invoke-virtual {p1, p3, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 134
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$13;

    invoke-direct {v1, p0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder$13;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    return-void
.end method

.method public bridge synthetic bind(Lbutterknife/ButterKnife$Finder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder<TT;>;"
    check-cast p2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-virtual {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;->bind(Lbutterknife/ButterKnife$Finder;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;Ljava/lang/Object;)V

    return-void
.end method

.method public unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder<TT;>;"
    .local p1, "target":Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;, "TT;"
    const/4 v0, 0x0

    .line 145
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mCountWatchCrashes:Landroid/widget/Button;

    .line 146
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mStopBleButton:Landroid/widget/Button;

    .line 147
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mReconnect:Landroid/widget/Button;

    .line 148
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mSendLocalization:Landroid/widget/Button;

    .line 149
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mReqLocalization:Landroid/widget/Button;

    .line 150
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mAddDummySteps:Landroid/widget/Button;

    .line 151
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mReqSyncToFit:Landroid/widget/Button;

    .line 152
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mEraseDb:Landroid/widget/Button;

    .line 153
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mConnectBle:Landroid/widget/Button;

    .line 154
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mReqSynRealmActivity:Landroid/widget/Button;

    .line 155
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mDisplayRealmActivityDb:Landroid/widget/Button;

    .line 156
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mSwitchOfflineMode:Landroid/widget/Button;

    .line 157
    iput-object v0, p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;->mRemoveTokenButton:Landroid/widget/Button;

    .line 158
    return-void
.end method

.method public bridge synthetic unbind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;, "Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder<TT;>;"
    check-cast p1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment$$ViewBinder;->unbind(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AboutFragment;)V

    return-void
.end method
