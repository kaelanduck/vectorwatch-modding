.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl$2;
.super Ljava/lang/Object;
.source "NewsletterInteractorImpl.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->changeNewsletterOptionsApp(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/NewsletterResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 2
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;

    move-result-object v0

    const-string v1, "MOBILE_APP_NEWSLETTER_LIST"

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;->onUpdateNewsletterFail(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/NewsletterResponse;Lretrofit/client/Response;)V
    .locals 1
    .param p1, "newsletterResponse"    # Lcom/vectorwatch/android/models/NewsletterResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;

    move-result-object v0

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/NewsletterPresenter$Interactor;->onUpdateNewsletterSuccess()V

    .line 68
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Lcom/vectorwatch/android/models/NewsletterResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/interactors/NewsletterInteractorImpl$2;->success(Lcom/vectorwatch/android/models/NewsletterResponse;Lretrofit/client/Response;)V

    return-void
.end method
