.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$2;
.super Ljava/lang/Object;
.source "ActivityInfoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 168
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090063

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 178
    :goto_0
    return-void

    .line 172
    :cond_0
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/DateOfBirthPickerFragment;

    invoke-direct {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/DateOfBirthPickerFragment;-><init>()V

    .line 173
    .local v0, "newFragment":Landroid/app/DialogFragment;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "datePicker"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 175
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ActivityInfoFragment;)Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v3, v4, v1}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    goto :goto_0
.end method
