.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;
.super Ljava/lang/Object;
.source "WatchActivityFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatterySetup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 152
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_LOW_BATTERY_SETTING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UPDATED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v0, 0x0

    check-cast v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v1, v2, v3, v0}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    .line 156
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->checkWatchConnectionWithAlert(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setSwitch(Z)V

    .line 158
    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Preferences low battery notification: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 159
    const-string v0, "pref_low_battery_notification"

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    .line 160
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 159
    invoke-static {v0, p2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 162
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/managers/SyncWatchSettingsManager;->sendWarningOptionsToWatch(Landroid/content/Context;)V

    .line 164
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;

    iget-object v1, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/WatchActivityFragment;->mLowBatteryNotificationSettings:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/PreferenceSwitchEnablerView;->setSwitch(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
