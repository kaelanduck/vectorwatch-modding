.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;
.super Landroid/widget/RelativeLayout;
.source "SettingsToggleWithSummaryView.java"


# instance fields
.field private log:Lorg/slf4j/Logger;

.field private mContext:Landroid/content/Context;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mSummary:Landroid/widget/TextView;

.field private mSwitch:Landroid/widget/Switch;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 22
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->log:Lorg/slf4j/Logger;

    .line 31
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->init(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->log:Lorg/slf4j/Logger;

    .line 36
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->init(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x2

    .line 40
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mContext:Landroid/content/Context;

    .line 42
    const-string v2, "layout_inflater"

    .line 43
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 44
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030074

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 46
    const v2, 0x7f100217

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mLayout:Landroid/widget/RelativeLayout;

    .line 48
    const v2, 0x7f100218

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mTitle:Landroid/widget/TextView;

    .line 49
    const v2, 0x7f100219

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mSummary:Landroid/widget/TextView;

    .line 50
    const v2, 0x7f100215

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Switch;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mSwitch:Landroid/widget/Switch;

    .line 52
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00ac

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 53
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mSummary:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00ad

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v0, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 56
    .local v0, "current":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "en"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 57
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mSummary:Landroid/widget/TextView;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 58
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mTitle:Landroid/widget/TextView;

    const/high16 v3, 0x41500000    # 13.0f

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 60
    :cond_0
    return-void
.end method


# virtual methods
.method public isEnabled()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public setOnCheckChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 76
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 1
    .param p1, "summary"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mSummary:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    return-void
.end method

.method public setSwitch(Z)V
    .locals 1
    .param p1, "isChecked"    # Z

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 72
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method
