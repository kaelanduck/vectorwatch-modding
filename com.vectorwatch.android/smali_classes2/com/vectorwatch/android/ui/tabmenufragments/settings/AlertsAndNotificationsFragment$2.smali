.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;
.super Ljava/lang/Object;
.source "AlertsAndNotificationsFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->setMirrorPhoneState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 140
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->checkWatchConnectionWithAlert(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    const-string v2, "flag_mirror_phone"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    .line 142
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 141
    invoke-static {v2, p2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 143
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    .line 149
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    new-instance v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$1;)V

    invoke-static {v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$102(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;

    .line 150
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Boolean;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-virtual {v2, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$MirrorAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 151
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/AlertsAndNotificationsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    move-result-object v2

    if-nez p2, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
