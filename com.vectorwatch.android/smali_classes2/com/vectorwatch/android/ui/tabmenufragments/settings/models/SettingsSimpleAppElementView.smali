.class public Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;
.super Landroid/widget/RelativeLayout;
.source "SettingsSimpleAppElementView.java"


# instance fields
.field private log:Lorg/slf4j/Logger;

.field private mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

.field private mContext:Landroid/content/Context;

.field private mLabel:Landroid/widget/TextView;

.field private mSwitch:Landroid/widget/Switch;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 26
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->log:Lorg/slf4j/Logger;

    .line 35
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->init(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsValueElementView;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->log:Lorg/slf4j/Logger;

    .line 40
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->init(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Lorg/slf4j/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Lcom/vectorwatch/android/models/AppInfoModel;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mContext:Landroid/content/Context;

    .line 46
    const-string v1, "layout_inflater"

    .line 47
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 48
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030073

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 50
    const v1, 0x7f100214

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mLabel:Landroid/widget/TextView;

    .line 51
    const v1, 0x7f100216

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mSwitch:Landroid/widget/Switch;

    .line 52
    return-void
.end method


# virtual methods
.method public bind(Lcom/vectorwatch/android/models/AppInfoModel;)V
    .locals 3
    .param p1, "appInfo"    # Lcom/vectorwatch/android/models/AppInfoModel;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mAppInfo:Lcom/vectorwatch/android/models/AppInfoModel;

    .line 56
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mLabel:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/vectorwatch/android/models/AppInfoModel;->appName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mSwitch:Landroid/widget/Switch;

    iget-object v1, p1, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getSupportedAppNotification(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;->mSwitch:Landroid/widget/Switch;

    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;

    invoke-direct {v1, p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSimpleAppElementView;Lcom/vectorwatch/android/models/AppInfoModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 97
    return-void
.end method
