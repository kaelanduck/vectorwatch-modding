.class Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;
.super Ljava/lang/Object;
.source "ContextualFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->valuesSetUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/16 v4, 0x5dc

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 116
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->checkWatchConnectionWithAlert(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 117
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualAutoSleepSettings(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setContextualMorningFaceSettings(ZLandroid/content/Context;)V

    .line 119
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$102(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;Z)Z

    .line 120
    const-string v1, "flag_changed_contextual"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 121
    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 120
    invoke-static {v1, v0, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 122
    if-eqz p2, :cond_0

    .line 123
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0901b2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 124
    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v1

    .line 123
    invoke-static {v0, v4, v1}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 135
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0901b1

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    .line 129
    invoke-static {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Landroid/content/Context;

    move-result-object v3

    .line 128
    invoke-static {v2, v4, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 130
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    move-result-object v2

    if-nez p2, :cond_2

    :goto_1
    invoke-virtual {v2, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 133
    :cond_3
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settings/ContextualFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;

    move-result-object v2

    if-nez p2, :cond_4

    :goto_2
    invoke-virtual {v2, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsToggleWithSummaryView;->setSwitch(Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method
