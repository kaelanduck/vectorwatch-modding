.class public Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;
.super Landroid/widget/BaseAdapter;
.source "StreamOptionsListAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFilter:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;

.field private mFilteredTimezoneList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTimezoneList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 20
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$1;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mFilter:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;

    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mFilteredTimezoneList:Ljava/util/List;

    .line 24
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mTimezoneList:Ljava/util/List;

    .line 25
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mContext:Landroid/content/Context;

    .line 26
    return-void
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mTimezoneList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$202(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mFilteredTimezoneList:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mFilteredTimezoneList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mFilter:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mFilteredTimezoneList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 40
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 45
    if-nez p2, :cond_0

    .line 46
    new-instance p2, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;

    .end local p2    # "convertView":Landroid/view/View;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;-><init>(Landroid/content/Context;)V

    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    move-object v0, p2

    .line 49
    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;->bind(Ljava/lang/String;)V

    .line 51
    return-object p2
.end method
