.class Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;
.super Landroid/widget/Filter;
.source "StreamOptionsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;


# direct methods
.method private constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;
    .param p2, "x1"    # Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$1;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 8
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 63
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "filterString":Ljava/lang/String;
    new-instance v6, Landroid/widget/Filter$FilterResults;

    invoke-direct {v6}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 67
    .local v6, "results":Landroid/widget/Filter$FilterResults;
    iget-object v7, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;

    # getter for: Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mTimezoneList:Ljava/util/List;
    invoke-static {v7}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;)Ljava/util/List;

    move-result-object v4

    .line 69
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 70
    .local v0, "count":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 74
    .local v5, "nlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_1

    .line 75
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 76
    .local v2, "filterableString":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 77
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 81
    .end local v2    # "filterableString":Ljava/lang/String;
    :cond_1
    iput-object v5, v6, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 82
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    iput v7, v6, Landroid/widget/Filter$FilterResults;->count:I

    .line 84
    return-object v6
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 90
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    # setter for: Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->mFilteredTimezoneList:Ljava/util/List;
    invoke-static {v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->access$202(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;Ljava/util/List;)Ljava/util/List;

    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter$ItemFilter;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->notifyDataSetChanged()V

    .line 92
    return-void
.end method
