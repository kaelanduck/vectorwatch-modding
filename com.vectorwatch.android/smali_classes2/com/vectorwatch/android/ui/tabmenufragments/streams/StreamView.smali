.class public Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;
.super Landroid/widget/RelativeLayout;
.source "StreamView.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mTimeZoneTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;->mContext:Landroid/content/Context;

    .line 23
    const-string v1, "layout_inflater"

    .line 24
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 25
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03007a

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 27
    const v1, 0x7f100229

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;->mLayout:Landroid/widget/RelativeLayout;

    .line 29
    const v1, 0x7f10022a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;->mTimeZoneTextView:Landroid/widget/TextView;

    .line 30
    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/String;)V
    .locals 1
    .param p1, "timeZone"    # Ljava/lang/String;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamView;->mTimeZoneTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    return-void
.end method
