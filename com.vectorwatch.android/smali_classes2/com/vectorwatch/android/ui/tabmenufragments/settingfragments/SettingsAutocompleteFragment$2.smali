.class Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;
.super Ljava/lang/Object;
.source "SettingsAutocompleteFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 133
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 128
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 112
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->getAsYouType()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->getMinChars()I

    move-result v1

    if-lez v1, :cond_1

    .line 114
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->getMinChars()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 115
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$500(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 116
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-interface {v1, v2, v3, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->requestOptions(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;Ljava/lang/String;)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$2;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
