.class Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;
.super Ljava/lang/Object;
.source "SettingsAutocompleteFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    .line 139
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x42

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x6

    if-eq p2, v0, :cond_1

    const/4 v0, 0x5

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$002(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;Ljava/lang/String;)Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    move-result-object v5

    .line 145
    .local v5, "dataDetails":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    if-nez v5, :cond_3

    .line 146
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-interface/range {v0 .. v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->select(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPropertyValueModel;)V

    .line 153
    .end local v5    # "dataDetails":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 148
    .restart local v5    # "dataDetails":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$3;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 149
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getType()Ljava/lang/String;

    move-result-object v4

    .line 148
    invoke-interface/range {v0 .. v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->select(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPropertyValueModel;)V

    goto :goto_0
.end method
