.class Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;
.super Ljava/lang/Object;
.source "SettingsAutocompleteFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$100(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamOptionsListAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$002(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$200(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)V

    .line 96
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$300(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;Ljava/lang/String;)Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    move-result-object v5

    .line 98
    .local v5, "dataDetails":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    if-eqz v5, :cond_1

    .line 99
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 100
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getType()Ljava/lang/String;

    move-result-object v4

    .line 99
    invoke-interface/range {v0 .. v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->select(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPropertyValueModel;)V

    .line 106
    .end local v5    # "dataDetails":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    :cond_0
    :goto_0
    return-void

    .line 102
    .restart local v5    # "dataDetails":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    iget-object v6, v0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->mCallbacks:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$400(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->access$000(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-interface/range {v6 .. v11}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;->select(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPropertyValueModel;)V

    goto :goto_0
.end method
