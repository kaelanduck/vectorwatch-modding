.class Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$1;
.super Ljava/lang/Object;
.source "SettingsPropertyFragment.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public continueOnEmptyOption()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public getHint(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 88
    const/4 v0, 0x0

    return-object v0
.end method

.method public getValues(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public requestOptions(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "fragment"    # Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    .param p3, "currentValue"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-virtual {p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onOptionsReceived()V

    .line 93
    return-void
.end method

.method public select(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPropertyValueModel;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "optionName"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "dataModel"    # Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    .prologue
    .line 78
    return-void
.end method
