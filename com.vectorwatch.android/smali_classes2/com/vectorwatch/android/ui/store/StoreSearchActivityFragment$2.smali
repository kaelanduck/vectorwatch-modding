.class Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;
.super Ljava/lang/Object;
.source "StoreSearchActivityFragment.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->handleLoadedStoreElementsEvent(Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

.field final synthetic val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;Lcom/vectorwatch/android/utils/AppInstallManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSaveAction(Lcom/vectorwatch/android/models/StoreElement;I)V
    .locals 8
    .param p1, "item"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "positionInList"    # I

    .prologue
    const v7, 0x7f09005d

    const/16 v6, 0x5dc

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 137
    new-instance v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 138
    .local v0, "elementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 139
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 145
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vectorwatch/android/managers/StreamsManager;->isStreamSaved(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    .line 146
    .local v1, "isAppSaved":Z
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    if-eqz v1, :cond_2

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 153
    .local v2, "isInstalled":Z
    :cond_1
    :goto_0
    if-nez v2, :cond_a

    .line 154
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v3

    if-nez v3, :cond_8

    .line 155
    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 190
    :goto_1
    return-void

    .end local v2    # "isInstalled":Z
    :cond_2
    move v2, v3

    .line 146
    goto :goto_0

    .line 148
    .end local v1    # "isAppSaved":Z
    :cond_3
    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/vectorwatch/android/managers/CloudAppsManager;->isAppInstalled(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)Z

    move-result v1

    .line 149
    .restart local v1    # "isAppSaved":Z
    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    if-eqz v1, :cond_6

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 150
    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vectorwatch/android/utils/Helpers;->stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .restart local v2    # "isInstalled":Z
    :cond_5
    :goto_2
    goto :goto_0

    .end local v2    # "isInstalled":Z
    :cond_6
    move v2, v3

    goto :goto_2

    .line 158
    .restart local v2    # "isInstalled":Z
    :cond_7
    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v5, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 160
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v6

    .line 158
    invoke-static {v3, v4, v5, v6}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 162
    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v3, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->installAppFromStore(Lcom/vectorwatch/android/models/StoreElement;)V

    goto :goto_1

    .line 165
    :cond_8
    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 166
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_1

    .line 168
    :cond_9
    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v5, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 170
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v6

    .line 168
    invoke-static {v3, v4, v5, v6}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 172
    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->val$appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v3, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->installStreamFromStore(Lcom/vectorwatch/android/models/StoreElement;)V

    goto/16 :goto_1

    .line 176
    :cond_a
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->isStream()Z

    move-result v3

    if-nez v3, :cond_b

    .line 177
    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_APP:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v5, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 179
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v6

    .line 177
    invoke-static {v3, v4, v5, v6}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 181
    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v0, p2, v3}, Lcom/vectorwatch/android/managers/CloudAppsManager;->uninstallAppFromWatch(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V

    goto/16 :goto_1

    .line 183
    :cond_b
    iget-object v3, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v5, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DELETED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 185
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v6

    .line 183
    invoke-static {v3, v4, v5, v6}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 187
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$2;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vectorwatch/android/managers/StreamsManager;->deleteStreamFromDatabase(Ljava/lang/String;Landroid/content/Context;)Z

    goto/16 :goto_1
.end method
