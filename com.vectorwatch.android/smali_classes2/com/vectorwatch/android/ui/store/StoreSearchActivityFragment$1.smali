.class Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$1;
.super Ljava/lang/Object;
.source "StoreSearchActivityFragment.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/store/OnEndListCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(I)V
    .locals 6
    .param p1, "page"    # I

    .prologue
    .line 104
    sput p1, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mCurrentPage:I

    .line 105
    iget-object v1, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    new-instance v2, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    sget-object v3, Lcom/vectorwatch/android/models/cloud/AppsOption;->ALL:Lcom/vectorwatch/android/models/cloud/AppsOption;

    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-static {v0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->access$000(Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    move-result-object v5

    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;

    sget-object v0, Lcom/vectorwatch/android/ui/store/StoreSearchActivity;->mCurrentQuery:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;-><init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Landroid/app/Activity;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;Ljava/lang/String;)V

    iput-object v2, v1, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    .line 106
    iget-object v0, p0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment$1;->this$0:Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/store/StoreSearchActivityFragment;->mDownloadStoreElementsSummary:Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadStoreElementsSummaryAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 107
    return-void
.end method
