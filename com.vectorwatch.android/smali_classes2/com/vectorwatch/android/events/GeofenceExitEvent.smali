.class public Lcom/vectorwatch/android/events/GeofenceExitEvent;
.super Ljava/lang/Object;
.source "GeofenceExitEvent.java"


# instance fields
.field private geoFenceId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "geoFenceId"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/vectorwatch/android/events/GeofenceExitEvent;->geoFenceId:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getGeoFenceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/events/GeofenceExitEvent;->geoFenceId:Ljava/lang/String;

    return-object v0
.end method
