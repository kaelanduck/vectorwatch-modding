.class public Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;
.super Ljava/lang/Object;
.source "AppInstalledOnWatchEvent.java"


# instance fields
.field private appId:I

.field private mPosition:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "appId"    # I
    .param p2, "position"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p2, p0, Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;->mPosition:I

    .line 12
    iput p1, p0, Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;->appId:I

    .line 13
    return-void
.end method


# virtual methods
.method public getAppId()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;->appId:I

    return v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;->mPosition:I

    return v0
.end method
