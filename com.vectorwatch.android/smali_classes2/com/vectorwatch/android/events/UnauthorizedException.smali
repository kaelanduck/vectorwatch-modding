.class public Lcom/vectorwatch/android/events/UnauthorizedException;
.super Ljava/lang/Throwable;
.source "UnauthorizedException.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Throwable;-><init>()V

    .line 18
    invoke-direct {p0}, Lcom/vectorwatch/android/events/UnauthorizedException;->handleUnauthorizedException()V

    .line 19
    return-void
.end method

.method private handleUnauthorizedException()V
    .locals 5

    .prologue
    .line 22
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 24
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 25
    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->logoutDrivenResets(Landroid/content/Context;)Z

    move-result v2

    .line 27
    .local v2, "ok":Z
    if-eqz v2, :cond_0

    .line 28
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 29
    .local v1, "intent":Landroid/content/Intent;
    const v3, 0x10008000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 30
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 33
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "ok":Z
    :cond_0
    return-void
.end method
