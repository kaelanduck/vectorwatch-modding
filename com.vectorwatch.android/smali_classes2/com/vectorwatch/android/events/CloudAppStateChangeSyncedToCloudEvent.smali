.class public Lcom/vectorwatch/android/events/CloudAppStateChangeSyncedToCloudEvent;
.super Ljava/lang/Object;
.source "CloudAppStateChangeSyncedToCloudEvent.java"


# instance fields
.field private mAppId:I

.field private mAppState:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "appId"    # I
    .param p2, "state"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/vectorwatch/android/events/CloudAppStateChangeSyncedToCloudEvent;->mAppId:I

    .line 12
    iput-object p2, p0, Lcom/vectorwatch/android/events/CloudAppStateChangeSyncedToCloudEvent;->mAppState:Ljava/lang/String;

    .line 13
    return-void
.end method


# virtual methods
.method public getAppId()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/events/CloudAppStateChangeSyncedToCloudEvent;->mAppId:I

    return v0
.end method

.method public getAppState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/events/CloudAppStateChangeSyncedToCloudEvent;->mAppState:Ljava/lang/String;

    return-object v0
.end method
