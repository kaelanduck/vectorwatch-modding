.class public Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;
.super Ljava/lang/Object;
.source "BatteryInformationUpdateEvent.java"


# instance fields
.field private mPercentage:B

.field private mStatus:B


# direct methods
.method public constructor <init>(BB)V
    .locals 0
    .param p1, "percentage"    # B
    .param p2, "status"    # B

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-byte p1, p0, Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;->mPercentage:B

    .line 12
    iput-byte p2, p0, Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;->mStatus:B

    .line 13
    return-void
.end method


# virtual methods
.method public getPercentage()B
    .locals 1

    .prologue
    .line 16
    iget-byte v0, p0, Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;->mPercentage:B

    return v0
.end method

.method public getStatus()B
    .locals 1

    .prologue
    .line 19
    iget-byte v0, p0, Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;->mStatus:B

    return v0
.end method
