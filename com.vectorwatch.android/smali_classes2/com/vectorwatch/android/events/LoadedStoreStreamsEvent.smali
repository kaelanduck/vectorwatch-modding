.class public Lcom/vectorwatch/android/events/LoadedStoreStreamsEvent;
.super Ljava/lang/Object;
.source "LoadedStoreStreamsEvent.java"


# instance fields
.field storeElements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;"
        }
    .end annotation
.end field

.field streamType:Lcom/vectorwatch/android/models/cloud/AppsOption;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Ljava/util/List;)V
    .locals 0
    .param p1, "streamType"    # Lcom/vectorwatch/android/models/cloud/AppsOption;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/cloud/AppsOption;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p2, "storeElementsList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p2, p0, Lcom/vectorwatch/android/events/LoadedStoreStreamsEvent;->storeElements:Ljava/util/List;

    .line 27
    iput-object p1, p0, Lcom/vectorwatch/android/events/LoadedStoreStreamsEvent;->streamType:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 28
    return-void
.end method


# virtual methods
.method public getStoreElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/events/LoadedStoreStreamsEvent;->storeElements:Ljava/util/List;

    return-object v0
.end method

.method public getStreamType()Lcom/vectorwatch/android/models/cloud/AppsOption;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/events/LoadedStoreStreamsEvent;->streamType:Lcom/vectorwatch/android/models/cloud/AppsOption;

    return-object v0
.end method
