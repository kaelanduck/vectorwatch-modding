.class public Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;
.super Ljava/lang/Object;
.source "WatchUuidInfoReceivedEvent.java"


# instance fields
.field uuidInfo:[B

.field version:S


# direct methods
.method public constructor <init>([BS)V
    .locals 0
    .param p1, "data"    # [B
    .param p2, "version"    # S

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;->uuidInfo:[B

    .line 11
    iput-short p2, p0, Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;->version:S

    .line 12
    return-void
.end method


# virtual methods
.method public getUuidInfo()[B
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;->uuidInfo:[B

    return-object v0
.end method

.method public getVersion()S
    .locals 1

    .prologue
    .line 19
    iget-short v0, p0, Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;->version:S

    return v0
.end method
