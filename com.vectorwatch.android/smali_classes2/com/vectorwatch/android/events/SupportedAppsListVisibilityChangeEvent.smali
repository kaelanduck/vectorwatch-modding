.class public Lcom/vectorwatch/android/events/SupportedAppsListVisibilityChangeEvent;
.super Ljava/lang/Object;
.source "SupportedAppsListVisibilityChangeEvent.java"


# instance fields
.field private mChecked:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-boolean p1, p0, Lcom/vectorwatch/android/events/SupportedAppsListVisibilityChangeEvent;->mChecked:Z

    .line 11
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/vectorwatch/android/events/SupportedAppsListVisibilityChangeEvent;->mChecked:Z

    return v0
.end method
