.class public Lcom/vectorwatch/android/events/DuplicateAccountEvent;
.super Ljava/lang/Object;
.source "DuplicateAccountEvent.java"


# instance fields
.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/events/DuplicateAccountEvent;->setMessage(Ljava/lang/String;)V

    .line 12
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vectorwatch/android/events/DuplicateAccountEvent;->message:Ljava/lang/String;

    return-object v0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vectorwatch/android/events/DuplicateAccountEvent;->message:Ljava/lang/String;

    .line 20
    return-void
.end method
