.class public Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;
.super Ljava/lang/Object;
.source "FullAppDetailsSavedToDbEvent.java"


# instance fields
.field private app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

.field private position:I


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/DownloadedAppDetails;I)V
    .locals 0
    .param p1, "app"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .param p2, "positionInList"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 14
    iput p2, p0, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;->position:I

    .line 15
    return-void
.end method


# virtual methods
.method public getApp()Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;->position:I

    return v0
.end method
