.class public Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;
.super Ljava/lang/Object;
.source "AppUninstalledFromWatchEvent.java"


# instance fields
.field private elementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 0
    .param p1, "elementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;->elementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 13
    return-void
.end method


# virtual methods
.method public getElementSummary()Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;->elementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

    return-object v0
.end method
