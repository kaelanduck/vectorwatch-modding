.class public Lcom/vectorwatch/android/events/CommandStatusEvent;
.super Ljava/lang/Object;
.source "CommandStatusEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/events/CommandStatusEvent$Status;
    }
.end annotation


# instance fields
.field private commandUuid:Ljava/util/UUID;

.field private packetIndex:I

.field private status:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

.field private totalPackets:I


# direct methods
.method public constructor <init>(Ljava/util/UUID;Lcom/vectorwatch/android/events/CommandStatusEvent$Status;II)V
    .locals 0
    .param p1, "commandUuid"    # Ljava/util/UUID;
    .param p2, "status"    # Lcom/vectorwatch/android/events/CommandStatusEvent$Status;
    .param p3, "packetIndex"    # I
    .param p4, "totalPackets"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/vectorwatch/android/events/CommandStatusEvent;->commandUuid:Ljava/util/UUID;

    .line 25
    iput-object p2, p0, Lcom/vectorwatch/android/events/CommandStatusEvent;->status:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    .line 26
    iput p3, p0, Lcom/vectorwatch/android/events/CommandStatusEvent;->packetIndex:I

    .line 27
    iput p4, p0, Lcom/vectorwatch/android/events/CommandStatusEvent;->totalPackets:I

    .line 28
    return-void
.end method


# virtual methods
.method public getCommandUuid()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/events/CommandStatusEvent;->commandUuid:Ljava/util/UUID;

    return-object v0
.end method

.method public getPacketIndex()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/vectorwatch/android/events/CommandStatusEvent;->packetIndex:I

    return v0
.end method

.method public getStatus()Lcom/vectorwatch/android/events/CommandStatusEvent$Status;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vectorwatch/android/events/CommandStatusEvent;->status:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    return-object v0
.end method

.method public getTotalPackets()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/vectorwatch/android/events/CommandStatusEvent;->totalPackets:I

    return v0
.end method
