.class public Lcom/vectorwatch/android/events/AuthTokenRenewedEvent;
.super Ljava/lang/Object;
.source "AuthTokenRenewedEvent.java"


# instance fields
.field private authToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/vectorwatch/android/events/AuthTokenRenewedEvent;->authToken:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public getAuthToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vectorwatch/android/events/AuthTokenRenewedEvent;->authToken:Ljava/lang/String;

    return-object v0
.end method
