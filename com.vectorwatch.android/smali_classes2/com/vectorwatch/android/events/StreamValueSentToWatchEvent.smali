.class public Lcom/vectorwatch/android/events/StreamValueSentToWatchEvent;
.super Ljava/lang/Object;
.source "StreamValueSentToWatchEvent.java"


# instance fields
.field private appId:I

.field private elementId:I

.field private faceId:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .param p3, "fieldId"    # I

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/vectorwatch/android/events/StreamValueSentToWatchEvent;->appId:I

    .line 13
    iput p3, p0, Lcom/vectorwatch/android/events/StreamValueSentToWatchEvent;->elementId:I

    .line 14
    iput p2, p0, Lcom/vectorwatch/android/events/StreamValueSentToWatchEvent;->faceId:I

    .line 15
    return-void
.end method


# virtual methods
.method public getAppId()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/vectorwatch/android/events/StreamValueSentToWatchEvent;->appId:I

    return v0
.end method

.method public getElementId()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/vectorwatch/android/events/StreamValueSentToWatchEvent;->elementId:I

    return v0
.end method

.method public getFaceId()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/vectorwatch/android/events/StreamValueSentToWatchEvent;->faceId:I

    return v0
.end method
