.class public Lcom/vectorwatch/android/events/NoNetworkConnectionEvent;
.super Ljava/lang/Object;
.source "NoNetworkConnectionEvent.java"


# instance fields
.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/events/NoNetworkConnectionEvent;->setMessage(Ljava/lang/String;)V

    .line 11
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/vectorwatch/android/events/NoNetworkConnectionEvent;->message:Ljava/lang/String;

    return-object v0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vectorwatch/android/events/NoNetworkConnectionEvent;->message:Ljava/lang/String;

    .line 19
    return-void
.end method
