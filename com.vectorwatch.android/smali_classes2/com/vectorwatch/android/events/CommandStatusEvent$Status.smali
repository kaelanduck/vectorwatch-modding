.class public final enum Lcom/vectorwatch/android/events/CommandStatusEvent$Status;
.super Ljava/lang/Enum;
.source "CommandStatusEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/events/CommandStatusEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/events/CommandStatusEvent$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

.field public static final enum FAIL:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

.field public static final enum PROGRESS:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

.field public static final enum SUCCESS:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->SUCCESS:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    new-instance v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->FAIL:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    new-instance v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    const-string v1, "PROGRESS"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->PROGRESS:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    .line 10
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    sget-object v1, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->SUCCESS:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->FAIL:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->PROGRESS:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->$VALUES:[Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/events/CommandStatusEvent$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/events/CommandStatusEvent$Status;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->$VALUES:[Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    return-object v0
.end method
