.class public Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;
.super Lcom/vectorwatch/android/events/StreamEvent;
.source "StreamPossibleSettingsReceivedEvent.java"


# instance fields
.field private elemId:Ljava/lang/Integer;

.field private streamPosInList:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/Stream;)V
    .locals 0
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/events/StreamEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/Stream;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "streamPosInList"    # Ljava/lang/Integer;
    .param p3, "elemId"    # Ljava/lang/Integer;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/events/StreamEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    .line 16
    iput-object p2, p0, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;->streamPosInList:Ljava/lang/Integer;

    .line 17
    iput-object p3, p0, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;->elemId:Ljava/lang/Integer;

    .line 18
    return-void
.end method


# virtual methods
.method public getElemId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;->elemId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getStreamPosInList()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;->streamPosInList:Ljava/lang/Integer;

    return-object v0
.end method
