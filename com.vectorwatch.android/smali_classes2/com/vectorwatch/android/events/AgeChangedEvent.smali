.class public Lcom/vectorwatch/android/events/AgeChangedEvent;
.super Ljava/lang/Object;
.source "AgeChangedEvent.java"


# instance fields
.field private mAge:I

.field private mDateOfBirthInMillis:J


# direct methods
.method public constructor <init>(IJ)V
    .locals 0
    .param p1, "age"    # I
    .param p2, "dateOfBirthInMillis"    # J

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/vectorwatch/android/events/AgeChangedEvent;->mAge:I

    .line 12
    iput-wide p2, p0, Lcom/vectorwatch/android/events/AgeChangedEvent;->mDateOfBirthInMillis:J

    .line 13
    return-void
.end method


# virtual methods
.method public getAge()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/events/AgeChangedEvent;->mAge:I

    return v0
.end method

.method public getDateOfBirthInMillis()J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/vectorwatch/android/events/AgeChangedEvent;->mDateOfBirthInMillis:J

    return-wide v0
.end method
