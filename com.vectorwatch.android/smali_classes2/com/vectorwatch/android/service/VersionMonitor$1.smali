.class final Lcom/vectorwatch/android/service/VersionMonitor$1;
.super Ljava/lang/Object;
.source "VersionMonitor.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/service/VersionMonitor;->startCompatibilityChecks(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/SoftwareUpdateModel;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 4
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 88
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getBody()Ljava/lang/Object;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 89
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/SoftwareUpdateModel;Lretrofit/client/Response;)V
    .locals 3
    .param p1, "softwareUpdateModel"    # Lcom/vectorwatch/android/models/SoftwareUpdateModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 77
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getErrorMessage()Lcom/vectorwatch/android/events/CloudMessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 78
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getErrorMessage()Lcom/vectorwatch/android/events/CloudMessageModel;

    move-result-object v0

    .line 80
    .local v0, "cloudMessage":Lcom/vectorwatch/android/events/CloudMessageModel;
    invoke-static {v0}, Lcom/vectorwatch/android/service/VersionMonitor;->handleResponseFromServer(Lcom/vectorwatch/android/events/CloudMessageModel;)V

    .line 84
    .end local v0    # "cloudMessage":Lcom/vectorwatch/android/events/CloudMessageModel;
    :goto_0
    return-void

    .line 82
    :cond_0
    const-string v1, "Unexpected"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/service/VersionMonitor$1;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 74
    check-cast p1, Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/service/VersionMonitor$1;->success(Lcom/vectorwatch/android/models/SoftwareUpdateModel;Lretrofit/client/Response;)V

    return-void
.end method
