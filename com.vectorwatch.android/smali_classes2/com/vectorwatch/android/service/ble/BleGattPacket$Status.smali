.class public final enum Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;
.super Ljava/lang/Enum;
.source "BleGattPacket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/BleGattPacket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

.field public static final enum FAIL:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

.field public static final enum SUCCESS:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->SUCCESS:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    new-instance v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->FAIL:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->SUCCESS:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->FAIL:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->$VALUES:[Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->$VALUES:[Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    return-object v0
.end method
