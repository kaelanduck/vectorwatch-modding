.class public interface abstract Lcom/vectorwatch/android/service/ble/Transceiver;
.super Ljava/lang/Object;
.source "Transceiver.java"


# virtual methods
.method public abstract addPacketsToBuffer(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract closeConnection()V
.end method

.method public abstract connect()Z
.end method

.method public abstract destroy()V
.end method

.method public abstract disconnect()V
.end method

.method public abstract getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;
.end method

.method public abstract readRssi()Z
.end method

.method public abstract reconnect()V
.end method
