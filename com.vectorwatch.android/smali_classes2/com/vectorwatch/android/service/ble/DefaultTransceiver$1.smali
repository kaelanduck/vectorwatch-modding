.class Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;
.super Landroid/bluetooth/BluetoothGattCallback;
.source "DefaultTransceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->getBleCallback()Landroid/bluetooth/BluetoothGattCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 5
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 82
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object v0

    .line 84
    .local v0, "data":[B
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    .line 85
    .local v1, "uuid":Ljava/util/UUID;
    if-nez v0, :cond_0

    .line 86
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v2

    const-string v3, "TRANSCEIVER: NO DATA"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 91
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    sget-object v3, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_RECEIVE_RAW_DATA_FROM_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    new-instance v4, Lcom/vectorwatch/android/models/BtResponse;

    invoke-direct {v4, v1, v0}, Lcom/vectorwatch/android/models/BtResponse;-><init>(Ljava/util/UUID;[B)V

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->dispatchMessageToHandlerIfAlive(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V
    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$100(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 0
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "status"    # I

    .prologue
    .line 97
    return-void
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 3
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "status"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 104
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "TRANSCEIVER: on_characteristic_write gatt != mBluetoothGatt."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    if-nez p3, :cond_3

    .line 109
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$300()Lcom/vectorwatch/android/service/ble/BleGattPacket;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 110
    if-eqz p2, :cond_2

    .line 111
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$300()Lcom/vectorwatch/android/service/ble/BleGattPacket;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getCharacteristicUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->SUCCESS:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->handlePacketStatus(Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V
    invoke-static {v0, v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$400(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V

    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sendNextBlePacket()V
    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$500(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V

    goto :goto_0

    .line 117
    :cond_2
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: Received onCharacteristicWrite for different characteristic: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_3
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: on_characteristic_write NOT OK : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->reconnect()V

    goto :goto_0
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 6
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "status"    # I
    .param p3, "newState"    # I

    .prologue
    const/16 v5, 0x85

    const/4 v4, 0x2

    .line 132
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TRANSCEIVER: on_connection_state_change - status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    if-nez v1, :cond_1

    .line 136
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "TRANSCEIVER: on_connection_state_change - mBluetoothGatt should not be null here."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    .line 139
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 141
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->handleBluetoothGattNullCase()V
    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$600(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 149
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "TRANSCEIVER: on_connection_state_change - mBluetoothGatt should not be null here (gatt != mBluetoothGatt."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :cond_2
    if-nez p3, :cond_3

    .line 157
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->cleanupBluetoothAppQueue()V
    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$700(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V

    .line 160
    :cond_3
    if-eqz p2, :cond_6

    .line 161
    if-ne p3, v4, :cond_4

    .line 162
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "TRANSCEIVER: New state == CONNECTED && status != SUCCESS"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 165
    :cond_4
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TRANSCEIVER: New state and status (!GAT_SUCCESS) state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 168
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    .line 171
    const/16 v1, 0x8

    if-eq p2, v1, :cond_5

    if-eq p2, v5, :cond_5

    .line 172
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TRANSCEIVER: !SUCCESS | !TIMEOUT !CONN_ERROR | status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 174
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->reconnect()V

    goto :goto_0

    .line 175
    :cond_5
    if-ne p2, v5, :cond_0

    .line 176
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->connect()Z

    goto/16 :goto_0

    .line 179
    :cond_6
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TRANSCEIVER: New state and status (GAT_SUCCESS) state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 181
    if-ne p3, v4, :cond_7

    .line 182
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGatt;->discoverServices()Z

    move-result v0

    .line 184
    .local v0, "servicesDiscovered":Z
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TRANSCEIVER: Attempted to start service discovery: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 185
    .end local v0    # "servicesDiscovered":Z
    :cond_7
    if-nez p3, :cond_0

    .line 186
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    .line 188
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->isPossibleToReconnect()Z
    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$800(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "TRANSCEIVER: Disconnected from gatt on KitKat version. Trying to reconnect."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 191
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->connect()Z

    goto/16 :goto_0
.end method

.method public onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 4
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "descriptor"    # Landroid/bluetooth/BluetoothGattDescriptor;
    .param p3, "status"    # I

    .prologue
    const/4 v3, 0x2

    .line 200
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    if-nez v0, :cond_1

    .line 201
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "TRANSCEIVER: on_descriptor_write - BluetoothGatt not initialized"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: on_descriptor_write: descriptor uuid and status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 213
    if-eqz p3, :cond_2

    const/16 v0, 0x80

    if-ne p3, v0, :cond_4

    .line 214
    :cond_2
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$300()Lcom/vectorwatch/android/service/ble/BleGattPacket;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 215
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$300()Lcom/vectorwatch/android/service/ble/BleGattPacket;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getDescriptor()Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 216
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$300()Lcom/vectorwatch/android/service/ble/BleGattPacket;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getDescriptor()Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 217
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$300()Lcom/vectorwatch/android/service/ble/BleGattPacket;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getDescriptor()Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # operator++ for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWrittenDescriptors:I
    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$908(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)I

    .line 220
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: Wrote GATT Descriptor successfully. Written = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWrittenDescriptors:I
    invoke-static {v2}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$900(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWrittenDescriptors:I
    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$900(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)I

    move-result v0

    if-lt v0, v3, :cond_3

    .line 223
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "TRANSCEIVER: Wrote all descriptors. Change state to CONNECTED."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 224
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    .line 227
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->SUCCESS:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->handlePacketStatus(Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V
    invoke-static {v0, v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$400(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V

    .line 228
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sendNextBlePacket()V
    invoke-static {v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$500(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V

    goto/16 :goto_0

    .line 232
    :cond_4
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "TRANSCEIVER: Error writing GATT Descriptor. Trying to reconnect."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->reconnect()V

    goto/16 :goto_0
.end method

.method public onReadRemoteRssi(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 3
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "rssi"    # I
    .param p3, "status"    # I

    .prologue
    .line 244
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: on_remote_read - Have read remote RSSI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 245
    if-nez p3, :cond_0

    .line 246
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: Status for remote rssi is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 248
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/RemoteRssiReceivedEvent;

    invoke-direct {v1, p2}, Lcom/vectorwatch/android/events/RemoteRssiReceivedEvent;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 252
    :goto_0
    return-void

    .line 250
    :cond_0
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: Status for remote rssi is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 12
    .param p1, "gatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "status"    # I

    .prologue
    .line 256
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "TRANSCEIVER: on_service_discovered - status:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 260
    if-eqz p1, :cond_0

    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 261
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    const-string v10, "TRANSCEIVER: on_services_discovered - mBluetoothGatt should not be null here (gatt != mBluetoothGatt."

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 340
    :goto_0
    return-void

    .line 266
    :cond_0
    if-nez p2, :cond_6

    .line 267
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->getSupportedGattService()Landroid/bluetooth/BluetoothGattService;
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$1000(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v1

    .line 268
    .local v1, "bluetoothGattNotifService":Landroid/bluetooth/BluetoothGattService;
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->getSupportedGattDataService()Landroid/bluetooth/BluetoothGattService;

    move-result-object v0

    .line 270
    .local v0, "bluetoothGattDataService":Landroid/bluetooth/BluetoothGattService;
    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    .line 272
    :cond_1
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    const-string v10, "TRANSCEIVER: BluetoothGatt null at notifications/data get supported service"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 273
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->connect()Z

    goto :goto_0

    .line 276
    :cond_2
    sget-object v9, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_TX:Ljava/util/UUID;

    invoke-virtual {v1, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v5

    .line 278
    .local v5, "characteristicTx":Landroid/bluetooth/BluetoothGattCharacteristic;
    sget-object v9, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_TX_NOT_INFO:Ljava/util/UUID;

    invoke-virtual {v1, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v6

    .line 280
    .local v6, "characteristicTxNot":Landroid/bluetooth/BluetoothGattCharacteristic;
    sget-object v9, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_RX:Ljava/util/UUID;

    invoke-virtual {v1, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v4

    .line 283
    .local v4, "characteristicRx":Landroid/bluetooth/BluetoothGattCharacteristic;
    sget-object v9, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_DATA_TX:Ljava/util/UUID;

    invoke-virtual {v0, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v3

    .line 285
    .local v3, "characteristicDataTx":Landroid/bluetooth/BluetoothGattCharacteristic;
    sget-object v9, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_DATA_RX:Ljava/util/UUID;

    invoke-virtual {v0, v9}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v2

    .line 289
    .local v2, "characteristicDataRx":Landroid/bluetooth/BluetoothGattCharacteristic;
    if-eqz v4, :cond_3

    if-eqz v5, :cond_3

    if-eqz v6, :cond_3

    if-eqz v2, :cond_3

    if-nez v3, :cond_4

    .line 292
    :cond_3
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    const-string v10, "TRANSCEIVER: One of the characteristics was null at discover services."

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 293
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->reconnect()V

    goto :goto_0

    .line 296
    :cond_4
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$1100(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v10

    invoke-interface {v9, v10, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$1100(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v10

    invoke-interface {v9, v10, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$1100(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v10

    invoke-interface {v9, v10, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    const/4 v9, 0x2

    invoke-virtual {v3, v9}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    .line 302
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$1100(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v10

    invoke-interface {v9, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$1100(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Ljava/util/Map;

    move-result-object v9

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v10

    invoke-interface {v9, v10, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->cleanupBluetoothAppQueue()V
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$700(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V

    .line 309
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;
    invoke-static {v10}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v10

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->configureDescriptors(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)Z
    invoke-static {v9, v10, v4, v2}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$1200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v8

    .line 311
    .local v8, "success":Z
    if-eqz v8, :cond_5

    .line 312
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    const-string v10, "TRANSCEIVER: Configured descriptors successfully."

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 314
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    const/4 v10, 0x0

    # setter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWrittenDescriptors:I
    invoke-static {v9, v10}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$902(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;I)I

    .line 325
    const-wide/16 v10, 0x1f4

    :try_start_0
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    :goto_1
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    # invokes: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sendNextBlePacket()V
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$500(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V

    goto/16 :goto_0

    .line 326
    :catch_0
    move-exception v7

    .line 327
    .local v7, "e":Ljava/lang/Exception;
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    const-string v10, "TRANSCEIVER: Error at sleep!"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 331
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_5
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    const-string v10, "TRANSCEIVER: error at configuring descriptors."

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 332
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->reconnect()V

    goto/16 :goto_0

    .line 337
    .end local v0    # "bluetoothGattDataService":Landroid/bluetooth/BluetoothGattService;
    .end local v1    # "bluetoothGattNotifService":Landroid/bluetooth/BluetoothGattService;
    .end local v2    # "characteristicDataRx":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v3    # "characteristicDataTx":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v4    # "characteristicRx":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v5    # "characteristicTx":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v6    # "characteristicTxNot":Landroid/bluetooth/BluetoothGattCharacteristic;
    .end local v8    # "success":Z
    :cond_6
    # getter for: Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "TRANSCEIVER: on_services_discovered received (!GATT_SUCCESS): "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 338
    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;->this$0:Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-virtual {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->reconnect()V

    goto/16 :goto_0
.end method
