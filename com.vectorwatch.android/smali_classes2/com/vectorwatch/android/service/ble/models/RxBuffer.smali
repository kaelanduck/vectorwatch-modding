.class public Lcom/vectorwatch/android/service/ble/models/RxBuffer;
.super Ljava/lang/Object;
.source "RxBuffer.java"


# instance fields
.field private buffer:[B

.field private index:S

.field private packets:S

.field private receptionId:B

.field private totalPackets:S


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->buffer:[B

    .line 15
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->invalidate()V

    .line 16
    return-void
.end method


# virtual methods
.method public add([B)I
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->index:S

    array-length v2, p1

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->buffer:[B

    array-length v2, v2

    if-le v1, v2, :cond_0

    .line 34
    const/4 v0, -0x1

    .line 40
    :goto_0
    return v0

    .line 36
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->buffer:[B

    iget-short v2, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->index:S

    array-length v3, p1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->index:S

    array-length v2, p1

    int-to-short v2, v2

    add-int/2addr v1, v2

    int-to-short v1, v1

    iput-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->index:S

    .line 38
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->packets:S

    add-int/lit8 v1, v1, 0x1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->packets:S

    goto :goto_0
.end method

.method public getBuffer()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 51
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->index:S

    new-array v0, v1, [B

    .line 52
    .local v0, "ret":[B
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->buffer:[B

    iget-short v2, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->index:S

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53
    return-object v0
.end method

.method public getPackets()S
    .locals 1

    .prologue
    .line 61
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->packets:S

    return v0
.end method

.method public getReceptionId()B
    .locals 1

    .prologue
    .line 65
    iget-byte v0, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->receptionId:B

    return v0
.end method

.method public getTotalPackets()S
    .locals 1

    .prologue
    .line 57
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->totalPackets:S

    return v0
.end method

.method public initialize([BBS)I
    .locals 3
    .param p1, "buffer"    # [B
    .param p2, "receptionID"    # B
    .param p3, "packets"    # S

    .prologue
    const/4 v0, 0x0

    .line 19
    array-length v1, p1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->buffer:[B

    array-length v2, v2

    if-le v1, v2, :cond_0

    .line 20
    const/4 v0, -0x1

    .line 29
    :goto_0
    return v0

    .line 22
    :cond_0
    iput-byte p2, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->receptionId:B

    .line 23
    iput-short p3, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->totalPackets:S

    .line 25
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->buffer:[B

    array-length v2, p1

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 26
    array-length v1, p1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->index:S

    .line 27
    const/4 v1, 0x1

    iput-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->packets:S

    goto :goto_0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    iput-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->index:S

    .line 45
    const/4 v0, -0x1

    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->receptionId:B

    .line 46
    iput-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->totalPackets:S

    .line 47
    iput-short v1, p0, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->packets:S

    .line 48
    return-void
.end method
