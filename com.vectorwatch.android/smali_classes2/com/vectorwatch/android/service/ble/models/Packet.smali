.class public Lcom/vectorwatch/android/service/ble/models/Packet;
.super Ljava/lang/Object;
.source "Packet.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public header:B

.field public payload:[B


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "length"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    .line 16
    return-void
.end method


# virtual methods
.method public deserialize([B)V
    .locals 4
    .param p1, "rawData"    # [B

    .prologue
    const/4 v3, 0x0

    .line 37
    aget-byte v0, p1, v3

    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/models/Packet;->header:B

    .line 38
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    array-length v2, v2

    invoke-static {p1, v0, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 39
    return-void
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public serialize()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 28
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [B

    .line 30
    .local v0, "rawData":[B
    iget-byte v1, p0, Lcom/vectorwatch/android/service/ble/models/Packet;->header:B

    aput-byte v1, v0, v4

    .line 31
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    array-length v3, v3

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 33
    return-object v0
.end method
