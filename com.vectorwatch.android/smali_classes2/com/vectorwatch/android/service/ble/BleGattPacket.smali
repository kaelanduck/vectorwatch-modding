.class public Lcom/vectorwatch/android/service/ble/BleGattPacket;
.super Ljava/lang/Object;
.source "BleGattPacket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;,
        Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;
    }
.end annotation


# instance fields
.field private characteristicUUID:Ljava/util/UUID;

.field private descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

.field private index:I

.field private packetUuid:Ljava/util/UUID;

.field private status:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

.field private totalPackets:I

.field private type:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

.field private val:[B


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothGattDescriptor;)V
    .locals 2
    .param p1, "descriptor"    # Landroid/bluetooth/BluetoothGattDescriptor;

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    .line 23
    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->packetUuid:Ljava/util/UUID;

    .line 24
    iput v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->index:I

    .line 25
    iput v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->totalPackets:I

    .line 26
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->FAIL:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->status:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    .line 39
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->WRITE_DESCRIPTOR:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->type:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    .line 40
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    .line 41
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattDescriptor;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->characteristicUUID:Ljava/util/UUID;

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/util/UUID;[BI)V
    .locals 3
    .param p1, "characteristicUUID"    # Ljava/util/UUID;
    .param p2, "val"    # [B
    .param p3, "length"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    .line 23
    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->packetUuid:Ljava/util/UUID;

    .line 24
    iput v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->index:I

    .line 25
    iput v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->totalPackets:I

    .line 26
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->FAIL:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->status:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    .line 32
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->WRITE_CHARACTERISTIC:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->type:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    .line 33
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->characteristicUUID:Ljava/util/UUID;

    .line 34
    new-array v0, p3, [B

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->val:[B

    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->val:[B

    invoke-static {p2, v1, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 36
    return-void
.end method


# virtual methods
.method public getCharacteristicUUID()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->characteristicUUID:Ljava/util/UUID;

    return-object v0
.end method

.method public getDescriptor()Landroid/bluetooth/BluetoothGattDescriptor;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->descriptor:Landroid/bluetooth/BluetoothGattDescriptor;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->index:I

    return v0
.end method

.method public getPacketUuid()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->packetUuid:Ljava/util/UUID;

    return-object v0
.end method

.method public getStatus()Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->status:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    return-object v0
.end method

.method public getTotalPackets()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->totalPackets:I

    return v0
.end method

.method public getType()Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->type:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    return-object v0
.end method

.method public getVal()[B
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->val:[B

    return-object v0
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->index:I

    .line 74
    return-void
.end method

.method public setPacketUuid(Ljava/util/UUID;)V
    .locals 0
    .param p1, "packetUuid"    # Ljava/util/UUID;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->packetUuid:Ljava/util/UUID;

    .line 66
    return-void
.end method

.method public setStatus(Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V
    .locals 0
    .param p1, "status"    # Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->status:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    .line 90
    return-void
.end method

.method public setTotalPackets(I)V
    .locals 0
    .param p1, "totalPackets"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/vectorwatch/android/service/ble/BleGattPacket;->totalPackets:I

    .line 82
    return-void
.end method
