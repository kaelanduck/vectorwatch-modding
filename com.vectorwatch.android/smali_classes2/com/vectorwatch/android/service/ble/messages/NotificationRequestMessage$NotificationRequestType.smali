.class public final enum Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;
.super Ljava/lang/Enum;
.source "NotificationRequestMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotificationRequestType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

.field public static final enum DETAILS:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

.field public static final enum NEGATIVE_ACTION:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

.field public static final enum POSITIVE_ACTION:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 149
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    const-string v1, "DETAILS"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->DETAILS:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    const-string v1, "POSITIVE_ACTION"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->POSITIVE_ACTION:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    const-string v1, "NEGATIVE_ACTION"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->NEGATIVE_ACTION:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    .line 148
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->DETAILS:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->POSITIVE_ACTION:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->NEGATIVE_ACTION:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->$VALUES:[Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 148
    const-class v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->$VALUES:[Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    return-object v0
.end method
