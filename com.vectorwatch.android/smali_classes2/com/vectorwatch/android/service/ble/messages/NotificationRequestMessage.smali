.class public Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;
.super Lcom/vectorwatch/android/service/ble/messages/BtMessage;
.source "NotificationRequestMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;,
        Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;
    }
.end annotation


# static fields
.field private static final APP_IDENTIFIER:B = 0x0t

.field private static final ID_MESSAGE:B = 0x3t

.field private static final ID_TITLE:B = 0x1t

.field private static final log:Lorg/slf4j/Logger;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mId:[B

.field private mRequestInfoNeeded:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mRequestType:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 4
    .param p1, "rawData"    # [B

    .prologue
    const/4 v3, 0x5

    .line 25
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;-><init>()V

    .line 27
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    packed-switch v0, :pswitch_data_0

    .line 54
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->log:Lorg/slf4j/Logger;

    const-string v1, "Should not get here. Notification request should have one of the treated types."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 57
    :goto_0
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->populateIdField([B)V

    .line 58
    return-void

    .line 29
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->log:Lorg/slf4j/Logger;

    const-string v1, "Received notification details request"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->DETAILS:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mRequestType:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    .line 31
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->populateDetailsRequestFields([B)V

    goto :goto_0

    .line 34
    :pswitch_2
    array-length v0, p1

    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    .line 35
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->log:Lorg/slf4j/Logger;

    const-string v1, "Action request should have at least 6 bytes."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_0
    aget-byte v0, p1, v3

    packed-switch v0, :pswitch_data_1

    .line 49
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Should not get here. Request type should be one of the treated case. It is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-byte v2, p1, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->log:Lorg/slf4j/Logger;

    const-string v1, "Received notification positive action request"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 42
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->POSITIVE_ACTION:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mRequestType:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    goto :goto_0

    .line 45
    :pswitch_4
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->log:Lorg/slf4j/Logger;

    const-string v1, "Received notification negative action request"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 46
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->NEGATIVE_ACTION:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mRequestType:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    goto :goto_0

    .line 27
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 39
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private populateDetailsRequestFields([B)V
    .locals 6
    .param p1, "rawData"    # [B

    .prologue
    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mRequestInfoNeeded:Ljava/util/List;

    .line 68
    const/4 v0, 0x5

    .line 69
    .local v0, "index":I
    const/4 v1, 0x0

    .line 71
    .local v1, "size":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 72
    aget-byte v2, p1, v0

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 74
    :pswitch_1
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mRequestInfoNeeded:Ljava/util/List;

    new-instance v3, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    const/4 v5, -0x1

    invoke-direct {v3, p0, v4, v5}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;-><init>(Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;Ljava/lang/Byte;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    add-int/lit8 v0, v0, 0x1

    .line 76
    goto :goto_0

    .line 78
    :pswitch_2
    add-int/lit8 v2, v0, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    add-int/lit8 v3, v0, 0x2

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    mul-int/lit16 v3, v3, 0x100

    add-int v1, v2, v3

    .line 79
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mRequestInfoNeeded:Ljava/util/List;

    new-instance v3, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-direct {v3, p0, v4, v1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;-><init>(Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;Ljava/lang/Byte;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    add-int/lit8 v0, v0, 0x3

    .line 81
    goto :goto_0

    .line 83
    :pswitch_3
    add-int/lit8 v2, v0, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    add-int/lit8 v3, v0, 0x2

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    mul-int/lit16 v3, v3, 0x100

    add-int v1, v2, v3

    .line 84
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mRequestInfoNeeded:Ljava/util/List;

    new-instance v3, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-direct {v3, p0, v4, v1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;-><init>(Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;Ljava/lang/Byte;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    .line 89
    :cond_0
    return-void

    .line 72
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private populateIdField([B)V
    .locals 7
    .param p1, "rawData"    # [B

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 97
    array-length v0, p1

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 98
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->log:Lorg/slf4j/Logger;

    const-string v1, "Request has length < 5. Cannot contain ID."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 109
    :goto_0
    return-void

    .line 102
    :cond_0
    new-array v0, v6, [B

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mId:[B

    .line 104
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mId:[B

    const/4 v1, 0x0

    aget-byte v2, p1, v3

    aput-byte v2, v0, v1

    .line 105
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mId:[B

    aget-byte v1, p1, v4

    aput-byte v1, v0, v3

    .line 106
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mId:[B

    aget-byte v1, p1, v5

    aput-byte v1, v0, v4

    .line 107
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mId:[B

    aget-byte v1, p1, v6

    aput-byte v1, v0, v5

    goto :goto_0
.end method


# virtual methods
.method public getId()[B
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mId:[B

    return-object v0
.end method

.method public getIntId()I
    .locals 3

    .prologue
    .line 135
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mId:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 136
    .local v0, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    return v1
.end method

.method public getMessageType()Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION_REQ:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    return-object v0
.end method

.method public getRequestInfoNeeded()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mRequestInfoNeeded:Ljava/util/List;

    return-object v0
.end method

.method public getRequestType()Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mRequestType:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    return-object v0
.end method

.method public getStringId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->mId:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public serialize()[B
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return-object v0
.end method
