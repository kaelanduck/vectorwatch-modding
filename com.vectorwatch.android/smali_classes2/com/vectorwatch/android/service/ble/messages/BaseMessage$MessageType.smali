.class public final enum Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;
.super Ljava/lang/Enum;
.source "BaseMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

.field public static final enum ACTION_ANSWER_CALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

.field public static final enum ACTION_REJECT_CALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

.field public static final enum DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

.field public static final enum NOTIFICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

.field public static final enum NOTIFICATION_DETAILS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

.field public static final enum NOTIFICATION_REQ:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

.field public static final enum PUSH_NOTIFICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 130
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    const-string v1, "DATA"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    const-string v1, "NOTIFICATION"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    const-string v1, "NOTIFICATION_REQ"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION_REQ:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    const-string v1, "NOTIFICATION_DETAILS"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION_DETAILS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    const-string v1, "PUSH_NOTIFICATION"

    invoke-direct {v0, v1, v7}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->PUSH_NOTIFICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    const-string v1, "ACTION_REJECT_CALL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->ACTION_REJECT_CALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    .line 131
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    const-string v1, "ACTION_ANSWER_CALL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->ACTION_ANSWER_CALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    .line 129
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION_REQ:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION_DETAILS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->PUSH_NOTIFICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->ACTION_REJECT_CALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->ACTION_ANSWER_CALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->$VALUES:[Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 129
    const-class v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->$VALUES:[Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    return-object v0
.end method
