.class public Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;
.super Lcom/vectorwatch/android/service/ble/messages/BtMessage;
.source "NotificationDetailsMessage.java"


# static fields
.field public static final APP_IDENTIFIER:B = 0x0t

.field public static final ID_MESSAGE:B = 0x3t

.field public static final ID_TITLE:B = 0x1t

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private log:Lorg/slf4j/Logger;

.field private notification:Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

.field private notificationId:[B

.field private requests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;",
            ">;"
        }
    .end annotation
.end field

.field private uuid:Ljava/util/UUID;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V
    .locals 1
    .param p2, "notification"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;",
            ">;",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;>;"
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;-><init>()V

    .line 19
    const-class v0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;

    .line 20
    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->log:Lorg/slf4j/Logger;

    .line 39
    invoke-virtual {p2}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getBtByteId()[B

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->notificationId:[B

    .line 40
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->requests:Ljava/util/List;

    .line 41
    iput-object p2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->notification:Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .line 43
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->uuid:Ljava/util/UUID;

    .line 44
    return-void
.end method

.method private composeResponseMessage([BBLjava/lang/String;I)[B
    .locals 9
    .param p1, "notificationIdBytes"    # [B
    .param p2, "type"    # B
    .param p3, "dataToTransfer"    # Ljava/lang/String;
    .param p4, "size"    # I

    .prologue
    const/4 v8, 0x0

    .line 97
    if-gtz p4, :cond_0

    .line 98
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Notification details request has asked for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " data bytes."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 99
    const/4 v5, 0x1

    new-array v4, v5, [B

    .line 133
    :goto_0
    return-object v4

    .line 102
    :cond_0
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Notification details request sent from watch with needed number of bytes = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 108
    .local v1, "dataToTransferBytes":[B
    array-length v5, v1

    if-le v5, p4, :cond_1

    .line 109
    new-array v0, p4, [B

    .line 111
    .local v0, "bytesToSend":[B
    invoke-static {v1, v8, v0, v8, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 119
    :goto_1
    array-length v5, p1

    array-length v6, v0

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x3

    new-array v4, v5, [B

    .line 120
    .local v4, "newMessage":[B
    array-length v5, p1

    invoke-static {p1, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    array-length v2, p1

    .line 123
    .local v2, "index":I
    aput-byte p2, v4, v2

    .line 126
    add-int/lit8 v2, v2, 0x1

    .line 127
    array-length v5, v0

    invoke-static {v5}, Lcom/vectorwatch/android/utils/Helpers;->returnIntTwoByte(I)[B

    move-result-object v3

    .line 128
    .local v3, "length":[B
    array-length v5, v3

    invoke-static {v3, v8, v4, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    array-length v5, v3

    add-int/2addr v5, v2

    array-length v6, v0

    invoke-static {v0, v8, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 113
    .end local v0    # "bytesToSend":[B
    .end local v2    # "index":I
    .end local v3    # "length":[B
    .end local v4    # "newMessage":[B
    :cond_1
    array-length v5, v1

    new-array v0, v5, [B

    .line 114
    .restart local v0    # "bytesToSend":[B
    array-length v5, v1

    invoke-static {v1, v8, v0, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1
.end method


# virtual methods
.method public getMessageType()Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION_DETAILS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    return-object v0
.end method

.method public getUuid()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->uuid:Ljava/util/UUID;

    return-object v0
.end method

.method public serialize()[B
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 53
    const/4 v5, 0x5

    new-array v2, v5, [B

    .line 56
    .local v2, "rawData":[B
    aput-byte v6, v2, v6

    .line 57
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->notificationId:[B

    aget-byte v5, v5, v6

    aput-byte v5, v2, v7

    .line 58
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->notificationId:[B

    aget-byte v5, v5, v7

    aput-byte v5, v2, v8

    .line 59
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->notificationId:[B

    aget-byte v5, v5, v8

    aput-byte v5, v2, v9

    .line 60
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->notificationId:[B

    aget-byte v6, v6, v9

    aput-byte v6, v2, v5

    .line 62
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->requests:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;

    .line 63
    .local v3, "request":Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;
    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->getFieldType()Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 65
    :pswitch_1
    iget-object v6, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->notification:Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    invoke-virtual {v6}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "appIdentifier":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->getFieldType()Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    const/16 v7, 0x1e

    .line 66
    invoke-direct {p0, v2, v6, v0, v7}, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->composeResponseMessage([BBLjava/lang/String;I)[B

    move-result-object v2

    .line 68
    goto :goto_0

    .line 71
    .end local v0    # "appIdentifier":Ljava/lang/String;
    :pswitch_2
    iget-object v6, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->notification:Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    invoke-virtual {v6}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 73
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->getFieldType()Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->getSize()I

    move-result v7

    .line 72
    invoke-direct {p0, v2, v6, v4, v7}, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->composeResponseMessage([BBLjava/lang/String;I)[B

    move-result-object v2

    .line 74
    goto :goto_0

    .line 77
    .end local v4    # "title":Ljava/lang/String;
    :pswitch_3
    iget-object v6, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->notification:Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    invoke-virtual {v6}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->getFieldType()Ljava/lang/Byte;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Byte;->byteValue()B

    move-result v6

    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->getSize()I

    move-result v7

    .line 78
    invoke-direct {p0, v2, v6, v1, v7}, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;->composeResponseMessage([BBLjava/lang/String;I)[B

    move-result-object v2

    goto :goto_0

    .line 83
    .end local v1    # "message":Ljava/lang/String;
    .end local v3    # "request":Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;
    :cond_0
    return-object v2

    .line 63
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
