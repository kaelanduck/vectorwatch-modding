.class public final enum Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;
.super Ljava/lang/Enum;
.source "BaseMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BleMessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_ACTIVITY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_ACTIVITY_TOTALS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_ALARM:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_ALERT:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_APP_INSTALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_BATTERY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_BLE_SPEED:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_BLE_TRULY_CONNECTED:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_BTN_PRESS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_CALENDAR_EVENTS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_CHANGE_COMPLICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_COMMAND:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_FRESH_START:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_GOAL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_PUSH:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_REQUEST_DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_SEND_LOGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_SERIAL_NUMBER:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_SYSTEM_INFO:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_SYSTEM_UPDATE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_TIME:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_UUID:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_VFTP:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_WATCHFACE_DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum MESSAGE_CONTROL_TYPE_WATCHFACE_ORDER:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

.field public static final enum UNKNOWN:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;


# instance fields
.field private value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_COMMAND"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_COMMAND:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 15
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_TIME"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_TIME:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 16
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_BATTERY"

    invoke-direct {v0, v1, v6, v6}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BATTERY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 17
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_ACTIVITY"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v7}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ACTIVITY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 18
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_ACTIVITY_TOTALS"

    invoke-direct {v0, v1, v7, v8}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ACTIVITY_TOTALS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 19
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_BTN_PRESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BTN_PRESS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 20
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_SYSTEM_UPDATE"

    const/4 v2, 0x6

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SYSTEM_UPDATE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 21
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_SYSTEM_INFO"

    const/4 v2, 0x7

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SYSTEM_INFO:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 22
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_ALARM"

    const/16 v2, 0x8

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ALARM:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 23
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_BLE_SPEED"

    const/16 v2, 0x9

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BLE_SPEED:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 24
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_BLE_TRULY_CONNECTED"

    const/16 v2, 0xa

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BLE_TRULY_CONNECTED:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 25
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_GOAL"

    const/16 v2, 0xb

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_GOAL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 26
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_APP_INSTALL"

    const/16 v2, 0xc

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_APP_INSTALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 27
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_WATCHFACE_ORDER"

    const/16 v2, 0xd

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_WATCHFACE_ORDER:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 28
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_PUSH"

    const/16 v2, 0xe

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_PUSH:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 29
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_FRESH_START"

    const/16 v2, 0xf

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_FRESH_START:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 30
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_SETTINGS"

    const/16 v2, 0x10

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 31
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_CALENDAR_EVENTS"

    const/16 v2, 0x11

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_CALENDAR_EVENTS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 32
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_UUID"

    const/16 v2, 0x12

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_UUID:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 33
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_CHANGE_COMPLICATION"

    const/16 v2, 0x13

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_CHANGE_COMPLICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 34
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_SEND_LOGS"

    const/16 v2, 0x14

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SEND_LOGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 35
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_SERIAL_NUMBER"

    const/16 v2, 0x15

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SERIAL_NUMBER:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 36
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_REQUEST_DATA"

    const/16 v2, 0x16

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_REQUEST_DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 37
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_VFTP"

    const/16 v2, 0x17

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_VFTP:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 38
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_ALERT"

    const/16 v2, 0x18

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ALERT:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 39
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "MESSAGE_CONTROL_TYPE_WATCHFACE_DATA"

    const/16 v2, 0x19

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_WATCHFACE_DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 41
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x1a

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->UNKNOWN:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 13
    const/16 v0, 0x1b

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_COMMAND:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_TIME:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BATTERY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ACTIVITY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ACTIVITY_TOTALS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BTN_PRESS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SYSTEM_UPDATE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SYSTEM_INFO:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ALARM:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BLE_SPEED:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BLE_TRULY_CONNECTED:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_GOAL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_APP_INSTALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_WATCHFACE_ORDER:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_PUSH:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_FRESH_START:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_CALENDAR_EVENTS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_UUID:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_CHANGE_COMPLICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SEND_LOGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SERIAL_NUMBER:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_REQUEST_DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_VFTP:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ALERT:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_WATCHFACE_DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->UNKNOWN:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->$VALUES:[Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .param p3, "val"    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput-short p3, p0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->value:S

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->$VALUES:[Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 50
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->value:S

    return v0
.end method
