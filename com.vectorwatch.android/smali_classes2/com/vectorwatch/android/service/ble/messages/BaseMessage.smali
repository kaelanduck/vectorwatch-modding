.class public Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
.super Ljava/lang/Object;
.source "BaseMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;,
        Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private type:S

.field private version:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(SS)V
    .locals 0
    .param p1, "type"    # S
    .param p2, "version"    # S

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->type:S

    .line 119
    iput-short p2, p0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->version:S

    .line 120
    return-void
.end method

.method public constructor <init>([B)V
    .locals 3
    .param p1, "rawData"    # [B

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 125
    .local v0, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    iput-short v1, p0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->type:S

    .line 126
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    iput-short v1, p0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->version:S

    .line 127
    return-void
.end method

.method public static getBleMessageTypeFromValue(S)Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;
    .locals 3
    .param p0, "value"    # S

    .prologue
    .line 55
    packed-switch p0, :pswitch_data_0

    .line 109
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trying to identify BLE Message type -> UNKNOWN code = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->UNKNOWN:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    :goto_0
    return-object v0

    .line 57
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_COMMAND:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 59
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_TIME:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 61
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BATTERY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 63
    :pswitch_4
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ACTIVITY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 65
    :pswitch_5
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ACTIVITY_TOTALS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 67
    :pswitch_6
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BTN_PRESS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 69
    :pswitch_7
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SYSTEM_UPDATE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 71
    :pswitch_8
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SYSTEM_INFO:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 73
    :pswitch_9
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ALARM:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 75
    :pswitch_a
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BLE_SPEED:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 77
    :pswitch_b
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_BLE_TRULY_CONNECTED:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 79
    :pswitch_c
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_GOAL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 81
    :pswitch_d
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_APP_INSTALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 83
    :pswitch_e
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_WATCHFACE_ORDER:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 85
    :pswitch_f
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_PUSH:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 87
    :pswitch_10
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_FRESH_START:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 89
    :pswitch_11
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 91
    :pswitch_12
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_CALENDAR_EVENTS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 93
    :pswitch_13
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_UUID:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 95
    :pswitch_14
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_CHANGE_COMPLICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 97
    :pswitch_15
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SEND_LOGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 99
    :pswitch_16
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SERIAL_NUMBER:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 101
    :pswitch_17
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_REQUEST_DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 103
    :pswitch_18
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_VFTP:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 105
    :pswitch_19
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ALERT:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 107
    :pswitch_1a
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_WATCHFACE_DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method


# virtual methods
.method public getType()S
    .locals 1

    .prologue
    .line 135
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->type:S

    return v0
.end method

.method public getVersion()S
    .locals 1

    .prologue
    .line 139
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->version:S

    return v0
.end method
