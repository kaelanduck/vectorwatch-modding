.class public Lcom/vectorwatch/android/service/ble/messages/DataMessage;
.super Lcom/vectorwatch/android/service/ble/messages/BtMessage;
.source "DataMessage.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public final data:[B

.field private uuid:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/DataMessage$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V
    .locals 1
    .param p1, "base"    # Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
    .param p2, "rawData"    # [B

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 40
    iput-object p2, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    .line 42
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->uuid:Ljava/util/UUID;

    .line 43
    return-void
.end method

.method public constructor <init>(SS[B)V
    .locals 1
    .param p1, "type"    # S
    .param p2, "version"    # S
    .param p3, "rawData"    # [B

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;-><init>()V

    .line 26
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-direct {v0, p1, p2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 27
    iput-object p3, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    .line 29
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->uuid:Ljava/util/UUID;

    .line 30
    return-void
.end method

.method public constructor <init>([B)V
    .locals 5
    .param p1, "rawData"    # [B

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;-><init>()V

    .line 51
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 52
    .local v0, "data":Ljava/nio/ByteBuffer;
    array-length v1, p1

    add-int/lit8 v1, v1, -0x4

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    .line 54
    new-instance v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 55
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    const/4 v3, 0x0

    array-length v4, p1

    add-int/lit8 v4, v4, -0x4

    invoke-static {p1, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->uuid:Ljava/util/UUID;

    .line 58
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method public getBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    return-object v0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    return-object v0
.end method

.method public getMessageType()Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    return-object v0
.end method

.method public getUuid()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->uuid:Ljava/util/UUID;

    return-object v0
.end method

.method public serialize()[B
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 67
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x4

    new-array v0, v1, [B

    .line 69
    .local v0, "rawData":[B
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getType()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-static {v1, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-static {v1, v4, v0, v3, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    array-length v3, v3

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getType()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    invoke-static {v1}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getType()S

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    array-length v0, v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->data:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 106
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
