.class final Lcom/vectorwatch/android/service/ble/messages/DataMessage$1;
.super Ljava/lang/Object;
.source "DataMessage.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/messages/DataMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .locals 7
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 111
    .local v2, "type":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 112
    .local v3, "version":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 113
    .local v0, "arraySize":I
    new-array v1, v0, [B

    .line 114
    .local v1, "messageContent":[B
    new-instance v4, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    int-to-short v5, v2

    int-to-short v6, v3

    invoke-direct {v4, v5, v6, v1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(SS[B)V

    return-object v4
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage$1;->createFromParcel(Landroid/os/Parcel;)Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 119
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage$1;->newArray(I)[Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    move-result-object v0

    return-object v0
.end method
