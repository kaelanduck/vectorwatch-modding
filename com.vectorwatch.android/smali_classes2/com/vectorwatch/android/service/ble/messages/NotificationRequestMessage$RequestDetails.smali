.class public Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;
.super Ljava/lang/Object;
.source "NotificationRequestMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RequestDetails"
.end annotation


# instance fields
.field private fieldType:Ljava/lang/Byte;

.field private size:I

.field final synthetic this$0:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;Ljava/lang/Byte;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;
    .param p2, "fieldType"    # Ljava/lang/Byte;
    .param p3, "size"    # I

    .prologue
    .line 156
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->this$0:Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput-object p2, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->fieldType:Ljava/lang/Byte;

    .line 159
    iput p3, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->size:I

    .line 160
    return-void
.end method


# virtual methods
.method public getFieldType()Ljava/lang/Byte;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->fieldType:Ljava/lang/Byte;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->size:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "field type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->fieldType:Ljava/lang/Byte;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$RequestDetails;->size:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
