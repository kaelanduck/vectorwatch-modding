.class public final enum Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;
.super Ljava/lang/Enum;
.source "NotificationsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/NotificationsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SpecialDismissForApps"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

.field public static final enum NOT_FOUND:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

.field public static final enum WHATSAPP:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;


# instance fields
.field private final appPackage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 50
    new-instance v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    const-string v1, "WHATSAPP"

    const-string v2, "com.whatsapp"

    invoke-direct {v0, v1, v3, v2}, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->WHATSAPP:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    .line 51
    new-instance v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    const-string v1, "NOT_FOUND"

    const-string v2, "NOT_FOUND"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->NOT_FOUND:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    .line 49
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    sget-object v1, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->WHATSAPP:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->NOT_FOUND:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->$VALUES:[Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "appPackage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput-object p3, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->appPackage:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public static containedInListOfValues(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;
    .locals 5
    .param p0, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 66
    if-nez p0, :cond_1

    .line 67
    sget-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->NOT_FOUND:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    .line 76
    :cond_0
    :goto_0
    return-object v0

    .line 70
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->values()[Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 71
    .local v0, "app":Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;
    iget-object v4, v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->appPackage:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 76
    .end local v0    # "app":Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->NOT_FOUND:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->$VALUES:[Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    return-object v0
.end method
