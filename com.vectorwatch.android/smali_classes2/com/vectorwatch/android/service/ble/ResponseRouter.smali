.class public Lcom/vectorwatch/android/service/ble/ResponseRouter;
.super Ljava/lang/Object;
.source "ResponseRouter.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

.field private notificationManager:Lcom/vectorwatch/android/service/ble/NotificationsManager;

.field private pool:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;Landroid/content/Context;)V
    .locals 1
    .param p1, "workerHandler"    # Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    .line 63
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    .line 65
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->pool:Ljava/util/concurrent/ExecutorService;

    .line 67
    invoke-static {}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->getInstance()Lcom/vectorwatch/android/service/ble/NotificationsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->notificationManager:Lcom/vectorwatch/android/service/ble/NotificationsManager;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/service/ble/ResponseRouter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/ResponseRouter;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private dispatchMessageToHandlerIfAlive(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V
    .locals 4
    .param p1, "type"    # Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    .param p2, "extra"    # Ljava/lang/Object;

    .prologue
    .line 300
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    monitor-enter v2

    .line 301
    :try_start_0
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isHandlerThreadTerminating()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 302
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-static {p1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I

    move-result v3

    invoke-static {v1, v3, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 303
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 305
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    monitor-exit v2

    .line 306
    return-void

    .line 305
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private handleWatchUuidInfoReceived(Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;

    .prologue
    .line 275
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;->getUuidInfo()[B

    move-result-object v2

    if-eqz v2, :cond_0

    .line 276
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;->getUuidInfo()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 277
    .local v0, "uuidBase64":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;->getVersion()S

    move-result v1

    .line 279
    .local v1, "version":S
    if-nez v1, :cond_1

    .line 280
    sget-object v2, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID: Old uuid received"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 281
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchOldCpuId(Ljava/lang/String;Landroid/content/Context;)V

    .line 288
    .end local v0    # "uuidBase64":Ljava/lang/String;
    .end local v1    # "version":S
    :cond_0
    :goto_0
    const-string v2, "bonded_watch_dirty"

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 290
    return-void

    .line 283
    .restart local v0    # "uuidBase64":Ljava/lang/String;
    .restart local v1    # "version":S
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID: New uuid received"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 284
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchNewCpuId(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public processDataFromRemoteDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;)V
    .locals 20
    .param p1, "msg"    # Lcom/vectorwatch/android/service/ble/messages/BtMessage;

    .prologue
    .line 78
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;->getMessageType()Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    move-result-object v15

    .line 80
    .local v15, "type":Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter$2;->$SwitchMap$com$vectorwatch$android$service$ble$messages$BaseMessage$MessageType:[I

    invoke-virtual {v15}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->ordinal()I

    move-result v17

    aget v16, v16, v17

    packed-switch v16, :pswitch_data_0

    .line 269
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "Logic - sendResponseData - should not get here"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 272
    .end local p1    # "msg":Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .restart local p1    # "msg":Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    :pswitch_1
    move-object/from16 v7, p1

    .line 82
    check-cast v7, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .line 84
    .local v7, "dataMsg":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    if-nez v7, :cond_1

    .line 85
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "dataMsg is null."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_1
    iget-object v0, v7, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-object/from16 v16, v0

    if-nez v16, :cond_2

    .line 90
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "dataMsg.base is null."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter$2;->$SwitchMap$com$vectorwatch$android$service$ble$messages$BaseMessage$BleMessageType:[I

    iget-object v0, v7, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getType()S

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getBleMessageTypeFromValue(S)Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->ordinal()I

    move-result v17

    aget v16, v16, v17

    packed-switch v16, :pswitch_data_1

    .line 219
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "BLE CALLS/RESP: Response router - should not get here - default for data "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v7, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :pswitch_2
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v16

    new-instance v17, Lcom/vectorwatch/android/events/ReceivedAppOrderEvent;

    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lcom/vectorwatch/android/events/ReceivedAppOrderEvent;-><init>([B)V

    invoke-virtual/range {v16 .. v17}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 99
    :pswitch_3
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Changes in alarms from watch."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v7, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AlarmsSync;->syncAlarms(Lcom/vectorwatch/android/service/ble/messages/DataMessage;Landroid/content/Context;)V

    goto :goto_0

    .line 103
    :pswitch_4
    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v16

    sget-object v17, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual/range {v16 .. v17}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 104
    .local v4, "commandBytes":Ljava/nio/ByteBuffer;
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v5

    .line 106
    .local v5, "commandType":S
    packed-switch v5, :pswitch_data_2

    :pswitch_5
    goto/16 :goto_0

    .line 108
    :pswitch_6
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Crash announcement from watch."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;->receivedCrashAnnouncement(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 113
    :pswitch_7
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALL: Find my phone"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->findPhone(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 117
    :pswitch_8
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALL: Activity announcement."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->requestActivityCausedByWatchAnnouncement(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 122
    :pswitch_9
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getSocialMediaManager()Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->shareActivity()V

    goto/16 :goto_0

    .line 127
    .end local v4    # "commandBytes":Ljava/nio/ByteBuffer;
    .end local v5    # "commandType":S
    :pswitch_a
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: crash logs received."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v7}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;->receivedCrashLogs(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto/16 :goto_0

    .line 132
    :pswitch_b
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received battery sync"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v7, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/BatterySync;->syncBatteryFromWatch(Lcom/vectorwatch/android/service/ble/messages/DataMessage;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 136
    :pswitch_c
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received activity sync"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->pool:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v16

    if-nez v16, :cond_0

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->pool:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v16, v0

    new-instance v17, Lcom/vectorwatch/android/service/ble/ResponseRouter$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7}, Lcom/vectorwatch/android/service/ble/ResponseRouter$1;-><init>(Lcom/vectorwatch/android/service/ble/ResponseRouter;Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    invoke-interface/range {v16 .. v17}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 147
    :pswitch_d
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received activity totals sync"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v7, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->syncActivityTotals(Lcom/vectorwatch/android/service/ble/messages/DataMessage;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 151
    :pswitch_e
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received button press sync"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v7}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->handleButtonPressed(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto/16 :goto_0

    .line 155
    :pswitch_f
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received system update sync"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 156
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v16

    new-instance v17, Lcom/vectorwatch/android/events/UpdateProgress;

    new-instance v18, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;

    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;-><init>([B)V

    invoke-direct/range {v17 .. v18}, Lcom/vectorwatch/android/events/UpdateProgress;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateStatus;)V

    invoke-virtual/range {v16 .. v17}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 159
    :pswitch_10
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "BLE CALLS/RESP: Received system info."

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 160
    new-instance v10, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v16

    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v17

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v10, v0, v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;-><init>([BS)V

    .line 161
    .local v10, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v10, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/WatchInfoSync;->syncWatchSystemInfo(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 164
    .end local v10    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :pswitch_11
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received uuid info."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 166
    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v2

    .line 167
    .local v2, "base":Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
    new-instance v8, Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;

    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v16

    .line 168
    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v17

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v8, v0, v1}, Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;-><init>([BS)V

    .line 169
    .local v8, "event":Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 171
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/vectorwatch/android/service/ble/ResponseRouter;->handleWatchUuidInfoReceived(Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;)V

    goto/16 :goto_0

    .line 174
    .end local v2    # "base":Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
    .end local v8    # "event":Lcom/vectorwatch/android/events/WatchUuidInfoReceivedEvent;
    :pswitch_12
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: serial number received."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v7, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/WatchInfoSync;->syncSerialNumber(Lcom/vectorwatch/android/service/ble/messages/DataMessage;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 178
    :pswitch_13
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received FRESH_START - needs to do sync."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    :pswitch_14
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received ble speed sync"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 183
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v16

    new-instance v17, Lcom/vectorwatch/android/events/BleSpeedReceivedEvent;

    new-instance v18, Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;

    .line 184
    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;-><init>([B)V

    invoke-direct/range {v17 .. v18}, Lcom/vectorwatch/android/events/BleSpeedReceivedEvent;-><init>(Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;)V

    .line 183
    invoke-virtual/range {v16 .. v17}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 187
    :pswitch_15
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received ble truly connected"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 188
    new-instance v3, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;

    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v3, v0}, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;-><init>([B)V

    .line 189
    .local v3, "ble":Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;
    const-string v16, "counter_watch_connection_number_new"

    .line 190
    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->getConnectionNumber()S

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    .line 189
    invoke-static/range {v16 .. v18}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 192
    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->getConnectionNumber()S

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v16 .. v17}, Lcom/vectorwatch/android/utils/Helpers;->getConnectionDetails(ILandroid/content/Context;)I

    move-result v6

    .line 194
    .local v6, "connectionDetails":I
    const-string v16, "check_compatibility"

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v16 .. v18}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v16

    if-nez v16, :cond_0

    if-nez v6, :cond_0

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v6, v0}, Lcom/vectorwatch/android/utils/Helpers;->startConnectionEstablishedDrivenSyncs(ILandroid/content/Context;)V

    goto/16 :goto_0

    .line 201
    .end local v3    # "ble":Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;
    .end local v6    # "connectionDetails":I
    :pswitch_16
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Requesting data for app"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v7}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->handleDataRequest(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto/16 :goto_0

    .line 205
    :pswitch_17
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: Received VFTP message"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v0, v7}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->receivedVftpMessage(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto/16 :goto_0

    .line 209
    :pswitch_18
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE CALLS/RESP: App install message from watch."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 210
    invoke-static {v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->receivedInstallMessageFromWatch(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto/16 :goto_0

    .line 213
    :pswitch_19
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "LIVE_STREAM: Received watchface data request"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 214
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInSetupMode()Z

    move-result v16

    if-nez v16, :cond_0

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v7}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->handleWatchfaceRequest(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V

    goto/16 :goto_0

    .end local v7    # "dataMsg":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    :pswitch_1a
    move-object/from16 v16, p1

    .line 223
    check-cast v16, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->getRequestType()Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;

    move-result-object v14

    .local v14, "reqType":Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;
    move-object/from16 v16, p1

    .line 224
    check-cast v16, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->getId()[B

    move-result-object v9

    .line 226
    .local v9, "id":[B
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Dismiss call "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 228
    invoke-static {v9}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->isCall([B)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 229
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter$2;->$SwitchMap$com$vectorwatch$android$service$ble$messages$NotificationRequestMessage$NotificationRequestType:[I

    invoke-virtual {v14}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->ordinal()I

    move-result v17

    aget v16, v16, v17

    packed-switch v16, :pswitch_data_3

    .line 237
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "Should not get here. Only positive and negative actions are treated."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :pswitch_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/vectorwatch/android/service/nls/PhoneCallHandler;->rejectCall(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 240
    :cond_3
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter$2;->$SwitchMap$com$vectorwatch$android$service$ble$messages$NotificationRequestMessage$NotificationRequestType:[I

    invoke-virtual {v14}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage$NotificationRequestType;->ordinal()I

    move-result v17

    aget v16, v16, v17

    packed-switch v16, :pswitch_data_4

    .line 264
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    const-string v17, "Should not get here. Only positive and negative actions are treated."

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 246
    :pswitch_1c
    sget-object v16, Lcom/vectorwatch/android/service/nls/NLService;->reference:Lcom/vectorwatch/android/service/nls/NLService;

    if-eqz v16, :cond_4

    .line 247
    check-cast p1, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;

    .end local p1    # "msg":Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->getIntId()I

    move-result v12

    .line 248
    .local v12, "notifId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->notificationManager:Lcom/vectorwatch/android/service/ble/NotificationsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->getNotification(I)Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    move-result-object v13

    .line 251
    .local v13, "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    if-eqz v13, :cond_0

    invoke-virtual {v13}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_0

    .line 253
    sget-object v16, Lcom/vectorwatch/android/service/ble/ResponseRouter;->log:Lorg/slf4j/Logger;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Notifications - button remove "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v13}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->notificationManager:Lcom/vectorwatch/android/service/ble/NotificationsManager;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->removeSingleNotificationsFromQueueReceivedFromWatch(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    goto/16 :goto_0

    .line 258
    .end local v12    # "notifId":I
    .end local v13    # "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    .restart local p1    # "msg":Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    :cond_4
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    const-class v17, Lcom/vectorwatch/android/service/nls/NLService;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 259
    .local v11, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1a
    .end packed-switch

    .line 94
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 106
    :pswitch_data_2
    .packed-switch 0x13
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_8
    .end packed-switch

    .line 229
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1b
    .end packed-switch

    .line 240
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1c
    .end packed-switch
.end method

.method public shutDownExecutorService()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/ResponseRouter;->pool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 72
    return-void
.end method
