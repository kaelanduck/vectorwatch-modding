.class public interface abstract Lcom/vectorwatch/android/service/ble/Transformer;
.super Ljava/lang/Object;
.source "Transformer.java"


# virtual methods
.method public abstract packMessageForDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;Landroid/bluetooth/BluetoothGattCharacteristic;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;"
        }
    .end annotation
.end method

.method public abstract unpackMessageFromDevice([BLjava/util/UUID;)V
.end method
