.class public final enum Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;
.super Ljava/lang/Enum;
.source "BleGattPacket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/BleGattPacket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

.field public static final enum READ_CHARACTERISTIC:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

.field public static final enum WRITE_CHARACTERISTIC:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

.field public static final enum WRITE_DESCRIPTOR:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    const-string v1, "WRITE_DESCRIPTOR"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->WRITE_DESCRIPTOR:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    new-instance v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    const-string v1, "READ_CHARACTERISTIC"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->READ_CHARACTERISTIC:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    new-instance v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    const-string v1, "WRITE_CHARACTERISTIC"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->WRITE_CHARACTERISTIC:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->WRITE_DESCRIPTOR:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->READ_CHARACTERISTIC:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->WRITE_CHARACTERISTIC:Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->$VALUES:[Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->$VALUES:[Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    return-object v0
.end method
