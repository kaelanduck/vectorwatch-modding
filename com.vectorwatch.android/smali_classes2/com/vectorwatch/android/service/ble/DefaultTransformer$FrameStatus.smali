.class final enum Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;
.super Ljava/lang/Enum;
.source "DefaultTransformer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/DefaultTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FrameStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

.field public static final enum FIRST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

.field public static final enum LAST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

.field public static final enum MORE_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

.field public static final enum NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;


# instance fields
.field private final value:B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 612
    new-instance v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    const-string v1, "NO_FRAGMENTS"

    invoke-direct {v0, v1, v2, v2}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    new-instance v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    const-string v1, "FIRST_FRAGMENT"

    invoke-direct {v0, v1, v3, v3}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->FIRST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    new-instance v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    const-string v1, "MORE_FRAGMENTS"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->MORE_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    new-instance v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    const-string v1, "LAST_FRAGMENT"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->LAST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    .line 611
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->FIRST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->MORE_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->LAST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->$VALUES:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .param p3, "value"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    .prologue
    .line 616
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 617
    iput-byte p3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->value:B

    .line 618
    return-void
.end method

.method public static getStatusByValue(B)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;
    .locals 1
    .param p0, "value"    # B

    .prologue
    .line 621
    packed-switch p0, :pswitch_data_0

    .line 632
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 623
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    goto :goto_0

    .line 625
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->FIRST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    goto :goto_0

    .line 627
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->MORE_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    goto :goto_0

    .line 629
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->LAST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    goto :goto_0

    .line 621
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 611
    const-class v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;
    .locals 1

    .prologue
    .line 611
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->$VALUES:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()B
    .locals 1

    .prologue
    .line 636
    iget-byte v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->value:B

    return v0
.end method
