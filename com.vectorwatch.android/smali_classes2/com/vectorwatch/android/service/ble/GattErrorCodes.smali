.class public Lcom/vectorwatch/android/service/ble/GattErrorCodes;
.super Ljava/lang/Object;
.source "GattErrorCodes.java"


# static fields
.field public static final GATT_AUTH_FAIL:I = 0x89

.field public static final GATT_BUSY:I = 0x84

.field public static final GATT_CCC_CFG_ERR:I = 0xfd

.field public static final GATT_CMD_STARTED:I = 0x86

.field public static final GATT_CONGESTED:I = 0x8f

.field public static final GATT_CONN_CANCEL:I = 0x100

.field public static final GATT_CONN_FAIL_ESTABLISH:I = 0x3e

.field public static final GATT_CONN_LMP_TIMEOUT:I = 0x22

.field public static final GATT_CONN_TERMINATE_LOCAL_HOST:I = 0x16

.field public static final GATT_CONN_TERMINATE_PEER_USER:I = 0x13

.field public static final GATT_CONN_TIMEOUT:I = 0x8

.field public static final GATT_DB_FULL:I = 0x83

.field public static final GATT_ENCRYPED_NO_MITM:I = 0x8d

.field public static final GATT_ERROR:I = 0x85

.field public static final GATT_ERR_UNLIKELY:I = 0xe

.field public static final GATT_ILLEGAL_PARAMETER:I = 0x87

.field public static final GATT_INSUF_AUTHENTICATION:I = 0x5

.field public static final GATT_INSUF_ENCRYPTION:I = 0xf

.field public static final GATT_INSUF_KEY_SIZE:I = 0xc

.field public static final GATT_INSUF_RESOURCE:I = 0x11

.field public static final GATT_INTERNAL_ERROR:I = 0x81

.field public static final GATT_INVALID_ATTR_LEN:I = 0xd

.field public static final GATT_INVALID_CFG:I = 0x8b

.field public static final GATT_INVALID_CONN_ID:I = 0xffff

.field public static final GATT_INVALID_HANDLE:I = 0x1

.field public static final GATT_INVALID_OFFSET:I = 0x7

.field public static final GATT_INVALID_PDU:I = 0x4

.field public static final GATT_MORE:I = 0x8a

.field public static final GATT_NOT_ENCRYPTED:I = 0x8e

.field public static final GATT_NOT_FOUND:I = 0xa

.field public static final GATT_NOT_LONG:I = 0xb

.field public static final GATT_NO_RESOURCES:I = 0x80

.field public static final GATT_OUT_OF_RANGE:I = 0xff

.field public static final GATT_PENDING:I = 0x88

.field public static final GATT_PRC_IN_PROGRESS:I = 0xfe

.field public static final GATT_PREPARE_Q_FULL:I = 0x9

.field public static final GATT_READ_NOT_PERMIT:I = 0x2

.field public static final GATT_REQ_NOT_SUPPORTED:I = 0x6

.field public static final GATT_SERVICE_STARTED:I = 0x8c

.field public static final GATT_SUCCESS:I = 0x0

.field public static final GATT_UNSUPPORT_GRP_TYPE:I = 0x10

.field public static final GATT_WRITE_NOT_PERMIT:I = 0x3

.field public static final GATT_WRONG_STATE:I = 0x82


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
