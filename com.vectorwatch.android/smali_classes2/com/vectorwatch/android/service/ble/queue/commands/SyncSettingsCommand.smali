.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncSettingsCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private age:S

.field private autoDiscreet:Z

.field private autoSleep:Z

.field private dropMode:Z

.field private gender:S

.field private height:S

.field private hourMode:S

.field private isDirtyAutoDiscreet:Z

.field private isDirtyAutoSleep:Z

.field private isDirtyDnd:Z

.field private isDirtyDropMode:Z

.field private isDirtyGlance:Z

.field private isDirtyMorningGreet:Z

.field private isDndOn:Z

.field private isGlanceOn:Z

.field private mBufferSize:I

.field private morningGreet:Z

.field private name:Ljava/lang/String;

.field private unitSystem:S

.field private weight:S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    .line 32
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 483
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 57
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setUuid(Ljava/util/UUID;)V

    .line 58
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 10
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 402
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v6, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {p0, v3, v6, v8, v9}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 404
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    int-to-short v3, v3

    iput-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->age:S

    .line 405
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    int-to-short v3, v3

    iput-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->gender:S

    .line 406
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    int-to-short v3, v3

    iput-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->weight:S

    .line 407
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    int-to-short v3, v3

    iput-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->height:S

    .line 408
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    int-to-short v3, v3

    iput-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->unitSystem:S

    .line 409
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    int-to-short v3, v3

    iput-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->hourMode:S

    .line 410
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_1

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyMorningGreet:Z

    .line 411
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_2

    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->morningGreet:Z

    .line 412
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_3

    move v3, v4

    :goto_2
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoDiscreet:Z

    .line 413
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_4

    move v3, v4

    :goto_3
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->autoDiscreet:Z

    .line 414
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_5

    move v3, v4

    :goto_4
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoSleep:Z

    .line 415
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_6

    move v3, v4

    :goto_5
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->autoSleep:Z

    .line 416
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_7

    move v3, v4

    :goto_6
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDropMode:Z

    .line 417
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_8

    move v3, v4

    :goto_7
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->dropMode:Z

    .line 418
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_9

    move v3, v4

    :goto_8
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDnd:Z

    .line 419
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_a

    move v3, v4

    :goto_9
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDndOn:Z

    .line 420
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_b

    move v3, v4

    :goto_a
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyGlance:Z

    .line 421
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    if-ne v3, v4, :cond_0

    move v5, v4

    :cond_0
    iput-boolean v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isGlanceOn:Z

    .line 423
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 425
    .local v0, "flag":I
    if-lez v0, :cond_c

    .line 426
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    .line 432
    :goto_b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 433
    .local v1, "hasUuid":I
    if-ne v1, v4, :cond_d

    .line 434
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 435
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->setUuid(Ljava/util/UUID;)V

    .line 439
    .end local v2    # "uuid":Ljava/lang/String;
    :goto_c
    return-void

    .end local v0    # "flag":I
    .end local v1    # "hasUuid":I
    :cond_1
    move v3, v5

    .line 410
    goto/16 :goto_0

    :cond_2
    move v3, v5

    .line 411
    goto :goto_1

    :cond_3
    move v3, v5

    .line 412
    goto :goto_2

    :cond_4
    move v3, v5

    .line 413
    goto :goto_3

    :cond_5
    move v3, v5

    .line 414
    goto :goto_4

    :cond_6
    move v3, v5

    .line 415
    goto :goto_5

    :cond_7
    move v3, v5

    .line 416
    goto :goto_6

    :cond_8
    move v3, v5

    .line 417
    goto :goto_7

    :cond_9
    move v3, v5

    .line 418
    goto :goto_8

    :cond_a
    move v3, v5

    .line 419
    goto :goto_9

    :cond_b
    move v3, v5

    .line 420
    goto :goto_a

    .line 428
    .restart local v0    # "flag":I
    :cond_c
    const-string v3, ""

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    goto :goto_b

    .line 437
    .restart local v1    # "hasUuid":I
    :cond_d
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "UUID missing in Sync settings"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_c
.end method

.method private getChangedSettings(II)[B
    .locals 7
    .param p1, "count"    # I
    .param p2, "bufferSize"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 191
    add-int/lit8 v1, p2, 0x1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 194
    .local v0, "dataBytes":Ljava/nio/ByteBuffer;
    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 197
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 198
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Name field: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "2 + "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    .line 199
    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 198
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 200
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_NAME:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 201
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 202
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 206
    :cond_0
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->age:S

    if-eq v1, v4, :cond_1

    .line 207
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Age field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 208
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AGE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 209
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->age:S

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 213
    :cond_1
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->gender:S

    if-eq v1, v4, :cond_2

    .line 214
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Gender field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 215
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_GENDER:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 216
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->gender:S

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 220
    :cond_2
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->weight:S

    if-eq v1, v4, :cond_3

    .line 221
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Weight field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 222
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_WEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 223
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->weight:S

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 227
    :cond_3
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->height:S

    if-eq v1, v4, :cond_4

    .line 228
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Height field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 229
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_HEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 230
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->height:S

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 234
    :cond_4
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->unitSystem:S

    if-eq v1, v4, :cond_5

    .line 235
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Unit sys field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 236
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_UNIT_SYSTEM:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 237
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->unitSystem:S

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 241
    :cond_5
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->hourMode:S

    if-eq v1, v4, :cond_6

    .line 242
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". T unit field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 243
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_HOUR_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 244
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->hourMode:S

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 248
    :cond_6
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyMorningGreet:Z

    if-eqz v1, :cond_7

    .line 249
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Morning greet field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 250
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->morningGreet:Z

    if-eqz v1, :cond_d

    .line 251
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_MORNING_GREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 252
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 260
    :cond_7
    :goto_0
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoDiscreet:Z

    if-eqz v1, :cond_8

    .line 261
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Auto discreet field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 262
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->autoDiscreet:Z

    if-eqz v1, :cond_e

    .line 263
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_DISCREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 264
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 271
    :cond_8
    :goto_1
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoSleep:Z

    if-eqz v1, :cond_9

    .line 272
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Auto sleep field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 273
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->autoSleep:Z

    if-eqz v1, :cond_f

    .line 274
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_SLEEP_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 275
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 283
    :cond_9
    :goto_2
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDropMode:Z

    if-eqz v1, :cond_a

    .line 284
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Drop mode field: 2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 285
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->dropMode:Z

    if-eqz v1, :cond_10

    .line 286
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_DROP_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 287
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 295
    :cond_a
    :goto_3
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDnd:Z

    if-eqz v1, :cond_b

    .line 296
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Dnd field: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDndOn:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 297
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDndOn:Z

    if-eqz v1, :cond_11

    .line 298
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_DND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 299
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 307
    :cond_b
    :goto_4
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyGlance:Z

    if-eqz v1, :cond_c

    .line 308
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changed settings to sync to watch. Allocated = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Dnd field: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isGlanceOn:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 309
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isGlanceOn:Z

    if-eqz v1, :cond_12

    .line 310
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_GLANCE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 311
    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 318
    :cond_c
    :goto_5
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    return-object v1

    .line 254
    :cond_d
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_MORNING_GREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 255
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    .line 266
    :cond_e
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_DISCREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 267
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto/16 :goto_1

    .line 277
    :cond_f
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_SLEEP_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 278
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto/16 :goto_2

    .line 289
    :cond_10
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_DROP_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 290
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto/16 :goto_3

    .line 301
    :cond_11
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_DND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 302
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto/16 :goto_4

    .line 313
    :cond_12
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_GLANCE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 314
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_5
.end method

.method private getChangedSettingsCount()B
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 93
    const/4 v0, 0x0

    .line 94
    .local v0, "count":I
    const/4 v1, 0x0

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 97
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->age:S

    if-eq v1, v4, :cond_0

    .line 98
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SYNC SETTINGS: age = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->age:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 99
    add-int/lit8 v0, v0, 0x1

    .line 100
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 104
    :cond_0
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->gender:S

    if-eq v1, v4, :cond_1

    .line 105
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SYNC SETTINGS: gender = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->gender:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 106
    add-int/lit8 v0, v0, 0x1

    .line 107
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 111
    :cond_1
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->weight:S

    if-eq v1, v4, :cond_2

    .line 112
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SYNC SETTINGS: weight = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->weight:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 113
    add-int/lit8 v0, v0, 0x1

    .line 114
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 118
    :cond_2
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->height:S

    if-eq v1, v4, :cond_3

    .line 119
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SYNC SETTINGS: height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->height:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 120
    add-int/lit8 v0, v0, 0x1

    .line 121
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 125
    :cond_3
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->unitSystem:S

    if-eq v1, v4, :cond_4

    .line 126
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SYNC SETTINGS: unit system = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->unitSystem:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 127
    add-int/lit8 v0, v0, 0x1

    .line 128
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 132
    :cond_4
    iget-short v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->hourMode:S

    if-eq v1, v4, :cond_5

    .line 133
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SYNC SETTINGS: hourMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->hourMode:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 134
    add-int/lit8 v0, v0, 0x1

    .line 135
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 139
    :cond_5
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 140
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SYNC SETTINGS: name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 141
    add-int/lit8 v0, v0, 0x1

    .line 142
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 146
    :cond_6
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyMorningGreet:Z

    if-eqz v1, :cond_7

    .line 147
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    const-string v2, "SYNC SETTINGS: morning greet"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 148
    add-int/lit8 v0, v0, 0x1

    .line 149
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 153
    :cond_7
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoDiscreet:Z

    if-eqz v1, :cond_8

    .line 154
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    const-string v2, "SYNC SETTINGS: auto discreet"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 155
    add-int/lit8 v0, v0, 0x1

    .line 156
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 160
    :cond_8
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoSleep:Z

    if-eqz v1, :cond_9

    .line 161
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    const-string v2, "SYNC SETTINGS: auto sleep"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 162
    add-int/lit8 v0, v0, 0x1

    .line 163
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 167
    :cond_9
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDropMode:Z

    if-eqz v1, :cond_a

    .line 168
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    const-string v2, "SYNC SETTINGS: drop mode"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 169
    add-int/lit8 v0, v0, 0x1

    .line 170
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 174
    :cond_a
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDnd:Z

    if-eqz v1, :cond_b

    .line 175
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    const-string v2, "SYNC SETTINGS: dnd"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 176
    add-int/lit8 v0, v0, 0x1

    .line 177
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 181
    :cond_b
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyGlance:Z

    if-eqz v1, :cond_c

    .line 182
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    const-string v2, "SYNC SETTINGS: glance"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 183
    add-int/lit8 v0, v0, 0x1

    .line 184
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 187
    :cond_c
    int-to-byte v1, v0

    return v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    const-string v5, "BLE COMMANDS: sync_settings_command"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 64
    const/4 v4, 0x0

    iput v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    .line 65
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->getChangedSettingsCount()B

    move-result v0

    .line 67
    .local v0, "count":B
    if-gtz v0, :cond_0

    .line 68
    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    const-string v5, "No changes where found in user settings."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 69
    const/4 v1, 0x0

    .line 82
    :goto_0
    return-object v1

    .line 72
    :cond_0
    iget v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->mBufferSize:I

    invoke-direct {p0, v0, v4}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->getChangedSettings(II)[B

    move-result-object v3

    .line 73
    .local v3, "settings":[B
    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BLE COMMANDS: settings = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 75
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v4

    invoke-direct {v2, v4, v3}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 76
    .local v2, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/SettingsSyncedToWatchEvent;

    invoke-direct {v5}, Lcom/vectorwatch/android/events/SettingsSyncedToWatchEvent;-><init>()V

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setAge(S)V
    .locals 0
    .param p1, "age"    # S

    .prologue
    .line 324
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->age:S

    .line 325
    return-void
.end method

.method public setAutoDiscreet(Z)V
    .locals 0
    .param p1, "autoDiscreet"    # Z

    .prologue
    .line 356
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->autoDiscreet:Z

    .line 357
    return-void
.end method

.method public setAutoSleep(Z)V
    .locals 0
    .param p1, "autoSleep"    # Z

    .prologue
    .line 360
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->autoSleep:Z

    .line 361
    return-void
.end method

.method public setDirtyAutoDiscreet(Z)V
    .locals 0
    .param p1, "dirtyAutoDiscreet"    # Z

    .prologue
    .line 380
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoDiscreet:Z

    .line 381
    return-void
.end method

.method public setDirtyAutoSleep(Z)V
    .locals 0
    .param p1, "dirtyAutoSleep"    # Z

    .prologue
    .line 384
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoSleep:Z

    .line 385
    return-void
.end method

.method public setDirtyDnd(Z)V
    .locals 0
    .param p1, "dirtyDnd"    # Z

    .prologue
    .line 392
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDnd:Z

    .line 393
    return-void
.end method

.method public setDirtyDropMode(Z)V
    .locals 0
    .param p1, "dirtyDropMode"    # Z

    .prologue
    .line 388
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDropMode:Z

    .line 389
    return-void
.end method

.method public setDirtyGlance(Z)V
    .locals 0
    .param p1, "dirtyGlance"    # Z

    .prologue
    .line 396
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyGlance:Z

    .line 397
    return-void
.end method

.method public setDirtyMorningGreet(Z)V
    .locals 0
    .param p1, "dirtyMorningGreet"    # Z

    .prologue
    .line 376
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyMorningGreet:Z

    .line 377
    return-void
.end method

.method public setDndOn(Z)V
    .locals 0
    .param p1, "dndOn"    # Z

    .prologue
    .line 368
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDndOn:Z

    .line 369
    return-void
.end method

.method public setDropMode(Z)V
    .locals 0
    .param p1, "dropMode"    # Z

    .prologue
    .line 364
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->dropMode:Z

    .line 365
    return-void
.end method

.method public setGender(S)V
    .locals 0
    .param p1, "gender"    # S

    .prologue
    .line 328
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->gender:S

    .line 329
    return-void
.end method

.method public setGlanceOn(Z)V
    .locals 0
    .param p1, "glanceOn"    # Z

    .prologue
    .line 372
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isGlanceOn:Z

    .line 373
    return-void
.end method

.method public setHeight(S)V
    .locals 0
    .param p1, "height"    # S

    .prologue
    .line 336
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->height:S

    .line 337
    return-void
.end method

.method public setHourMode(S)V
    .locals 0
    .param p1, "hourMode"    # S

    .prologue
    .line 344
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->hourMode:S

    .line 345
    return-void
.end method

.method public setMorningGreet(Z)V
    .locals 0
    .param p1, "morningGreet"    # Z

    .prologue
    .line 352
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->morningGreet:Z

    .line 353
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    .line 349
    return-void
.end method

.method public setUnitSystem(S)V
    .locals 0
    .param p1, "unitSystem"    # S

    .prologue
    .line 340
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->unitSystem:S

    .line 341
    return-void
.end method

.method public setWeight(S)V
    .locals 0
    .param p1, "weight"    # S

    .prologue
    .line 332
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->weight:S

    .line 333
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 448
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->age:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 449
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->gender:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 450
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->weight:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 451
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->height:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 452
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->unitSystem:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 453
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->hourMode:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 454
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyMorningGreet:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 455
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->morningGreet:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 456
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoDiscreet:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 457
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->autoDiscreet:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 458
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyAutoSleep:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 459
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->autoSleep:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 460
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDropMode:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 461
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->dropMode:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 462
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyDnd:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 463
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDndOn:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 464
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isDirtyGlance:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->isGlanceOn:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 466
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    if-nez v0, :cond_c

    .line 467
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 474
    :goto_c
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 475
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 476
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 480
    :goto_d
    return-void

    :cond_0
    move v0, v2

    .line 454
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 455
    goto :goto_1

    :cond_2
    move v0, v2

    .line 456
    goto :goto_2

    :cond_3
    move v0, v2

    .line 457
    goto :goto_3

    :cond_4
    move v0, v2

    .line 458
    goto :goto_4

    :cond_5
    move v0, v2

    .line 459
    goto :goto_5

    :cond_6
    move v0, v2

    .line 460
    goto :goto_6

    :cond_7
    move v0, v2

    .line 461
    goto :goto_7

    :cond_8
    move v0, v2

    .line 462
    goto :goto_8

    :cond_9
    move v0, v2

    .line 463
    goto :goto_9

    :cond_a
    move v0, v2

    .line 464
    goto :goto_a

    :cond_b
    move v0, v2

    .line 465
    goto :goto_b

    .line 469
    :cond_c
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 470
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncSettingsCommand;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_c

    .line 478
    :cond_d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_d
.end method
