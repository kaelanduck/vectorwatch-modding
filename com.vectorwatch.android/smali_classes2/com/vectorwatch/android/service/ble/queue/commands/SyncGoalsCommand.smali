.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncGoalsCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field private static final CALORIES:B = 0x1t

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final DISTANCE:B = 0x2t

.field private static final GOAL_INFO_SIZE:I = 0x5

.field private static final SLEEP:B = 0x3t

.field private static final STEPS:B

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private goals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/GoalModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->log:Lorg/slf4j/Logger;

    .line 28
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_GOAL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 157
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 48
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->setUuid(Ljava/util/UUID;)V

    .line 49
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 10
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 104
    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v6, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {p0, v5, v6, v8, v9}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 108
    .local v0, "count":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    .line 110
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 111
    new-instance v1, Lcom/vectorwatch/android/models/GoalModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/GoalModel;-><init>()V

    .line 112
    .local v1, "goal":Lcom/vectorwatch/android/models/GoalModel;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-static {v5}, Lcom/vectorwatch/android/utils/Constants;->getGoalTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/models/GoalModel;->setType(Lcom/vectorwatch/android/utils/Constants$GoalType;)V

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/vectorwatch/android/models/GoalModel;->setValue(I)V

    .line 115
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 119
    .end local v1    # "goal":Lcom/vectorwatch/android/models/GoalModel;
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 120
    .local v2, "hasUuid":I
    const/4 v5, 0x1

    if-ne v2, v5, :cond_1

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 122
    .local v4, "uuid":Ljava/lang/String;
    invoke-static {v4}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->setUuid(Ljava/util/UUID;)V

    .line 126
    .end local v4    # "uuid":Ljava/lang/String;
    :goto_1
    return-void

    .line 124
    :cond_1
    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->log:Lorg/slf4j/Logger;

    const-string v6, "UUID missing in TIME SYNC"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->log:Lorg/slf4j/Logger;

    const-string v5, "BLE COMMANDS: sync_goals"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 54
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-object v2

    .line 63
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x5

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 66
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 68
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/GoalModel;

    .line 69
    .local v1, "goal":Lcom/vectorwatch/android/models/GoalModel;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getType()Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    if-ne v5, v6, :cond_2

    .line 70
    const/4 v3, 0x0

    .line 71
    .local v3, "type":B
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 72
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getValue()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 73
    .end local v3    # "type":B
    :cond_2
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getType()Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    if-ne v5, v6, :cond_3

    .line 74
    const/4 v3, 0x1

    .line 75
    .restart local v3    # "type":B
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 76
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getValue()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 77
    .end local v3    # "type":B
    :cond_3
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getType()Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    if-ne v5, v6, :cond_4

    .line 78
    const/4 v3, 0x2

    .line 79
    .restart local v3    # "type":B
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 80
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getValue()I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 81
    .end local v3    # "type":B
    :cond_4
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getType()Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    if-ne v5, v6, :cond_5

    .line 82
    const/4 v3, 0x3

    .line 83
    .restart local v3    # "type":B
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 84
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getValue()I

    move-result v5

    mul-int/lit8 v5, v5, 0x4

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 86
    .end local v3    # "type":B
    :cond_5
    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->log:Lorg/slf4j/Logger;

    const-string v6, "Sync goal has incorrect arguments"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 90
    .end local v1    # "goal":Lcom/vectorwatch/android/models/GoalModel;
    :cond_6
    new-instance v4, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v5

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public setGoals(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/GoalModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "goalList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/GoalModel;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    .line 99
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v4, 0x0

    .line 135
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 136
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 137
    .local v0, "count":I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->goals:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/GoalModel;

    .line 140
    .local v1, "goal":Lcom/vectorwatch/android/models/GoalModel;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getType()Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$GoalType;->getVal()I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/GoalModel;->getValue()I

    move-result v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 144
    .end local v0    # "count":I
    .end local v1    # "goal":Lcom/vectorwatch/android/models/GoalModel;
    :cond_0
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 149
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 150
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncGoalsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154
    :goto_1
    return-void

    .line 152
    :cond_2
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
