.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncBacklightTimeoutCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;

.field private timeout:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->log:Lorg/slf4j/Logger;

    .line 27
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 104
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 34
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->setUuid(Ljava/util/UUID;)V

    .line 35
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 69
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->timeout:I

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Constants;->getSettingsTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$SettingsType;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 76
    .local v0, "hasUuid":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->setUuid(Ljava/util/UUID;)V

    .line 82
    .end local v1    # "uuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 80
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID missing in TIME SYNC"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE COMMANDS: sync_backlight_timeout"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 40
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BACKLIGHT - TIMEOUT - send timeout with value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->timeout:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 42
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    const/4 v2, 0x3

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 47
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 48
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v2

    int-to-byte v2, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 49
    iget v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->timeout:I

    int-to-byte v2, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 51
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    return-object v1
.end method

.method public setSettingsType(Lcom/vectorwatch/android/utils/Constants$SettingsType;)V
    .locals 0
    .param p1, "settingsType"    # Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 64
    return-void
.end method

.method public setTimeout(I)V
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->timeout:I

    .line 60
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->timeout:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 97
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncBacklightTimeoutCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
