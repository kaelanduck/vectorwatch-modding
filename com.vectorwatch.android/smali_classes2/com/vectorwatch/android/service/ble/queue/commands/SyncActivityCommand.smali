.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncActivityCommand.java"


# static fields
.field private static final BASE_0:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field private static final BASE_1:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private endTime:I

.field private isVosOver077:Z

.field private startTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->log:Lorg/slf4j/Logger;

    .line 27
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ACTIVITY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 28
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->BASE_1:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 29
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_ACTIVITY:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 30
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->BASE_0:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 120
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x1

    .line 70
    const/4 v3, 0x0

    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->LOW:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v3, v5, v6, v7}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->startTime:I

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->endTime:I

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    .line 77
    .local v0, "flag":B
    if-ne v0, v4, :cond_0

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->isVosOver077:Z

    .line 80
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->isVosOver077:Z

    if-eqz v3, :cond_1

    .line 81
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->BASE_1:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->setCommandBase(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;)V

    .line 87
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 88
    .local v1, "hasUuid":I
    if-ne v1, v4, :cond_2

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 90
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->setUuid(Ljava/util/UUID;)V

    .line 94
    .end local v2    # "uuid":Ljava/lang/String;
    :goto_2
    return-void

    .line 77
    .end local v1    # "hasUuid":I
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 83
    :cond_1
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->BASE_0:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->setCommandBase(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;)V

    goto :goto_1

    .line 92
    .restart local v1    # "hasUuid":I
    :cond_2
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "UUID missing in TIME SYNC"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isVosOver077"    # Z

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 37
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->isVosOver077:Z

    .line 38
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->setUuid(Ljava/util/UUID;)V

    .line 39
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE COMMAND: sync_get_activity"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 45
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    const/16 v2, 0x8

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 49
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    iget v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->startTime:I

    invoke-static {v2}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 50
    iget v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->endTime:I

    invoke-static {v2}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 52
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    return-object v1
.end method

.method public setEndTime(I)V
    .locals 0
    .param p1, "endTime"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->endTime:I

    .line 65
    return-void
.end method

.method public setStartTime(I)V
    .locals 0
    .param p1, "startTime"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->startTime:I

    .line 61
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 103
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->startTime:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->endTime:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->isVosOver077:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    int-to-byte v0, v1

    .line 108
    .local v0, "flag":B
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 111
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 112
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 117
    :goto_1
    return-void

    .end local v0    # "flag":B
    :cond_0
    move v1, v3

    .line 107
    goto :goto_0

    .line 115
    .restart local v0    # "flag":B
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
