.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncDisplaySecondHandCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private isEnabled:Z

.field private settingsCount:B

.field private settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->log:Lorg/slf4j/Logger;

    .line 27
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 111
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 35
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->setUuid(Ljava/util/UUID;)V

    .line 36
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x1

    .line 74
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v2, v4, v6, v7}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    iput-byte v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->settingsCount:B

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-ne v2, v3, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->isEnabled:Z

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Constants;->getSettingsTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$SettingsType;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 82
    .local v0, "hasUuid":I
    if-ne v0, v3, :cond_1

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->setUuid(Ljava/util/UUID;)V

    .line 88
    .end local v1    # "uuid":Ljava/lang/String;
    :goto_1
    return-void

    .line 77
    .end local v0    # "hasUuid":I
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 86
    .restart local v0    # "hasUuid":I
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID missing in TIME SYNC"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE COMMANDS: sync_display_s_hand"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 41
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WATCH SECOND HAND - send pref with value "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->isEnabled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    const/4 v2, 0x3

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 48
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    iget-byte v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->settingsCount:B

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 49
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v2

    int-to-byte v2, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 50
    iget-boolean v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->isEnabled:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    int-to-byte v2, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 52
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    return-object v1

    .line 50
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setIsEnabled(Z)V
    .locals 0
    .param p1, "isEnabled"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->isEnabled:Z

    .line 65
    return-void
.end method

.method public setSettingsCount(B)V
    .locals 0
    .param p1, "settingsCount"    # B

    .prologue
    .line 68
    iput-byte p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->settingsCount:B

    .line 69
    return-void
.end method

.method public setSettingsType(Lcom/vectorwatch/android/utils/Constants$SettingsType;)V
    .locals 0
    .param p1, "settingsType"    # Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    .line 61
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 97
    iget-byte v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->settingsCount:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 98
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->isEnabled:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 99
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->settingsType:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 103
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncDisplaySecondHandCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 98
    goto :goto_0

    .line 106
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
