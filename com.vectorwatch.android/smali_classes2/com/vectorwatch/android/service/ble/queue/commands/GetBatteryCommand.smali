.class public Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "GetBatteryCommand.java"


# static fields
.field private static final BASE_0:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field private static final BASE_2:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private isSystemInfoWith4Decimals:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->log:Lorg/slf4j/Logger;

    .line 28
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_COMMAND:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 29
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->BASE_2:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 30
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_COMMAND:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 31
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->BASE_0:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 111
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x1

    .line 67
    const/4 v3, 0x0

    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v3, v5, v6, v7}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    .line 71
    .local v0, "flag":B
    if-ne v0, v4, :cond_0

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->isSystemInfoWith4Decimals:Z

    .line 74
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->isSystemInfoWith4Decimals:Z

    if-eqz v3, :cond_1

    .line 75
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->BASE_2:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->setCommandBase(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;)V

    .line 81
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 82
    .local v1, "hasUuid":I
    if-ne v1, v4, :cond_2

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->setUuid(Ljava/util/UUID;)V

    .line 88
    .end local v2    # "uuid":Ljava/lang/String;
    :goto_2
    return-void

    .line 71
    .end local v1    # "hasUuid":I
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 77
    :cond_1
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->BASE_0:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->setCommandBase(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;)V

    goto :goto_1

    .line 86
    .restart local v1    # "hasUuid":I
    :cond_2
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "UUID missing in TIME SYNC"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isSystemInfoWith4Decimals"    # Z

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 36
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->isSystemInfoWith4Decimals:Z

    .line 38
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->setUuid(Ljava/util/UUID;)V

    .line 39
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v5, 0xd

    const/4 v4, 0x2

    .line 43
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE COMMANDS: sync_battery_command"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 45
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v2

    if-ne v2, v4, :cond_0

    .line 49
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 51
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 52
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 59
    :goto_0
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    return-object v1

    .line 54
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    :cond_0
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 56
    .restart local v0    # "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 98
    iget-boolean v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->isSystemInfoWith4Decimals:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    int-to-byte v0, v1

    .line 99
    .local v0, "flag":B
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 102
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 103
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/GetBatteryCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    :goto_1
    return-void

    .end local v0    # "flag":B
    :cond_0
    move v1, v3

    .line 98
    goto :goto_0

    .line 106
    .restart local v0    # "flag":B
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
