.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncAppInstallCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->log:Lorg/slf4j/Logger;

    .line 33
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_APP_INSTALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 34
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 191
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 39
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->setUuid(Ljava/util/UUID;)V

    .line 40
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 24
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 83
    sget-object v20, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v21, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-wide/from16 v3, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 85
    new-instance v20, Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-direct/range {v20 .. v20}, Lcom/vectorwatch/android/models/InstallMessageModel;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    .line 87
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    move-object/from16 v20, v0

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lcom/vectorwatch/android/utils/Constants;->getMessageInstallTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/vectorwatch/android/models/InstallMessageModel;->setType(Lcom/vectorwatch/android/utils/Constants$MessageInstallType;)V

    .line 89
    new-instance v6, Lcom/vectorwatch/android/models/ApplicationPackageModel;

    invoke-direct {v6}, Lcom/vectorwatch/android/models/ApplicationPackageModel;-><init>()V

    .line 90
    .local v6, "applicationPackageModel":Lcom/vectorwatch/android/models/ApplicationPackageModel;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setAppId(I)V

    .line 91
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readByte()B

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setPosition(B)V

    .line 92
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readByte()B

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setNumberOfFiles(B)V

    .line 94
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readByte()B

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setNameLength(B)V

    .line 96
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v16

    .line 97
    .local v16, "nameComputedSize":I
    if-lez v16, :cond_0

    .line 98
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setName(Ljava/lang/String;)V

    .line 103
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 105
    .local v7, "countMetaDataItems":I
    if-lez v7, :cond_2

    .line 106
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .local v13, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v7, :cond_1

    .line 108
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v18

    .line 109
    .local v18, "typeOption":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v12

    .line 110
    .local v12, "id":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v20

    move/from16 v0, v20

    int-to-short v0, v0

    move/from16 v17, v0

    .line 111
    .local v17, "size":S
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v15

    .line 112
    .local v15, "md5Size":I
    new-array v14, v15, [B

    .line 113
    .local v14, "md5":[B
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->readByteArray([B)V

    .line 115
    new-instance v8, Lcom/vectorwatch/android/models/FileMetadataModel;

    invoke-direct {v8}, Lcom/vectorwatch/android/models/FileMetadataModel;-><init>()V

    .line 116
    .local v8, "file":Lcom/vectorwatch/android/models/FileMetadataModel;
    new-instance v9, Lcom/vectorwatch/android/models/FileIdModel;

    invoke-direct {v9}, Lcom/vectorwatch/android/models/FileIdModel;-><init>()V

    .line 117
    .local v9, "fileIdModel":Lcom/vectorwatch/android/models/FileIdModel;
    invoke-virtual {v9, v12}, Lcom/vectorwatch/android/models/FileIdModel;->setId(I)V

    .line 118
    invoke-static/range {v18 .. v18}, Lcom/vectorwatch/android/utils/Constants;->getVftpFileTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/vectorwatch/android/models/FileIdModel;->setType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V

    .line 119
    invoke-virtual {v8, v9}, Lcom/vectorwatch/android/models/FileMetadataModel;->setFileId(Lcom/vectorwatch/android/models/FileIdModel;)V

    .line 120
    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/vectorwatch/android/models/FileMetadataModel;->setSize(S)V

    .line 121
    invoke-virtual {v8, v14}, Lcom/vectorwatch/android/models/FileMetadataModel;->setMd5([B)V

    .line 123
    invoke-interface {v13, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 100
    .end local v7    # "countMetaDataItems":I
    .end local v8    # "file":Lcom/vectorwatch/android/models/FileMetadataModel;
    .end local v9    # "fileIdModel":Lcom/vectorwatch/android/models/FileIdModel;
    .end local v11    # "i":I
    .end local v12    # "id":I
    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    .end local v14    # "md5":[B
    .end local v15    # "md5Size":I
    .end local v17    # "size":S
    .end local v18    # "typeOption":I
    :cond_0
    const-string v20, ""

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setName(Ljava/lang/String;)V

    goto :goto_0

    .line 126
    .restart local v7    # "countMetaDataItems":I
    .restart local v11    # "i":I
    .restart local v13    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    :cond_1
    invoke-virtual {v6, v13}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setMetadataList(Ljava/util/List;)V

    .line 131
    .end local v11    # "i":I
    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lcom/vectorwatch/android/models/InstallMessageModel;->setApplicationPackage(Lcom/vectorwatch/android/models/ApplicationPackageModel;)V

    .line 134
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 135
    .local v10, "hasUuid":I
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v10, v0, :cond_3

    .line 136
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v19

    .line 137
    .local v19, "uuid":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->setUuid(Ljava/util/UUID;)V

    .line 141
    .end local v19    # "uuid":Ljava/lang/String;
    :goto_3
    return-void

    .line 128
    .end local v10    # "hasUuid":I
    :cond_2
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setMetadataList(Ljava/util/List;)V

    goto :goto_2

    .line 139
    .restart local v10    # "hasUuid":I
    :cond_3
    sget-object v20, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->log:Lorg/slf4j/Logger;

    const-string v21, "UUID missing in TIME SYNC"

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_3
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public getInstallAppId()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 44
    :cond_0
    const/4 v0, -0x1

    .line 47
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getAppId()I

    move-result v0

    goto :goto_0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 52
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE COMMANDS: sync_app_install"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    if-nez v2, :cond_0

    .line 57
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v3, v4, v5}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 71
    :goto_0
    return-object v0

    .line 62
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->packInstallMessage(Lcom/vectorwatch/android/models/InstallMessageModel;)[B

    move-result-object v1

    .line 64
    .local v1, "packedInstallMessage":[B
    if-nez v1, :cond_1

    .line 65
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v3, v4, v5}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 69
    :cond_1
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setInstallMessageModel(Lcom/vectorwatch/android/models/InstallMessageModel;)V
    .locals 0
    .param p1, "installMessageModel"    # Lcom/vectorwatch/android/models/InstallMessageModel;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    .line 78
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 8
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v7, 0x0

    .line 150
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/InstallMessageModel;->getType()Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->getVal()I

    move-result v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 151
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getAppId()I

    move-result v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 152
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getPosition()B

    move-result v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeByte(B)V

    .line 153
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getNumberOfFiles()B

    move-result v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeByte(B)V

    .line 154
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getNameLength()B

    move-result v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeByte(B)V

    .line 155
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 156
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    array-length v5, v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 162
    :goto_0
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->installMessageModel:Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getMetadataList()Ljava/util/List;

    move-result-object v3

    .line 163
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    if-eqz v3, :cond_1

    .line 164
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 165
    .local v0, "countMetaDataItems":I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 166
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/FileMetadataModel;

    .line 167
    .local v1, "file":Lcom/vectorwatch/android/models/FileMetadataModel;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/FileMetadataModel;->getFileId()Lcom/vectorwatch/android/models/FileIdModel;

    move-result-object v2

    .line 168
    .local v2, "fileIdModel":Lcom/vectorwatch/android/models/FileIdModel;
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/FileIdModel;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->getVal()I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/FileIdModel;->getId()I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/FileMetadataModel;->getSize()S

    move-result v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 173
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/FileMetadataModel;->getMd5()[B

    move-result-object v6

    array-length v4, v6

    .line 174
    .local v4, "md5Size":I
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 175
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/FileMetadataModel;->getMd5()[B

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_1

    .line 159
    .end local v0    # "countMetaDataItems":I
    .end local v1    # "file":Lcom/vectorwatch/android/models/FileMetadataModel;
    .end local v2    # "fileIdModel":Lcom/vectorwatch/android/models/FileIdModel;
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    .end local v4    # "md5Size":I
    :cond_0
    invoke-virtual {p1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 178
    .restart local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    :cond_1
    invoke-virtual {p1, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 182
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->getUuid()Ljava/util/UUID;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 183
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 184
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppInstallCommand;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 188
    :goto_2
    return-void

    .line 186
    :cond_3
    invoke-virtual {p1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2
.end method
