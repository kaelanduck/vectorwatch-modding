.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncTimeCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private crtTime:J

.field private dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->log:Lorg/slf4j/Logger;

    .line 29
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_TIME:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 128
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 38
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->setUuid(Ljava/util/UUID;)V

    .line 39
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 88
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->HIGH:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->crtTime:J

    .line 91
    new-instance v2, Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-direct {v2}, Lcom/vectorwatch/android/models/DstInfoModel;-><init>()V

    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    .line 92
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/vectorwatch/android/models/DstInfoModel;->setStart(J)V

    .line 93
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/vectorwatch/android/models/DstInfoModel;->setEnd(J)V

    .line 94
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/vectorwatch/android/models/DstInfoModel;->setDstOffset(J)V

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 98
    .local v0, "hasUuid":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->setUuid(Ljava/util/UUID;)V

    .line 104
    .end local v1    # "uuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 102
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID missing in TIME SYNC"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x3e8

    .line 43
    sget-object v8, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->log:Lorg/slf4j/Logger;

    const-string v9, "BLE COMMANDS: sync_time"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 44
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    const/16 v8, 0x10

    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 48
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v7

    .line 49
    .local v7, "timeZone":Ljava/util/TimeZone;
    invoke-virtual {v7}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v6

    .line 52
    .local v6, "offsetLocal":I
    div-int/lit16 v8, v6, 0x3e8

    div-int/lit8 v8, v8, 0x3c

    int-to-short v5, v8

    .line 54
    .local v5, "offset":S
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/DstInfoModel;->getStart()J

    move-result-wide v8

    div-long/2addr v8, v10

    long-to-int v8, v8

    invoke-static {v8}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v8

    div-int/lit8 v3, v8, 0x3c

    .line 55
    .local v3, "dstStart":I
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/DstInfoModel;->getEnd()J

    move-result-wide v8

    div-long/2addr v8, v10

    long-to-int v8, v8

    invoke-static {v8}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v8

    div-int/lit8 v1, v8, 0x3c

    .line 58
    .local v1, "dstEnd":I
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/DstInfoModel;->getDstOffset()J

    move-result-wide v8

    div-long/2addr v8, v10

    const-wide/16 v10, 0x3c

    div-long/2addr v8, v10

    long-to-int v2, v8

    .line 59
    .local v2, "dstOffset":I
    sget-object v8, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BLE COMMANDS: TIME SYNC - offset = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " dstStart = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " dstEnd = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " dstOffset "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " | crt time = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->crtTime:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 62
    sget-object v8, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BLE COMMANDS: TIME SYNC crt time in Vector time = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->crtTime:J

    long-to-int v10, v10

    invoke-static {v10}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 63
    iget-wide v8, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->crtTime:J

    long-to-int v8, v8

    invoke-static {v8}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 64
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 65
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 66
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 67
    int-to-short v8, v2

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 69
    new-instance v8, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v9

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    return-object v4
.end method

.method public setCrtTime(J)V
    .locals 1
    .param p1, "crtTime"    # J

    .prologue
    .line 82
    iput-wide p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->crtTime:J

    .line 83
    return-void
.end method

.method public setDstInfo(Lcom/vectorwatch/android/models/DstInfoModel;)V
    .locals 4
    .param p1, "dstInfo"    # Lcom/vectorwatch/android/models/DstInfoModel;

    .prologue
    .line 76
    sget-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TIME SYNC - END: start = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/DstInfoModel;->getStart()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | end = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/DstInfoModel;->getEnd()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | offset = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 77
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/DstInfoModel;->getDstOffset()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 76
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 78
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    .line 79
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->crtTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/DstInfoModel;->getStart()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 115
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/DstInfoModel;->getEnd()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 116
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->dstInfo:Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/DstInfoModel;->getDstOffset()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 119
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 120
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTimeCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
