.class public Lcom/vectorwatch/android/service/ble/queue/PriorityComparator;
.super Ljava/lang/Object;
.source "PriorityComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/PriorityComparator;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/PriorityComparator;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 8
    .param p1, "firstObject"    # Ljava/lang/Object;
    .param p2, "secondObject"    # Ljava/lang/Object;

    .prologue
    .line 16
    instance-of v4, p1, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    if-eqz v4, :cond_0

    instance-of v4, p2, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    if-nez v4, :cond_1

    .line 17
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Object is not a Command type instance."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    move-object v0, p1

    .line 20
    check-cast v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .local v0, "firstCommand":Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
    move-object v2, p2

    .line 21
    check-cast v2, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;

    .line 26
    .local v2, "secondCommand":Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getPriority()Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    move-result-object v4

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getPriority()Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    move-result-object v5

    if-ne v4, v5, :cond_2

    .line 27
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getTimestamp()J

    move-result-wide v4

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getTimestamp()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-int v4, v4

    .line 33
    :goto_0
    return v4

    .line 31
    :cond_2
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getPriority()Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->ordinal()I

    move-result v1

    .line 32
    .local v1, "firstOrdinal":I
    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;->getPriority()Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->ordinal()I

    move-result v3

    .line 33
    .local v3, "secondOrdinal":I
    sub-int v4, v1, v3

    neg-int v4, v4

    goto :goto_0
.end method
