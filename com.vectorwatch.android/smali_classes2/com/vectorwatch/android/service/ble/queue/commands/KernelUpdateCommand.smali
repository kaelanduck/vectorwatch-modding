.class public Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "KernelUpdateCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field private static final BLOCK_SIZE:I = 0x222

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private pathToFile:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->log:Lorg/slf4j/Logger;

    .line 31
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SYSTEM_UPDATE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 32
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 146
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 38
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->setUuid(Ljava/util/UUID;)V

    .line 39
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 113
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->HIGH:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->pathToFile:Ljava/lang/String;

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 119
    .local v0, "hasUuid":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->setUuid(Ljava/util/UUID;)V

    .line 125
    .end local v1    # "uuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 123
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID missing in TIME SYNC"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    sget-object v16, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v17, "BLE COMMANDS: kernel_update_command"

    invoke-interface/range {v16 .. v17}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 45
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v9, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    :try_start_0
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->pathToFile:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 50
    .local v14, "softwareOSFile":Ljava/io/File;
    new-instance v8, Ljava/io/RandomAccessFile;

    const-string v16, "r"

    move-object/from16 v0, v16

    invoke-direct {v8, v14, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 53
    .local v8, "input":Ljava/io/RandomAccessFile;
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    add-int/lit8 v15, v16, -0x10

    .line 54
    .local v15, "totalBytes":I
    const/4 v13, 0x0

    .line 55
    .local v13, "sentBytes":I
    const/4 v12, 0x0

    .line 57
    .local v12, "packetIndex":S
    const/16 v16, 0x10

    move/from16 v0, v16

    new-array v7, v0, [B

    .line 60
    .local v7, "headerBuff":[B
    const/16 v16, 0x11

    invoke-static/range {v16 .. v16}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v16

    sget-object v17, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual/range {v16 .. v17}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 61
    .local v5, "data":Ljava/nio/ByteBuffer;
    const/16 v16, 0x66

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 63
    const/16 v16, 0x0

    const/16 v17, 0x10

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v7, v0, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 64
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 66
    new-instance v10, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v16

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v10, v0, v1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 67
    .local v10, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    const/16 v16, 0x222

    move/from16 v0, v16

    new-array v2, v0, [B

    .line 73
    .local v2, "block":[B
    :goto_0
    if-ge v13, v15, :cond_1

    .line 74
    const/16 v16, 0x222

    sub-int v17, v15, v13

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 75
    .local v3, "blockLength":I
    const/4 v11, 0x0

    .line 78
    .local v11, "offset":I
    :goto_1
    if-ge v11, v3, :cond_0

    sub-int v16, v3, v11

    move/from16 v0, v16

    invoke-virtual {v8, v2, v11, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v4

    .local v4, "count":I
    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v4, v0, :cond_0

    .line 80
    add-int/2addr v11, v4

    goto :goto_1

    .line 82
    .end local v4    # "count":I
    :cond_0
    add-int/2addr v13, v3

    .line 85
    const/16 v16, 0x225

    invoke-static/range {v16 .. v16}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v16

    sget-object v17, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual/range {v16 .. v17}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 86
    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 88
    invoke-virtual {v5, v12}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 89
    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 91
    new-instance v10, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .end local v10    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v16

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v10, v0, v1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 92
    .restart local v10    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 93
    add-int/lit8 v16, v12, 0x1

    move/from16 v0, v16

    int-to-short v12, v0

    .line 94
    goto :goto_0

    .line 96
    .end local v2    # "block":[B
    .end local v3    # "blockLength":I
    .end local v5    # "data":Ljava/nio/ByteBuffer;
    .end local v7    # "headerBuff":[B
    .end local v8    # "input":Ljava/io/RandomAccessFile;
    .end local v10    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .end local v11    # "offset":I
    .end local v12    # "packetIndex":S
    .end local v13    # "sentBytes":I
    .end local v14    # "softwareOSFile":Ljava/io/File;
    .end local v15    # "totalBytes":I
    :catch_0
    move-exception v6

    .line 98
    .local v6, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v6}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 103
    .end local v6    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_2
    return-object v9

    .line 99
    :catch_1
    move-exception v6

    .line 100
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public setPathToFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "pathToFile"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->pathToFile:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 134
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->pathToFile:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/KernelUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
