.class public final enum Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;
.super Ljava/lang/Enum;
.source "BaseCommand.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Priority"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

.field public static final enum HIGH:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

.field public static final enum LOW:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

.field public static final enum MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->LOW:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    .line 83
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    .line 84
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->HIGH:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    .line 80
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->LOW:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->HIGH:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->$VALUES:[Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 80
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->$VALUES:[Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    return-object v0
.end method
