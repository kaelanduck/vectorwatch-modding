.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncStreamUpdateCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private route:Lcom/vectorwatch/android/models/PushUpdateRoute;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    .line 32
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_PUSH:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 253
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 38
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->setUuid(Ljava/util/UUID;)V

    .line 39
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 10
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 144
    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v6, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {p0, v5, v6, v8, v9}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 146
    new-instance v5, Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-direct {v5}, Lcom/vectorwatch/android/models/PushUpdateRoute;-><init>()V

    iput-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_3

    .line 149
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setAppId(Ljava/lang/Integer;)V

    .line 150
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setWatchfaceId(Ljava/lang/Integer;)V

    .line 151
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setFieldId(Ljava/lang/Integer;)V

    .line 152
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setSecondsToLive(Ljava/lang/Integer;)V

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 155
    .local v1, "flag":I
    if-ltz v1, :cond_0

    .line 156
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setChannelUniqueLabel(Ljava/lang/String;)V

    .line 159
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 160
    if-ltz v1, :cond_1

    .line 161
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setUpdateData(Ljava/lang/String;)V

    .line 164
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 165
    if-ltz v1, :cond_2

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 167
    .local v3, "streamType":Ljava/lang/String;
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v5, v3}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setStreamType(Ljava/lang/String;)V

    .line 170
    .end local v3    # "streamType":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 171
    if-ltz v1, :cond_3

    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "elemType":Ljava/lang/String;
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v5, v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setElementType(Ljava/lang/String;)V

    .line 178
    .end local v0    # "elemType":Ljava/lang/String;
    .end local v1    # "flag":I
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 179
    .local v2, "hasUuid":I
    const/4 v5, 0x1

    if-ne v2, v5, :cond_4

    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 181
    .local v4, "uuid":Ljava/lang/String;
    invoke-static {v4}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->setUuid(Ljava/util/UUID;)V

    .line 185
    .end local v4    # "uuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 183
    :cond_4
    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v6, "UUID missing in TIME SYNC"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    .line 43
    sget-object v10, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v11, "BLE COMMANDS: sync_stream_update"

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 44
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    if-nez v10, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-object v6

    .line 54
    :cond_1
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getAppId()Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 55
    .local v0, "appId":I
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getWatchfaceId()Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 56
    .local v9, "wId":I
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getFieldId()Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 57
    .local v5, "fId":I
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getStreamType()Ljava/lang/String;

    move-result-object v8

    .line 60
    .local v8, "streamType":Ljava/lang/String;
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getUpdateData()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_3

    .line 61
    const-string v2, "\u0000"

    .line 66
    .local v2, "data":Ljava/lang/String;
    :goto_1
    const/16 v4, 0x8

    .line 67
    .local v4, "elementTypeId":I
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getElementType()Ljava/lang/String;

    move-result-object v3

    .line 68
    .local v3, "elementType":Ljava/lang/String;
    sget-object v10, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Stream type = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " Element type = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 69
    sget-object v10, Lcom/vectorwatch/android/models/Element$Type;->TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element$Type;->getLabel()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    sget-object v10, Lcom/vectorwatch/android/models/Stream$StreamTypes;->WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 70
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    invoke-virtual {v10}, Lcom/vectorwatch/android/utils/Constants$AppElementType;->getVal()I

    move-result v4

    .line 77
    :cond_2
    :goto_2
    sget-object v10, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "STREAM FIELD: Update: app = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " face = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " element = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " data = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 80
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    if-eqz v10, :cond_0

    .line 83
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_MULTIPLE_LINES:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    invoke-virtual {v10}, Lcom/vectorwatch/android/utils/Constants$AppElementType;->getVal()I

    move-result v10

    if-ne v4, v10, :cond_6

    .line 84
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    array-length v10, v10

    add-int/lit8 v10, v10, 0xc

    add-int/lit8 v10, v10, 0x1

    add-int/lit8 v10, v10, 0x4

    add-int/lit8 v7, v10, 0x1

    .line 89
    .local v7, "size":I
    :goto_3
    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v10

    sget-object v11, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v10, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 91
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 92
    int-to-byte v10, v9

    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 93
    int-to-byte v10, v5

    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 94
    int-to-byte v10, v4

    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 95
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getSecondsToLive()Ljava/lang/Integer;

    move-result-object v10

    if-eqz v10, :cond_7

    .line 96
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getSecondsToLive()Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 100
    :goto_4
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 101
    const/4 v10, 0x1

    invoke-static {v10, v13}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v10

    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 106
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_MULTIPLE_LINES:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    invoke-virtual {v10}, Lcom/vectorwatch/android/utils/Constants$AppElementType;->getVal()I

    move-result v10

    if-eq v4, v10, :cond_8

    .line 107
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 108
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 109
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 110
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 111
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 112
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 113
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 114
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 120
    :goto_5
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 122
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 124
    new-instance v10, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v11

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getAppId()Ljava/lang/Integer;

    move-result-object v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getFieldId()Ljava/lang/Integer;

    move-result-object v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getWatchfaceId()Ljava/lang/Integer;

    move-result-object v10

    if-eqz v10, :cond_0

    .line 128
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v10

    new-instance v11, Lcom/vectorwatch/android/events/StreamValueSentToWatchEvent;

    iget-object v12, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getAppId()Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iget-object v13, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v13}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getWatchfaceId()Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    iget-object v14, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getFieldId()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-direct {v11, v12, v13, v14}, Lcom/vectorwatch/android/events/StreamValueSentToWatchEvent;-><init>(III)V

    invoke-virtual {v10, v11}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 63
    .end local v1    # "buffer":Ljava/nio/ByteBuffer;
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "elementType":Ljava/lang/String;
    .end local v4    # "elementTypeId":I
    .end local v7    # "size":I
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v11}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getUpdateData()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "data":Ljava/lang/String;
    goto/16 :goto_1

    .line 71
    .restart local v3    # "elementType":Ljava/lang/String;
    .restart local v4    # "elementTypeId":I
    :cond_4
    sget-object v10, Lcom/vectorwatch/android/models/Element$Type;->TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element$Type;->getLabel()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 72
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_COMPLICATION:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    invoke-virtual {v10}, Lcom/vectorwatch/android/utils/Constants$AppElementType;->getVal()I

    move-result v4

    goto/16 :goto_2

    .line 73
    :cond_5
    sget-object v10, Lcom/vectorwatch/android/models/Element$Type;->LONG_TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/Element$Type;->getLabel()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 74
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_MULTIPLE_LINES:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    invoke-virtual {v10}, Lcom/vectorwatch/android/utils/Constants$AppElementType;->getVal()I

    move-result v4

    goto/16 :goto_2

    .line 86
    :cond_6
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    array-length v10, v10

    add-int/lit8 v10, v10, 0x13

    add-int/lit8 v10, v10, 0x1

    add-int/lit8 v10, v10, 0x4

    add-int/lit8 v7, v10, 0x1

    .restart local v7    # "size":I
    goto/16 :goto_3

    .line 98
    .restart local v1    # "buffer":Ljava/nio/ByteBuffer;
    :cond_7
    const/4 v10, -0x1

    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto/16 :goto_4

    .line 117
    :cond_8
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto/16 :goto_5
.end method

.method public setRoute(Lcom/vectorwatch/android/models/PushUpdateRoute;)V
    .locals 0
    .param p1, "route"    # Lcom/vectorwatch/android/models/PushUpdateRoute;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 139
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 194
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    if-nez v0, :cond_0

    .line 195
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 242
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 243
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 244
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 249
    :goto_1
    sget-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v1, "BLE COMMANDS: sync_stream_update_command: Null update data"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 250
    return-void

    .line 197
    :cond_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 199
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getAppId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 200
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getWatchfaceId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 201
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getFieldId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 202
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getSecondsToLive()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getSecondsToLive()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 208
    :goto_2
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getChannelUniqueLabel()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 209
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 210
    sget-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v1, "BLE COMMANDS: sync_stream_update_command: Null unique label"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 216
    :goto_3
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getUpdateData()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 217
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 218
    sget-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v1, "BLE COMMANDS: sync_stream_update_command: Null update data"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 224
    :goto_4
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getStreamType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 225
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 226
    sget-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v1, "BLE COMMANDS: sync_stream_update_command: Null stream type"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 232
    :goto_5
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getElementType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 233
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 234
    sget-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v1, "BLE COMMANDS: sync_stream_update_command: Null element type"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 205
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getChannelUniqueLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 213
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getChannelUniqueLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 220
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getUpdateData()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 221
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getUpdateData()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_4

    .line 228
    :cond_4
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getStreamType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 229
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getStreamType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_5

    .line 236
    :cond_5
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getElementType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 237
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamUpdateCommand;->route:Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->getElementType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 246
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_1
.end method
