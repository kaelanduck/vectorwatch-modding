.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncWarningOptionsCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private defaultLinkLostIcon:Z

.field private defaultLinkLostNotification:Z

.field private defaultLowBatteryIcon:Z

.field private defaultLowBatteryNotifications:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->log:Lorg/slf4j/Logger;

    .line 30
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 166
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 39
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->setUuid(Ljava/util/UUID;)V

    .line 40
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 10
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 113
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v6, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {p0, v3, v6, v8, v9}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 116
    .local v0, "flag":I
    if-ne v0, v4, :cond_1

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLinkLostIcon:Z

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 119
    if-ne v0, v4, :cond_2

    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLowBatteryIcon:Z

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 122
    if-ne v0, v4, :cond_3

    move v3, v4

    :goto_2
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLinkLostNotification:Z

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 125
    if-ne v0, v4, :cond_0

    move v5, v4

    :cond_0
    iput-boolean v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLowBatteryNotifications:Z

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 129
    .local v1, "hasUuid":I
    if-ne v1, v4, :cond_4

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->setUuid(Ljava/util/UUID;)V

    .line 135
    .end local v2    # "uuid":Ljava/lang/String;
    :goto_3
    return-void

    .end local v1    # "hasUuid":I
    :cond_1
    move v3, v5

    .line 116
    goto :goto_0

    :cond_2
    move v3, v5

    .line 119
    goto :goto_1

    :cond_3
    move v3, v5

    .line 122
    goto :goto_2

    .line 133
    .restart local v1    # "hasUuid":I
    :cond_4
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "UUID missing in TIME SYNC"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private getWarningData()[B
    .locals 9

    .prologue
    .line 57
    const/4 v6, 0x3

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 59
    .local v0, "dataBytes":Ljava/nio/ByteBuffer;
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 60
    sget-object v6, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_WARNING_OPTIONS:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v6}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v6

    int-to-byte v6, v6

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 63
    const/4 v1, 0x0

    .line 64
    .local v1, "linkLostIcon":B
    iget-boolean v6, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLinkLostIcon:Z

    if-eqz v6, :cond_0

    .line 65
    const/4 v1, 0x1

    .line 69
    :cond_0
    const/4 v3, 0x0

    .line 70
    .local v3, "lowBatteryIcon":B
    iget-boolean v6, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLowBatteryIcon:Z

    if-eqz v6, :cond_1

    .line 71
    const/4 v3, 0x2

    .line 75
    :cond_1
    const/4 v2, 0x0

    .line 76
    .local v2, "linkLostNotif":B
    iget-boolean v6, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLinkLostNotification:Z

    if-eqz v6, :cond_2

    .line 77
    const/4 v2, 0x4

    .line 81
    :cond_2
    const/4 v4, 0x0

    .line 82
    .local v4, "lowBatteryNotif":B
    iget-boolean v6, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLowBatteryNotifications:Z

    if-eqz v6, :cond_3

    .line 83
    const/16 v4, 0x8

    .line 86
    :cond_3
    or-int v6, v1, v3

    or-int/2addr v6, v2

    or-int/2addr v6, v4

    int-to-byte v5, v6

    .line 87
    .local v5, "value":B
    sget-object v6, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "BLE SYNC WARNINGS: - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 91
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    return-object v6
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMAND: sync_warning_options"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->getWarningData()[B

    move-result-object v2

    .line 48
    .local v2, "settings":[B
    new-instance v1, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 49
    .local v1, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    return-object v0
.end method

.method public setDefaultLinkLostIcon(Z)V
    .locals 0
    .param p1, "defaultLinkLostIcon"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLinkLostIcon:Z

    .line 97
    return-void
.end method

.method public setDefaultLinkLostNotification(Z)V
    .locals 0
    .param p1, "defaultLinkLostNotification"    # Z

    .prologue
    .line 100
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLinkLostNotification:Z

    .line 101
    return-void
.end method

.method public setDefaultLowBatteryIcon(Z)V
    .locals 0
    .param p1, "defaultLowBatteryIcon"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLowBatteryIcon:Z

    .line 105
    return-void
.end method

.method public setDefaultLowBatteryNotifications(Z)V
    .locals 0
    .param p1, "defaultLowBatteryNotifications"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLowBatteryNotifications:Z

    .line 109
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 144
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLinkLostIcon:Z

    if-eqz v3, :cond_0

    move v0, v1

    .line 145
    .local v0, "flag":I
    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 147
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLowBatteryIcon:Z

    if-eqz v3, :cond_1

    move v0, v1

    .line 148
    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 150
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLinkLostNotification:Z

    if-eqz v3, :cond_2

    move v0, v1

    .line 151
    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 153
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->defaultLowBatteryNotifications:Z

    if-eqz v3, :cond_3

    move v0, v1

    .line 154
    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 158
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 159
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncWarningOptionsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 163
    :goto_4
    return-void

    .end local v0    # "flag":I
    :cond_0
    move v0, v2

    .line 144
    goto :goto_0

    .restart local v0    # "flag":I
    :cond_1
    move v0, v2

    .line 147
    goto :goto_1

    :cond_2
    move v0, v2

    .line 150
    goto :goto_2

    :cond_3
    move v0, v2

    .line 153
    goto :goto_3

    .line 161
    :cond_4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4
.end method
