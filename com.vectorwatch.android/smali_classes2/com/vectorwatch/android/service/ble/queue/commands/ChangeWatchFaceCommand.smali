.class public Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "ChangeWatchFaceCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private animationType:I

.field private appId:I

.field private forced:Z

.field private vibration:Z

.field private watchFaceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->log:Lorg/slf4j/Logger;

    .line 27
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_COMMAND:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 126
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 33
    iput-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->forced:Z

    .line 34
    iput-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->vibration:Z

    .line 37
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->setUuid(Ljava/util/UUID;)V

    .line 38
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 85
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v2, v5, v6, v7}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 33
    iput-boolean v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->forced:Z

    .line 34
    iput-boolean v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->vibration:Z

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->appId:I

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->watchFaceId:I

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->animationType:I

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-ne v2, v3, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->forced:Z

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-ne v2, v3, :cond_0

    move v4, v3

    :cond_0
    iput-boolean v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->vibration:Z

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 95
    .local v0, "hasUuid":I
    if-ne v0, v3, :cond_2

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->setUuid(Ljava/util/UUID;)V

    .line 101
    .end local v1    # "uuid":Ljava/lang/String;
    :goto_1
    return-void

    .end local v0    # "hasUuid":I
    :cond_1
    move v2, v4

    .line 90
    goto :goto_0

    .line 99
    .restart local v0    # "hasUuid":I
    :cond_2
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID missing in CHANGE FACE"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 46
    const/16 v5, 0xb

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 47
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/16 v5, 0x12

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 48
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 49
    iget-boolean v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->forced:Z

    if-eqz v5, :cond_1

    :goto_0
    int-to-byte v3, v3

    iget-boolean v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->vibration:Z

    if-eqz v5, :cond_0

    const/4 v4, 0x2

    :cond_0
    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 50
    iget v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->appId:I

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 51
    iget v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->watchFaceId:I

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 52
    iget v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->animationType:I

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 54
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 55
    .local v2, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    return-object v1

    .end local v1    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    .end local v2    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    :cond_1
    move v3, v4

    .line 49
    goto :goto_0
.end method

.method public setAnimationType(I)V
    .locals 0
    .param p1, "animationType"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->animationType:I

    .line 68
    return-void
.end method

.method public setAppId(I)V
    .locals 0
    .param p1, "appId"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->appId:I

    .line 64
    return-void
.end method

.method public setForced(Z)V
    .locals 0
    .param p1, "forced"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->forced:Z

    .line 72
    return-void
.end method

.method public setVibration(Z)V
    .locals 0
    .param p1, "vibration"    # Z

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->vibration:Z

    .line 76
    return-void
.end method

.method public setWatchFaceId(I)V
    .locals 0
    .param p1, "watchFaceId"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->watchFaceId:I

    .line 80
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 110
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->appId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->watchFaceId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->animationType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->forced:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 114
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->vibration:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 117
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 118
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/ChangeWatchFaceCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 123
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 113
    goto :goto_0

    :cond_1
    move v0, v2

    .line 114
    goto :goto_1

    .line 121
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2
.end method
