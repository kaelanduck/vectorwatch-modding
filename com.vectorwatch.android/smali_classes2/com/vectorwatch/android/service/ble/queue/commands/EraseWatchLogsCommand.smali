.class public Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "EraseWatchLogsCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->log:Lorg/slf4j/Logger;

    .line 27
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_COMMAND:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 81
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 31
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->setUuid(Ljava/util/UUID;)V

    .line 32
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 52
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 56
    .local v0, "hasUuid":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->setUuid(Ljava/util/UUID;)V

    .line 62
    .end local v1    # "uuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 60
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID missing in ERASE LOGS FROM WATCH"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMANDS: erase_watch_logs_command"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 40
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    const/4 v3, 0x2

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 41
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/16 v3, 0x15

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 43
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 44
    .local v2, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 74
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/EraseWatchLogsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
