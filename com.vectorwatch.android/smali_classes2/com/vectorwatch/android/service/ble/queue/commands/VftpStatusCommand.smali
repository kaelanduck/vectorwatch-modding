.class public Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "VftpStatusCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

.field private status:Lcom/vectorwatch/android/utils/Constants$VftpStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->log:Lorg/slf4j/Logger;

    .line 27
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_VFTP:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 101
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 34
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->setUuid(Ljava/util/UUID;)V

    .line 35
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 63
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v3, v4, v6, v7}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 66
    .local v1, "option":I
    invoke-static {v1}, Lcom/vectorwatch/android/utils/Constants;->getVftpStatusFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    move-result-object v3

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->status:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 69
    invoke-static {v1}, Lcom/vectorwatch/android/utils/Constants;->getVftpMessageTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    move-result-object v3

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 73
    .local v0, "hasUuid":I
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 75
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->setUuid(Ljava/util/UUID;)V

    .line 79
    .end local v2    # "uuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 77
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "UUID missing in TIME SYNC"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE COMMANDS: vftp_status"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 40
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 43
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v2

    int-to-byte v2, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 44
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->status:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->getVal()I

    move-result v2

    int-to-byte v2, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 46
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    return-object v1
.end method

.method public setMsgType(Lcom/vectorwatch/android/utils/Constants$VftpMessageType;)V
    .locals 0
    .param p1, "msgType"    # Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 58
    return-void
.end method

.method public setStatus(Lcom/vectorwatch/android/utils/Constants$VftpStatus;)V
    .locals 0
    .param p1, "status"    # Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->status:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    .line 54
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->status:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->getVal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpStatusCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
