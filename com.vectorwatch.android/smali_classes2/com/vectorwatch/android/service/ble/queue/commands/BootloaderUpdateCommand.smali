.class public Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "BootloaderUpdateCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field private static final BLOCK_SIZE:I = 0x222

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private pathToFile:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->log:Lorg/slf4j/Logger;

    .line 31
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_SYSTEM_UPDATE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 32
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 152
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 40
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->setUuid(Ljava/util/UUID;)V

    .line 41
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 119
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->HIGH:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->pathToFile:Ljava/lang/String;

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 125
    .local v0, "hasUuid":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->setUuid(Ljava/util/UUID;)V

    .line 131
    .end local v1    # "uuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 129
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID missing in BOOT UPDATE"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    sget-object v18, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->log:Lorg/slf4j/Logger;

    const-string v19, "BLE COMMANDS: boot_update"

    invoke-interface/range {v18 .. v19}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 46
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v10, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    const/4 v8, 0x0

    .line 51
    .local v8, "input":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v15, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->pathToFile:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 52
    .local v15, "softwareOSFile":Ljava/io/File;
    new-instance v9, Ljava/io/RandomAccessFile;

    const-string v18, "r"

    move-object/from16 v0, v18

    invoke-direct {v9, v15, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 55
    .end local v8    # "input":Ljava/io/RandomAccessFile;
    .local v9, "input":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    add-int/lit8 v17, v18, -0x10

    .line 56
    .local v17, "totalBytes":I
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v18, v0

    const v19, 0x44088000    # 546.0f

    div-float v18, v18, v19

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v16, v0

    .line 57
    .local v16, "totalBlocks":I
    const/4 v14, 0x0

    .line 58
    .local v14, "sentBytes":I
    const/4 v13, 0x0

    .line 60
    .local v13, "packetIndex":S
    const/16 v18, 0x10

    move/from16 v0, v18

    new-array v7, v0, [B

    .line 63
    .local v7, "header_buff":[B
    const/16 v18, 0x11

    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v18

    sget-object v19, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual/range {v18 .. v19}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 64
    .local v5, "data":Ljava/nio/ByteBuffer;
    const/16 v18, 0x65

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 66
    const/16 v18, 0x0

    const/16 v19, 0x10

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v9, v7, v0, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    .line 67
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 69
    new-instance v11, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v18

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 70
    .local v11, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    const/16 v18, 0x222

    move/from16 v0, v18

    new-array v2, v0, [B

    .line 76
    .local v2, "block":[B
    :goto_0
    move/from16 v0, v17

    if-ge v14, v0, :cond_1

    .line 77
    const/16 v18, 0x222

    sub-int v19, v17, v14

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 78
    .local v3, "blockLength":I
    const/4 v12, 0x0

    .line 81
    .local v12, "offset":I
    :goto_1
    if-ge v12, v3, :cond_0

    sub-int v18, v3, v12

    move/from16 v0, v18

    invoke-virtual {v9, v2, v12, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v4

    .local v4, "count":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v4, v0, :cond_0

    .line 83
    add-int/2addr v12, v4

    goto :goto_1

    .line 85
    .end local v4    # "count":I
    :cond_0
    add-int/2addr v14, v3

    .line 88
    const/16 v18, 0x225

    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v18

    sget-object v19, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual/range {v18 .. v19}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 89
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 91
    invoke-virtual {v5, v13}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 92
    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 94
    new-instance v11, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .end local v11    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v18

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 95
    .restart local v11    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 96
    add-int/lit8 v18, v13, 0x1

    move/from16 v0, v18

    int-to-short v13, v0

    .line 97
    goto :goto_0

    .end local v3    # "blockLength":I
    .end local v12    # "offset":I
    :cond_1
    move-object v8, v9

    .line 106
    .end local v2    # "block":[B
    .end local v5    # "data":Ljava/nio/ByteBuffer;
    .end local v7    # "header_buff":[B
    .end local v9    # "input":Ljava/io/RandomAccessFile;
    .end local v11    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .end local v13    # "packetIndex":S
    .end local v14    # "sentBytes":I
    .end local v15    # "softwareOSFile":Ljava/io/File;
    .end local v16    # "totalBlocks":I
    .end local v17    # "totalBytes":I
    .restart local v8    # "input":Ljava/io/RandomAccessFile;
    :goto_2
    return-object v10

    .line 99
    :catch_0
    move-exception v6

    .line 101
    .local v6, "e":Ljava/io/FileNotFoundException;
    :goto_3
    invoke-virtual {v6}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 102
    .end local v6    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v6

    .line 103
    .local v6, "e":Ljava/io/IOException;
    :goto_4
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 102
    .end local v6    # "e":Ljava/io/IOException;
    .end local v8    # "input":Ljava/io/RandomAccessFile;
    .restart local v9    # "input":Ljava/io/RandomAccessFile;
    .restart local v15    # "softwareOSFile":Ljava/io/File;
    :catch_2
    move-exception v6

    move-object v8, v9

    .end local v9    # "input":Ljava/io/RandomAccessFile;
    .restart local v8    # "input":Ljava/io/RandomAccessFile;
    goto :goto_4

    .line 99
    .end local v8    # "input":Ljava/io/RandomAccessFile;
    .restart local v9    # "input":Ljava/io/RandomAccessFile;
    :catch_3
    move-exception v6

    move-object v8, v9

    .end local v9    # "input":Ljava/io/RandomAccessFile;
    .restart local v8    # "input":Ljava/io/RandomAccessFile;
    goto :goto_3
.end method

.method public setPathToFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "pathToFile"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->pathToFile:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->pathToFile:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 145
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/BootloaderUpdateCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
