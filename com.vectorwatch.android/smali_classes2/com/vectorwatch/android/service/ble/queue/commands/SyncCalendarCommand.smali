.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncCalendarCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAX_LENGTH_LOCATION:I = 0x28

.field private static final MAX_LENGTH_TITLE:I = 0x28

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CalendarEventModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    .line 30
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_CALENDAR_EVENTS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 31
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 250
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 39
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->setUuid(Ljava/util/UUID;)V

    .line 40
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 20
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 152
    sget-object v15, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v16, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-wide/from16 v2, v18

    invoke-direct {v0, v15, v1, v2, v3}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 154
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 156
    .local v4, "count":I
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->events:Ljava/util/List;

    .line 157
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v4, :cond_2

    .line 158
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 161
    .local v6, "flag":I
    if-lez v6, :cond_0

    .line 162
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 167
    .local v11, "name":Ljava/lang/String;
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 170
    if-lez v6, :cond_1

    .line 171
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 177
    .local v10, "location":Ljava/lang/String;
    :goto_2
    new-instance v12, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-direct {v12, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 178
    .local v12, "start":Ljava/util/Date;
    new-instance v13, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-direct {v13, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 180
    .local v13, "stop":Ljava/util/Date;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readByte()B

    move-result v9

    .line 182
    .local v9, "index":B
    new-instance v5, Lcom/vectorwatch/android/models/CalendarEventModel;

    invoke-direct {v5, v11, v10, v12, v13}, Lcom/vectorwatch/android/models/CalendarEventModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V

    .line 183
    .local v5, "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    invoke-virtual {v5, v9}, Lcom/vectorwatch/android/models/CalendarEventModel;->setIndex(B)V

    .line 185
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->events:Ljava/util/List;

    invoke-interface {v15, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 164
    .end local v5    # "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    .end local v9    # "index":B
    .end local v10    # "location":Ljava/lang/String;
    .end local v11    # "name":Ljava/lang/String;
    .end local v12    # "start":Ljava/util/Date;
    .end local v13    # "stop":Ljava/util/Date;
    :cond_0
    const-string v11, "No Title"

    .restart local v11    # "name":Ljava/lang/String;
    goto :goto_1

    .line 173
    :cond_1
    const-string v10, ""

    .restart local v10    # "location":Ljava/lang/String;
    goto :goto_2

    .line 189
    .end local v6    # "flag":I
    .end local v10    # "location":Ljava/lang/String;
    .end local v11    # "name":Ljava/lang/String;
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 190
    .local v7, "hasUuid":I
    const/4 v15, 0x1

    if-ne v7, v15, :cond_3

    .line 191
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    .line 192
    .local v14, "uuid":Ljava/lang/String;
    invoke-static {v14}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->setUuid(Ljava/util/UUID;)V

    .line 196
    .end local v14    # "uuid":Ljava/lang/String;
    :goto_3
    return-void

    .line 194
    :cond_3
    sget-object v15, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    const-string v16, "UUID missing in TIME SYNC"

    invoke-interface/range {v15 .. v16}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_3
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    sget-object v20, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    const-string v21, "BLE COMMANDS: sync_calendar_command"

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 46
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v10, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->events:Ljava/util/List;

    move-object/from16 v20, v0

    if-eqz v20, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->events:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    if-lez v20, :cond_4

    .line 49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->events:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_5

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/models/CalendarEventModel;

    .line 50
    .local v7, "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CalendarEventModel;->getName()Ljava/lang/String;

    move-result-object v17

    .line 52
    .local v17, "title":Ljava/lang/String;
    sget-object v21, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "CALENDAR: Event name = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 54
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CalendarEventModel;->getLocation()Ljava/lang/String;

    move-result-object v11

    .line 56
    .local v11, "location":Ljava/lang/String;
    if-nez v17, :cond_0

    .line 57
    const-string v17, "\u0000"

    .line 62
    :goto_1
    if-nez v11, :cond_1

    .line 63
    const-string v11, "\u0000"

    .line 68
    :goto_2
    sget-object v21, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "CALENDAR: Event to send"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CalendarEventModel;->getStartTimestamp()Ljava/util/Date;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CalendarEventModel;->getEndTimestamp()Ljava/util/Date;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CalendarEventModel;->getStartTimestamp()Ljava/util/Date;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->getTime()J

    move-result-wide v22

    const-wide/16 v24, 0x3e8

    div-long v22, v22, v24

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v16

    .line 71
    .local v16, "timestampStart":I
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CalendarEventModel;->getEndTimestamp()Ljava/util/Date;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->getTime()J

    move-result-wide v22

    const-wide/16 v24, 0x3e8

    div-long v22, v22, v24

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v15

    .line 75
    .local v15, "timestampEnd":I
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v21

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    const/16 v22, 0x28

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_2

    .line 77
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x27

    invoke-static/range {v21 .. v23}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v19

    .line 79
    .local v19, "titleSubArray":[B
    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 80
    .local v18, "titleBytes":[B
    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 82
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v18

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    .end local v19    # "titleSubArray":[B
    :goto_3
    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v21

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    const/16 v22, 0x28

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_3

    .line 89
    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x27

    invoke-static/range {v21 .. v23}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v13

    .line 91
    .local v13, "locationSubArray":[B
    array-length v0, v13

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    new-array v12, v0, [B

    .line 92
    .local v12, "locationBytes":[B
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-static {v12, v0}, Ljava/util/Arrays;->fill([BB)V

    .line 94
    const/16 v21, 0x0

    const/16 v22, 0x0

    array-length v0, v13

    move/from16 v23, v0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v13, v0, v12, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    .end local v13    # "locationSubArray":[B
    :goto_4
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x9

    array-length v0, v12

    move/from16 v22, v0

    add-int v9, v21, v22

    .line 101
    .local v9, "length":I
    sget-object v21, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "CAL: Event to send "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " start-end: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "***"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 104
    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    sget-object v22, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 105
    .local v6, "commandBytes":Ljava/nio/ByteBuffer;
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CalendarEventModel;->getIndex()B

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 106
    move/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 107
    invoke-virtual {v6, v15}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 108
    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 109
    invoke-virtual {v6, v12}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 112
    new-instance v14, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v21

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v14, v0, v1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 114
    .local v14, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 59
    .end local v6    # "commandBytes":Ljava/nio/ByteBuffer;
    .end local v9    # "length":I
    .end local v12    # "locationBytes":[B
    .end local v14    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .end local v15    # "timestampEnd":I
    .end local v16    # "timestampStart":I
    .end local v18    # "titleBytes":[B
    :cond_0
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "\u0000"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_1

    .line 65
    :cond_1
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "\u0000"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_2

    .line 84
    .restart local v15    # "timestampEnd":I
    .restart local v16    # "timestampStart":I
    :cond_2
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v18

    .restart local v18    # "titleBytes":[B
    goto/16 :goto_3

    .line 96
    :cond_3
    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v12

    .restart local v12    # "locationBytes":[B
    goto/16 :goto_4

    .line 117
    .end local v7    # "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    .end local v11    # "location":Ljava/lang/String;
    .end local v12    # "locationBytes":[B
    .end local v15    # "timestampEnd":I
    .end local v16    # "timestampStart":I
    .end local v17    # "title":Ljava/lang/String;
    .end local v18    # "titleBytes":[B
    :cond_4
    sget-object v20, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    const-string v21, "CALENDAR: Dummy."

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 119
    const-string v17, "\u0000"

    .line 120
    .restart local v17    # "title":Ljava/lang/String;
    const-string v11, "\u0000"

    .line 122
    .restart local v11    # "location":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v20

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x9

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v21

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    add-int v9, v20, v21

    .line 125
    .restart local v9    # "length":I
    const/4 v8, 0x0

    .line 128
    .local v8, "index":B
    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v20

    sget-object v21, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual/range {v20 .. v21}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 129
    .restart local v6    # "commandBytes":Ljava/nio/ByteBuffer;
    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 130
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 131
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 132
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 133
    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 136
    new-instance v14, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v20

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v14, v0, v1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 137
    .restart local v14    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    .end local v6    # "commandBytes":Ljava/nio/ByteBuffer;
    .end local v8    # "index":B
    .end local v9    # "length":I
    .end local v11    # "location":Ljava/lang/String;
    .end local v14    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .end local v17    # "title":Ljava/lang/String;
    :cond_5
    return-object v10
.end method

.method public setEvents(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/CalendarEventModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->events:Ljava/util/List;

    .line 147
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 9
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v8, 0x0

    .line 205
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->events:Ljava/util/List;

    if-eqz v2, :cond_4

    .line 206
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->events:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 208
    .local v0, "count":I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 210
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->events:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/CalendarEventModel;

    .line 211
    .local v1, "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 212
    :cond_0
    invoke-virtual {p1, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 213
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMANDS: sync_calendar: Null name"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 220
    :goto_1
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getLocation()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 221
    :cond_1
    invoke-virtual {p1, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 222
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMANDS: sync_calendar: Null location"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 229
    :goto_2
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CALENDAR: start/end = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getStartTimestamp()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getEndTimestamp()Ljava/util/Date;

    move-result-object v5

    .line 230
    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 229
    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 231
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getStartTimestamp()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 232
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getEndTimestamp()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 234
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getIndex()B

    move-result v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByte(B)V

    goto/16 :goto_0

    .line 215
    :cond_2
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CALENDAR: Name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 216
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v3, v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 217
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 224
    :cond_3
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CALENDAR: Location = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 225
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v3, v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 226
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 237
    .end local v0    # "count":I
    .end local v1    # "event":Lcom/vectorwatch/android/models/CalendarEventModel;
    :cond_4
    invoke-virtual {p1, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 241
    :cond_5
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 242
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 243
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncCalendarCommand;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 247
    :goto_3
    return-void

    .line 245
    :cond_6
    invoke-virtual {p1, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3
.end method
