.class public Lcom/vectorwatch/android/service/ble/queue/QueueCommand;
.super Ljava/lang/Object;
.source "QueueCommand.java"


# instance fields
.field private priority:I

.field private serializedCommand:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/DataMessage;",
            ">;"
        }
    .end annotation
.end field

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 0
    .param p2, "priority"    # I
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/DataMessage;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 15
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/DataMessage;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/QueueCommand;->serializedCommand:Ljava/util/ArrayList;

    .line 17
    iput p2, p0, Lcom/vectorwatch/android/service/ble/queue/QueueCommand;->priority:I

    .line 18
    iput-object p3, p0, Lcom/vectorwatch/android/service/ble/queue/QueueCommand;->type:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public getPriority()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/QueueCommand;->priority:I

    return v0
.end method

.method public getSerializedCommand()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/DataMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/QueueCommand;->serializedCommand:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/QueueCommand;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setPriority(I)V
    .locals 0
    .param p1, "priority"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/QueueCommand;->priority:I

    .line 35
    return-void
.end method
