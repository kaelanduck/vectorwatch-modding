.class public Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "PushDataCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

.field private appId:I

.field private commandBytes:[B

.field private dataId:I

.field private elementId:I

.field private ttl:I

.field private ttlType:I

.field private watchfaceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->log:Lorg/slf4j/Logger;

    .line 28
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_PUSH:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 216
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 41
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setUuid(Ljava/util/UUID;)V

    .line 42
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 126
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v3, v4, v6, v7}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appId:I

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->watchfaceId:I

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->elementId:I

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 172
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->ttl:I

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->dataId:I

    .line 174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->ttlType:I

    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 176
    .local v0, "arraySize":I
    new-array v3, v0, [B

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->commandBytes:[B

    .line 177
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->commandBytes:[B

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readByteArray([B)V

    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 181
    .local v1, "hasUuid":I
    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 183
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->setUuid(Ljava/util/UUID;)V

    .line 187
    .end local v2    # "uuid":Ljava/lang/String;
    :goto_1
    return-void

    .line 133
    .end local v0    # "arraySize":I
    .end local v1    # "hasUuid":I
    :pswitch_0
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 136
    :pswitch_1
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 139
    :pswitch_2
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_LINE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 142
    :pswitch_3
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 145
    :pswitch_4
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_RECTANGLE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 148
    :pswitch_5
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NORMAL_HAND:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 151
    :pswitch_6
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_BITMAP_HAND:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 154
    :pswitch_7
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_MULTIPLE_LINES:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 157
    :pswitch_8
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_COMPLICATION:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 160
    :pswitch_9
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_LIST:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 163
    :pswitch_a
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_GAUGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 166
    :pswitch_b
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 169
    :pswitch_c
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NUM:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 185
    .restart local v0    # "arraySize":I
    .restart local v1    # "hasUuid":I
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "UUID missing in TIME SYNC"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 46
    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->log:Lorg/slf4j/Logger;

    const-string v5, "BLE COMMANDS: push_data"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 47
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->commandBytes:[B

    if-nez v4, :cond_0

    .line 50
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMANDS: command bytes are null."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 86
    :goto_0
    return-object v2

    .line 54
    :cond_0
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    if-nez v4, :cond_1

    .line 55
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMANDS: no app element type found."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    sget-object v5, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_COMPLICATION:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    if-ne v4, v5, :cond_3

    const/4 v1, 0x1

    .line 62
    .local v1, "isTextElement":Z
    :goto_1
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->commandBytes:[B

    array-length v4, v4

    add-int/lit8 v5, v4, 0x10

    if-eqz v1, :cond_4

    const/16 v4, 0x8

    :goto_2
    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 64
    .local v0, "bytes":Ljava/nio/ByteBuffer;
    iget v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appId:I

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 65
    iget v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->watchfaceId:I

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 66
    iget v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->elementId:I

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 67
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/Constants$AppElementType;->getVal()I

    move-result v4

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 68
    iget v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->ttl:I

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 69
    iget v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->dataId:I

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 70
    iget v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->ttlType:I

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 72
    if-eqz v1, :cond_2

    .line 74
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 75
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 76
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 77
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 78
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 79
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 80
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 81
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 83
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->commandBytes:[B

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 85
    new-instance v3, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v4

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .end local v0    # "bytes":Ljava/nio/ByteBuffer;
    .end local v1    # "isTextElement":Z
    :cond_3
    move v1, v3

    .line 59
    goto :goto_1

    .restart local v1    # "isTextElement":Z
    :cond_4
    move v4, v3

    .line 62
    goto :goto_2
.end method

.method public setAppElementType(Lcom/vectorwatch/android/utils/Constants$AppElementType;)V
    .locals 0
    .param p1, "appElementType"    # Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 101
    return-void
.end method

.method public setAppId(I)V
    .locals 0
    .param p1, "appId"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appId:I

    .line 97
    return-void
.end method

.method public setCommandBytes([B)V
    .locals 0
    .param p1, "commandBytes"    # [B

    .prologue
    .line 120
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->commandBytes:[B

    .line 121
    return-void
.end method

.method public setDataId(I)V
    .locals 0
    .param p1, "dataId"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->dataId:I

    .line 93
    return-void
.end method

.method public setElementId(I)V
    .locals 0
    .param p1, "elementId"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->elementId:I

    .line 113
    return-void
.end method

.method public setTtl(I)V
    .locals 0
    .param p1, "ttl"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->ttl:I

    .line 105
    return-void
.end method

.method public setTtlType(I)V
    .locals 0
    .param p1, "ttlType"    # I

    .prologue
    .line 116
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->ttlType:I

    .line 117
    return-void
.end method

.method public setWatchfaceId(I)V
    .locals 0
    .param p1, "watchfaceId"    # I

    .prologue
    .line 108
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->watchfaceId:I

    .line 109
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 196
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 197
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->watchfaceId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 198
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->elementId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 199
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->appElementType:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$AppElementType;->getVal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 200
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->ttl:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 201
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->dataId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 202
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->ttlType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 203
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->commandBytes:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 204
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->commandBytes:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 207
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 208
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 209
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/PushDataCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 213
    :goto_0
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
