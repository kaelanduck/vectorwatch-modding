.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncAppUninstallCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private installType:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

.field private removedAppId:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->log:Lorg/slf4j/Logger;

    .line 27
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_APP_INSTALL:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 28
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 103
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 34
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->setUuid(Ljava/util/UUID;)V

    .line 35
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 68
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->removedAppId:I

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Constants;->getMessageInstallTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->installType:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 75
    .local v0, "hasUuid":I
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->setUuid(Ljava/util/UUID;)V

    .line 81
    .end local v1    # "uuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 79
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID missing in TIME SYNC"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "BLE COMMANDS: sync_app_uninstall_details_command"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 41
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    const/4 v2, 0x5

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 46
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->installType:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->getVal()I

    move-result v2

    int-to-byte v2, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 48
    iget v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->removedAppId:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 50
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    return-object v1
.end method

.method public setInstallType(Lcom/vectorwatch/android/utils/Constants$MessageInstallType;)V
    .locals 0
    .param p1, "installType"    # Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->installType:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    .line 63
    return-void
.end method

.method public setRemovedAppId(I)V
    .locals 0
    .param p1, "removedAppId"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->removedAppId:I

    .line 59
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 90
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->removedAppId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->installType:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->getVal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncAppUninstallCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
