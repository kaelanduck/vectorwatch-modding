.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncActivityOptionsCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private activityReminder:Z

.field private goalAchievementAlert:Z

.field private goalAlmostAlert:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->log:Lorg/slf4j/Logger;

    .line 29
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 148
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 37
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->setUuid(Ljava/util/UUID;)V

    .line 38
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 10
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 101
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v6, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {p0, v3, v6, v8, v9}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 104
    .local v0, "flag":I
    if-ne v0, v4, :cond_1

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->goalAchievementAlert:Z

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 107
    if-ne v0, v4, :cond_2

    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->goalAlmostAlert:Z

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 110
    if-ne v0, v4, :cond_0

    move v5, v4

    :cond_0
    iput-boolean v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->activityReminder:Z

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 114
    .local v1, "hasUuid":I
    if-ne v1, v4, :cond_3

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->setUuid(Ljava/util/UUID;)V

    .line 120
    .end local v2    # "uuid":Ljava/lang/String;
    :goto_2
    return-void

    .end local v1    # "hasUuid":I
    :cond_1
    move v3, v5

    .line 104
    goto :goto_0

    :cond_2
    move v3, v5

    .line 107
    goto :goto_1

    .line 118
    .restart local v1    # "hasUuid":I
    :cond_3
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "UUID missing in TIME SYNC"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private getWarningData()[B
    .locals 8

    .prologue
    .line 55
    const/4 v5, 0x3

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 57
    .local v1, "dataBytes":Ljava/nio/ByteBuffer;
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 58
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_ACTIVITY_ALERTS:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v5

    int-to-byte v5, v5

    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 61
    const/4 v2, 0x0

    .line 62
    .local v2, "goalAchievement":B
    iget-boolean v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->goalAchievementAlert:Z

    if-eqz v5, :cond_0

    .line 63
    const/4 v2, 0x1

    .line 67
    :cond_0
    const/4 v3, 0x0

    .line 68
    .local v3, "goalAlmost":B
    iget-boolean v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->goalAlmostAlert:Z

    if-eqz v5, :cond_1

    .line 69
    const/4 v3, 0x2

    .line 73
    :cond_1
    const/4 v0, 0x0

    .line 74
    .local v0, "activityRem":B
    iget-boolean v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->activityReminder:Z

    if-eqz v5, :cond_2

    .line 75
    const/4 v0, 0x4

    .line 78
    :cond_2
    or-int v5, v2, v3

    or-int/2addr v5, v0

    int-to-byte v4, v5

    .line 79
    .local v4, "value":B
    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "BLE SYNC ACTIVITY: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 83
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    return-object v5
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMAND: sync_activity_options"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->getWarningData()[B

    move-result-object v2

    .line 46
    .local v2, "settings":[B
    new-instance v1, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 47
    .local v1, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    return-object v0
.end method

.method public setActivityReminder(Z)V
    .locals 0
    .param p1, "activityReminder"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->activityReminder:Z

    .line 97
    return-void
.end method

.method public setGoalAchievementAlert(Z)V
    .locals 0
    .param p1, "goalAchievementAlert"    # Z

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->goalAchievementAlert:Z

    .line 89
    return-void
.end method

.method public setGoalAlmostAlert(Z)V
    .locals 0
    .param p1, "goalAlmostAlert"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->goalAlmostAlert:Z

    .line 93
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 129
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->goalAchievementAlert:Z

    if-eqz v3, :cond_0

    move v0, v1

    .line 130
    .local v0, "flag":I
    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->goalAlmostAlert:Z

    if-eqz v3, :cond_1

    move v0, v1

    .line 133
    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 135
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->activityReminder:Z

    if-eqz v3, :cond_2

    move v0, v1

    .line 136
    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 140
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncActivityOptionsCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    :goto_3
    return-void

    .end local v0    # "flag":I
    :cond_0
    move v0, v2

    .line 129
    goto :goto_0

    .restart local v0    # "flag":I
    :cond_1
    move v0, v2

    .line 132
    goto :goto_1

    :cond_2
    move v0, v2

    .line 135
    goto :goto_2

    .line 143
    :cond_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3
.end method
