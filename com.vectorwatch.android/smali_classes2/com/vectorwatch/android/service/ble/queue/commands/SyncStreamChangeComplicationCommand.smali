.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncStreamChangeComplicationCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->log:Lorg/slf4j/Logger;

    .line 28
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_CHANGE_COMPLICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    .line 29
    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 136
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 34
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->setUuid(Ljava/util/UUID;)V

    .line 35
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 76
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v3, v4, v6, v7}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 78
    new-instance v3, Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-direct {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;-><init>()V

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    .line 80
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setAppId(I)V

    .line 81
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setWatchfaceId(B)V

    .line 82
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementId(B)V

    .line 83
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Constants;->getElementTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$ElementType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V

    .line 84
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setLength(B)V

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 88
    .local v0, "flag":I
    if-lez v0, :cond_0

    .line 89
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setValue(Ljava/lang/String;)V

    .line 95
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 96
    .local v1, "hasUuid":I
    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 98
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->setUuid(Ljava/util/UUID;)V

    .line 102
    .end local v2    # "uuid":Ljava/lang/String;
    :goto_1
    return-void

    .line 91
    .end local v1    # "hasUuid":I
    :cond_0
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 100
    .restart local v1    # "hasUuid":I
    :cond_1
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "UUID missing in TIME SYNC"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMANDS: sync_stream_change_complication"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 40
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    if-nez v3, :cond_0

    .line 64
    :goto_0
    return-object v2

    .line 46
    :cond_0
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v1, v3

    .line 48
    .local v1, "dataValueLength":I
    :goto_1
    add-int/lit8 v3, v1, 0x7

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 50
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getAppId()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 51
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getWatchfaceId()B

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 52
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getElementId()B

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 53
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getElementType()Lcom/vectorwatch/android/utils/Constants$ElementType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$ElementType;->getVal()I

    move-result v3

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 54
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getLength()B

    move-result v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 55
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 56
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 59
    :cond_1
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SYNC_STREAM_COMPLICATION: app id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getAppId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " wf = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    .line 60
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getWatchfaceId()B

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " eid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getElementId()B

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 59
    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 62
    new-instance v3, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v4

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 46
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "dataValueLength":I
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method public setComplication(Lcom/vectorwatch/android/models/ChangeComplicationModel;)V
    .locals 0
    .param p1, "complication"    # Lcom/vectorwatch/android/models/ChangeComplicationModel;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    .line 71
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v3, 0x0

    .line 111
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getAppId()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getWatchfaceId()B

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 113
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getElementId()B

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 114
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getElementType()Lcom/vectorwatch/android/utils/Constants$ElementType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$ElementType;->getVal()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 115
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getLength()B

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 117
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->complication:Lcom/vectorwatch/android/models/ChangeComplicationModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/ChangeComplicationModel;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "value":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 120
    sget-object v1, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE COMMANDS: sync_stream_change_complication: Null value"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 127
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 128
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncStreamChangeComplicationCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    :goto_1
    return-void

    .line 122
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method
