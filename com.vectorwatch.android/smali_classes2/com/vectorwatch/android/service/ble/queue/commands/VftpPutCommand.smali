.class public Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "VftpPutCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private compressedSize:S

.field private fileId:I

.field private fileType:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

.field private isCompressed:Z

.field private isForced:Z

.field private msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

.field private realSize:S


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->log:Lorg/slf4j/Logger;

    .line 27
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_VFTP:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 151
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 39
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->setUuid(Ljava/util/UUID;)V

    .line 40
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 106
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v2, v5, v6, v7}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Constants;->getVftpMessageTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    iput-short v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->realSize:S

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-ne v2, v3, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->isCompressed:Z

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-ne v2, v3, :cond_0

    move v4, v3

    :cond_0
    iput-boolean v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->isForced:Z

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    iput-short v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->compressedSize:S

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Constants;->getVftpFileTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->fileType:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->fileId:I

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 118
    .local v0, "hasUuid":I
    if-ne v0, v3, :cond_2

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "uuid":Ljava/lang/String;
    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->setUuid(Ljava/util/UUID;)V

    .line 124
    .end local v1    # "uuid":Ljava/lang/String;
    :goto_1
    return-void

    .end local v0    # "hasUuid":I
    :cond_1
    move v2, v4

    .line 110
    goto :goto_0

    .line 122
    .restart local v0    # "hasUuid":I
    :cond_2
    sget-object v2, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->log:Lorg/slf4j/Logger;

    const-string v3, "UUID missing in TIME SYNC"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 44
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMANDS: vftp_put"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 45
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    const/16 v3, 0xc

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 51
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v4

    int-to-byte v4, v4

    invoke-virtual {v0, v3, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 52
    iget-short v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->realSize:S

    invoke-virtual {v0, v5, v3}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    .line 54
    const/4 v1, 0x0

    .line 56
    .local v1, "flags":B
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->isCompressed:Z

    if-eqz v3, :cond_0

    .line 57
    int-to-byte v1, v5

    .line 60
    :cond_0
    iget-boolean v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->isForced:Z

    if-eqz v3, :cond_1

    .line 61
    or-int/lit8 v3, v1, 0x2

    int-to-byte v1, v3

    .line 64
    :cond_1
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 65
    const/4 v3, 0x4

    iget-short v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->compressedSize:S

    invoke-virtual {v0, v3, v4}, Ljava/nio/ByteBuffer;->putShort(IS)Ljava/nio/ByteBuffer;

    .line 66
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->fileType:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->getVal()I

    move-result v4

    int-to-byte v4, v4

    invoke-virtual {v0, v3, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 67
    const/4 v3, 0x7

    iget v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->fileId:I

    invoke-virtual {v0, v3, v4}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 69
    new-instance v3, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v4

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    return-object v2
.end method

.method public setCompressedSize(S)V
    .locals 0
    .param p1, "compressedSize"    # S

    .prologue
    .line 92
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->compressedSize:S

    .line 93
    return-void
.end method

.method public setFileId(I)V
    .locals 0
    .param p1, "fileId"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->fileId:I

    .line 101
    return-void
.end method

.method public setFileType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V
    .locals 0
    .param p1, "fileType"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->fileType:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 97
    return-void
.end method

.method public setIsCompressed(Z)V
    .locals 0
    .param p1, "isCompressed"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->isCompressed:Z

    .line 85
    return-void
.end method

.method public setIsForced(Z)V
    .locals 0
    .param p1, "isForced"    # Z

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->isForced:Z

    .line 89
    return-void
.end method

.method public setMsgType(Lcom/vectorwatch/android/utils/Constants$VftpMessageType;)V
    .locals 0
    .param p1, "msgType"    # Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 77
    return-void
.end method

.method public setRealSize(S)V
    .locals 0
    .param p1, "realSize"    # S

    .prologue
    .line 80
    iput-short p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->realSize:S

    .line 81
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 133
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->realSize:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 135
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->isCompressed:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 136
    iget-boolean v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->isForced:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 137
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->compressedSize:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->fileType:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->getVal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    iget v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->fileId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 142
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 143
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 144
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpPutCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 148
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 135
    goto :goto_0

    :cond_1
    move v0, v2

    .line 136
    goto :goto_1

    .line 146
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2
.end method
