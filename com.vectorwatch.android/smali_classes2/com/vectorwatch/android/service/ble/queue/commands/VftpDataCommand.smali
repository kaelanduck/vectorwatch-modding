.class public Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "VftpDataCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

.field private msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->log:Lorg/slf4j/Logger;

    .line 29
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_VFTP:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 128
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 36
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->setUuid(Ljava/util/UUID;)V

    .line 37
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 10
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 76
    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v7, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {p0, v4, v7, v8, v9}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Constants;->getVftpMessageTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    move-result-object v4

    iput-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 79
    new-instance v4, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-direct {v4}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;-><init>()V

    iput-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 80
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    invoke-static {v7}, Lcom/vectorwatch/android/utils/Constants;->getVftpFileTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V

    .line 81
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setFileId(Ljava/lang/Integer;)V

    .line 82
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    int-to-short v7, v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setUncompressedSize(Ljava/lang/Short;)V

    .line 83
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    int-to-short v7, v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setCompressedSize(Ljava/lang/Short;)V

    .line 84
    iget-object v7, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v4

    if-ne v4, v5, :cond_1

    move v4, v5

    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v7, v4}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setIsCompressed(Ljava/lang/Boolean;)V

    .line 85
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v7

    if-ne v7, v5, :cond_0

    move v6, v5

    :cond_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setIsForced(Ljava/lang/Boolean;)V

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 87
    .local v1, "contentSize":I
    new-array v0, v1, [B

    .line 88
    .local v0, "content":[B
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 89
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v4, v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->setFileContent([B)V

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 93
    .local v2, "hasUuid":I
    if-ne v2, v5, :cond_2

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 95
    .local v3, "uuid":Ljava/lang/String;
    invoke-static {v3}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->setUuid(Ljava/util/UUID;)V

    .line 99
    .end local v3    # "uuid":Ljava/lang/String;
    :goto_1
    return-void

    .end local v0    # "content":[B
    .end local v1    # "contentSize":I
    .end local v2    # "hasUuid":I
    :cond_1
    move v4, v6

    .line 84
    goto :goto_0

    .line 97
    .restart local v0    # "content":[B
    .restart local v1    # "contentSize":I
    .restart local v2    # "hasUuid":I
    :cond_2
    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->log:Lorg/slf4j/Logger;

    const-string v5, "UUID missing in TIME SYNC"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->log:Lorg/slf4j/Logger;

    const-string v6, "BLE COMMANDS: vftp_data"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 42
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v5}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v5}, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->getPackageCount(I)I

    move-result v3

    .line 45
    .local v3, "numberOfPackages":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 46
    sget-object v5, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "VFTP - pack_file_section: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ". Full content size = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 47
    invoke-virtual {v7}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v7

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 46
    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 49
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v5}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v5

    array-length v5, v5

    invoke-static {v1, v5}, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->getDataPackageSize(II)I

    move-result v4

    .line 51
    .local v4, "size":I
    add-int/lit8 v5, v4, 0x2

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 53
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v5

    int-to-byte v5, v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 54
    int-to-short v5, v1

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 55
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v5}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v5

    invoke-static {v1, v5}, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->getFileSection(I[B)[B

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 57
    new-instance v5, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v6

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v4    # "size":I
    :cond_0
    return-object v2
.end method

.method public setFile(Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;)V
    .locals 0
    .param p1, "file"    # Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    .line 67
    return-void
.end method

.method public setMsgType(Lcom/vectorwatch/android/utils/Constants$VftpMessageType;)V
    .locals 0
    .param p1, "msgType"    # Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 71
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->msgType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getVal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->getVal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getUncompressedSize()Ljava/lang/Short;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getCompressedSize()Ljava/lang/Short;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getIsCompressed()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getIsForced()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 115
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v0

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->file:Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getFileContent()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 119
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 120
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/VftpDataCommand;->getUuid()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 125
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 113
    goto :goto_0

    :cond_1
    move v0, v2

    .line 114
    goto :goto_1

    .line 123
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2
.end method
