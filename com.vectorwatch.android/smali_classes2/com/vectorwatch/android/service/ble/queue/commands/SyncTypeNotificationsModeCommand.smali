.class public Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;
.super Lcom/vectorwatch/android/service/ble/queue/BaseCommand;
.source "SyncTypeNotificationsModeCommand.java"


# static fields
.field private static final BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;",
            ">;"
        }
    .end annotation
.end field

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private notificationMode:Lcom/vectorwatch/android/utils/Constants$NotificationMode;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->log:Lorg/slf4j/Logger;

    .line 30
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_SETTINGS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;-><init>(SS)V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    .line 101
    new-instance v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>()V

    .line 36
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->setUuid(Ljava/util/UUID;)V

    .line 37
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 66
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->BASE:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    sget-object v4, Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;->MEDIUM:Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {p0, v3, v4, v6, v7}, Lcom/vectorwatch/android/service/ble/queue/BaseCommand;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;Lcom/vectorwatch/android/service/ble/queue/BaseCommand$Priority;J)V

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 69
    .local v1, "mode":I
    invoke-static {v1}, Lcom/vectorwatch/android/utils/Constants;->getNotificationModeFromValue(I)Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    move-result-object v3

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->notificationMode:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 73
    .local v0, "hasUuid":I
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 75
    .local v2, "uuid":Ljava/lang/String;
    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->setUuid(Ljava/util/UUID;)V

    .line 79
    .end local v2    # "uuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 77
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "UUID missing in SYNC TYPE NOTIFICATION MODE"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public getTransferMessages()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->log:Lorg/slf4j/Logger;

    const-string v4, "BLE COMMAND: sync_activity_options"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 43
    const/4 v3, 0x3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 45
    .local v0, "dataBytes":Ljava/nio/ByteBuffer;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 46
    sget-object v3, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_NOTIFICATIONS_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$SettingsType;->getValue()I

    move-result v3

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 48
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->notificationMode:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    invoke-virtual {v3}, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->getValue()I

    move-result v3

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 50
    sget-object v3, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOTIFICATIONS DISPLAY: Command for watch = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-static {v5}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 52
    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->getCommandBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>(Lcom/vectorwatch/android/service/ble/messages/BaseMessage;[B)V

    .line 53
    .local v2, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    return-object v1
.end method

.method public setNotificationMode(Lcom/vectorwatch/android/utils/Constants$NotificationMode;)V
    .locals 0
    .param p1, "mode"    # Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->notificationMode:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    .line 62
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 88
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->notificationMode:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->getValue()I

    move-result v0

    .line 89
    .local v0, "mode":I
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 93
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/queue/commands/SyncTypeNotificationsModeCommand;->getUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
