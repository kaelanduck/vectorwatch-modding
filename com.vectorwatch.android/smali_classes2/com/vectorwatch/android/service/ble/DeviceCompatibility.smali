.class public Lcom/vectorwatch/android/service/ble/DeviceCompatibility;
.super Ljava/lang/Object;
.source "DeviceCompatibility.java"


# static fields
.field public static final HTC_M8:Ljava/lang/String; = "HTC One_M8"

.field private static final HTC_M8_OS_VERSION_PROBLEM:Ljava/lang/String; = "5.0.1"

.field public static final NOTE_4:Ljava/lang/String; = "SM-N910F"

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/DeviceCompatibility;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fixConnectProblems(Landroid/bluetooth/BluetoothAdapter;)V
    .locals 5
    .param p0, "bluetoothAdapter"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 52
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 53
    .local v1, "model":Ljava/lang/String;
    sget-object v2, Lcom/vectorwatch/android/service/ble/DeviceCompatibility;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Device model:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 54
    const-string v2, "HTC One_M8"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "5.0.1"

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    if-eqz p0, :cond_0

    .line 57
    sget-object v2, Lcom/vectorwatch/android/service/ble/DeviceCompatibility;->log:Lorg/slf4j/Logger;

    const-string v3, "custom fix for  HTC One_M8 5.0.1"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/vectorwatch/android/service/ble/DeviceCompatibility$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/DeviceCompatibility$1;-><init>()V

    .line 64
    .local v0, "dummyCallback":Landroid/bluetooth/BluetoothAdapter$LeScanCallback;
    invoke-virtual {p0, v0}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    .line 66
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    invoke-virtual {p0, v0}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 72
    .end local v0    # "dummyCallback":Landroid/bluetooth/BluetoothAdapter$LeScanCallback;
    :cond_0
    return-void

    .line 67
    .restart local v0    # "dummyCallback":Landroid/bluetooth/BluetoothAdapter$LeScanCallback;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static refreshDeviceCache(Landroid/bluetooth/BluetoothGatt;)Z
    .locals 7
    .param p0, "gatt"    # Landroid/bluetooth/BluetoothGatt;

    .prologue
    const/4 v0, 0x0

    .line 32
    move-object v1, p0

    .line 33
    .local v1, "localBluetoothGatt":Landroid/bluetooth/BluetoothGatt;
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "refresh"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 34
    .local v3, "localMethod":Ljava/lang/reflect/Method;
    if-eqz v3, :cond_0

    .line 35
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 42
    .end local v3    # "localMethod":Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    return v0

    .line 38
    :catch_0
    move-exception v2

    .line 39
    .local v2, "localException":Ljava/lang/Exception;
    sget-object v4, Lcom/vectorwatch/android/service/ble/DeviceCompatibility;->log:Lorg/slf4j/Logger;

    const-string v5, "An exception occured while refreshing device"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method
