.class public Lcom/vectorwatch/android/service/ble/NotificationsManager;
.super Ljava/lang/Object;
.source "NotificationsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;
    }
.end annotation


# static fields
.field private static final FREE:I = 0x0

.field private static final IN_USE_ON_WATCH:I = 0x1

.field private static final SIZE_NOTIFICATION_QUEUE:I = 0x64

.field private static final log:Lorg/slf4j/Logger;

.field private static notifManagerInstance:Lcom/vectorwatch/android/service/ble/NotificationsManager;


# instance fields
.field private crtIndex:I

.field private notificationQueue:[Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

.field private notificationQueueStatus:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notifManagerInstance:Lcom/vectorwatch/android/service/ble/NotificationsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->setUpQueue()V

    .line 36
    return-void
.end method

.method private addNotificationInQueue(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Ljava/util/List;
    .locals 6
    .param p1, "notificationInfo"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v0, "notificationsToSend":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    sget-object v1, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notifications Container - Adding Container ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 179
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v1

    if-nez v1, :cond_0

    .line 181
    invoke-virtual {p1, v4}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->setUniqueId(I)V

    .line 183
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueue:[Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    aput-object p1, v1, v4

    .line 184
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueueStatus:[B

    aput-byte v5, v1, v4

    .line 206
    :goto_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    return-object v0

    .line 189
    :cond_0
    iget v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    if-nez v1, :cond_1

    .line 190
    iget v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    .line 194
    :cond_1
    iget v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    invoke-virtual {p1, v1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->setUniqueId(I)V

    .line 195
    sget-object v1, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notifications - New notification id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getBtIntId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 198
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueue:[Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    iget v2, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    aput-object p1, v1, v2

    .line 199
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueueStatus:[B

    iget v2, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    aput-byte v5, v1, v2

    .line 202
    iget v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    .line 203
    iget v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    rem-int/lit8 v1, v1, 0x64

    iput v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    goto :goto_0
.end method

.method private dismissGlobalPlaceholderIfExists(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V
    .locals 9
    .param p1, "notificationInfo"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .prologue
    .line 319
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->containedInListOfValues(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    move-result-object v8

    .line 321
    .local v8, "appName":Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;
    sget-object v1, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->NOT_FOUND:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    if-ne v8, v1, :cond_1

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getStbnKey()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->getSpecialDismissNotification(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 326
    .local v7, "dismissKey":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 329
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v1

    .line 330
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 331
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 332
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getStatusBarNotificationId()I

    move-result v5

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getStbnTag()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;-><init>(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 333
    .local v0, "dismissGeneralPlaceholder":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    sget-object v1, Lcom/vectorwatch/android/service/nls/NLService;->reference:Lcom/vectorwatch/android/service/nls/NLService;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/service/nls/NLService;->cancelNotification(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    goto :goto_0
.end method

.method private dismissGroup(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Ljava/util/List;
    .locals 7
    .param p1, "notRequestFromWatch"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v0

    .line 375
    .local v0, "containerId":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 377
    .local v3, "notificationsToDismiss":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v4, 0x64

    if-ge v1, v4, :cond_3

    .line 378
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueue:[Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    aget-object v2, v4, v1

    .line 379
    .local v2, "notificationFromQueue":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    if-eqz v2, :cond_2

    .line 380
    sget-object v4, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Notifications - i, id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " | "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getBtIntId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getBtIntId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 382
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueueStatus:[B

    aget-byte v4, v4, v1

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 384
    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    .line 385
    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 387
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueueStatus:[B

    const/4 v5, 0x0

    aput-byte v5, v4, v1

    .line 389
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v4

    invoke-virtual {v2, v4}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->setType(S)V

    .line 390
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 391
    sget-object v4, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Notifications - Inside Remove notification: containerId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " |"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 392
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 391
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 377
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 398
    .end local v2    # "notificationFromQueue":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    :cond_3
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->dismissNotificationFromPhoneBar(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    .line 399
    return-object v3
.end method

.method private dismissNotificationFromPhoneBar(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V
    .locals 3
    .param p1, "notificationInfo"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .prologue
    .line 303
    sget-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Notifications Container - DISMISS from BAR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 305
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->reference:Lcom/vectorwatch/android/service/nls/NLService;

    if-eqz v0, :cond_0

    .line 306
    sget-object v0, Lcom/vectorwatch/android/service/nls/NLService;->reference:Lcom/vectorwatch/android/service/nls/NLService;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/service/nls/NLService;->cancelNotification(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    .line 308
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->dismissGlobalPlaceholderIfExists(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    .line 310
    :cond_0
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/vectorwatch/android/service/ble/NotificationsManager;
    .locals 2

    .prologue
    .line 39
    const-class v1, Lcom/vectorwatch/android/service/ble/NotificationsManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notifManagerInstance:Lcom/vectorwatch/android/service/ble/NotificationsManager;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;

    invoke-direct {v0}, Lcom/vectorwatch/android/service/ble/NotificationsManager;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notifManagerInstance:Lcom/vectorwatch/android/service/ble/NotificationsManager;

    .line 42
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notifManagerInstance:Lcom/vectorwatch/android/service/ble/NotificationsManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getSpecialDismissNotification(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "notificationKey"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x5

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 342
    if-nez p1, :cond_1

    .line 365
    :cond_0
    :goto_0
    return-object v2

    .line 345
    :cond_1
    const-string v3, "|"

    invoke-static {v3}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 346
    .local v1, "keyParts":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 349
    sget-object v3, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Notifications Container Values "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 350
    array-length v5, v1

    move v3, v4

    :goto_1
    if-ge v3, v5, :cond_2

    aget-object v0, v1, v3

    .line 351
    .local v0, "keyPart":Ljava/lang/String;
    sget-object v6, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Notifications Container Values "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 350
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 354
    .end local v0    # "keyPart":Ljava/lang/String;
    :cond_2
    array-length v3, v1

    if-gt v3, v9, :cond_0

    array-length v3, v1

    if-lt v3, v9, :cond_0

    .line 357
    const-string v3, "null"

    const/4 v5, 0x3

    aget-object v5, v1, v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 362
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "|null|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 363
    .local v2, "newGlobalAppDismissKey":Ljava/lang/String;
    sget-object v3, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Notifications Container New dismiss key "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private isOtherFromGroupActive(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Z
    .locals 9
    .param p1, "notification"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .prologue
    const/4 v5, 0x1

    .line 265
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "containerId":Ljava/lang/String;
    const/4 v4, 0x0

    .line 268
    .local v4, "specialAppForDismiss":Z
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->containedInListOfValues(Ljava/lang/String;)Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    move-result-object v0

    .line 269
    .local v0, "appName":Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;
    sget-object v6, Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;->NOT_FOUND:Lcom/vectorwatch/android/service/ble/NotificationsManager$SpecialDismissForApps;

    if-eq v0, v6, :cond_0

    .line 270
    const/4 v4, 0x1

    .line 273
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/16 v6, 0x64

    if-ge v2, v6, :cond_3

    .line 274
    iget-object v6, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueue:[Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    aget-object v3, v6, v2

    .line 275
    .local v3, "notificationFromQueue":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    if-eqz v3, :cond_2

    .line 277
    sget-object v6, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Notifications Container  - check "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueueStatus:[B

    aget-byte v8, v8, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 279
    iget-object v6, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueueStatus:[B

    aget-byte v6, v6, v2

    if-ne v6, v5, :cond_2

    .line 281
    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 282
    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 295
    .end local v3    # "notificationFromQueue":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    :goto_1
    return v5

    .line 287
    .restart local v3    # "notificationFromQueue":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    :cond_1
    if-eqz v1, :cond_2

    .line 288
    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 289
    sget-object v6, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Notifications - Other notifications with Container Id present "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 273
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 295
    .end local v3    # "notificationFromQueue":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private setUpQueue()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x64

    .line 85
    iput v3, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->crtIndex:I

    .line 87
    new-array v1, v2, [B

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueueStatus:[B

    .line 89
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 90
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueueStatus:[B

    aput-byte v3, v1, v0

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    new-array v1, v2, [Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueue:[Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .line 94
    return-void
.end method


# virtual methods
.method public getNotification(I)Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 97
    sget-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getNotification with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 98
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-lt p1, v0, :cond_1

    .line 99
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Incorrect ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 100
    const/4 v0, 0x0

    .line 103
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueue:[Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public processNotification(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Ljava/util/List;
    .locals 5
    .param p1, "notificationInfo"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 114
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v2

    if-nez v2, :cond_0

    .line 116
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 117
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 116
    invoke-static {v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->getContactName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "contactName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->setTitle(Ljava/lang/String;)V

    .line 121
    .end local v0    # "contactName":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Notifications - notification process type  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v2

    const/16 v3, 0x9

    if-eq v2, v3, :cond_1

    .line 123
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v2

    const/4 v3, 0x7

    if-eq v2, v3, :cond_1

    .line 124
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 125
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 126
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_1

    .line 128
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->addNotificationInQueue(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Ljava/util/List;

    move-result-object v2

    .line 132
    :goto_0
    return-object v2

    .line 130
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Notifications - Trying to remove notification. Received notification type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 131
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getType()S

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 130
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 132
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->removeNotificationsFromQueueReceivedFromAndroid(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method public processNotificationDetailsRequest(Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;)Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;
    .locals 11
    .param p1, "notificationRequest"    # Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;

    .prologue
    .line 144
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->getIntId()I

    move-result v8

    .line 145
    .local v8, "id":I
    invoke-virtual {p0, v8}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->getNotification(I)Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    move-result-object v9

    .line 147
    .local v9, "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    if-nez v9, :cond_0

    .line 149
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    const/4 v1, 0x4

    const-string v2, "Warning"

    const-string v3, "Please report to support"

    const-string v4, "com.package"

    const/16 v5, 0x6f

    const-string v6, "dummmyTag"

    const-string v7, "dummyKey"

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;-><init>(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 151
    .local v0, "dummyNotifications":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->getIntId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->setUniqueId(I)V

    .line 153
    new-instance v10, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;

    .line 154
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->getRequestInfoNeeded()Ljava/util/List;

    move-result-object v1

    invoke-direct {v10, v1, v0}, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;-><init>(Ljava/util/List;Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    .line 165
    .end local v0    # "dummyNotifications":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    .local v10, "notificationDetails":Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;
    :goto_0
    return-object v10

    .line 158
    .end local v10    # "notificationDetails":Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notifications - Asking for details for notification: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with package name (app identifier)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 159
    invoke-virtual {v9}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 158
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 162
    new-instance v10, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;

    .line 163
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;->getRequestInfoNeeded()Ljava/util/List;

    move-result-object v1

    invoke-direct {v10, v1, v9}, Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;-><init>(Ljava/util/List;Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    .line 165
    .restart local v10    # "notificationDetails":Lcom/vectorwatch/android/service/ble/messages/NotificationDetailsMessage;
    goto :goto_0
.end method

.method public removeNotificationsFromQueueReceivedFromAndroid(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Ljava/util/List;
    .locals 5
    .param p1, "notificationInfo"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .local v1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/messages/BtMessage;>;"
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getContainerId()Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "containerId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 254
    :goto_0
    return-object v1

    .line 252
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Notifications Container - Remove notification: containerId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 253
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->dismissGroup(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public removeSingleNotificationsFromQueueReceivedFromWatch(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V
    .locals 4
    .param p1, "notificationInfo"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .prologue
    .line 219
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;->getBtIntId()I

    move-result v0

    .line 221
    .local v0, "notifId":I
    if-ltz v0, :cond_0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_2

    .line 222
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notifications ID out of bounds "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 233
    :cond_1
    :goto_0
    return-void

    .line 226
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/NotificationsManager;->notificationQueueStatus:[B

    const/4 v2, 0x0

    aput-byte v2, v1, v0

    .line 227
    sget-object v1, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notifications - Dismiss single notification with id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 229
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->isOtherFromGroupActive(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 230
    sget-object v1, Lcom/vectorwatch/android/service/ble/NotificationsManager;->log:Lorg/slf4j/Logger;

    const-string v2, "Dismiss also from bar because last one"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 231
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/NotificationsManager;->dismissNotificationFromPhoneBar(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V

    goto :goto_0
.end method
