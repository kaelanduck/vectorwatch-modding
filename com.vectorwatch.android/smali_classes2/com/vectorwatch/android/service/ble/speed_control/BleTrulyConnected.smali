.class public Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;
.super Ljava/lang/Object;
.source "BleTrulyConnected.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final BLE_NOT_CONNECTED:B = 0x0t

.field public static final BLE_TRULY_CONNECTED:B = 0x1t


# instance fields
.field private connectionNumber:S

.field private currentSpeed:B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->currentSpeed:B

    .line 19
    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1, "rawData"    # [B

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->currentSpeed:B

    .line 25
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    iput-short v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->connectionNumber:S

    .line 26
    return-void
.end method


# virtual methods
.method public duplicate(Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;)V
    .locals 1
    .param p1, "info"    # Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;

    .prologue
    .line 42
    iget-byte v0, p1, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->currentSpeed:B

    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->currentSpeed:B

    .line 43
    return-void
.end method

.method public getConnectionNumber()S
    .locals 1

    .prologue
    .line 34
    iget-short v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->connectionNumber:S

    return v0
.end method

.method public isTrulyConnected()Z
    .locals 1

    .prologue
    .line 38
    iget-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->currentSpeed:B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 46
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 47
    .local v0, "data":Ljava/nio/ByteBuffer;
    iget-byte v1, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->currentSpeed:B

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 48
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "currentSpeed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->currentSpeed:B

    if-nez v0, :cond_0

    const-string v0, "BLE NOT CONNECTED"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "connection count "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 53
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/speed_control/BleTrulyConnected;->getConnectionNumber()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 52
    :cond_0
    const-string v0, "BLE TRULY CONNECTED"

    goto :goto_0
.end method
