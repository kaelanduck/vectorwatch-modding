.class public Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;
.super Ljava/lang/Object;
.source "BleSpeedInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final BLE_SPEED_FAST:B = 0x2t

.field public static final BLE_SPEED_NONE:B = 0x0t

.field public static final BLE_SPEED_SLOW:B = 0x1t


# instance fields
.field private currentSpeed:B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;->currentSpeed:B

    .line 19
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "rawData"    # [B

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;->currentSpeed:B

    .line 24
    return-void
.end method


# virtual methods
.method public dublicate(Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;

    .prologue
    .line 31
    iget-byte v0, p1, Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;->currentSpeed:B

    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;->currentSpeed:B

    .line 32
    return-void
.end method

.method public getCurrentSpeed()B
    .locals 1

    .prologue
    .line 27
    iget-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;->currentSpeed:B

    return v0
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 35
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 36
    .local v0, "data":Ljava/nio/ByteBuffer;
    iget-byte v1, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;->currentSpeed:B

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 37
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "currentSpeed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v0, p0, Lcom/vectorwatch/android/service/ble/speed_control/BleSpeedInfo;->currentSpeed:B

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const-string v0, "Fast Speed"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "Slow Speed"

    goto :goto_0
.end method
