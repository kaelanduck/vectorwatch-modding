.class public Lcom/vectorwatch/android/service/ble/DefaultTransformer;
.super Ljava/lang/Object;
.source "DefaultTransformer.java"

# interfaces
.implements Lcom/vectorwatch/android/service/ble/Transformer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private final MAX_FRAME_LENGTH:I

.field private final MAX_PACKET_LENGTH:I

.field private final MAX_PAYLOAD_LENGTH:I

.field private final RX_BUFFER_SIZE:I

.field private final STATUS_LIST_SIZE:I

.field private lastAssignedID:B

.field private mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

.field private rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

.field private rxStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

.field private txStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;)V
    .locals 7
    .param p1, "workerHandler"    # Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    .prologue
    const/16 v2, 0x200

    const/16 v6, 0x40

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/16 v1, 0x14

    iput v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PACKET_LENGTH:I

    .line 63
    iput v6, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->STATUS_LIST_SIZE:I

    .line 64
    new-array v1, v6, [Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->txStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    .line 65
    new-array v1, v6, [Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    .line 70
    iput v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->RX_BUFFER_SIZE:I

    .line 71
    new-instance v1, Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;-><init>(I)V

    iput-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    .line 73
    const/4 v1, 0x0

    iput-byte v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->lastAssignedID:B

    .line 76
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    .line 78
    const/16 v1, 0x13

    iput v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PAYLOAD_LENGTH:I

    .line 79
    const v1, 0xffff

    const-wide v2, 0x40a9998000000000L    # 3276.75

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    sub-double/2addr v2, v4

    double-to-int v2, v2

    int-to-short v2, v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_FRAME_LENGTH:I

    .line 82
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v6, :cond_0

    .line 83
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->txStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object v2, v1, v0

    .line 84
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object v2, v1, v0

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_0
    return-void
.end method

.method private dispatchMessageToHandlerIfAlive(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V
    .locals 3
    .param p1, "type"    # Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    .param p2, "extra"    # Ljava/lang/Object;

    .prologue
    .line 602
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isHandlerThreadTerminating()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 603
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-static {p1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I

    move-result v2

    invoke-static {v1, v2, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 604
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 606
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private frameStatus(SS)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;
    .locals 3
    .param p1, "n"    # S
    .param p2, "frameLength"    # S

    .prologue
    .line 313
    if-le p1, p2, :cond_0

    .line 314
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Protocol - invalid number of packets: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is larger than the frame\'s "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 319
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    .line 325
    :goto_0
    return-object v0

    .line 320
    :cond_1
    if-nez p1, :cond_2

    .line 321
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->FIRST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    goto :goto_0

    .line 322
    :cond_2
    add-int/lit8 v0, p2, -0x1

    if-ne p1, v0, :cond_3

    .line 323
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->LAST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    goto :goto_0

    .line 325
    :cond_3
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->MORE_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    goto :goto_0
.end method

.method private getReceptionStatus(B)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;
    .locals 3
    .param p1, "receptionId"    # B

    .prologue
    .line 572
    if-ltz p1, :cond_0

    const/16 v0, 0x40

    if-lt p1, v0, :cond_1

    .line 573
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Protocol - invalid reception id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 576
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private getTransmissionId()B
    .locals 3

    .prologue
    .line 270
    iget-byte v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->lastAssignedID:B

    add-int/lit8 v1, v1, 0x1

    int-to-byte v0, v1

    .line 272
    .local v0, "i":B
    :goto_0
    const/16 v1, 0x40

    if-ge v0, v1, :cond_1

    .line 273
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->txStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aget-object v1, v1, v0

    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    if-ne v1, v2, :cond_0

    .line 274
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->txStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->LAST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object v2, v1, v0

    .line 275
    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->lastAssignedID:B

    move v1, v0

    .line 289
    :goto_1
    return v1

    .line 272
    :cond_0
    add-int/lit8 v1, v0, 0x1

    int-to-byte v0, v1

    goto :goto_0

    .line 280
    :cond_1
    rem-int/lit8 v1, v0, 0x40

    int-to-byte v0, v1

    .line 281
    :goto_2
    iget-byte v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->lastAssignedID:B

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_3

    .line 282
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->txStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aget-object v1, v1, v0

    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    if-ne v1, v2, :cond_2

    .line 283
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->txStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->LAST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object v2, v1, v0

    .line 284
    iput-byte v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->lastAssignedID:B

    move v1, v0

    .line 285
    goto :goto_1

    .line 281
    :cond_2
    add-int/lit8 v1, v0, 0x1

    int-to-byte v0, v1

    goto :goto_2

    .line 289
    :cond_3
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private invalidateRxBufferIfNeeded()V
    .locals 2

    .prologue
    .line 557
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->getReceptionId()B

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 559
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    const-string v1, "Protocol unpacking data - invalidating buffer for rx"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 561
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->invalidate()V

    .line 563
    :cond_0
    return-void
.end method

.method private pack([B)[B
    .locals 16
    .param p1, "rawData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/vectorwatch/android/events/FrameOverflowException;,
            Lcom/vectorwatch/android/events/ChannelBusyException;
        }
    .end annotation

    .prologue
    .line 187
    move-object/from16 v0, p1

    array-length v11, v0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_FRAME_LENGTH:I

    if-le v11, v12, :cond_0

    .line 188
    new-instance v11, Lcom/vectorwatch/android/events/FrameOverflowException;

    invoke-direct {v11}, Lcom/vectorwatch/android/events/FrameOverflowException;-><init>()V

    throw v11

    .line 191
    :cond_0
    sget-object v11, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Protocol pack method - unpacked data: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 193
    new-instance v4, Lcom/vectorwatch/android/service/ble/models/Packet;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PAYLOAD_LENGTH:I

    invoke-direct {v4, v11}, Lcom/vectorwatch/android/service/ble/models/Packet;-><init>(I)V

    .line 196
    .local v4, "packet":Lcom/vectorwatch/android/service/ble/models/Packet;
    const/4 v7, 0x0

    .line 197
    .local v7, "rawDataIndex":I
    const/4 v2, 0x0

    .line 198
    .local v2, "dataIndex":I
    const/4 v3, 0x1

    .line 199
    .local v3, "numPackets":S
    const/4 v5, 0x0

    .line 205
    .local v5, "packets":S
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->getTransmissionId()B

    move-result v9

    .line 206
    .local v9, "transmissionId":B
    const/4 v11, -0x1

    if-ne v9, v11, :cond_1

    .line 207
    new-instance v11, Lcom/vectorwatch/android/events/ChannelBusyException;

    invoke-direct {v11}, Lcom/vectorwatch/android/events/ChannelBusyException;-><init>()V

    throw v11

    .line 210
    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->getNumPacks([B)S

    move-result v3

    .line 213
    move-object/from16 v0, p1

    array-length v11, v0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PAYLOAD_LENGTH:I

    if-le v11, v12, :cond_3

    .line 215
    add-int/lit8 v11, v3, 0x2

    move-object/from16 v0, p1

    array-length v12, v0

    add-int/2addr v11, v12

    new-array v1, v11, [B

    .line 221
    .local v1, "data":[B
    :goto_0
    const/4 v11, 0x1

    if-ne v3, v11, :cond_4

    .line 222
    shl-int/lit8 v11, v9, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v3}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->frameStatus(SS)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->getValue()B

    move-result v12

    or-int/2addr v11, v12

    int-to-byte v11, v11

    iput-byte v11, v4, Lcom/vectorwatch/android/service/ble/models/Packet;->header:B

    .line 223
    const/4 v11, 0x0

    iget-object v12, v4, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    const/4 v13, 0x0

    move-object/from16 v0, p1

    array-length v14, v0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12, v13, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 225
    move-object/from16 v0, p1

    array-length v11, v0

    add-int/lit8 v10, v11, 0x1

    .line 226
    .local v10, "writeLength":I
    invoke-virtual {v4}, Lcom/vectorwatch/android/service/ble/models/Packet;->serialize()[B

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v11, v12, v1, v13, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 259
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->releaseTransmissionId(B)V

    .line 261
    return-object v1

    .line 218
    .end local v1    # "data":[B
    .end local v10    # "writeLength":I
    :cond_3
    move-object/from16 v0, p1

    array-length v11, v0

    add-int/lit8 v11, v11, 0x1

    new-array v1, v11, [B

    .restart local v1    # "data":[B
    goto :goto_0

    .line 228
    :cond_4
    shl-int/lit8 v11, v9, 0x2

    const/4 v12, 0x1

    int-to-short v6, v12

    .end local v5    # "packets":S
    .local v6, "packets":S
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v3}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->frameStatus(SS)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->getValue()B

    move-result v12

    or-int/2addr v11, v12

    int-to-byte v11, v11

    iput-byte v11, v4, Lcom/vectorwatch/android/service/ble/models/Packet;->header:B

    .line 231
    const/4 v11, 0x2

    invoke-static {v11}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    sget-object v12, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v11, v12}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v11

    const/4 v12, 0x0

    iget-object v13, v4, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static {v11, v12, v13, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 235
    move-object/from16 v0, p0

    iget v11, v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PAYLOAD_LENGTH:I

    add-int/lit8 v8, v11, -0x2

    .line 236
    .local v8, "readLength":I
    const/4 v11, 0x0

    iget-object v12, v4, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    const/4 v13, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12, v13, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 237
    add-int/2addr v7, v8

    .line 240
    move-object/from16 v0, p0

    iget v11, v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PAYLOAD_LENGTH:I

    add-int/lit8 v10, v11, 0x1

    .line 241
    .restart local v10    # "writeLength":I
    invoke-virtual {v4}, Lcom/vectorwatch/android/service/ble/models/Packet;->serialize()[B

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v11, v12, v1, v13, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 242
    add-int/2addr v2, v10

    move v5, v6

    .line 245
    .end local v6    # "packets":S
    .restart local v5    # "packets":S
    :goto_1
    move-object/from16 v0, p1

    array-length v11, v0

    if-ge v7, v11, :cond_2

    .line 246
    shl-int/lit8 v11, v9, 0x2

    add-int/lit8 v12, v5, 0x1

    int-to-short v6, v12

    .end local v5    # "packets":S
    .restart local v6    # "packets":S
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v3}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->frameStatus(SS)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->getValue()B

    move-result v12

    or-int/2addr v11, v12

    int-to-byte v11, v11

    iput-byte v11, v4, Lcom/vectorwatch/android/service/ble/models/Packet;->header:B

    .line 248
    move-object/from16 v0, p1

    array-length v11, v0

    sub-int v8, v11, v7

    .line 249
    move-object/from16 v0, p0

    iget v11, v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PAYLOAD_LENGTH:I

    if-lt v8, v11, :cond_5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PAYLOAD_LENGTH:I

    .line 250
    :cond_5
    iget-object v11, v4, Lcom/vectorwatch/android/service/ble/models/Packet;->payload:[B

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v7, v11, v12, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 251
    add-int/2addr v7, v8

    .line 253
    add-int/lit8 v10, v8, 0x1

    .line 254
    invoke-virtual {v4}, Lcom/vectorwatch/android/service/ble/models/Packet;->serialize()[B

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v12, v1, v2, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 255
    add-int/2addr v2, v10

    move v5, v6

    .end local v6    # "packets":S
    .restart local v5    # "packets":S
    goto :goto_1
.end method

.method private releaseTransmissionId(B)V
    .locals 3
    .param p1, "transmissionID"    # B

    .prologue
    .line 298
    if-ltz p1, :cond_0

    const/16 v0, 0x40

    if-lt p1, v0, :cond_1

    .line 299
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Protocol release transmission id - invalid transmission id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->txStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object v1, v0, p1

    .line 303
    return-void
.end method

.method private send(Landroid/bluetooth/BluetoothGattCharacteristic;[B)Ljava/util/List;
    .locals 9
    .param p1, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p2, "data"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            "[B)",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x14

    .line 143
    sget-object v6, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Trying to send on characteristic: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 145
    new-array v4, v5, [B

    .line 146
    .local v4, "serializedPacket":[B
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 148
    .local v3, "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    const/4 v0, 0x0

    .line 149
    .local v0, "index":I
    :goto_0
    array-length v6, p2

    if-ge v0, v6, :cond_1

    .line 150
    array-length v6, p2

    sub-int/2addr v6, v0

    if-ge v6, v5, :cond_0

    array-length v6, p2

    sub-int v1, v6, v0

    .line 152
    .local v1, "length":I
    :goto_1
    const/4 v6, 0x0

    invoke-static {p2, v0, v4, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 153
    add-int/2addr v0, v1

    .line 155
    new-instance v2, Lcom/vectorwatch/android/service/ble/BleGattPacket;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v6

    invoke-direct {v2, v6, v4, v1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;-><init>(Ljava/util/UUID;[BI)V

    .line 156
    .local v2, "packet":Lcom/vectorwatch/android/service/ble/BleGattPacket;
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .end local v1    # "length":I
    .end local v2    # "packet":Lcom/vectorwatch/android/service/ble/BleGattPacket;
    :cond_0
    move v1, v5

    .line 150
    goto :goto_1

    .line 159
    :cond_1
    return-object v3
.end method

.method private setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V
    .locals 3
    .param p1, "receptionId"    # B
    .param p2, "status"    # Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    .prologue
    .line 587
    if-ltz p1, :cond_0

    const/16 v0, 0x40

    if-lt p1, v0, :cond_1

    .line 588
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Protocol - invalid reception id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 591
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxStatus:[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    aput-object p2, v0, p1

    .line 592
    return-void
.end method

.method private unpack([B)Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .locals 11
    .param p1, "rawData"    # [B

    .prologue
    .line 363
    array-length v8, p1

    if-nez v8, :cond_0

    .line 364
    const/4 v6, 0x0

    .line 550
    :goto_0
    :pswitch_0
    return-object v6

    .line 367
    :cond_0
    const/4 v6, 0x0

    .line 377
    .local v6, "responseData":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    const/4 v8, 0x0

    aget-byte v1, p1, v8

    .line 378
    .local v1, "header":B
    and-int/lit16 v8, v1, 0xff

    shr-int/lit8 v8, v8, 0x2

    int-to-byte v5, v8

    .line 379
    .local v5, "receptionId":B
    and-int/lit8 v8, v1, 0x3

    int-to-byte v8, v8

    invoke-static {v8}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->getStatusByValue(B)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    move-result-object v0

    .line 381
    .local v0, "frameStatus":Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$DefaultTransformer$FrameStatus:[I

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    goto :goto_0

    .line 386
    :pswitch_1
    array-length v8, p1

    add-int/lit8 v7, v8, -0x1

    .line 387
    .local v7, "writeLength":I
    new-array v3, v7, [B

    .line 388
    .local v3, "payload":[B
    const/4 v8, 0x1

    const/4 v9, 0x0

    array-length v10, v3

    invoke-static {p1, v8, v3, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 389
    new-instance v6, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .end local v6    # "responseData":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-direct {v6, v3}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>([B)V

    .line 395
    .restart local v6    # "responseData":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    invoke-direct {p0, v5, v0}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto :goto_0

    .line 401
    .end local v3    # "payload":[B
    .end local v7    # "writeLength":I
    :pswitch_2
    array-length v8, p1

    const/16 v9, 0x14

    if-eq v8, v9, :cond_1

    .line 406
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-direct {p0, v5, v8}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto :goto_0

    .line 410
    :cond_1
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v2

    .line 411
    .local v2, "numPackets":S
    array-length v8, p1

    add-int/lit8 v8, v8, -0x2

    add-int/lit8 v4, v8, -0x1

    .line 412
    .local v4, "readLength":I
    new-array v3, v4, [B

    .line 413
    .restart local v3    # "payload":[B
    const/4 v8, 0x3

    const/4 v9, 0x0

    array-length v10, v3

    invoke-static {p1, v8, v3, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 415
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v8, v3, v5, v2}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->initialize([BBS)I

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_2

    .line 421
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-direct {p0, v5, v8}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto :goto_0

    .line 426
    :cond_2
    invoke-direct {p0, v5, v0}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto :goto_0

    .line 430
    .end local v2    # "numPackets":S
    .end local v3    # "payload":[B
    .end local v4    # "readLength":I
    :pswitch_3
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$DefaultTransformer$FrameStatus:[I

    invoke-direct {p0, v5}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->getReceptionStatus(B)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    goto :goto_0

    .line 440
    :pswitch_4
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v8}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->getReceptionId()B

    move-result v8

    if-ne v5, v8, :cond_5

    .line 443
    array-length v8, p1

    const/16 v9, 0x14

    if-eq v8, v9, :cond_3

    .line 449
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-direct {p0, v5, v8}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto/16 :goto_0

    .line 454
    :cond_3
    array-length v8, p1

    add-int/lit8 v4, v8, -0x1

    .line 455
    .restart local v4    # "readLength":I
    new-array v3, v4, [B

    .line 456
    .restart local v3    # "payload":[B
    const/4 v8, 0x1

    const/4 v9, 0x0

    array-length v10, v3

    invoke-static {p1, v8, v3, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 457
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v8, v3}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->add([B)I

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_4

    .line 463
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-direct {p0, v5, v8}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto/16 :goto_0

    .line 468
    :cond_4
    invoke-direct {p0, v5, v0}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto/16 :goto_0

    .line 470
    .end local v3    # "payload":[B
    .end local v4    # "readLength":I
    :cond_5
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Protocol - dropping middle fragment of RX "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " because of "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "invalid sequence status"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 476
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-direct {p0, v5, v8}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto/16 :goto_0

    .line 483
    :pswitch_5
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$DefaultTransformer$FrameStatus:[I

    invoke-direct {p0, v5}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->getReceptionStatus(B)Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    goto/16 :goto_0

    .line 493
    :pswitch_6
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v8}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->getReceptionId()B

    move-result v8

    if-ne v5, v8, :cond_9

    .line 498
    array-length v8, p1

    const/4 v9, 0x3

    if-ge v8, v9, :cond_6

    .line 499
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Protocol - dropping last fragment of RX "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " because of "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "incorrect packet length"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 504
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-direct {p0, v5, v8}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto/16 :goto_0

    .line 509
    :cond_6
    array-length v8, p1

    add-int/lit8 v4, v8, -0x1

    .line 510
    .restart local v4    # "readLength":I
    new-array v3, v4, [B

    .line 511
    .restart local v3    # "payload":[B
    const/4 v8, 0x1

    const/4 v9, 0x0

    array-length v10, v3

    invoke-static {p1, v8, v3, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 513
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v8, v3}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->add([B)I

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_7

    .line 514
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Protocol - dropping last fragment of RX "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " because of "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "insufficient space in reception buffer"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 519
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-direct {p0, v5, v8}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    .line 522
    :cond_7
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v8}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->getPackets()S

    move-result v8

    iget-object v9, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v9}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->getTotalPackets()S

    move-result v9

    if-ne v8, v9, :cond_8

    .line 524
    new-instance v6, Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .end local v6    # "responseData":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    iget-object v8, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    invoke-virtual {v8}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->getBuffer()[B

    move-result-object v8

    invoke-direct {v6, v8}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;-><init>([B)V

    .restart local v6    # "responseData":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    goto/16 :goto_0

    .line 530
    :cond_8
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Protocol - dropping frame for RX "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " because of incorrect"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "number of packets: expected "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    .line 531
    invoke-virtual {v10}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->getTotalPackets()S

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "got "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->rxBuffer:Lcom/vectorwatch/android/service/ble/models/RxBuffer;

    .line 532
    invoke-virtual {v10}, Lcom/vectorwatch/android/service/ble/models/RxBuffer;->getPackets()S

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 530
    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 537
    invoke-direct {p0, v5, v0}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->setReceptionStatus(BLcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;)V

    goto/16 :goto_0

    .line 540
    .end local v3    # "payload":[B
    .end local v4    # "readLength":I
    :cond_9
    sget-object v8, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Protocol - dropping last fragment of RX "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " because of invalid"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " sequence status"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 381
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 430
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 483
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public getNumPacks([B)S
    .locals 4
    .param p1, "rawData"    # [B

    .prologue
    .line 169
    const/4 v0, 0x1

    .line 170
    .local v0, "numPackets":S
    array-length v1, p1

    iget v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PAYLOAD_LENGTH:I

    if-le v1, v2, :cond_0

    .line 171
    array-length v1, p1

    add-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->MAX_PAYLOAD_LENGTH:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    int-to-short v0, v1

    .line 174
    :cond_0
    return v0
.end method

.method public declared-synchronized packMessageForDevice(Lcom/vectorwatch/android/service/ble/messages/BtMessage;Landroid/bluetooth/BluetoothGattCharacteristic;)Ljava/util/List;
    .locals 4
    .param p1, "message"    # Lcom/vectorwatch/android/service/ble/messages/BtMessage;
    .param p2, "characteristic"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/service/ble/messages/BtMessage;",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$messages$BaseMessage$MessageType:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;->getMessageType()Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 128
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: Trying to pack message for device, but message is neither NOTIFICATION, DATA nor NOTIFICATION_DETAILS"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    :goto_0
    const/4 v1, 0x0

    :goto_1
    monitor-exit p0

    return-object v1

    .line 98
    :pswitch_0
    if-eqz p2, :cond_0

    .line 99
    :try_start_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;->serialize()[B

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->send(Landroid/bluetooth/BluetoothGattCharacteristic;[B)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    .line 101
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: Tried to send notification to device but characteristic was null."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 96
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 105
    :pswitch_1
    if-eqz p2, :cond_1

    .line 107
    :try_start_2
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;->serialize()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->pack([B)[B

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->send(Landroid/bluetooth/BluetoothGattCharacteristic;[B)Ljava/util/List;
    :try_end_2
    .catch Lcom/vectorwatch/android/events/FrameOverflowException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/vectorwatch/android/events/ChannelBusyException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto :goto_1

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Lcom/vectorwatch/android/events/FrameOverflowException;
    :try_start_3
    invoke-virtual {v0}, Lcom/vectorwatch/android/events/FrameOverflowException;->printStackTrace()V

    .line 110
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BLE CALLS: Can\'t send data (Frame overflow) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vectorwatch/android/events/FrameOverflowException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 111
    .end local v0    # "e":Lcom/vectorwatch/android/events/FrameOverflowException;
    :catch_1
    move-exception v0

    .line 112
    .local v0, "e":Lcom/vectorwatch/android/events/ChannelBusyException;
    invoke-virtual {v0}, Lcom/vectorwatch/android/events/ChannelBusyException;->printStackTrace()V

    .line 113
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BLE CALLS: Can\'t send data (Channel busy) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vectorwatch/android/events/ChannelBusyException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    .end local v0    # "e":Lcom/vectorwatch/android/events/ChannelBusyException;
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: Tried to send data to device but characteristic was null."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :pswitch_2
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: Sending notification details"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 121
    if-eqz p2, :cond_2

    .line 122
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/BtMessage;->serialize()[B

    move-result-object v1

    invoke-direct {p0, p2, v1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->send(Landroid/bluetooth/BluetoothGattCharacteristic;[B)Ljava/util/List;

    move-result-object v1

    goto/16 :goto_1

    .line 124
    :cond_2
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    const-string v2, "BLE CALLS: Tried to send notification details to device but characteristic was null."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public unpackMessageFromDevice([BLjava/util/UUID;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "uuid"    # Ljava/util/UUID;

    .prologue
    .line 335
    sget-object v1, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_RX:Ljava/util/UUID;

    invoke-virtual {p2, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    const-string v2, "Received request for more details from device"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 339
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_NOTIFICATION_DETAILS_REQUEST:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    new-instance v2, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;

    invoke-direct {v2, p1}, Lcom/vectorwatch/android/service/ble/messages/NotificationRequestMessage;-><init>([B)V

    invoke-direct {p0, v1, v2}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->dispatchMessageToHandlerIfAlive(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V

    .line 353
    :goto_0
    return-void

    .line 341
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_DATA_RX:Ljava/util/UUID;

    invoke-virtual {p2, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 343
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->unpack([B)Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    move-result-object v0

    .line 345
    .local v0, "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    if-eqz v0, :cond_1

    .line 346
    sget-object v1, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_RECEIVE_MESSAGE_FROM_DEVICE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    invoke-direct {p0, v1, v0}, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->dispatchMessageToHandlerIfAlive(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V

    goto :goto_0

    .line 348
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    const-string v2, "Protocol - Unpacked message is null!"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 351
    .end local v0    # "message":Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    :cond_2
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer;->log:Lorg/slf4j/Logger;

    const-string v2, "Protocol - unpacking message: Should not get here at this time"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method
