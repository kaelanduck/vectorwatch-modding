.class synthetic Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;
.super Ljava/lang/Object;
.source "DefaultTransformer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/service/ble/DefaultTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$vectorwatch$android$service$ble$DefaultTransformer$FrameStatus:[I

.field static final synthetic $SwitchMap$com$vectorwatch$android$service$ble$messages$BaseMessage$MessageType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 430
    invoke-static {}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->values()[Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$DefaultTransformer$FrameStatus:[I

    :try_start_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$DefaultTransformer$FrameStatus:[I

    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->NO_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$DefaultTransformer$FrameStatus:[I

    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->LAST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$DefaultTransformer$FrameStatus:[I

    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->FIRST_FRAGMENT:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$DefaultTransformer$FrameStatus:[I

    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->MORE_FRAGMENTS:Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/DefaultTransformer$FrameStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 96
    :goto_3
    invoke-static {}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->values()[Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$messages$BaseMessage$MessageType:[I

    :try_start_4
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$messages$BaseMessage$MessageType:[I

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$messages$BaseMessage$MessageType:[I

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->DATA:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransformer$1;->$SwitchMap$com$vectorwatch$android$service$ble$messages$BaseMessage$MessageType:[I

    sget-object v1, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->NOTIFICATION_DETAILS:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$MessageType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    .line 430
    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v0

    goto :goto_1

    :catch_6
    move-exception v0

    goto :goto_0
.end method
