.class public Lcom/vectorwatch/android/service/ble/DefaultTransceiver;
.super Ljava/lang/Object;
.source "DefaultTransceiver.java"

# interfaces
.implements Lcom/vectorwatch/android/service/ble/Transceiver;


# static fields
.field private static final API_KITKAT:I = 0x13

.field private static final COUNT_DESCRIPTORS:I = 0x2

.field private static final GATT_NO_RESOURCE:I = 0x80

.field private static final fLockThread:Ljava/lang/Object;

.field private static final log:Lorg/slf4j/Logger;

.field private static sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;


# instance fields
.field private mBleGattCallback:Landroid/bluetooth/BluetoothGattCallback;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

.field private mBluetoothManager:Landroid/bluetooth/BluetoothManager;

.field private mBluetoothState:Ljava/lang/Integer;

.field private mGattCharacteristicsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "Landroid/bluetooth/BluetoothGattCharacteristic;",
            ">;"
        }
    .end annotation
.end field

.field private mGattPacketsList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceContext:Landroid/content/Context;

.field private mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

.field private mWrittenDescriptors:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->fLockThread:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;Landroid/content/Context;)V
    .locals 2
    .param p1, "workerHandler"    # Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWrittenDescriptors:I

    .line 61
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothState:Ljava/lang/Integer;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBleGattCallback:Landroid/bluetooth/BluetoothGattCallback;

    .line 66
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: constructor."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 67
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;

    .line 69
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    .line 71
    iput-object p2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mServiceContext:Landroid/content/Context;

    .line 72
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mServiceContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->initializeBluetoothAdapter(Landroid/content/Context;)Z

    .line 73
    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;
    .param p1, "x1"    # Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    .param p2, "x2"    # Ljava/lang/Object;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->dispatchMessageToHandlerIfAlive(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGattService;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->getSupportedGattService()Landroid/bluetooth/BluetoothGattService;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "x2"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "x3"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->configureDescriptors(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Landroid/bluetooth/BluetoothGatt;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    return-object v0
.end method

.method static synthetic access$300()Lcom/vectorwatch/android/service/ble/BleGattPacket;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;
    .param p1, "x1"    # Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->handlePacketStatus(Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V

    return-void
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sendNextBlePacket()V

    return-void
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->handleBluetoothGattNullCase()V

    return-void
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->cleanupBluetoothAppQueue()V

    return-void
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->isPossibleToReconnect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 35
    iget v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWrittenDescriptors:I

    return v0
.end method

.method static synthetic access$902(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;
    .param p1, "x1"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWrittenDescriptors:I

    return p1
.end method

.method static synthetic access$908(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)I
    .locals 2
    .param p0, "x0"    # Lcom/vectorwatch/android/service/ble/DefaultTransceiver;

    .prologue
    .line 35
    iget v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWrittenDescriptors:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWrittenDescriptors:I

    return v0
.end method

.method private cleanupBluetoothAppQueue()V
    .locals 4

    .prologue
    .line 742
    sget-object v1, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    if-eqz v1, :cond_0

    .line 743
    sget-object v1, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->FAIL:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    invoke-direct {p0, v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->handlePacketStatus(Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V

    .line 746
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    monitor-enter v2

    .line 747
    :try_start_0
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/service/ble/BleGattPacket;

    .line 748
    .local v0, "packet":Lcom/vectorwatch/android/service/ble/BleGattPacket;
    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    .line 749
    sget-object v3, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->FAIL:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    invoke-direct {p0, v3}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->handlePacketStatus(Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V

    goto :goto_0

    .line 753
    .end local v0    # "packet":Lcom/vectorwatch/android/service/ble/BleGattPacket;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 752
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 753
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 754
    return-void
.end method

.method private configureDescriptors(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;Landroid/bluetooth/BluetoothGattCharacteristic;)Z
    .locals 7
    .param p1, "bluetoothGatt"    # Landroid/bluetooth/BluetoothGatt;
    .param p2, "characteristicRx"    # Landroid/bluetooth/BluetoothGattCharacteristic;
    .param p3, "characteristicDataRx"    # Landroid/bluetooth/BluetoothGattCharacteristic;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 570
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 571
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TRANSCEIVER: Configure descriptors received null characteristic data = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " notif = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    move v3, v4

    .line 615
    :goto_0
    return v3

    .line 579
    :cond_1
    invoke-virtual {p2, v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    .line 580
    sget-object v4, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->CLIENT_CHARACTERISTIC_CONFIG:Ljava/lang/String;

    .line 581
    invoke-static {v4}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v4

    .line 580
    invoke-virtual {p2, v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v1

    .line 584
    .local v1, "descriptorRx":Landroid/bluetooth/BluetoothGattDescriptor;
    sget-object v4, Landroid/bluetooth/BluetoothGattDescriptor;->ENABLE_INDICATION_VALUE:[B

    invoke-virtual {v1, v4}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    .line 586
    invoke-virtual {p1, p2, v3}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    .line 588
    new-instance v2, Lcom/vectorwatch/android/service/ble/BleGattPacket;

    invoke-direct {v2, v1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;-><init>(Landroid/bluetooth/BluetoothGattDescriptor;)V

    .line 590
    .local v2, "packet":Lcom/vectorwatch/android/service/ble/BleGattPacket;
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    monitor-enter v4

    .line 591
    :try_start_0
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v2}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 592
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 594
    sget-object v4, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TRANSCEIVER: Descriptor (NOTIFICATION) - packet created and added to the list: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 595
    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getCharacteristicUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 594
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 598
    invoke-virtual {p3, v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->setWriteType(I)V

    .line 599
    sget-object v4, Lcom/vectorwatch/android/service/ble/BleGattAttributes;->CLIENT_CHARACTERISTIC_CONFIG:Ljava/lang/String;

    .line 600
    invoke-static {v4}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v4

    .line 599
    invoke-virtual {p3, v4}, Landroid/bluetooth/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    .line 602
    .local v0, "descriptorDataRx":Landroid/bluetooth/BluetoothGattDescriptor;
    sget-object v4, Landroid/bluetooth/BluetoothGattDescriptor;->ENABLE_NOTIFICATION_VALUE:[B

    invoke-virtual {v0, v4}, Landroid/bluetooth/BluetoothGattDescriptor;->setValue([B)Z

    .line 604
    invoke-virtual {p1, p3, v3}, Landroid/bluetooth/BluetoothGatt;->setCharacteristicNotification(Landroid/bluetooth/BluetoothGattCharacteristic;Z)Z

    .line 606
    new-instance v2, Lcom/vectorwatch/android/service/ble/BleGattPacket;

    .end local v2    # "packet":Lcom/vectorwatch/android/service/ble/BleGattPacket;
    invoke-direct {v2, v0}, Lcom/vectorwatch/android/service/ble/BleGattPacket;-><init>(Landroid/bluetooth/BluetoothGattDescriptor;)V

    .line 608
    .restart local v2    # "packet":Lcom/vectorwatch/android/service/ble/BleGattPacket;
    iget-object v4, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    monitor-enter v4

    .line 609
    :try_start_1
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v2}, Ljava/util/LinkedList;->add(ILjava/lang/Object;)V

    .line 610
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 612
    sget-object v4, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TRANSCEIVER: Descriptor (DATA) - packet created and added to the list: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 613
    invoke-virtual {v2}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getCharacteristicUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 612
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 592
    .end local v0    # "descriptorDataRx":Landroid/bluetooth/BluetoothGattDescriptor;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 610
    .restart local v0    # "descriptorDataRx":Landroid/bluetooth/BluetoothGattDescriptor;
    :catchall_1
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3
.end method

.method private dispatchMessageToHandlerIfAlive(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V
    .locals 4
    .param p1, "type"    # Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;
    .param p2, "extra"    # Ljava/lang/Object;

    .prologue
    .line 729
    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->fLockThread:Ljava/lang/Object;

    monitor-enter v2

    .line 730
    :try_start_0
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->isHandlerThreadTerminating()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 731
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-static {p1}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->getOrdinal(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;)I

    move-result v3

    invoke-static {v1, v3, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 732
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mWorkerHandler:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler;->sendMessage(Landroid/os/Message;)Z

    .line 734
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    monitor-exit v2

    .line 735
    return-void

    .line 734
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getBleCallback()Landroid/bluetooth/BluetoothGattCallback;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$1;-><init>(Lcom/vectorwatch/android/service/ble/DefaultTransceiver;)V

    return-object v0
.end method

.method private getSupportedGattService()Landroid/bluetooth/BluetoothGattService;
    .locals 2

    .prologue
    .line 536
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    .line 537
    const/4 v0, 0x0

    .line 541
    :goto_0
    return-object v0

    .line 540
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: Get supported GATT services."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 541
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_SERVICE:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v0

    goto :goto_0
.end method

.method private handleBluetoothGattNullCase()V
    .locals 1

    .prologue
    .line 764
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    .line 766
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->cleanupBluetoothAppQueue()V

    .line 770
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->connect()Z

    .line 771
    return-void
.end method

.method private handlePacketStatus(Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V
    .locals 2
    .param p1, "status"    # Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    .prologue
    .line 798
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    if-nez v0, :cond_0

    .line 810
    :goto_0
    return-void

    .line 802
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getDescriptor()Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 803
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: packet == descriptor. Not sending status to above layer."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 809
    :goto_1
    const/4 v0, 0x0

    sput-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    goto :goto_0

    .line 805
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    invoke-direct {p0, v0, p1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sendStatusForPacket(Lcom/vectorwatch/android/service/ble/BleGattPacket;Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V

    goto :goto_1
.end method

.method private declared-synchronized initializeBluetoothAdapter(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 513
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mServiceContext:Landroid/content/Context;

    .line 515
    const-string v0, "bluetooth"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothManager;

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothManager:Landroid/bluetooth/BluetoothManager;

    .line 516
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothManager:Landroid/bluetooth/BluetoothManager;

    if-nez v0, :cond_0

    .line 517
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v2, "TRANSCEIVER: Unable to initialize BluetoothManager."

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 526
    :goto_0
    monitor-exit p0

    return v0

    .line 521
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothManager:Landroid/bluetooth/BluetoothManager;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 522
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_1

    .line 523
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v2, "TRANSCEIVER: Unable to obtain a BluetoothAdapter."

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 524
    goto :goto_0

    .line 526
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 513
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private isPossibleToReconnect()Z
    .locals 3

    .prologue
    .line 779
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mServiceContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 780
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: Possible to reconnect - true"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 782
    const/4 v0, 0x1

    .line 788
    :goto_0
    return v0

    .line 785
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: Possible to reconnect - false. Bonded address = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mServiceContext:Landroid/content/Context;

    .line 786
    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " adapter = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "enabled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 787
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 785
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 788
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendNextBlePacket()V
    .locals 5

    .prologue
    .line 638
    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v3, "TRANSCEIVER: Send next ble packet."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 641
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    monitor-enter v3

    .line 642
    :try_start_0
    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "TRANSCEIVER: Send next - synchronized block."

    invoke-interface {v2, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 644
    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    if-eqz v2, :cond_0

    .line 645
    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "TRANSCEIVER: Tried to call send NEXT, but PACKET already in send mode."

    invoke-interface {v2, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 646
    monitor-exit v3

    .line 672
    :goto_0
    return-void

    .line 649
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 650
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v2, :cond_1

    .line 651
    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "TRANSCEIVER: BluetoothGatt is null at while trying to write D/C. Trying to connect."

    invoke-interface {v2, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 652
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->handleBluetoothGattNullCase()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 658
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/service/ble/BleGattPacket;

    sput-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    .line 659
    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sPacketSelectedForSend:Lcom/vectorwatch/android/service/ble/BleGattPacket;

    invoke-direct {p0, v2}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->writePacket(Lcom/vectorwatch/android/service/ble/BleGattPacket;)Z

    move-result v1

    .line 661
    .local v1, "success":Z
    if-nez v1, :cond_2

    .line 662
    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "TRANSCEIVER: Write packet fail. This should not happen. It means it\'s impossible to write characteristic or descriptor."

    invoke-interface {v2, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 664
    sget-object v2, Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;->FAIL:Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    invoke-direct {p0, v2}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->handlePacketStatus(Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 671
    .end local v1    # "success":Z
    :cond_2
    :goto_1
    :try_start_2
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 666
    :catch_0
    move-exception v0

    .line 667
    .local v0, "e":Ljava/util/NoSuchElementException;
    :try_start_3
    invoke-static {v0}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    .line 668
    sget-object v2, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "TRANSCEIVER: No such element exception for ble packets queue."

    invoke-interface {v2, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private sendStatusForPacket(Lcom/vectorwatch/android/service/ble/BleGattPacket;Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V
    .locals 2
    .param p1, "packet"    # Lcom/vectorwatch/android/service/ble/BleGattPacket;
    .param p2, "status"    # Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;

    .prologue
    .line 625
    if-nez p1, :cond_0

    .line 626
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: Trying to send status, but packet null."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 632
    :goto_0
    return-void

    .line 630
    :cond_0
    invoke-virtual {p1, p2}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->setStatus(Lcom/vectorwatch/android/service/ble/BleGattPacket$Status;)V

    .line 631
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_PACKET_SENT_TO_BLE:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    invoke-direct {p0, v0, p1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->dispatchMessageToHandlerIfAlive(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private writePacket(Lcom/vectorwatch/android/service/ble/BleGattPacket;)Z
    .locals 5
    .param p1, "packet"    # Lcom/vectorwatch/android/service/ble/BleGattPacket;

    .prologue
    .line 681
    const/4 v0, 0x0

    .line 683
    .local v0, "executedOk":Z
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver$2;->$SwitchMap$com$vectorwatch$android$service$ble$BleGattPacket$Type:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getType()Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/service/ble/BleGattPacket$Type;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 703
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "TRANSCEIVER: Should not have reached this point writePacket"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 704
    const/4 v0, 0x0

    .line 707
    :cond_0
    :goto_0
    return v0

    .line 685
    :pswitch_0
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "TRANSCEIVER: Exec packet WRITE_DESCRIPTOR"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 686
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getDescriptor()Landroid/bluetooth/BluetoothGattDescriptor;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothGatt;->writeDescriptor(Landroid/bluetooth/BluetoothGattDescriptor;)Z

    move-result v0

    .line 687
    if-eqz v0, :cond_0

    .line 688
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "TRANSCEIVER: Descriptor write ok."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 692
    :pswitch_1
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getCharacteristicUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 693
    .local v2, "writeCharacteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getVal()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothGattCharacteristic;->setValue([B)Z

    .line 694
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v3, v2}, Landroid/bluetooth/BluetoothGatt;->writeCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    .line 695
    goto :goto_0

    .line 698
    .end local v2    # "writeCharacteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    :pswitch_2
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "TRANSCEIVER: READ_CHARACTERISTIC"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 699
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/BleGattPacket;->getCharacteristicUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 700
    .local v1, "readCharacteristic":Landroid/bluetooth/BluetoothGattCharacteristic;
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v3, v1}, Landroid/bluetooth/BluetoothGatt;->readCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v0

    .line 701
    goto :goto_0

    .line 683
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public addPacketsToBuffer(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/service/ble/BleGattPacket;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 358
    .local p1, "packets":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/service/ble/BleGattPacket;>;"
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: Adding packets. Buffer initial size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " adding "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 359
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " packets"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 361
    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 362
    :try_start_0
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattPacketsList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 363
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: After adding command -> Call send next."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 366
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->sendNextBlePacket()V

    .line 367
    return-void

    .line 363
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public closeConnection()V
    .locals 2

    .prologue
    .line 434
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->cleanupBluetoothAppQueue()V

    .line 436
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    .line 437
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: BluetoothGatt not initialized - close conn"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 450
    :goto_0
    return-void

    .line 441
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    .line 442
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: Connection state changed to DISCONNECTED (close)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 445
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->close()V

    .line 447
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 449
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: Closed gatt. bluetoothGatt set to null."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public connect()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 376
    sget-object v5, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v6, "TRANSCEIVER: Trying to connect to a BLE device"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 377
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mServiceContext:Landroid/content/Context;

    invoke-direct {p0, v5}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->initializeBluetoothAdapter(Landroid/content/Context;)Z

    .line 379
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v5, :cond_0

    .line 380
    sget-object v4, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v5, "TRANSCEIVER: BluetoothAdapter is not initialized in connect."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 381
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    .line 425
    :goto_0
    return v3

    .line 385
    :cond_0
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mServiceContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 387
    .local v0, "address":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 388
    sget-object v4, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v5, "TRANSCEIVER: Unspecified address"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 389
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    goto :goto_0

    .line 393
    :cond_1
    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v5, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 395
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    if-nez v1, :cond_2

    .line 396
    sget-object v4, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v5, "TRANSCEIVER: Device not found. Unable to connect"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 397
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    goto :goto_0

    .line 401
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothState:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v5, 0x2

    if-eq v3, v5, :cond_4

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothState:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v4, :cond_4

    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 402
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 403
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->cleanupBluetoothAppQueue()V

    .line 404
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    .line 406
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v3, :cond_3

    .line 407
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->getBleCallback()Landroid/bluetooth/BluetoothGattCallback;

    move-result-object v3

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBleGattCallback:Landroid/bluetooth/BluetoothGattCallback;

    .line 410
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mServiceContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBleGattCallback:Landroid/bluetooth/BluetoothGattCallback;

    invoke-virtual {v1, v3, v4, v5}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v3

    iput-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    .line 412
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TRANSCEIVER: Creating new connection (via new mBluetoothGatt) "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothManager:Landroid/bluetooth/BluetoothManager;

    const/4 v7, 0x7

    .line 413
    invoke-virtual {v6, v1, v7}, Landroid/bluetooth/BluetoothManager;->getConnectionState(Landroid/bluetooth/BluetoothDevice;I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 412
    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 420
    :goto_1
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v5, "TRANSCEIVER: Connection state changed to CONNECTING"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    :goto_2
    move v3, v4

    .line 425
    goto/16 :goto_0

    .line 415
    :cond_3
    iget-object v3, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothGatt;->connect()Z

    move-result v2

    .line 417
    .local v2, "result":Z
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TRANSCEIVER: Connecting on previous gatt "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 422
    .end local v2    # "result":Z
    :cond_4
    sget-object v3, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v5, "TRANSCEIVER: BLE already CONNECTED or CONNECTING or Bluetooth is not enabled."

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 500
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: destroy object."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 501
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->closeConnection()V

    .line 502
    return-void
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 455
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->cleanupBluetoothAppQueue()V

    .line 457
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    .line 458
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: BluetoothGatt not initialized - disconnect"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 470
    :goto_0
    return-void

    .line 462
    :cond_0
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->updateBluetoothState(Ljava/lang/Integer;)V

    .line 464
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: Connection state changed to DISCONNECTING (disconnect)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 469
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->disconnect()V

    goto :goto_0
.end method

.method public getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;
    .locals 1
    .param p1, "uuid"    # Ljava/util/UUID;

    .prologue
    .line 495
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mGattCharacteristicsMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    return-object v0
.end method

.method public getSupportedGattDataService()Landroid/bluetooth/BluetoothGattService;
    .locals 2

    .prologue
    .line 551
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    .line 552
    const/4 v0, 0x0

    .line 556
    :goto_0
    return-object v0

    .line 555
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: Get supported GATT DATA services."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    sget-object v1, Lcom/vectorwatch/android/service/ble/BleService;->UUID_BLE_SHIELD_DATA_SERVICE:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v0

    goto :goto_0
.end method

.method public readRssi()Z
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    if-nez v0, :cond_0

    .line 349
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: readRssi - BluetoothAdapter not initialized"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 350
    const/4 v0, 0x0

    .line 353
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothGatt:Landroid/bluetooth/BluetoothGatt;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGatt;->readRemoteRssi()Z

    move-result v0

    goto :goto_0
.end method

.method public reconnect()V
    .locals 2

    .prologue
    .line 477
    invoke-direct {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->isPossibleToReconnect()Z

    move-result v0

    if-nez v0, :cond_0

    .line 478
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: RECONNECT: Not possible to reconnect to gatt. Must NOT get here."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 491
    :goto_0
    return-void

    .line 482
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "TRANSCEIVER: Reconnect activated. Will close_connection and connect."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 483
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->closeConnection()V

    .line 486
    const-wide/16 v0, 0x1388

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 490
    :goto_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->connect()Z

    goto :goto_0

    .line 487
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public updateBluetoothState(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "newState"    # Ljava/lang/Integer;

    .prologue
    .line 716
    sget-object v0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TRANSCEIVER: Set bluetooth state to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and dispatch state to WorkerThread."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 717
    iput-object p1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothState:Ljava/lang/Integer;

    .line 718
    sget-object v0, Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;->MESSAGE_TYPE_BLE_CONNECTION_STATE_CHANGED:Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;

    iget-object v1, p0, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->mBluetoothState:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/service/ble/DefaultTransceiver;->dispatchMessageToHandlerIfAlive(Lcom/vectorwatch/android/service/ble/BluetoothWorkerThreadHandler$MessageType;Ljava/lang/Object;)V

    .line 719
    return-void
.end method
