.class public Lcom/vectorwatch/android/service/nls/NotificationContainer;
.super Ljava/lang/Object;
.source "NotificationContainer.java"


# instance fields
.field private conversationId:Ljava/lang/String;

.field private notificationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "conversationId"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/service/nls/NotificationContainer;->notificationList:Ljava/util/ArrayList;

    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/service/nls/NotificationContainer;->conversationId:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public addNotification(Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;)V
    .locals 1
    .param p1, "info"    # Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/service/nls/NotificationContainer;->notificationList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    return-void
.end method

.method public getConversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/service/nls/NotificationContainer;->conversationId:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vectorwatch/android/service/nls/NotificationContainer;->notificationList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setConversationId(Ljava/lang/String;)V
    .locals 0
    .param p1, "conversationId"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vectorwatch/android/service/nls/NotificationContainer;->conversationId:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setNotificationList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "notificationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/service/nls/NotificationContainer;->notificationList:Ljava/util/ArrayList;

    .line 37
    return-void
.end method
