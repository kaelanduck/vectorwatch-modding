.class public Lcom/vectorwatch/android/service/nls/PhoneCallHandler;
.super Ljava/lang/Object;
.source "PhoneCallHandler.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/vectorwatch/android/service/nls/PhoneCallHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/service/nls/PhoneCallHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static rejectCall(Landroid/content/Context;)V
    .locals 23
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    :try_start_0
    const-string v7, "android.os.ServiceManager"

    .line 24
    .local v7, "serviceManagerName":Ljava/lang/String;
    const-string v9, "android.os.ServiceManagerNative"

    .line 25
    .local v9, "serviceManagerNativeName":Ljava/lang/String;
    const-string v14, "com.android.internal.telephony.ITelephony"

    .line 33
    .local v14, "telephonyName":Ljava/lang/String;
    invoke-static {v14}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v12

    .line 34
    .local v12, "telephonyClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v12}, Ljava/lang/Class;->getClasses()[Ljava/lang/Class;

    move-result-object v19

    const/16 v20, 0x0

    aget-object v16, v19, v20

    .line 35
    .local v16, "telephonyStubClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    .line 36
    .local v6, "serviceManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v9}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    .line 37
    .local v8, "serviceManagerNativeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v19, "getService"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, Ljava/lang/String;

    aput-object v22, v20, v21

    .line 38
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 39
    .local v4, "getService":Ljava/lang/reflect/Method;
    const-string v19, "asInterface"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, Landroid/os/IBinder;

    aput-object v22, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v8, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v17

    .line 40
    .local v17, "tempInterfaceMethod":Ljava/lang/reflect/Method;
    new-instance v18, Landroid/os/Binder;

    invoke-direct/range {v18 .. v18}, Landroid/os/Binder;-><init>()V

    .line 41
    .local v18, "tmpBinder":Landroid/os/Binder;
    const/16 v19, 0x0

    const-string v20, "fake"

    invoke-virtual/range {v18 .. v20}, Landroid/os/Binder;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 42
    const/16 v19, 0x0

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v18, v20, v21

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .line 43
    .local v10, "serviceManagerObject":Ljava/lang/Object;
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "phone"

    aput-object v21, v19, v20

    move-object/from16 v0, v19

    invoke-virtual {v4, v10, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/IBinder;

    .line 44
    .local v5, "retbinder":Landroid/os/IBinder;
    const-string v19, "asInterface"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-class v22, Landroid/os/IBinder;

    aput-object v22, v20, v21

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    .line 45
    .local v11, "serviceMethod":Ljava/lang/reflect/Method;
    const/16 v19, 0x0

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v5, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v11, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .line 46
    .local v15, "telephonyObject":Ljava/lang/Object;
    const-string v19, "endCall"

    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v13

    .line 47
    .local v13, "telephonyEndCall":Ljava/lang/reflect/Method;
    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v13, v15, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .end local v4    # "getService":Ljava/lang/reflect/Method;
    .end local v5    # "retbinder":Landroid/os/IBinder;
    .end local v6    # "serviceManagerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v7    # "serviceManagerName":Ljava/lang/String;
    .end local v8    # "serviceManagerNativeClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v9    # "serviceManagerNativeName":Ljava/lang/String;
    .end local v10    # "serviceManagerObject":Ljava/lang/Object;
    .end local v11    # "serviceMethod":Ljava/lang/reflect/Method;
    .end local v12    # "telephonyClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v13    # "telephonyEndCall":Ljava/lang/reflect/Method;
    .end local v14    # "telephonyName":Ljava/lang/String;
    .end local v15    # "telephonyObject":Ljava/lang/Object;
    .end local v16    # "telephonyStubClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v17    # "tempInterfaceMethod":Ljava/lang/reflect/Method;
    .end local v18    # "tmpBinder":Landroid/os/Binder;
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v3

    .line 50
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 51
    sget-object v19, Lcom/vectorwatch/android/service/nls/PhoneCallHandler;->log:Lorg/slf4j/Logger;

    const-string v20, "FATAL ERROR: could not connect to telephony subsystem"

    invoke-interface/range {v19 .. v20}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 52
    sget-object v19, Lcom/vectorwatch/android/service/nls/PhoneCallHandler;->log:Lorg/slf4j/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception object: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method
