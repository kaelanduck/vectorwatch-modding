.class public Lcom/vectorwatch/android/models/DynamicResource;
.super Ljava/lang/Object;
.source "DynamicResource.java"


# static fields
.field private static final DATA_CHUNK_SIZE:I = 0x80


# instance fields
.field private compressed:Z

.field private compressedSize:S

.field private contentBase64:Ljava/lang/String;

.field private decodedContent:[B

.field private id:I

.field private size:S


# direct methods
.method public constructor <init>(ISLjava/lang/String;ZS)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "size"    # S
    .param p3, "content"    # Ljava/lang/String;
    .param p4, "compressed"    # Z
    .param p5, "compressedSize"    # S

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/models/DynamicResource;->decodedContent:[B

    .line 25
    iput p1, p0, Lcom/vectorwatch/android/models/DynamicResource;->id:I

    .line 26
    iput-short p2, p0, Lcom/vectorwatch/android/models/DynamicResource;->size:S

    .line 27
    iput-object p3, p0, Lcom/vectorwatch/android/models/DynamicResource;->contentBase64:Ljava/lang/String;

    .line 28
    iput-boolean p4, p0, Lcom/vectorwatch/android/models/DynamicResource;->compressed:Z

    .line 29
    iput-short p5, p0, Lcom/vectorwatch/android/models/DynamicResource;->compressedSize:S

    .line 30
    return-void
.end method


# virtual methods
.method public getChunkMessageBytes(I)[B
    .locals 8
    .param p1, "chunkIndex"    # I

    .prologue
    const/4 v5, 0x0

    .line 63
    new-array v1, v5, [B

    .line 66
    .local v1, "chunkBytes":[B
    if-nez p1, :cond_2

    .line 67
    const/16 v6, 0xd

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 68
    .local v0, "bytes":Ljava/nio/ByteBuffer;
    int-to-short v6, p1

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 69
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 70
    iget v6, p0, Lcom/vectorwatch/android/models/DynamicResource;->id:I

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 71
    iget-short v6, p0, Lcom/vectorwatch/android/models/DynamicResource;->size:S

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 72
    iget-boolean v6, p0, Lcom/vectorwatch/android/models/DynamicResource;->compressed:Z

    if-eqz v6, :cond_0

    const/4 v5, 0x1

    :cond_0
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 73
    iget-short v5, p0, Lcom/vectorwatch/android/models/DynamicResource;->compressedSize:S

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 75
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 105
    .end local v0    # "bytes":Ljava/nio/ByteBuffer;
    :cond_1
    :goto_0
    return-object v1

    .line 78
    :cond_2
    iget-object v5, p0, Lcom/vectorwatch/android/models/DynamicResource;->decodedContent:[B

    if-nez v5, :cond_3

    .line 79
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/DynamicResource;->getContentBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/vectorwatch/android/models/DynamicResource;->decodedContent:[B

    .line 83
    :cond_3
    add-int/lit8 v5, p1, -0x1

    mul-int/lit16 v4, v5, 0x80

    .line 85
    .local v4, "chunk_start":I
    iget-object v5, p0, Lcom/vectorwatch/android/models/DynamicResource;->decodedContent:[B

    array-length v5, v5

    if-ge v4, v5, :cond_1

    .line 87
    mul-int/lit16 v2, p1, 0x80

    .line 88
    .local v2, "chunk_end":I
    iget-object v5, p0, Lcom/vectorwatch/android/models/DynamicResource;->decodedContent:[B

    array-length v5, v5

    if-le v2, v5, :cond_4

    .line 89
    iget-object v5, p0, Lcom/vectorwatch/android/models/DynamicResource;->decodedContent:[B

    array-length v2, v5

    .line 92
    :cond_4
    sub-int v3, v2, v4

    .line 94
    .local v3, "chunk_size":I
    add-int/lit8 v5, v3, 0x4

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 96
    .restart local v0    # "bytes":Ljava/nio/ByteBuffer;
    int-to-short v5, p1

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 97
    int-to-short v5, v3

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 99
    iget-object v5, p0, Lcom/vectorwatch/android/models/DynamicResource;->decodedContent:[B

    invoke-virtual {v0, v5, v4, v3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 101
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    goto :goto_0
.end method

.method public getCompressedSize()S
    .locals 1

    .prologue
    .line 53
    iget-short v0, p0, Lcom/vectorwatch/android/models/DynamicResource;->compressedSize:S

    return v0
.end method

.method public getContentBase64()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/models/DynamicResource;->contentBase64:Ljava/lang/String;

    return-object v0
.end method

.method public getContentBytes()[B
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/models/DynamicResource;->contentBase64:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/vectorwatch/android/models/DynamicResource;->id:I

    return v0
.end method

.method public getSize()S
    .locals 1

    .prologue
    .line 37
    iget-short v0, p0, Lcom/vectorwatch/android/models/DynamicResource;->size:S

    return v0
.end method

.method public isCompressed()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/DynamicResource;->compressed:Z

    return v0
.end method
