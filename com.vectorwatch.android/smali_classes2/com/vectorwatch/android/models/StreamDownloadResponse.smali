.class public Lcom/vectorwatch/android/models/StreamDownloadResponse;
.super Ljava/lang/Object;
.source "StreamDownloadResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;
    }
.end annotation


# instance fields
.field private data:Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method


# virtual methods
.method public getData()Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamDownloadResponse;->data:Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamDownloadResponse;->message:Ljava/lang/String;

    return-object v0
.end method
