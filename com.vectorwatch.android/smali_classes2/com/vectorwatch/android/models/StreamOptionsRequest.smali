.class public Lcom/vectorwatch/android/models/StreamOptionsRequest;
.super Lcom/vectorwatch/android/models/BaseCloudRequestModel;
.source "StreamOptionsRequest.java"


# instance fields
.field public settingName:Ljava/lang/String;

.field public userSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;"
        }
    .end annotation
.end field

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .param p1, "settingName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p3, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    invoke-direct {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamOptionsRequest;->settingName:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/vectorwatch/android/models/StreamOptionsRequest;->value:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/vectorwatch/android/models/StreamOptionsRequest;->userSettings:Ljava/util/Map;

    .line 19
    return-void
.end method
