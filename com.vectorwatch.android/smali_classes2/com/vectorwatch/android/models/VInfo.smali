.class public Lcom/vectorwatch/android/models/VInfo;
.super Ljava/lang/Object;
.source "VInfo.java"


# instance fields
.field private appVersion:Ljava/lang/String;

.field private deviceType:Ljava/lang/String;

.field private kernelVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/VInfo;->appVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/vectorwatch/android/models/VInfo;->deviceType:Ljava/lang/String;

    return-object v0
.end method

.method public getKernelVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/models/VInfo;->kernelVersion:Ljava/lang/String;

    return-object v0
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "appVersion"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/VInfo;->appVersion:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setDeviceType(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/vectorwatch/android/models/VInfo;->deviceType:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public setKernelVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "kernelVersion"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/models/VInfo;->kernelVersion:Ljava/lang/String;

    .line 32
    return-void
.end method
