.class public Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;
.super Ljava/lang/Object;
.source "AppCallbackProxyResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Command"
.end annotation


# instance fields
.field public appUuid:Ljava/lang/String;

.field public command:Ljava/lang/String;

.field public data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Data;

.field public elementId:I

.field public elementType:Ljava/lang/String;

.field public messageType:Ljava/lang/String;

.field public parameters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public secondsToLive:Ljava/lang/Integer;

.field public ttl:Ljava/lang/Integer;

.field public watchfaceId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTTLForWatch()I
    .locals 6

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->secondsToLive:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->secondsToLive:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 71
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->ttl:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method
