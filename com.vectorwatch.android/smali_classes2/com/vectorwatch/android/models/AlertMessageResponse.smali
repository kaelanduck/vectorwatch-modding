.class public Lcom/vectorwatch/android/models/AlertMessageResponse;
.super Ljava/lang/Object;
.source "AlertMessageResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/AlertMessageResponse$AlertMessageData;
    }
.end annotation


# instance fields
.field private data:Lcom/vectorwatch/android/models/AlertMessageResponse$AlertMessageData;

.field private error:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method


# virtual methods
.method public getData()Lcom/vectorwatch/android/models/AlertMessageResponse$AlertMessageData;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessageResponse;->data:Lcom/vectorwatch/android/models/AlertMessageResponse$AlertMessageData;

    return-object v0
.end method

.method public getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessageResponse;->error:Ljava/lang/String;

    return-object v0
.end method

.method public setData(Lcom/vectorwatch/android/models/AlertMessageResponse$AlertMessageData;)V
    .locals 0
    .param p1, "data"    # Lcom/vectorwatch/android/models/AlertMessageResponse$AlertMessageData;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessageResponse;->data:Lcom/vectorwatch/android/models/AlertMessageResponse$AlertMessageData;

    .line 17
    return-void
.end method

.method public setError(Ljava/lang/String;)V
    .locals 0
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessageResponse;->error:Ljava/lang/String;

    .line 25
    return-void
.end method
