.class public Lcom/vectorwatch/android/models/DstInfoModel;
.super Ljava/lang/Object;
.source "DstInfoModel.java"


# instance fields
.field private dstOffset:J

.field private end:J

.field private start:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDstOffset()J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/vectorwatch/android/models/DstInfoModel;->dstOffset:J

    return-wide v0
.end method

.method public getEnd()J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/vectorwatch/android/models/DstInfoModel;->end:J

    return-wide v0
.end method

.method public getStart()J
    .locals 2

    .prologue
    .line 12
    iget-wide v0, p0, Lcom/vectorwatch/android/models/DstInfoModel;->start:J

    return-wide v0
.end method

.method public setDstOffset(J)V
    .locals 1
    .param p1, "dstOffset"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/vectorwatch/android/models/DstInfoModel;->dstOffset:J

    .line 33
    return-void
.end method

.method public setEnd(J)V
    .locals 1
    .param p1, "end"    # J

    .prologue
    .line 24
    iput-wide p1, p0, Lcom/vectorwatch/android/models/DstInfoModel;->end:J

    .line 25
    return-void
.end method

.method public setStart(J)V
    .locals 1
    .param p1, "start"    # J

    .prologue
    .line 16
    iput-wide p1, p0, Lcom/vectorwatch/android/models/DstInfoModel;->start:J

    .line 17
    return-void
.end method
