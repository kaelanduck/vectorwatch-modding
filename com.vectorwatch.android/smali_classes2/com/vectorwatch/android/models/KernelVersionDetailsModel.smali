.class public Lcom/vectorwatch/android/models/KernelVersionDetailsModel;
.super Ljava/lang/Object;
.source "KernelVersionDetailsModel.java"


# instance fields
.field private buildVersion:B

.field private majorVersion:B

.field private minorVersion:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBuildVersion()B
    .locals 1

    .prologue
    .line 12
    iget-byte v0, p0, Lcom/vectorwatch/android/models/KernelVersionDetailsModel;->buildVersion:B

    return v0
.end method

.method public getMajorVersion()B
    .locals 1

    .prologue
    .line 16
    iget-byte v0, p0, Lcom/vectorwatch/android/models/KernelVersionDetailsModel;->majorVersion:B

    return v0
.end method

.method public getMinorVersion()B
    .locals 1

    .prologue
    .line 20
    iget-byte v0, p0, Lcom/vectorwatch/android/models/KernelVersionDetailsModel;->minorVersion:B

    return v0
.end method

.method public setBuildVersion(B)V
    .locals 0
    .param p1, "buildVersion"    # B

    .prologue
    .line 24
    iput-byte p1, p0, Lcom/vectorwatch/android/models/KernelVersionDetailsModel;->buildVersion:B

    .line 25
    return-void
.end method

.method public setMajorVersion(B)V
    .locals 0
    .param p1, "majorVersion"    # B

    .prologue
    .line 28
    iput-byte p1, p0, Lcom/vectorwatch/android/models/KernelVersionDetailsModel;->majorVersion:B

    .line 29
    return-void
.end method

.method public setMinorVersion(B)V
    .locals 0
    .param p1, "minorVersion"    # B

    .prologue
    .line 32
    iput-byte p1, p0, Lcom/vectorwatch/android/models/KernelVersionDetailsModel;->minorVersion:B

    .line 33
    return-void
.end method
