.class public Lcom/vectorwatch/android/models/UserSettings;
.super Ljava/lang/Object;
.source "UserSettings.java"


# instance fields
.field private activityNewsletter:Z

.field private activityReminder:Z

.field private goalAchievementAlert:Z

.field private goalAlmostAlert:Z

.field private linkLostIcon:Z

.field private linkLostNotification:Z

.field private lowBatteryIcon:Z

.field private lowBatteryNotification:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->linkLostIcon:Z

    .line 11
    iput-boolean v1, p0, Lcom/vectorwatch/android/models/UserSettings;->linkLostNotification:Z

    .line 12
    iput-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->lowBatteryIcon:Z

    .line 13
    iput-boolean v1, p0, Lcom/vectorwatch/android/models/UserSettings;->lowBatteryNotification:Z

    .line 15
    iput-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->goalAchievementAlert:Z

    .line 16
    iput-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->goalAlmostAlert:Z

    .line 17
    iput-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->activityReminder:Z

    .line 18
    iput-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->activityNewsletter:Z

    .line 20
    return-void
.end method


# virtual methods
.method public isActivityNewsletter()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->activityNewsletter:Z

    return v0
.end method

.method public isActivityReminder()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->activityReminder:Z

    return v0
.end method

.method public isGoalAchievementAlert()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->goalAchievementAlert:Z

    return v0
.end method

.method public isGoalAlmostAlert()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->goalAlmostAlert:Z

    return v0
.end method

.method public isLinkLostIcon()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->linkLostIcon:Z

    return v0
.end method

.method public isLinkLostNotification()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->linkLostNotification:Z

    return v0
.end method

.method public isLowBatteryIcon()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->lowBatteryIcon:Z

    return v0
.end method

.method public isLowBatteryNotification()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/UserSettings;->lowBatteryNotification:Z

    return v0
.end method

.method public setActivityNewsletter(Z)V
    .locals 0
    .param p1, "activityNewsletter"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/UserSettings;->activityNewsletter:Z

    .line 84
    return-void
.end method

.method public setActivityReminder(Z)V
    .locals 0
    .param p1, "activityReminder"    # Z

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/UserSettings;->activityReminder:Z

    .line 76
    return-void
.end method

.method public setGoalAchievementAlert(Z)V
    .locals 0
    .param p1, "goalAchievementAlert"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/UserSettings;->goalAchievementAlert:Z

    .line 60
    return-void
.end method

.method public setGoalAlmostAlert(Z)V
    .locals 0
    .param p1, "goalAlmostAlert"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/UserSettings;->goalAlmostAlert:Z

    .line 68
    return-void
.end method

.method public setLinkLostIcon(Z)V
    .locals 0
    .param p1, "linkLostIcon"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/UserSettings;->linkLostIcon:Z

    .line 28
    return-void
.end method

.method public setLinkLostNotification(Z)V
    .locals 0
    .param p1, "linkLostNotification"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/UserSettings;->linkLostNotification:Z

    .line 36
    return-void
.end method

.method public setLowBatteryIcon(Z)V
    .locals 0
    .param p1, "lowBatteryIcon"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/UserSettings;->lowBatteryIcon:Z

    .line 44
    return-void
.end method

.method public setLowBatteryNotification(Z)V
    .locals 0
    .param p1, "lowBatteryNotification"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/UserSettings;->lowBatteryNotification:Z

    .line 52
    return-void
.end method
