.class public Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;
.super Ljava/lang/Object;
.source "StreamMultiChannelValueModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/StreamMultiChannelValueModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Payload"
.end annotation


# instance fields
.field private appID:Ljava/lang/Integer;

.field private channelLabels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private fID:Ljava/lang/Integer;

.field private secondsToLive:Ljava/lang/Integer;

.field private streamUUID:Ljava/lang/String;

.field final synthetic this$0:Lcom/vectorwatch/android/models/StreamMultiChannelValueModel;

.field private ttl:Ljava/lang/Integer;

.field private type:Ljava/lang/Integer;

.field private wID:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/StreamMultiChannelValueModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/models/StreamMultiChannelValueModel;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->this$0:Lcom/vectorwatch/android/models/StreamMultiChannelValueModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppID()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->appID:Ljava/lang/Integer;

    return-object v0
.end method

.method public getChannelLabels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->channelLabels:Ljava/util/List;

    return-object v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondsToLive()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->secondsToLive:Ljava/lang/Integer;

    return-object v0
.end method

.method public getStreamUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->streamUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getTtl()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->ttl:Ljava/lang/Integer;

    return-object v0
.end method

.method public getType()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->type:Ljava/lang/Integer;

    return-object v0
.end method

.method public getfID()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->fID:Ljava/lang/Integer;

    return-object v0
.end method

.method public getwID()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamMultiChannelValueModel$Payload;->wID:Ljava/lang/Integer;

    return-object v0
.end method
