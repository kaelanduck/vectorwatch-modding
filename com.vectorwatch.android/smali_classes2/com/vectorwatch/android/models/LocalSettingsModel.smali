.class public Lcom/vectorwatch/android/models/LocalSettingsModel;
.super Ljava/lang/Object;
.source "LocalSettingsModel.java"


# static fields
.field public static final GENDER_FEMALE:Ljava/lang/String; = "F"

.field public static final GENDER_MALE:Ljava/lang/String; = "M"

.field public static final TIME_FORMAT_12H:Ljava/lang/String; = "h12"

.field public static final TIME_FORMAT_24H:Ljava/lang/String; = "h24"

.field public static final UNIT_SYSTEM_IMPERIAL:Ljava/lang/String; = "I"

.field public static final UNIT_SYSTEM_METRIC:Ljava/lang/String; = "M"


# instance fields
.field private name:Ljava/lang/String;

.field private preferences:Lcom/vectorwatch/android/models/UserSettingsModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    return-void
.end method


# virtual methods
.method public getPreferences()Lcom/vectorwatch/android/models/UserSettingsModel;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    return-object v0
.end method

.method public setActivityLevel(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "activityLevel"    # Ljava/lang/Integer;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setActivityLevel(Ljava/lang/Integer;)V

    .line 55
    return-void
.end method

.method public setAlarmsSettings(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "alarms":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setAlarmSettings(Ljava/util/List;)V

    .line 75
    return-void
.end method

.method public setBirthday(Ljava/lang/Long;)V
    .locals 1
    .param p1, "birthday"    # Ljava/lang/Long;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setBirthday(Ljava/lang/Long;)V

    .line 43
    return-void
.end method

.method public setCaloriesGoal(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "caloriesGoal"    # Ljava/lang/Integer;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setCaloriesGoal(Ljava/lang/Integer;)V

    .line 59
    return-void
.end method

.method public setDistanceGoal(Ljava/lang/Float;)V
    .locals 1
    .param p1, "distanceGoal"    # Ljava/lang/Float;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setDistanceGoal(Ljava/lang/Float;)V

    .line 63
    return-void
.end method

.method public setGender(Ljava/lang/String;)V
    .locals 1
    .param p1, "gender"    # Ljava/lang/String;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setGender(Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public setHeight(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "height"    # Ljava/lang/Integer;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setHeight(Ljava/lang/Integer;)V

    .line 51
    return-void
.end method

.method public setIntensity(I)V
    .locals 3
    .param p1, "intensity"    # I

    .prologue
    .line 78
    iget-object v1, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/UserSettingsModel;->getBackLightSettings()Lcom/vectorwatch/android/models/BackLightItem;

    move-result-object v0

    .line 79
    .local v0, "item":Lcom/vectorwatch/android/models/BackLightItem;
    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lcom/vectorwatch/android/models/BackLightItem;

    .end local v0    # "item":Lcom/vectorwatch/android/models/BackLightItem;
    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/models/BackLightItem;-><init>(II)V

    .line 82
    .restart local v0    # "item":Lcom/vectorwatch/android/models/BackLightItem;
    :cond_0
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/BackLightItem;->setIntensity(I)V

    .line 83
    iget-object v1, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->setBackLightSettings(Lcom/vectorwatch/android/models/BackLightItem;)V

    .line 84
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->name:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setNotifList(Lcom/vectorwatch/android/models/NotificationSettingsItem;)V
    .locals 1
    .param p1, "notifSettings"    # Lcom/vectorwatch/android/models/NotificationSettingsItem;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setNotifiedApps(Lcom/vectorwatch/android/models/NotificationSettingsItem;)V

    .line 101
    return-void
.end method

.method public setSecondsHand(Z)V
    .locals 2
    .param p1, "active"    # Z

    .prologue
    .line 96
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setSecondsHand(Ljava/lang/Boolean;)V

    .line 97
    return-void
.end method

.method public setSleepGoal(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "sleepGoal"    # Ljava/lang/Integer;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setSleepGoal(Ljava/lang/Integer;)V

    .line 71
    return-void
.end method

.method public setStepsGoal(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "stepsGoal"    # Ljava/lang/Integer;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setStepsGoal(Ljava/lang/Integer;)V

    .line 67
    return-void
.end method

.method public setTimeFormat(Ljava/lang/String;)V
    .locals 1
    .param p1, "timeFormat"    # Ljava/lang/String;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setTimeFormat(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public setTimeout(I)V
    .locals 3
    .param p1, "timeout"    # I

    .prologue
    .line 87
    iget-object v1, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/UserSettingsModel;->getBackLightSettings()Lcom/vectorwatch/android/models/BackLightItem;

    move-result-object v0

    .line 88
    .local v0, "item":Lcom/vectorwatch/android/models/BackLightItem;
    if-nez v0, :cond_0

    .line 89
    new-instance v0, Lcom/vectorwatch/android/models/BackLightItem;

    .end local v0    # "item":Lcom/vectorwatch/android/models/BackLightItem;
    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/models/BackLightItem;-><init>(II)V

    .line 91
    .restart local v0    # "item":Lcom/vectorwatch/android/models/BackLightItem;
    :cond_0
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/BackLightItem;->setTimeout(I)V

    .line 92
    iget-object v1, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->setBackLightSettings(Lcom/vectorwatch/android/models/BackLightItem;)V

    .line 93
    return-void
.end method

.method public setUnitSystem(Ljava/lang/String;)V
    .locals 1
    .param p1, "unitSystem"    # Ljava/lang/String;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setUnitSystem(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public setUserSettings(Lcom/vectorwatch/android/models/UserSettings;)V
    .locals 1
    .param p1, "settings"    # Lcom/vectorwatch/android/models/UserSettings;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setSettings(Lcom/vectorwatch/android/models/UserSettings;)V

    .line 105
    return-void
.end method

.method public setWeight(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "weight"    # Ljava/lang/Integer;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/models/LocalSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserSettingsModel;->setWeight(Ljava/lang/Integer;)V

    .line 47
    return-void
.end method
