.class public Lcom/vectorwatch/android/models/WatchHand;
.super Ljava/lang/Object;
.source "WatchHand.java"


# static fields
.field public static final HOUR_BITMAP:Ljava/lang/String; = "hour_bitmap"

.field public static final HOUR_NORMAL:Ljava/lang/String; = "hour_normal"

.field public static final MINUTE_BITMAP:Ljava/lang/String; = "minute_bitmap"

.field public static final MINUTE_NORMAL:Ljava/lang/String; = "minute_normal"

.field public static final SECOND_BITMAP:Ljava/lang/String; = "second_bitmap"

.field public static final SECOND_NORMAL:Ljava/lang/String; = "second_normal"


# instance fields
.field private image:Lcom/vectorwatch/android/models/ResourceReference;

.field private length:Ljava/lang/Integer;

.field private offset:Ljava/lang/Integer;

.field private placement:Lcom/vectorwatch/android/models/PlacementPosition;

.field private shadow:Ljava/lang/Integer;

.field private thickness:Ljava/lang/Integer;

.field private type:Ljava/lang/String;

.field private zIndex:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getImage()Lcom/vectorwatch/android/models/ResourceReference;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchHand;->image:Lcom/vectorwatch/android/models/ResourceReference;

    return-object v0
.end method

.method public getLength()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchHand;->length:Ljava/lang/Integer;

    return-object v0
.end method

.method public getOffset()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchHand;->offset:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPlacement()Lcom/vectorwatch/android/models/PlacementPosition;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchHand;->placement:Lcom/vectorwatch/android/models/PlacementPosition;

    return-object v0
.end method

.method public getShadow()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchHand;->shadow:Ljava/lang/Integer;

    return-object v0
.end method

.method public getThickness()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchHand;->thickness:Ljava/lang/Integer;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchHand;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getzIndex()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchHand;->zIndex:Ljava/lang/Integer;

    return-object v0
.end method
