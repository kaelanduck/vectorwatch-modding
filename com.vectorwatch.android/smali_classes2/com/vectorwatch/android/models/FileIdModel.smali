.class public Lcom/vectorwatch/android/models/FileIdModel;
.super Ljava/lang/Object;
.source "FileIdModel.java"


# instance fields
.field private id:I

.field private type:Lcom/vectorwatch/android/utils/Constants$VftpFileType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/vectorwatch/android/models/FileIdModel;->id:I

    return v0
.end method

.method public getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vectorwatch/android/models/FileIdModel;->type:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    return-object v0
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/vectorwatch/android/models/FileIdModel;->id:I

    .line 26
    return-void
.end method

.method public setType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V
    .locals 0
    .param p1, "type"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/vectorwatch/android/models/FileIdModel;->type:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 18
    return-void
.end method
