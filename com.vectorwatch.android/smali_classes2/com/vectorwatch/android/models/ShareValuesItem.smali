.class public Lcom/vectorwatch/android/models/ShareValuesItem;
.super Ljava/lang/Object;
.source "ShareValuesItem.java"


# instance fields
.field private calories:Lcom/vectorwatch/android/models/CloudGoalValueModel;

.field private distance:Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

.field private sleep:Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

.field private steps:Lcom/vectorwatch/android/models/CloudGoalValueModel;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/CloudGoalValueModel;Lcom/vectorwatch/android/models/CloudGoalValueModel;Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;)V
    .locals 0
    .param p1, "steps"    # Lcom/vectorwatch/android/models/CloudGoalValueModel;
    .param p2, "calories"    # Lcom/vectorwatch/android/models/CloudGoalValueModel;
    .param p3, "distance"    # Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;
    .param p4, "sleep"    # Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->steps:Lcom/vectorwatch/android/models/CloudGoalValueModel;

    .line 13
    iput-object p2, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->calories:Lcom/vectorwatch/android/models/CloudGoalValueModel;

    .line 14
    iput-object p4, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->sleep:Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    .line 15
    iput-object p3, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->distance:Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    .line 16
    return-void
.end method


# virtual methods
.method public getCalories()Lcom/vectorwatch/android/models/CloudGoalValueModel;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->calories:Lcom/vectorwatch/android/models/CloudGoalValueModel;

    return-object v0
.end method

.method public getDistance()Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->distance:Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    return-object v0
.end method

.method public getSleep()Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->sleep:Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    return-object v0
.end method

.method public getSteps()Lcom/vectorwatch/android/models/CloudGoalValueModel;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->steps:Lcom/vectorwatch/android/models/CloudGoalValueModel;

    return-object v0
.end method

.method public setCalories(Lcom/vectorwatch/android/models/CloudGoalValueModel;)V
    .locals 0
    .param p1, "calories"    # Lcom/vectorwatch/android/models/CloudGoalValueModel;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->calories:Lcom/vectorwatch/android/models/CloudGoalValueModel;

    .line 32
    return-void
.end method

.method public setDistance(Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;)V
    .locals 0
    .param p1, "distance"    # Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->distance:Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    .line 48
    return-void
.end method

.method public setSleep(Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;)V
    .locals 0
    .param p1, "sleep"    # Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->sleep:Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;

    .line 40
    return-void
.end method

.method public setSteps(Lcom/vectorwatch/android/models/CloudGoalValueModel;)V
    .locals 0
    .param p1, "steps"    # Lcom/vectorwatch/android/models/CloudGoalValueModel;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/ShareValuesItem;->steps:Lcom/vectorwatch/android/models/CloudGoalValueModel;

    .line 24
    return-void
.end method
