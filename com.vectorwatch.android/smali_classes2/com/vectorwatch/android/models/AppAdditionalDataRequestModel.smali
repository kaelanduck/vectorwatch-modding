.class public Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;
.super Lcom/vectorwatch/android/models/BaseCloudRequestModel;
.source "AppAdditionalDataRequestModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;
    }
.end annotation


# instance fields
.field public appEventData:Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel$EventData;

.field public args:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public location:Lcom/vectorwatch/android/models/VectorLocation;

.field public remoteMethod:Lcom/vectorwatch/android/models/Element$RemoteMethod;

.field public userSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public setSingleValueInArgs(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/Integer;

    .prologue
    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->args:Ljava/util/List;

    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->args:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    return-void
.end method

.method public setValuePairsInArgs(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "value1"    # Ljava/lang/Integer;
    .param p2, "value2"    # Ljava/lang/Integer;

    .prologue
    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->args:Ljava/util/List;

    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->args:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;->args:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    return-void
.end method
