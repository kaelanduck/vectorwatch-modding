.class public Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;
.super Ljava/lang/Object;
.source "EventDestination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/EventDestination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ButtonWatch"
.end annotation


# instance fields
.field public destinationEndpoint:Ljava/lang/String;

.field public destinationType:Ljava/lang/String;

.field public elementId:Ljava/lang/Integer;

.field public elementType:Ljava/lang/String;

.field public eventType:Ljava/lang/String;

.field public remoteMethod:Lcom/vectorwatch/android/models/Element$RemoteMethod;

.field public requirements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vectorwatch/android/models/EventDestination;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/EventDestination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/models/EventDestination;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->this$0:Lcom/vectorwatch/android/models/EventDestination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDestinationEndpoint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->destinationEndpoint:Ljava/lang/String;

    return-object v0
.end method

.method public getDestinationType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->destinationType:Ljava/lang/String;

    return-object v0
.end method

.method public getEventType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->eventType:Ljava/lang/String;

    return-object v0
.end method

.method public setDestinationEndpoint(Ljava/lang/String;)V
    .locals 0
    .param p1, "destinationEndpoint"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->destinationEndpoint:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setDestinationType(Ljava/lang/String;)V
    .locals 0
    .param p1, "destinationType"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->destinationType:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setEventType(Ljava/lang/String;)V
    .locals 0
    .param p1, "eventType"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->eventType:Ljava/lang/String;

    .line 56
    return-void
.end method
