.class public Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;
.super Ljava/lang/Object;
.source "LoginResponseModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/LoginResponseModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LoginResponseData"
.end annotation


# instance fields
.field private accountType:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field final synthetic this$0:Lcom/vectorwatch/android/models/LoginResponseModel;

.field private updateType:Ljava/lang/String;

.field private username:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/LoginResponseModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/models/LoginResponseModel;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->this$0:Lcom/vectorwatch/android/models/LoginResponseModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccountType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->accountType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->updateType:Ljava/lang/String;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->username:Ljava/lang/String;

    return-object v0
.end method

.method public setAccountType(Ljava/lang/String;)V
    .locals 0
    .param p1, "accountType"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->accountType:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->name:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setUpdateType(Ljava/lang/String;)V
    .locals 0
    .param p1, "updateType"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->updateType:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0
    .param p1, "username"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->username:Ljava/lang/String;

    .line 36
    return-void
.end method
