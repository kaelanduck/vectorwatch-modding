.class public Lcom/vectorwatch/android/models/EventDestination;
.super Ljava/lang/Object;
.source "EventDestination.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;
    }
.end annotation


# instance fields
.field public BTN1:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;",
            ">;"
        }
    .end annotation
.end field

.field public BTN2:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;",
            ">;"
        }
    .end annotation
.end field

.field public BTN3:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method


# virtual methods
.method public getBTN1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/models/EventDestination;->BTN1:Ljava/util/List;

    return-object v0
.end method

.method public getBTN2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/models/EventDestination;->BTN2:Ljava/util/List;

    return-object v0
.end method

.method public getBTN3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/models/EventDestination;->BTN3:Ljava/util/List;

    return-object v0
.end method

.method public setBTN1(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "BTN1":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/EventDestination;->BTN1:Ljava/util/List;

    .line 22
    return-void
.end method

.method public setBTN2(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "BTN2":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/EventDestination;->BTN2:Ljava/util/List;

    .line 30
    return-void
.end method

.method public setBTN3(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "BTN3":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/EventDestination;->BTN3:Ljava/util/List;

    .line 38
    return-void
.end method
