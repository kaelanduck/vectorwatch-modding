.class public Lcom/vectorwatch/android/models/FileMetadataModel;
.super Ljava/lang/Object;
.source "FileMetadataModel.java"


# instance fields
.field private fileId:Lcom/vectorwatch/android/models/FileIdModel;

.field private md5:[B

.field private size:S


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFileId()Lcom/vectorwatch/android/models/FileIdModel;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/vectorwatch/android/models/FileMetadataModel;->fileId:Lcom/vectorwatch/android/models/FileIdModel;

    return-object v0
.end method

.method public getMd5()[B
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/models/FileMetadataModel;->md5:[B

    return-object v0
.end method

.method public getSize()S
    .locals 1

    .prologue
    .line 20
    iget-short v0, p0, Lcom/vectorwatch/android/models/FileMetadataModel;->size:S

    return v0
.end method

.method public setFileId(Lcom/vectorwatch/android/models/FileIdModel;)V
    .locals 0
    .param p1, "fileId"    # Lcom/vectorwatch/android/models/FileIdModel;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/models/FileMetadataModel;->fileId:Lcom/vectorwatch/android/models/FileIdModel;

    .line 17
    return-void
.end method

.method public setMd5([B)V
    .locals 0
    .param p1, "md5"    # [B

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vectorwatch/android/models/FileMetadataModel;->md5:[B

    .line 33
    return-void
.end method

.method public setSize(S)V
    .locals 0
    .param p1, "size"    # S

    .prologue
    .line 24
    iput-short p1, p0, Lcom/vectorwatch/android/models/FileMetadataModel;->size:S

    .line 25
    return-void
.end method
