.class public Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;
.super Ljava/lang/Object;
.source "CloudGoalValueFloatModel.java"


# instance fields
.field private current:F

.field private goal:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0
    .param p1, "goal"    # F
    .param p2, "current"    # F

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;->goal:F

    .line 12
    iput p2, p0, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;->current:F

    .line 13
    return-void
.end method


# virtual methods
.method public getCurrent()F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;->current:F

    return v0
.end method

.method public getGoal()F
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;->goal:F

    return v0
.end method

.method public setCurrent(F)V
    .locals 0
    .param p1, "current"    # F

    .prologue
    .line 28
    iput p1, p0, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;->current:F

    .line 29
    return-void
.end method

.method public setGoal(F)V
    .locals 0
    .param p1, "goal"    # F

    .prologue
    .line 20
    iput p1, p0, Lcom/vectorwatch/android/models/CloudGoalValueFloatModel;->goal:F

    .line 21
    return-void
.end method
