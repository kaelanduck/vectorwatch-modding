.class public Lcom/vectorwatch/android/models/UpdateAppDetailsModel;
.super Ljava/lang/Object;
.source "UpdateAppDetailsModel.java"


# instance fields
.field private appUuid:Ljava/lang/String;

.field private streamChannels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/UpdateStreamChannelModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;->appUuid:Ljava/lang/String;

    return-object v0
.end method

.method public getStreamChannels()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/UpdateStreamChannelModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;->streamChannels:Ljava/util/List;

    return-object v0
.end method

.method public setAppUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "appUuid"    # Ljava/lang/String;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;->appUuid:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public setStreamChannels(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/UpdateStreamChannelModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "streamChannels":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateStreamChannelModel;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;->streamChannels:Ljava/util/List;

    .line 19
    return-void
.end method
