.class public Lcom/vectorwatch/android/models/NewsletterItem;
.super Ljava/lang/Object;
.source "NewsletterItem.java"


# instance fields
.field private name:Ljava/lang/String;

.field private subscribed:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "subscribed"    # Z

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/vectorwatch/android/models/NewsletterItem;->name:Ljava/lang/String;

    .line 15
    iput-boolean p2, p0, Lcom/vectorwatch/android/models/NewsletterItem;->subscribed:Z

    .line 16
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/NewsletterItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method public isSubscribed()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/NewsletterItem;->subscribed:Z

    return v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/NewsletterItem;->name:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setSubscribed(Z)V
    .locals 0
    .param p1, "subscribed"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/NewsletterItem;->subscribed:Z

    .line 32
    return-void
.end method
