.class public Lcom/vectorwatch/android/models/StreamLogItem;
.super Ljava/lang/Object;
.source "StreamLogItem.java"


# instance fields
.field private channelLabel:Ljava/lang/String;

.field private lastReceived:J

.field private streamUuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1
    .param p1, "channelLabel"    # Ljava/lang/String;
    .param p2, "streamUuid"    # Ljava/lang/String;
    .param p3, "lastReceived"    # J

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamLogItem;->channelLabel:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/vectorwatch/android/models/StreamLogItem;->streamUuid:Ljava/lang/String;

    .line 15
    iput-wide p3, p0, Lcom/vectorwatch/android/models/StreamLogItem;->lastReceived:J

    .line 16
    return-void
.end method


# virtual methods
.method public getChannelLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamLogItem;->channelLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getLastReceived()J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/vectorwatch/android/models/StreamLogItem;->lastReceived:J

    return-wide v0
.end method

.method public getStreamUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamLogItem;->streamUuid:Ljava/lang/String;

    return-object v0
.end method

.method public setChannelLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "channelLabel"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamLogItem;->channelLabel:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setLastReceived(J)V
    .locals 1
    .param p1, "lastReceived"    # J

    .prologue
    .line 39
    iput-wide p1, p0, Lcom/vectorwatch/android/models/StreamLogItem;->lastReceived:J

    .line 40
    return-void
.end method

.method public setStreamUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "streamUuid"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamLogItem;->streamUuid:Ljava/lang/String;

    .line 32
    return-void
.end method
