.class public Lcom/vectorwatch/android/models/UserSettingsModel;
.super Ljava/lang/Object;
.source "UserSettingsModel.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private activityLevel:Ljava/lang/Integer;

.field private alarmSettings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;",
            ">;"
        }
    .end annotation
.end field

.field private backLightSettings:Lcom/vectorwatch/android/models/BackLightItem;

.field private birthday:Ljava/lang/Long;

.field private caloriesGoal:Ljava/lang/Integer;

.field private distanceGoal:Ljava/lang/Float;

.field private gender:Ljava/lang/String;

.field private height:Ljava/lang/Integer;

.field private notifiedApps:Lcom/vectorwatch/android/models/NotificationSettingsItem;

.field private secondsHand:Ljava/lang/Boolean;

.field private settings:Lcom/vectorwatch/android/models/UserSettings;

.field private sleepGoal:Ljava/lang/Integer;

.field private stepsGoal:Ljava/lang/Integer;

.field private tFormat:Ljava/lang/String;

.field private unitSystem:Ljava/lang/String;

.field private weight:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getActivityLevel()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->activityLevel:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAlarmSettings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->alarmSettings:Ljava/util/List;

    return-object v0
.end method

.method public getBackLightSettings()Lcom/vectorwatch/android/models/BackLightItem;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->backLightSettings:Lcom/vectorwatch/android/models/BackLightItem;

    return-object v0
.end method

.method public getBirthday()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->birthday:Ljava/lang/Long;

    return-object v0
.end method

.method public getCaloriesGoal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->caloriesGoal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getDistanceGoal()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->distanceGoal:Ljava/lang/Float;

    return-object v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->gender:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->height:Ljava/lang/Integer;

    return-object v0
.end method

.method public getNotifiedApps()Lcom/vectorwatch/android/models/NotificationSettingsItem;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->notifiedApps:Lcom/vectorwatch/android/models/NotificationSettingsItem;

    return-object v0
.end method

.method public getSecondsHand()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->secondsHand:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getSettings()Lcom/vectorwatch/android/models/UserSettings;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->settings:Lcom/vectorwatch/android/models/UserSettings;

    return-object v0
.end method

.method public getSleepGoal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->sleepGoal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getStepsGoal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->stepsGoal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getTimeFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->tFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getUnitSystem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->unitSystem:Ljava/lang/String;

    return-object v0
.end method

.method public getWeight()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->weight:Ljava/lang/Integer;

    return-object v0
.end method

.method public setActivityLevel(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "activityLevel"    # Ljava/lang/Integer;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->activityLevel:Ljava/lang/Integer;

    .line 82
    return-void
.end method

.method public setAlarmSettings(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "alarmSettings":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->alarmSettings:Ljava/util/List;

    .line 122
    return-void
.end method

.method public setBackLightSettings(Lcom/vectorwatch/android/models/BackLightItem;)V
    .locals 0
    .param p1, "backLightSettings"    # Lcom/vectorwatch/android/models/BackLightItem;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->backLightSettings:Lcom/vectorwatch/android/models/BackLightItem;

    .line 134
    return-void
.end method

.method public setBirthday(Ljava/lang/Long;)V
    .locals 0
    .param p1, "birthday"    # Ljava/lang/Long;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->birthday:Ljava/lang/Long;

    .line 58
    return-void
.end method

.method public setCaloriesGoal(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "caloriesGoal"    # Ljava/lang/Integer;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->caloriesGoal:Ljava/lang/Integer;

    .line 90
    return-void
.end method

.method public setDistanceGoal(Ljava/lang/Float;)V
    .locals 0
    .param p1, "distanceGoal"    # Ljava/lang/Float;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->distanceGoal:Ljava/lang/Float;

    .line 98
    return-void
.end method

.method public setGender(Ljava/lang/String;)V
    .locals 0
    .param p1, "gender"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->gender:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setHeight(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "height"    # Ljava/lang/Integer;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->height:Ljava/lang/Integer;

    .line 74
    return-void
.end method

.method public setNotifiedApps(Lcom/vectorwatch/android/models/NotificationSettingsItem;)V
    .locals 0
    .param p1, "notifiedApps"    # Lcom/vectorwatch/android/models/NotificationSettingsItem;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->notifiedApps:Lcom/vectorwatch/android/models/NotificationSettingsItem;

    .line 146
    return-void
.end method

.method public setSecondsHand(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "secondsHand"    # Ljava/lang/Boolean;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->secondsHand:Ljava/lang/Boolean;

    .line 138
    return-void
.end method

.method public setSettings(Lcom/vectorwatch/android/models/UserSettings;)V
    .locals 0
    .param p1, "settings"    # Lcom/vectorwatch/android/models/UserSettings;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->settings:Lcom/vectorwatch/android/models/UserSettings;

    .line 154
    return-void
.end method

.method public setSleepGoal(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "sleepGoal"    # Ljava/lang/Integer;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->sleepGoal:Ljava/lang/Integer;

    .line 114
    return-void
.end method

.method public setStepsGoal(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "stepsGoal"    # Ljava/lang/Integer;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->stepsGoal:Ljava/lang/Integer;

    .line 106
    return-void
.end method

.method public setTimeFormat(Ljava/lang/String;)V
    .locals 0
    .param p1, "tFormat"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->tFormat:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setUnitSystem(Ljava/lang/String;)V
    .locals 0
    .param p1, "unitSystem"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->unitSystem:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setWeight(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "weight"    # Ljava/lang/Integer;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserSettingsModel;->weight:Ljava/lang/Integer;

    .line 66
    return-void
.end method
