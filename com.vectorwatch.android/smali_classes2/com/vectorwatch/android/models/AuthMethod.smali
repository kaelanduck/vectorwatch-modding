.class public Lcom/vectorwatch/android/models/AuthMethod;
.super Ljava/lang/Object;
.source "AuthMethod.java"


# instance fields
.field private loginUrl:Ljava/lang/String;

.field private protocol:Ljava/lang/String;

.field private version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLoginUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/models/AuthMethod;->loginUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/vectorwatch/android/models/AuthMethod;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vectorwatch/android/models/AuthMethod;->version:Ljava/lang/String;

    return-object v0
.end method

.method public setLoginUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vectorwatch/android/models/AuthMethod;->loginUrl:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setProtocol(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/AuthMethod;->protocol:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/vectorwatch/android/models/AuthMethod;->version:Ljava/lang/String;

    .line 29
    return-void
.end method
