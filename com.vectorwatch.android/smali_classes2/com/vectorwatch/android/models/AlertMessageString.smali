.class public Lcom/vectorwatch/android/models/AlertMessageString;
.super Ljava/lang/Object;
.source "AlertMessageString.java"


# instance fields
.field private content:Ljava/lang/String;

.field private length:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "length"    # I
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/vectorwatch/android/models/AlertMessageString;->length:I

    .line 13
    iput-object p2, p0, Lcom/vectorwatch/android/models/AlertMessageString;->content:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessageString;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/vectorwatch/android/models/AlertMessageString;->length:I

    return v0
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessageString;->content:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/vectorwatch/android/models/AlertMessageString;->length:I

    .line 30
    return-void
.end method
