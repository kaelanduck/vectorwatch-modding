.class public final enum Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;
.super Ljava/lang/Enum;
.source "Stream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/Stream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SettingsDefaultTypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

.field public static final enum DIGITAL_TIME:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

.field public static final enum LIVE_STREAM:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

.field public static final enum SYSTEM_VARS:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

.field public static final enum TIMEZONE:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;


# instance fields
.field private val:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 191
    new-instance v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    const-string v1, "DIGITAL_TIME"

    const-string v2, "DIGITAL_TIME"

    invoke-direct {v0, v1, v3, v2}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->DIGITAL_TIME:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    .line 192
    new-instance v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    const-string v1, "SYSTEM_VARS"

    const-string v2, "SYSTEM_VARS"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->SYSTEM_VARS:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    .line 193
    new-instance v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    const-string v1, "TIMEZONE"

    const-string v2, "TIMEZONE"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->TIMEZONE:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    .line 194
    new-instance v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    const-string v1, "LIVE_STREAM"

    const-string v2, "LIVE_STREAM"

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->LIVE_STREAM:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    .line 190
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    sget-object v1, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->DIGITAL_TIME:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->SYSTEM_VARS:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->TIMEZONE:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->LIVE_STREAM:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->$VALUES:[Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "val"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 198
    iput-object p3, p0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->val:Ljava/lang/String;

    .line 199
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 190
    const-class v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->$VALUES:[Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    return-object v0
.end method


# virtual methods
.method public getVal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->val:Ljava/lang/String;

    return-object v0
.end method
