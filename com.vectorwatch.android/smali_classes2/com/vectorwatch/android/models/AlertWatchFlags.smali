.class public Lcom/vectorwatch/android/models/AlertWatchFlags;
.super Ljava/lang/Object;
.source "AlertWatchFlags.java"


# instance fields
.field private forced:Z

.field private silent:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0
    .param p1, "forced"    # Z
    .param p2, "silent"    # Z

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/AlertWatchFlags;->forced:Z

    .line 14
    iput-boolean p2, p0, Lcom/vectorwatch/android/models/AlertWatchFlags;->silent:Z

    .line 15
    return-void
.end method


# virtual methods
.method public getForced()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/AlertWatchFlags;->forced:Z

    return v0
.end method

.method public getSilent()Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/AlertWatchFlags;->silent:Z

    return v0
.end method

.method public setForced(Z)V
    .locals 0
    .param p1, "forced"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/AlertWatchFlags;->forced:Z

    .line 31
    return-void
.end method

.method public setSilent(Z)V
    .locals 0
    .param p1, "silent"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/AlertWatchFlags;->silent:Z

    .line 23
    return-void
.end method
