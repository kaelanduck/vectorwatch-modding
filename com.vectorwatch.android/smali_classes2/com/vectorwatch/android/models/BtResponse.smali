.class public Lcom/vectorwatch/android/models/BtResponse;
.super Ljava/lang/Object;
.source "BtResponse.java"


# instance fields
.field public final data:[B

.field public final uuid:Ljava/util/UUID;


# direct methods
.method public constructor <init>(Ljava/util/UUID;[B)V
    .locals 0
    .param p1, "uuid"    # Ljava/util/UUID;
    .param p2, "data"    # [B

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/vectorwatch/android/models/BtResponse;->uuid:Ljava/util/UUID;

    .line 11
    iput-object p2, p0, Lcom/vectorwatch/android/models/BtResponse;->data:[B

    .line 12
    return-void
.end method
