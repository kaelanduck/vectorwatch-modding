.class public Lcom/vectorwatch/android/models/ShareServerResponse;
.super Ljava/lang/Object;
.source "ShareServerResponse.java"


# instance fields
.field private data:Lcom/vectorwatch/android/models/ShareServerData;

.field private error:Ljava/lang/String;

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Lcom/vectorwatch/android/models/ShareServerData;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/models/ShareServerResponse;->data:Lcom/vectorwatch/android/models/ShareServerData;

    return-object v0
.end method

.method public getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/models/ShareServerResponse;->error:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vectorwatch/android/models/ShareServerResponse;->message:Ljava/lang/String;

    return-object v0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/vectorwatch/android/models/ShareServerResponse;->message:Ljava/lang/String;

    .line 18
    return-void
.end method
