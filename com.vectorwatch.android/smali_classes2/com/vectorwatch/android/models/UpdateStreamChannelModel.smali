.class public Lcom/vectorwatch/android/models/UpdateStreamChannelModel;
.super Ljava/lang/Object;
.source "UpdateStreamChannelModel.java"


# instance fields
.field private streamChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

.field private streamUuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getStreamChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;->streamChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    return-object v0
.end method

.method public getStreamUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;->streamUuid:Ljava/lang/String;

    return-object v0
.end method

.method public setStreamChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V
    .locals 0
    .param p1, "streamChannelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;->streamChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 15
    return-void
.end method

.method public setStreamUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "streamUuid"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;->streamUuid:Ljava/lang/String;

    .line 19
    return-void
.end method
