.class public Lcom/vectorwatch/android/models/AlertCallbacks;
.super Ljava/lang/Object;
.source "AlertCallbacks.java"


# instance fields
.field private eval:Ljava/lang/String;

.field private index:I

.field private key:I


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "eval"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "key"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertCallbacks;->eval:Ljava/lang/String;

    .line 14
    iput p2, p0, Lcom/vectorwatch/android/models/AlertCallbacks;->index:I

    .line 15
    iput p3, p0, Lcom/vectorwatch/android/models/AlertCallbacks;->key:I

    .line 16
    return-void
.end method


# virtual methods
.method public getEval()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertCallbacks;->eval:Ljava/lang/String;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/vectorwatch/android/models/AlertCallbacks;->index:I

    return v0
.end method

.method public getKey()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/vectorwatch/android/models/AlertCallbacks;->key:I

    return v0
.end method

.method public setEval(Ljava/lang/String;)V
    .locals 0
    .param p1, "eval"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertCallbacks;->eval:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/vectorwatch/android/models/AlertCallbacks;->index:I

    .line 32
    return-void
.end method

.method public setKey(I)V
    .locals 0
    .param p1, "key"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/vectorwatch/android/models/AlertCallbacks;->key:I

    .line 40
    return-void
.end method
