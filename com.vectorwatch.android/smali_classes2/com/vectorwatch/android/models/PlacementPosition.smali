.class public Lcom/vectorwatch/android/models/PlacementPosition;
.super Ljava/lang/Object;
.source "PlacementPosition.java"


# instance fields
.field private posX:Ljava/lang/Integer;

.field private posY:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPosX()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/vectorwatch/android/models/PlacementPosition;->posX:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPosY()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vectorwatch/android/models/PlacementPosition;->posY:Ljava/lang/Integer;

    return-object v0
.end method

.method public setPosX(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "posX"    # Ljava/lang/Integer;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vectorwatch/android/models/PlacementPosition;->posX:Ljava/lang/Integer;

    .line 20
    return-void
.end method

.method public setPosY(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "posY"    # Ljava/lang/Integer;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/PlacementPosition;->posY:Ljava/lang/Integer;

    .line 24
    return-void
.end method
