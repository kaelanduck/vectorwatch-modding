.class Lcom/vectorwatch/android/models/StreamSettingOptionsResponse$Data;
.super Ljava/lang/Object;
.source "StreamSettingOptionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Data"
.end annotation


# instance fields
.field private options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;


# direct methods
.method private constructor <init>(Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamSettingOptionsResponse$Data;->this$0:Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamSettingOptionsResponse$Data;->options:Ljava/util/List;

    return-object v0
.end method
