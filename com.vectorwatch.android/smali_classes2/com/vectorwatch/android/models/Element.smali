.class public Lcom/vectorwatch/android/models/Element;
.super Ljava/lang/Object;
.source "Element.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/Element$RemoteMethod;,
        Lcom/vectorwatch/android/models/Element$DataSource;,
        Lcom/vectorwatch/android/models/Element$Type;
    }
.end annotation


# instance fields
.field private alignment:Ljava/lang/String;

.field private datasource:Lcom/vectorwatch/android/models/Element$DataSource;

.field private defaultStreamChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

.field private defaultStreamUUID:Ljava/lang/String;

.field private defaultValue:Ljava/lang/String;

.field private fontResource:Lcom/vectorwatch/android/models/ResourceReference;

.field private id:Ljava/lang/Integer;

.field private image:Lcom/vectorwatch/android/models/ResourceReference;

.field private imgResource:Lcom/vectorwatch/android/models/ResourceReference;

.field private inverse:Ljava/lang/Boolean;

.field private length:Ljava/lang/Integer;

.field private maxHeight:Ljava/lang/Integer;

.field private maxWidth:Ljava/lang/Integer;

.field private offset:Ljava/lang/Integer;

.field private placement:Lcom/vectorwatch/android/models/PlacementPosition;

.field private shadow:Ljava/lang/Integer;

.field private thickness:Ljava/lang/Integer;

.field private type:Ljava/lang/String;

.field private zIndex:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    return-void
.end method


# virtual methods
.method public getAlignment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->alignment:Ljava/lang/String;

    return-object v0
.end method

.method public getDatasource()Lcom/vectorwatch/android/models/Element$DataSource;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->datasource:Lcom/vectorwatch/android/models/Element$DataSource;

    return-object v0
.end method

.method public getDefaultStreamChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->defaultStreamChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    return-object v0
.end method

.method public getDefaultStreamUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->defaultStreamUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->defaultValue:Ljava/lang/String;

    return-object v0
.end method

.method public getFontResource()Lcom/vectorwatch/android/models/ResourceReference;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->fontResource:Lcom/vectorwatch/android/models/ResourceReference;

    return-object v0
.end method

.method public getId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->id:Ljava/lang/Integer;

    return-object v0
.end method

.method public getImage()Lcom/vectorwatch/android/models/ResourceReference;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->image:Lcom/vectorwatch/android/models/ResourceReference;

    return-object v0
.end method

.method public getImgResource()Lcom/vectorwatch/android/models/ResourceReference;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->imgResource:Lcom/vectorwatch/android/models/ResourceReference;

    return-object v0
.end method

.method public getInverse()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->inverse:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getLength()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->length:Ljava/lang/Integer;

    return-object v0
.end method

.method public getMaxHeight()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->maxHeight:Ljava/lang/Integer;

    return-object v0
.end method

.method public getMaxWidth()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->maxWidth:Ljava/lang/Integer;

    return-object v0
.end method

.method public getOffset()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->offset:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPlacement()Lcom/vectorwatch/android/models/PlacementPosition;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->placement:Lcom/vectorwatch/android/models/PlacementPosition;

    return-object v0
.end method

.method public getShadow()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->shadow:Ljava/lang/Integer;

    return-object v0
.end method

.method public getThickness()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->thickness:Ljava/lang/Integer;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getzIndex()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element;->zIndex:Ljava/lang/Integer;

    return-object v0
.end method

.method public setAlignment(Ljava/lang/String;)V
    .locals 0
    .param p1, "alignment"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->alignment:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setDefaultStreamUUID(Ljava/lang/String;)V
    .locals 0
    .param p1, "defaultStreamUUID"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->defaultStreamUUID:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setFontResource(Lcom/vectorwatch/android/models/ResourceReference;)V
    .locals 0
    .param p1, "fontResource"    # Lcom/vectorwatch/android/models/ResourceReference;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->fontResource:Lcom/vectorwatch/android/models/ResourceReference;

    .line 123
    return-void
.end method

.method public setId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/Integer;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->id:Ljava/lang/Integer;

    .line 74
    return-void
.end method

.method public setImage(Lcom/vectorwatch/android/models/ResourceReference;)V
    .locals 0
    .param p1, "image"    # Lcom/vectorwatch/android/models/ResourceReference;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->image:Lcom/vectorwatch/android/models/ResourceReference;

    .line 191
    return-void
.end method

.method public setImgResource(Lcom/vectorwatch/android/models/ResourceReference;)V
    .locals 0
    .param p1, "imgResource"    # Lcom/vectorwatch/android/models/ResourceReference;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->imgResource:Lcom/vectorwatch/android/models/ResourceReference;

    .line 151
    return-void
.end method

.method public setInverse(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "inverse"    # Ljava/lang/Boolean;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->inverse:Ljava/lang/Boolean;

    .line 131
    return-void
.end method

.method public setLength(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "length"    # Ljava/lang/Integer;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->length:Ljava/lang/Integer;

    .line 159
    return-void
.end method

.method public setMaxHeight(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "maxHeight"    # Ljava/lang/Integer;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->maxHeight:Ljava/lang/Integer;

    .line 119
    return-void
.end method

.method public setMaxWidth(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "maxWidth"    # Ljava/lang/Integer;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->maxWidth:Ljava/lang/Integer;

    .line 115
    return-void
.end method

.method public setOffset(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "offset"    # Ljava/lang/Integer;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->offset:Ljava/lang/Integer;

    .line 199
    return-void
.end method

.method public setPlacement(Lcom/vectorwatch/android/models/PlacementPosition;)V
    .locals 0
    .param p1, "placement"    # Lcom/vectorwatch/android/models/PlacementPosition;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->placement:Lcom/vectorwatch/android/models/PlacementPosition;

    .line 139
    return-void
.end method

.method public setShadow(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "shadow"    # Ljava/lang/Integer;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->shadow:Ljava/lang/Integer;

    .line 175
    return-void
.end method

.method public setThickness(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "thickness"    # Ljava/lang/Integer;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->thickness:Ljava/lang/Integer;

    .line 167
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->type:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public setzIndex(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "zIndex"    # Ljava/lang/Integer;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/vectorwatch/android/models/Element;->zIndex:Ljava/lang/Integer;

    .line 183
    return-void
.end method
