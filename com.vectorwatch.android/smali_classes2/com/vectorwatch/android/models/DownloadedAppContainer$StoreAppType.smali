.class public final enum Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;
.super Ljava/lang/Enum;
.source "DownloadedAppContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/DownloadedAppContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StoreAppType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

.field public static final enum CLOUD:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

.field public static final enum SYSTEM_APP:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

.field public static final enum SYSTEM_WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

.field public static final enum WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    const-string v1, "WATCHFACE"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    new-instance v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    const-string v1, "CLOUD"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->CLOUD:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    new-instance v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    const-string v1, "SYSTEM_WATCHFACE"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->SYSTEM_WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    new-instance v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    const-string v1, "SYSTEM_APP"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->SYSTEM_APP:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    sget-object v1, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->CLOUD:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->SYSTEM_WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->SYSTEM_APP:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->$VALUES:[Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->$VALUES:[Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    return-object v0
.end method
