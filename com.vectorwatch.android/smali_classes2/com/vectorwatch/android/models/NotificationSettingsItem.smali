.class public Lcom/vectorwatch/android/models/NotificationSettingsItem;
.super Ljava/lang/Object;
.source "NotificationSettingsItem.java"


# instance fields
.field private enabled:Z

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Z)V
    .locals 0
    .param p2, "enable"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "listNotif":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-boolean p2, p0, Lcom/vectorwatch/android/models/NotificationSettingsItem;->enabled:Z

    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/models/NotificationSettingsItem;->list:Ljava/util/List;

    .line 17
    return-void
.end method


# virtual methods
.method public getList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/models/NotificationSettingsItem;->list:Ljava/util/List;

    return-object v0
.end method

.method public isEnable()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/NotificationSettingsItem;->enabled:Z

    return v0
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/NotificationSettingsItem;->enabled:Z

    .line 33
    return-void
.end method

.method public setList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/NotificationSettingsItem;->list:Ljava/util/List;

    .line 25
    return-void
.end method
