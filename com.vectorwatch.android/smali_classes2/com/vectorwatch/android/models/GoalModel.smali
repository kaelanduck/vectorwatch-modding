.class public Lcom/vectorwatch/android/models/GoalModel;
.super Ljava/lang/Object;
.source "GoalModel.java"


# static fields
.field public static final UNDEFINED:I = -0x1


# instance fields
.field private mType:Lcom/vectorwatch/android/utils/Constants$GoalType;

.field private mValue:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public constructor <init>(ILcom/vectorwatch/android/utils/Constants$GoalType;)V
    .locals 0
    .param p1, "value"    # I
    .param p2, "type"    # Lcom/vectorwatch/android/utils/Constants$GoalType;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/vectorwatch/android/models/GoalModel;->mValue:I

    .line 25
    iput-object p2, p0, Lcom/vectorwatch/android/models/GoalModel;->mType:Lcom/vectorwatch/android/utils/Constants$GoalType;

    .line 26
    return-void
.end method


# virtual methods
.method public getType()Lcom/vectorwatch/android/utils/Constants$GoalType;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/GoalModel;->mType:Lcom/vectorwatch/android/utils/Constants$GoalType;

    return-object v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/vectorwatch/android/models/GoalModel;->mValue:I

    return v0
.end method

.method public setType(Lcom/vectorwatch/android/utils/Constants$GoalType;)V
    .locals 0
    .param p1, "type"    # Lcom/vectorwatch/android/utils/Constants$GoalType;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vectorwatch/android/models/GoalModel;->mType:Lcom/vectorwatch/android/utils/Constants$GoalType;

    .line 34
    return-void
.end method

.method public setValue(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/vectorwatch/android/models/GoalModel;->mValue:I

    .line 42
    return-void
.end method
