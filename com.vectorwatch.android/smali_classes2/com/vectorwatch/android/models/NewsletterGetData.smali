.class public Lcom/vectorwatch/android/models/NewsletterGetData;
.super Ljava/lang/Object;
.source "NewsletterGetData.java"


# instance fields
.field private newsletters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 12
    .local p1, "newsletters":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/NewsletterItem;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/models/NewsletterGetData;->newsletters:Ljava/util/List;

    .line 14
    return-void
.end method


# virtual methods
.method public getNewsletters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/models/NewsletterGetData;->newsletters:Ljava/util/List;

    return-object v0
.end method

.method public setNewsletters(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "newsletters":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/NewsletterItem;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/NewsletterGetData;->newsletters:Ljava/util/List;

    .line 22
    return-void
.end method
