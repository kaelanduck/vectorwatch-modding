.class public Lcom/vectorwatch/android/models/ChangeComplicationModel;
.super Ljava/lang/Object;
.source "ChangeComplicationModel.java"


# instance fields
.field private appId:I

.field private elementId:B

.field private elementType:Lcom/vectorwatch/android/utils/Constants$ElementType;

.field private length:B

.field private value:Ljava/lang/String;

.field private watchfaceId:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppId()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->appId:I

    return v0
.end method

.method public getElementId()B
    .locals 1

    .prologue
    .line 33
    iget-byte v0, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->elementId:B

    return v0
.end method

.method public getElementType()Lcom/vectorwatch/android/utils/Constants$ElementType;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->elementType:Lcom/vectorwatch/android/utils/Constants$ElementType;

    return-object v0
.end method

.method public getLength()B
    .locals 1

    .prologue
    .line 57
    iget-byte v0, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->length:B

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->value:Ljava/lang/String;

    return-object v0
.end method

.method public getWatchfaceId()B
    .locals 1

    .prologue
    .line 25
    iget-byte v0, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->watchfaceId:B

    return v0
.end method

.method public setAppId(I)V
    .locals 0
    .param p1, "appId"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->appId:I

    .line 22
    return-void
.end method

.method public setElementId(B)V
    .locals 0
    .param p1, "elementId"    # B

    .prologue
    .line 37
    iput-byte p1, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->elementId:B

    .line 38
    return-void
.end method

.method public setElementType(Lcom/vectorwatch/android/utils/Constants$ElementType;)V
    .locals 0
    .param p1, "elementType"    # Lcom/vectorwatch/android/utils/Constants$ElementType;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->elementType:Lcom/vectorwatch/android/utils/Constants$ElementType;

    .line 46
    return-void
.end method

.method public setLength(B)V
    .locals 0
    .param p1, "length"    # B

    .prologue
    .line 61
    iput-byte p1, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->length:B

    .line 62
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->value:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setWatchfaceId(B)V
    .locals 0
    .param p1, "watchfaceId"    # B

    .prologue
    .line 29
    iput-byte p1, p0, Lcom/vectorwatch/android/models/ChangeComplicationModel;->watchfaceId:B

    .line 30
    return-void
.end method
