.class public Lcom/vectorwatch/android/models/AuthCredentialsRequest;
.super Lcom/vectorwatch/android/models/BaseCloudRequestModel;
.source "AuthCredentialsRequest.java"


# instance fields
.field public location:Lcom/vectorwatch/android/models/VectorLocation;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/models/AuthCredentialsRequest;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 13
    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/VectorLocation;)V
    .locals 1
    .param p1, "location"    # Lcom/vectorwatch/android/models/VectorLocation;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/models/AuthCredentialsRequest;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 16
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/models/AuthCredentialsRequest;->setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/models/AuthCredentialsRequest;

    .line 17
    return-void
.end method


# virtual methods
.method public setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/models/AuthCredentialsRequest;
    .locals 0
    .param p1, "location"    # Lcom/vectorwatch/android/models/VectorLocation;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/models/AuthCredentialsRequest;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 22
    return-object p0
.end method
