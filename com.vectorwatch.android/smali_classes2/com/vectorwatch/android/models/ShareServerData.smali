.class public Lcom/vectorwatch/android/models/ShareServerData;
.super Ljava/lang/Object;
.source "ShareServerData.java"


# instance fields
.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/vectorwatch/android/models/ShareServerData;->url:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vectorwatch/android/models/ShareServerData;->url:Ljava/lang/String;

    return-object v0
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/vectorwatch/android/models/ShareServerData;->url:Ljava/lang/String;

    .line 20
    return-void
.end method
