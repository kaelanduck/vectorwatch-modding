.class public Lcom/vectorwatch/android/models/AppOptionsRequest;
.super Lcom/vectorwatch/android/models/BaseCloudRequestModel;
.source "AppOptionsRequest.java"


# instance fields
.field public location:Lcom/vectorwatch/android/models/VectorLocation;

.field public settingName:Ljava/lang/String;

.field public userSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;"
        }
    .end annotation
.end field

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1, "settingName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p3, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    invoke-direct {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/models/AppOptionsRequest;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 17
    iput-object p1, p0, Lcom/vectorwatch/android/models/AppOptionsRequest;->settingName:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/vectorwatch/android/models/AppOptionsRequest;->value:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lcom/vectorwatch/android/models/AppOptionsRequest;->userSettings:Ljava/util/Map;

    .line 20
    return-void
.end method


# virtual methods
.method public setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/models/AppOptionsRequest;
    .locals 0
    .param p1, "location"    # Lcom/vectorwatch/android/models/VectorLocation;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vectorwatch/android/models/AppOptionsRequest;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 25
    return-object p0
.end method
