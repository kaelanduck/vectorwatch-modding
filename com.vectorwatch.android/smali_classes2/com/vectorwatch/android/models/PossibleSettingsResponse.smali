.class public Lcom/vectorwatch/android/models/PossibleSettingsResponse;
.super Ljava/lang/Object;
.source "PossibleSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/PossibleSettingsResponse$StreamPossibleSettingsData;
    }
.end annotation


# instance fields
.field private data:Lcom/vectorwatch/android/models/PossibleSettingsResponse$StreamPossibleSettingsData;

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/vectorwatch/android/models/PossibleSettingsResponse;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/models/PossibleSettingsResponse;->data:Lcom/vectorwatch/android/models/PossibleSettingsResponse$StreamPossibleSettingsData;

    if-eqz v0, :cond_0

    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/PossibleSettingsResponse;->data:Lcom/vectorwatch/android/models/PossibleSettingsResponse$StreamPossibleSettingsData;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/PossibleSettingsResponse$StreamPossibleSettingsData;->getPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    .line 21
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
