.class public Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;
.super Ljava/lang/Object;
.source "AlarmCloudDetailsModel.java"


# instance fields
.field private enabled:Ljava/lang/Boolean;

.field private hour:Ljava/lang/Integer;

.field private minute:Ljava/lang/Integer;

.field private title:Ljava/lang/String;

.field private weekDays:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHour()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->hour:Ljava/lang/Integer;

    return-object v0
.end method

.method public getIsEnabled()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->enabled:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getMinute()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->minute:Ljava/lang/Integer;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getWeekDays()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->weekDays:Ljava/util/List;

    return-object v0
.end method

.method public setHour(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "hour"    # Ljava/lang/Integer;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->hour:Ljava/lang/Integer;

    .line 38
    return-void
.end method

.method public setIsEnabled(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "isEnabled"    # Ljava/lang/Boolean;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->enabled:Ljava/lang/Boolean;

    .line 42
    return-void
.end method

.method public setMinute(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "minute"    # Ljava/lang/Integer;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->minute:Ljava/lang/Integer;

    .line 46
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->title:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setWeekDays(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "weekDays":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->weekDays:Ljava/util/List;

    .line 54
    return-void
.end method
