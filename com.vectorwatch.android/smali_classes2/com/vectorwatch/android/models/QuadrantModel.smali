.class public Lcom/vectorwatch/android/models/QuadrantModel;
.super Ljava/lang/Object;
.source "QuadrantModel.java"


# instance fields
.field private angle:F

.field private curentValue:F

.field private fullRotation:I

.field private maxRotation:I

.field private maxValue:F


# direct methods
.method public constructor <init>(FFFII)V
    .locals 0
    .param p1, "angle"    # F
    .param p2, "maxValue"    # F
    .param p3, "curentValue"    # F
    .param p4, "maxRotation"    # I
    .param p5, "fullRotation"    # I

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput p1, p0, Lcom/vectorwatch/android/models/QuadrantModel;->angle:F

    .line 64
    iput p2, p0, Lcom/vectorwatch/android/models/QuadrantModel;->maxValue:F

    .line 65
    iput p3, p0, Lcom/vectorwatch/android/models/QuadrantModel;->curentValue:F

    .line 66
    iput p4, p0, Lcom/vectorwatch/android/models/QuadrantModel;->maxRotation:I

    .line 67
    iput p5, p0, Lcom/vectorwatch/android/models/QuadrantModel;->fullRotation:I

    .line 68
    return-void
.end method

.method public constructor <init>(FIFII)V
    .locals 1
    .param p1, "angle"    # F
    .param p2, "maxValue"    # I
    .param p3, "curentValue"    # F
    .param p4, "maxRotation"    # I
    .param p5, "fullRotation"    # I

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput p1, p0, Lcom/vectorwatch/android/models/QuadrantModel;->angle:F

    .line 56
    int-to-float v0, p2

    iput v0, p0, Lcom/vectorwatch/android/models/QuadrantModel;->maxValue:F

    .line 57
    iput p3, p0, Lcom/vectorwatch/android/models/QuadrantModel;->curentValue:F

    .line 58
    iput p4, p0, Lcom/vectorwatch/android/models/QuadrantModel;->maxRotation:I

    .line 59
    iput p5, p0, Lcom/vectorwatch/android/models/QuadrantModel;->fullRotation:I

    .line 60
    return-void
.end method


# virtual methods
.method public getAngle()F
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/vectorwatch/android/models/QuadrantModel;->angle:F

    return v0
.end method

.method public getCurentValue()F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/vectorwatch/android/models/QuadrantModel;->curentValue:F

    return v0
.end method

.method public getFullRotation()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/vectorwatch/android/models/QuadrantModel;->fullRotation:I

    return v0
.end method

.method public getMaxRotation()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/vectorwatch/android/models/QuadrantModel;->maxRotation:I

    return v0
.end method

.method public getMaxValue()F
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/vectorwatch/android/models/QuadrantModel;->maxValue:F

    return v0
.end method

.method public setAngle(F)V
    .locals 0
    .param p1, "angle"    # F

    .prologue
    .line 19
    iput p1, p0, Lcom/vectorwatch/android/models/QuadrantModel;->angle:F

    .line 20
    return-void
.end method

.method public setCurentValue(F)V
    .locals 0
    .param p1, "curentValue"    # F

    .prologue
    .line 35
    iput p1, p0, Lcom/vectorwatch/android/models/QuadrantModel;->curentValue:F

    .line 36
    return-void
.end method

.method public setFullRotation(I)V
    .locals 0
    .param p1, "fullRotation"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/vectorwatch/android/models/QuadrantModel;->fullRotation:I

    .line 52
    return-void
.end method

.method public setMaxRotation(I)V
    .locals 0
    .param p1, "maxRotation"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/vectorwatch/android/models/QuadrantModel;->maxRotation:I

    .line 44
    return-void
.end method

.method public setMaxValue(I)V
    .locals 1
    .param p1, "maxValue"    # I

    .prologue
    .line 27
    int-to-float v0, p1

    iput v0, p0, Lcom/vectorwatch/android/models/QuadrantModel;->maxValue:F

    .line 28
    return-void
.end method
