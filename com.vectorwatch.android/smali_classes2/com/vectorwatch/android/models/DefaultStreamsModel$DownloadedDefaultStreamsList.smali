.class public Lcom/vectorwatch/android/models/DefaultStreamsModel$DownloadedDefaultStreamsList;
.super Ljava/lang/Object;
.source "DefaultStreamsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/DefaultStreamsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DownloadedDefaultStreamsList"
.end annotation


# instance fields
.field public streams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vectorwatch/android/models/DefaultStreamsModel;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/DefaultStreamsModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/models/DefaultStreamsModel;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vectorwatch/android/models/DefaultStreamsModel$DownloadedDefaultStreamsList;->this$0:Lcom/vectorwatch/android/models/DefaultStreamsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getStreams()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/models/DefaultStreamsModel$DownloadedDefaultStreamsList;->streams:Ljava/util/List;

    return-object v0
.end method
