.class public Lcom/vectorwatch/android/models/AppContent;
.super Ljava/lang/Object;
.source "AppContent.java"


# instance fields
.field public appFile:Lcom/vectorwatch/android/models/AppFile;

.field public appSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Lcom/vectorwatch/android/models/settings/PossibleSettings;",
            ">;"
        }
    .end annotation
.end field

.field public localResources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/LocalResource;",
            ">;"
        }
    .end annotation
.end field

.field public pVersion:Ljava/lang/String;

.field public watchfaces:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Watchface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
