.class public Lcom/vectorwatch/android/models/ResourceReference;
.super Ljava/lang/Object;
.source "ResourceReference.java"


# instance fields
.field private resourceId:Ljava/lang/Long;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getResourceId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/vectorwatch/android/models/ResourceReference;->resourceId:Ljava/lang/Long;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vectorwatch/android/models/ResourceReference;->type:Ljava/lang/String;

    return-object v0
.end method
