.class public Lcom/vectorwatch/android/models/CloudGoalValueModel;
.super Ljava/lang/Object;
.source "CloudGoalValueModel.java"


# instance fields
.field private current:I

.field private goal:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "goal"    # I
    .param p2, "current"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/vectorwatch/android/models/CloudGoalValueModel;->goal:I

    .line 12
    iput p2, p0, Lcom/vectorwatch/android/models/CloudGoalValueModel;->current:I

    .line 13
    return-void
.end method


# virtual methods
.method public getCurrent()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/vectorwatch/android/models/CloudGoalValueModel;->current:I

    return v0
.end method

.method public getGoal()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/models/CloudGoalValueModel;->goal:I

    return v0
.end method

.method public setCurrent(I)V
    .locals 0
    .param p1, "current"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/vectorwatch/android/models/CloudGoalValueModel;->current:I

    .line 29
    return-void
.end method

.method public setGoal(I)V
    .locals 0
    .param p1, "goal"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/vectorwatch/android/models/CloudGoalValueModel;->goal:I

    .line 21
    return-void
.end method
