.class public Lcom/vectorwatch/android/models/AlertMessage;
.super Ljava/lang/Object;
.source "AlertMessage.java"


# instance fields
.field private appId:Ljava/lang/Integer;

.field private callbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AlertCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private content:Ljava/lang/String;

.field private displayTime:Ljava/lang/Integer;

.field private flags:Lcom/vectorwatch/android/models/AlertWatchFlags;

.field private label:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/AlertWatchFlags;Ljava/util/List;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/Integer;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "content"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "displayTime"    # Ljava/lang/Integer;
    .param p6, "flags"    # Lcom/vectorwatch/android/models/AlertWatchFlags;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/vectorwatch/android/models/AlertWatchFlags;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AlertCallbacks;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p7, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlertCallbacks;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessage;->appId:Ljava/lang/Integer;

    .line 20
    iput-object p2, p0, Lcom/vectorwatch/android/models/AlertMessage;->title:Ljava/lang/String;

    .line 21
    iput-object p3, p0, Lcom/vectorwatch/android/models/AlertMessage;->content:Ljava/lang/String;

    .line 22
    iput-object p4, p0, Lcom/vectorwatch/android/models/AlertMessage;->label:Ljava/lang/String;

    .line 23
    iput-object p5, p0, Lcom/vectorwatch/android/models/AlertMessage;->displayTime:Ljava/lang/Integer;

    .line 24
    iput-object p6, p0, Lcom/vectorwatch/android/models/AlertMessage;->flags:Lcom/vectorwatch/android/models/AlertWatchFlags;

    .line 25
    iput-object p7, p0, Lcom/vectorwatch/android/models/AlertMessage;->callbacks:Ljava/util/List;

    .line 26
    return-void
.end method


# virtual methods
.method public getAppId()I
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessage;->appId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getCallbacks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AlertCallbacks;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessage;->callbacks:Ljava/util/List;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessage;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayTime()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessage;->displayTime:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getFlags()Lcom/vectorwatch/android/models/AlertWatchFlags;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessage;->flags:Lcom/vectorwatch/android/models/AlertWatchFlags;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessage;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/models/AlertMessage;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setAppId(I)V
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 33
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/AlertMessage;->appId:Ljava/lang/Integer;

    .line 34
    return-void
.end method

.method public setCallbacks(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AlertCallbacks;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "callbacks":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlertCallbacks;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessage;->callbacks:Ljava/util/List;

    .line 82
    return-void
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessage;->content:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setDisplayTime(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "displayTime"    # Ljava/lang/Integer;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessage;->displayTime:Ljava/lang/Integer;

    .line 66
    return-void
.end method

.method public setFlags(Lcom/vectorwatch/android/models/AlertWatchFlags;)V
    .locals 0
    .param p1, "flags"    # Lcom/vectorwatch/android/models/AlertWatchFlags;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessage;->flags:Lcom/vectorwatch/android/models/AlertWatchFlags;

    .line 74
    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessage;->label:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/vectorwatch/android/models/AlertMessage;->title:Ljava/lang/String;

    .line 42
    return-void
.end method
