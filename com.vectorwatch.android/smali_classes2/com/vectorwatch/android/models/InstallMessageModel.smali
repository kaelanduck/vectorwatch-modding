.class public Lcom/vectorwatch/android/models/InstallMessageModel;
.super Ljava/lang/Object;
.source "InstallMessageModel.java"


# instance fields
.field private applicationPackage:Lcom/vectorwatch/android/models/ApplicationPackageModel;

.field private type:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/models/InstallMessageModel;->applicationPackage:Lcom/vectorwatch/android/models/ApplicationPackageModel;

    return-object v0
.end method

.method public getType()Lcom/vectorwatch/android/utils/Constants$MessageInstallType;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vectorwatch/android/models/InstallMessageModel;->type:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    return-object v0
.end method

.method public setApplicationPackage(Lcom/vectorwatch/android/models/ApplicationPackageModel;)V
    .locals 0
    .param p1, "applicationPackage"    # Lcom/vectorwatch/android/models/ApplicationPackageModel;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vectorwatch/android/models/InstallMessageModel;->applicationPackage:Lcom/vectorwatch/android/models/ApplicationPackageModel;

    .line 26
    return-void
.end method

.method public setType(Lcom/vectorwatch/android/utils/Constants$MessageInstallType;)V
    .locals 0
    .param p1, "type"    # Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/vectorwatch/android/models/InstallMessageModel;->type:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    .line 18
    return-void
.end method
