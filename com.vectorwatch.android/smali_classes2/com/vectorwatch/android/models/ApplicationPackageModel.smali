.class public Lcom/vectorwatch/android/models/ApplicationPackageModel;
.super Ljava/lang/Object;
.source "ApplicationPackageModel.java"


# instance fields
.field private appId:I

.field private metadataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/FileMetadataModel;",
            ">;"
        }
    .end annotation
.end field

.field name:Ljava/lang/String;

.field private nameLength:B

.field private numberOfFiles:B

.field private position:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppId()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->appId:I

    return v0
.end method

.method public getMetadataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/FileMetadataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->metadataList:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNameLength()B
    .locals 1

    .prologue
    .line 25
    iget-byte v0, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->nameLength:B

    return v0
.end method

.method public getNumberOfFiles()B
    .locals 1

    .prologue
    .line 41
    iget-byte v0, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->numberOfFiles:B

    return v0
.end method

.method public getPosition()B
    .locals 1

    .prologue
    .line 57
    iget-byte v0, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->position:B

    return v0
.end method

.method public setAppId(I)V
    .locals 0
    .param p1, "appId"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->appId:I

    .line 22
    return-void
.end method

.method public setMetadataList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/FileMetadataModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "metadataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->metadataList:Ljava/util/List;

    .line 54
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->name:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setNameLength(B)V
    .locals 0
    .param p1, "nameLength"    # B

    .prologue
    .line 29
    iput-byte p1, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->nameLength:B

    .line 30
    return-void
.end method

.method public setNumberOfFiles(B)V
    .locals 0
    .param p1, "numberOfFiles"    # B

    .prologue
    .line 45
    iput-byte p1, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->numberOfFiles:B

    .line 46
    return-void
.end method

.method public setPosition(B)V
    .locals 0
    .param p1, "position"    # B

    .prologue
    .line 61
    iput-byte p1, p0, Lcom/vectorwatch/android/models/ApplicationPackageModel;->position:B

    .line 62
    return-void
.end method
