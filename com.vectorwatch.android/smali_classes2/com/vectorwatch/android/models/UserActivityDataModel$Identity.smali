.class public Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;
.super Ljava/lang/Object;
.source "UserActivityDataModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/UserActivityDataModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Identity"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/models/UserActivityDataModel;

.field private ts:J

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/UserActivityDataModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/models/UserActivityDataModel;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;->this$0:Lcom/vectorwatch/android/models/UserActivityDataModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTs()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;->ts:J

    return-wide v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setTs(J)V
    .locals 1
    .param p1, "ts"    # J

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;->ts:J

    .line 55
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;->type:Ljava/lang/String;

    .line 47
    return-void
.end method
