.class public Lcom/vectorwatch/android/models/SleepInfo;
.super Ljava/lang/Object;
.source "SleepInfo.java"


# instance fields
.field private sleepStart:I

.field private sleepStop:I

.field private timestamp:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "sleepStart"    # I
    .param p2, "sleepStop"    # I
    .param p3, "timestamp"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/vectorwatch/android/models/SleepInfo;->sleepStart:I

    .line 12
    iput p2, p0, Lcom/vectorwatch/android/models/SleepInfo;->sleepStop:I

    .line 13
    iput p3, p0, Lcom/vectorwatch/android/models/SleepInfo;->timestamp:I

    .line 14
    return-void
.end method


# virtual methods
.method public getSleepStart()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/vectorwatch/android/models/SleepInfo;->sleepStart:I

    return v0
.end method

.method public getSleepStop()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/vectorwatch/android/models/SleepInfo;->sleepStop:I

    return v0
.end method

.method public getTimestamp()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/vectorwatch/android/models/SleepInfo;->timestamp:I

    return v0
.end method

.method public setSleepStart(I)V
    .locals 0
    .param p1, "sleepStart"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/vectorwatch/android/models/SleepInfo;->sleepStart:I

    .line 30
    return-void
.end method

.method public setSleepStop(I)V
    .locals 0
    .param p1, "sleepStop"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/vectorwatch/android/models/SleepInfo;->sleepStop:I

    .line 22
    return-void
.end method

.method public setTimestamp(I)V
    .locals 0
    .param p1, "timestamp"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/vectorwatch/android/models/SleepInfo;->timestamp:I

    .line 38
    return-void
.end method
