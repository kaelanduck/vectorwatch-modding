.class public final enum Lcom/vectorwatch/android/models/WatchType;
.super Ljava/lang/Enum;
.source "WatchType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/models/WatchType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/models/WatchType;

.field public static final enum ROUND_240:Lcom/vectorwatch/android/models/WatchType;

.field public static final enum SQUARE_144:Lcom/vectorwatch/android/models/WatchType;


# instance fields
.field private val:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/vectorwatch/android/models/WatchType;

    const-string v1, "ROUND_240"

    const-string v2, "ROUND_240"

    invoke-direct {v0, v1, v3, v2}, Lcom/vectorwatch/android/models/WatchType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    new-instance v0, Lcom/vectorwatch/android/models/WatchType;

    const-string v1, "SQUARE_144"

    const-string v2, "SQUARE_144"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/models/WatchType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    .line 3
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vectorwatch/android/models/WatchType;

    sget-object v1, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/models/WatchType;->$VALUES:[Lcom/vectorwatch/android/models/WatchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "val"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 8
    iput-object p3, p0, Lcom/vectorwatch/android/models/WatchType;->val:Ljava/lang/String;

    .line 9
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/models/WatchType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/vectorwatch/android/models/WatchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/WatchType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/models/WatchType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/vectorwatch/android/models/WatchType;->$VALUES:[Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/models/WatchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/models/WatchType;

    return-object v0
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchType;->val:Ljava/lang/String;

    return-object v0
.end method
