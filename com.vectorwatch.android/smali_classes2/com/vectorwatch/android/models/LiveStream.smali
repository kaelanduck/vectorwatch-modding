.class public Lcom/vectorwatch/android/models/LiveStream;
.super Lcom/vectorwatch/android/models/Stream;
.source "LiveStream.java"


# instance fields
.field private appId:I

.field private nextUpdate:I

.field private refreshInterval:I

.field private value:Ljava/lang/String;

.field private watchfaceId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/vectorwatch/android/models/Stream;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppId()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/vectorwatch/android/models/LiveStream;->appId:I

    return v0
.end method

.method public getNextUpdate()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/vectorwatch/android/models/LiveStream;->nextUpdate:I

    return v0
.end method

.method public getRefreshInterval()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/vectorwatch/android/models/LiveStream;->refreshInterval:I

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/models/LiveStream;->value:Ljava/lang/String;

    return-object v0
.end method

.method public getWatchfaceId()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/vectorwatch/android/models/LiveStream;->watchfaceId:I

    return v0
.end method

.method public setAppId(I)V
    .locals 0
    .param p1, "appId"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/vectorwatch/android/models/LiveStream;->appId:I

    .line 22
    return-void
.end method

.method public setNextUpdate(I)V
    .locals 0
    .param p1, "nextUpdate"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/vectorwatch/android/models/LiveStream;->nextUpdate:I

    .line 48
    return-void
.end method

.method public setRefreshInterval(I)V
    .locals 0
    .param p1, "refreshInterval"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/vectorwatch/android/models/LiveStream;->refreshInterval:I

    .line 56
    return-void
.end method

.method public setStream(Lcom/vectorwatch/android/models/Stream;)V
    .locals 1
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 67
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->id:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->id:Ljava/lang/Integer;

    .line 68
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->uuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->uuid:Ljava/lang/String;

    .line 69
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->name:Ljava/lang/String;

    .line 70
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->labelText:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->labelText:Ljava/lang/String;

    .line 71
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->description:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->description:Ljava/lang/String;

    .line 72
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->img:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->img:Ljava/lang/String;

    .line 73
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->contentVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->contentVersion:Ljava/lang/String;

    .line 74
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->streamPossibleSettings:Lcom/vectorwatch/android/models/settings/PossibleSettings;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->streamPossibleSettings:Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .line 75
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->streamType:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->streamType:Ljava/lang/String;

    .line 76
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->supportedType:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->supportedType:Ljava/lang/String;

    .line 77
    iget-object v0, p1, Lcom/vectorwatch/android/models/Stream;->streamData:Lcom/vectorwatch/android/models/StreamValueModel;

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->streamData:Lcom/vectorwatch/android/models/StreamValueModel;

    .line 78
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/vectorwatch/android/models/Stream;->setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 79
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-super {p0, v0}, Lcom/vectorwatch/android/models/Stream;->setRequireUserAuth(Z)V

    .line 80
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getDynamic()Z

    move-result v0

    invoke-super {p0, v0}, Lcom/vectorwatch/android/models/Stream;->setDynamic(Z)V

    .line 81
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/vectorwatch/android/models/Stream;->setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V

    .line 82
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vectorwatch/android/models/LiveStream;->value:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setWatchfaceId(I)V
    .locals 0
    .param p1, "watchfaceId"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/vectorwatch/android/models/LiveStream;->watchfaceId:I

    .line 30
    return-void
.end method
