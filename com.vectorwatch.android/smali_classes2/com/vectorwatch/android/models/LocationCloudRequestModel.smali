.class public Lcom/vectorwatch/android/models/LocationCloudRequestModel;
.super Lcom/vectorwatch/android/models/BaseCloudRequestModel;
.source "LocationCloudRequestModel.java"


# instance fields
.field private latitude:D

.field private longitude:D


# direct methods
.method public constructor <init>(DD)V
    .locals 1
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 11
    iput-wide p1, p0, Lcom/vectorwatch/android/models/LocationCloudRequestModel;->latitude:D

    .line 12
    iput-wide p3, p0, Lcom/vectorwatch/android/models/LocationCloudRequestModel;->longitude:D

    .line 13
    return-void
.end method


# virtual methods
.method public getLatitude()D
    .locals 2

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/vectorwatch/android/models/LocationCloudRequestModel;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/vectorwatch/android/models/LocationCloudRequestModel;->longitude:D

    return-wide v0
.end method

.method public setLatitude(D)V
    .locals 1
    .param p1, "latitude"    # D

    .prologue
    .line 20
    iput-wide p1, p0, Lcom/vectorwatch/android/models/LocationCloudRequestModel;->latitude:D

    .line 21
    return-void
.end method

.method public setLongitude(D)V
    .locals 1
    .param p1, "longitude"    # D

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/vectorwatch/android/models/LocationCloudRequestModel;->longitude:D

    .line 29
    return-void
.end method
