.class public Lcom/vectorwatch/android/models/CloudRequestModel;
.super Lcom/vectorwatch/android/models/BaseCloudRequestModel;
.source "CloudRequestModel.java"


# instance fields
.field public andFilters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public from:Ljava/lang/Integer;

.field public orFilters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public size:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1, "startIndex"    # Ljava/lang/Integer;
    .param p2, "count"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Filter;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Filter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p3, "orFilters":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Filter;>;"
    .local p4, "andFilters":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Filter;>;"
    invoke-direct {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 16
    iput-object p3, p0, Lcom/vectorwatch/android/models/CloudRequestModel;->orFilters:Ljava/util/List;

    .line 17
    iput-object p4, p0, Lcom/vectorwatch/android/models/CloudRequestModel;->andFilters:Ljava/util/List;

    .line 18
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudRequestModel;->from:Ljava/lang/Integer;

    .line 19
    iput-object p2, p0, Lcom/vectorwatch/android/models/CloudRequestModel;->size:Ljava/lang/Integer;

    .line 20
    return-void
.end method
