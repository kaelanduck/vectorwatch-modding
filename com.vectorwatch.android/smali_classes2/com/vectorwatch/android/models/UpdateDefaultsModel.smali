.class public Lcom/vectorwatch/android/models/UpdateDefaultsModel;
.super Ljava/lang/Object;
.source "UpdateDefaultsModel.java"


# instance fields
.field private appVersion:Ljava/lang/String;

.field private buildNumber:Ljava/lang/String;

.field private compatibility:Ljava/lang/String;

.field private currentApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/UpdateAppDetailsModel;",
            ">;"
        }
    .end annotation
.end field

.field private deviceType:Ljava/lang/String;

.field private kernelVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "android"

    iput-object v0, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->deviceType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public setAppVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "appVersion"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->appVersion:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setBuildNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "buildNumber"    # Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->buildNumber:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public setCompatibility(Ljava/lang/String;)V
    .locals 0
    .param p1, "compatibility"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->compatibility:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setCurrentApps(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/UpdateAppDetailsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "currentApps":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateAppDetailsModel;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->currentApps:Ljava/util/List;

    .line 22
    return-void
.end method

.method public setDeviceType(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->deviceType:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setKernelVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "kernelVersion"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->kernelVersion:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .prologue
    .line 45
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Build="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->buildNumber:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " | "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "compatibility="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->compatibility:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " | "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "appVersion="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->appVersion:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " | "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "kernelVersion="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->kernelVersion:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " | "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "deviceType="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->deviceType:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 48
    .local v2, "generalInfo":Ljava/lang/String;
    move-object v3, v2

    .line 49
    .local v3, "result":Ljava/lang/String;
    const-string v1, ""

    .line 51
    .local v1, "apps":Ljava/lang/String;
    iget-object v8, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->currentApps:Ljava/util/List;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->currentApps:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_1

    .line 52
    iget-object v8, p0, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->currentApps:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;

    .line 53
    .local v0, "appModel":Lcom/vectorwatch/android/models/UpdateAppDetailsModel;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " [ "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 54
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;->getAppUuid()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;->getStreamChannels()Ljava/util/List;

    move-result-object v7

    .line 57
    .local v7, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateStreamChannelModel;>;"
    const-string v6, ""

    .line 58
    .local v6, "streamString":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 59
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;

    .line 60
    .local v5, "streamModel":Lcom/vectorwatch/android/models/UpdateStreamChannelModel;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ( "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 61
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;->getStreamUuid()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 63
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;->getStreamChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v4

    .line 65
    .local v4, "settings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 67
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " )"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 68
    goto :goto_1

    .line 71
    .end local v4    # "settings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v5    # "streamModel":Lcom/vectorwatch/android/models/UpdateStreamChannelModel;
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " | "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 72
    goto/16 :goto_0

    .line 75
    .end local v0    # "appModel":Lcom/vectorwatch/android/models/UpdateAppDetailsModel;
    .end local v6    # "streamString":Ljava/lang/String;
    .end local v7    # "streams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateStreamChannelModel;>;"
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Apps = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 76
    return-object v3
.end method
