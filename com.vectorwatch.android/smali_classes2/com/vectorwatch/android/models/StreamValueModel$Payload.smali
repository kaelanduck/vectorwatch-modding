.class public Lcom/vectorwatch/android/models/StreamValueModel$Payload;
.super Ljava/lang/Object;
.source "StreamValueModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/StreamValueModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Payload"
.end annotation


# instance fields
.field private appID:Ljava/lang/Integer;

.field private channelLabel:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private fID:Ljava/lang/Integer;

.field private secondsToLive:Ljava/lang/Integer;

.field private streamUUID:Ljava/lang/String;

.field final synthetic this$0:Lcom/vectorwatch/android/models/StreamValueModel;

.field private ttl:Ljava/lang/Integer;

.field private type:Ljava/lang/Integer;

.field private wID:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/StreamValueModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/models/StreamValueModel;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->this$0:Lcom/vectorwatch/android/models/StreamValueModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppID()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->appID:Ljava/lang/Integer;

    return-object v0
.end method

.method public getChannelLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->channelLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondsToLive()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->secondsToLive:Ljava/lang/Integer;

    return-object v0
.end method

.method public getStreamUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->streamUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getTtl()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->ttl:Ljava/lang/Integer;

    return-object v0
.end method

.method public getType()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->type:Ljava/lang/Integer;

    return-object v0
.end method

.method public getfID()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->fID:Ljava/lang/Integer;

    return-object v0
.end method

.method public getwID()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->wID:Ljava/lang/Integer;

    return-object v0
.end method
