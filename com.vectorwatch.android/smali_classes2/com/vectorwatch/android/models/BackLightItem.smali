.class public Lcom/vectorwatch/android/models/BackLightItem;
.super Ljava/lang/Object;
.source "BackLightItem.java"


# instance fields
.field private intensity:I

.field private timeout:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "intensity"    # I
    .param p2, "timeout"    # I

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/vectorwatch/android/models/BackLightItem;->intensity:I

    .line 13
    iput p2, p0, Lcom/vectorwatch/android/models/BackLightItem;->timeout:I

    .line 14
    return-void
.end method


# virtual methods
.method public getIntensity()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/vectorwatch/android/models/BackLightItem;->intensity:I

    return v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/vectorwatch/android/models/BackLightItem;->timeout:I

    return v0
.end method

.method public setIntensity(I)V
    .locals 0
    .param p1, "intensity"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/vectorwatch/android/models/BackLightItem;->intensity:I

    .line 22
    return-void
.end method

.method public setTimeout(I)V
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/vectorwatch/android/models/BackLightItem;->timeout:I

    .line 30
    return-void
.end method
