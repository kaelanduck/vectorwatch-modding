.class public Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;
.super Ljava/lang/Object;
.source "SoftwareSingleUpdateModel.java"


# instance fields
.field private data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

.field private message:Lcom/vectorwatch/android/events/CloudMessageModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/SoftwareUpdateData;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    .line 18
    return-void
.end method


# virtual methods
.method public getContentBase64()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getContentBase64()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getId()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getSummary()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setContentBase64(Ljava/lang/String;)V
    .locals 1
    .param p1, "contentBase64"    # Ljava/lang/String;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setContentBase64(Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public setId(Ljava/lang/Long;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/Long;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setId(Ljava/lang/Long;)V

    .line 42
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 1
    .param p1, "summary"    # Ljava/lang/String;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setSummary(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setType(Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 1
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareSingleUpdateModel;->data:Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setVersion(Ljava/lang/String;)V

    .line 54
    return-void
.end method
