.class public final enum Lcom/vectorwatch/android/models/Element$Type;
.super Ljava/lang/Enum;
.source "Element.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/Element;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/models/Element$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum ANALOG_DAY_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum BACKGROUND_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum BITMAP_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum HOUR_BITMAP:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum HOUR_NORMAL:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum LINE_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum LONG_TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum MINUTE_BITMAP:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum MINUTE_NORMAL:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum SECOND_BITMAP:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum SECOND_NORMAL:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum STATIC_TIME_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

.field public static final enum TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;


# instance fields
.field private label:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "HOUR_BITMAP"

    const-string v2, "HOUR_BITMAP"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->HOUR_BITMAP:Lcom/vectorwatch/android/models/Element$Type;

    .line 11
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "MINUTE_BITMAP"

    const-string v2, "MINUTE_BITMAP"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->MINUTE_BITMAP:Lcom/vectorwatch/android/models/Element$Type;

    .line 12
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "SECOND_BITMAP"

    const-string v2, "SECOND_BITMAP"

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->SECOND_BITMAP:Lcom/vectorwatch/android/models/Element$Type;

    .line 13
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "HOUR_NORMAL"

    const-string v2, "HOUR_NORMAL"

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->HOUR_NORMAL:Lcom/vectorwatch/android/models/Element$Type;

    .line 14
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "MINUTE_NORMAL"

    const-string v2, "MINUTE_NORMAL"

    invoke-direct {v0, v1, v8, v2}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->MINUTE_NORMAL:Lcom/vectorwatch/android/models/Element$Type;

    .line 15
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "SECOND_NORMAL"

    const/4 v2, 0x5

    const-string v3, "SECOND_NORMAL"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->SECOND_NORMAL:Lcom/vectorwatch/android/models/Element$Type;

    .line 16
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "TEXT_ELEMENT"

    const/4 v2, 0x6

    const-string v3, "TEXT_ELEMENT"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    .line 17
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "LONG_TEXT_ELEMENT"

    const/4 v2, 0x7

    const-string v3, "LONG_TEXT_ELEMENT"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->LONG_TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    .line 18
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "BITMAP_ELEMENT"

    const/16 v2, 0x8

    const-string v3, "BITMAP_ELEMENT"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->BITMAP_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    .line 19
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "BACKGROUND_ELEMENT"

    const/16 v2, 0x9

    const-string v3, "BACKGROUND_ELEMENT"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->BACKGROUND_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    .line 20
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "ANALOG_DAY_ELEMENT"

    const/16 v2, 0xa

    const-string v3, "ANALOG_DAY_ELEMENT"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->ANALOG_DAY_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    .line 21
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "LINE_ELEMENT"

    const/16 v2, 0xb

    const-string v3, "LINE_ELEMENT"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->LINE_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    .line 22
    new-instance v0, Lcom/vectorwatch/android/models/Element$Type;

    const-string v1, "STATIC_TIME_ELEMENT"

    const/16 v2, 0xc

    const-string v3, "STATIC_TIME_ELEMENT"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/models/Element$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->STATIC_TIME_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    .line 9
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/vectorwatch/android/models/Element$Type;

    sget-object v1, Lcom/vectorwatch/android/models/Element$Type;->HOUR_BITMAP:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/models/Element$Type;->MINUTE_BITMAP:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/models/Element$Type;->SECOND_BITMAP:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/models/Element$Type;->HOUR_NORMAL:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/models/Element$Type;->MINUTE_NORMAL:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->SECOND_NORMAL:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->LONG_TEXT_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->BITMAP_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->BACKGROUND_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->ANALOG_DAY_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->LINE_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/models/Element$Type;->STATIC_TIME_ELEMENT:Lcom/vectorwatch/android/models/Element$Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/models/Element$Type;->$VALUES:[Lcom/vectorwatch/android/models/Element$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput-object p3, p0, Lcom/vectorwatch/android/models/Element$Type;->label:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/models/Element$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/vectorwatch/android/models/Element$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Element$Type;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/models/Element$Type;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/vectorwatch/android/models/Element$Type;->$VALUES:[Lcom/vectorwatch/android/models/Element$Type;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/models/Element$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/models/Element$Type;

    return-object v0
.end method


# virtual methods
.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vectorwatch/android/models/Element$Type;->label:Ljava/lang/String;

    return-object v0
.end method
