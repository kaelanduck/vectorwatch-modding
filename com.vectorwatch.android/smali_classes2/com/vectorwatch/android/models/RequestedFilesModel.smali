.class public Lcom/vectorwatch/android/models/RequestedFilesModel;
.super Ljava/lang/Object;
.source "RequestedFilesModel.java"


# instance fields
.field private files:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/FileIdModel;",
            ">;"
        }
    .end annotation
.end field

.field private numberOfFiles:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFiles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/FileIdModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/models/RequestedFilesModel;->files:Ljava/util/List;

    return-object v0
.end method

.method public getNumberOfFiles()B
    .locals 1

    .prologue
    .line 13
    iget-byte v0, p0, Lcom/vectorwatch/android/models/RequestedFilesModel;->numberOfFiles:B

    return v0
.end method

.method public setFiles(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/FileIdModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "files":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileIdModel;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/RequestedFilesModel;->files:Ljava/util/List;

    .line 26
    return-void
.end method

.method public setNumberOfFiles(B)V
    .locals 0
    .param p1, "numberOfFiles"    # B

    .prologue
    .line 17
    iput-byte p1, p0, Lcom/vectorwatch/android/models/RequestedFilesModel;->numberOfFiles:B

    .line 18
    return-void
.end method
