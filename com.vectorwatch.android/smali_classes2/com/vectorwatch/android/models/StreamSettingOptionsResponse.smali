.class public Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;
.super Ljava/lang/Object;
.source "StreamSettingOptionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/StreamSettingOptionsResponse$Data;
    }
.end annotation


# instance fields
.field private data:Lcom/vectorwatch/android/models/StreamSettingOptionsResponse$Data;

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;->data:Lcom/vectorwatch/android/models/StreamSettingOptionsResponse$Data;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;->data:Lcom/vectorwatch/android/models/StreamSettingOptionsResponse$Data;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StreamSettingOptionsResponse$Data;->getOptions()Ljava/util/List;

    move-result-object v0

    .line 22
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
