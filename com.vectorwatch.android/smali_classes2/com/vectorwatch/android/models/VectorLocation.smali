.class public Lcom/vectorwatch/android/models/VectorLocation;
.super Ljava/lang/Object;
.source "VectorLocation.java"


# instance fields
.field public accuracy:D

.field public altitude:D

.field public latitude:D

.field public longitude:D


# direct methods
.method public constructor <init>(DDDD)V
    .locals 1
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "altitude"    # D
    .param p7, "accuracy"    # D

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide p1, p0, Lcom/vectorwatch/android/models/VectorLocation;->latitude:D

    .line 21
    iput-wide p3, p0, Lcom/vectorwatch/android/models/VectorLocation;->longitude:D

    .line 22
    iput-wide p5, p0, Lcom/vectorwatch/android/models/VectorLocation;->altitude:D

    .line 23
    iput-wide p7, p0, Lcom/vectorwatch/android/models/VectorLocation;->accuracy:D

    .line 24
    return-void
.end method

.method public static fromLocation(Landroid/location/Location;)Lcom/vectorwatch/android/models/VectorLocation;
    .locals 10
    .param p0, "location"    # Landroid/location/Location;

    .prologue
    .line 28
    new-instance v1, Lcom/vectorwatch/android/models/VectorLocation;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {p0}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v8, v0

    invoke-direct/range {v1 .. v9}, Lcom/vectorwatch/android/models/VectorLocation;-><init>(DDDD)V

    return-object v1
.end method
