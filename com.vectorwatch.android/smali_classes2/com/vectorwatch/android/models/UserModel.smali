.class public Lcom/vectorwatch/android/models/UserModel;
.super Ljava/lang/Object;
.source "UserModel.java"


# instance fields
.field private deviceType:Ljava/lang/String;

.field private endpointArn:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private password:Ljava/lang/String;

.field private phoneManufacturer:Ljava/lang/String;

.field private phoneModel:Ljava/lang/String;

.field private phoneOsVersion:I

.field private preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

.field private registerForUpdates:Z

.field private username:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "endpointArn"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->endpointArn:Ljava/lang/String;

    .line 54
    const-string v0, "android"

    iput-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->deviceType:Ljava/lang/String;

    .line 55
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneManufacturer:Ljava/lang/String;

    .line 56
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneModel:Ljava/lang/String;

    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneOsVersion:I

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "installationId"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->username:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/vectorwatch/android/models/UserModel;->password:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "endpointArn"    # Ljava/lang/String;
    .param p4, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->username:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/vectorwatch/android/models/UserModel;->password:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/vectorwatch/android/models/UserModel;->endpointArn:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lcom/vectorwatch/android/models/UserModel;->deviceType:Ljava/lang/String;

    .line 47
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneManufacturer:Ljava/lang/String;

    .line 48
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneModel:Ljava/lang/String;

    .line 49
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneOsVersion:I

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "deviceType"    # Ljava/lang/String;
    .param p5, "registerForUpdates"    # Z
    .param p6, "endpointArn"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->username:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/vectorwatch/android/models/UserModel;->password:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/vectorwatch/android/models/UserModel;->name:Ljava/lang/String;

    .line 34
    iput-object p6, p0, Lcom/vectorwatch/android/models/UserModel;->endpointArn:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lcom/vectorwatch/android/models/UserModel;->deviceType:Ljava/lang/String;

    .line 36
    iput-boolean p5, p0, Lcom/vectorwatch/android/models/UserModel;->registerForUpdates:Z

    .line 37
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneManufacturer:Ljava/lang/String;

    .line 38
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneModel:Ljava/lang/String;

    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneOsVersion:I

    .line 40
    return-void
.end method


# virtual methods
.method public getDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->deviceType:Ljava/lang/String;

    return-object v0
.end method

.method public getEndpointArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->endpointArn:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->password:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneManufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneModel:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneOsVersion()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/vectorwatch/android/models/UserModel;->phoneOsVersion:I

    return v0
.end method

.method public getPreferences()Lcom/vectorwatch/android/models/UserSettingsModel;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserModel;->username:Ljava/lang/String;

    return-object v0
.end method

.method public isRegisterForUpdates()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/UserModel;->registerForUpdates:Z

    return v0
.end method

.method public setDeviceType(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->deviceType:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setEndpointArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "endpointArn"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->endpointArn:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->name:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->password:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setPhoneManufacturer(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneManufacturer"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->phoneManufacturer:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setPhoneModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneModel"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->phoneModel:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setPhoneOsVersion(I)V
    .locals 0
    .param p1, "phoneOsVersion"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/vectorwatch/android/models/UserModel;->phoneOsVersion:I

    .line 102
    return-void
.end method

.method public setPreferences(Lcom/vectorwatch/android/models/UserSettingsModel;)V
    .locals 0
    .param p1, "preferences"    # Lcom/vectorwatch/android/models/UserSettingsModel;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    .line 70
    return-void
.end method

.method public setRegisterForUpdates(Z)V
    .locals 0
    .param p1, "registerForUpdates"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/UserModel;->registerForUpdates:Z

    .line 90
    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0
    .param p1, "username"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserModel;->username:Ljava/lang/String;

    .line 74
    return-void
.end method
