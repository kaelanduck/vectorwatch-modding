.class public Lcom/vectorwatch/android/models/RemoteSettingsModel;
.super Ljava/lang/Object;
.source "RemoteSettingsModel.java"


# static fields
.field public static final GENDER_FEMALE:Ljava/lang/String; = "F"

.field public static final GENDER_MALE:Ljava/lang/String; = "M"

.field public static final TIME_FORMAT_12H:Ljava/lang/String; = "h12"

.field public static final TIME_FORMAT_24H:Ljava/lang/String; = "h24"

.field public static final UNIT_SYSTEM_IMPERIAL:Ljava/lang/String; = "I"

.field public static final UNIT_SYSTEM_METRIC:Ljava/lang/String; = "M"


# instance fields
.field private deviceType:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private preferences:Lcom/vectorwatch/android/models/UserSettingsModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getActivityLevel()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 74
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getActivityLevel()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getAlarmsSettings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 107
    const/4 v0, 0x0

    .line 109
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getAlarmSettings()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getBacklightSettings()Lcom/vectorwatch/android/models/BackLightItem;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x0

    .line 117
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getBackLightSettings()Lcom/vectorwatch/android/models/BackLightItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getBirthday()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 53
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getBirthday()Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public getCaloriesGoal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getCaloriesGoal()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->deviceType:Ljava/lang/String;

    return-object v0
.end method

.method public getDistanceGoal()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 86
    const/4 v0, 0x0

    .line 88
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getDistanceGoal()Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 44
    const/4 v0, 0x0

    .line 46
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getGender()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeight()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getHeight()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNotifs()Lcom/vectorwatch/android/models/NotificationSettingsItem;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getNotifiedApps()Lcom/vectorwatch/android/models/NotificationSettingsItem;

    move-result-object v0

    return-object v0
.end method

.method public getSecondsHandActive()Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getSecondsHand()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getSleepGoal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 102
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getSleepGoal()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getStepsGoal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 95
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getStepsGoal()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getTimeFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getTimeFormat()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUnitSystem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x0

    .line 39
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getUnitSystem()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUserSettings()Lcom/vectorwatch/android/models/UserSettings;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v0

    return-object v0
.end method

.method public getWeight()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    if-nez v0, :cond_0

    .line 58
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/RemoteSettingsModel;->preferences:Lcom/vectorwatch/android/models/UserSettingsModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/UserSettingsModel;->getWeight()Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method
