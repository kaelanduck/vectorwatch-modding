.class public Lcom/vectorwatch/android/models/AuthMethodResponse;
.super Ljava/lang/Object;
.source "AuthMethodResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/AuthMethodResponse$AuthMethodData;
    }
.end annotation


# instance fields
.field private data:Lcom/vectorwatch/android/models/AuthMethodResponse$AuthMethodData;

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method


# virtual methods
.method public getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vectorwatch/android/models/AuthMethodResponse;->data:Lcom/vectorwatch/android/models/AuthMethodResponse$AuthMethodData;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/models/AuthMethodResponse;->data:Lcom/vectorwatch/android/models/AuthMethodResponse$AuthMethodData;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/AuthMethodResponse$AuthMethodData;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;

    move-result-object v0

    .line 26
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/AuthMethodResponse;->message:Ljava/lang/String;

    return-object v0
.end method
