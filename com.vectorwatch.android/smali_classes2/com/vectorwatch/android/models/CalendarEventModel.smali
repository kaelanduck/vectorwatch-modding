.class public Lcom/vectorwatch/android/models/CalendarEventModel;
.super Ljava/lang/Object;
.source "CalendarEventModel.java"


# instance fields
.field private count:B

.field private endTimestamp:Ljava/util/Date;

.field private location:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private startTimestamp:Ljava/util/Date;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;
    .param p3, "startTimestamp"    # Ljava/util/Date;
    .param p4, "endTimestamp"    # Ljava/util/Date;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->name:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->location:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->startTimestamp:Ljava/util/Date;

    .line 26
    iput-object p4, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->endTimestamp:Ljava/util/Date;

    .line 27
    return-void
.end method


# virtual methods
.method public getEndTimestamp()Ljava/util/Date;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->endTimestamp:Ljava/util/Date;

    return-object v0
.end method

.method public getIndex()B
    .locals 1

    .prologue
    .line 39
    iget-byte v0, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->count:B

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getStartTimestamp()Ljava/util/Date;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->startTimestamp:Ljava/util/Date;

    return-object v0
.end method

.method public getUniqueLabel()Ljava/lang/String;
    .locals 5

    .prologue
    .line 30
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->location:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->startTimestamp:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->endTimestamp:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "label":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    invoke-static {v0}, Lorg/apache/commons/codec/digest/DigestUtils;->md5(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Lorg/apache/commons/codec/binary/Hex;->encodeHex([B)[C

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    .line 33
    .local v1, "md5Label":Ljava/lang/String;
    const-string v2, "Calendar"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Calendar Event "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    return-object v1
.end method

.method public setEndTimestamp(Ljava/util/Date;)V
    .locals 0
    .param p1, "endTimestamp"    # Ljava/util/Date;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->endTimestamp:Ljava/util/Date;

    .line 76
    return-void
.end method

.method public setIndex(B)V
    .locals 0
    .param p1, "count"    # B

    .prologue
    .line 43
    iput-byte p1, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->count:B

    .line 44
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->location:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->name:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setStartTimestamp(Ljava/util/Date;)V
    .locals 0
    .param p1, "startTimestamp"    # Ljava/util/Date;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/vectorwatch/android/models/CalendarEventModel;->startTimestamp:Ljava/util/Date;

    .line 68
    return-void
.end method
