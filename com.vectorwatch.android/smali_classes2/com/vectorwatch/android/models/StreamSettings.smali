.class public Lcom/vectorwatch/android/models/StreamSettings;
.super Ljava/lang/Object;
.source "StreamSettings.java"


# instance fields
.field public name:Ljava/lang/String;

.field public type:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamSettings;->name:Ljava/lang/String;

    .line 13
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamSettings;->name:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/vectorwatch/android/models/StreamSettings;->value:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/vectorwatch/android/models/StreamSettings;->type:Ljava/lang/String;

    .line 19
    return-void
.end method
