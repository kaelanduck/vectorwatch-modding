.class public Lcom/vectorwatch/android/models/Watchface;
.super Ljava/lang/Object;
.source "Watchface.java"


# instance fields
.field private actions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private elements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Element;",
            ">;"
        }
    .end annotation
.end field

.field private eventsDestination:Lcom/vectorwatch/android/models/EventDestination;

.field private id:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getActions()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/vectorwatch/android/models/Watchface;->actions:Ljava/util/Map;

    return-object v0
.end method

.method public getElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Element;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/models/Watchface;->elements:Ljava/util/List;

    return-object v0
.end method

.method public getEventsDestination()Lcom/vectorwatch/android/models/EventDestination;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/models/Watchface;->eventsDestination:Lcom/vectorwatch/android/models/EventDestination;

    return-object v0
.end method

.method public getId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/models/Watchface;->id:Ljava/lang/Integer;

    return-object v0
.end method

.method public setElements(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Element;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "elements":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/Watchface;->elements:Ljava/util/List;

    .line 39
    return-void
.end method

.method public setEventsDestination(Lcom/vectorwatch/android/models/EventDestination;)V
    .locals 0
    .param p1, "eventsDestination"    # Lcom/vectorwatch/android/models/EventDestination;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/vectorwatch/android/models/Watchface;->eventsDestination:Lcom/vectorwatch/android/models/EventDestination;

    .line 47
    return-void
.end method
