.class public Lcom/vectorwatch/android/models/SignUpReturnModel;
.super Ljava/lang/Object;
.source "SignUpReturnModel.java"


# instance fields
.field private authToken:Ljava/lang/String;

.field private errorCode:Ljava/lang/Integer;

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/models/SignUpReturnModel;->authToken:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorCode()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/models/SignUpReturnModel;->errorCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/vectorwatch/android/models/SignUpReturnModel;->message:Ljava/lang/String;

    return-object v0
.end method

.method public setAuthToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/vectorwatch/android/models/SignUpReturnModel;->authToken:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setErrorCode(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "errorCode"    # Ljava/lang/Integer;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vectorwatch/android/models/SignUpReturnModel;->errorCode:Ljava/lang/Integer;

    .line 33
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/models/SignUpReturnModel;->message:Ljava/lang/String;

    .line 17
    return-void
.end method
