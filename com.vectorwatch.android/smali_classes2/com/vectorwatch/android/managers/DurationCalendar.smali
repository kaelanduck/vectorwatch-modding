.class public Lcom/vectorwatch/android/managers/DurationCalendar;
.super Ljava/lang/Object;
.source "DurationCalendar.java"


# instance fields
.field public days:I

.field public hours:I

.field public minutes:I

.field public seconds:I

.field public sign:I

.field public weeks:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x1

    iput v0, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->sign:I

    .line 26
    return-void
.end method


# virtual methods
.method public getMillis()J
    .locals 5

    .prologue
    .line 112
    iget v2, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->sign:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v0, v2

    .line 113
    .local v0, "factor":J
    const v2, 0x93a80

    iget v3, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->weeks:I

    mul-int/2addr v2, v3

    const v3, 0x15180

    iget v4, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->days:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    iget v3, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->hours:I

    mul-int/lit16 v3, v3, 0xe10

    add-int/2addr v2, v3

    iget v3, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->minutes:I

    mul-int/lit8 v3, v3, 0x3c

    add-int/2addr v2, v3

    iget v3, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->seconds:I

    add-int/2addr v2, v3

    int-to-long v2, v2

    mul-long/2addr v2, v0

    return-wide v2
.end method

.method public parse(Ljava/lang/String;)V
    .locals 7
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    iput v5, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->sign:I

    .line 35
    iput v4, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->weeks:I

    .line 36
    iput v4, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->days:I

    .line 37
    iput v4, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->hours:I

    .line 38
    iput v4, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->minutes:I

    .line 39
    iput v4, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->seconds:I

    .line 41
    if-nez p1, :cond_1

    .line 109
    :cond_0
    return-void

    .line 45
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 46
    .local v2, "len":I
    const/4 v1, 0x0

    .line 49
    .local v1, "index":I
    if-lt v2, v5, :cond_0

    .line 53
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 54
    .local v0, "c":C
    const/16 v4, 0x2d

    if-ne v0, v4, :cond_3

    .line 55
    const/4 v4, -0x1

    iput v4, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->sign:I

    .line 56
    add-int/lit8 v1, v1, 0x1

    .line 62
    :cond_2
    :goto_0
    if-lt v2, v1, :cond_0

    .line 66
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 67
    const/16 v4, 0x50

    if-eq v0, v4, :cond_4

    .line 68
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Duration.parse(str=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\') expected \'P\' at index="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 58
    :cond_3
    const/16 v4, 0x2b

    if-ne v0, v4, :cond_2

    .line 59
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 72
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 74
    const/4 v3, 0x0

    .line 75
    .local v3, "n":I
    :goto_1
    if-ge v1, v2, :cond_0

    .line 76
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 77
    const/16 v4, 0x30

    if-lt v0, v4, :cond_6

    const/16 v4, 0x39

    if-gt v0, v4, :cond_6

    .line 78
    mul-int/lit8 v3, v3, 0xa

    .line 79
    add-int/lit8 v4, v0, -0x30

    add-int/2addr v3, v4

    .line 75
    :cond_5
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 81
    :cond_6
    const/16 v4, 0x57

    if-ne v0, v4, :cond_7

    .line 82
    iput v3, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->weeks:I

    .line 83
    const/4 v3, 0x0

    goto :goto_2

    .line 85
    :cond_7
    const/16 v4, 0x48

    if-ne v0, v4, :cond_8

    .line 86
    iput v3, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->hours:I

    .line 87
    const/4 v3, 0x0

    goto :goto_2

    .line 89
    :cond_8
    const/16 v4, 0x4d

    if-ne v0, v4, :cond_9

    .line 90
    iput v3, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->minutes:I

    .line 91
    const/4 v3, 0x0

    goto :goto_2

    .line 93
    :cond_9
    const/16 v4, 0x53

    if-ne v0, v4, :cond_a

    .line 94
    iput v3, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->seconds:I

    .line 95
    const/4 v3, 0x0

    goto :goto_2

    .line 97
    :cond_a
    const/16 v4, 0x44

    if-ne v0, v4, :cond_b

    .line 98
    iput v3, p0, Lcom/vectorwatch/android/managers/DurationCalendar;->days:I

    .line 99
    const/4 v3, 0x0

    goto :goto_2

    .line 101
    :cond_b
    const/16 v4, 0x54

    if-eq v0, v4, :cond_5

    .line 104
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Duration.parse(str=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\') unexpected char \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' at index="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
