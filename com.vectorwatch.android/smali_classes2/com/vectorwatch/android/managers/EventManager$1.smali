.class final Lcom/vectorwatch/android/managers/EventManager$1;
.super Ljava/lang/Object;
.source "EventManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/managers/EventManager;->getNext12HoursEvents(Landroid/content/Context;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/vectorwatch/android/models/CalendarEventModel;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/vectorwatch/android/models/CalendarEventModel;Lcom/vectorwatch/android/models/CalendarEventModel;)I
    .locals 4
    .param p1, "lhs"    # Lcom/vectorwatch/android/models/CalendarEventModel;
    .param p2, "rhs"    # Lcom/vectorwatch/android/models/CalendarEventModel;

    .prologue
    .line 120
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getStartTimestamp()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/vectorwatch/android/models/CalendarEventModel;->getStartTimestamp()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 121
    const/4 v0, -0x1

    .line 125
    :goto_0
    return v0

    .line 122
    :cond_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CalendarEventModel;->getStartTimestamp()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/vectorwatch/android/models/CalendarEventModel;->getStartTimestamp()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 123
    const/4 v0, 0x1

    goto :goto_0

    .line 125
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 117
    check-cast p1, Lcom/vectorwatch/android/models/CalendarEventModel;

    check-cast p2, Lcom/vectorwatch/android/models/CalendarEventModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/managers/EventManager$1;->compare(Lcom/vectorwatch/android/models/CalendarEventModel;Lcom/vectorwatch/android/models/CalendarEventModel;)I

    move-result v0

    return v0
.end method
