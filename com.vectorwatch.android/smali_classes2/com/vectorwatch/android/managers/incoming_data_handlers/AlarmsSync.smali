.class public Lcom/vectorwatch/android/managers/incoming_data_handlers/AlarmsSync;
.super Ljava/lang/Object;
.source "AlarmsSync.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AlarmsSync;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AlarmsSync;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static syncAlarms(Lcom/vectorwatch/android/service/ble/messages/DataMessage;Landroid/content/Context;)V
    .locals 23
    .param p0, "dataMessage"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v11

    .line 34
    .local v11, "baseMessage":Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 35
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_ALARM:Landroid/net/Uri;

    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "alarm_name"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "alarm_hour"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "alarm_minute"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "alarm_time_of_day"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "alarm_angle"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "alarm_status"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 40
    .local v13, "cursor":Landroid/database/Cursor;
    invoke-virtual {v11}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 89
    sget-object v3, Lcom/vectorwatch/android/managers/incoming_data_handlers/AlarmsSync;->log:Lorg/slf4j/Logger;

    const-string v4, "Should not get here."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 91
    :goto_0
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 92
    return-void

    .line 42
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v21

    .line 49
    .local v21, "payload":[B
    const/4 v12, 0x0

    .line 51
    .local v12, "crt":I
    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v14

    .line 52
    .local v14, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->get()B

    move-result v10

    .line 54
    .local v10, "alarmsCount":B
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 57
    .local v22, "receivedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    const/16 v16, 0x0

    .local v16, "i":B
    :goto_1
    move/from16 v0, v16

    if-ge v0, v10, :cond_0

    .line 58
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->get()B

    move-result v15

    .line 59
    .local v15, "hour":B
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->get()B

    move-result v20

    .line 60
    .local v20, "min":B
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->get()B

    move-result v17

    .line 63
    .local v17, "isEnable":B
    new-instance v8, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-direct {v8}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;-><init>()V

    .line 64
    .local v8, "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    invoke-virtual {v8, v15}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setNumberOfHours(I)V

    .line 65
    move/from16 v0, v20

    invoke-virtual {v8, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setNumberOfMinutes(I)V

    .line 66
    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setIsEnabled(B)V

    .line 68
    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    add-int/lit8 v3, v16, 0x1

    int-to-byte v0, v3

    move/from16 v16, v0

    goto :goto_1

    .line 72
    .end local v8    # "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .end local v15    # "hour":B
    .end local v17    # "isEnable":B
    .end local v20    # "min":B
    :cond_0
    const/16 v16, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v0, v10, :cond_2

    .line 73
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->get()B

    move-result v19

    .line 75
    .local v19, "labelSize":B
    if-nez v19, :cond_1

    .line 76
    move-object/from16 v0, v22

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090059

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setName(Ljava/lang/String;)V

    .line 72
    :goto_3
    add-int/lit8 v3, v16, 0x1

    int-to-byte v0, v3

    move/from16 v16, v0

    goto :goto_2

    .line 78
    :cond_1
    move/from16 v0, v19

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 79
    .local v18, "labelBytes":[B
    const/4 v3, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v14, v0, v3, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    .line 80
    new-instance v9, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/lang/String;-><init>([B)V

    .line 82
    .local v9, "alarmLabel":Ljava/lang/String;
    move-object/from16 v0, v22

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {v3, v9}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setName(Ljava/lang/String;)V

    goto :goto_3

    .line 86
    .end local v9    # "alarmLabel":Ljava/lang/String;
    .end local v18    # "labelBytes":[B
    .end local v19    # "labelSize":B
    :cond_2
    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AlarmsSync;->updateAlarmsTable(Ljava/util/List;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static updateAlarmsTable(Ljava/util/List;Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "receivedAlarms":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v1

    .line 103
    .local v1, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/database/DatabaseManager;->deleteAllAlarms(Lio/realm/Realm;)V

    .line 106
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .line 107
    .local v6, "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v0

    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfHours()I

    move-result v3

    .line 108
    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfMinutes()I

    move-result v4

    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getIsEnabled()B

    move-result v5

    .line 107
    invoke-virtual/range {v0 .. v5}, Lcom/vectorwatch/android/database/DatabaseManager;->createAlarm(Lio/realm/Realm;Ljava/lang/String;IIB)V

    goto :goto_0

    .line 111
    .end local v6    # "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    :cond_0
    invoke-virtual {v1}, Lio/realm/Realm;->close()V

    .line 114
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v2, Lcom/vectorwatch/android/events/AlarmsRefreshedByWatchEvent;

    invoke-direct {v2}, Lcom/vectorwatch/android/events/AlarmsRefreshedByWatchEvent;-><init>()V

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 115
    return-void
.end method
