.class public Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;
.super Ljava/lang/Object;
.source "SyncWatchLogs.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static receivedCrashAnnouncement(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    sget-object v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;->log:Lorg/slf4j/Logger;

    const-string v1, "Received crash report command."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 32
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendWatchLogsRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 33
    return-void
.end method

.method public static receivedCrashLogs(Landroid/content/Context;Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dataMessage"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    .line 42
    invoke-virtual {p1}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v7

    .line 43
    .local v7, "payload":[B
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    sget-object v10, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 44
    .local v1, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v8

    .line 45
    .local v8, "total":B
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 48
    .local v3, "index":B
    const/4 v9, 0x2

    const/16 v10, 0x82

    invoke-static {v7, v9, v10}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    .line 50
    .local v5, "logs":[B
    invoke-static {}, Lcom/vectorwatch/android/utils/FileUtils;->getCurrentWatchLogFile()Ljava/io/File;

    move-result-object v2

    .line 52
    .local v2, "file":Ljava/io/File;
    invoke-static {v2, v5}, Lcom/vectorwatch/android/cloud_communication/FileManager;->performWriteToFile(Ljava/io/File;[B)Z

    .line 54
    add-int/lit8 v9, v3, 0x1

    if-ne v8, v9, :cond_1

    .line 55
    sget-object v9, Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;->log:Lorg/slf4j/Logger;

    const-string v10, "WATCH CRASH LOGS: end file."

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 56
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getCounterWatchCrashLogs(Landroid/content/Context;)I

    move-result v9

    add-int/lit8 v0, v9, 0x1

    .line 57
    .local v0, "currentCountCrashLogs":I
    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setCounterWatchCrashLogs(ILandroid/content/Context;)V

    .line 60
    invoke-static {v2}, Lcom/vectorwatch/android/cloud_communication/FileManager;->closeFile(Ljava/io/File;)Z

    move-result v4

    .line 61
    .local v4, "isDone":Z
    sget-object v9, Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "WATCH CRASH LOGS: closed file? "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 63
    if-eqz v4, :cond_0

    .line 64
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/FileManager;->generateUploadFileName()Ljava/lang/String;

    move-result-object v6

    .line 66
    .local v6, "newName":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/utils/FileUtils;->getUploadDir()Ljava/io/File;

    move-result-object v9

    .line 67
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    .line 65
    invoke-static {v9, v10, v6}, Lcom/vectorwatch/android/cloud_communication/FileManager;->renameFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 69
    sget-object v9, Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "WATCH CRASH LOGS: renamed file to: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " worked? "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 70
    if-nez v4, :cond_0

    .line 71
    sget-object v9, Lcom/vectorwatch/android/managers/incoming_data_handlers/SyncWatchLogs;->log:Lorg/slf4j/Logger;

    const-string v10, "WATCH CRASH LOGS: Couldn\'t rename file."

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 76
    .end local v6    # "newName":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/vectorwatch/android/cloud_communication/FileManager;->uploadAllFilesToCloud(Landroid/content/Context;)V

    .line 79
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendEraseWatchCrashLogs(Landroid/content/Context;)Ljava/util/UUID;

    .line 81
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v9

    new-instance v10, Lcom/vectorwatch/android/events/CrashLogsReceivedEvent;

    invoke-direct {v10}, Lcom/vectorwatch/android/events/CrashLogsReceivedEvent;-><init>()V

    invoke-virtual {v9, v10}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 83
    .end local v0    # "currentCountCrashLogs":I
    .end local v4    # "isDone":Z
    :cond_1
    return-void
.end method
