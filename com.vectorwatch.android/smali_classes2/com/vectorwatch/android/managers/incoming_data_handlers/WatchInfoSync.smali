.class public Lcom/vectorwatch/android/managers/incoming_data_handlers/WatchInfoSync;
.super Ljava/lang/Object;
.source "WatchInfoSync.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/WatchInfoSync;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/WatchInfoSync;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static syncSerialNumber(Lcom/vectorwatch/android/service/ble/messages/DataMessage;Landroid/content/Context;)V
    .locals 11
    .param p0, "dataMessage"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v10, 0x24

    .line 30
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v1

    .line 32
    .local v1, "payload":[B
    sget-object v5, Lcom/vectorwatch/android/managers/incoming_data_handlers/WatchInfoSync;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SERIAL_NO: Payload = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 33
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 34
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 35
    .local v4, "unsignedSerialNumber":I
    int-to-long v6, v4

    const-wide v8, 0xffffffffL

    and-long v2, v6, v8

    .line 37
    .local v2, "serialNumber":J
    sget-object v5, Lcom/vectorwatch/android/managers/incoming_data_handlers/WatchInfoSync;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SERIAL_NO: Serial number base36 = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2, v3, v10}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 38
    const-string v5, "pref_serial_number"

    .line 39
    invoke-static {v2, v3, v10}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v6

    .line 38
    invoke-static {v5, v6, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method public static syncWatchSystemInfo(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Landroid/content/Context;)V
    .locals 5
    .param p0, "info"    # Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 43
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v1

    const-string v2, "last_known_system_info"

    invoke-virtual {v1, v2, p0}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    if-eqz p0, :cond_1

    .line 47
    sget-object v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/WatchInfoSync;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SYS_INFO: Version K "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "kernel"

    invoke-static {p0, v3}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Version B: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "bootloader"

    .line 48
    invoke-static {p0, v3}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 47
    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v1

    if-lt v1, v4, :cond_2

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v1

    if-lt v1, v4, :cond_2

    .line 50
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 51
    sget-object v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/WatchInfoSync;->log:Lorg/slf4j/Logger;

    const-string v2, "SYS_INFO: Sys info v2 Version with 4. Request with v2. Get system info again because version > = 1"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 55
    invoke-static {p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSystemInfoRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-static {p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSystemInfoRequest(Landroid/content/Context;)Ljava/util/UUID;

    goto :goto_0

    .line 64
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/WatchSystemInfoReceivedEvent;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/events/WatchSystemInfoReceivedEvent;-><init>(Lcom/vectorwatch/com/android/vos/update/SystemInfo;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 67
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "K:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "B:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    .line 68
    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "version":Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchOSVersion(Ljava/lang/String;Landroid/content/Context;)V

    .line 70
    const-string v1, "check_compatibility"

    const/4 v2, 0x0

    invoke-static {v1, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_3

    .line 73
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    .line 74
    :cond_3
    invoke-static {p1}, Lcom/vectorwatch/android/service/VersionMonitor;->startCompatibilityChecks(Landroid/content/Context;)V

    goto :goto_0
.end method
