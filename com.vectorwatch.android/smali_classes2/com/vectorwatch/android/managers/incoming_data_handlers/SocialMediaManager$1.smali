.class Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;
.super Ljava/lang/Object;
.source "SocialMediaManager.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->shareActivityToFacebook(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/ShareServerResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 2
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 132
    invoke-static {}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "SOCIAL MEDIA: Can\'t get share link."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/ShareServerResponse;Lretrofit/client/Response;)V
    .locals 4
    .param p1, "shareServerResponse"    # Lcom/vectorwatch/android/models/ShareServerResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 79
    invoke-static {}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "SOCIAL MEDIA: Got share link."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/ShareServerResponse;->getData()Lcom/vectorwatch/android/models/ShareServerData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/ShareServerResponse;->getData()Lcom/vectorwatch/android/models/ShareServerData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/ShareServerData;->getUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 83
    invoke-static {}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SOCIAL MEDIA: Share link = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/ShareServerResponse;->getData()Lcom/vectorwatch/android/models/ShareServerData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ShareServerData;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 84
    new-instance v1, Lcom/facebook/share/model/ShareLinkContent$Builder;

    invoke-direct {v1}, Lcom/facebook/share/model/ShareLinkContent$Builder;-><init>()V

    .line 85
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/ShareServerResponse;->getData()Lcom/vectorwatch/android/models/ShareServerData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/ShareServerData;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/share/model/ShareLinkContent$Builder;->setContentUrl(Landroid/net/Uri;)Lcom/facebook/share/model/ShareContent$Builder;

    move-result-object v1

    check-cast v1, Lcom/facebook/share/model/ShareLinkContent$Builder;

    .line 86
    invoke-virtual {v1}, Lcom/facebook/share/model/ShareLinkContent$Builder;->build()Lcom/facebook/share/model/ShareLinkContent;

    move-result-object v0

    .line 88
    .local v0, "content":Lcom/facebook/share/model/ShareLinkContent;
    new-instance v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;-><init>(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;)V

    invoke-static {v0, v1}, Lcom/facebook/share/ShareApi;->share(Lcom/facebook/share/model/ShareContent;Lcom/facebook/FacebookCallback;)V

    .line 127
    .end local v0    # "content":Lcom/facebook/share/model/ShareLinkContent;
    :goto_0
    return-void

    .line 124
    :cond_0
    const-string v1, "Unexpected"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->failure(Lretrofit/RetrofitError;)V

    .line 125
    invoke-static {}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "SOCIAL MEDIA: Null server data or link.."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 76
    check-cast p1, Lcom/vectorwatch/android/models/ShareServerResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->success(Lcom/vectorwatch/android/models/ShareServerResponse;Lretrofit/client/Response;)V

    return-void
.end method
