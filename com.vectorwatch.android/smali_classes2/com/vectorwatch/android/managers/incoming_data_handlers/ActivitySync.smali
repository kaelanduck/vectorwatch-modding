.class public Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;
.super Ljava/lang/Object;
.source "ActivitySync.java"


# static fields
.field private static final SECONDS_IN_MINUTE:I = 0x3c

.field private static final SIZE_ACTIVITY_BASE_LOG:I = 0x5

.field private static final SIZE_ACTIVITY_STRUCT:I = 0x10

.field private static final SIZE_OTHER_LOGS:I = 0x14

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getActivityBucketTimestamp(ILandroid/content/Context;)I
    .locals 6
    .param p0, "realTimestamp"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 300
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 304
    .local v0, "resolver":Landroid/content/ContentResolver;
    int-to-long v2, p0

    const-wide/16 v4, 0xf

    invoke-static {v2, v3, v4, v5}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->getStartInterval(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 305
    .local v1, "timestamp":I
    return v1
.end method

.method private static getActivityType(IIIIII)Ljava/lang/String;
    .locals 1
    .param p0, "stepCount"    # I
    .param p1, "activeTime"    # I
    .param p2, "amplitude"    # I
    .param p3, "period"    # I
    .param p4, "calories"    # I
    .param p5, "distance"    # I

    .prologue
    .line 325
    sget-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->ACTIVITY:Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;

    invoke-virtual {v0}, Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getStartInterval(JJ)J
    .locals 6
    .param p0, "timeInSec"    # J
    .param p2, "offset"    # J

    .prologue
    .line 316
    const-wide/16 v4, 0x3c

    mul-long v2, p2, v4

    .line 317
    .local v2, "offsetInSec":J
    div-long v0, p0, v2

    .line 318
    .local v0, "groupTime":J
    mul-long v4, v0, v2

    return-wide v4
.end method

.method private static handleActivityVersion0([BLandroid/content/Context;)V
    .locals 26
    .param p0, "payload"    # [B
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 387
    invoke-static/range {p0 .. p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v11, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v14

    .line 390
    .local v14, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getUnixTimeFromVectorTime(I)I

    move-result v2

    .line 391
    .local v2, "baseTimestamp":I
    sget-object v4, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Base timestamp: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v11}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 394
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->get()B

    move-result v18

    .line 396
    .local v18, "structCount":B
    move-object/from16 v0, p0

    array-length v4, v0

    mul-int/lit8 v11, v18, 0x10

    add-int/lit8 v11, v11, 0x5

    if-ge v4, v11, :cond_1

    .line 397
    sget-object v4, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    const-string v11, "Received activity seems to be incorrect. Payload length not as expected. Possible data loss."

    invoke-interface {v4, v11}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 419
    :cond_0
    return-void

    .line 401
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v13

    .line 402
    .local v13, "cal":Ljava/util/Calendar;
    new-instance v15, Landroid/text/format/Time;

    invoke-direct {v15}, Landroid/text/format/Time;-><init>()V

    .line 403
    .local v15, "now":Landroid/text/format/Time;
    invoke-virtual {v15}, Landroid/text/format/Time;->setToNow()V

    .line 404
    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v15, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 405
    iget-wide v0, v15, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v16, v0

    .line 406
    .local v16, "offsetTimezone":J
    const-wide/16 v22, 0x4

    mul-long v22, v22, v16

    const-wide/16 v24, 0xe10

    div-long v20, v22, v24

    .line 408
    .local v20, "timezone":J
    const/4 v3, 0x0

    .local v3, "i":B
    :goto_0
    move/from16 v0, v18

    if-ge v3, v0, :cond_0

    .line 409
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v5

    .line 410
    .local v5, "steps":S
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v6

    .line 411
    .local v6, "effectiveTime":S
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v7

    .line 412
    .local v7, "avgAmplitude":S
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v8

    .line 413
    .local v8, "avgPeriod":S
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v9

    .line 414
    .local v9, "calories":I
    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v10

    .line 416
    .local v10, "distance":I
    move-wide/from16 v0, v20

    long-to-int v4, v0

    int-to-byte v4, v4

    const/16 v11, 0xf

    move-object/from16 v12, p1

    invoke-static/range {v2 .. v12}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->saveActivityToDatabase(IBBSSSSIIILandroid/content/Context;)V

    .line 408
    add-int/lit8 v4, v3, 0x1

    int-to-byte v3, v4

    goto :goto_0
.end method

.method private static handleActivityVersion1([BLandroid/content/Context;)V
    .locals 23
    .param p0, "payload"    # [B
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 423
    invoke-static/range {p0 .. p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->handleActivityVersion0([BLandroid/content/Context;)V

    .line 425
    invoke-static/range {p0 .. p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v14

    sget-object v15, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v14, v15}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v18

    .line 431
    .local v18, "data":Ljava/nio/ByteBuffer;
    invoke-virtual/range {v18 .. v18}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v14

    invoke-static {v14}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getUnixTimeFromVectorTime(I)I

    move-result v2

    .line 434
    .local v2, "baseTimestamp":I
    invoke-virtual/range {v18 .. v18}, Ljava/nio/ByteBuffer;->get()B

    move-result v22

    .line 437
    .local v22, "structCount":B
    mul-int/lit8 v14, v22, 0x10

    add-int/lit8 v21, v14, 0x5

    .line 439
    .local v21, "startLogs":I
    move-object/from16 v0, p0

    array-length v14, v0

    mul-int/lit8 v15, v22, 0x10

    add-int/lit8 v15, v15, 0x5

    add-int/lit8 v15, v15, 0x14

    if-ge v14, v15, :cond_0

    .line 440
    sget-object v14, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    const-string v15, "Received activity seems to be incorrect. Payload length not as expected. Possible data loss."

    invoke-interface {v14, v15}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 485
    :goto_0
    return-void

    .line 456
    :cond_0
    const/16 v19, -0x1

    .line 457
    .local v19, "sleepStartTime":I
    const/16 v20, -0x1

    .line 460
    .local v20, "sleepStopTime":I
    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v3

    .line 461
    .local v3, "notificationsCount":S
    add-int/lit8 v14, v21, 0x2

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v4

    .line 462
    .local v4, "glanceCount":S
    add-int/lit8 v14, v21, 0x4

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v5

    .line 463
    .local v5, "packetsReceivedCount":S
    add-int/lit8 v14, v21, 0x6

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v6

    .line 464
    .local v6, "packetsTransmittedCount":S
    add-int/lit8 v14, v21, 0x8

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    .line 465
    .local v7, "bleConnectsCount":B
    add-int/lit8 v14, v21, 0x9

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    .line 466
    .local v8, "bleDisconnectsCount":B
    add-int/lit8 v14, v21, 0xa

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v9

    .line 467
    .local v9, "secsDisconnectedCount":S
    add-int/lit8 v14, v21, 0xc

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v10

    .line 468
    .local v10, "buttonPressesCount":S
    add-int/lit8 v14, v21, 0xe

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v11

    .line 469
    .local v11, "backlightActivationCount":S
    add-int/lit8 v14, v21, 0x10

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v12

    .line 470
    .local v12, "shakerActivationCount":S
    add-int/lit8 v14, v21, 0x12

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v13

    .line 472
    .local v13, "batteryVoltage":S
    invoke-virtual/range {v18 .. v18}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v14

    array-length v14, v14

    sub-int v14, v14, v21

    const/16 v15, 0x1c

    if-lt v14, v15, :cond_1

    .line 473
    add-int/lit8 v14, v21, 0x14

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v19

    .line 474
    add-int/lit8 v14, v21, 0x18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v20

    .line 480
    :cond_1
    invoke-static/range {v19 .. v19}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getUnixTimeFromVectorTime(I)I

    move-result v14

    .line 481
    invoke-static/range {v20 .. v20}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getUnixTimeFromVectorTime(I)I

    move-result v15

    const/16 v16, 0x3c

    move-object/from16 v17, p1

    .line 477
    invoke-static/range {v2 .. v17}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->saveLogsToDatabase(ISSSSBBSSSSSIIILandroid/content/Context;)V

    .line 484
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v14

    new-instance v15, Lcom/vectorwatch/android/events/ActivityUpdateEvent;

    invoke-direct {v15}, Lcom/vectorwatch/android/events/ActivityUpdateEvent;-><init>()V

    invoke-virtual {v14, v15}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private static previousEntryInDatabase(ILjava/lang/String;Landroid/net/Uri;Landroid/content/Context;)Z
    .locals 9
    .param p0, "columnValue"    # I
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "table"    # Landroid/net/Uri;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 337
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 338
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-array v2, v7, [Ljava/lang/String;

    aput-object p1, v2, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v1, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 340
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v7

    .line 346
    :goto_0
    return v1

    .line 345
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v8

    .line 346
    goto :goto_0
.end method

.method private static previousEntryInDatabaseSleep(IILjava/lang/String;Landroid/net/Uri;Landroid/content/Context;)Z
    .locals 10
    .param p0, "columnValue"    # I
    .param p1, "start"    # I
    .param p2, "column"    # Ljava/lang/String;
    .param p3, "table"    # Landroid/net/Uri;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 360
    invoke-virtual {p4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 361
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    aput-object p2, v2, v9

    const-string v1, "start_time"

    aput-object v1, v2, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v1, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 363
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 364
    const-string v1, "start_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 365
    .local v7, "dbStart":I
    if-ge v7, p1, :cond_0

    .line 366
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v8

    .line 372
    .end local v7    # "dbStart":I
    :goto_0
    return v1

    .line 371
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v9

    .line 372
    goto :goto_0
.end method

.method public static requestActivityCausedByWatchAnnouncement(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    if-nez p0, :cond_0

    .line 61
    sget-object v2, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    const-string v3, "Activity announcement cannot trigger sync due to null context."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getActivitySyncLastTimestamp(Landroid/content/Context;)I

    move-result v1

    .line 66
    .local v1, "startTime":I
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentTime()I

    move-result v0

    .line 67
    .local v0, "endTime":I
    invoke-static {p0, v1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendActivityRequest(Landroid/content/Context;II)Ljava/util/UUID;

    goto :goto_0
.end method

.method private static saveActivityToDatabase(IBBSSSSIIILandroid/content/Context;)V
    .locals 8
    .param p0, "baseTimestamp"    # I
    .param p1, "activityIndex"    # B
    .param p2, "timezone"    # B
    .param p3, "stepCount"    # S
    .param p4, "effectiveTime"    # S
    .param p5, "amplitude"    # S
    .param p6, "period"    # S
    .param p7, "calories"    # I
    .param p8, "distance"    # I
    .param p9, "offset"    # I
    .param p10, "context"    # Landroid/content/Context;

    .prologue
    .line 258
    new-instance v2, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-direct {v2}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;-><init>()V

    .line 261
    .local v2, "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyDistance(Z)V

    .line 262
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyCalories(Z)V

    .line 263
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtySteps(Z)V

    .line 264
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyCloud(Z)V

    .line 267
    move/from16 v0, p9

    invoke-virtual {v2, v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setOffset(I)V

    .line 270
    mul-int/lit8 v5, p9, 0x3c

    mul-int/2addr v5, p1

    add-int/2addr v5, p0

    move-object/from16 v0, p10

    invoke-static {v5, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->getActivityBucketTimestamp(ILandroid/content/Context;)I

    move-result v3

    .line 271
    .local v3, "timestamp":I
    int-to-long v6, v3

    invoke-virtual {v2, v6, v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setTimestamp(J)V

    .line 274
    invoke-static/range {p3 .. p8}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->getActivityType(IIIIII)Ljava/lang/String;

    move-result-object v4

    .line 275
    .local v4, "type":Ljava/lang/String;
    invoke-virtual {v2, v4}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setActivityType(Ljava/lang/String;)V

    .line 278
    invoke-virtual {v2, p3}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setVal(I)V

    .line 279
    invoke-virtual {v2, p7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setCal(I)V

    .line 280
    move/from16 v0, p8

    invoke-virtual {v2, v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDist(I)V

    .line 281
    invoke-virtual {v2, p5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setAvgAmpl(I)V

    .line 282
    invoke-virtual {v2, p6}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setAvgPeriod(I)V

    .line 283
    invoke-virtual {v2, p4}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setEffTime(I)V

    .line 286
    invoke-virtual {v2, p2}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setTimezone(I)V

    .line 289
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/vectorwatch/android/database/DatabaseManager;->insertActivityData(Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;)V

    .line 290
    return-void
.end method

.method private static saveLogsToDatabase(ISSSSBBSSSSSIIILandroid/content/Context;)V
    .locals 10
    .param p0, "baseTimestamp"    # I
    .param p1, "notificationsCount"    # S
    .param p2, "glanceCount"    # S
    .param p3, "packetsReceivedCount"    # S
    .param p4, "packetsTransmittedCount"    # S
    .param p5, "bleConnectsCount"    # B
    .param p6, "bleDisconnectsCount"    # B
    .param p7, "secsDisconnectedCount"    # S
    .param p8, "buttonPressesCount"    # S
    .param p9, "backlightActivationCount"    # S
    .param p10, "shakerActivationCount"    # S
    .param p11, "batteryVoltage"    # S
    .param p12, "startSleep"    # I
    .param p13, "stopSleep"    # I
    .param p14, "offset"    # I
    .param p15, "context"    # Landroid/content/Context;

    .prologue
    .line 174
    invoke-virtual/range {p15 .. p15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 177
    .local v3, "resolver":Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 179
    .local v4, "valuesGenericLogs":Landroid/content/ContentValues;
    const-string v6, "log_offset"

    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 180
    const-string v6, "dirty_field"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 181
    const-string v6, "timestamp"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 183
    const-string v6, "log_notification"

    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 184
    const-string v6, "log_glance_count"

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 185
    const-string v6, "log_received_packets"

    invoke-static {p3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 186
    const-string v6, "log_transmitted_packets"

    invoke-static {p4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 187
    const-string v6, "log_connects"

    invoke-static {p5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    .line 188
    const-string v6, "log_disconnects"

    invoke-static/range {p6 .. p6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    .line 189
    const-string v6, "log_secs_disconnected"

    invoke-static/range {p7 .. p7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 190
    const-string v6, "log_button_presses"

    invoke-static/range {p8 .. p8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 191
    const-string v6, "log_backlight_count"

    invoke-static/range {p9 .. p9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 192
    const-string v6, "log_shaker_count"

    invoke-static/range {p10 .. p10}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 193
    const-string v6, "log_battery_voltage"

    invoke-static/range {p11 .. p11}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 203
    const-string v6, "timestamp"

    sget-object v7, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_LOGS:Landroid/net/Uri;

    move-object/from16 v0, p15

    invoke-static {p0, v6, v7, v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->previousEntryInDatabase(ILjava/lang/String;Landroid/net/Uri;Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 205
    sget-object v6, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_LOGS:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "timestamp="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v3, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 212
    :goto_0
    const/4 v6, -0x1

    move/from16 v0, p12

    if-eq v0, v6, :cond_1

    const/4 v6, -0x1

    move/from16 v0, p13

    if-eq v0, v6, :cond_1

    .line 213
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 215
    .local v5, "valuesSleepData":Landroid/content/ContentValues;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 216
    .local v2, "calendar":Ljava/util/Calendar;
    new-instance v6, Ljava/util/Date;

    move/from16 v0, p12

    int-to-long v8, v0

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 217
    const/16 v6, 0xb

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    const/16 v7, 0x14

    if-ge v6, v7, :cond_0

    .line 218
    const/4 v6, 0x6

    const/4 v7, -0x1

    invoke-virtual {v2, v6, v7}, Ljava/util/Calendar;->add(II)V

    .line 220
    :cond_0
    const/16 v6, 0xb

    const/16 v7, 0x14

    invoke-virtual {v2, v6, v7}, Ljava/util/Calendar;->set(II)V

    .line 221
    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Ljava/util/Calendar;->set(II)V

    .line 222
    sub-int v6, p13, p12

    if-lez v6, :cond_1

    .line 223
    sget-object v6, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SyncActivity: Sleep: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p12

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p13

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 225
    const-string v6, "start_time"

    invoke-static/range {p12 .. p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 226
    const-string v6, "stop_time"

    invoke-static/range {p13 .. p13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 227
    const-string v6, "timestamp"

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    long-to-int v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 228
    const-string v6, "dirty_field"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 230
    const-string v6, "start_time"

    sget-object v7, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    move/from16 v0, p12

    move-object/from16 v1, p15

    invoke-static {v0, v6, v7, v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->previousEntryInDatabase(ILjava/lang/String;Landroid/net/Uri;Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 232
    sget-object v6, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "start_time="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p12

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v3, v6, v5, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 239
    .end local v2    # "calendar":Ljava/util/Calendar;
    .end local v5    # "valuesSleepData":Landroid/content/ContentValues;
    :cond_1
    :goto_1
    return-void

    .line 208
    :cond_2
    sget-object v6, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_LOGS:Landroid/net/Uri;

    invoke-virtual {v3, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0

    .line 235
    .restart local v2    # "calendar":Ljava/util/Calendar;
    .restart local v5    # "valuesSleepData":Landroid/content/ContentValues;
    :cond_3
    sget-object v6, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    invoke-virtual {v3, v6, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1
.end method

.method public static syncActivityFromWatch(Lcom/vectorwatch/android/service/ble/messages/DataMessage;Landroid/content/Context;)V
    .locals 5
    .param p0, "dataMessage"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v0

    .line 130
    .local v0, "baseMessage":Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v1

    .line 131
    .local v1, "payload":[B
    sget-object v2, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Activity Payload: size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 132
    invoke-static {v1}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 131
    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 142
    sget-object v2, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    const-string v3, "Unknown activity sync version."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 144
    :goto_0
    return-void

    .line 136
    :pswitch_0
    invoke-static {v1, p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->handleActivityVersion0([BLandroid/content/Context;)V

    goto :goto_0

    .line 139
    :pswitch_1
    invoke-static {v1, p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->handleActivityVersion1([BLandroid/content/Context;)V

    goto :goto_0

    .line 134
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static syncActivityTotals(Lcom/vectorwatch/android/service/ble/messages/DataMessage;Landroid/content/Context;)V
    .locals 13
    .param p0, "dataMessage"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x0

    .line 78
    const-string v8, "pref_last_triggered_alarm_sync"

    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 78
    invoke-static {v8, v10, v11, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 81
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getBase()Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    move-result-object v0

    .line 82
    .local v0, "baseMessage":Lcom/vectorwatch/android/service/ble/messages/BaseMessage;
    invoke-virtual {v0}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 119
    sget-object v8, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    const-string v9, "Unknown totals version."

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 121
    :goto_0
    return-void

    .line 84
    :pswitch_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v5

    .line 92
    .local v5, "payload":[B
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 93
    .local v3, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v7

    .line 94
    .local v7, "steps":S
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    .line 100
    .local v1, "calories":S
    const v8, 0xffff

    and-int v2, v1, v8

    .line 101
    .local v2, "caloriesInt":I
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v4

    .line 102
    .local v4, "distance":S
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v6

    .line 104
    .local v6, "sleep":S
    sget-object v8, Lcom/vectorwatch/android/managers/incoming_data_handlers/ActivitySync;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Activity sync totals: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 106
    invoke-static {v12, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTotalsCalories(ILandroid/content/Context;)V

    .line 107
    invoke-static {v12, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTotalsDistance(ILandroid/content/Context;)V

    .line 108
    invoke-static {v12, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTotalsSleep(ILandroid/content/Context;)V

    .line 109
    invoke-static {v12, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTotalsSteps(ILandroid/content/Context;)V

    .line 111
    invoke-static {v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTotalsCalories(ILandroid/content/Context;)V

    .line 112
    invoke-static {v4, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTotalsDistance(ILandroid/content/Context;)V

    .line 113
    invoke-static {v6, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTotalsSleep(ILandroid/content/Context;)V

    .line 114
    invoke-static {v7, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTotalsSteps(ILandroid/content/Context;)V

    .line 116
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v8

    new-instance v9, Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;

    invoke-direct {v9}, Lcom/vectorwatch/android/events/ActivityTotalsUpdateEvent;-><init>()V

    invoke-virtual {v8, v9}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
