.class final Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;
.super Ljava/lang/Object;
.source "AppHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->takeActionForButtonPress(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$manager:Landroid/media/AudioManager;


# direct methods
.method constructor <init>(Landroid/media/AudioManager;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;->val$manager:Landroid/media/AudioManager;

    iput-object p2, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 306
    iget-object v3, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;->val$manager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 307
    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->sLastArtist:Ljava/lang/String;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->sLastArtist:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->sLastTrack:Ljava/lang/String;

    if-eqz v3, :cond_1

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->sLastTrack:Ljava/lang/String;

    const-string v4, ""

    .line 308
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 309
    :cond_1
    iget-object v3, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/receivers/scheduler/NoMusicTrigger;->setAlarm(Landroid/content/Context;)V

    .line 324
    :cond_2
    return-void

    .line 311
    :cond_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 312
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v3, v4

    add-int/lit8 v3, v3, 0x32

    .line 311
    invoke-static {v3}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v0

    .line 313
    .local v0, "duration":I
    const-string v3, "3906BFAD46D7E6E1BF757891F26AEFB1"

    sget-object v4, Lcom/vectorwatch/android/VectorApplication;->sLastArtist:Ljava/lang/String;

    iget-object v5, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;->val$context:Landroid/content/Context;

    invoke-static {v3, v4, v0, v5}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 315
    const-string v3, "6375B098967A1B6F1C37C891B02295F2"

    sget-object v4, Lcom/vectorwatch/android/VectorApplication;->sLastTrack:Ljava/lang/String;

    iget-object v5, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;->val$context:Landroid/content/Context;

    invoke-static {v3, v4, v0, v5}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 318
    iget-object v3, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/managers/StreamsManager;->getUpdates(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 319
    .local v2, "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 320
    .local v1, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    iget-object v4, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper$2;->val$context:Landroid/content/Context;

    invoke-static {v4, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;

    goto :goto_0
.end method
