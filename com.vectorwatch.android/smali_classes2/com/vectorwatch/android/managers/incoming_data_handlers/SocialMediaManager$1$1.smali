.class Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;
.super Ljava/lang/Object;
.source "SocialMediaManager.java"

# interfaces
.implements Lcom/facebook/FacebookCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->success(Lcom/vectorwatch/android/models/ShareServerResponse;Lretrofit/client/Response;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/FacebookCallback",
        "<",
        "Lcom/facebook/share/Sharer$Result;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 2

    .prologue
    .line 101
    invoke-static {}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "SOCIAL MEDIA: Share cancel."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public onError(Lcom/facebook/FacebookException;)V
    .locals 9
    .param p1, "error"    # Lcom/facebook/FacebookException;

    .prologue
    const v5, 0x7f090124

    const/16 v6, 0xbb8

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v4, -0x1

    .line 106
    invoke-static {}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SOCIAL MEDIA: Share error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/FacebookException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p1}, Lcom/facebook/FacebookException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "200"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    new-instance v0, Lcom/vectorwatch/android/models/AlertMessage;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v2, v2, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v3, v3, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    .line 109
    invoke-static {v3}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f09022c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v4, v4, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    .line 110
    invoke-static {v4}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0901d8

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 111
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/vectorwatch/android/models/AlertWatchFlags;

    invoke-direct {v6, v8, v7}, Lcom/vectorwatch/android/models/AlertWatchFlags;-><init>(ZZ)V

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/models/AlertMessage;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/AlertWatchFlags;Ljava/util/List;)V

    .line 113
    .local v0, "message":Lcom/vectorwatch/android/models/AlertMessage;
    iget-object v1, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v1, v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-static {v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packMessageForAlerts(Lcom/vectorwatch/android/models/AlertMessage;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    .line 121
    :goto_0
    return-void

    .line 115
    .end local v0    # "message":Lcom/vectorwatch/android/models/AlertMessage;
    :cond_0
    new-instance v0, Lcom/vectorwatch/android/models/AlertMessage;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v2, v2, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v3, v3, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    .line 116
    invoke-static {v3}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f09022b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v4, v4, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-static {v4}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0901d8

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 117
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/vectorwatch/android/models/AlertWatchFlags;

    invoke-direct {v6, v8, v7}, Lcom/vectorwatch/android/models/AlertWatchFlags;-><init>(ZZ)V

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/models/AlertMessage;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/AlertWatchFlags;Ljava/util/List;)V

    .line 119
    .restart local v0    # "message":Lcom/vectorwatch/android/models/AlertMessage;
    iget-object v1, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v1, v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-static {v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packMessageForAlerts(Lcom/vectorwatch/android/models/AlertMessage;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    goto :goto_0
.end method

.method public onSuccess(Lcom/facebook/share/Sharer$Result;)V
    .locals 9
    .param p1, "result"    # Lcom/facebook/share/Sharer$Result;

    .prologue
    .line 91
    invoke-static {}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "SOCIAL MEDIA: Share success."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 92
    new-instance v0, Lcom/vectorwatch/android/models/AlertMessage;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v2, v2, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f090124

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v3, v3, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    .line 93
    invoke-static {v3}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f09022d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v4, v4, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    .line 94
    invoke-static {v4}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0901d8

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xbb8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Lcom/vectorwatch/android/models/AlertWatchFlags;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Lcom/vectorwatch/android/models/AlertWatchFlags;-><init>(ZZ)V

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/models/AlertMessage;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/AlertWatchFlags;Ljava/util/List;)V

    .line 96
    .local v0, "message":Lcom/vectorwatch/android/models/AlertMessage;
    iget-object v1, p0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->this$1:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;

    iget-object v1, v1, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1;->this$0:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-static {v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;->access$100(Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packMessageForAlerts(Lcom/vectorwatch/android/models/AlertMessage;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    .line 97
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 88
    check-cast p1, Lcom/facebook/share/Sharer$Result;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager$1$1;->onSuccess(Lcom/facebook/share/Sharer$Result;)V

    return-void
.end method
