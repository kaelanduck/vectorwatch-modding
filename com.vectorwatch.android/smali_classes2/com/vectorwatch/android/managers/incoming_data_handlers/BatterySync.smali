.class public Lcom/vectorwatch/android/managers/incoming_data_handlers/BatterySync;
.super Ljava/lang/Object;
.source "BatterySync.java"


# static fields
.field private static final BASE_TIMEZONE:Ljava/lang/String; = "UTC"

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/BatterySync;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/BatterySync;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static syncBatteryFromWatch(Lcom/vectorwatch/android/service/ble/messages/DataMessage;Landroid/content/Context;)V
    .locals 12
    .param p0, "dataMessage"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v1

    .line 38
    .local v1, "payload":[B
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    sget-object v8, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 41
    .local v0, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v6

    .line 44
    .local v6, "voltage":S
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    .line 45
    .local v2, "percentage":B
    const/4 v3, 0x0

    .line 46
    .local v3, "status":B
    iget-object v7, p0, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->base:Lcom/vectorwatch/android/service/ble/messages/BaseMessage;

    invoke-virtual {v7}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage;->getVersion()S

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 47
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 50
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v4, v8, v10

    .line 52
    .local v4, "timestamp":J
    sget-object v7, Lcom/vectorwatch/android/managers/incoming_data_handlers/BatterySync;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Battery: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 54
    const-string v7, "last_synced_battery_level"

    invoke-static {v7, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 56
    const-string v7, "last_synced_battery_status"

    invoke-static {v7, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 58
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v7

    new-instance v8, Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;

    invoke-direct {v8, v2, v3}, Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;-><init>(BB)V

    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 59
    return-void
.end method
