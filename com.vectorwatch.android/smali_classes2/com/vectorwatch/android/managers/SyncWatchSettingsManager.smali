.class public Lcom/vectorwatch/android/managers/SyncWatchSettingsManager;
.super Ljava/lang/Object;
.source "SyncWatchSettingsManager.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/vectorwatch/android/managers/SyncWatchSettingsManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/SyncWatchSettingsManager;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static sendActivityOptionsToWatch(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 30
    const-string v3, "pref_goal_achievement"

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 32
    .local v1, "goalAchievement":Z
    const-string v3, "pref_goal_almost"

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    .line 34
    .local v2, "goalAlmost":Z
    const-string v3, "pref_activity_reminder"

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 36
    .local v0, "activityReminder":Z
    invoke-static {p0, v1, v2, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendActivityOptions(Landroid/content/Context;ZZZ)Ljava/util/UUID;

    .line 37
    return-void
.end method

.method public static sendWarningOptionsToWatch(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 43
    const-string v4, "pref_link_lost_icon"

    invoke-static {v4, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 45
    .local v0, "linkLostIcon":Z
    const-string v4, "pref_link_lost_notification"

    invoke-static {v4, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 48
    .local v1, "linkLostNotification":Z
    const-string v4, "pref_low_battery_icon"

    invoke-static {v4, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    .line 50
    .local v2, "lowBatteryIcon":Z
    const-string v4, "pref_low_battery_notification"

    invoke-static {v4, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    .line 52
    .local v3, "lowBatteryNotification":Z
    invoke-static {p0, v0, v2, v1, v3}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendWarningOption(Landroid/content/Context;ZZZZ)Ljava/util/UUID;

    .line 54
    return-void
.end method

.method public static syncAll(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-static {p0}, Lcom/vectorwatch/android/managers/SyncWatchSettingsManager;->sendWarningOptionsToWatch(Landroid/content/Context;)V

    .line 22
    invoke-static {p0}, Lcom/vectorwatch/android/managers/SyncWatchSettingsManager;->sendActivityOptionsToWatch(Landroid/content/Context;)V

    .line 23
    invoke-static {p0}, Lcom/vectorwatch/android/managers/SyncWatchSettingsManager;->syncTimeout(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public static syncTimeout(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, -0x1

    .line 60
    const-string v2, "pref_backlight_duration"

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 62
    .local v0, "timeout":I
    if-eq v0, v3, :cond_0

    .line 63
    sget-object v2, Lcom/vectorwatch/android/managers/SyncWatchSettingsManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BACKLIGHT - TIMEOUT - set to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 64
    const-string v2, "pref_backlight_duration"

    const/4 v3, 0x1

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 65
    const-string v2, "pref_backlight_duration"

    const/4 v3, 0x2

    .line 66
    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v2

    .line 65
    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->getWatchTimeoutDuration(I)I

    move-result v1

    .line 67
    .local v1, "timeoutInSeconds":I
    invoke-static {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBacklightTimeout(Landroid/content/Context;I)Ljava/util/UUID;

    .line 69
    .end local v1    # "timeoutInSeconds":I
    :cond_0
    return-void
.end method
