.class public Lcom/vectorwatch/android/managers/WatchActionRequestManager;
.super Ljava/lang/Object;
.source "WatchActionRequestManager.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;

.field private static mMediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findPhone(Landroid/content/Context;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const v4, 0x7f0701e1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x3

    .line 30
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/os/Vibrator;

    .line 35
    .local v12, "vibrator":Landroid/os/Vibrator;
    const/16 v0, 0xc

    new-array v10, v0, [J

    fill-array-data v10, :array_0

    .line 38
    .local v10, "pattern":[J
    const/4 v0, -0x1

    invoke-virtual {v12, v10, v0}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 41
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/media/AudioManager;

    .line 43
    .local v7, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v7, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v8

    .line 44
    .local v8, "currentVolume":I
    invoke-virtual {v7, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    invoke-virtual {v7, v1, v0, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 46
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 47
    .local v11, "resources":Landroid/content/res/Resources;
    invoke-virtual {v11, v4}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    .line 49
    .local v6, "assetFileDescriptor":Landroid/content/res/AssetFileDescriptor;
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 50
    invoke-static {p0, v4}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 52
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 53
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 56
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 57
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 59
    :try_start_0
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 60
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/vectorwatch/android/managers/WatchActionRequestManager$1;

    invoke-direct {v1, v7, v8}, Lcom/vectorwatch/android/managers/WatchActionRequestManager$1;-><init>(Landroid/media/AudioManager;I)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 66
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/vectorwatch/android/managers/WatchActionRequestManager$2;

    invoke-direct {v1, v7, v8}, Lcom/vectorwatch/android/managers/WatchActionRequestManager$2;-><init>(Landroid/media/AudioManager;I)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 73
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 74
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v9

    .line 76
    .local v9, "e":Ljava/io/IOException;
    sget-object v0, Lcom/vectorwatch/android/managers/WatchActionRequestManager;->log:Lorg/slf4j/Logger;

    const-string v1, "Cannot initialize media player for find my phone"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :array_0
    .array-data 8
        0x0
        0x64
        0x3e8
        0x0
        0x64
        0x3e8
        0x0
        0x64
        0x3e8
        0x0
        0x64
        0x3e8
    .end array-data
.end method
