.class final Lcom/vectorwatch/android/managers/StreamsManager$1;
.super Ljava/lang/Object;
.source "StreamsManager.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/managers/StreamsManager;->placeStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIIZLandroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$stream:Lcom/vectorwatch/android/models/Stream;

.field final synthetic val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iput-object p2, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    iput-object p3, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 4
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 228
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v1

    if-nez v1, :cond_2

    .line 230
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090294

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    iget-object v3, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$context:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    :cond_1
    :goto_0
    return-void

    .line 234
    :catch_0
    move-exception v0

    .line 235
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/vectorwatch/android/managers/StreamsManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Subscribe error: There was a problem with the stream option. Please try again."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 240
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/managers/StreamsManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SUBSCRIBE: Error message = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v1

    invoke-virtual {v1}, Lretrofit/client/Response;->getStatus()I

    move-result v1

    const/16 v2, 0x385

    if-ne v1, v2, :cond_3

    .line 242
    invoke-static {}, Lcom/vectorwatch/android/managers/StreamsManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Subscribe error: 901"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 243
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;

    iget-object v3, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 246
    :cond_3
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getUrl()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unexpected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 248
    :try_start_1
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 249
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    iget-object v3, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$context:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 256
    :catch_1
    move-exception v0

    .line 257
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/vectorwatch/android/managers/StreamsManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "Subscribe error: There was a problem with the stream option. Please try again."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 252
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    :try_start_2
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090294

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xbb8

    iget-object v3, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$context:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method

.method public success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V
    .locals 13
    .param p1, "streamDownloadResponse"    # Lcom/vectorwatch/android/models/StreamDownloadResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    const/4 v0, -0x1

    .line 182
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamDownloadResponse;->getData()Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamDownloadResponse;->getData()Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;

    move-result-object v1

    .line 183
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 185
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamDownloadResponse;->getData()Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v12

    .line 187
    .local v12, "responseStream":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 188
    :cond_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamDownloadResponse;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/managers/StreamsManager$1;->failure(Lretrofit/RetrofitError;)V

    .line 224
    .end local v12    # "responseStream":Lcom/vectorwatch/android/models/Stream;
    :cond_1
    :goto_0
    return-void

    .line 192
    .restart local v12    # "responseStream":Lcom/vectorwatch/android/models/Stream;
    :cond_2
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getStreamData()Lcom/vectorwatch/android/models/StreamValueModel;

    move-result-object v5

    .line 193
    .local v5, "modelData":Lcom/vectorwatch/android/models/StreamValueModel;
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v7

    .line 194
    .local v7, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    invoke-static {}, Lcom/vectorwatch/android/managers/StreamsManager;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Stream - standalone - channel label - "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v8

    invoke-virtual {v8}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 198
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/vectorwatch/android/models/Stream;->setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 200
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$stream:Lcom/vectorwatch/android/models/Stream;

    .line 201
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lcom/vectorwatch/android/models/Stream$StreamTypes;->MOBILE_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 206
    :cond_3
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 207
    .local v2, "appId":I
    :goto_1
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 208
    .local v3, "faceId":I
    :goto_2
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 209
    .local v4, "elementId":I
    :goto_3
    iget-object v0, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    iget-object v6, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$context:Landroid/content/Context;

    invoke-static/range {v0 .. v6}, Lcom/vectorwatch/android/managers/StreamsManager;->onPostExecuteSubscribeStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILcom/vectorwatch/android/models/StreamValueModel;Landroid/content/Context;)V

    goto/16 :goto_0

    .end local v2    # "appId":I
    .end local v3    # "faceId":I
    .end local v4    # "elementId":I
    :cond_4
    move v2, v0

    .line 206
    goto :goto_1

    .restart local v2    # "appId":I
    :cond_5
    move v3, v0

    .line 207
    goto :goto_2

    .restart local v3    # "faceId":I
    :cond_6
    move v4, v0

    .line 208
    goto :goto_3

    .line 211
    .end local v2    # "appId":I
    .end local v3    # "faceId":I
    :cond_7
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v1

    sget-object v6, Lcom/vectorwatch/android/models/Stream$StreamTypes;->WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 214
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 215
    .restart local v2    # "appId":I
    :goto_4
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 216
    .restart local v3    # "faceId":I
    :goto_5
    iget-object v1, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v0, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 218
    .restart local v4    # "elementId":I
    :goto_6
    iget-object v6, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iget-object v11, p0, Lcom/vectorwatch/android/managers/StreamsManager$1;->val$context:Landroid/content/Context;

    move v8, v2

    move v9, v3

    move v10, v4

    invoke-static/range {v6 .. v11}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->activateStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILandroid/content/Context;)V

    goto/16 :goto_0

    .end local v2    # "appId":I
    .end local v3    # "faceId":I
    .end local v4    # "elementId":I
    :cond_8
    move v2, v0

    .line 214
    goto :goto_4

    .restart local v2    # "appId":I
    :cond_9
    move v3, v0

    .line 215
    goto :goto_5

    .restart local v3    # "faceId":I
    :cond_a
    move v4, v0

    .line 216
    goto :goto_6

    .line 222
    .end local v2    # "appId":I
    .end local v3    # "faceId":I
    .end local v5    # "modelData":Lcom/vectorwatch/android/models/StreamValueModel;
    .end local v7    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v12    # "responseStream":Lcom/vectorwatch/android/models/Stream;
    :cond_b
    const-string v0, "Unexpected"

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/managers/StreamsManager$1;->failure(Lretrofit/RetrofitError;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 179
    check-cast p1, Lcom/vectorwatch/android/models/StreamDownloadResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/managers/StreamsManager$1;->success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V

    return-void
.end method
