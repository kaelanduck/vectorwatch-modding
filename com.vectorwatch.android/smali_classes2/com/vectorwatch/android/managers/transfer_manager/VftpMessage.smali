.class public Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;
.super Ljava/lang/Object;
.source "VftpMessage.java"


# instance fields
.field private extra:[B

.field private vftpMessageType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/utils/Constants$VftpMessageType;[B)V
    .locals 1
    .param p1, "vftpMessageType"    # Lcom/vectorwatch/android/utils/Constants$VftpMessageType;
    .param p2, "extra"    # [B

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->vftpMessageType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 13
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->extra:[B

    .line 16
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->vftpMessageType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 17
    iput-object p2, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->extra:[B

    .line 18
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4
    .param p1, "serializedMessage"    # [B

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->vftpMessageType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 13
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->extra:[B

    .line 21
    if-eqz p1, :cond_0

    array-length v0, p1

    if-le v0, v3, :cond_0

    .line 22
    aget-byte v0, p1, v2

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->getType(I)Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->vftpMessageType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    .line 23
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->extra:[B

    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->extra:[B

    array-length v1, p1

    invoke-static {p1, v3, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 26
    :cond_0
    return-void
.end method


# virtual methods
.method public getExtra()[B
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->extra:[B

    return-object v0
.end method

.method public getVftpMessageType()Lcom/vectorwatch/android/utils/Constants$VftpMessageType;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->vftpMessageType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    return-object v0
.end method

.method public serialize()[B
    .locals 3

    .prologue
    .line 37
    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->extra:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->vftpMessageType:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    if-nez v1, :cond_1

    .line 38
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [B

    .line 42
    :goto_0
    return-object v1

    .line 41
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpMessage;->extra:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 42
    .local v0, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    goto :goto_0
.end method
