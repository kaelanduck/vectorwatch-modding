.class public Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;
.super Ljava/lang/Object;
.source "VftpFile.java"


# instance fields
.field private compressedSize:Ljava/lang/Short;

.field private fileContent:[B

.field private fileId:Ljava/lang/Integer;

.field private isCompressed:Ljava/lang/Boolean;

.field private isForced:Ljava/lang/Boolean;

.field private type:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

.field private uncompressedSize:Ljava/lang/Short;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->type:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 10
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileId:Ljava/lang/Integer;

    .line 11
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileContent:[B

    .line 12
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->uncompressedSize:Ljava/lang/Short;

    .line 13
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->compressedSize:Ljava/lang/Short;

    .line 14
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isCompressed:Ljava/lang/Boolean;

    .line 15
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isForced:Ljava/lang/Boolean;

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/utils/Constants$VftpFileType;I[BSSZZ)V
    .locals 1
    .param p1, "type"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .param p2, "fileId"    # I
    .param p3, "fileContent"    # [B
    .param p4, "compressedSize"    # S
    .param p5, "uncompressedSize"    # S
    .param p6, "isCompressed"    # Z
    .param p7, "isForced"    # Z

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->type:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 10
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileId:Ljava/lang/Integer;

    .line 11
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileContent:[B

    .line 12
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->uncompressedSize:Ljava/lang/Short;

    .line 13
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->compressedSize:Ljava/lang/Short;

    .line 14
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isCompressed:Ljava/lang/Boolean;

    .line 15
    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isForced:Ljava/lang/Boolean;

    .line 24
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->type:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 25
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileId:Ljava/lang/Integer;

    .line 26
    iput-object p3, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileContent:[B

    .line 27
    invoke-static {p4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->compressedSize:Ljava/lang/Short;

    .line 28
    invoke-static {p5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->uncompressedSize:Ljava/lang/Short;

    .line 29
    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isCompressed:Ljava/lang/Boolean;

    .line 30
    invoke-static {p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isForced:Ljava/lang/Boolean;

    .line 31
    return-void
.end method


# virtual methods
.method public addContent(I[B)Z
    .locals 5
    .param p1, "position"    # I
    .param p2, "data"    # [B

    .prologue
    const/4 v1, 0x0

    .line 104
    if-nez p2, :cond_0

    .line 111
    :goto_0
    return v1

    .line 108
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileContent:[B

    array-length v4, p2

    invoke-static {p2, v2, v3, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    const/4 v1, 0x1

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public allocateDataBuffer(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 93
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileContent:[B

    .line 94
    return-void
.end method

.method public getCompressedSize()Ljava/lang/Short;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->compressedSize:Ljava/lang/Short;

    return-object v0
.end method

.method public getFileContent()[B
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileContent:[B

    return-object v0
.end method

.method public getFileId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getIsCompressed()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isCompressed:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getIsForced()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isForced:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->type:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    return-object v0
.end method

.method public getUncompressedSize()Ljava/lang/Short;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->uncompressedSize:Ljava/lang/Short;

    return-object v0
.end method

.method public setCompressedSize(Ljava/lang/Short;)V
    .locals 0
    .param p1, "compressedSize"    # Ljava/lang/Short;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->compressedSize:Ljava/lang/Short;

    .line 80
    return-void
.end method

.method public setFileContent([B)V
    .locals 0
    .param p1, "fileContent"    # [B

    .prologue
    .line 71
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileContent:[B

    .line 72
    return-void
.end method

.method public setFileId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "fileId"    # Ljava/lang/Integer;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->fileId:Ljava/lang/Integer;

    .line 68
    return-void
.end method

.method public setIsCompressed(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "isCompressed"    # Ljava/lang/Boolean;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isCompressed:Ljava/lang/Boolean;

    .line 84
    return-void
.end method

.method public setIsForced(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "isForced"    # Ljava/lang/Boolean;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->isForced:Ljava/lang/Boolean;

    .line 88
    return-void
.end method

.method public setType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V
    .locals 0
    .param p1, "type"    # Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->type:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 64
    return-void
.end method

.method public setUncompressedSize(Ljava/lang/Short;)V
    .locals 0
    .param p1, "uncompressedSize"    # Ljava/lang/Short;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->uncompressedSize:Ljava/lang/Short;

    .line 76
    return-void
.end method
