.class public Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;
.super Ljava/lang/Object;
.source "VftpHelpers.java"


# static fields
.field public static final FILE_PACKAGE_SIZE:I = 0x80

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDataPackageSize(II)I
    .locals 4
    .param p0, "chunkIndex"    # I
    .param p1, "contentSize"    # I

    .prologue
    const/16 v1, 0x80

    .line 62
    rem-int/lit16 v2, p1, 0x80

    if-nez v2, :cond_0

    .line 63
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - package size 128"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 75
    :goto_0
    return v1

    .line 67
    :cond_0
    invoke-static {p1}, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->getPackageCount(I)I

    move-result v0

    .line 69
    .local v0, "totalChunks":I
    add-int/lit8 v2, p0, 0x1

    if-ge v2, v0, :cond_1

    .line 70
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->log:Lorg/slf4j/Logger;

    const-string v3, "VFTP - package size 128"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 74
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VFTP - package size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    mul-int/lit16 v3, v3, 0x80

    sub-int v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 75
    add-int/lit8 v1, v0, -0x1

    mul-int/lit16 v1, v1, 0x80

    sub-int v1, p1, v1

    goto :goto_0
.end method

.method public static getFileSection(I[B)[B
    .locals 5
    .param p0, "chunkIndex"    # I
    .param p1, "fileContent"    # [B

    .prologue
    .line 42
    array-length v2, p1

    invoke-static {p0, v2}, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->getDataPackageSize(II)I

    move-result v1

    .line 43
    .local v1, "toWriteCount":I
    new-array v0, v1, [B

    .line 45
    .local v0, "section":[B
    sget-object v2, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "VFTP - get_file_section: chunk index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " content size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " allocated = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 47
    mul-int/lit16 v2, p0, 0x80

    const/4 v3, 0x0

    invoke-static {p1, v2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    return-object v0
.end method

.method public static getPackageCount(I)I
    .locals 4
    .param p0, "fileContentSize"    # I

    .prologue
    .line 22
    div-int/lit16 v0, p0, 0x80

    .line 24
    .local v0, "packageCount":I
    rem-int/lit16 v1, p0, 0x80

    if-eqz v1, :cond_0

    .line 25
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VFTP - Current file ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ) package count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 26
    add-int/lit8 v0, v0, 0x1

    .line 30
    .end local v0    # "packageCount":I
    :goto_0
    return v0

    .line 29
    .restart local v0    # "packageCount":I
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/managers/transfer_manager/VftpHelpers;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VFTP - Current file ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ) package count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method
