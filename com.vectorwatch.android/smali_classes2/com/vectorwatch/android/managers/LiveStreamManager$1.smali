.class Lcom/vectorwatch/android/managers/LiveStreamManager$1;
.super Ljava/lang/Object;
.source "LiveStreamManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/managers/LiveStreamManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/managers/LiveStreamManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/managers/LiveStreamManager;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 77
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$000(Lcom/vectorwatch/android/managers/LiveStreamManager;)V

    .line 78
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$100(Lcom/vectorwatch/android/managers/LiveStreamManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 79
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$100(Lcom/vectorwatch/android/managers/LiveStreamManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/LiveStream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/LiveStream;->getNextUpdate()I

    move-result v2

    if-gtz v2, :cond_0

    .line 80
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$100(Lcom/vectorwatch/android/managers/LiveStreamManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/LiveStream;

    .line 81
    .local v1, "liveStream":Lcom/vectorwatch/android/models/LiveStream;
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v2, v1}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$200(Lcom/vectorwatch/android/managers/LiveStreamManager;Lcom/vectorwatch/android/models/LiveStream;)V

    .line 82
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/LiveStream;->getRefreshInterval()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/LiveStream;->setNextUpdate(I)V

    .line 78
    .end local v1    # "liveStream":Lcom/vectorwatch/android/models/LiveStream;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$300(Lcom/vectorwatch/android/managers/LiveStreamManager;)V

    .line 88
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$100(Lcom/vectorwatch/android/managers/LiveStreamManager;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 89
    iget-object v2, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v2}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$600(Lcom/vectorwatch/android/managers/LiveStreamManager;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v3}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$400(Lcom/vectorwatch/android/managers/LiveStreamManager;)Ljava/lang/Runnable;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/managers/LiveStreamManager$1;->this$0:Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {v4}, Lcom/vectorwatch/android/managers/LiveStreamManager;->access$500(Lcom/vectorwatch/android/managers/LiveStreamManager;)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 91
    :cond_2
    return-void
.end method
