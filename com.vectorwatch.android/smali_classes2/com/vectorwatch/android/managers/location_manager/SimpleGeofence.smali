.class public Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;
.super Ljava/lang/Object;
.source "SimpleGeofence.java"


# instance fields
.field private mExpirationDuration:J

.field private final mId:Ljava/lang/String;

.field private final mLatitude:D

.field private final mLongitude:D

.field private final mRadius:F

.field private mTransitionType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;DDFJI)V
    .locals 0
    .param p1, "geofenceId"    # Ljava/lang/String;
    .param p2, "latitude"    # D
    .param p4, "longitude"    # D
    .param p6, "radius"    # F
    .param p7, "expiration"    # J
    .param p9, "transition"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mId:Ljava/lang/String;

    .line 30
    iput-wide p2, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mLatitude:D

    .line 31
    iput-wide p4, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mLongitude:D

    .line 32
    iput p6, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mRadius:F

    .line 33
    iput-wide p7, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mExpirationDuration:J

    .line 34
    iput p9, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mTransitionType:I

    .line 35
    return-void
.end method


# virtual methods
.method public getExpirationDuration()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mExpirationDuration:J

    return-wide v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mLatitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mLongitude:D

    return-wide v0
.end method

.method public getRadius()F
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mRadius:F

    return v0
.end method

.method public getTransitionType()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mTransitionType:I

    return v0
.end method

.method public toGeofence()Lcom/google/android/gms/location/Geofence;
    .locals 7

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/location/Geofence$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/location/Geofence$Builder;-><init>()V

    iget-object v1, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mId:Ljava/lang/String;

    .line 64
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/Geofence$Builder;->setRequestId(Ljava/lang/String;)Lcom/google/android/gms/location/Geofence$Builder;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mTransitionType:I

    .line 65
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/Geofence$Builder;->setTransitionTypes(I)Lcom/google/android/gms/location/Geofence$Builder;

    move-result-object v1

    iget-wide v2, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mLatitude:D

    iget-wide v4, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mLongitude:D

    iget v6, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mRadius:F

    .line 66
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/location/Geofence$Builder;->setCircularRegion(DDF)Lcom/google/android/gms/location/Geofence$Builder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vectorwatch/android/managers/location_manager/SimpleGeofence;->mExpirationDuration:J

    .line 67
    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/Geofence$Builder;->setExpirationDuration(J)Lcom/google/android/gms/location/Geofence$Builder;

    move-result-object v0

    const v1, 0xdbba0

    .line 68
    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/Geofence$Builder;->setLoiteringDelay(I)Lcom/google/android/gms/location/Geofence$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/google/android/gms/location/Geofence$Builder;->build()Lcom/google/android/gms/location/Geofence;

    move-result-object v0

    return-object v0
.end method
