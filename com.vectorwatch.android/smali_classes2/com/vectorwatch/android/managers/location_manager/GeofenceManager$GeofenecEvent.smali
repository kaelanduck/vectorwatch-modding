.class final enum Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;
.super Ljava/lang/Enum;
.source "GeofenceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/managers/location_manager/GeofenceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "GeofenecEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

.field public static final enum GEOFENCE_EVENT_DWELL:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

.field public static final enum GEOFENCE_EVENT_ENTER:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

.field public static final enum GEOFENCE_EVENT_EXIT:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    new-instance v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    const-string v1, "GEOFENCE_EVENT_ENTER"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;->GEOFENCE_EVENT_ENTER:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    .line 73
    new-instance v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    const-string v1, "GEOFENCE_EVENT_DWELL"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;->GEOFENCE_EVENT_DWELL:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    .line 74
    new-instance v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    const-string v1, "GEOFENCE_EVENT_EXIT"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;->GEOFENCE_EVENT_EXIT:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;->GEOFENCE_EVENT_ENTER:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;->GEOFENCE_EVENT_DWELL:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;->GEOFENCE_EVENT_EXIT:Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    aput-object v1, v0, v4

    sput-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;->$VALUES:[Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 71
    const-class v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;->$VALUES:[Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/managers/location_manager/GeofenceManager$GeofenecEvent;

    return-object v0
.end method
