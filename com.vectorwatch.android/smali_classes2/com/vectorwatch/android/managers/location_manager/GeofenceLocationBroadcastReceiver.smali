.class public Lcom/vectorwatch/android/managers/location_manager/GeofenceLocationBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GeofenceLocationBroadcastReceiver.java"


# static fields
.field private static final UPDATE_INTERVAL:J = 0x1388L

.field private static lastUpdate:J

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const-class v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceLocationBroadcastReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceLocationBroadcastReceiver;->log:Lorg/slf4j/Logger;

    .line 27
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/vectorwatch/android/managers/location_manager/GeofenceLocationBroadcastReceiver;->lastUpdate:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 67
    return-void
.end method
