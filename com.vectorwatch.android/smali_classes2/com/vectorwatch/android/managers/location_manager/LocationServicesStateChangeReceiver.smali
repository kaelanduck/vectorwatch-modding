.class public Lcom/vectorwatch/android/managers/location_manager/LocationServicesStateChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocationServicesStateChangeReceiver.java"


# static fields
.field private static final UPDATE_INTERVAL:J = 0x927c0L

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/vectorwatch/android/managers/location_manager/LocationServicesStateChangeReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/managers/location_manager/LocationServicesStateChangeReceiver;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 53
    return-void
.end method
