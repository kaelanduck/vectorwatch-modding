.class public Lcom/hannesdorfmann/annotatedadapter/AutoAbsListViewDelegators;
.super Ljava/lang/Object;
.source "AutoAbsListViewDelegators.java"

# interfaces
.implements Lcom/hannesdorfmann/annotatedadapter/AbsListViewDelegators;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDelegator(Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;)Lcom/hannesdorfmann/annotatedadapter/AbsListViewAdapterDelegator;
    .locals 4
    .param p1, "adapter"    # Lcom/hannesdorfmann/annotatedadapter/AbsListViewAnnotatedAdapter;

    .prologue
    .line 12
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 14
    .local v0, "name":Ljava/lang/String;
    const-string v1, "com.vectorwatch.android.ui.tabmenufragments.activity.adapter.NotificationAlertListAdapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 15
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterAdapterDelegator;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/NotificationAlertListAdapterAdapterDelegator;-><init>()V

    .line 27
    :goto_0
    return-object v1

    .line 18
    :cond_0
    const-string v1, "com.vectorwatch.android.ui.tabmenufragments.activity.adapter.ActivityListAdapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 19
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterAdapterDelegator;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/adapter/ActivityListAdapterAdapterDelegator;-><init>()V

    goto :goto_0

    .line 22
    :cond_1
    const-string v1, "com.vectorwatch.android.ui.tabmenufragments.store.adapter.StoreRatingAdapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 23
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterAdapterDelegator;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StoreRatingAdapterAdapterDelegator;-><init>()V

    goto :goto_0

    .line 26
    :cond_2
    const-string v1, "com.vectorwatch.android.ui.tabmenufragments.store.adapter.StorePlacesAdapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 27
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterAdapterDelegator;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/tabmenufragments/store/adapter/StorePlacesAdapterAdapterDelegator;-><init>()V

    goto :goto_0

    .line 30
    :cond_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not find adapter delegate for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
