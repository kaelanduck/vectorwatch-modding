.class public final enum Lcom/amazonaws/auth/policy/actions/SNSActions;
.super Ljava/lang/Enum;
.source "SNSActions.java"

# interfaces
.implements Lcom/amazonaws/auth/policy/Action;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/amazonaws/auth/policy/actions/SNSActions;",
        ">;",
        "Lcom/amazonaws/auth/policy/Action;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum AddPermission:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum AllSNSActions:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum CheckIfPhoneNumberIsOptedOut:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum ConfirmSubscription:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum CreatePlatformApplication:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum CreatePlatformEndpoint:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum CreateTopic:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum DeleteEndpoint:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum DeletePlatformApplication:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum DeleteTopic:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum GetEndpointAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum GetPlatformApplicationAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum GetSMSAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum GetSubscriptionAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum GetTopicAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum ListEndpointsByPlatformApplication:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum ListPhoneNumbersOptedOut:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum ListPlatformApplications:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum ListSubscriptions:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum ListSubscriptionsByTopic:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum ListTopics:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum OptInPhoneNumber:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum Publish:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum RemovePermission:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum SetEndpointAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum SetPlatformApplicationAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum SetSMSAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum SetSubscriptionAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum SetTopicAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum Subscribe:Lcom/amazonaws/auth/policy/actions/SNSActions;

.field public static final enum Unsubscribe:Lcom/amazonaws/auth/policy/actions/SNSActions;


# instance fields
.field private final action:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "AllSNSActions"

    const-string v2, "sns:*"

    invoke-direct {v0, v1, v4, v2}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->AllSNSActions:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 29
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "AddPermission"

    const-string v2, "sns:AddPermission"

    invoke-direct {v0, v1, v5, v2}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->AddPermission:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 32
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "CheckIfPhoneNumberIsOptedOut"

    const-string v2, "sns:CheckIfPhoneNumberIsOptedOut"

    invoke-direct {v0, v1, v6, v2}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->CheckIfPhoneNumberIsOptedOut:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 35
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "ConfirmSubscription"

    const-string v2, "sns:ConfirmSubscription"

    invoke-direct {v0, v1, v7, v2}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->ConfirmSubscription:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 38
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "CreatePlatformApplication"

    const-string v2, "sns:CreatePlatformApplication"

    invoke-direct {v0, v1, v8, v2}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->CreatePlatformApplication:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 41
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "CreatePlatformEndpoint"

    const/4 v2, 0x5

    const-string v3, "sns:CreatePlatformEndpoint"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->CreatePlatformEndpoint:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 44
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "CreateTopic"

    const/4 v2, 0x6

    const-string v3, "sns:CreateTopic"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->CreateTopic:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 47
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "DeleteEndpoint"

    const/4 v2, 0x7

    const-string v3, "sns:DeleteEndpoint"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->DeleteEndpoint:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 50
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "DeletePlatformApplication"

    const/16 v2, 0x8

    const-string v3, "sns:DeletePlatformApplication"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->DeletePlatformApplication:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 53
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "DeleteTopic"

    const/16 v2, 0x9

    const-string v3, "sns:DeleteTopic"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->DeleteTopic:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 56
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "GetEndpointAttributes"

    const/16 v2, 0xa

    const-string v3, "sns:GetEndpointAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetEndpointAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 59
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "GetPlatformApplicationAttributes"

    const/16 v2, 0xb

    const-string v3, "sns:GetPlatformApplicationAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetPlatformApplicationAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 62
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "GetSMSAttributes"

    const/16 v2, 0xc

    const-string v3, "sns:GetSMSAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetSMSAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 65
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "GetSubscriptionAttributes"

    const/16 v2, 0xd

    const-string v3, "sns:GetSubscriptionAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetSubscriptionAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 68
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "GetTopicAttributes"

    const/16 v2, 0xe

    const-string v3, "sns:GetTopicAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetTopicAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 71
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "ListEndpointsByPlatformApplication"

    const/16 v2, 0xf

    const-string v3, "sns:ListEndpointsByPlatformApplication"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListEndpointsByPlatformApplication:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 74
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "ListPhoneNumbersOptedOut"

    const/16 v2, 0x10

    const-string v3, "sns:ListPhoneNumbersOptedOut"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListPhoneNumbersOptedOut:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 77
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "ListPlatformApplications"

    const/16 v2, 0x11

    const-string v3, "sns:ListPlatformApplications"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListPlatformApplications:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 80
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "ListSubscriptions"

    const/16 v2, 0x12

    const-string v3, "sns:ListSubscriptions"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListSubscriptions:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 83
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "ListSubscriptionsByTopic"

    const/16 v2, 0x13

    const-string v3, "sns:ListSubscriptionsByTopic"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListSubscriptionsByTopic:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 86
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "ListTopics"

    const/16 v2, 0x14

    const-string v3, "sns:ListTopics"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListTopics:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 89
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "OptInPhoneNumber"

    const/16 v2, 0x15

    const-string v3, "sns:OptInPhoneNumber"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->OptInPhoneNumber:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 92
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "Publish"

    const/16 v2, 0x16

    const-string v3, "sns:Publish"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->Publish:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 95
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "RemovePermission"

    const/16 v2, 0x17

    const-string v3, "sns:RemovePermission"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->RemovePermission:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 98
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "SetEndpointAttributes"

    const/16 v2, 0x18

    const-string v3, "sns:SetEndpointAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetEndpointAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 101
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "SetPlatformApplicationAttributes"

    const/16 v2, 0x19

    const-string v3, "sns:SetPlatformApplicationAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetPlatformApplicationAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 104
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "SetSMSAttributes"

    const/16 v2, 0x1a

    const-string v3, "sns:SetSMSAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetSMSAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 107
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "SetSubscriptionAttributes"

    const/16 v2, 0x1b

    const-string v3, "sns:SetSubscriptionAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetSubscriptionAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 110
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "SetTopicAttributes"

    const/16 v2, 0x1c

    const-string v3, "sns:SetTopicAttributes"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetTopicAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 113
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "Subscribe"

    const/16 v2, 0x1d

    const-string v3, "sns:Subscribe"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->Subscribe:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 116
    new-instance v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    const-string v1, "Unsubscribe"

    const/16 v2, 0x1e

    const-string v3, "sns:Unsubscribe"

    invoke-direct {v0, v1, v2, v3}, Lcom/amazonaws/auth/policy/actions/SNSActions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->Unsubscribe:Lcom/amazonaws/auth/policy/actions/SNSActions;

    .line 24
    const/16 v0, 0x1f

    new-array v0, v0, [Lcom/amazonaws/auth/policy/actions/SNSActions;

    sget-object v1, Lcom/amazonaws/auth/policy/actions/SNSActions;->AllSNSActions:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v1, v0, v4

    sget-object v1, Lcom/amazonaws/auth/policy/actions/SNSActions;->AddPermission:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v1, v0, v5

    sget-object v1, Lcom/amazonaws/auth/policy/actions/SNSActions;->CheckIfPhoneNumberIsOptedOut:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v1, v0, v6

    sget-object v1, Lcom/amazonaws/auth/policy/actions/SNSActions;->ConfirmSubscription:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v1, v0, v7

    sget-object v1, Lcom/amazonaws/auth/policy/actions/SNSActions;->CreatePlatformApplication:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->CreatePlatformEndpoint:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->CreateTopic:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->DeleteEndpoint:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->DeletePlatformApplication:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->DeleteTopic:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetEndpointAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetPlatformApplicationAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetSMSAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetSubscriptionAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->GetTopicAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListEndpointsByPlatformApplication:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListPhoneNumbersOptedOut:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListPlatformApplications:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListSubscriptions:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListSubscriptionsByTopic:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->ListTopics:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->OptInPhoneNumber:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->Publish:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->RemovePermission:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetEndpointAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetPlatformApplicationAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetSMSAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetSubscriptionAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->SetTopicAttributes:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->Subscribe:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/amazonaws/auth/policy/actions/SNSActions;->Unsubscribe:Lcom/amazonaws/auth/policy/actions/SNSActions;

    aput-object v2, v0, v1

    sput-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->$VALUES:[Lcom/amazonaws/auth/policy/actions/SNSActions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "action"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 121
    iput-object p3, p0, Lcom/amazonaws/auth/policy/actions/SNSActions;->action:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amazonaws/auth/policy/actions/SNSActions;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/amazonaws/auth/policy/actions/SNSActions;

    return-object v0
.end method

.method public static values()[Lcom/amazonaws/auth/policy/actions/SNSActions;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/amazonaws/auth/policy/actions/SNSActions;->$VALUES:[Lcom/amazonaws/auth/policy/actions/SNSActions;

    invoke-virtual {v0}, [Lcom/amazonaws/auth/policy/actions/SNSActions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/amazonaws/auth/policy/actions/SNSActions;

    return-object v0
.end method


# virtual methods
.method public getActionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/amazonaws/auth/policy/actions/SNSActions;->action:Ljava/lang/String;

    return-object v0
.end method
