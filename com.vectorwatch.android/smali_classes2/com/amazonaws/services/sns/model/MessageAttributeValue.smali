.class public Lcom/amazonaws/services/sns/model/MessageAttributeValue;
.super Ljava/lang/Object;
.source "MessageAttributeValue.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private binaryValue:Ljava/nio/ByteBuffer;

.field private dataType:Ljava/lang/String;

.field private stringValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 283
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 306
    :cond_0
    :goto_0
    return v3

    .line 285
    :cond_1
    if-eqz p1, :cond_0

    .line 288
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/MessageAttributeValue;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 290
    check-cast v0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;

    .line 292
    .local v0, "other":Lcom/amazonaws/services/sns/model/MessageAttributeValue;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 294
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 296
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_8

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 298
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 299
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 301
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v4

    if-nez v4, :cond_a

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 303
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 304
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    move v3, v2

    .line 306
    goto :goto_0

    :cond_5
    move v1, v3

    .line 292
    goto :goto_1

    :cond_6
    move v4, v3

    goto :goto_2

    :cond_7
    move v1, v3

    .line 296
    goto :goto_3

    :cond_8
    move v4, v3

    goto :goto_4

    :cond_9
    move v1, v3

    .line 301
    goto :goto_5

    :cond_a
    move v4, v3

    goto :goto_6
.end method

.method public getBinaryValue()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->binaryValue:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public getDataType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->dataType:Ljava/lang/String;

    return-object v0
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->stringValue:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 270
    const/16 v1, 0x1f

    .line 271
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 273
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 274
    mul-int/lit8 v4, v0, 0x1f

    .line 275
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 276
    mul-int/lit8 v2, v0, 0x1f

    .line 277
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v4

    if-nez v4, :cond_2

    :goto_2
    add-int v0, v2, v3

    .line 278
    return v0

    .line 273
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 275
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 277
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->hashCode()I

    move-result v3

    goto :goto_2
.end method

.method public setBinaryValue(Ljava/nio/ByteBuffer;)V
    .locals 0
    .param p1, "binaryValue"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->binaryValue:Ljava/nio/ByteBuffer;

    .line 224
    return-void
.end method

.method public setDataType(Ljava/lang/String;)V
    .locals 0
    .param p1, "dataType"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->dataType:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setStringValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "stringValue"    # Ljava/lang/String;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->stringValue:Ljava/lang/String;

    .line 168
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 257
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DataType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 261
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StringValue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 263
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BinaryValue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    :cond_2
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withBinaryValue(Ljava/nio/ByteBuffer;)Lcom/amazonaws/services/sns/model/MessageAttributeValue;
    .locals 0
    .param p1, "binaryValue"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->binaryValue:Ljava/nio/ByteBuffer;

    .line 244
    return-object p0
.end method

.method public withDataType(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/MessageAttributeValue;
    .locals 0
    .param p1, "dataType"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->dataType:Ljava/lang/String;

    .line 127
    return-object p0
.end method

.method public withStringValue(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/MessageAttributeValue;
    .locals 0
    .param p1, "stringValue"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->stringValue:Ljava/lang/String;

    .line 193
    return-object p0
.end method
