.class public Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "ConfirmSubscriptionRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private authenticateOnUnsubscribe:Ljava/lang/String;

.field private token:Ljava/lang/String;

.field private topicArn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 81
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setTopicArn(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0, p2}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setToken(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;
    .param p3, "authenticateOnUnsubscribe"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 108
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setTopicArn(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0, p2}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setToken(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0, p3}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->setAuthenticateOnUnsubscribe(Ljava/lang/String;)V

    .line 111
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 318
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 341
    :cond_0
    :goto_0
    return v3

    .line 320
    :cond_1
    if-eqz p1, :cond_0

    .line 323
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 325
    check-cast v0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;

    .line 327
    .local v0, "other":Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 329
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getToken()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getToken()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_8

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 333
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 335
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getAuthenticateOnUnsubscribe()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    move v1, v2

    .line 336
    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getAuthenticateOnUnsubscribe()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_a

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 338
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getAuthenticateOnUnsubscribe()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 339
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getAuthenticateOnUnsubscribe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getAuthenticateOnUnsubscribe()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    move v3, v2

    .line 341
    goto :goto_0

    :cond_5
    move v1, v3

    .line 327
    goto :goto_1

    :cond_6
    move v4, v3

    goto :goto_2

    :cond_7
    move v1, v3

    .line 331
    goto :goto_3

    :cond_8
    move v4, v3

    goto :goto_4

    :cond_9
    move v1, v3

    .line 335
    goto :goto_5

    :cond_a
    move v4, v3

    .line 336
    goto :goto_6
.end method

.method public getAuthenticateOnUnsubscribe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->authenticateOnUnsubscribe:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->token:Ljava/lang/String;

    return-object v0
.end method

.method public getTopicArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->topicArn:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 304
    const/16 v1, 0x1f

    .line 305
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 307
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 308
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getToken()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 309
    mul-int/lit8 v2, v0, 0x1f

    .line 311
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getAuthenticateOnUnsubscribe()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 312
    :goto_2
    add-int v0, v2, v3

    .line 313
    return v0

    .line 307
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 308
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 311
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getAuthenticateOnUnsubscribe()Ljava/lang/String;

    move-result-object v3

    .line 312
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_2
.end method

.method public setAuthenticateOnUnsubscribe(Ljava/lang/String;)V
    .locals 0
    .param p1, "authenticateOnUnsubscribe"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->authenticateOnUnsubscribe:Ljava/lang/String;

    .line 252
    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->token:Ljava/lang/String;

    .line 189
    return-void
.end method

.method public setTopicArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->topicArn:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 290
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 291
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TopicArn: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 295
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Token: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getAuthenticateOnUnsubscribe()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 297
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AuthenticateOnUnsubscribe: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->getAuthenticateOnUnsubscribe()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    :cond_2
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withAuthenticateOnUnsubscribe(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;
    .locals 0
    .param p1, "authenticateOnUnsubscribe"    # Ljava/lang/String;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->authenticateOnUnsubscribe:Ljava/lang/String;

    .line 278
    return-object p0
.end method

.method public withToken(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;
    .locals 0
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->token:Ljava/lang/String;

    .line 209
    return-object p0
.end method

.method public withTopicArn(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ConfirmSubscriptionRequest;->topicArn:Ljava/lang/String;

    .line 158
    return-object p0
.end method
