.class public Lcom/amazonaws/services/sns/model/PublishRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "PublishRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private message:Ljava/lang/String;

.field private messageAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sns/model/MessageAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field private messageStructure:Ljava/lang/String;

.field private phoneNumber:Ljava/lang/String;

.field private subject:Ljava/lang/String;

.field private targetArn:Ljava/lang/String;

.field private topicArn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    .line 224
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    .line 318
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->setTopicArn(Ljava/lang/String;)V

    .line 319
    invoke-virtual {p0, p2}, Lcom/amazonaws/services/sns/model/PublishRequest;->setMessage(Ljava/lang/String;)V

    .line 320
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "subject"    # Ljava/lang/String;

    .prologue
    .line 425
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 216
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    .line 426
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->setTopicArn(Ljava/lang/String;)V

    .line 427
    invoke-virtual {p0, p2}, Lcom/amazonaws/services/sns/model/PublishRequest;->setMessage(Ljava/lang/String;)V

    .line 428
    invoke-virtual {p0, p3}, Lcom/amazonaws/services/sns/model/PublishRequest;->setSubject(Ljava/lang/String;)V

    .line 429
    return-void
.end method


# virtual methods
.method public addMessageAttributesEntry(Ljava/lang/String;Lcom/amazonaws/services/sns/model/MessageAttributeValue;)Lcom/amazonaws/services/sns/model/PublishRequest;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lcom/amazonaws/services/sns/model/MessageAttributeValue;

    .prologue
    .line 1532
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 1533
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    .line 1535
    :cond_0
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1536
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicated keys ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") are provided."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1538
    :cond_1
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1539
    return-object p0
.end method

.method public clearMessageAttributesEntries()Lcom/amazonaws/services/sns/model/PublishRequest;
    .locals 1

    .prologue
    .line 1549
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    .line 1550
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1602
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 1643
    :cond_0
    :goto_0
    return v3

    .line 1604
    :cond_1
    if-eqz p1, :cond_0

    .line 1607
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/PublishRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 1609
    check-cast v0, Lcom/amazonaws/services/sns/model/PublishRequest;

    .line 1611
    .local v0, "other":Lcom/amazonaws/services/sns/model/PublishRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_a

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1613
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1615
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_b

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_c

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1617
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1618
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1620
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_e

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1622
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1623
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1625
    :cond_4
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_f

    move v1, v2

    :goto_7
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_10

    move v4, v2

    :goto_8
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1627
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1629
    :cond_5
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_11

    move v1, v2

    :goto_9
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_12

    move v4, v2

    :goto_a
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1631
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1633
    :cond_6
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_13

    move v1, v2

    :goto_b
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_14

    move v4, v2

    :goto_c
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1635
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1636
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1638
    :cond_7
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_15

    move v1, v2

    :goto_d
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_16

    move v4, v2

    :goto_e
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1640
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1641
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_8
    move v3, v2

    .line 1643
    goto/16 :goto_0

    :cond_9
    move v1, v3

    .line 1611
    goto/16 :goto_1

    :cond_a
    move v4, v3

    goto/16 :goto_2

    :cond_b
    move v1, v3

    .line 1615
    goto/16 :goto_3

    :cond_c
    move v4, v3

    goto/16 :goto_4

    :cond_d
    move v1, v3

    .line 1620
    goto/16 :goto_5

    :cond_e
    move v4, v3

    goto/16 :goto_6

    :cond_f
    move v1, v3

    .line 1625
    goto/16 :goto_7

    :cond_10
    move v4, v3

    goto/16 :goto_8

    :cond_11
    move v1, v3

    .line 1629
    goto/16 :goto_9

    :cond_12
    move v4, v3

    goto :goto_a

    :cond_13
    move v1, v3

    .line 1633
    goto :goto_b

    :cond_14
    move v4, v3

    goto :goto_c

    :cond_15
    move v1, v3

    .line 1638
    goto :goto_d

    :cond_16
    move v4, v3

    goto :goto_e
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 814
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sns/model/MessageAttributeValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1480
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getMessageStructure()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageStructure:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->targetArn:Ljava/lang/String;

    return-object v0
.end method

.method public getTopicArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->topicArn:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1584
    const/16 v1, 0x1f

    .line 1585
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 1587
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 1588
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 1589
    mul-int/lit8 v4, v0, 0x1f

    .line 1590
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v0, v4, v2

    .line 1591
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v0, v4, v2

    .line 1592
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v0, v4, v2

    .line 1593
    mul-int/lit8 v4, v0, 0x1f

    .line 1594
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    move v2, v3

    :goto_5
    add-int v0, v4, v2

    .line 1595
    mul-int/lit8 v2, v0, 0x1f

    .line 1596
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_6

    :goto_6
    add-int v0, v2, v3

    .line 1597
    return v0

    .line 1587
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 1588
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 1590
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 1591
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    .line 1592
    :cond_4
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    .line 1594
    :cond_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    .line 1596
    :cond_6
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->hashCode()I

    move-result v3

    goto :goto_6
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 970
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->message:Ljava/lang/String;

    .line 971
    return-void
.end method

.method public setMessageAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sns/model/MessageAttributeValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1493
    .local p1, "messageAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sns/model/MessageAttributeValue;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    .line 1494
    return-void
.end method

.method public setMessageStructure(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageStructure"    # Ljava/lang/String;

    .prologue
    .line 1381
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageStructure:Ljava/lang/String;

    .line 1382
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 628
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->phoneNumber:Ljava/lang/String;

    .line 629
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 1189
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->subject:Ljava/lang/String;

    .line 1190
    return-void
.end method

.method public setTargetArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "targetArn"    # Ljava/lang/String;

    .prologue
    .line 549
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->targetArn:Ljava/lang/String;

    .line 550
    return-void
.end method

.method public setTopicArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;

    .prologue
    .line 474
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->topicArn:Ljava/lang/String;

    .line 475
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1562
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1563
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1564
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1565
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TopicArn: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1566
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1567
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TargetArn: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1568
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1569
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PhoneNumber: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1570
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1571
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1572
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1573
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Subject: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1574
    :cond_4
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1575
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageStructure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1576
    :cond_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1577
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageAttributes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1578
    :cond_6
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1579
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withMessage(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/PublishRequest;
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 1131
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->message:Ljava/lang/String;

    .line 1132
    return-object p0
.end method

.method public withMessageAttributes(Ljava/util/Map;)Lcom/amazonaws/services/sns/model/PublishRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/amazonaws/services/sns/model/MessageAttributeValue;",
            ">;)",
            "Lcom/amazonaws/services/sns/model/PublishRequest;"
        }
    .end annotation

    .prologue
    .line 1512
    .local p1, "messageAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sns/model/MessageAttributeValue;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageAttributes:Ljava/util/Map;

    .line 1513
    return-object p0
.end method

.method public withMessageStructure(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/PublishRequest;
    .locals 0
    .param p1, "messageStructure"    # Ljava/lang/String;

    .prologue
    .line 1466
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->messageStructure:Ljava/lang/String;

    .line 1467
    return-object p0
.end method

.method public withPhoneNumber(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/PublishRequest;
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 658
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->phoneNumber:Ljava/lang/String;

    .line 659
    return-object p0
.end method

.method public withSubject(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/PublishRequest;
    .locals 0
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 1223
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->subject:Ljava/lang/String;

    .line 1224
    return-object p0
.end method

.method public withTargetArn(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/PublishRequest;
    .locals 0
    .param p1, "targetArn"    # Ljava/lang/String;

    .prologue
    .line 577
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->targetArn:Ljava/lang/String;

    .line 578
    return-object p0
.end method

.method public withTopicArn(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/PublishRequest;
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;

    .prologue
    .line 502
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/PublishRequest;->topicArn:Ljava/lang/String;

    .line 503
    return-object p0
.end method
