.class public Lcom/amazonaws/services/sns/model/AddPermissionRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "AddPermissionRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private aWSAccountIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private actionNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private label:Ljava/lang/String;

.field private topicArn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->aWSAccountIds:Ljava/util/List;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->actionNames:Ljava/util/List;

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p3, "aWSAccountIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "actionNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->aWSAccountIds:Ljava/util/List;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->actionNames:Ljava/util/List;

    .line 96
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setTopicArn(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0, p2}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setLabel(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p0, p3}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setAWSAccountIds(Ljava/util/Collection;)V

    .line 99
    invoke-virtual {p0, p4}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setActionNames(Ljava/util/Collection;)V

    .line 100
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 422
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 449
    :cond_0
    :goto_0
    return v3

    .line 424
    :cond_1
    if-eqz p1, :cond_0

    .line 427
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/AddPermissionRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 429
    check-cast v0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;

    .line 431
    .local v0, "other":Lcom/amazonaws/services/sns/model/AddPermissionRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_7

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 433
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 435
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_9

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 437
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 439
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_a

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_b

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 441
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 442
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 444
    :cond_4
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_c

    move v1, v2

    :goto_7
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_d

    move v4, v2

    :goto_8
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 446
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 447
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_5
    move v3, v2

    .line 449
    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 431
    goto/16 :goto_1

    :cond_7
    move v4, v3

    goto/16 :goto_2

    :cond_8
    move v1, v3

    .line 435
    goto :goto_3

    :cond_9
    move v4, v3

    goto :goto_4

    :cond_a
    move v1, v3

    .line 439
    goto :goto_5

    :cond_b
    move v4, v3

    goto :goto_6

    :cond_c
    move v1, v3

    .line 444
    goto :goto_7

    :cond_d
    move v4, v3

    goto :goto_8
.end method

.method public getAWSAccountIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->aWSAccountIds:Ljava/util/List;

    return-object v0
.end method

.method public getActionNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->actionNames:Ljava/util/List;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getTopicArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->topicArn:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 408
    const/16 v1, 0x1f

    .line 409
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 411
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 412
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 413
    mul-int/lit8 v4, v0, 0x1f

    .line 414
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v0, v4, v2

    .line 415
    mul-int/lit8 v2, v0, 0x1f

    .line 416
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_3

    :goto_3
    add-int v0, v2, v3

    .line 417
    return v0

    .line 411
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 412
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 414
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_2

    .line 416
    :cond_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    goto :goto_3
.end method

.method public setAWSAccountIds(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    .local p1, "aWSAccountIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 227
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->aWSAccountIds:Ljava/util/List;

    .line 232
    :goto_0
    return-void

    .line 231
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->aWSAccountIds:Ljava/util/List;

    goto :goto_0
.end method

.method public setActionNames(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 320
    .local p1, "actionNames":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->actionNames:Ljava/util/List;

    .line 326
    :goto_0
    return-void

    .line 325
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->actionNames:Ljava/util/List;

    goto :goto_0
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->label:Ljava/lang/String;

    .line 174
    return-void
.end method

.method public setTopicArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->topicArn:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 392
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 393
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 395
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TopicArn: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 397
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Label: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 399
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AWSAccountIds: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 401
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ActionNames: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    :cond_3
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withAWSAccountIds(Ljava/util/Collection;)Lcom/amazonaws/services/sns/model/AddPermissionRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sns/model/AddPermissionRequest;"
        }
    .end annotation

    .prologue
    .line 281
    .local p1, "aWSAccountIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setAWSAccountIds(Ljava/util/Collection;)V

    .line 282
    return-object p0
.end method

.method public varargs withAWSAccountIds([Ljava/lang/String;)Lcom/amazonaws/services/sns/model/AddPermissionRequest;
    .locals 4
    .param p1, "aWSAccountIds"    # [Ljava/lang/String;

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getAWSAccountIds()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 254
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->aWSAccountIds:Ljava/util/List;

    .line 256
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 257
    .local v0, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->aWSAccountIds:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 259
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    return-object p0
.end method

.method public withActionNames(Ljava/util/Collection;)Lcom/amazonaws/services/sns/model/AddPermissionRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sns/model/AddPermissionRequest;"
        }
    .end annotation

    .prologue
    .line 379
    .local p1, "actionNames":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->setActionNames(Ljava/util/Collection;)V

    .line 380
    return-object p0
.end method

.method public varargs withActionNames([Ljava/lang/String;)Lcom/amazonaws/services/sns/model/AddPermissionRequest;
    .locals 4
    .param p1, "actionNames"    # [Ljava/lang/String;

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->getActionNames()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 350
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->actionNames:Ljava/util/List;

    .line 352
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 353
    .local v0, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->actionNames:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 355
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    return-object p0
.end method

.method public withLabel(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/AddPermissionRequest;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->label:Ljava/lang/String;

    .line 192
    return-object p0
.end method

.method public withTopicArn(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/AddPermissionRequest;
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/AddPermissionRequest;->topicArn:Ljava/lang/String;

    .line 147
    return-object p0
.end method
