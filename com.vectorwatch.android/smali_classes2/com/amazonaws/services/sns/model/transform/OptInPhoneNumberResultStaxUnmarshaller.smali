.class public Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;
.super Ljava/lang/Object;
.source "OptInPhoneNumberResultStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sns/model/OptInPhoneNumberResult;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;

    .line 64
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/OptInPhoneNumberResult;
    .locals 5
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lcom/amazonaws/services/sns/model/OptInPhoneNumberResult;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/OptInPhoneNumberResult;-><init>()V

    .line 38
    .local v0, "optInPhoneNumberResult":Lcom/amazonaws/services/sns/model/OptInPhoneNumberResult;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v1

    .line 39
    .local v1, "originalDepth":I
    add-int/lit8 v2, v1, 0x1

    .line 41
    .local v2, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 42
    add-int/lit8 v2, v2, 0x2

    .line 45
    :cond_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v3

    .line 46
    .local v3, "xmlEvent":I
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 56
    :goto_0
    return-object v0

    .line 49
    :cond_1
    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 50
    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 51
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v4

    if-ge v4, v1, :cond_0

    goto :goto_0
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/OptInPhoneNumberResultStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/OptInPhoneNumberResult;

    move-result-object v0

    return-object v0
.end method
