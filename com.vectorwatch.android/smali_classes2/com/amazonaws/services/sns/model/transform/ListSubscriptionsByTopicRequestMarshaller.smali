.class public Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicRequestMarshaller;
.super Ljava/lang/Object;
.source "ListSubscriptionsByTopicRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;",
        ">;",
        "Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;)Lcom/amazonaws/Request;
    .locals 6
    .param p1, "listSubscriptionsByTopicRequest"    # Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v4, Lcom/amazonaws/AmazonClientException;

    const-string v5, "Invalid argument passed to marshall(ListSubscriptionsByTopicRequest)"

    invoke-direct {v4, v5}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 38
    :cond_0
    new-instance v2, Lcom/amazonaws/DefaultRequest;

    const-string v4, "AmazonSNS"

    invoke-direct {v2, p1, v4}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 40
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;>;"
    const-string v4, "Action"

    const-string v5, "ListSubscriptionsByTopic"

    invoke-interface {v2, v4, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v4, "Version"

    const-string v5, "2010-03-31"

    invoke-interface {v2, v4, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 45
    const-string v1, "TopicArn"

    .line 46
    .local v1, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v3

    .line 47
    .local v3, "topicArn":Ljava/lang/String;
    invoke-static {v3}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v1, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .end local v1    # "prefix":Ljava/lang/String;
    .end local v3    # "topicArn":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;->getNextToken()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 50
    const-string v1, "NextToken"

    .line 51
    .restart local v1    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;->getNextToken()Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "nextToken":Ljava/lang/String;
    invoke-static {v0}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v1, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .end local v0    # "nextToken":Ljava/lang/String;
    .end local v1    # "prefix":Ljava/lang/String;
    :cond_2
    return-object v2
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
