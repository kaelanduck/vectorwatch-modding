.class public Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;
.super Ljava/lang/Object;
.source "GetSMSAttributesResultStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller$attributesMapEntryUnmarshaller;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;

    .line 110
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;
    .locals 7
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v1, Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;

    invoke-direct {v1}, Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;-><init>()V

    .line 78
    .local v1, "getSMSAttributesResult":Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v2

    .line 79
    .local v2, "originalDepth":I
    add-int/lit8 v3, v2, 0x1

    .line 81
    .local v3, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 82
    add-int/lit8 v3, v3, 0x2

    .line 85
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v4

    .line 86
    .local v4, "xmlEvent":I
    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 102
    :goto_1
    return-object v1

    .line 89
    :cond_1
    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 90
    const-string v5, "attributes/entry"

    invoke-virtual {p1, v5, v3}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 91
    invoke-static {}, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller$attributesMapEntryUnmarshaller;->getInstance()Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller$attributesMapEntryUnmarshaller;

    move-result-object v5

    .line 92
    invoke-virtual {v5, p1}, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller$attributesMapEntryUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 93
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;->addattributesEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;

    goto :goto_0

    .line 96
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 97
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v5

    if-ge v5, v2, :cond_0

    goto :goto_1
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/GetSMSAttributesResultStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/GetSMSAttributesResult;

    move-result-object v0

    return-object v0
.end method
