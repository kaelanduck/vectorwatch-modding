.class public Lcom/amazonaws/services/sns/model/transform/GetSubscriptionAttributesRequestMarshaller;
.super Ljava/lang/Object;
.source "GetSubscriptionAttributesRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;",
        ">;",
        "Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;)Lcom/amazonaws/Request;
    .locals 5
    .param p1, "getSubscriptionAttributesRequest"    # Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v3, Lcom/amazonaws/AmazonClientException;

    const-string v4, "Invalid argument passed to marshall(GetSubscriptionAttributesRequest)"

    invoke-direct {v3, v4}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 38
    :cond_0
    new-instance v1, Lcom/amazonaws/DefaultRequest;

    const-string v3, "AmazonSNS"

    invoke-direct {v1, p1, v3}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 40
    .local v1, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;>;"
    const-string v3, "Action"

    const-string v4, "GetSubscriptionAttributes"

    invoke-interface {v1, v3, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v3, "Version"

    const-string v4, "2010-03-31"

    invoke-interface {v1, v3, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 45
    const-string v0, "SubscriptionArn"

    .line 46
    .local v0, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "subscriptionArn":Ljava/lang/String;
    invoke-static {v2}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .end local v0    # "prefix":Ljava/lang/String;
    .end local v2    # "subscriptionArn":Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/GetSubscriptionAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/GetSubscriptionAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
