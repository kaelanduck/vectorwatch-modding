.class public Lcom/amazonaws/services/sns/model/transform/PublishRequestMarshaller;
.super Ljava/lang/Object;
.source "PublishRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sns/model/PublishRequest;",
        ">;",
        "Lcom/amazonaws/services/sns/model/PublishRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/PublishRequest;)Lcom/amazonaws/Request;
    .locals 18
    .param p1, "publishRequest"    # Lcom/amazonaws/services/sns/model/PublishRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/PublishRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sns/model/PublishRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    if-nez p1, :cond_0

    .line 33
    new-instance v14, Lcom/amazonaws/AmazonClientException;

    const-string v15, "Invalid argument passed to marshall(PublishRequest)"

    invoke-direct {v14, v15}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 36
    :cond_0
    new-instance v10, Lcom/amazonaws/DefaultRequest;

    const-string v14, "AmazonSNS"

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v14}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 38
    .local v10, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/PublishRequest;>;"
    const-string v14, "Action"

    const-string v15, "Publish"

    invoke-interface {v10, v14, v15}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v14, "Version"

    const-string v15, "2010-03-31"

    invoke-interface {v10, v14, v15}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_1

    .line 43
    const-string v9, "TopicArn"

    .line 44
    .local v9, "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v13

    .line 45
    .local v13, "topicArn":Ljava/lang/String;
    invoke-static {v13}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v9, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .end local v9    # "prefix":Ljava/lang/String;
    .end local v13    # "topicArn":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_2

    .line 48
    const-string v9, "TargetArn"

    .line 49
    .restart local v9    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getTargetArn()Ljava/lang/String;

    move-result-object v12

    .line 50
    .local v12, "targetArn":Ljava/lang/String;
    invoke-static {v12}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v9, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .end local v9    # "prefix":Ljava/lang/String;
    .end local v12    # "targetArn":Ljava/lang/String;
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 53
    const-string v9, "PhoneNumber"

    .line 54
    .restart local v9    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v8

    .line 55
    .local v8, "phoneNumber":Ljava/lang/String;
    invoke-static {v8}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v9, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .end local v8    # "phoneNumber":Ljava/lang/String;
    .end local v9    # "prefix":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_4

    .line 58
    const-string v9, "Message"

    .line 59
    .restart local v9    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "message":Ljava/lang/String;
    invoke-static {v1}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v9, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .end local v1    # "message":Ljava/lang/String;
    .end local v9    # "prefix":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_5

    .line 63
    const-string v9, "Subject"

    .line 64
    .restart local v9    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getSubject()Ljava/lang/String;

    move-result-object v11

    .line 65
    .local v11, "subject":Ljava/lang/String;
    invoke-static {v11}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v9, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .end local v9    # "prefix":Ljava/lang/String;
    .end local v11    # "subject":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_6

    .line 68
    const-string v9, "MessageStructure"

    .line 69
    .restart local v9    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageStructure()Ljava/lang/String;

    move-result-object v7

    .line 70
    .local v7, "messageStructure":Ljava/lang/String;
    invoke-static {v7}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v9, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .end local v7    # "messageStructure":Ljava/lang/String;
    .end local v9    # "prefix":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v14

    if-eqz v14, :cond_9

    .line 73
    const-string v9, "MessageAttributes"

    .line 75
    .restart local v9    # "prefix":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/amazonaws/services/sns/model/PublishRequest;->getMessageAttributes()Ljava/util/Map;

    move-result-object v2

    .line 76
    .local v2, "messageAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sns/model/MessageAttributeValue;>;"
    const/4 v4, 0x1

    .line 77
    .local v4, "messageAttributesIndex":I
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ".entry."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 79
    .local v5, "messageAttributesPrefix":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v14

    .line 78
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_9

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 80
    .local v3, "messageAttributesEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/amazonaws/services/sns/model/MessageAttributeValue;>;"
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 81
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    if-eqz v14, :cond_7

    .line 82
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ".Name"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 83
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-static {v14}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 82
    move-object/from16 v0, v16

    invoke-interface {v10, v0, v14}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_7
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v16, ".Value"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 86
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    if-eqz v14, :cond_8

    .line 88
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/amazonaws/services/sns/model/MessageAttributeValue;

    .line 89
    .local v6, "messageAttributesValue":Lcom/amazonaws/services/sns/model/MessageAttributeValue;
    invoke-static {}, Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;->getInstance()Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;

    move-result-object v14

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "."

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v14, v6, v10, v0}, Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;->marshall(Lcom/amazonaws/services/sns/model/MessageAttributeValue;Lcom/amazonaws/Request;Ljava/lang/String;)V

    .line 92
    .end local v6    # "messageAttributesValue":Lcom/amazonaws/services/sns/model/MessageAttributeValue;
    :cond_8
    add-int/lit8 v4, v4, 0x1

    .line 93
    goto/16 :goto_0

    .line 97
    .end local v2    # "messageAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/amazonaws/services/sns/model/MessageAttributeValue;>;"
    .end local v3    # "messageAttributesEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/amazonaws/services/sns/model/MessageAttributeValue;>;"
    .end local v4    # "messageAttributesIndex":I
    .end local v5    # "messageAttributesPrefix":Ljava/lang/String;
    .end local v9    # "prefix":Ljava/lang/String;
    :cond_9
    return-object v10
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sns/model/PublishRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/PublishRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/PublishRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
