.class public Lcom/amazonaws/services/sns/model/transform/DeleteTopicRequestMarshaller;
.super Ljava/lang/Object;
.source "DeleteTopicRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sns/model/DeleteTopicRequest;",
        ">;",
        "Lcom/amazonaws/services/sns/model/DeleteTopicRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/DeleteTopicRequest;)Lcom/amazonaws/Request;
    .locals 5
    .param p1, "deleteTopicRequest"    # Lcom/amazonaws/services/sns/model/DeleteTopicRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/DeleteTopicRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sns/model/DeleteTopicRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    if-nez p1, :cond_0

    .line 33
    new-instance v3, Lcom/amazonaws/AmazonClientException;

    const-string v4, "Invalid argument passed to marshall(DeleteTopicRequest)"

    invoke-direct {v3, v4}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 37
    :cond_0
    new-instance v1, Lcom/amazonaws/DefaultRequest;

    const-string v3, "AmazonSNS"

    invoke-direct {v1, p1, v3}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 39
    .local v1, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/DeleteTopicRequest;>;"
    const-string v3, "Action"

    const-string v4, "DeleteTopic"

    invoke-interface {v1, v3, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v3, "Version"

    const-string v4, "2010-03-31"

    invoke-interface {v1, v3, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/DeleteTopicRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 44
    const-string v0, "TopicArn"

    .line 45
    .local v0, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/DeleteTopicRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "topicArn":Ljava/lang/String;
    invoke-static {v2}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .end local v0    # "prefix":Ljava/lang/String;
    .end local v2    # "topicArn":Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sns/model/DeleteTopicRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/DeleteTopicRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/DeleteTopicRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
