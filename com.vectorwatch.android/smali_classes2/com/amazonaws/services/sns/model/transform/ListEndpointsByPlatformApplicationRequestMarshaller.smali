.class public Lcom/amazonaws/services/sns/model/transform/ListEndpointsByPlatformApplicationRequestMarshaller;
.super Ljava/lang/Object;
.source "ListEndpointsByPlatformApplicationRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;",
        ">;",
        "Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;)Lcom/amazonaws/Request;
    .locals 6
    .param p1, "listEndpointsByPlatformApplicationRequest"    # Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v4, Lcom/amazonaws/AmazonClientException;

    const-string v5, "Invalid argument passed to marshall(ListEndpointsByPlatformApplicationRequest)"

    invoke-direct {v4, v5}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 39
    :cond_0
    new-instance v3, Lcom/amazonaws/DefaultRequest;

    const-string v4, "AmazonSNS"

    invoke-direct {v3, p1, v4}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 41
    .local v3, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;>;"
    const-string v4, "Action"

    const-string v5, "ListEndpointsByPlatformApplication"

    invoke-interface {v3, v4, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v4, "Version"

    const-string v5, "2010-03-31"

    invoke-interface {v3, v4, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 46
    const-string v2, "PlatformApplicationArn"

    .line 48
    .local v2, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "platformApplicationArn":Ljava/lang/String;
    invoke-static {v1}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .end local v1    # "platformApplicationArn":Ljava/lang/String;
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;->getNextToken()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 52
    const-string v2, "NextToken"

    .line 53
    .restart local v2    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;->getNextToken()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "nextToken":Ljava/lang/String;
    invoke-static {v0}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .end local v0    # "nextToken":Ljava/lang/String;
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_2
    return-object v3
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/ListEndpointsByPlatformApplicationRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
