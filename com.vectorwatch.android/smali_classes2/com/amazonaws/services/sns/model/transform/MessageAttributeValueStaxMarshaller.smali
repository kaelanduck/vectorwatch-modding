.class Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;
.super Ljava/lang/Object;
.source "MessageAttributeValueStaxMarshaller.java"


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;

    .line 52
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/MessageAttributeValueStaxMarshaller;

    return-object v0
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/MessageAttributeValue;Lcom/amazonaws/Request;Ljava/lang/String;)V
    .locals 6
    .param p1, "_messageAttributeValue"    # Lcom/amazonaws/services/sns/model/MessageAttributeValue;
    .param p3, "_prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/MessageAttributeValue;",
            "Lcom/amazonaws/Request",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<*>;"
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 31
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "DataType"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 32
    .local v2, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getDataType()Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "dataType":Ljava/lang/String;
    invoke-static {v1}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .end local v1    # "dataType":Ljava/lang/String;
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 36
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "StringValue"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 37
    .restart local v2    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getStringValue()Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "stringValue":Ljava/lang/String;
    invoke-static {v3}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .end local v2    # "prefix":Ljava/lang/String;
    .end local v3    # "stringValue":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 41
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "BinaryValue"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 42
    .restart local v2    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/MessageAttributeValue;->getBinaryValue()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 43
    .local v0, "binaryValue":Ljava/nio/ByteBuffer;
    invoke-static {v0}, Lcom/amazonaws/util/StringUtils;->fromByteBuffer(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .end local v0    # "binaryValue":Ljava/nio/ByteBuffer;
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_2
    return-void
.end method
