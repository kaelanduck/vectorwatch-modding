.class public Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;
.super Ljava/lang/Object;
.source "CreatePlatformApplicationResultStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;

    .line 71
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;
    .locals 5
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;-><init>()V

    .line 39
    .local v0, "createPlatformApplicationResult":Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v1

    .line 40
    .local v1, "originalDepth":I
    add-int/lit8 v2, v1, 0x1

    .line 42
    .local v2, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 43
    add-int/lit8 v2, v2, 0x2

    .line 46
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v3

    .line 47
    .local v3, "xmlEvent":I
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 63
    :goto_1
    return-object v0

    .line 50
    :cond_1
    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 51
    const-string v4, "PlatformApplicationArn"

    invoke-virtual {p1, v4, v2}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 53
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v4

    .line 54
    invoke-virtual {v4, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v4

    .line 53
    invoke-virtual {v0, v4}, Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;->setPlatformApplicationArn(Ljava/lang/String;)V

    goto :goto_0

    .line 57
    :cond_2
    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 58
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v4

    if-ge v4, v1, :cond_0

    goto :goto_1
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/CreatePlatformApplicationResultStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/CreatePlatformApplicationResult;

    move-result-object v0

    return-object v0
.end method
