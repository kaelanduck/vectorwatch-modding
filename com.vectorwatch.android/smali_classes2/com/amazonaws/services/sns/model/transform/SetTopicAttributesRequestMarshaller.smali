.class public Lcom/amazonaws/services/sns/model/transform/SetTopicAttributesRequestMarshaller;
.super Ljava/lang/Object;
.source "SetTopicAttributesRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;",
        ">;",
        "Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;)Lcom/amazonaws/Request;
    .locals 7
    .param p1, "setTopicAttributesRequest"    # Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v5, Lcom/amazonaws/AmazonClientException;

    const-string v6, "Invalid argument passed to marshall(SetTopicAttributesRequest)"

    invoke-direct {v5, v6}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 38
    :cond_0
    new-instance v3, Lcom/amazonaws/DefaultRequest;

    const-string v5, "AmazonSNS"

    invoke-direct {v3, p1, v5}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 40
    .local v3, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;>;"
    const-string v5, "Action"

    const-string v6, "SetTopicAttributes"

    invoke-interface {v3, v5, v6}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v5, "Version"

    const-string v6, "2010-03-31"

    invoke-interface {v3, v5, v6}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 45
    const-string v2, "TopicArn"

    .line 46
    .local v2, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    .line 47
    .local v4, "topicArn":Ljava/lang/String;
    invoke-static {v4}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v2, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .end local v2    # "prefix":Ljava/lang/String;
    .end local v4    # "topicArn":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 50
    const-string v2, "AttributeName"

    .line 51
    .restart local v2    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "attributeName":Ljava/lang/String;
    invoke-static {v0}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v2, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .end local v0    # "attributeName":Ljava/lang/String;
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 55
    const-string v2, "AttributeValue"

    .line 56
    .restart local v2    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v1

    .line 57
    .local v1, "attributeValue":Ljava/lang/String;
    invoke-static {v1}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v2, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .end local v1    # "attributeValue":Ljava/lang/String;
    .end local v2    # "prefix":Ljava/lang/String;
    :cond_3
    return-object v3
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/SetTopicAttributesRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/SetTopicAttributesRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
