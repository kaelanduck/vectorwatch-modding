.class public Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;
.super Ljava/lang/Object;
.source "ListSubscriptionsResultStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;

    .line 74
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;
    .locals 8
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 36
    new-instance v0, Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;-><init>()V

    .line 38
    .local v0, "listSubscriptionsResult":Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v1

    .line 39
    .local v1, "originalDepth":I
    add-int/lit8 v2, v1, 0x1

    .line 41
    .local v2, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 42
    add-int/lit8 v2, v2, 0x2

    .line 45
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v3

    .line 46
    .local v3, "xmlEvent":I
    if-ne v3, v7, :cond_1

    .line 66
    :goto_1
    return-object v0

    .line 49
    :cond_1
    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 50
    const-string v4, "Subscriptions/member"

    invoke-virtual {p1, v4, v2}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 51
    new-array v4, v7, [Lcom/amazonaws/services/sns/model/Subscription;

    const/4 v5, 0x0

    .line 52
    invoke-static {}, Lcom/amazonaws/services/sns/model/transform/SubscriptionStaxUnmarshaller;->getInstance()Lcom/amazonaws/services/sns/model/transform/SubscriptionStaxUnmarshaller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/amazonaws/services/sns/model/transform/SubscriptionStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/Subscription;

    move-result-object v6

    aput-object v6, v4, v5

    .line 51
    invoke-virtual {v0, v4}, Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;->withSubscriptions([Lcom/amazonaws/services/sns/model/Subscription;)Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;

    goto :goto_0

    .line 55
    :cond_2
    const-string v4, "NextToken"

    invoke-virtual {p1, v4, v2}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 56
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v4

    .line 57
    invoke-virtual {v4, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v4

    .line 56
    invoke-virtual {v0, v4}, Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;->setNextToken(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_3
    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 61
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v4

    if-ge v4, v1, :cond_0

    goto :goto_1
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsResultStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/ListSubscriptionsResult;

    move-result-object v0

    return-object v0
.end method
