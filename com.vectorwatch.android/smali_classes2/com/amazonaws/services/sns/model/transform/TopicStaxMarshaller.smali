.class Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;
.super Ljava/lang/Object;
.source "TopicStaxMarshaller.java"


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;

    .line 41
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/TopicStaxMarshaller;

    return-object v0
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/Topic;Lcom/amazonaws/Request;Ljava/lang/String;)V
    .locals 4
    .param p1, "_topic"    # Lcom/amazonaws/services/sns/model/Topic;
    .param p3, "_prefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/Topic;",
            "Lcom/amazonaws/Request",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<*>;"
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/Topic;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 30
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "TopicArn"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/Topic;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "topicArn":Ljava/lang/String;
    invoke-static {v1}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v0, v2}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .end local v0    # "prefix":Ljava/lang/String;
    .end local v1    # "topicArn":Ljava/lang/String;
    :cond_0
    return-void
.end method
