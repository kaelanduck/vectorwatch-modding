.class public Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;
.super Ljava/lang/Object;
.source "ListSubscriptionsByTopicResultStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;

    .line 75
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    .locals 8
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 37
    new-instance v0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;-><init>()V

    .line 39
    .local v0, "listSubscriptionsByTopicResult":Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v1

    .line 40
    .local v1, "originalDepth":I
    add-int/lit8 v2, v1, 0x1

    .line 42
    .local v2, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 43
    add-int/lit8 v2, v2, 0x2

    .line 46
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v3

    .line 47
    .local v3, "xmlEvent":I
    if-ne v3, v7, :cond_1

    .line 67
    :goto_1
    return-object v0

    .line 50
    :cond_1
    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 51
    const-string v4, "Subscriptions/member"

    invoke-virtual {p1, v4, v2}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 52
    new-array v4, v7, [Lcom/amazonaws/services/sns/model/Subscription;

    const/4 v5, 0x0

    .line 53
    invoke-static {}, Lcom/amazonaws/services/sns/model/transform/SubscriptionStaxUnmarshaller;->getInstance()Lcom/amazonaws/services/sns/model/transform/SubscriptionStaxUnmarshaller;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/amazonaws/services/sns/model/transform/SubscriptionStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/Subscription;

    move-result-object v6

    aput-object v6, v4, v5

    .line 52
    invoke-virtual {v0, v4}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->withSubscriptions([Lcom/amazonaws/services/sns/model/Subscription;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;

    goto :goto_0

    .line 56
    :cond_2
    const-string v4, "NextToken"

    invoke-virtual {p1, v4, v2}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 58
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v4

    .line 57
    invoke-virtual {v0, v4}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->setNextToken(Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_3
    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v4

    if-ge v4, v1, :cond_0

    goto :goto_1
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/ListSubscriptionsByTopicResultStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;

    move-result-object v0

    return-object v0
.end method
