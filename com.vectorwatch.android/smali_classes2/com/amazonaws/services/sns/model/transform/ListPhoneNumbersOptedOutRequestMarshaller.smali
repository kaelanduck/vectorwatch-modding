.class public Lcom/amazonaws/services/sns/model/transform/ListPhoneNumbersOptedOutRequestMarshaller;
.super Ljava/lang/Object;
.source "ListPhoneNumbersOptedOutRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;",
        ">;",
        "Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;)Lcom/amazonaws/Request;
    .locals 5
    .param p1, "listPhoneNumbersOptedOutRequest"    # Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v3, Lcom/amazonaws/AmazonClientException;

    const-string v4, "Invalid argument passed to marshall(ListPhoneNumbersOptedOutRequest)"

    invoke-direct {v3, v4}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 38
    :cond_0
    new-instance v2, Lcom/amazonaws/DefaultRequest;

    const-string v3, "AmazonSNS"

    invoke-direct {v2, p1, v3}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 40
    .local v2, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;>;"
    const-string v3, "Action"

    const-string v4, "ListPhoneNumbersOptedOut"

    invoke-interface {v2, v3, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v3, "Version"

    const-string v4, "2010-03-31"

    invoke-interface {v2, v3, v4}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;->getNextToken()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 45
    const-string v1, "nextToken"

    .line 46
    .local v1, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;->getNextToken()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "nextToken":Ljava/lang/String;
    invoke-static {v0}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .end local v0    # "nextToken":Ljava/lang/String;
    .end local v1    # "prefix":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/ListPhoneNumbersOptedOutRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/ListPhoneNumbersOptedOutRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
