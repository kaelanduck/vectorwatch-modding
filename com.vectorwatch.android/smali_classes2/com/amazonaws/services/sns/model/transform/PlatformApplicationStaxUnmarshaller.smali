.class Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;
.super Ljava/lang/Object;
.source "PlatformApplicationStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller$AttributesMapEntryUnmarshaller;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sns/model/PlatformApplication;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;

    .line 115
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/PlatformApplication;
    .locals 7
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v2, Lcom/amazonaws/services/sns/model/PlatformApplication;

    invoke-direct {v2}, Lcom/amazonaws/services/sns/model/PlatformApplication;-><init>()V

    .line 78
    .local v2, "platformApplication":Lcom/amazonaws/services/sns/model/PlatformApplication;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v1

    .line 79
    .local v1, "originalDepth":I
    add-int/lit8 v3, v1, 0x1

    .line 81
    .local v3, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 82
    add-int/lit8 v3, v3, 0x2

    .line 85
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v4

    .line 86
    .local v4, "xmlEvent":I
    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 107
    :goto_1
    return-object v2

    .line 89
    :cond_1
    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 90
    const-string v5, "PlatformApplicationArn"

    invoke-virtual {p1, v5, v3}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 92
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v5

    .line 91
    invoke-virtual {v2, v5}, Lcom/amazonaws/services/sns/model/PlatformApplication;->setPlatformApplicationArn(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :cond_2
    const-string v5, "Attributes/entry"

    invoke-virtual {p1, v5, v3}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 96
    invoke-static {}, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller$AttributesMapEntryUnmarshaller;->getInstance()Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller$AttributesMapEntryUnmarshaller;

    move-result-object v5

    .line 97
    invoke-virtual {v5, p1}, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller$AttributesMapEntryUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 98
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lcom/amazonaws/services/sns/model/PlatformApplication;->addAttributesEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/PlatformApplication;

    goto :goto_0

    .line 101
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 102
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v5

    if-ge v5, v1, :cond_0

    goto :goto_1
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/PlatformApplicationStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/PlatformApplication;

    move-result-object v0

    return-object v0
.end method
