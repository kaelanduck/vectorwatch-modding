.class public Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;
.super Ljava/lang/Object;
.source "CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 69
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;

    .line 70
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;
    .locals 5
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;-><init>()V

    .line 39
    .local v0, "checkIfPhoneNumberIsOptedOutResult":Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v1

    .line 40
    .local v1, "originalDepth":I
    add-int/lit8 v2, v1, 0x1

    .line 42
    .local v2, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 43
    add-int/lit8 v2, v2, 0x2

    .line 46
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v3

    .line 47
    .local v3, "xmlEvent":I
    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 62
    :goto_1
    return-object v0

    .line 50
    :cond_1
    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 51
    const-string v4, "isOptedOut"

    invoke-virtual {p1, v4, v2}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 53
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$BooleanStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$BooleanStaxUnmarshaller;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$BooleanStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/Boolean;

    move-result-object v4

    .line 52
    invoke-virtual {v0, v4}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->setIsOptedOut(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 56
    :cond_2
    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 57
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v4

    if-ge v4, v1, :cond_0

    goto :goto_1
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/CheckIfPhoneNumberIsOptedOutResultStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;

    move-result-object v0

    return-object v0
.end method
