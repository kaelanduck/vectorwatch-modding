.class Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;
.super Ljava/lang/Object;
.source "EndpointStaxUnmarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Unmarshaller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller$AttributesMapEntryUnmarshaller;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Unmarshaller",
        "<",
        "Lcom/amazonaws/services/sns/model/Endpoint;",
        "Lcom/amazonaws/transform/StaxUnmarshallerContext;",
        ">;"
    }
.end annotation


# static fields
.field private static instance:Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;

    if-nez v0, :cond_0

    .line 113
    new-instance v0, Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;-><init>()V

    sput-object v0, Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;

    .line 114
    :cond_0
    sget-object v0, Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;->instance:Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;

    return-object v0
.end method


# virtual methods
.method public unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/Endpoint;
    .locals 7
    .param p1, "context"    # Lcom/amazonaws/transform/StaxUnmarshallerContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lcom/amazonaws/services/sns/model/Endpoint;

    invoke-direct {v0}, Lcom/amazonaws/services/sns/model/Endpoint;-><init>()V

    .line 77
    .local v0, "endpoint":Lcom/amazonaws/services/sns/model/Endpoint;
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v2

    .line 78
    .local v2, "originalDepth":I
    add-int/lit8 v3, v2, 0x1

    .line 80
    .local v3, "targetDepth":I
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->isStartOfDocument()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 81
    add-int/lit8 v3, v3, 0x2

    .line 84
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->nextEvent()I

    move-result v4

    .line 85
    .local v4, "xmlEvent":I
    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 106
    :goto_1
    return-object v0

    .line 88
    :cond_1
    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 89
    const-string v5, "EndpointArn"

    invoke-virtual {p1, v5, v3}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 90
    invoke-static {}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->getInstance()Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;

    move-result-object v5

    .line 91
    invoke-virtual {v5, p1}, Lcom/amazonaws/transform/SimpleTypeStaxUnmarshallers$StringStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/lang/String;

    move-result-object v5

    .line 90
    invoke-virtual {v0, v5}, Lcom/amazonaws/services/sns/model/Endpoint;->setEndpointArn(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    const-string v5, "Attributes/entry"

    invoke-virtual {p1, v5, v3}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->testExpression(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 95
    invoke-static {}, Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller$AttributesMapEntryUnmarshaller;->getInstance()Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller$AttributesMapEntryUnmarshaller;

    move-result-object v5

    .line 96
    invoke-virtual {v5, p1}, Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller$AttributesMapEntryUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Ljava/util/Map$Entry;

    move-result-object v1

    .line 97
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Lcom/amazonaws/services/sns/model/Endpoint;->addAttributesEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/Endpoint;

    goto :goto_0

    .line 100
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 101
    invoke-virtual {p1}, Lcom/amazonaws/transform/StaxUnmarshallerContext;->getCurrentDepth()I

    move-result v5

    if-ge v5, v2, :cond_0

    goto :goto_1
.end method

.method public bridge synthetic unmarshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p1, Lcom/amazonaws/transform/StaxUnmarshallerContext;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/EndpointStaxUnmarshaller;->unmarshall(Lcom/amazonaws/transform/StaxUnmarshallerContext;)Lcom/amazonaws/services/sns/model/Endpoint;

    move-result-object v0

    return-object v0
.end method
