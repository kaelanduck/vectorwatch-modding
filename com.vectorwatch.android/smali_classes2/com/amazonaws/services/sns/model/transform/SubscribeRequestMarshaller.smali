.class public Lcom/amazonaws/services/sns/model/transform/SubscribeRequestMarshaller;
.super Ljava/lang/Object;
.source "SubscribeRequestMarshaller.java"

# interfaces
.implements Lcom/amazonaws/transform/Marshaller;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/amazonaws/transform/Marshaller",
        "<",
        "Lcom/amazonaws/Request",
        "<",
        "Lcom/amazonaws/services/sns/model/SubscribeRequest;",
        ">;",
        "Lcom/amazonaws/services/sns/model/SubscribeRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public marshall(Lcom/amazonaws/services/sns/model/SubscribeRequest;)Lcom/amazonaws/Request;
    .locals 7
    .param p1, "subscribeRequest"    # Lcom/amazonaws/services/sns/model/SubscribeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amazonaws/services/sns/model/SubscribeRequest;",
            ")",
            "Lcom/amazonaws/Request",
            "<",
            "Lcom/amazonaws/services/sns/model/SubscribeRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    if-nez p1, :cond_0

    .line 33
    new-instance v5, Lcom/amazonaws/AmazonClientException;

    const-string v6, "Invalid argument passed to marshall(SubscribeRequest)"

    invoke-direct {v5, v6}, Lcom/amazonaws/AmazonClientException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 36
    :cond_0
    new-instance v3, Lcom/amazonaws/DefaultRequest;

    const-string v5, "AmazonSNS"

    invoke-direct {v3, p1, v5}, Lcom/amazonaws/DefaultRequest;-><init>(Lcom/amazonaws/AmazonWebServiceRequest;Ljava/lang/String;)V

    .line 38
    .local v3, "request":Lcom/amazonaws/Request;, "Lcom/amazonaws/Request<Lcom/amazonaws/services/sns/model/SubscribeRequest;>;"
    const-string v5, "Action"

    const-string v6, "Subscribe"

    invoke-interface {v3, v5, v6}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v5, "Version"

    const-string v6, "2010-03-31"

    invoke-interface {v3, v5, v6}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 43
    const-string v1, "TopicArn"

    .line 44
    .local v1, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    .line 45
    .local v4, "topicArn":Ljava/lang/String;
    invoke-static {v4}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v1, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .end local v1    # "prefix":Ljava/lang/String;
    .end local v4    # "topicArn":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 48
    const-string v1, "Protocol"

    .line 49
    .restart local v1    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "protocol":Ljava/lang/String;
    invoke-static {v2}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v1, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .end local v1    # "prefix":Ljava/lang/String;
    .end local v2    # "protocol":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 53
    const-string v1, "Endpoint"

    .line 54
    .restart local v1    # "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "endpoint":Ljava/lang/String;
    invoke-static {v0}, Lcom/amazonaws/util/StringUtils;->fromString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v1, v5}, Lcom/amazonaws/Request;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .end local v0    # "endpoint":Ljava/lang/String;
    .end local v1    # "prefix":Ljava/lang/String;
    :cond_3
    return-object v3
.end method

.method public bridge synthetic marshall(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/amazonaws/services/sns/model/SubscribeRequest;

    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/transform/SubscribeRequestMarshaller;->marshall(Lcom/amazonaws/services/sns/model/SubscribeRequest;)Lcom/amazonaws/Request;

    move-result-object v0

    return-object v0
.end method
