.class public Lcom/amazonaws/services/sns/model/SubscribeRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "SubscribeRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private endpoint:Ljava/lang/String;

.field private protocol:Ljava/lang/String;

.field private topicArn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 153
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "endpoint"    # Ljava/lang/String;

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 270
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->setTopicArn(Ljava/lang/String;)V

    .line 271
    invoke-virtual {p0, p2}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->setProtocol(Ljava/lang/String;)V

    .line 272
    invoke-virtual {p0, p3}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->setEndpoint(Ljava/lang/String;)V

    .line 273
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1026
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 1047
    :cond_0
    :goto_0
    return v3

    .line 1028
    :cond_1
    if-eqz p1, :cond_0

    .line 1031
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/SubscribeRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 1033
    check-cast v0, Lcom/amazonaws/services/sns/model/SubscribeRequest;

    .line 1035
    .local v0, "other":Lcom/amazonaws/services/sns/model/SubscribeRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1037
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1039
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_8

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1041
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1043
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_a

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 1045
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    move v3, v2

    .line 1047
    goto :goto_0

    :cond_5
    move v1, v3

    .line 1035
    goto :goto_1

    :cond_6
    move v4, v3

    goto :goto_2

    :cond_7
    move v1, v3

    .line 1039
    goto :goto_3

    :cond_8
    move v4, v3

    goto :goto_4

    :cond_9
    move v1, v3

    .line 1043
    goto :goto_5

    :cond_a
    move v4, v3

    goto :goto_6
.end method

.method public getEndpoint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 755
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SubscribeRequest;->endpoint:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SubscribeRequest;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getTopicArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SubscribeRequest;->topicArn:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1015
    const/16 v1, 0x1f

    .line 1016
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 1018
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 1019
    mul-int/lit8 v4, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 1020
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    :goto_2
    add-int v0, v2, v3

    .line 1021
    return v0

    .line 1018
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 1019
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 1020
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_2
.end method

.method public setEndpoint(Ljava/lang/String;)V
    .locals 0
    .param p1, "endpoint"    # Ljava/lang/String;

    .prologue
    .line 869
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SubscribeRequest;->endpoint:Ljava/lang/String;

    .line 870
    return-void
.end method

.method public setProtocol(Ljava/lang/String;)V
    .locals 0
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 529
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SubscribeRequest;->protocol:Ljava/lang/String;

    .line 530
    return-void
.end method

.method public setTopicArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;

    .prologue
    .line 298
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SubscribeRequest;->topicArn:Ljava/lang/String;

    .line 299
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1001
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1002
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1003
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1004
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TopicArn: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getTopicArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1005
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1006
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Protocol: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1007
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1008
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Endpoint: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SubscribeRequest;->getEndpoint()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1009
    :cond_2
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1010
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withEndpoint(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/SubscribeRequest;
    .locals 0
    .param p1, "endpoint"    # Ljava/lang/String;

    .prologue
    .line 988
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SubscribeRequest;->endpoint:Ljava/lang/String;

    .line 989
    return-object p0
.end method

.method public withProtocol(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/SubscribeRequest;
    .locals 0
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 640
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SubscribeRequest;->protocol:Ljava/lang/String;

    .line 641
    return-object p0
.end method

.method public withTopicArn(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/SubscribeRequest;
    .locals 0
    .param p1, "topicArn"    # Ljava/lang/String;

    .prologue
    .line 316
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SubscribeRequest;->topicArn:Ljava/lang/String;

    .line 317
    return-object p0
.end method
