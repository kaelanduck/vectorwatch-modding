.class public Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
.super Ljava/lang/Object;
.source "ListSubscriptionsByTopicResult.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private nextToken:Ljava/lang/String;

.field private subscriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sns/model/Subscription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->subscriptions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 206
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 225
    :cond_0
    :goto_0
    return v3

    .line 208
    :cond_1
    if-eqz p1, :cond_0

    .line 211
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 213
    check-cast v0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;

    .line 215
    .local v0, "other":Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_5

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 217
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 218
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getNextToken()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getNextToken()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_7

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 222
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getNextToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 223
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getNextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getNextToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    move v3, v2

    .line 225
    goto :goto_0

    :cond_4
    move v1, v3

    .line 215
    goto :goto_1

    :cond_5
    move v4, v3

    goto :goto_2

    :cond_6
    move v1, v3

    .line 220
    goto :goto_3

    :cond_7
    move v4, v3

    goto :goto_4
.end method

.method public getNextToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->nextToken:Ljava/lang/String;

    return-object v0
.end method

.method public getSubscriptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sns/model/Subscription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->subscriptions:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 195
    const/16 v1, 0x1f

    .line 196
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 199
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 200
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getNextToken()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    :goto_1
    add-int v0, v2, v3

    .line 201
    return v0

    .line 199
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_0

    .line 200
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getNextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public setNextToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "nextToken"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->nextToken:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public setSubscriptions(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sns/model/Subscription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "subscriptions":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sns/model/Subscription;>;"
    if-nez p1, :cond_0

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->subscriptions:Ljava/util/List;

    .line 71
    :goto_0
    return-void

    .line 70
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->subscriptions:Ljava/util/List;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Subscriptions: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getNextToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 188
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NextToken: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getNextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withNextToken(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    .locals 0
    .param p1, "nextToken"    # Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->nextToken:Ljava/lang/String;

    .line 171
    return-object p0
.end method

.method public withSubscriptions(Ljava/util/Collection;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sns/model/Subscription;",
            ">;)",
            "Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "subscriptions":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sns/model/Subscription;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->setSubscriptions(Ljava/util/Collection;)V

    .line 114
    return-object p0
.end method

.method public varargs withSubscriptions([Lcom/amazonaws/services/sns/model/Subscription;)Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;
    .locals 4
    .param p1, "subscriptions"    # [Lcom/amazonaws/services/sns/model/Subscription;

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->getSubscriptions()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 89
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->subscriptions:Ljava/util/List;

    .line 91
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 92
    .local v0, "value":Lcom/amazonaws/services/sns/model/Subscription;
    iget-object v3, p0, Lcom/amazonaws/services/sns/model/ListSubscriptionsByTopicResult;->subscriptions:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 94
    .end local v0    # "value":Lcom/amazonaws/services/sns/model/Subscription;
    :cond_1
    return-object p0
.end method
