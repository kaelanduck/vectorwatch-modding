.class public Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "SetPlatformApplicationAttributesRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private platformApplicationArn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->attributes:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addAttributesEntry(Ljava/lang/String;Ljava/lang/String;)Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 665
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->attributes:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 666
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->attributes:Ljava/util/Map;

    .line 668
    :cond_0
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 669
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicated keys ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") are provided."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 671
    :cond_1
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->attributes:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 672
    return-object p0
.end method

.method public clearAttributesEntries()Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;
    .locals 1

    .prologue
    .line 682
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->attributes:Ljava/util/Map;

    .line 683
    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 720
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 739
    :cond_0
    :goto_0
    return v3

    .line 722
    :cond_1
    if-eqz p1, :cond_0

    .line 725
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 727
    check-cast v0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;

    .line 729
    .local v0, "other":Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 731
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 732
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 734
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getAttributes()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_7

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 736
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 737
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getAttributes()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getAttributes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    move v3, v2

    .line 739
    goto :goto_0

    :cond_4
    move v1, v3

    .line 729
    goto :goto_1

    :cond_5
    move v4, v3

    goto :goto_2

    :cond_6
    move v1, v3

    .line 734
    goto :goto_3

    :cond_7
    move v4, v3

    goto :goto_4
.end method

.method public getAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->attributes:Ljava/util/Map;

    return-object v0
.end method

.method public getPlatformApplicationArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->platformApplicationArn:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 707
    const/16 v1, 0x1f

    .line 708
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 712
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    .line 713
    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 714
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getAttributes()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_1

    :goto_1
    add-int v0, v2, v3

    .line 715
    return v0

    .line 712
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v2

    .line 713
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 714
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getAttributes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public setAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 436
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->attributes:Ljava/util/Map;

    .line 437
    return-void
.end method

.method public setPlatformApplicationArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "platformApplicationArn"    # Ljava/lang/String;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->platformApplicationArn:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 695
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 696
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 698
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PlatformApplicationArn: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getPlatformApplicationArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getAttributes()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 700
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attributes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->getAttributes()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withAttributes(Ljava/util/Map;)Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;"
        }
    .end annotation

    .prologue
    .line 583
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->attributes:Ljava/util/Map;

    .line 584
    return-object p0
.end method

.method public withPlatformApplicationArn(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;
    .locals 0
    .param p1, "platformApplicationArn"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetPlatformApplicationAttributesRequest;->platformApplicationArn:Ljava/lang/String;

    .line 157
    return-object p0
.end method
