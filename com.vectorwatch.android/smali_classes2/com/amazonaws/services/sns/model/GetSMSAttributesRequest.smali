.class public Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "GetSMSAttributesRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private attributes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->attributes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 225
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 239
    :cond_0
    :goto_0
    return v3

    .line 227
    :cond_1
    if-eqz p1, :cond_0

    .line 230
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 232
    check-cast v0, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;

    .line 234
    .local v0, "other":Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_4

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 236
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 237
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    move v3, v2

    .line 239
    goto :goto_0

    :cond_3
    move v1, v3

    .line 234
    goto :goto_1

    :cond_4
    move v4, v3

    goto :goto_2
.end method

.method public getAttributes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->attributes:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 216
    const/16 v1, 0x1f

    .line 217
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 219
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 220
    return v0

    .line 219
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public setAttributes(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "attributes":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->attributes:Ljava/util/List;

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->attributes:Ljava/util/List;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "attributes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    :cond_0
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withAttributes(Ljava/util/Collection;)Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;"
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "attributes":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->setAttributes(Ljava/util/Collection;)V

    .line 194
    return-object p0
.end method

.method public varargs withAttributes([Ljava/lang/String;)Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;
    .locals 4
    .param p1, "attributes"    # [Ljava/lang/String;

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->getAttributes()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 151
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->attributes:Ljava/util/List;

    .line 153
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 154
    .local v0, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/amazonaws/services/sns/model/GetSMSAttributesRequest;->attributes:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 156
    .end local v0    # "value":Ljava/lang/String;
    :cond_1
    return-object p0
.end method
