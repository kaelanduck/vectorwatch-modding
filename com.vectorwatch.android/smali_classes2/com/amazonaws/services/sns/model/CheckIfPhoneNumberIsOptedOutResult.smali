.class public Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;
.super Ljava/lang/Object;
.source "CheckIfPhoneNumberIsOptedOutResult.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private isOptedOut:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 245
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 259
    :cond_0
    :goto_0
    return v3

    .line 247
    :cond_1
    if-eqz p1, :cond_0

    .line 250
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 252
    check-cast v0, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;

    .line 254
    .local v0, "other":Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->getIsOptedOut()Ljava/lang/Boolean;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->getIsOptedOut()Ljava/lang/Boolean;

    move-result-object v4

    if-nez v4, :cond_4

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 256
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->getIsOptedOut()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 257
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->getIsOptedOut()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->getIsOptedOut()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    move v3, v2

    .line 259
    goto :goto_0

    :cond_3
    move v1, v3

    .line 254
    goto :goto_1

    :cond_4
    move v4, v3

    goto :goto_2
.end method

.method public getIsOptedOut()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->isOptedOut:Ljava/lang/Boolean;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 236
    const/16 v1, 0x1f

    .line 237
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 239
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->getIsOptedOut()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 240
    return v0

    .line 239
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->getIsOptedOut()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    goto :goto_0
.end method

.method public isIsOptedOut()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->isOptedOut:Ljava/lang/Boolean;

    return-object v0
.end method

.method public setIsOptedOut(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "isOptedOut"    # Ljava/lang/Boolean;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->isOptedOut:Ljava/lang/Boolean;

    .line 168
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 227
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->getIsOptedOut()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 229
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isOptedOut: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->getIsOptedOut()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    :cond_0
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withIsOptedOut(Ljava/lang/Boolean;)Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;
    .locals 0
    .param p1, "isOptedOut"    # Ljava/lang/Boolean;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/CheckIfPhoneNumberIsOptedOutResult;->isOptedOut:Ljava/lang/Boolean;

    .line 214
    return-object p0
.end method
