.class public Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;
.super Ljava/lang/Object;
.source "ListEndpointsByPlatformApplicationResult.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private endpoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sns/model/Endpoint;",
            ">;"
        }
    .end annotation
.end field

.field private nextToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->endpoints:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 208
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 227
    :cond_0
    :goto_0
    return v3

    .line 210
    :cond_1
    if-eqz p1, :cond_0

    .line 213
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 215
    check-cast v0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;

    .line 217
    .local v0, "other":Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_5

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 219
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 220
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 222
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getNextToken()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getNextToken()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_7

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 224
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getNextToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 225
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getNextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getNextToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    move v3, v2

    .line 227
    goto :goto_0

    :cond_4
    move v1, v3

    .line 217
    goto :goto_1

    :cond_5
    move v4, v3

    goto :goto_2

    :cond_6
    move v1, v3

    .line 222
    goto :goto_3

    :cond_7
    move v4, v3

    goto :goto_4
.end method

.method public getEndpoints()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amazonaws/services/sns/model/Endpoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->endpoints:Ljava/util/List;

    return-object v0
.end method

.method public getNextToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->nextToken:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 198
    const/16 v1, 0x1f

    .line 199
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 201
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 202
    mul-int/lit8 v2, v0, 0x1f

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getNextToken()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    :goto_1
    add-int v0, v2, v3

    .line 203
    return v0

    .line 201
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_0

    .line 202
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getNextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method public setEndpoints(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sns/model/Endpoint;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "endpoints":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sns/model/Endpoint;>;"
    if-nez p1, :cond_0

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->endpoints:Ljava/util/List;

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->endpoints:Ljava/util/List;

    goto :goto_0
.end method

.method public setNextToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "nextToken"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->nextToken:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 189
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Endpoints: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getNextToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 191
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NextToken: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getNextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withEndpoints(Ljava/util/Collection;)Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/amazonaws/services/sns/model/Endpoint;",
            ">;)",
            "Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "endpoints":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/amazonaws/services/sns/model/Endpoint;>;"
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->setEndpoints(Ljava/util/Collection;)V

    .line 117
    return-object p0
.end method

.method public varargs withEndpoints([Lcom/amazonaws/services/sns/model/Endpoint;)Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;
    .locals 4
    .param p1, "endpoints"    # [Lcom/amazonaws/services/sns/model/Endpoint;

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->getEndpoints()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    .line 91
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->endpoints:Ljava/util/List;

    .line 93
    :cond_0
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 94
    .local v0, "value":Lcom/amazonaws/services/sns/model/Endpoint;
    iget-object v3, p0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->endpoints:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    .end local v0    # "value":Lcom/amazonaws/services/sns/model/Endpoint;
    :cond_1
    return-object p0
.end method

.method public withNextToken(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;
    .locals 0
    .param p1, "nextToken"    # Ljava/lang/String;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/ListEndpointsByPlatformApplicationResult;->nextToken:Ljava/lang/String;

    .line 174
    return-object p0
.end method
