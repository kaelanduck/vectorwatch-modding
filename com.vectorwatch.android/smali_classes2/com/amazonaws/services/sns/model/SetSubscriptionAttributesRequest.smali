.class public Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;
.super Lcom/amazonaws/AmazonWebServiceRequest;
.source "SetSubscriptionAttributesRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private attributeName:Ljava/lang/String;

.field private attributeValue:Ljava/lang/String;

.field private subscriptionArn:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "subscriptionArn"    # Ljava/lang/String;
    .param p2, "attributeName"    # Ljava/lang/String;
    .param p3, "attributeValue"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/amazonaws/AmazonWebServiceRequest;-><init>()V

    .line 85
    invoke-virtual {p0, p1}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->setSubscriptionArn(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0, p2}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->setAttributeName(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0, p3}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->setAttributeValue(Ljava/lang/String;)V

    .line 88
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 292
    if-ne p0, p1, :cond_1

    move v3, v2

    .line 316
    :cond_0
    :goto_0
    return v3

    .line 294
    :cond_1
    if-eqz p1, :cond_0

    .line 297
    instance-of v1, p1, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 299
    check-cast v0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;

    .line 301
    .local v0, "other":Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    move v4, v2

    :goto_2
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 303
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 304
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 306
    :cond_2
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    move v1, v2

    :goto_3
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_8

    move v4, v2

    :goto_4
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 308
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 309
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 311
    :cond_3
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_a

    move v4, v2

    :goto_6
    xor-int/2addr v1, v4

    if-nez v1, :cond_0

    .line 313
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 314
    invoke-virtual {v0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    move v3, v2

    .line 316
    goto :goto_0

    :cond_5
    move v1, v3

    .line 301
    goto :goto_1

    :cond_6
    move v4, v3

    goto :goto_2

    :cond_7
    move v1, v3

    .line 306
    goto :goto_3

    :cond_8
    move v4, v3

    goto :goto_4

    :cond_9
    move v1, v3

    .line 311
    goto :goto_5

    :cond_a
    move v4, v3

    goto :goto_6
.end method

.method public getAttributeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->attributeName:Ljava/lang/String;

    return-object v0
.end method

.method public getAttributeValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->attributeValue:Ljava/lang/String;

    return-object v0
.end method

.method public getSubscriptionArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->subscriptionArn:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 278
    const/16 v1, 0x1f

    .line 279
    .local v1, "prime":I
    const/4 v0, 0x1

    .line 282
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v0, v2, 0x1f

    .line 283
    mul-int/lit8 v4, v0, 0x1f

    .line 284
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v0, v4, v2

    .line 285
    mul-int/lit8 v2, v0, 0x1f

    .line 286
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    :goto_2
    add-int v0, v2, v3

    .line 287
    return v0

    .line 282
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 286
    :cond_2
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_2
.end method

.method public setAttributeName(Ljava/lang/String;)V
    .locals 0
    .param p1, "attributeName"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->attributeName:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setAttributeValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "attributeValue"    # Ljava/lang/String;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->attributeValue:Ljava/lang/String;

    .line 234
    return-void
.end method

.method public setSubscriptionArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "subscriptionArn"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->subscriptionArn:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 264
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 265
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 267
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SubscriptionArn: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getSubscriptionArn()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    :cond_0
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AttributeName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_1
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 271
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AttributeValue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->getAttributeValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    :cond_2
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public withAttributeName(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;
    .locals 0
    .param p1, "attributeName"    # Ljava/lang/String;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->attributeName:Ljava/lang/String;

    .line 207
    return-object p0
.end method

.method public withAttributeValue(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;
    .locals 0
    .param p1, "attributeValue"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->attributeValue:Ljava/lang/String;

    .line 252
    return-object p0
.end method

.method public withSubscriptionArn(Ljava/lang/String;)Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;
    .locals 0
    .param p1, "subscriptionArn"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/amazonaws/services/sns/model/SetSubscriptionAttributesRequest;->subscriptionArn:Ljava/lang/String;

    .line 132
    return-object p0
.end method
