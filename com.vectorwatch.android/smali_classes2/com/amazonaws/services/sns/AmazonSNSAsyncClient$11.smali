.class Lcom/amazonaws/services/sns/AmazonSNSAsyncClient$11;
.super Ljava/lang/Object;
.source "AmazonSNSAsyncClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/amazonaws/services/sns/AmazonSNSAsyncClient;->createTopicAsync(Lcom/amazonaws/services/sns/model/CreateTopicRequest;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/amazonaws/services/sns/model/CreateTopicResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/amazonaws/services/sns/AmazonSNSAsyncClient;

.field final synthetic val$createTopicRequest:Lcom/amazonaws/services/sns/model/CreateTopicRequest;


# direct methods
.method constructor <init>(Lcom/amazonaws/services/sns/AmazonSNSAsyncClient;Lcom/amazonaws/services/sns/model/CreateTopicRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/amazonaws/services/sns/AmazonSNSAsyncClient;

    .prologue
    .line 809
    iput-object p1, p0, Lcom/amazonaws/services/sns/AmazonSNSAsyncClient$11;->this$0:Lcom/amazonaws/services/sns/AmazonSNSAsyncClient;

    iput-object p2, p0, Lcom/amazonaws/services/sns/AmazonSNSAsyncClient$11;->val$createTopicRequest:Lcom/amazonaws/services/sns/model/CreateTopicRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/amazonaws/services/sns/model/CreateTopicResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 811
    iget-object v0, p0, Lcom/amazonaws/services/sns/AmazonSNSAsyncClient$11;->this$0:Lcom/amazonaws/services/sns/AmazonSNSAsyncClient;

    iget-object v1, p0, Lcom/amazonaws/services/sns/AmazonSNSAsyncClient$11;->val$createTopicRequest:Lcom/amazonaws/services/sns/model/CreateTopicRequest;

    invoke-virtual {v0, v1}, Lcom/amazonaws/services/sns/AmazonSNSAsyncClient;->createTopic(Lcom/amazonaws/services/sns/model/CreateTopicRequest;)Lcom/amazonaws/services/sns/model/CreateTopicResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 809
    invoke-virtual {p0}, Lcom/amazonaws/services/sns/AmazonSNSAsyncClient$11;->call()Lcom/amazonaws/services/sns/model/CreateTopicResult;

    move-result-object v0

    return-object v0
.end method
