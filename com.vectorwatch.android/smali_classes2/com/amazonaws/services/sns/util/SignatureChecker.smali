.class public Lcom/amazonaws/services/sns/util/SignatureChecker;
.super Ljava/lang/Object;
.source "SignatureChecker.java"


# instance fields
.field private final INTERESTING_FIELDS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final MESSAGE:Ljava/lang/String;

.field private final MESSAGE_ID:Ljava/lang/String;

.field private final NOTIFICATION_TYPE:Ljava/lang/String;

.field private final SIGNATURE:Ljava/lang/String;

.field private final SIGNATURE_VERSION:Ljava/lang/String;

.field private final SUBJECT:Ljava/lang/String;

.field private final SUBSCRIBE_TYPE:Ljava/lang/String;

.field private final SUBSCRIBE_URL:Ljava/lang/String;

.field private final TIMESTAMP:Ljava/lang/String;

.field private final TOKEN:Ljava/lang/String;

.field private final TOPIC:Ljava/lang/String;

.field private final TYPE:Ljava/lang/String;

.field private final UNSUBSCRIBE_TYPE:Ljava/lang/String;

.field private sigChecker:Ljava/security/Signature;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-string v0, "Notification"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->NOTIFICATION_TYPE:Ljava/lang/String;

    .line 45
    const-string v0, "SubscriptionConfirmation"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->SUBSCRIBE_TYPE:Ljava/lang/String;

    .line 46
    const-string v0, "UnsubscribeConfirmation"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->UNSUBSCRIBE_TYPE:Ljava/lang/String;

    .line 48
    const-string v0, "Type"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->TYPE:Ljava/lang/String;

    .line 49
    const-string v0, "SubscribeURL"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->SUBSCRIBE_URL:Ljava/lang/String;

    .line 50
    const-string v0, "Message"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->MESSAGE:Ljava/lang/String;

    .line 51
    const-string v0, "Timestamp"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->TIMESTAMP:Ljava/lang/String;

    .line 52
    const-string v0, "SignatureVersion"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->SIGNATURE_VERSION:Ljava/lang/String;

    .line 53
    const-string v0, "Signature"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->SIGNATURE:Ljava/lang/String;

    .line 54
    const-string v0, "MessageId"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->MESSAGE_ID:Ljava/lang/String;

    .line 55
    const-string v0, "Subject"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->SUBJECT:Ljava/lang/String;

    .line 56
    const-string v0, "TopicArn"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->TOPIC:Ljava/lang/String;

    .line 57
    const-string v0, "Token"

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->TOKEN:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "SubscribeURL"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "Message"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "Timestamp"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "Signature"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "SignatureVersion"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "MessageId"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "Subject"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "TopicArn"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "Token"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->INTERESTING_FIELDS:Ljava/util/Set;

    return-void
.end method

.method private parseJSON(Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .param p1, "jsonmessage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 163
    .local v3, "parsed":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v6, Ljava/io/StringReader;

    invoke-direct {v6, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/amazonaws/util/json/JsonUtils;->getJsonReader(Ljava/io/Reader;)Lcom/amazonaws/util/json/AwsJsonReader;

    move-result-object v4

    .line 165
    .local v4, "reader":Lcom/amazonaws/util/json/AwsJsonReader;
    :try_start_0
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->beginObject()V

    .line 166
    :goto_0
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 167
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 169
    .local v2, "name":Ljava/lang/String;
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->isContainer()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 170
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->beginArray()V

    .line 171
    const-string v5, ""

    .line 172
    .local v5, "value":Ljava/lang/String;
    const/4 v1, 0x1

    .line 173
    .local v1, "first":Z
    :goto_1
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 174
    if-nez v1, :cond_0

    .line 175
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 177
    :cond_0
    const/4 v1, 0x0

    .line 178
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->nextString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 180
    :cond_1
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->endArray()V

    .line 184
    .end local v1    # "first":Z
    :goto_2
    invoke-interface {v3, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 188
    .end local v2    # "name":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 192
    .end local v0    # "e":Ljava/io/IOException;
    :goto_3
    return-object v3

    .line 182
    .restart local v2    # "name":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->nextString()Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "value":Ljava/lang/String;
    goto :goto_2

    .line 186
    .end local v2    # "name":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :cond_3
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->endObject()V

    .line 187
    invoke-interface {v4}, Lcom/amazonaws/util/json/AwsJsonReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method private publishMessageValues(Ljava/util/Map;)Ljava/util/TreeMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "parsedMessage":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 196
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 197
    .local v2, "signables":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x6

    new-array v1, v4, [Ljava/lang/String;

    const-string v4, "Message"

    aput-object v4, v1, v3

    const/4 v4, 0x1

    const-string v5, "MessageId"

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string v5, "Subject"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string v5, "Type"

    aput-object v5, v1, v4

    const/4 v4, 0x4

    const-string v5, "Timestamp"

    aput-object v5, v1, v4

    const/4 v4, 0x5

    const-string v5, "TopicArn"

    aput-object v5, v1, v4

    .line 198
    .local v1, "keys":[Ljava/lang/String;
    array-length v4, v1

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 199
    .local v0, "key":Ljava/lang/String;
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 200
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 203
    .end local v0    # "key":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method private subscribeMessageValues(Ljava/util/Map;)Ljava/util/TreeMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "parsedMessage":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 207
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 208
    .local v2, "signables":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x7

    new-array v1, v4, [Ljava/lang/String;

    const-string v4, "SubscribeURL"

    aput-object v4, v1, v3

    const/4 v4, 0x1

    const-string v5, "Message"

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string v5, "MessageId"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string v5, "Type"

    aput-object v5, v1, v4

    const/4 v4, 0x4

    const-string v5, "Timestamp"

    aput-object v5, v1, v4

    const/4 v4, 0x5

    const-string v5, "Token"

    aput-object v5, v1, v4

    const/4 v4, 0x6

    const-string v5, "TopicArn"

    aput-object v5, v1, v4

    .line 209
    .local v1, "keys":[Ljava/lang/String;
    array-length v4, v1

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v1, v3

    .line 210
    .local v0, "key":Ljava/lang/String;
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 211
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 214
    .end local v0    # "key":Ljava/lang/String;
    :cond_1
    return-object v2
.end method


# virtual methods
.method protected stringToSign(Ljava/util/SortedMap;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "signables":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-interface {p1}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 154
    .local v0, "k":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    invoke-interface {p1, v0}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 157
    .end local v0    # "k":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "result":Ljava/lang/String;
    return-object v1
.end method

.method public verifyMessageSignature(Ljava/lang/String;Ljava/security/PublicKey;)Z
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "publicKey"    # Ljava/security/PublicKey;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/amazonaws/services/sns/util/SignatureChecker;->parseJSON(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 80
    .local v0, "parsed":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, v0, p2}, Lcom/amazonaws/services/sns/util/SignatureChecker;->verifySignature(Ljava/util/Map;Ljava/security/PublicKey;)Z

    move-result v1

    return v1
.end method

.method public verifySignature(Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)Z
    .locals 4
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "signature"    # Ljava/lang/String;
    .param p3, "publicKey"    # Ljava/security/PublicKey;

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 133
    .local v0, "result":Z
    const/4 v1, 0x0

    .line 135
    .local v1, "sigbytes":[B
    :try_start_0
    sget-object v2, Lcom/amazonaws/util/StringUtils;->UTF8:Ljava/nio/charset/Charset;

    invoke-virtual {p2, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/amazonaws/util/Base64;->decode([B)[B

    move-result-object v1

    .line 136
    const-string v2, "SHA1withRSA"

    invoke-static {v2}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v2

    iput-object v2, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->sigChecker:Ljava/security/Signature;

    .line 137
    iget-object v2, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->sigChecker:Ljava/security/Signature;

    invoke-virtual {v2, p3}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 138
    iget-object v2, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->sigChecker:Ljava/security/Signature;

    sget-object v3, Lcom/amazonaws/util/StringUtils;->UTF8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/security/Signature;->update([B)V

    .line 139
    iget-object v2, p0, Lcom/amazonaws/services/sns/util/SignatureChecker;->sigChecker:Ljava/security/Signature;

    invoke-virtual {v2, v1}, Ljava/security/Signature;->verify([B)Z
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 147
    :goto_0
    return v0

    .line 144
    :catch_0
    move-exception v2

    goto :goto_0

    .line 142
    :catch_1
    move-exception v2

    goto :goto_0

    .line 140
    :catch_2
    move-exception v2

    goto :goto_0
.end method

.method public verifySignature(Ljava/util/Map;Ljava/security/PublicKey;)Z
    .locals 8
    .param p2, "publicKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/security/PublicKey;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "parsedMessage":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 97
    .local v3, "valid":Z
    const-string v5, "SignatureVersion"

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 98
    .local v4, "version":Ljava/lang/String;
    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 100
    const-string v5, "Type"

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 101
    .local v2, "type":Ljava/lang/String;
    const-string v5, "Signature"

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 102
    .local v0, "signature":Ljava/lang/String;
    const-string v1, ""

    .line 103
    .local v1, "signed":Ljava/lang/String;
    const-string v5, "Notification"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 104
    invoke-direct {p0, p1}, Lcom/amazonaws/services/sns/util/SignatureChecker;->publishMessageValues(Ljava/util/Map;)Ljava/util/TreeMap;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/amazonaws/services/sns/util/SignatureChecker;->stringToSign(Ljava/util/SortedMap;)Ljava/lang/String;

    move-result-object v1

    .line 112
    :goto_0
    invoke-virtual {p0, v1, v0, p2}, Lcom/amazonaws/services/sns/util/SignatureChecker;->verifySignature(Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)Z

    move-result v3

    .line 114
    .end local v0    # "signature":Ljava/lang/String;
    .end local v1    # "signed":Ljava/lang/String;
    .end local v2    # "type":Ljava/lang/String;
    :cond_0
    return v3

    .line 105
    .restart local v0    # "signature":Ljava/lang/String;
    .restart local v1    # "signed":Ljava/lang/String;
    .restart local v2    # "type":Ljava/lang/String;
    :cond_1
    const-string v5, "SubscriptionConfirmation"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 106
    invoke-direct {p0, p1}, Lcom/amazonaws/services/sns/util/SignatureChecker;->subscribeMessageValues(Ljava/util/Map;)Ljava/util/TreeMap;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/amazonaws/services/sns/util/SignatureChecker;->stringToSign(Ljava/util/SortedMap;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 107
    :cond_2
    const-string v5, "UnsubscribeConfirmation"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 108
    invoke-direct {p0, p1}, Lcom/amazonaws/services/sns/util/SignatureChecker;->subscribeMessageValues(Ljava/util/Map;)Ljava/util/TreeMap;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/amazonaws/services/sns/util/SignatureChecker;->stringToSign(Ljava/util/SortedMap;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 110
    :cond_3
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot process message of type "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
.end method
