.class public final Lcom/software/shell/fab/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/software/shell/fab/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final fab_material_amber_500:I = 0x7f0f0048

.field public static final fab_material_amber_900:I = 0x7f0f0049

.field public static final fab_material_black:I = 0x7f0f004a

.field public static final fab_material_blue_500:I = 0x7f0f004b

.field public static final fab_material_blue_900:I = 0x7f0f004c

.field public static final fab_material_blue_grey_500:I = 0x7f0f004d

.field public static final fab_material_blue_grey_900:I = 0x7f0f004e

.field public static final fab_material_brown_500:I = 0x7f0f004f

.field public static final fab_material_brown_900:I = 0x7f0f0050

.field public static final fab_material_cyan_500:I = 0x7f0f0051

.field public static final fab_material_cyan_900:I = 0x7f0f0052

.field public static final fab_material_deep_orange_500:I = 0x7f0f0053

.field public static final fab_material_deep_orange_900:I = 0x7f0f0054

.field public static final fab_material_deep_purple_500:I = 0x7f0f0055

.field public static final fab_material_deep_purple_900:I = 0x7f0f0056

.field public static final fab_material_green_500:I = 0x7f0f0057

.field public static final fab_material_green_900:I = 0x7f0f0058

.field public static final fab_material_grey_500:I = 0x7f0f0059

.field public static final fab_material_grey_900:I = 0x7f0f005a

.field public static final fab_material_indigo_500:I = 0x7f0f005b

.field public static final fab_material_indigo_900:I = 0x7f0f005c

.field public static final fab_material_light_blue_500:I = 0x7f0f005d

.field public static final fab_material_light_blue_900:I = 0x7f0f005e

.field public static final fab_material_light_green_500:I = 0x7f0f005f

.field public static final fab_material_light_green_900:I = 0x7f0f0060

.field public static final fab_material_lime_500:I = 0x7f0f0061

.field public static final fab_material_lime_900:I = 0x7f0f0062

.field public static final fab_material_orange_500:I = 0x7f0f0063

.field public static final fab_material_orange_900:I = 0x7f0f0064

.field public static final fab_material_pink_500:I = 0x7f0f0065

.field public static final fab_material_pink_900:I = 0x7f0f0066

.field public static final fab_material_purple_500:I = 0x7f0f0067

.field public static final fab_material_purple_900:I = 0x7f0f0068

.field public static final fab_material_red_500:I = 0x7f0f0069

.field public static final fab_material_red_900:I = 0x7f0f006a

.field public static final fab_material_teal_500:I = 0x7f0f006b

.field public static final fab_material_teal_900:I = 0x7f0f006c

.field public static final fab_material_white:I = 0x7f0f006d

.field public static final fab_material_yellow_500:I = 0x7f0f006e

.field public static final fab_material_yellow_900:I = 0x7f0f006f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
