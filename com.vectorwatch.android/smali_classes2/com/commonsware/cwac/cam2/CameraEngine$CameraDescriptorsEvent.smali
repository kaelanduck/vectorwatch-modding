.class public Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;
.super Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;
.source "CameraEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CameraDescriptorsEvent"
.end annotation


# instance fields
.field public final descriptors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/commonsware/cwac/cam2/CameraDescriptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;-><init>(Ljava/lang/Exception;)V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;->descriptors:Ljava/util/List;

    .line 95
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/commonsware/cwac/cam2/CameraDescriptor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "descriptors":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/CameraDescriptor;>;"
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraEngine$CrashableEvent;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;->descriptors:Ljava/util/List;

    .line 90
    return-void
.end method
