.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$AreaComparator;
.super Ljava/lang/Object;
.source "CameraTwoEngine.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AreaComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/commonsware/cwac/cam2/util/Size;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/commonsware/cwac/cam2/util/Size;Lcom/commonsware/cwac/cam2/util/Size;)I
    .locals 8
    .param p1, "lhs"    # Lcom/commonsware/cwac/cam2/util/Size;
    .param p2, "rhs"    # Lcom/commonsware/cwac/cam2/util/Size;

    .prologue
    .line 687
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v6

    int-to-long v6, v6

    mul-long v0, v4, v6

    .line 688
    .local v0, "lhArea":J
    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v6

    int-to-long v6, v6

    mul-long v2, v4, v6

    .line 690
    .local v2, "rhArea":J
    sub-long v4, v0, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->signum(J)I

    move-result v4

    return v4
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 684
    check-cast p1, Lcom/commonsware/cwac/cam2/util/Size;

    check-cast p2, Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {p0, p1, p2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$AreaComparator;->compare(Lcom/commonsware/cwac/cam2/util/Size;Lcom/commonsware/cwac/cam2/util/Size;)I

    move-result v0

    return v0
.end method
