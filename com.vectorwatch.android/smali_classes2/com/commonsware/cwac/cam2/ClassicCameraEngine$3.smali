.class Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;
.super Ljava/lang/Object;
.source "ClassicCameraEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->open(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

.field final synthetic val$session:Lcom/commonsware/cwac/cam2/CameraSession;

.field final synthetic val$texture:Landroid/graphics/SurfaceTexture;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iput-object p2, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    iput-object p3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->val$texture:Landroid/graphics/SurfaceTexture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 209
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v7}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 210
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCamera()Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$600(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;)Landroid/hardware/Camera;

    move-result-object v0

    .line 212
    .local v0, "camera":Landroid/hardware/Camera;
    if-nez v0, :cond_0

    .line 213
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCameraId()I

    move-result v7

    invoke-static {v7}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    .line 214
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->setCamera(Landroid/hardware/Camera;)V
    invoke-static {v1, v0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$700(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Landroid/hardware/Camera;)V

    .line 217
    :cond_0
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    .line 218
    .local v4, "params":Landroid/hardware/Camera$Parameters;
    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v6

    .line 220
    .local v6, "rawFlashModes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iget-object v7, v7, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 222
    if-eqz v6, :cond_6

    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iget-object v7, v7, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->preferredFlashModes:Ljava/util/List;

    if-eqz v7, :cond_6

    .line 223
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iget-object v7, v7, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->preferredFlashModes:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/commonsware/cwac/cam2/FlashMode;

    .line 224
    .local v3, "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 226
    .local v5, "rawFlashMode":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/FlashMode;->getClassicMode()Ljava/lang/String;

    move-result-object v9

    .line 225
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 227
    iget-object v8, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iget-object v8, v8, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 233
    .end local v3    # "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    .end local v5    # "rawFlashMode":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iget-object v7, v7, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 234
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 236
    .restart local v5    # "rawFlashMode":Ljava/lang/String;
    invoke-static {v5}, Lcom/commonsware/cwac/cam2/FlashMode;->lookupClassicMode(Ljava/lang/String;)Lcom/commonsware/cwac/cam2/FlashMode;

    move-result-object v3

    .line 238
    .restart local v3    # "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    if-eqz v3, :cond_4

    .line 239
    iget-object v8, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iget-object v8, v8, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 244
    .end local v3    # "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    .end local v5    # "rawFlashMode":Ljava/lang/String;
    :cond_5
    iget-object v8, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iget-object v7, v7, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v8, v7}, Lcom/commonsware/cwac/cam2/CameraSession;->setCurrentFlashMode(Lcom/commonsware/cwac/cam2/FlashMode;)V

    .line 248
    :cond_6
    :try_start_0
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    check-cast v7, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;->configureStillCamera(Z)Landroid/hardware/Camera$Parameters;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 250
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->val$texture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0, v7}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    .line 251
    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 252
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v7}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v7

    new-instance v8, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;

    invoke-direct {v8}, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;-><init>()V

    invoke-virtual {v7, v8}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :cond_7
    :goto_2
    return-void

    .line 253
    :catch_0
    move-exception v2

    .line 254
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 255
    const/4 v7, 0x0

    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->setCamera(Landroid/hardware/Camera;)V
    invoke-static {v1, v7}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$700(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Landroid/hardware/Camera;)V

    .line 256
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v7}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v7

    new-instance v8, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;

    invoke-direct {v8, v2}, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v7, v8}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 258
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v7}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->isDebug()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 259
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Exception opening camera"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method
