.class public Lcom/commonsware/cwac/cam2/SimpleClassicCameraConfigurator;
.super Ljava/lang/Object;
.source "SimpleClassicCameraConfigurator.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public configureRecorder(Lcom/commonsware/cwac/cam2/CameraSession;ILcom/commonsware/cwac/cam2/VideoTransaction;Landroid/media/MediaRecorder;)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cameraId"    # I
    .param p3, "xact"    # Lcom/commonsware/cwac/cam2/VideoTransaction;
    .param p4, "recorder"    # Landroid/media/MediaRecorder;

    .prologue
    .line 26
    return-void
.end method

.method public configureStillCamera(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/Camera$CameraInfo;Landroid/hardware/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 1
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "info"    # Landroid/hardware/Camera$CameraInfo;
    .param p3, "camera"    # Landroid/hardware/Camera;
    .param p4, "params"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 17
    const/4 v0, 0x0

    return-object v0
.end method
