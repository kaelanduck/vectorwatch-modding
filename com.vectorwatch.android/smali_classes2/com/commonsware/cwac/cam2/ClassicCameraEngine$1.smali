.class Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;
.super Ljava/lang/Object;
.source "ClassicCameraEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->loadCameraDescriptors(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

.field final synthetic val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iput-object p2, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/16 v14, 0x870

    .line 72
    iget-object v10, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->descriptors:Ljava/util/List;
    invoke-static {v10}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$100(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)Ljava/util/List;

    move-result-object v10

    if-nez v10, :cond_6

    .line 73
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    .line 74
    .local v2, "count":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 75
    .local v7, "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;>;"
    new-instance v4, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v4}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 77
    .local v4, "info":Landroid/hardware/Camera$CameraInfo;
    const/4 v1, 0x0

    .local v1, "cameraId":I
    :goto_0
    if-ge v1, v2, :cond_5

    .line 78
    invoke-static {v1, v4}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 79
    new-instance v3, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    const/4 v10, 0x0

    invoke-direct {v3, v1, v4, v10}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;-><init>(ILandroid/hardware/Camera$CameraInfo;Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;)V

    .line 81
    .local v3, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCameraId()I

    move-result v10

    invoke-static {v10}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    .line 82
    .local v0, "camera":Landroid/hardware/Camera;
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    .line 83
    .local v5, "params":Landroid/hardware/Camera$Parameters;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 85
    .local v9, "sizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/commonsware/cwac/cam2/util/Size;>;"
    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/hardware/Camera$Size;

    .line 86
    .local v8, "size":Landroid/hardware/Camera$Size;
    iget v11, v8, Landroid/hardware/Camera$Size;->height:I

    if-ge v11, v14, :cond_0

    iget v11, v8, Landroid/hardware/Camera$Size;->width:I

    if-ge v11, v14, :cond_0

    .line 87
    new-instance v11, Lcom/commonsware/cwac/cam2/util/Size;

    iget v12, v8, Landroid/hardware/Camera$Size;->width:I

    iget v13, v8, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v11, v12, v13}, Lcom/commonsware/cwac/cam2/util/Size;-><init>(II)V

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 91
    .end local v8    # "size":Landroid/hardware/Camera$Size;
    :cond_1
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->setPreviewSizes(Ljava/util/ArrayList;)V
    invoke-static {v3, v9}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$300(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Ljava/util/ArrayList;)V

    .line 93
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "sizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/commonsware/cwac/cam2/util/Size;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .restart local v9    # "sizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/commonsware/cwac/cam2/util/Size;>;"
    invoke-virtual {v5}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/hardware/Camera$Size;

    .line 96
    .restart local v8    # "size":Landroid/hardware/Camera$Size;
    const-string v11, "samsung"

    sget-object v12, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const-string v11, "jflteuc"

    sget-object v12, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 97
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    iget v11, v8, Landroid/hardware/Camera$Size;->width:I

    const/16 v12, 0x800

    if-ge v11, v12, :cond_2

    .line 99
    :cond_3
    new-instance v11, Lcom/commonsware/cwac/cam2/util/Size;

    iget v12, v8, Landroid/hardware/Camera$Size;->width:I

    iget v13, v8, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v11, v12, v13}, Lcom/commonsware/cwac/cam2/util/Size;-><init>(II)V

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 103
    .end local v8    # "size":Landroid/hardware/Camera$Size;
    :cond_4
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->setPictureSizes(Ljava/util/ArrayList;)V
    invoke-static {v3, v9}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$400(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Ljava/util/ArrayList;)V

    .line 104
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 105
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 108
    .end local v0    # "camera":Landroid/hardware/Camera;
    .end local v3    # "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    .end local v5    # "params":Landroid/hardware/Camera$Parameters;
    .end local v9    # "sizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/commonsware/cwac/cam2/util/Size;>;"
    :cond_5
    iget-object v10, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    # setter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->descriptors:Ljava/util/List;
    invoke-static {v10, v7}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$102(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Ljava/util/List;)Ljava/util/List;

    .line 111
    .end local v1    # "cameraId":I
    .end local v2    # "count":I
    .end local v4    # "info":Landroid/hardware/Camera$CameraInfo;
    .end local v7    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;>;"
    :cond_6
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v6, "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/CameraDescriptor;>;"
    iget-object v10, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->descriptors:Ljava/util/List;
    invoke-static {v10}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$100(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_7
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 114
    .restart local v3    # "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    iget-object v11, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    invoke-virtual {v11}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->getFacingExactMatch()Z

    move-result v11

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .line 115
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    invoke-static {v3, v11}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$500(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I

    move-result v11

    if-lez v11, :cond_7

    .line 116
    :cond_8
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 120
    .end local v3    # "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    :cond_9
    new-instance v10, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1$1;

    invoke-direct {v10, p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1$1;-><init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;)V

    invoke-static {v6, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 137
    iget-object v10, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v10}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v10

    new-instance v11, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;

    invoke-direct {v11, v6}, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;-><init>(Ljava/util/List;)V

    invoke-virtual {v10, v11}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 138
    return-void
.end method
