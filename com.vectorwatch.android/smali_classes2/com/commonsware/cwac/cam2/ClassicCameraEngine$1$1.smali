.class Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1$1;
.super Ljava/lang/Object;
.source "ClassicCameraEngine.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/commonsware/cwac/cam2/CameraDescriptor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1$1;->this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/commonsware/cwac/cam2/CameraDescriptor;Lcom/commonsware/cwac/cam2/CameraDescriptor;)I
    .locals 5
    .param p1, "descriptor"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;
    .param p2, "t1"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    .line 123
    move-object v1, p1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .local v1, "lhs":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    move-object v3, p2

    .line 124
    check-cast v3, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 128
    .local v3, "rhs":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1$1;->this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;

    iget-object v4, v4, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    invoke-static {v3, v4}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$500(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I

    move-result v0

    .line 129
    .local v0, "lhScore":I
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1$1;->this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;

    iget-object v4, v4, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    invoke-static {v1, v4}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$500(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I

    move-result v2

    .line 133
    .local v2, "rhScore":I
    if-ge v0, v2, :cond_0

    const/4 v4, -0x1

    :goto_0
    return v4

    :cond_0
    if-ne v0, v2, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 120
    check-cast p1, Lcom/commonsware/cwac/cam2/CameraDescriptor;

    check-cast p2, Lcom/commonsware/cwac/cam2/CameraDescriptor;

    invoke-virtual {p0, p1, p2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1$1;->compare(Lcom/commonsware/cwac/cam2/CameraDescriptor;Lcom/commonsware/cwac/cam2/CameraDescriptor;)I

    move-result v0

    return v0
.end method
