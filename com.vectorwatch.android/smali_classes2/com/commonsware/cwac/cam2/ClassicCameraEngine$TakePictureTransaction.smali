.class Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;
.super Ljava/lang/Object;
.source "ClassicCameraEngine.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/ClassicCameraEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TakePictureTransaction"
.end annotation


# instance fields
.field private final ctxt:Landroid/content/Context;

.field final synthetic this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

.field private final xact:Lcom/commonsware/cwac/cam2/PictureTransaction;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Landroid/content/Context;Lcom/commonsware/cwac/cam2/PictureTransaction;)V
    .locals 1
    .param p2, "ctxt"    # Landroid/content/Context;
    .param p3, "xact"    # Lcom/commonsware/cwac/cam2/PictureTransaction;

    .prologue
    .line 452
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 453
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->ctxt:Landroid/content/Context;

    .line 454
    iput-object p3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->xact:Lcom/commonsware/cwac/cam2/PictureTransaction;

    .line 455
    return-void
.end method

.method static synthetic access$1100(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;)Lcom/commonsware/cwac/cam2/PictureTransaction;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->xact:Lcom/commonsware/cwac/cam2/PictureTransaction;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->ctxt:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 2
    .param p1, "bytes"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 459
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getThreadPool()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;-><init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;Landroid/hardware/Camera;[B)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 467
    return-void
.end method
