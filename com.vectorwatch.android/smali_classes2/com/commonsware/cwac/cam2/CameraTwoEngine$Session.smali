.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;
.super Lcom/commonsware/cwac/cam2/CameraSession;
.source "CameraTwoEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Session"
.end annotation


# instance fields
.field cameraDevice:Landroid/hardware/camera2/CameraDevice;

.field captureSession:Landroid/hardware/camera2/CameraCaptureSession;

.field isClosed:Z

.field previewRequest:Landroid/hardware/camera2/CaptureRequest;

.field previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

.field reader:Landroid/media/ImageReader;

.field zoomRect:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)V
    .locals 2
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "descriptor"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    const/4 v1, 0x0

    .line 776
    invoke-direct {p0, p1, p2}, Lcom/commonsware/cwac/cam2/CameraSession;-><init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)V

    .line 767
    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->cameraDevice:Landroid/hardware/camera2/CameraDevice;

    .line 768
    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    .line 769
    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 772
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->isClosed:Z

    .line 773
    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->zoomRect:Landroid/graphics/Rect;

    .line 777
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;
    .param p3, "x2"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;

    .prologue
    .line 766
    invoke-direct {p0, p1, p2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;-><init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)V

    return-void
.end method


# virtual methods
.method addToCaptureRequest(Landroid/hardware/camera2/CameraCharacteristics;ZLandroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 4
    .param p1, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p2, "isFacingFront"    # Z
    .param p3, "captureBuilder"    # Landroid/hardware/camera2/CaptureRequest$Builder;

    .prologue
    .line 798
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getPlugins()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/CameraPlugin;

    .line 799
    .local v1, "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    const-class v3, Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;

    invoke-interface {v1, v3}, Lcom/commonsware/cwac/cam2/CameraPlugin;->buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;

    .line 801
    .local v0, "configurator":Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;
    if-eqz v0, :cond_0

    .line 802
    invoke-interface {v0, p0, p1, p2, p3}, Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;->addToCaptureRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;ZLandroid/hardware/camera2/CaptureRequest$Builder;)V

    goto :goto_0

    .line 805
    .end local v0    # "configurator":Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;
    .end local v1    # "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    :cond_1
    return-void
.end method

.method addToPreviewRequest(Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 4
    .param p1, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p2, "captureBuilder"    # Landroid/hardware/camera2/CaptureRequest$Builder;

    .prologue
    .line 809
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getPlugins()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/CameraPlugin;

    .line 810
    .local v1, "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    const-class v3, Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;

    invoke-interface {v1, v3}, Lcom/commonsware/cwac/cam2/CameraPlugin;->buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;

    .line 812
    .local v0, "configurator":Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;
    if-eqz v0, :cond_0

    .line 813
    invoke-interface {v0, p0, p1, p2}, Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;->addToPreviewRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    goto :goto_0

    .line 816
    .end local v0    # "configurator":Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;
    .end local v1    # "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    :cond_1
    return-void
.end method

.method buildImageReader()Landroid/media/ImageReader;
    .locals 5

    .prologue
    .line 780
    const/4 v2, 0x0

    .line 782
    .local v2, "result":Landroid/media/ImageReader;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getPlugins()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/CameraPlugin;

    .line 783
    .local v1, "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    const-class v4, Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;

    invoke-interface {v1, v4}, Lcom/commonsware/cwac/cam2/CameraPlugin;->buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;

    .line 785
    .local v0, "configurator":Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;
    if-eqz v0, :cond_1

    .line 786
    invoke-interface {v0}, Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;->buildImageReader()Landroid/media/ImageReader;

    move-result-object v2

    .line 789
    :cond_1
    if-eqz v2, :cond_0

    .line 792
    .end local v0    # "configurator":Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;
    .end local v1    # "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    :cond_2
    return-object v2
.end method

.method getZoomRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 827
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->zoomRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method isClosed()Z
    .locals 1

    .prologue
    .line 819
    iget-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->isClosed:Z

    return v0
.end method

.method setClosed(Z)V
    .locals 0
    .param p1, "isClosed"    # Z

    .prologue
    .line 823
    iput-boolean p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->isClosed:Z

    .line 824
    return-void
.end method

.method setZoomRect(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "zoomRect"    # Landroid/graphics/Rect;

    .prologue
    .line 831
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->zoomRect:Landroid/graphics/Rect;

    .line 832
    return-void
.end method
