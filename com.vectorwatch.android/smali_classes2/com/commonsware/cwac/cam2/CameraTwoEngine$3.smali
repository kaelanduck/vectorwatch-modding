.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;
.super Ljava/lang/Object;
.source "CameraTwoEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;->takePicture(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/PictureTransaction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

.field final synthetic val$s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .prologue
    .line 286
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->val$s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 291
    :try_start_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->val$s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v1, v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v2, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v3, 0x1

    .line 292
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 291
    invoke-virtual {v1, v2, v3}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 293
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->val$s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v1, v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->val$s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v2, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 294
    invoke-virtual {v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v2

    new-instance v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->val$s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-direct {v3, v4, v5}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;-><init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;)V

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .line 296
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$900(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/os/Handler;

    move-result-object v4

    .line 293
    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v1

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    invoke-direct {v2, v0}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v1, v2}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 300
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 301
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception taking picture"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
