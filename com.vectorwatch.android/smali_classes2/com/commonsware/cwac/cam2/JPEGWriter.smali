.class public Lcom/commonsware/cwac/cam2/JPEGWriter;
.super Lcom/commonsware/cwac/cam2/AbstractImageProcessor;
.source "JPEGWriter.java"


# static fields
.field public static final PROP_OUTPUT:Ljava/lang/String; = "output"

.field public static final PROP_UPDATE_MEDIA_STORE:Ljava/lang/String; = "update"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "ctxt"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/AbstractImageProcessor;-><init>(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/commonsware/cwac/cam2/AbstractImageProcessor;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 60
    return-void
.end method


# virtual methods
.method public process(Lcom/commonsware/cwac/cam2/PictureTransaction;Lcom/commonsware/cwac/cam2/ImageContext;)V
    .locals 12
    .param p1, "xact"    # Lcom/commonsware/cwac/cam2/PictureTransaction;
    .param p2, "imageContext"    # Lcom/commonsware/cwac/cam2/ImageContext;

    .prologue
    const/4 v9, 0x0

    .line 67
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/PictureTransaction;->getProperties()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "output"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 69
    .local v4, "output":Landroid/net/Uri;
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/PictureTransaction;->getProperties()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "update"

    .line 70
    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 72
    .local v6, "updateMediaStore":Z
    if-eqz v4, :cond_0

    .line 74
    :try_start_0
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    const-string v8, "file"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 75
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 76
    .local v5, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 80
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 82
    .local v2, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/ImageContext;->getJpeg()[B

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/FileOutputStream;->write([B)V

    .line 83
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    .line 84
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/FileDescriptor;->sync()V

    .line 85
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 87
    if-eqz v6, :cond_0

    .line 88
    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/ImageContext;->getContext()Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "image/jpeg"

    aput-object v11, v9, v10

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v10}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 104
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .end local v5    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/JPEGWriter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v3

    .line 95
    .local v3, "out":Ljava/io/OutputStream;
    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/ImageContext;->getJpeg()[B

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/io/OutputStream;->write([B)V

    .line 96
    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    .line 97
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 99
    .end local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v7

    new-instance v8, Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;

    invoke-direct {v8, v0}, Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v7, v8}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
