.class public Lcom/commonsware/cwac/cam2/ClassicCameraEngine;
.super Lcom/commonsware/cwac/cam2/CameraEngine;
.source "ClassicCameraEngine.java"

# interfaces
.implements Landroid/media/MediaRecorder$OnInfoListener;
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Landroid/hardware/Camera$OnZoomChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/ClassicCameraEngine$SessionBuilder;,
        Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;,
        Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;,
        Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;
    }
.end annotation


# instance fields
.field private final ctxt:Landroid/content/Context;

.field private descriptors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;",
            ">;"
        }
    .end annotation
.end field

.field private previewFormat:I

.field private previewHeight:I

.field private previewWidth:I

.field private recorder:Landroid/media/MediaRecorder;

.field private xact:Lcom/commonsware/cwac/cam2/VideoTransaction;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraEngine;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->descriptors:Ljava/util/List;

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->ctxt:Landroid/content/Context;

    .line 55
    return-void
.end method

.method static synthetic access$100(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->descriptors:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .prologue
    .line 43
    iget v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewFormat:I

    return v0
.end method

.method static synthetic access$1002(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewFormat:I

    return p1
.end method

.method static synthetic access$102(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->descriptors:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .prologue
    .line 43
    iget v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewWidth:I

    return v0
.end method

.method static synthetic access$802(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewWidth:I

    return p1
.end method

.method static synthetic access$900(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)I
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .prologue
    .line 43
    iget v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewHeight:I

    return v0
.end method

.method static synthetic access$902(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;I)I
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewHeight:I

    return p1
.end method


# virtual methods
.method public buildSession(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraSession$Builder;
    .locals 2
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "descriptor"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    .line 62
    new-instance v0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$SessionBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$SessionBuilder;-><init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;)V

    return-object v0
.end method

.method public close(Lcom/commonsware/cwac/cam2/CameraSession;)V
    .locals 4
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 147
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 148
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCamera()Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$600(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;)Landroid/hardware/Camera;

    move-result-object v0

    .line 150
    .local v0, "camera":Landroid/hardware/Camera;
    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 152
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 153
    const/4 v2, 0x0

    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->setCamera(Landroid/hardware/Camera;)V
    invoke-static {v1, v2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$700(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Landroid/hardware/Camera;)V

    .line 156
    :cond_0
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->destroy()V

    .line 157
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v2

    new-instance v3, Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;

    invoke-direct {v3}, Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;-><init>()V

    invoke-virtual {v2, v3}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 158
    return-void
.end method

.method public handleOrientationChange(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;)V
    .locals 1
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;

    .prologue
    .line 351
    if-eqz p1, :cond_0

    .line 352
    check-cast p1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;

    .end local p1    # "session":Lcom/commonsware/cwac/cam2/CameraSession;
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;->configureStillCamera(Z)Landroid/hardware/Camera$Parameters;

    .line 354
    :cond_0
    return-void
.end method

.method public loadCameraDescriptors(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V
    .locals 2
    .param p1, "criteria"    # Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getThreadPool()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;

    invoke-direct {v1, p0, p1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;-><init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 140
    return-void
.end method

.method public onInfo(Landroid/media/MediaRecorder;II)V
    .locals 4
    .param p1, "mediaRecorder"    # Landroid/media/MediaRecorder;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 358
    const/16 v1, 0x320

    if-eq p2, v1, :cond_0

    const/16 v1, 0x321

    if-eq p2, v1, :cond_0

    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    .line 363
    .local v0, "tempRecorder":Landroid/media/MediaRecorder;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    .line 365
    if-eqz v0, :cond_1

    .line 366
    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 367
    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 370
    :cond_1
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v1

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->xact:Lcom/commonsware/cwac/cam2/VideoTransaction;

    invoke-direct {v2, v3}, Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;-><init>(Lcom/commonsware/cwac/cam2/VideoTransaction;)V

    invoke-virtual {v1, v2}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 372
    .end local v0    # "tempRecorder":Landroid/media/MediaRecorder;
    :cond_2
    return-void
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 376
    new-instance v0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;

    invoke-direct {v0, p0, p1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;-><init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;[B)V

    .line 400
    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->start()V

    .line 401
    return-void
.end method

.method public onZoomChange(IZLandroid/hardware/Camera;)V
    .locals 2
    .param p1, "zoomValue"    # I
    .param p2, "stopped"    # Z
    .param p3, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 443
    if-eqz p2, :cond_0

    .line 444
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraEngine$SmoothZoomCompletedEvent;

    invoke-direct {v1}, Lcom/commonsware/cwac/cam2/CameraEngine$SmoothZoomCompletedEvent;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 446
    :cond_0
    return-void
.end method

.method public open(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V
    .locals 2
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "texture"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getThreadPool()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$3;-><init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 264
    return-void
.end method

.method public recordVideo(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/VideoTransaction;)V
    .locals 6
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "xact"    # Lcom/commonsware/cwac/cam2/VideoTransaction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 269
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 270
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCamera()Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$600(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;)Landroid/hardware/Camera;

    move-result-object v0

    .line 272
    .local v0, "camera":Landroid/hardware/Camera;
    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 274
    invoke-virtual {v0}, Landroid/hardware/Camera;->unlock()V

    .line 277
    :try_start_0
    new-instance v3, Landroid/media/MediaRecorder;

    invoke-direct {v3}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    .line 278
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v0}, Landroid/media/MediaRecorder;->setCamera(Landroid/hardware/Camera;)V

    .line 279
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 281
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    .line 283
    check-cast p1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;

    .end local p1    # "session":Lcom/commonsware/cwac/cam2/CameraSession;
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {p1, p2, v3}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;->configureRecorder(Lcom/commonsware/cwac/cam2/VideoTransaction;Landroid/media/MediaRecorder;)V

    .line 285
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/VideoTransaction;->getOutputPath()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 286
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/VideoTransaction;->getSizeLimit()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/media/MediaRecorder;->setMaxFileSize(J)V

    .line 287
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/VideoTransaction;->getDurationLimit()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setMaxDuration(I)V

    .line 288
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, p0}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 289
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->prepare()V

    .line 290
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->start()V

    .line 291
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->xact:Lcom/commonsware/cwac/cam2/VideoTransaction;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :cond_0
    return-void

    .line 292
    :catch_0
    move-exception v2

    .line 293
    .local v2, "e":Ljava/io/IOException;
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->release()V

    .line 294
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    .line 295
    throw v2
.end method

.method public stopVideoRecording(Lcom/commonsware/cwac/cam2/CameraSession;Z)V
    .locals 7
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "abandon"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 303
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 304
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCamera()Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$600(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;)Landroid/hardware/Camera;

    move-result-object v0

    .line 306
    .local v0, "camera":Landroid/hardware/Camera;
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    if-eqz v3, :cond_0

    .line 307
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    .line 309
    .local v2, "tempRecorder":Landroid/media/MediaRecorder;
    iput-object v6, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->recorder:Landroid/media/MediaRecorder;

    .line 311
    invoke-virtual {v2}, Landroid/media/MediaRecorder;->stop()V

    .line 312
    invoke-virtual {v2}, Landroid/media/MediaRecorder;->release()V

    .line 314
    if-nez p2, :cond_0

    .line 315
    invoke-virtual {v0}, Landroid/hardware/Camera;->reconnect()V

    .line 316
    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 320
    .end local v2    # "tempRecorder":Landroid/media/MediaRecorder;
    :cond_0
    if-nez p2, :cond_1

    .line 321
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v3

    new-instance v4, Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;

    iget-object v5, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->xact:Lcom/commonsware/cwac/cam2/VideoTransaction;

    invoke-direct {v4, v5}, Lcom/commonsware/cwac/cam2/CameraEngine$VideoTakenEvent;-><init>(Lcom/commonsware/cwac/cam2/VideoTransaction;)V

    invoke-virtual {v3, v4}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 324
    :cond_1
    iput-object v6, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->xact:Lcom/commonsware/cwac/cam2/VideoTransaction;

    .line 325
    return-void
.end method

.method public supportsDynamicFlashModes()Z
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    return v0
.end method

.method public supportsZoom(Lcom/commonsware/cwac/cam2/CameraSession;)Z
    .locals 4
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 413
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 414
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCamera()Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$600(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;)Landroid/hardware/Camera;

    move-result-object v0

    .line 415
    .local v0, "camera":Landroid/hardware/Camera;
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    .line 417
    .local v2, "params":Landroid/hardware/Camera$Parameters;
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v3

    return v3
.end method

.method public takePicture(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/PictureTransaction;)V
    .locals 2
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "xact"    # Lcom/commonsware/cwac/cam2/PictureTransaction;

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getThreadPool()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;-><init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/PictureTransaction;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 198
    return-void
.end method

.method public zoomTo(Lcom/commonsware/cwac/cam2/CameraSession;I)Z
    .locals 6
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "zoomLevel"    # I

    .prologue
    .line 422
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 423
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCamera()Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$600(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;)Landroid/hardware/Camera;

    move-result-object v0

    .line 424
    .local v0, "camera":Landroid/hardware/Camera;
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    .line 425
    .local v2, "params":Landroid/hardware/Camera$Parameters;
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v5

    mul-int/2addr v5, p2

    div-int/lit8 v4, v5, 0x64

    .line 426
    .local v4, "zoom":I
    const/4 v3, 0x0

    .line 428
    .local v3, "result":Z
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->isSmoothZoomSupported()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 429
    invoke-virtual {v0, p0}, Landroid/hardware/Camera;->setZoomChangeListener(Landroid/hardware/Camera$OnZoomChangeListener;)V

    .line 430
    invoke-virtual {v0, v4}, Landroid/hardware/Camera;->startSmoothZoom(I)V

    .line 431
    const/4 v3, 0x1

    .line 437
    :cond_0
    :goto_0
    return v3

    .line 432
    :cond_1
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->isZoomSupported()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 433
    invoke-virtual {v2, v4}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 434
    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_0
.end method
