.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;
.super Ljava/lang/Object;
.source "CameraTwoEngine.java"

# interfaces
.implements Landroid/media/ImageReader$OnImageAvailableListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TakePictureTransaction"
.end annotation


# instance fields
.field private final bus:Lde/greenrobot/event/EventBus;

.field private final ctxt:Landroid/content/Context;

.field private final xact:Lcom/commonsware/cwac/cam2/PictureTransaction;


# direct methods
.method constructor <init>(Landroid/content/Context;Lde/greenrobot/event/EventBus;Lcom/commonsware/cwac/cam2/PictureTransaction;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "bus"    # Lde/greenrobot/event/EventBus;
    .param p3, "xact"    # Lcom/commonsware/cwac/cam2/PictureTransaction;

    .prologue
    .line 846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 847
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;->bus:Lde/greenrobot/event/EventBus;

    .line 848
    iput-object p3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;->xact:Lcom/commonsware/cwac/cam2/PictureTransaction;

    .line 849
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;->ctxt:Landroid/content/Context;

    .line 850
    return-void
.end method


# virtual methods
.method public onImageAvailable(Landroid/media/ImageReader;)V
    .locals 9
    .param p1, "imageReader"    # Landroid/media/ImageReader;

    .prologue
    .line 854
    invoke-virtual {p1}, Landroid/media/ImageReader;->acquireNextImage()Landroid/media/Image;

    move-result-object v2

    .line 855
    .local v2, "image":Landroid/media/Image;
    invoke-virtual {v2}, Landroid/media/Image;->getPlanes()[Landroid/media/Image$Plane;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/media/Image$Plane;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 856
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    new-array v1, v3, [B

    .line 858
    .local v1, "bytes":[B
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 859
    invoke-virtual {v2}, Landroid/media/Image;->close()V

    .line 861
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;->bus:Lde/greenrobot/event/EventBus;

    new-instance v4, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;->xact:Lcom/commonsware/cwac/cam2/PictureTransaction;

    iget-object v6, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;->xact:Lcom/commonsware/cwac/cam2/PictureTransaction;

    new-instance v7, Lcom/commonsware/cwac/cam2/ImageContext;

    iget-object v8, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;->ctxt:Landroid/content/Context;

    invoke-direct {v7, v8, v1}, Lcom/commonsware/cwac/cam2/ImageContext;-><init>(Landroid/content/Context;[B)V

    .line 862
    invoke-virtual {v6, v7}, Lcom/commonsware/cwac/cam2/PictureTransaction;->process(Lcom/commonsware/cwac/cam2/ImageContext;)Lcom/commonsware/cwac/cam2/ImageContext;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;-><init>(Lcom/commonsware/cwac/cam2/PictureTransaction;Lcom/commonsware/cwac/cam2/ImageContext;)V

    .line 861
    invoke-virtual {v3, v4}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 863
    return-void
.end method
