.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;
.super Ljava/lang/Object;
.source "CameraTwoEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;->open(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

.field final synthetic val$session:Lcom/commonsware/cwac/cam2/CameraSession;

.field final synthetic val$texture:Landroid/graphics/SurfaceTexture;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    iput-object p3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->val$texture:Landroid/graphics/SurfaceTexture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 186
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v7}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .line 189
    .local v1, "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    :try_start_0
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .line 190
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;
    invoke-static {v7}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/hardware/camera2/CameraManager;

    move-result-object v7

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v2

    .line 192
    .local v2, "cc":Landroid/hardware/camera2/CameraCharacteristics;
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iget-object v7, v7, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 194
    sget-object v7, Landroid/hardware/camera2/CameraCharacteristics;->CONTROL_AE_AVAILABLE_MODES:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v2, v7}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 196
    .local v0, "availModes":[I
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iget-object v7, v7, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->preferredFlashModes:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/commonsware/cwac/cam2/FlashMode;

    .line 197
    .local v4, "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    array-length v9, v0

    move v7, v6

    :goto_1
    if-ge v7, v9, :cond_0

    aget v5, v0, v7

    .line 198
    .local v5, "rawFlashMode":I
    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/FlashMode;->getCameraTwoMode()I

    move-result v10

    if-ne v5, v10, :cond_2

    .line 199
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iget-object v7, v7, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 225
    .end local v0    # "availModes":[I
    .end local v2    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    .end local v4    # "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    .end local v5    # "rawFlashMode":I
    :catch_0
    move-exception v3

    .line 226
    .local v3, "e":Ljava/lang/Exception;
    iget-object v6, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v6}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v6

    new-instance v7, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;

    invoke-direct {v7, v3}, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v6, v7}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 228
    iget-object v6, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v6}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->isDebug()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 229
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Exception opening camera"

    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 232
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-void

    .line 197
    .restart local v0    # "availModes":[I
    .restart local v2    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    .restart local v4    # "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    .restart local v5    # "rawFlashMode":I
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 205
    .end local v4    # "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    .end local v5    # "rawFlashMode":I
    :cond_3
    :try_start_1
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iget-object v7, v7, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 206
    array-length v7, v0

    :goto_3
    if-ge v6, v7, :cond_5

    aget v5, v0, v6

    .line 207
    .restart local v5    # "rawFlashMode":I
    invoke-static {v5}, Lcom/commonsware/cwac/cam2/FlashMode;->lookupCameraTwoMode(I)Lcom/commonsware/cwac/cam2/FlashMode;

    move-result-object v4

    .line 210
    .restart local v4    # "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    if-eqz v4, :cond_4

    .line 211
    iget-object v8, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iget-object v8, v8, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 216
    .end local v4    # "flashMode":Lcom/commonsware/cwac/cam2/FlashMode;
    .end local v5    # "rawFlashMode":I
    :cond_5
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    iget-object v6, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iget-object v6, v6, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->eligibleFlashModes:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/commonsware/cwac/cam2/FlashMode;

    invoke-virtual {v7, v6}, Lcom/commonsware/cwac/cam2/CameraSession;->setCurrentFlashMode(Lcom/commonsware/cwac/cam2/FlashMode;)V

    .line 218
    iget-object v6, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$800(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    const-wide/16 v8, 0x9c4

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v8, v9, v7}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 219
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Time out waiting to lock camera opening."

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 222
    :cond_6
    iget-object v6, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;
    invoke-static {v6}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/hardware/camera2/CameraManager;

    move-result-object v6

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->getId()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;

    iget-object v9, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iget-object v10, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    new-instance v11, Landroid/view/Surface;

    iget-object v12, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->val$texture:Landroid/graphics/SurfaceTexture;

    invoke-direct {v11, v12}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-direct {v8, v9, v10, v11}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;-><init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;Landroid/view/Surface;)V

    iget-object v9, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .line 224
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$900(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/os/Handler;

    move-result-object v9

    .line 222
    invoke-virtual {v6, v7, v8, v9}, Landroid/hardware/camera2/CameraManager;->openCamera(Ljava/lang/String;Landroid/hardware/camera2/CameraDevice$StateCallback;Landroid/os/Handler;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
