.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
.super Ljava/lang/Object;
.source "CameraTwoEngine.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraDescriptor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Descriptor"
.end annotation


# instance fields
.field private final cameraId:Ljava/lang/String;

.field private device:Landroid/hardware/camera2/CameraDevice;

.field private final facing:Ljava/lang/Integer;

.field private isFacingFront:Z

.field private pictureSizes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation
.end field

.field private previewSizes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Landroid/hardware/camera2/CameraCharacteristics;)V
    .locals 1
    .param p1, "cameraId"    # Ljava/lang/String;
    .param p2, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;

    .prologue
    .line 702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->cameraId:Ljava/lang/String;

    .line 704
    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {p2, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->facing:Ljava/lang/Integer;

    .line 705
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Landroid/hardware/camera2/CameraCharacteristics;Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p3, "x2"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;

    .prologue
    .line 694
    invoke-direct {p0, p1, p2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;-><init>(Ljava/lang/String;Landroid/hardware/camera2/CameraCharacteristics;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Landroid/hardware/camera2/CameraDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    .param p1, "x1"    # Landroid/hardware/camera2/CameraDevice;

    .prologue
    .line 694
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->setDevice(Landroid/hardware/camera2/CameraDevice;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .prologue
    .line 694
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->cameraId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .prologue
    .line 694
    iget-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->isFacingFront:Z

    return v0
.end method

.method static synthetic access$400(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 694
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->setPreviewSizes(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$500(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 694
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->setPictureSizes(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$600(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    .param p1, "x1"    # Z

    .prologue
    .line 694
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->setFacingFront(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    .param p1, "x1"    # Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .prologue
    .line 694
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I

    move-result v0

    return v0
.end method

.method private getDevice()Landroid/hardware/camera2/CameraDevice;
    .locals 1

    .prologue
    .line 716
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->device:Landroid/hardware/camera2/CameraDevice;

    return-object v0
.end method

.method private getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    .locals 3
    .param p1, "criteria"    # Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .prologue
    .line 751
    const/16 v0, 0xa

    .line 753
    .local v0, "score":I
    if-eqz p1, :cond_2

    .line 754
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->getFacing()Lcom/commonsware/cwac/cam2/Facing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/Facing;->isFront()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->facing:Ljava/lang/Integer;

    .line 755
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_1

    .line 756
    :cond_0
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->getFacing()Lcom/commonsware/cwac/cam2/Facing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/Facing;->isFront()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->facing:Ljava/lang/Integer;

    .line 757
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 758
    :cond_1
    const/4 v0, 0x0

    .line 762
    :cond_2
    return v0
.end method

.method private setDevice(Landroid/hardware/camera2/CameraDevice;)V
    .locals 0
    .param p1, "device"    # Landroid/hardware/camera2/CameraDevice;

    .prologue
    .line 712
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->device:Landroid/hardware/camera2/CameraDevice;

    .line 713
    return-void
.end method

.method private setFacingFront(Z)V
    .locals 0
    .param p1, "isFacingFront"    # Z

    .prologue
    .line 747
    iput-boolean p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->isFacingFront:Z

    .line 748
    return-void
.end method

.method private setPictureSizes(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Size;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 739
    .local p1, "sizes":Ljava/util/List;, "Ljava/util/List<Landroid/util/Size;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->pictureSizes:Ljava/util/ArrayList;

    .line 741
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Size;

    .line 742
    .local v0, "size":Landroid/util/Size;
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->pictureSizes:Ljava/util/ArrayList;

    new-instance v3, Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v0}, Landroid/util/Size;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/util/Size;->getHeight()I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/commonsware/cwac/cam2/util/Size;-><init>(II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 744
    .end local v0    # "size":Landroid/util/Size;
    :cond_0
    return-void
.end method

.method private setPreviewSizes(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 730
    .local p1, "sizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/commonsware/cwac/cam2/util/Size;>;"
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->previewSizes:Ljava/util/ArrayList;

    .line 731
    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->cameraId:Ljava/lang/String;

    return-object v0
.end method

.method public getPictureSizes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation

    .prologue
    .line 735
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->pictureSizes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPreviewSizes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation

    .prologue
    .line 726
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->previewSizes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isPictureFormatSupported(I)Z
    .locals 1
    .param p1, "format"    # I

    .prologue
    .line 721
    const/16 v0, 0x100

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
