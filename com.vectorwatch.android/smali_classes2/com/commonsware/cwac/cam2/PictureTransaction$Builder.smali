.class public Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;
.super Ljava/lang/Object;
.source "PictureTransaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/PictureTransaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private result:Lcom/commonsware/cwac/cam2/PictureTransaction;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Lcom/commonsware/cwac/cam2/PictureTransaction;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/commonsware/cwac/cam2/PictureTransaction;-><init>(Lcom/commonsware/cwac/cam2/PictureTransaction$1;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/PictureTransaction;

    return-void
.end method


# virtual methods
.method public append(Lcom/commonsware/cwac/cam2/ImageProcessor;)Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;
    .locals 1
    .param p1, "processor"    # Lcom/commonsware/cwac/cam2/ImageProcessor;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/PictureTransaction;

    # getter for: Lcom/commonsware/cwac/cam2/PictureTransaction;->processors:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/PictureTransaction;->access$100(Lcom/commonsware/cwac/cam2/PictureTransaction;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    return-object p0
.end method

.method public build()Lcom/commonsware/cwac/cam2/PictureTransaction;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/PictureTransaction;

    return-object v0
.end method

.method public toUri(Landroid/content/Context;Landroid/net/Uri;Z)Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;
    .locals 3
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "output"    # Landroid/net/Uri;
    .param p3, "updateMediaStore"    # Z

    .prologue
    .line 105
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/PictureTransaction;

    const-class v2, Lcom/commonsware/cwac/cam2/JPEGWriter;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/commonsware/cwac/cam2/PictureTransaction;->findProcessorByTag(Ljava/lang/String;)Lcom/commonsware/cwac/cam2/ImageProcessor;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/JPEGWriter;

    .line 107
    .local v0, "jpeg":Lcom/commonsware/cwac/cam2/JPEGWriter;
    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/commonsware/cwac/cam2/JPEGWriter;

    .end local v0    # "jpeg":Lcom/commonsware/cwac/cam2/JPEGWriter;
    invoke-direct {v0, p1}, Lcom/commonsware/cwac/cam2/JPEGWriter;-><init>(Landroid/content/Context;)V

    .line 109
    .restart local v0    # "jpeg":Lcom/commonsware/cwac/cam2/JPEGWriter;
    invoke-virtual {p0, v0}, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;->append(Lcom/commonsware/cwac/cam2/ImageProcessor;)Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/PictureTransaction;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/PictureTransaction;->getProperties()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "output"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 114
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;->result:Lcom/commonsware/cwac/cam2/PictureTransaction;

    .line 115
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/PictureTransaction;->getProperties()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "update"

    .line 116
    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 119
    return-object p0
.end method
