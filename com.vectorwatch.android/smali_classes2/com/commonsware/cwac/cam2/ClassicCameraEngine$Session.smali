.class Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;
.super Lcom/commonsware/cwac/cam2/CameraSession;
.source "ClassicCameraEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/ClassicCameraEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Session"
.end annotation


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)V
    .locals 0
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "descriptor"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    .line 535
    invoke-direct {p0, p1, p2}, Lcom/commonsware/cwac/cam2/CameraSession;-><init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)V

    .line 536
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;
    .param p3, "x2"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;

    .prologue
    .line 533
    invoke-direct {p0, p1, p2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;-><init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)V

    return-void
.end method


# virtual methods
.method configureRecorder(Lcom/commonsware/cwac/cam2/VideoTransaction;Landroid/media/MediaRecorder;)V
    .locals 5
    .param p1, "xact"    # Lcom/commonsware/cwac/cam2/VideoTransaction;
    .param p2, "recorder"    # Landroid/media/MediaRecorder;

    .prologue
    .line 570
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 572
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;->getPlugins()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/commonsware/cwac/cam2/CameraPlugin;

    .line 573
    .local v2, "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    const-class v4, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;

    .line 574
    invoke-interface {v2, v4}, Lcom/commonsware/cwac/cam2/CameraPlugin;->buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;

    .line 576
    .local v0, "configurator":Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;
    if-eqz v0, :cond_0

    .line 577
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCameraId()I

    move-result v4

    invoke-interface {v0, p0, v4, p1, p2}, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;->configureRecorder(Lcom/commonsware/cwac/cam2/CameraSession;ILcom/commonsware/cwac/cam2/VideoTransaction;Landroid/media/MediaRecorder;)V

    goto :goto_0

    .line 581
    .end local v0    # "configurator":Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;
    .end local v2    # "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    :cond_1
    return-void
.end method

.method configureStillCamera(Z)Landroid/hardware/Camera$Parameters;
    .locals 8
    .param p1, "noParams"    # Z

    .prologue
    .line 539
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v2

    check-cast v2, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 540
    .local v2, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCamera()Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$600(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;)Landroid/hardware/Camera;

    move-result-object v0

    .line 541
    .local v0, "camera":Landroid/hardware/Camera;
    const/4 v4, 0x0

    .line 543
    .local v4, "params":Landroid/hardware/Camera$Parameters;
    if-eqz v0, :cond_2

    .line 544
    new-instance v3, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v3}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 546
    .local v3, "info":Landroid/hardware/Camera$CameraInfo;
    if-nez p1, :cond_0

    .line 547
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    .line 550
    :cond_0
    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCameraId()I

    move-result v6

    invoke-static {v6, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 552
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Session;->getPlugins()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/commonsware/cwac/cam2/CameraPlugin;

    .line 553
    .local v5, "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    const-class v7, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;

    .line 554
    invoke-interface {v5, v7}, Lcom/commonsware/cwac/cam2/CameraPlugin;->buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;

    .line 557
    .local v1, "configurator":Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;
    if-eqz v1, :cond_1

    .line 559
    invoke-interface {v1, p0, v3, v0, v4}, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;->configureStillCamera(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/Camera$CameraInfo;Landroid/hardware/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;

    move-result-object v4

    goto :goto_0

    .line 565
    .end local v1    # "configurator":Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;
    .end local v3    # "info":Landroid/hardware/Camera$CameraInfo;
    .end local v5    # "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    :cond_2
    return-object v4
.end method
