.class Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
.super Ljava/lang/Object;
.source "ClassicCameraEngine.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraDescriptor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/ClassicCameraEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Descriptor"
.end annotation


# instance fields
.field private camera:Landroid/hardware/Camera;

.field private cameraId:I

.field private final facing:I

.field private pictureSizes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation
.end field

.field private previewSizes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ILandroid/hardware/Camera$CameraInfo;)V
    .locals 1
    .param p1, "cameraId"    # I
    .param p2, "info"    # Landroid/hardware/Camera$CameraInfo;

    .prologue
    .line 477
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478
    iput p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->cameraId:I

    .line 479
    iget v0, p2, Landroid/hardware/Camera$CameraInfo;->facing:I

    iput v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->facing:I

    .line 480
    return-void
.end method

.method synthetic constructor <init>(ILandroid/hardware/Camera$CameraInfo;Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/hardware/Camera$CameraInfo;
    .param p3, "x2"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$1;

    .prologue
    .line 470
    invoke-direct {p0, p1, p2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;-><init>(ILandroid/hardware/Camera$CameraInfo;)V

    return-void
.end method

.method static synthetic access$300(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 470
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->setPreviewSizes(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$400(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 470
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->setPictureSizes(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$500(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    .param p1, "x1"    # Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .prologue
    .line 470
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;)Landroid/hardware/Camera;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .prologue
    .line 470
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCamera()Landroid/hardware/Camera;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;Landroid/hardware/Camera;)V
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    .param p1, "x1"    # Landroid/hardware/Camera;

    .prologue
    .line 470
    invoke-direct {p0, p1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->setCamera(Landroid/hardware/Camera;)V

    return-void
.end method

.method private getCamera()Landroid/hardware/Camera;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->camera:Landroid/hardware/Camera;

    return-object v0
.end method

.method private getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    .locals 3
    .param p1, "criteria"    # Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .prologue
    .line 518
    const/16 v0, 0xa

    .line 520
    .local v0, "score":I
    if-eqz p1, :cond_2

    .line 521
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->getFacing()Lcom/commonsware/cwac/cam2/Facing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/Facing;->isFront()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->facing:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 523
    :cond_0
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->getFacing()Lcom/commonsware/cwac/cam2/Facing;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/Facing;->isFront()Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->facing:I

    if-eqz v1, :cond_2

    .line 525
    :cond_1
    const/4 v0, 0x0

    .line 529
    :cond_2
    return v0
.end method

.method private setCamera(Landroid/hardware/Camera;)V
    .locals 0
    .param p1, "camera"    # Landroid/hardware/Camera;

    .prologue
    .line 487
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->camera:Landroid/hardware/Camera;

    .line 488
    return-void
.end method

.method private setPictureSizes(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 514
    .local p1, "sizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/commonsware/cwac/cam2/util/Size;>;"
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->pictureSizes:Ljava/util/ArrayList;

    .line 515
    return-void
.end method

.method private setPreviewSizes(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 500
    .local p1, "sizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/commonsware/cwac/cam2/util/Size;>;"
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->previewSizes:Ljava/util/ArrayList;

    .line 501
    return-void
.end method


# virtual methods
.method public getCameraId()I
    .locals 1

    .prologue
    .line 483
    iget v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->cameraId:I

    return v0
.end method

.method public getPictureSizes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation

    .prologue
    .line 505
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->pictureSizes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPreviewSizes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->previewSizes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isPictureFormatSupported(I)Z
    .locals 1
    .param p1, "format"    # I

    .prologue
    .line 510
    const/16 v0, 0x100

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
