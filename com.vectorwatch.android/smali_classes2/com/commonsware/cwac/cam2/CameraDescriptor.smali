.class public interface abstract Lcom/commonsware/cwac/cam2/CameraDescriptor;
.super Ljava/lang/Object;
.source "CameraDescriptor.java"


# virtual methods
.method public abstract getPictureSizes()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPreviewSizes()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/util/Size;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isPictureFormatSupported(I)Z
.end method
