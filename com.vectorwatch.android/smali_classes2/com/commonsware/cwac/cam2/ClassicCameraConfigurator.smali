.class public interface abstract Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;
.super Ljava/lang/Object;
.source "ClassicCameraConfigurator.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraConfigurator;


# virtual methods
.method public abstract configureRecorder(Lcom/commonsware/cwac/cam2/CameraSession;ILcom/commonsware/cwac/cam2/VideoTransaction;Landroid/media/MediaRecorder;)V
.end method

.method public abstract configureStillCamera(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/Camera$CameraInfo;Landroid/hardware/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
.end method
