.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$1$1;
.super Ljava/lang/Object;
.source "CameraTwoEngine.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/commonsware/cwac/cam2/CameraDescriptor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1$1;->this$1:Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/commonsware/cwac/cam2/CameraDescriptor;Lcom/commonsware/cwac/cam2/CameraDescriptor;)I
    .locals 4
    .param p1, "descriptor"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;
    .param p2, "t1"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    .line 161
    move-object v0, p1

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .local v0, "lhs":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    move-object v1, p2

    .line 162
    check-cast v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .line 166
    .local v1, "rhs":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1$1;->this$1:Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;

    iget-object v2, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    # invokes: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    invoke-static {v1, v2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$700(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I

    move-result v2

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1$1;->this$1:Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;

    iget-object v3, v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .line 167
    # invokes: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    invoke-static {v0, v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$700(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I

    move-result v3

    .line 166
    invoke-static {v2, v3}, Ljava/lang/Integer;->compare(II)I

    move-result v2

    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 157
    check-cast p1, Lcom/commonsware/cwac/cam2/CameraDescriptor;

    check-cast p2, Lcom/commonsware/cwac/cam2/CameraDescriptor;

    invoke-virtual {p0, p1, p2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1$1;->compare(Lcom/commonsware/cwac/cam2/CameraDescriptor;Lcom/commonsware/cwac/cam2/CameraDescriptor;)I

    move-result v0

    return v0
.end method
