.class Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;
.super Ljava/lang/Thread;
.source "ClassicCameraEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->onPreviewFrame([BLandroid/hardware/Camera;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

.field final synthetic val$data:[B


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;[B)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .prologue
    .line 376
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iput-object p2, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->val$data:[B

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 379
    new-instance v0, Landroid/graphics/YuvImage;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->val$data:[B

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewFormat:I
    invoke-static {v2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$1000(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)I

    move-result v2

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .line 380
    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewWidth:I
    invoke-static {v3}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$800(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)I

    move-result v3

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewHeight:I
    invoke-static {v4}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$900(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)I

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/YuvImage;-><init>([BIII[I)V

    .line 383
    .local v0, "yuv":Landroid/graphics/YuvImage;
    :try_start_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->savePreviewFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->savePreviewFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 387
    :cond_0
    new-instance v7, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .line 388
    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->savePreviewFile()Ljava/io/File;

    move-result-object v1

    invoke-direct {v7, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 390
    .local v7, "fos":Ljava/io/FileOutputStream;
    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewWidth:I
    invoke-static {v4}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$800(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)I

    move-result v4

    iget-object v5, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$4;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewHeight:I
    invoke-static {v5}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$900(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;)I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v2, 0x5a

    invoke-virtual {v0, v1, v2, v7}, Landroid/graphics/YuvImage;->compressToJpeg(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z

    .line 392
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V

    .line 393
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileDescriptor;->sync()V

    .line 394
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 395
    :catch_0
    move-exception v6

    .line 396
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception saving preview frame"

    invoke-static {v1, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
