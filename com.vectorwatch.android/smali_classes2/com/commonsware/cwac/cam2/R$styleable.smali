.class public final Lcom/commonsware/cwac/cam2/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final FloatingActionButton:[I

.field public static final FloatingActionButton_fab_colorDisabled:I = 0x4

.field public static final FloatingActionButton_fab_colorNormal:I = 0x2

.field public static final FloatingActionButton_fab_colorPressed:I = 0x3

.field public static final FloatingActionButton_fab_colorRipple:I = 0x5

.field public static final FloatingActionButton_fab_elevationCompat:I = 0xf

.field public static final FloatingActionButton_fab_hideAnimation:I = 0xd

.field public static final FloatingActionButton_fab_label:I = 0xe

.field public static final FloatingActionButton_fab_progress:I = 0x14

.field public static final FloatingActionButton_fab_progress_backgroundColor:I = 0x11

.field public static final FloatingActionButton_fab_progress_color:I = 0x10

.field public static final FloatingActionButton_fab_progress_indeterminate:I = 0x12

.field public static final FloatingActionButton_fab_progress_max:I = 0x13

.field public static final FloatingActionButton_fab_progress_showBackground:I = 0x15

.field public static final FloatingActionButton_fab_shadowColor:I = 0x7

.field public static final FloatingActionButton_fab_shadowRadius:I = 0x8

.field public static final FloatingActionButton_fab_shadowXOffset:I = 0x9

.field public static final FloatingActionButton_fab_shadowYOffset:I = 0xa

.field public static final FloatingActionButton_fab_showAnimation:I = 0xc

.field public static final FloatingActionButton_fab_showShadow:I = 0x6

.field public static final FloatingActionButton_fab_size:I = 0xb

.field public static final FloatingActionMenu:[I

.field public static final FloatingActionMenu_menu_animationDelayPerItem:I = 0x13

.field public static final FloatingActionMenu_menu_backgroundColor:I = 0x22

.field public static final FloatingActionMenu_menu_buttonSpacing:I = 0x1

.field public static final FloatingActionMenu_menu_buttonToggleAnimation:I = 0x14

.field public static final FloatingActionMenu_menu_colorNormal:I = 0x1e

.field public static final FloatingActionMenu_menu_colorPressed:I = 0x1f

.field public static final FloatingActionMenu_menu_colorRipple:I = 0x20

.field public static final FloatingActionMenu_menu_fab_hide_animation:I = 0x25

.field public static final FloatingActionMenu_menu_fab_label:I = 0x23

.field public static final FloatingActionMenu_menu_fab_show_animation:I = 0x24

.field public static final FloatingActionMenu_menu_fab_size:I = 0x18

.field public static final FloatingActionMenu_menu_icon:I = 0x12

.field public static final FloatingActionMenu_menu_labels_colorNormal:I = 0xe

.field public static final FloatingActionMenu_menu_labels_colorPressed:I = 0xf

.field public static final FloatingActionMenu_menu_labels_colorRipple:I = 0x10

.field public static final FloatingActionMenu_menu_labels_cornerRadius:I = 0xc

.field public static final FloatingActionMenu_menu_labels_ellipsize:I = 0x16

.field public static final FloatingActionMenu_menu_labels_hideAnimation:I = 0x4

.field public static final FloatingActionMenu_menu_labels_margin:I = 0x2

.field public static final FloatingActionMenu_menu_labels_maxLines:I = 0x17

.field public static final FloatingActionMenu_menu_labels_padding:I = 0x9

.field public static final FloatingActionMenu_menu_labels_paddingBottom:I = 0x8

.field public static final FloatingActionMenu_menu_labels_paddingLeft:I = 0x6

.field public static final FloatingActionMenu_menu_labels_paddingRight:I = 0x7

.field public static final FloatingActionMenu_menu_labels_paddingTop:I = 0x5

.field public static final FloatingActionMenu_menu_labels_position:I = 0x11

.field public static final FloatingActionMenu_menu_labels_showAnimation:I = 0x3

.field public static final FloatingActionMenu_menu_labels_showShadow:I = 0xd

.field public static final FloatingActionMenu_menu_labels_singleLine:I = 0x15

.field public static final FloatingActionMenu_menu_labels_style:I = 0x19

.field public static final FloatingActionMenu_menu_labels_textColor:I = 0xa

.field public static final FloatingActionMenu_menu_labels_textSize:I = 0xb

.field public static final FloatingActionMenu_menu_openDirection:I = 0x21

.field public static final FloatingActionMenu_menu_shadowColor:I = 0x1a

.field public static final FloatingActionMenu_menu_shadowRadius:I = 0x1b

.field public static final FloatingActionMenu_menu_shadowXOffset:I = 0x1c

.field public static final FloatingActionMenu_menu_shadowYOffset:I = 0x1d

.field public static final FloatingActionMenu_menu_showShadow:I = 0x0

.field public static final SubsamplingScaleImageView:[I

.field public static final SubsamplingScaleImageView_assetName:I = 0x1

.field public static final SubsamplingScaleImageView_panEnabled:I = 0x2

.field public static final SubsamplingScaleImageView_quickScaleEnabled:I = 0x4

.field public static final SubsamplingScaleImageView_src:I = 0x0

.field public static final SubsamplingScaleImageView_tileBackgroundColor:I = 0x5

.field public static final SubsamplingScaleImageView_zoomEnabled:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 203
    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/commonsware/cwac/cam2/R$styleable;->FloatingActionButton:[I

    .line 224
    const/16 v0, 0x26

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/commonsware/cwac/cam2/R$styleable;->FloatingActionMenu:[I

    .line 263
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/commonsware/cwac/cam2/R$styleable;->SubsamplingScaleImageView:[I

    return-void

    .line 203
    nop

    :array_0
    .array-data 4
        0x10100d4
        0x7f01001f
        0x7f01008b
        0x7f01008c
        0x7f01008d
        0x7f01008e
        0x7f01008f
        0x7f010090
        0x7f010091
        0x7f010092
        0x7f010093
        0x7f010094
        0x7f010095
        0x7f010096
        0x7f010097
        0x7f010098
        0x7f010099
        0x7f01009a
        0x7f01009b
        0x7f01009c
        0x7f01009d
        0x7f01009e
        0x7f01009f
        0x7f0100a0
        0x7f0100a1
        0x7f0100a2
        0x7f0101ca
        0x7f0101cb
    .end array-data

    .line 224
    :array_1
    .array-data 4
        0x7f0100a3
        0x7f0100a4
        0x7f0100a5
        0x7f0100a6
        0x7f0100a7
        0x7f0100a8
        0x7f0100a9
        0x7f0100aa
        0x7f0100ab
        0x7f0100ac
        0x7f0100ad
        0x7f0100ae
        0x7f0100af
        0x7f0100b0
        0x7f0100b1
        0x7f0100b2
        0x7f0100b3
        0x7f0100b4
        0x7f0100b5
        0x7f0100b6
        0x7f0100b7
        0x7f0100b8
        0x7f0100b9
        0x7f0100ba
        0x7f0100bb
        0x7f0100bc
        0x7f0100bd
        0x7f0100be
        0x7f0100bf
        0x7f0100c0
        0x7f0100c1
        0x7f0100c2
        0x7f0100c3
        0x7f0100c4
        0x7f0100c5
        0x7f0100c6
        0x7f0100c7
        0x7f0100c8
    .end array-data

    .line 263
    :array_2
    .array-data 4
        0x7f010115
        0x7f010116
        0x7f010117
        0x7f010118
        0x7f010119
        0x7f01011a
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
