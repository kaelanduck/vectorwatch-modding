.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;
.super Landroid/hardware/camera2/CameraDevice$StateCallback;
.source "CameraTwoEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitPreviewTransaction"
.end annotation


# instance fields
.field private final s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

.field private final surface:Landroid/view/Surface;

.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;Landroid/view/Surface;)V
    .locals 0
    .param p2, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p3, "surface"    # Landroid/view/Surface;

    .prologue
    .line 407
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraDevice$StateCallback;-><init>()V

    .line 408
    check-cast p2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .end local p2    # "session":Lcom/commonsware/cwac/cam2/CameraSession;
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .line 409
    iput-object p3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->surface:Landroid/view/Surface;

    .line 410
    return-void
.end method


# virtual methods
.method public onClosed(Landroid/hardware/camera2/CameraDevice;)V
    .locals 1
    .param p1, "camera"    # Landroid/hardware/camera2/CameraDevice;

    .prologue
    .line 446
    invoke-super {p0, p1}, Landroid/hardware/camera2/CameraDevice$StateCallback;->onClosed(Landroid/hardware/camera2/CameraDevice;)V

    .line 448
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->closeLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$1200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->closeLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$1200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 451
    :cond_0
    return-void
.end method

.method public onDisconnected(Landroid/hardware/camera2/CameraDevice;)V
    .locals 1
    .param p1, "cameraDevice"    # Landroid/hardware/camera2/CameraDevice;

    .prologue
    .line 433
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$800(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 434
    invoke-virtual {p1}, Landroid/hardware/camera2/CameraDevice;->close()V

    .line 435
    return-void
.end method

.method public onError(Landroid/hardware/camera2/CameraDevice;I)V
    .locals 2
    .param p1, "cameraDevice"    # Landroid/hardware/camera2/CameraDevice;
    .param p2, "i"    # I

    .prologue
    .line 439
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$800(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 440
    invoke-virtual {p1}, Landroid/hardware/camera2/CameraDevice;->close()V

    .line 441
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraEngine$CameraTwoPreviewErrorEvent;

    invoke-direct {v1, p2}, Lcom/commonsware/cwac/cam2/CameraEngine$CameraTwoPreviewErrorEvent;-><init>(I)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 442
    return-void
.end method

.method public onOpened(Landroid/hardware/camera2/CameraDevice;)V
    .locals 7
    .param p1, "cameraDevice"    # Landroid/hardware/camera2/CameraDevice;

    .prologue
    .line 414
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;
    invoke-static {v2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$800(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/concurrent/Semaphore;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    .line 415
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iput-object p1, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->cameraDevice:Landroid/hardware/camera2/CameraDevice;

    .line 416
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->buildImageReader()Landroid/media/ImageReader;

    move-result-object v3

    iput-object v3, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->reader:Landroid/media/ImageReader;

    .line 418
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .line 420
    .local v0, "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    # invokes: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->setDevice(Landroid/hardware/camera2/CameraDevice;)V
    invoke-static {v0, p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$1000(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Landroid/hardware/camera2/CameraDevice;)V

    .line 423
    const/4 v2, 0x2

    :try_start_0
    new-array v2, v2, [Landroid/view/Surface;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->surface:Landroid/view/Surface;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v4, v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->reader:Landroid/media/ImageReader;

    .line 424
    invoke-virtual {v4}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v6, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->surface:Landroid/view/Surface;

    invoke-direct {v3, v4, v5, v6}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;-><init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;Landroid/view/Surface;)V

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .line 425
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$900(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/os/Handler;

    move-result-object v4

    .line 423
    invoke-virtual {p1, v2, v3, v4}, Landroid/hardware/camera2/CameraDevice;->createCaptureSession(Ljava/util/List;Landroid/hardware/camera2/CameraCaptureSession$StateCallback;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 429
    :goto_0
    return-void

    .line 426
    :catch_0
    move-exception v1

    .line 427
    .local v1, "e":Landroid/hardware/camera2/CameraAccessException;
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v2

    new-instance v3, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;

    invoke-direct {v3, v1}, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v2, v3}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
