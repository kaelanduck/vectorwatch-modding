.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;
.super Landroid/hardware/camera2/CameraCaptureSession$StateCallback;
.source "CameraTwoEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StartPreviewTransaction"
.end annotation


# instance fields
.field private final s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

.field private final surface:Landroid/view/Surface;

.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;Landroid/view/Surface;)V
    .locals 0
    .param p2, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p3, "surface"    # Landroid/view/Surface;

    .prologue
    .line 458
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraCaptureSession$StateCallback;-><init>()V

    .line 459
    check-cast p2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .end local p2    # "session":Lcom/commonsware/cwac/cam2/CameraSession;
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .line 460
    iput-object p3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->surface:Landroid/view/Surface;

    .line 461
    return-void
.end method


# virtual methods
.method public onConfigureFailed(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 2
    .param p1, "session"    # Landroid/hardware/camera2/CameraCaptureSession;

    .prologue
    .line 505
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraEngine$CameraTwoPreviewFailureEvent;

    invoke-direct {v1}, Lcom/commonsware/cwac/cam2/CameraEngine$CameraTwoPreviewFailureEvent;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 506
    return-void
.end method

.method public onConfigured(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 6
    .param p1, "session"    # Landroid/hardware/camera2/CameraCaptureSession;

    .prologue
    .line 466
    :try_start_0
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 467
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iput-object p1, v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    .line 469
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-virtual {p1}, Landroid/hardware/camera2/CameraCaptureSession;->getDevice()Landroid/hardware/camera2/CameraDevice;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v4

    iput-object v4, v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    .line 470
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v3, v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->surface:Landroid/view/Surface;

    invoke-virtual {v3, v4}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    .line 471
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v3, v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v4, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v5, 0x4

    .line 472
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 471
    invoke-virtual {v3, v4, v5}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 473
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v3, v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v4, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v5, 0x2

    .line 474
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 473
    invoke-virtual {v3, v4, v5}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 476
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .line 477
    .local v0, "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;
    invoke-static {v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/hardware/camera2/CameraManager;

    move-result-object v3

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->cameraId:Ljava/lang/String;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$1100(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v1

    .line 479
    .local v1, "cc":Landroid/hardware/camera2/CameraCharacteristics;
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getZoomRect()Landroid/graphics/Rect;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 480
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v3, v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v4, Landroid/hardware/camera2/CaptureRequest;->SCALER_CROP_REGION:Landroid/hardware/camera2/CaptureRequest$Key;

    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .line 483
    invoke-virtual {v5}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getZoomRect()Landroid/graphics/Rect;

    move-result-object v5

    .line 482
    invoke-virtual {v3, v4, v5}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 486
    :cond_0
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v4, v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v3, v1, v4}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->addToPreviewRequest(Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureRequest$Builder;)V

    .line 488
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v4, v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v4}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v4

    iput-object v4, v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequest:Landroid/hardware/camera2/CaptureRequest;

    .line 490
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v3, v3, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequest:Landroid/hardware/camera2/CaptureRequest;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$900(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {p1, v3, v4, v5}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I

    .line 492
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v3

    new-instance v4, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;

    invoke-direct {v4}, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;-><init>()V

    invoke-virtual {v3, v4}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 501
    .end local v0    # "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    .end local v1    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    :cond_1
    :goto_0
    return-void

    .line 494
    :catch_0
    move-exception v2

    .line 495
    .local v2, "e":Landroid/hardware/camera2/CameraAccessException;
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v3

    new-instance v4, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;

    invoke-direct {v4, v2}, Lcom/commonsware/cwac/cam2/CameraEngine$OpenedEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v3, v4}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 496
    .end local v2    # "e":Landroid/hardware/camera2/CameraAccessException;
    :catch_1
    move-exception v2

    .line 497
    .local v2, "e":Ljava/lang/IllegalStateException;
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 498
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Exception resetting focus"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
