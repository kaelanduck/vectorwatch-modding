.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;
.super Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;
.source "CameraTwoEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestCaptureTransaction"
.end annotation


# instance fields
.field haveWeStartedCapture:Z

.field isWaitingForFocus:Z

.field isWaitingForPrecapture:Z

.field private final s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;)V
    .locals 2
    .param p2, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    const/4 v1, 0x0

    .line 515
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;-><init>()V

    .line 511
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->isWaitingForFocus:Z

    .line 512
    iput-boolean v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->isWaitingForPrecapture:Z

    .line 513
    iput-boolean v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->haveWeStartedCapture:Z

    .line 516
    check-cast p2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .end local p2    # "session":Lcom/commonsware/cwac/cam2/CameraSession;
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .line 517
    return-void
.end method

.method private capture(Landroid/hardware/camera2/CaptureResult;)V
    .locals 7
    .param p1, "result"    # Landroid/hardware/camera2/CaptureResult;

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 536
    iget-boolean v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->isWaitingForFocus:Z

    if-eqz v2, :cond_4

    .line 537
    iput-boolean v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->isWaitingForFocus:Z

    .line 539
    sget-object v2, Landroid/hardware/camera2/CaptureResult;->CONTROL_AF_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v2}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 541
    .local v0, "autoFocusState":I
    if-eq v6, v0, :cond_0

    if-ne v3, v0, :cond_2

    .line 543
    :cond_0
    sget-object v2, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v2}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 545
    .local v1, "state":Ljava/lang/Integer;
    if-eqz v1, :cond_1

    .line 546
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 547
    :cond_1
    iput-boolean v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->isWaitingForPrecapture:Z

    .line 548
    iput-boolean v5, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->haveWeStartedCapture:Z

    .line 549
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-direct {p0, v2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->capture(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;)V

    .line 572
    .end local v0    # "autoFocusState":I
    .end local v1    # "state":Ljava/lang/Integer;
    :cond_2
    :goto_0
    return-void

    .line 551
    .restart local v0    # "autoFocusState":I
    .restart local v1    # "state":Ljava/lang/Integer;
    :cond_3
    iput-boolean v5, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->isWaitingForPrecapture:Z

    .line 552
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-direct {p0, v2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->precapture(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;)V

    goto :goto_0

    .line 555
    .end local v0    # "autoFocusState":I
    .end local v1    # "state":Ljava/lang/Integer;
    :cond_4
    iget-boolean v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->isWaitingForPrecapture:Z

    if-eqz v2, :cond_6

    .line 556
    sget-object v2, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v2}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 558
    .restart local v1    # "state":Ljava/lang/Integer;
    if-eqz v1, :cond_5

    .line 559
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v3, :cond_5

    .line 560
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v6, :cond_2

    .line 561
    :cond_5
    iput-boolean v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->isWaitingForPrecapture:Z

    goto :goto_0

    .line 563
    .end local v1    # "state":Ljava/lang/Integer;
    :cond_6
    iget-boolean v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->haveWeStartedCapture:Z

    if-nez v2, :cond_2

    .line 564
    sget-object v2, Landroid/hardware/camera2/CaptureResult;->CONTROL_AE_STATE:Landroid/hardware/camera2/CaptureResult$Key;

    invoke-virtual {p1, v2}, Landroid/hardware/camera2/CaptureResult;->get(Landroid/hardware/camera2/CaptureResult$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 566
    .restart local v1    # "state":Ljava/lang/Integer;
    if-eqz v1, :cond_7

    .line 567
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v3, :cond_2

    .line 568
    :cond_7
    iput-boolean v5, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->haveWeStartedCapture:Z

    .line 569
    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-direct {p0, v2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->capture(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;)V

    goto :goto_0
.end method

.method private capture(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;)V
    .locals 8
    .param p1, "s"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .prologue
    .line 591
    :try_start_0
    iget-object v4, p1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->cameraDevice:Landroid/hardware/camera2/CameraDevice;

    const/4 v5, 0x2

    .line 592
    invoke-virtual {v4, v5}, Landroid/hardware/camera2/CameraDevice;->createCaptureRequest(I)Landroid/hardware/camera2/CaptureRequest$Builder;

    move-result-object v1

    .line 594
    .local v1, "captureBuilder":Landroid/hardware/camera2/CaptureRequest$Builder;
    iget-object v4, p1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->reader:Landroid/media/ImageReader;

    invoke-virtual {v4}, Landroid/media/ImageReader;->getSurface()Landroid/view/Surface;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/hardware/camera2/CaptureRequest$Builder;->addTarget(Landroid/view/Surface;)V

    .line 595
    sget-object v4, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v5, 0x4

    .line 596
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 595
    invoke-virtual {v1, v4, v5}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 597
    sget-object v4, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v5, 0x2

    .line 598
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 597
    invoke-virtual {v1, v4, v5}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 600
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .line 601
    .local v0, "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;
    invoke-static {v4}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/hardware/camera2/CameraManager;

    move-result-object v4

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->cameraId:Ljava/lang/String;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$1100(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v2

    .line 603
    .local v2, "cc":Landroid/hardware/camera2/CameraCharacteristics;
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getZoomRect()Landroid/graphics/Rect;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 604
    sget-object v4, Landroid/hardware/camera2/CaptureRequest;->SCALER_CROP_REGION:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 606
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->getZoomRect()Landroid/graphics/Rect;

    move-result-object v5

    .line 605
    invoke-virtual {v1, v4, v5}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 609
    :cond_0
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->isFacingFront:Z
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$1300(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;)Z

    move-result v4

    invoke-virtual {p1, v2, v4, v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->addToCaptureRequest(Landroid/hardware/camera2/CameraCharacteristics;ZLandroid/hardware/camera2/CaptureRequest$Builder;)V

    .line 611
    iget-object v4, p1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v4}, Landroid/hardware/camera2/CameraCaptureSession;->stopRepeating()V

    .line 612
    iget-object v4, p1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v5

    new-instance v6, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;

    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-direct {v6, v7, p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;-><init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;)V

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 621
    .end local v0    # "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    .end local v1    # "captureBuilder":Landroid/hardware/camera2/CaptureRequest$Builder;
    .end local v2    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    :cond_1
    :goto_0
    return-void

    .line 614
    :catch_0
    move-exception v3

    .line 615
    .local v3, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v4

    new-instance v5, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    invoke-direct {v5, v3}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v4, v5}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 617
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->isDebug()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 618
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Exception running capture"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private precapture(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;)V
    .locals 4
    .param p1, "s"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .prologue
    .line 576
    :try_start_0
    iget-object v1, p1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v2, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_PRECAPTURE_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v3, 0x1

    .line 577
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 576
    invoke-virtual {v1, v2, v3}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 578
    iget-object v1, p1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v2, p1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .line 579
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$900(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/os/Handler;

    move-result-object v3

    .line 578
    invoke-virtual {v1, v2, p0, v3}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    :cond_0
    :goto_0
    return-void

    .line 580
    :catch_0
    move-exception v0

    .line 581
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v1

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    invoke-direct {v2, v0}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v1, v2}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 583
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 584
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception running precapture"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public onCaptureCompleted(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
    .locals 0
    .param p1, "session"    # Landroid/hardware/camera2/CameraCaptureSession;
    .param p2, "request"    # Landroid/hardware/camera2/CaptureRequest;
    .param p3, "result"    # Landroid/hardware/camera2/TotalCaptureResult;

    .prologue
    .line 532
    invoke-direct {p0, p3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->capture(Landroid/hardware/camera2/CaptureResult;)V

    .line 533
    return-void
.end method

.method public onCaptureFailed(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureFailure;)V
    .locals 4
    .param p1, "session"    # Landroid/hardware/camera2/CameraCaptureSession;
    .param p2, "request"    # Landroid/hardware/camera2/CaptureRequest;
    .param p3, "failure"    # Landroid/hardware/camera2/CaptureFailure;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "generic camera2 capture failure"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 528
    return-void
.end method

.method public onCaptureProgressed(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureResult;)V
    .locals 0
    .param p1, "session"    # Landroid/hardware/camera2/CameraCaptureSession;
    .param p2, "request"    # Landroid/hardware/camera2/CaptureRequest;
    .param p3, "partialResult"    # Landroid/hardware/camera2/CaptureResult;

    .prologue
    .line 522
    invoke-direct {p0, p3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;->capture(Landroid/hardware/camera2/CaptureResult;)V

    .line 523
    return-void
.end method
