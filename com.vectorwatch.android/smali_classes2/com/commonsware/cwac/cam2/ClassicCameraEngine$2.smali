.class Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;
.super Ljava/lang/Object;
.source "ClassicCameraEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->takePicture(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/PictureTransaction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

.field final synthetic val$session:Lcom/commonsware/cwac/cam2/CameraSession;

.field final synthetic val$xact:Lcom/commonsware/cwac/cam2/PictureTransaction;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/PictureTransaction;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iput-object p2, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    iput-object p3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->val$xact:Lcom/commonsware/cwac/cam2/PictureTransaction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 168
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;

    .line 169
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;
    # invokes: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->getCamera()Landroid/hardware/Camera;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;->access$600(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$Descriptor;)Landroid/hardware/Camera;

    move-result-object v0

    .line 171
    .local v0, "camera":Landroid/hardware/Camera;
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->savePreviewFile()Ljava/io/File;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 172
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v0, v4}, Landroid/hardware/Camera;->setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 174
    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v3

    .line 176
    .local v3, "parameters":Landroid/hardware/Camera$Parameters;
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v5

    iget v5, v5, Landroid/hardware/Camera$Size;->width:I

    # setter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewWidth:I
    invoke-static {v4, v5}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$802(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;I)I

    .line 177
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v5

    iget v5, v5, Landroid/hardware/Camera$Size;->height:I

    # setter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewHeight:I
    invoke-static {v4, v5}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$902(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;I)I

    .line 178
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v3}, Landroid/hardware/Camera$Parameters;->getPreviewFormat()I

    move-result v5

    # setter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->previewFormat:I
    invoke-static {v4, v5}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->access$1002(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;I)I

    .line 182
    .end local v3    # "parameters":Landroid/hardware/Camera$Parameters;
    :cond_0
    :try_start_0
    new-instance v4, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2$1;

    invoke-direct {v4, p0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2$1;-><init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;)V

    const/4 v5, 0x0

    new-instance v6, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

    iget-object v7, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    iget-object v8, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->val$session:Lcom/commonsware/cwac/cam2/CameraSession;

    .line 188
    invoke-virtual {v8}, Lcom/commonsware/cwac/cam2/CameraSession;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->val$xact:Lcom/commonsware/cwac/cam2/PictureTransaction;

    invoke-direct {v6, v7, v8, v9}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;-><init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine;Landroid/content/Context;Lcom/commonsware/cwac/cam2/PictureTransaction;)V

    .line 182
    invoke-virtual {v0, v4, v5, v6}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :cond_1
    :goto_0
    return-void

    .line 189
    :catch_0
    move-exception v2

    .line 190
    .local v2, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v4

    new-instance v5, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    invoke-direct {v5, v2}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v4, v5}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 192
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$2;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v4}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->isDebug()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 193
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Exception taking picture"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
