.class public Lcom/commonsware/cwac/cam2/CameraSession;
.super Ljava/lang/Object;
.source "CameraSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/CameraSession$Builder;
    }
.end annotation


# instance fields
.field private ctxt:Landroid/content/Context;

.field private currentFlashMode:Lcom/commonsware/cwac/cam2/FlashMode;

.field private final descriptor:Lcom/commonsware/cwac/cam2/CameraDescriptor;

.field private final plugins:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/CameraPlugin;",
            ">;"
        }
    .end annotation
.end field

.field private previewSize:Lcom/commonsware/cwac/cam2/util/Size;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "descriptor"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession;->plugins:Ljava/util/ArrayList;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession;->ctxt:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraSession;->descriptor:Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/commonsware/cwac/cam2/CameraSession;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession;->plugins:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraSession;->getPlugins()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraPlugin;

    .line 78
    .local v0, "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    invoke-interface {v0}, Lcom/commonsware/cwac/cam2/CameraPlugin;->destroy()V

    goto :goto_0

    .line 80
    .end local v0    # "plugin":Lcom/commonsware/cwac/cam2/CameraPlugin;
    :cond_0
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession;->ctxt:Landroid/content/Context;

    return-object v0
.end method

.method public getCurrentFlashMode()Lcom/commonsware/cwac/cam2/FlashMode;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession;->currentFlashMode:Lcom/commonsware/cwac/cam2/FlashMode;

    return-object v0
.end method

.method public getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession;->descriptor:Lcom/commonsware/cwac/cam2/CameraDescriptor;

    return-object v0
.end method

.method protected getPlugins()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/CameraPlugin;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession;->plugins:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPreviewSize()Lcom/commonsware/cwac/cam2/util/Size;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    return-object v0
.end method

.method setCurrentFlashMode(Lcom/commonsware/cwac/cam2/FlashMode;)V
    .locals 0
    .param p1, "currentFlashMode"    # Lcom/commonsware/cwac/cam2/FlashMode;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraSession;->currentFlashMode:Lcom/commonsware/cwac/cam2/FlashMode;

    .line 96
    return-void
.end method

.method public setPreviewSize(Lcom/commonsware/cwac/cam2/util/Size;)V
    .locals 0
    .param p1, "previewSize"    # Lcom/commonsware/cwac/cam2/util/Size;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraSession;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    .line 88
    return-void
.end method
