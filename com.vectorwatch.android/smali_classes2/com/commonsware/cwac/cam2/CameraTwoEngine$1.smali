.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;
.super Ljava/lang/Object;
.source "CameraTwoEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;->loadCameraDescriptors(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

.field final synthetic val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .prologue
    .line 103
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->descriptors:Ljava/util/List;
    invoke-static {v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$100(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/List;

    move-result-object v12

    if-nez v12, :cond_4

    .line 104
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .local v9, "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;
    invoke-static {v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/hardware/camera2/CameraManager;

    move-result-object v12

    invoke-virtual {v12}, Landroid/hardware/camera2/CameraManager;->getCameraIdList()[Ljava/lang/String;

    move-result-object v14

    array-length v15, v14

    const/4 v12, 0x0

    move v13, v12

    :goto_0
    if-ge v13, v15, :cond_3

    aget-object v3, v14, v13

    .line 108
    .local v3, "cameraId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .line 109
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;
    invoke-static {v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/hardware/camera2/CameraManager;

    move-result-object v12

    invoke-virtual {v12, v3}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v4

    .line 110
    .local v4, "cc":Landroid/hardware/camera2/CameraCharacteristics;
    new-instance v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    const/4 v12, 0x0

    invoke-direct {v2, v3, v4, v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;-><init>(Ljava/lang/String;Landroid/hardware/camera2/CameraCharacteristics;Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;)V

    .line 111
    .local v2, "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    sget-object v12, Landroid/hardware/camera2/CameraCharacteristics;->SCALER_STREAM_CONFIGURATION_MAP:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v4, v12}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/camera2/params/StreamConfigurationMap;

    .line 113
    .local v6, "map":Landroid/hardware/camera2/params/StreamConfigurationMap;
    const-class v12, Landroid/graphics/SurfaceTexture;

    .line 114
    invoke-virtual {v6, v12}, Landroid/hardware/camera2/params/StreamConfigurationMap;->getOutputSizes(Ljava/lang/Class;)[Landroid/util/Size;

    move-result-object v7

    .line 115
    .local v7, "rawSizes":[Landroid/util/Size;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v11, "sizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/commonsware/cwac/cam2/util/Size;>;"
    array-length v0, v7

    move/from16 v16, v0

    const/4 v12, 0x0

    :goto_1
    move/from16 v0, v16

    if-ge v12, v0, :cond_1

    aget-object v10, v7, v12

    .line 118
    .local v10, "size":Landroid/util/Size;
    invoke-virtual {v10}, Landroid/util/Size;->getWidth()I

    move-result v17

    const/16 v18, 0x870

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    .line 119
    invoke-virtual {v10}, Landroid/util/Size;->getHeight()I

    move-result v17

    const/16 v18, 0x870

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    .line 120
    new-instance v17, Lcom/commonsware/cwac/cam2/util/Size;

    .line 121
    invoke-virtual {v10}, Landroid/util/Size;->getWidth()I

    move-result v18

    invoke-virtual {v10}, Landroid/util/Size;->getHeight()I

    move-result v19

    invoke-direct/range {v17 .. v19}, Lcom/commonsware/cwac/cam2/util/Size;-><init>(II)V

    .line 120
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 125
    .end local v10    # "size":Landroid/util/Size;
    :cond_1
    # invokes: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->setPreviewSizes(Ljava/util/ArrayList;)V
    invoke-static {v2, v11}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$400(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Ljava/util/ArrayList;)V

    .line 126
    const/16 v12, 0x100

    .line 127
    invoke-virtual {v6, v12}, Landroid/hardware/camera2/params/StreamConfigurationMap;->getOutputSizes(I)[Landroid/util/Size;

    move-result-object v12

    .line 126
    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    # invokes: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->setPictureSizes(Ljava/util/List;)V
    invoke-static {v2, v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$500(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Ljava/util/List;)V

    .line 128
    sget-object v12, Landroid/hardware/camera2/CameraCharacteristics;->LENS_FACING:Landroid/hardware/camera2/CameraCharacteristics$Key;

    .line 129
    invoke-virtual {v4, v12}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    if-nez v12, :cond_2

    const/4 v12, 0x1

    .line 128
    :goto_2
    # invokes: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->setFacingFront(Z)V
    invoke-static {v2, v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$600(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Z)V

    .line 131
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    add-int/lit8 v12, v13, 0x1

    move v13, v12

    goto/16 :goto_0

    .line 129
    :cond_2
    const/4 v12, 0x0

    goto :goto_2

    .line 134
    .end local v2    # "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    .end local v3    # "cameraId":Ljava/lang/String;
    .end local v4    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    .end local v6    # "map":Landroid/hardware/camera2/params/StreamConfigurationMap;
    .end local v7    # "rawSizes":[Landroid/util/Size;
    .end local v11    # "sizes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/commonsware/cwac/cam2/util/Size;>;"
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # setter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->descriptors:Ljava/util/List;
    invoke-static {v12, v9}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$102(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    .end local v9    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;>;"
    :cond_4
    :goto_3
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v8, "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/CameraDescriptor;>;"
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->descriptors:Ljava/util/List;
    invoke-static {v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$100(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_5
    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .line 150
    .restart local v2    # "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    invoke-virtual {v13}, Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;->getFacingExactMatch()Z

    move-result v13

    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->val$criteria:Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .line 151
    # invokes: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->getScore(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I
    invoke-static {v2, v13}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$700(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)I

    move-result v13

    if-lez v13, :cond_5

    .line 152
    :cond_6
    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 135
    .end local v2    # "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    .end local v8    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/CameraDescriptor;>;"
    .restart local v9    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;>;"
    :catch_0
    move-exception v5

    .line 136
    .local v5, "e":Landroid/hardware/camera2/CameraAccessException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v12

    new-instance v13, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;

    invoke-direct {v13, v5}, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v12, v13}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 139
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->isDebug()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 140
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "Exception accessing camera"

    invoke-static {v12, v13, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 156
    .end local v5    # "e":Landroid/hardware/camera2/CameraAccessException;
    .end local v9    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;>;"
    .restart local v8    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/commonsware/cwac/cam2/CameraDescriptor;>;"
    :cond_7
    new-instance v12, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1$1;-><init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;)V

    invoke-static {v8, v12}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 171
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v12}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v12

    new-instance v13, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;

    invoke-direct {v13, v8}, Lcom/commonsware/cwac/cam2/CameraEngine$CameraDescriptorsEvent;-><init>(Ljava/util/List;)V

    invoke-virtual {v12, v13}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 173
    return-void
.end method
