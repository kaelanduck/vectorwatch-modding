.class Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;
.super Lcom/commonsware/cwac/cam2/SimpleCameraTwoConfigurator;
.source "FocusModePlugin.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Two"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/SimpleCameraTwoConfigurator;-><init>()V

    return-void
.end method

.method private getDesiredFocusMode(Landroid/hardware/camera2/CameraCharacteristics;)I
    .locals 5
    .param p1, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;

    .prologue
    .line 170
    sget-object v3, Landroid/hardware/camera2/CameraCharacteristics;->CONTROL_AF_AVAILABLE_MODES:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {p1, v3}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 173
    .local v1, "availModes":[I
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->focusMode:Lcom/commonsware/cwac/cam2/FocusMode;
    invoke-static {v3}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->access$100(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)Lcom/commonsware/cwac/cam2/FocusMode;

    move-result-object v3

    sget-object v4, Lcom/commonsware/cwac/cam2/FocusMode;->OFF:Lcom/commonsware/cwac/cam2/FocusMode;

    if-ne v3, v4, :cond_2

    .line 174
    const/4 v2, 0x0

    .line 183
    .local v2, "desiredMode":I
    :goto_0
    const-string v3, "Sony"

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "C6603"

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 184
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "D5803"

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 185
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "C6802"

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 186
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 187
    :cond_0
    const/4 v2, 0x0

    .line 194
    :cond_1
    :goto_1
    array-length v4, v1

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_8

    aget v0, v1, v3

    .line 195
    .local v0, "availMode":I
    if-ne v0, v2, :cond_7

    .line 200
    .end local v0    # "availMode":I
    .end local v2    # "desiredMode":I
    :goto_3
    return v2

    .line 175
    :cond_2
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->focusMode:Lcom/commonsware/cwac/cam2/FocusMode;
    invoke-static {v3}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->access$100(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)Lcom/commonsware/cwac/cam2/FocusMode;

    move-result-object v3

    sget-object v4, Lcom/commonsware/cwac/cam2/FocusMode;->EDOF:Lcom/commonsware/cwac/cam2/FocusMode;

    if-ne v3, v4, :cond_3

    .line 176
    const/4 v2, 0x5

    .restart local v2    # "desiredMode":I
    goto :goto_0

    .line 177
    .end local v2    # "desiredMode":I
    :cond_3
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->isVideo:Z
    invoke-static {v3}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->access$200(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 178
    const/4 v2, 0x3

    .restart local v2    # "desiredMode":I
    goto :goto_0

    .line 180
    .end local v2    # "desiredMode":I
    :cond_4
    const/4 v2, 0x4

    .restart local v2    # "desiredMode":I
    goto :goto_0

    .line 188
    :cond_5
    const-string v3, "htc"

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "volantis"

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 189
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "volantisg"

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 190
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 191
    :cond_6
    const/4 v2, 0x0

    goto :goto_1

    .line 194
    .restart local v0    # "availMode":I
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 200
    .end local v0    # "availMode":I
    :cond_8
    const/4 v2, -0x1

    goto :goto_3
.end method


# virtual methods
.method public addToCaptureRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;ZLandroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 3
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p3, "facingFront"    # Z
    .param p4, "captureBuilder"    # Landroid/hardware/camera2/CaptureRequest$Builder;

    .prologue
    .line 159
    invoke-direct {p0, p2}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;->getDesiredFocusMode(Landroid/hardware/camera2/CameraCharacteristics;)I

    move-result v0

    .line 161
    .local v0, "desiredMode":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 162
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 163
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 162
    invoke-virtual {p4, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_0
    const-string v1, "CWAC-Cam2"

    const-string v2, "no support for requested focus mode"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addToPreviewRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 3
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p3, "captureBuilder"    # Landroid/hardware/camera2/CaptureRequest$Builder;

    .prologue
    .line 144
    invoke-direct {p0, p2}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;->getDesiredFocusMode(Landroid/hardware/camera2/CameraCharacteristics;)I

    move-result v0

    .line 146
    .local v0, "desiredMode":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 147
    sget-object v1, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 148
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 147
    invoke-virtual {p3, v1, v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 152
    :goto_0
    return-void

    .line 150
    :cond_0
    const-string v1, "CWAC-Cam2"

    const-string v2, "no support for requested focus mode"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
