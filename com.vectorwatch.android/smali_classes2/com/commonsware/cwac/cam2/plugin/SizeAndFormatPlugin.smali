.class public Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;
.super Ljava/lang/Object;
.source "SizeAndFormatPlugin.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraPlugin;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Two;,
        Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;
    }
.end annotation


# instance fields
.field private final pictureFormat:I

.field private final pictureSize:Lcom/commonsware/cwac/cam2/util/Size;

.field private final previewSize:Lcom/commonsware/cwac/cam2/util/Size;


# direct methods
.method public constructor <init>(Lcom/commonsware/cwac/cam2/util/Size;Lcom/commonsware/cwac/cam2/util/Size;I)V
    .locals 0
    .param p1, "previewSize"    # Lcom/commonsware/cwac/cam2/util/Size;
    .param p2, "pictureSize"    # Lcom/commonsware/cwac/cam2/util/Size;
    .param p3, "pictureFormat"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    .line 54
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureSize:Lcom/commonsware/cwac/cam2/util/Size;

    .line 55
    iput p3, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureFormat:I

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)Lcom/commonsware/cwac/cam2/util/Size;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    return-object v0
.end method

.method static synthetic access$100(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)Lcom/commonsware/cwac/cam2/util/Size;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureSize:Lcom/commonsware/cwac/cam2/util/Size;

    return-object v0
.end method

.method static synthetic access$200(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)I
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    .prologue
    .line 38
    iget v0, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureFormat:I

    return v0
.end method


# virtual methods
.method public buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/commonsware/cwac/cam2/CameraConfigurator;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const-class v0, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;

    if-ne p1, v0, :cond_0

    .line 64
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;-><init>(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraConfigurator;

    .line 67
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Two;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Two;-><init>(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraConfigurator;

    goto :goto_0
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public validate(Lcom/commonsware/cwac/cam2/CameraSession;)V
    .locals 2
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 75
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v0

    invoke-interface {v0}, Lcom/commonsware/cwac/cam2/CameraDescriptor;->getPreviewSizes()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Requested preview size is not one that the camera supports"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v0

    invoke-interface {v0}, Lcom/commonsware/cwac/cam2/CameraDescriptor;->getPictureSizes()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureSize:Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Requested picture size is not one that the camera supports"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    return-void
.end method
