.class Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Classic;
.super Lcom/commonsware/cwac/cam2/SimpleClassicCameraConfigurator;
.source "FlashModePlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Classic"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/SimpleClassicCameraConfigurator;-><init>()V

    return-void
.end method


# virtual methods
.method public configureStillCamera(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/Camera$CameraInfo;Landroid/hardware/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 1
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "info"    # Landroid/hardware/Camera$CameraInfo;
    .param p3, "camera"    # Landroid/hardware/Camera;
    .param p4, "params"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 71
    if-eqz p4, :cond_0

    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getCurrentFlashMode()Lcom/commonsware/cwac/cam2/FlashMode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getCurrentFlashMode()Lcom/commonsware/cwac/cam2/FlashMode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/FlashMode;->getClassicMode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 75
    :cond_0
    return-object p4
.end method
