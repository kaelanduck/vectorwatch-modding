.class Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$1;
.super Landroid/view/OrientationEventListener;
.source "OrientationPlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$1;->this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$1;->this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->lastOrientation:I
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->access$000(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 56
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;

    invoke-direct {v1}, Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;-><init>()V

    .line 57
    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$1;->this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    # setter for: Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->lastOrientation:I
    invoke-static {v0, p1}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->access$002(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;I)I

    .line 61
    return-void
.end method
