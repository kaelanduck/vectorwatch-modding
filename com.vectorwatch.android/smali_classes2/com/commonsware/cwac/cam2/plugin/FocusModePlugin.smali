.class public Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;
.super Ljava/lang/Object;
.source "FocusModePlugin.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraPlugin;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;,
        Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Classic;
    }
.end annotation


# instance fields
.field private final ctxt:Landroid/content/Context;

.field private final focusMode:Lcom/commonsware/cwac/cam2/FocusMode;

.field private final isVideo:Z

.field private lastOrientation:I

.field private orientationEventListener:Landroid/view/OrientationEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/FocusMode;Z)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "focusMode"    # Lcom/commonsware/cwac/cam2/FocusMode;
    .param p3, "isVideo"    # Z

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->lastOrientation:I

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->ctxt:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->focusMode:Lcom/commonsware/cwac/cam2/FocusMode;

    .line 50
    iput-boolean p3, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->isVideo:Z

    .line 52
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$1;

    invoke-direct {v0, p0, p1}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$1;-><init>(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    .line 59
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->canDetectOrientation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;I)I
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;
    .param p1, "x1"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->lastOrientation:I

    return p1
.end method

.method static synthetic access$100(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)Lcom/commonsware/cwac/cam2/FocusMode;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->focusMode:Lcom/commonsware/cwac/cam2/FocusMode;

    return-object v0
.end method

.method static synthetic access$200(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)Z
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->isVideo:Z

    return v0
.end method


# virtual methods
.method public buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/commonsware/cwac/cam2/CameraConfigurator;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 72
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const-class v0, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;

    if-ne p1, v0, :cond_0

    .line 73
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Classic;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Classic;-><init>(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraConfigurator;

    .line 76
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Two;-><init>(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraConfigurator;

    goto :goto_0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    .line 96
    :cond_0
    return-void
.end method

.method public validate(Lcom/commonsware/cwac/cam2/CameraSession;)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 85
    return-void
.end method
