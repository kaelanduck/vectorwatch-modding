.class Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Two;
.super Lcom/commonsware/cwac/cam2/SimpleCameraTwoConfigurator;
.source "OrientationPlugin.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Two"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/SimpleCameraTwoConfigurator;-><init>()V

    return-void
.end method


# virtual methods
.method public addToCaptureRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;ZLandroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 5
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p3, "facingFront"    # Z
    .param p4, "captureBuilder"    # Landroid/hardware/camera2/CaptureRequest$Builder;

    .prologue
    .line 255
    const/4 v1, 0x0

    .line 257
    .local v1, "pictureOrientation":I
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->lastOrientation:I
    invoke-static {v3}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->access$000(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 258
    sget-object v3, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_ORIENTATION:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {p2, v3}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 259
    .local v2, "sensorOrientation":I
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->lastOrientation:I
    invoke-static {v3}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->access$000(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)I

    move-result v3

    add-int/lit8 v3, v3, 0x2d

    div-int/lit8 v3, v3, 0x5a

    mul-int/lit8 v0, v3, 0x5a

    .line 261
    .local v0, "deviceOrientation":I
    if-eqz p3, :cond_0

    .line 262
    neg-int v0, v0

    .line 265
    :cond_0
    add-int v3, v2, v0

    add-int/lit16 v3, v3, 0x168

    rem-int/lit16 v1, v3, 0x168

    .line 268
    .end local v0    # "deviceOrientation":I
    .end local v2    # "sensorOrientation":I
    :cond_1
    sget-object v3, Landroid/hardware/camera2/CaptureRequest;->JPEG_ORIENTATION:Landroid/hardware/camera2/CaptureRequest$Key;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v3, v4}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 269
    return-void
.end method
