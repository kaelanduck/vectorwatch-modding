.class public Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;
.super Ljava/lang/Object;
.source "FlashModePlugin.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraPlugin;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Two;,
        Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Classic;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    return-void
.end method


# virtual methods
.method public buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/commonsware/cwac/cam2/CameraConfigurator;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const-class v0, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;

    if-ne p1, v0, :cond_0

    .line 40
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Classic;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Classic;-><init>(Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraConfigurator;

    .line 43
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Two;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Two;-><init>(Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraConfigurator;

    goto :goto_0
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public validate(Lcom/commonsware/cwac/cam2/CameraSession;)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 52
    return-void
.end method
