.class Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Classic;
.super Lcom/commonsware/cwac/cam2/SimpleClassicCameraConfigurator;
.source "FocusModePlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Classic"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/SimpleClassicCameraConfigurator;-><init>()V

    return-void
.end method


# virtual methods
.method public configureStillCamera(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/Camera$CameraInfo;Landroid/hardware/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 3
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "info"    # Landroid/hardware/Camera$CameraInfo;
    .param p3, "camera"    # Landroid/hardware/Camera;
    .param p4, "params"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 107
    if-eqz p4, :cond_0

    .line 108
    const/4 v0, 0x0

    .line 110
    .local v0, "desiredMode":Ljava/lang/String;
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->focusMode:Lcom/commonsware/cwac/cam2/FocusMode;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->access$100(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)Lcom/commonsware/cwac/cam2/FocusMode;

    move-result-object v1

    sget-object v2, Lcom/commonsware/cwac/cam2/FocusMode;->OFF:Lcom/commonsware/cwac/cam2/FocusMode;

    if-ne v1, v2, :cond_1

    .line 111
    const-string v0, "fixed"

    .line 123
    :goto_0
    invoke-virtual {p4}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 124
    invoke-virtual {p4, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 131
    .end local v0    # "desiredMode":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object p4

    .line 112
    .restart local v0    # "desiredMode":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->focusMode:Lcom/commonsware/cwac/cam2/FocusMode;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->access$100(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)Lcom/commonsware/cwac/cam2/FocusMode;

    move-result-object v1

    sget-object v2, Lcom/commonsware/cwac/cam2/FocusMode;->EDOF:Lcom/commonsware/cwac/cam2/FocusMode;

    if-ne v1, v2, :cond_2

    .line 114
    const-string v0, "edof"

    goto :goto_0

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->isVideo:Z
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;->access$200(Lcom/commonsware/cwac/cam2/plugin/FocusModePlugin;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 116
    const-string v0, "continuous-video"

    goto :goto_0

    .line 119
    :cond_3
    const-string v0, "continuous-picture"

    goto :goto_0

    .line 126
    :cond_4
    const-string v1, "CWAC-Cam2"

    const-string v2, "no support for requested focus mode"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
