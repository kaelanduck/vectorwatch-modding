.class public Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;
.super Ljava/lang/Object;
.source "OrientationPlugin.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraPlugin;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Two;,
        Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Classic;
    }
.end annotation


# instance fields
.field private final ctxt:Landroid/content/Context;

.field private lastOrientation:I

.field private orientationEventListener:Landroid/view/OrientationEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->lastOrientation:I

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->ctxt:Landroid/content/Context;

    .line 51
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$1;

    invoke-direct {v0, p0, p1}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$1;-><init>(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    .line 64
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->canDetectOrientation()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 70
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)I
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    .prologue
    .line 43
    iget v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->lastOrientation:I

    return v0
.end method

.method static synthetic access$002(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;I)I
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->lastOrientation:I

    return p1
.end method

.method static synthetic access$100(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->ctxt:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/commonsware/cwac/cam2/CameraConfigurator;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const-class v0, Lcom/commonsware/cwac/cam2/ClassicCameraConfigurator;

    if-ne p1, v0, :cond_0

    .line 78
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Classic;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Classic;-><init>(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraConfigurator;

    .line 81
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Two;

    invoke-direct {v0, p0}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Two;-><init>(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)V

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraConfigurator;

    goto :goto_0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->orientationEventListener:Landroid/view/OrientationEventListener;

    .line 101
    :cond_0
    return-void
.end method

.method public validate(Lcom/commonsware/cwac/cam2/CameraSession;)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 90
    return-void
.end method
