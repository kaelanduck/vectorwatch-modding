.class Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Two;
.super Lcom/commonsware/cwac/cam2/SimpleCameraTwoConfigurator;
.source "FlashModePlugin.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Two"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/FlashModePlugin;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/SimpleCameraTwoConfigurator;-><init>()V

    return-void
.end method


# virtual methods
.method public addToCaptureRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;ZLandroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 2
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p3, "facingFront"    # Z
    .param p4, "captureBuilder"    # Landroid/hardware/camera2/CaptureRequest$Builder;

    .prologue
    .line 89
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getCurrentFlashMode()Lcom/commonsware/cwac/cam2/FlashMode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 90
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 91
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getCurrentFlashMode()Lcom/commonsware/cwac/cam2/FlashMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/FlashMode;->getCameraTwoMode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 90
    invoke-virtual {p4, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 93
    :cond_0
    return-void
.end method

.method public addToPreviewRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 2
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p3, "captureBuilder"    # Landroid/hardware/camera2/CaptureRequest$Builder;

    .prologue
    .line 99
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getCurrentFlashMode()Lcom/commonsware/cwac/cam2/FlashMode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 101
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getCurrentFlashMode()Lcom/commonsware/cwac/cam2/FlashMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/FlashMode;->getCameraTwoMode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 100
    invoke-virtual {p3, v0, v1}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 103
    :cond_0
    return-void
.end method
