.class Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;
.super Lcom/commonsware/cwac/cam2/SimpleClassicCameraConfigurator;
.source "SizeAndFormatPlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Classic"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/SimpleClassicCameraConfigurator;-><init>()V

    return-void
.end method

.method private getHigh()I
    .locals 2

    .prologue
    .line 137
    const-string v0, "LGE"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "g3_tmo_us"

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 138
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x4

    .line 142
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public configureRecorder(Lcom/commonsware/cwac/cam2/CameraSession;ILcom/commonsware/cwac/cam2/VideoTransaction;Landroid/media/MediaRecorder;)V
    .locals 6
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cameraId"    # I
    .param p3, "xact"    # Lcom/commonsware/cwac/cam2/VideoTransaction;
    .param p4, "recorder"    # Landroid/media/MediaRecorder;

    .prologue
    const/4 v5, 0x0

    .line 117
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;->getHigh()I

    move-result v2

    .line 119
    .local v2, "highProfile":I
    invoke-static {p2, v2}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v0

    .line 121
    .local v0, "canGoHigh":Z
    invoke-static {p2, v5}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v1

    .line 124
    .local v1, "canGoLow":Z
    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lcom/commonsware/cwac/cam2/VideoTransaction;->getQuality()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    if-nez v1, :cond_1

    .line 125
    :cond_0
    invoke-static {p2, v2}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    move-result-object v3

    invoke-virtual {p4, v3}, Landroid/media/MediaRecorder;->setProfile(Landroid/media/CamcorderProfile;)V

    .line 134
    :goto_0
    return-void

    .line 127
    :cond_1
    if-eqz v1, :cond_2

    .line 128
    invoke-static {p2, v5}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    move-result-object v3

    invoke-virtual {p4, v3}, Landroid/media/MediaRecorder;->setProfile(Landroid/media/CamcorderProfile;)V

    goto :goto_0

    .line 131
    :cond_2
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "cannot find valid CamcorderProfile"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public configureStillCamera(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/Camera$CameraInfo;Landroid/hardware/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 2
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "info"    # Landroid/hardware/Camera$CameraInfo;
    .param p3, "camera"    # Landroid/hardware/Camera;
    .param p4, "params"    # Landroid/hardware/Camera$Parameters;

    .prologue
    .line 101
    if-eqz p4, :cond_0

    .line 102
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->access$000(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v0

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    .line 103
    # getter for: Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->previewSize:Lcom/commonsware/cwac/cam2/util/Size;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->access$000(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v1

    .line 102
    invoke-virtual {p4, v0, v1}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 104
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureSize:Lcom/commonsware/cwac/cam2/util/Size;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->access$100(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v0

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    .line 105
    # getter for: Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureSize:Lcom/commonsware/cwac/cam2/util/Size;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->access$100(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v1

    .line 104
    invoke-virtual {p4, v0, v1}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 106
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureFormat:I
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->access$200(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/hardware/Camera$Parameters;->setPictureFormat(I)V

    .line 109
    :cond_0
    return-object p4
.end method
