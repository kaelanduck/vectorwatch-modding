.class Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Classic;
.super Lcom/commonsware/cwac/cam2/SimpleClassicCameraConfigurator;
.source "OrientationPlugin.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Classic"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/SimpleClassicCameraConfigurator;-><init>()V

    return-void
.end method

.method private getDisplayOrientation(Landroid/hardware/Camera$CameraInfo;Z)I
    .locals 6
    .param p1, "info"    # Landroid/hardware/Camera$CameraInfo;
    .param p2, "isStillCapture"    # Z

    .prologue
    .line 207
    iget-object v4, p0, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Classic;->this$0:Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->ctxt:Landroid/content/Context;
    invoke-static {v4}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;->access$100(Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 208
    .local v2, "mgr":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v3

    .line 209
    .local v3, "rotation":I
    const/4 v0, 0x0

    .line 211
    .local v0, "degrees":I
    packed-switch v3, :pswitch_data_0

    .line 228
    :goto_0
    iget v4, p1, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 229
    iget v4, p1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v4, v0

    rem-int/lit16 v1, v4, 0x168

    .line 230
    .local v1, "displayOrientation":I
    rsub-int v4, v1, 0x168

    rem-int/lit16 v1, v4, 0x168

    .line 232
    if-nez p2, :cond_0

    const/16 v4, 0x5a

    if-ne v1, v4, :cond_0

    .line 233
    const/16 v1, 0x10e

    .line 239
    :cond_0
    :goto_1
    return v1

    .line 213
    .end local v1    # "displayOrientation":I
    :pswitch_0
    const/4 v0, 0x0

    .line 214
    goto :goto_0

    .line 216
    :pswitch_1
    const/16 v0, 0x5a

    .line 217
    goto :goto_0

    .line 219
    :pswitch_2
    const/16 v0, 0xb4

    .line 220
    goto :goto_0

    .line 222
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 236
    :cond_1
    iget v4, p1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v4, v0

    add-int/lit16 v4, v4, 0x168

    rem-int/lit16 v1, v4, 0x168

    .restart local v1    # "displayOrientation":I
    goto :goto_1

    .line 211
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private useAltAlgorithm()Z
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public configureRecorder(Lcom/commonsware/cwac/cam2/CameraSession;ILcom/commonsware/cwac/cam2/VideoTransaction;Landroid/media/MediaRecorder;)V
    .locals 3
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cameraId"    # I
    .param p3, "xact"    # Lcom/commonsware/cwac/cam2/VideoTransaction;
    .param p4, "recorder"    # Landroid/media/MediaRecorder;

    .prologue
    .line 198
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 199
    .local v1, "info":Landroid/hardware/Camera$CameraInfo;
    invoke-static {p2, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 200
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Classic;->getDisplayOrientation(Landroid/hardware/Camera$CameraInfo;Z)I

    move-result v0

    .line 202
    .local v0, "displayOrientation":I
    invoke-virtual {p4, v0}, Landroid/media/MediaRecorder;->setOrientationHint(I)V

    .line 203
    return-void
.end method

.method public configureStillCamera(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/Camera$CameraInfo;Landroid/hardware/Camera;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Parameters;
    .locals 8
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "info"    # Landroid/hardware/Camera$CameraInfo;
    .param p3, "camera"    # Landroid/hardware/Camera;
    .param p4, "params"    # Landroid/hardware/Camera$Parameters;

    .prologue
    const/4 v7, 0x1

    .line 112
    invoke-direct {p0, p2, v7}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Classic;->getDisplayOrientation(Landroid/hardware/Camera$CameraInfo;Z)I

    move-result v2

    .line 113
    .local v2, "displayOrientation":I
    const/16 v0, 0x5a

    .line 115
    .local v0, "cameraDisplayOrientation":I
    const-string v5, "samsung"

    sget-object v6, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "sf2wifixx"

    sget-object v6, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 116
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 117
    const/4 v0, 0x0

    .line 154
    :cond_0
    :goto_0
    invoke-virtual {p3, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 156
    if-eqz p4, :cond_1

    .line 159
    iget v5, p2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v5, v7, :cond_5

    .line 160
    rsub-int v5, v2, 0x168

    rem-int/lit16 v3, v5, 0x168

    .line 165
    .local v3, "outputOrientation":I
    :goto_1
    invoke-virtual {p4, v3}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    .line 168
    .end local v3    # "outputOrientation":I
    :cond_1
    return-object p4

    .line 118
    :cond_2
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/plugin/OrientationPlugin$Classic;->useAltAlgorithm()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 119
    const/4 v1, 0x0

    .line 120
    .local v1, "degrees":I
    move v4, v2

    .line 122
    .local v4, "temp":I
    packed-switch v4, :pswitch_data_0

    .line 137
    :goto_2
    iget v5, p2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v5, v7, :cond_3

    .line 138
    iget v5, p2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v5, v1

    rem-int/lit16 v4, v5, 0x168

    .line 139
    rsub-int v5, v4, 0x168

    rem-int/lit16 v4, v5, 0x168

    .line 144
    :goto_3
    move v0, v4

    .line 145
    goto :goto_0

    .line 124
    :pswitch_0
    const/4 v1, 0x0

    .line 125
    goto :goto_2

    .line 127
    :pswitch_1
    const/16 v1, 0x5a

    .line 128
    goto :goto_2

    .line 130
    :pswitch_2
    const/16 v1, 0xb4

    .line 131
    goto :goto_2

    .line 133
    :pswitch_3
    const/16 v1, 0x10e

    goto :goto_2

    .line 141
    :cond_3
    iget v5, p2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v5, v1

    add-int/lit16 v5, v5, 0x168

    rem-int/lit16 v4, v5, 0x168

    goto :goto_3

    .line 145
    .end local v1    # "degrees":I
    .end local v4    # "temp":I
    :cond_4
    const/16 v5, 0xb4

    if-ne v2, v5, :cond_0

    .line 146
    const/16 v0, 0x10e

    goto :goto_0

    .line 162
    :cond_5
    move v3, v2

    .restart local v3    # "outputOrientation":I
    goto :goto_1

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
