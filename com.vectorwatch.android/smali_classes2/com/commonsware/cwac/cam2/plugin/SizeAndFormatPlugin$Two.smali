.class Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Two;
.super Lcom/commonsware/cwac/cam2/SimpleCameraTwoConfigurator;
.source "SizeAndFormatPlugin.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Two"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)V
    .locals 0
    .param p1, "this$0"    # Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/SimpleCameraTwoConfigurator;-><init>()V

    return-void
.end method


# virtual methods
.method public buildImageReader()Landroid/media/ImageReader;
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureSize:Lcom/commonsware/cwac/cam2/util/Size;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->access$100(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v0

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    .line 154
    # getter for: Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureSize:Lcom/commonsware/cwac/cam2/util/Size;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->access$100(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)Lcom/commonsware/cwac/cam2/util/Size;

    move-result-object v1

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin$Two;->this$0:Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;

    # getter for: Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->pictureFormat:I
    invoke-static {v2}, Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;->access$200(Lcom/commonsware/cwac/cam2/plugin/SizeAndFormatPlugin;)I

    move-result v2

    const/4 v3, 0x2

    .line 153
    invoke-static {v0, v1, v2, v3}, Landroid/media/ImageReader;->newInstance(IIII)Landroid/media/ImageReader;

    move-result-object v0

    return-object v0
.end method
