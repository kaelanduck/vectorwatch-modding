.class public abstract Lcom/commonsware/cwac/cam2/CameraSession$Builder;
.super Ljava/lang/Object;
.source "CameraSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# instance fields
.field protected final session:Lcom/commonsware/cwac/cam2/CameraSession;


# direct methods
.method protected constructor <init>(Lcom/commonsware/cwac/cam2/CameraSession;)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraSession$Builder;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    .line 107
    return-void
.end method


# virtual methods
.method public addPlugin(Lcom/commonsware/cwac/cam2/CameraPlugin;)Lcom/commonsware/cwac/cam2/CameraSession$Builder;
    .locals 1
    .param p1, "plugin"    # Lcom/commonsware/cwac/cam2/CameraPlugin;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession$Builder;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    invoke-interface {p1, v0}, Lcom/commonsware/cwac/cam2/CameraPlugin;->validate(Lcom/commonsware/cwac/cam2/CameraSession;)V

    .line 118
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession$Builder;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    # getter for: Lcom/commonsware/cwac/cam2/CameraSession;->plugins:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraSession;->access$000(Lcom/commonsware/cwac/cam2/CameraSession;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    return-object p0
.end method

.method public build()Lcom/commonsware/cwac/cam2/CameraSession;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraSession$Builder;->session:Lcom/commonsware/cwac/cam2/CameraSession;

    return-object v0
.end method
