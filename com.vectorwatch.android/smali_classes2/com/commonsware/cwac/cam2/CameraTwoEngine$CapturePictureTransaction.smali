.class Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;
.super Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;
.source "CameraTwoEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/CameraTwoEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CapturePictureTransaction"
.end annotation


# instance fields
.field private final s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

.field final synthetic this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;)V
    .locals 0
    .param p2, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 628
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;-><init>()V

    .line 629
    check-cast p2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .end local p2    # "session":Lcom/commonsware/cwac/cam2/CameraSession;
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .line 630
    return-void
.end method

.method private unlockFocus()V
    .locals 5

    .prologue
    .line 659
    :try_start_0
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 660
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v1, v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v2, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AF_TRIGGER:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v3, 0x2

    .line 661
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 660
    invoke-virtual {v1, v2, v3}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 662
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v1, v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v2, Landroid/hardware/camera2/CaptureRequest;->CONTROL_AE_MODE:Landroid/hardware/camera2/CaptureRequest$Key;

    const/4 v3, 0x2

    .line 663
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 662
    invoke-virtual {v1, v2, v3}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 664
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v1, v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v2, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v2}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .line 665
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$900(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/os/Handler;

    move-result-object v4

    .line 664
    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/camera2/CameraCaptureSession;->capture(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I

    .line 666
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v1, v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->s:Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    iget-object v2, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequest:Landroid/hardware/camera2/CaptureRequest;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$900(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 681
    :cond_0
    :goto_0
    return-void

    .line 668
    :catch_0
    move-exception v0

    .line 669
    .local v0, "e":Landroid/hardware/camera2/CameraAccessException;
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v1

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    invoke-direct {v2, v0}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v1, v2}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 671
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 672
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception resetting focus"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 674
    .end local v0    # "e":Landroid/hardware/camera2/CameraAccessException;
    :catch_1
    move-exception v0

    .line 675
    .local v0, "e":Ljava/lang/IllegalStateException;
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v1

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;

    invoke-direct {v2, v0}, Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v1, v2}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 677
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception resetting focus"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public onCaptureCompleted(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/TotalCaptureResult;)V
    .locals 0
    .param p1, "session"    # Landroid/hardware/camera2/CameraCaptureSession;
    .param p2, "request"    # Landroid/hardware/camera2/CaptureRequest;
    .param p3, "result"    # Landroid/hardware/camera2/TotalCaptureResult;

    .prologue
    .line 646
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->unlockFocus()V

    .line 647
    return-void
.end method

.method public onCaptureFailed(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CaptureFailure;)V
    .locals 4
    .param p1, "session"    # Landroid/hardware/camera2/CameraCaptureSession;
    .param p2, "request"    # Landroid/hardware/camera2/CaptureRequest;
    .param p3, "failure"    # Landroid/hardware/camera2/CaptureFailure;

    .prologue
    .line 653
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "generic camera2 capture failure"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;-><init>(Ljava/lang/Exception;)V

    .line 654
    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 655
    return-void
.end method

.method public onCaptureStarted(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;JJ)V
    .locals 3
    .param p1, "session"    # Landroid/hardware/camera2/CameraCaptureSession;
    .param p2, "request"    # Landroid/hardware/camera2/CaptureRequest;
    .param p3, "timestamp"    # J
    .param p5, "frameNumber"    # J

    .prologue
    .line 636
    invoke-super/range {p0 .. p6}, Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;->onCaptureStarted(Landroid/hardware/camera2/CameraCaptureSession;Landroid/hardware/camera2/CaptureRequest;JJ)V

    .line 638
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine;->shutter:Landroid/media/MediaActionSound;
    invoke-static {v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->access$1400(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/media/MediaActionSound;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaActionSound;->play(I)V

    .line 639
    return-void
.end method
