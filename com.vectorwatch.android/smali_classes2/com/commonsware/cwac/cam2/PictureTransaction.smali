.class public Lcom/commonsware/cwac/cam2/PictureTransaction;
.super Ljava/lang/Object;
.source "PictureTransaction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/PictureTransaction$Builder;
    }
.end annotation


# instance fields
.field private processors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/commonsware/cwac/cam2/ImageProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private props:Landroid/os/Bundle;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/PictureTransaction;->processors:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/PictureTransaction;->props:Landroid/os/Bundle;

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/commonsware/cwac/cam2/PictureTransaction$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/commonsware/cwac/cam2/PictureTransaction$1;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/PictureTransaction;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/commonsware/cwac/cam2/PictureTransaction;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/PictureTransaction;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/PictureTransaction;->processors:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method findProcessorByTag(Ljava/lang/String;)Lcom/commonsware/cwac/cam2/ImageProcessor;
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 45
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/PictureTransaction;->processors:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/ImageProcessor;

    .line 46
    .local v0, "processor":Lcom/commonsware/cwac/cam2/ImageProcessor;
    invoke-interface {v0}, Lcom/commonsware/cwac/cam2/ImageProcessor;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    .end local v0    # "processor":Lcom/commonsware/cwac/cam2/ImageProcessor;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProperties()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/PictureTransaction;->props:Landroid/os/Bundle;

    return-object v0
.end method

.method process(Lcom/commonsware/cwac/cam2/ImageContext;)Lcom/commonsware/cwac/cam2/ImageContext;
    .locals 3
    .param p1, "imageContext"    # Lcom/commonsware/cwac/cam2/ImageContext;

    .prologue
    .line 37
    iget-object v1, p0, Lcom/commonsware/cwac/cam2/PictureTransaction;->processors:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/ImageProcessor;

    .line 38
    .local v0, "processor":Lcom/commonsware/cwac/cam2/ImageProcessor;
    invoke-interface {v0, p0, p1}, Lcom/commonsware/cwac/cam2/ImageProcessor;->process(Lcom/commonsware/cwac/cam2/PictureTransaction;Lcom/commonsware/cwac/cam2/ImageContext;)V

    goto :goto_0

    .line 41
    .end local v0    # "processor":Lcom/commonsware/cwac/cam2/ImageProcessor;
    :cond_0
    return-object p1
.end method
