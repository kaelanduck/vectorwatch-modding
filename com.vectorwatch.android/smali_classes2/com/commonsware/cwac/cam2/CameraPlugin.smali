.class public interface abstract Lcom/commonsware/cwac/cam2/CameraPlugin;
.super Ljava/lang/Object;
.source "CameraPlugin.java"


# virtual methods
.method public abstract buildConfigurator(Ljava/lang/Class;)Lcom/commonsware/cwac/cam2/CameraConfigurator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/commonsware/cwac/cam2/CameraConfigurator;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract destroy()V
.end method

.method public abstract validate(Lcom/commonsware/cwac/cam2/CameraSession;)V
.end method
