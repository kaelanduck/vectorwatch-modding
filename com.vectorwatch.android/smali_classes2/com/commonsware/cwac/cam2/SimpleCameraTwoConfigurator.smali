.class public Lcom/commonsware/cwac/cam2/SimpleCameraTwoConfigurator;
.super Ljava/lang/Object;
.source "SimpleCameraTwoConfigurator.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addToCaptureRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;ZLandroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p3, "facingFront"    # Z
    .param p4, "captureBuilder"    # Landroid/hardware/camera2/CaptureRequest$Builder;

    .prologue
    .line 25
    return-void
.end method

.method public addToPreviewRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureRequest$Builder;)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p3, "captureBuilder"    # Landroid/hardware/camera2/CaptureRequest$Builder;

    .prologue
    .line 32
    return-void
.end method

.method public buildImageReader()Landroid/media/ImageReader;
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    return-object v0
.end method
