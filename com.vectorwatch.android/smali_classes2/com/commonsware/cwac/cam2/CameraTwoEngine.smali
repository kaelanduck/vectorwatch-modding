.class public Lcom/commonsware/cwac/cam2/CameraTwoEngine;
.super Lcom/commonsware/cwac/cam2/CameraEngine;
.source "CameraTwoEngine.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;,
        Lcom/commonsware/cwac/cam2/CameraTwoEngine$SessionBuilder;,
        Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;,
        Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;,
        Lcom/commonsware/cwac/cam2/CameraTwoEngine$AreaComparator;,
        Lcom/commonsware/cwac/cam2/CameraTwoEngine$CapturePictureTransaction;,
        Lcom/commonsware/cwac/cam2/CameraTwoEngine$RequestCaptureTransaction;,
        Lcom/commonsware/cwac/cam2/CameraTwoEngine$StartPreviewTransaction;,
        Lcom/commonsware/cwac/cam2/CameraTwoEngine$InitPreviewTransaction;
    }
.end annotation


# instance fields
.field private closeLatch:Ljava/util/concurrent/CountDownLatch;

.field private final ctxt:Landroid/content/Context;

.field private descriptors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;",
            ">;"
        }
    .end annotation
.end field

.field private final handler:Landroid/os/Handler;

.field private final handlerThread:Landroid/os/HandlerThread;

.field private final lock:Ljava/util/concurrent/Semaphore;

.field private mgr:Landroid/hardware/camera2/CameraManager;

.field private shutter:Landroid/media/MediaActionSound;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "ctxt"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 78
    invoke-direct {p0}, Lcom/commonsware/cwac/cam2/CameraEngine;-><init>()V

    .line 65
    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handlerThread:Landroid/os/HandlerThread;

    .line 68
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;

    .line 69
    iput-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->closeLatch:Ljava/util/concurrent/CountDownLatch;

    .line 70
    new-instance v0, Landroid/media/MediaActionSound;

    invoke-direct {v0}, Landroid/media/MediaActionSound;-><init>()V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->shutter:Landroid/media/MediaActionSound;

    .line 71
    iput-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->descriptors:Ljava/util/List;

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->ctxt:Landroid/content/Context;

    .line 80
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->ctxt:Landroid/content/Context;

    const-string v1, "camera"

    .line 81
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/camera2/CameraManager;

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;

    .line 82
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 83
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;

    .line 84
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->shutter:Landroid/media/MediaActionSound;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaActionSound;->load(I)V

    .line 85
    return-void
.end method

.method static synthetic access$100(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->descriptors:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$102(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->descriptors:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/concurrent/CountDownLatch;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->closeLatch:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/media/MediaActionSound;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->shutter:Landroid/media/MediaActionSound;

    return-object v0
.end method

.method static synthetic access$200(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/hardware/camera2/CameraManager;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$900(Lcom/commonsware/cwac/cam2/CameraTwoEngine;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/commonsware/cwac/cam2/CameraTwoEngine;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private static cropRegionForZoom(Landroid/hardware/camera2/CameraCharacteristics;F)Landroid/graphics/Rect;
    .locals 10
    .param p0, "cc"    # Landroid/hardware/camera2/CameraCharacteristics;
    .param p1, "zoomTo"    # F
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    .line 389
    sget-object v5, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_INFO_ACTIVE_ARRAY_SIZE:Landroid/hardware/camera2/CameraCharacteristics$Key;

    .line 390
    invoke-virtual {p0, v5}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 391
    .local v2, "sensor":Landroid/graphics/Rect;
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/lit8 v3, v5, 0x2

    .line 392
    .local v3, "sensorCenterX":I
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v5

    div-int/lit8 v4, v5, 0x2

    .line 393
    .local v4, "sensorCenterY":I
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    div-float/2addr v5, p1

    float-to-int v0, v5

    .line 394
    .local v0, "deltaX":I
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    div-float/2addr v5, p1

    float-to-int v1, v5

    .line 396
    .local v1, "deltaY":I
    new-instance v5, Landroid/graphics/Rect;

    sub-int v6, v3, v0

    sub-int v7, v4, v1

    add-int v8, v3, v0

    add-int v9, v4, v1

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v5
.end method


# virtual methods
.method public buildSession(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;)Lcom/commonsware/cwac/cam2/CameraSession$Builder;
    .locals 2
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "descriptor"    # Lcom/commonsware/cwac/cam2/CameraDescriptor;

    .prologue
    .line 92
    new-instance v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$SessionBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$SessionBuilder;-><init>(Landroid/content/Context;Lcom/commonsware/cwac/cam2/CameraDescriptor;Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;)V

    return-object v0
.end method

.method public close(Lcom/commonsware/cwac/cam2/CameraSession;)V
    .locals 7
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 241
    move-object v2, p1

    check-cast v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .line 244
    .local v2, "s":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;
    :try_start_0
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 246
    iget-object v3, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    if-eqz v3, :cond_0

    .line 247
    new-instance v3, Ljava/util/concurrent/CountDownLatch;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->closeLatch:Ljava/util/concurrent/CountDownLatch;

    .line 248
    iget-object v3, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    invoke-virtual {v3}, Landroid/hardware/camera2/CameraCaptureSession;->close()V

    .line 249
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->closeLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v4, 0x2

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    .line 250
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    .line 253
    :cond_0
    iget-object v3, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->cameraDevice:Landroid/hardware/camera2/CameraDevice;

    if-eqz v3, :cond_1

    .line 254
    iget-object v3, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->cameraDevice:Landroid/hardware/camera2/CameraDevice;

    invoke-virtual {v3}, Landroid/hardware/camera2/CameraDevice;->close()V

    .line 255
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->cameraDevice:Landroid/hardware/camera2/CameraDevice;

    .line 258
    :cond_1
    iget-object v3, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->reader:Landroid/media/ImageReader;

    if-eqz v3, :cond_2

    .line 259
    iget-object v3, v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->reader:Landroid/media/ImageReader;

    invoke-virtual {v3}, Landroid/media/ImageReader;->close()V

    .line 262
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->setClosed(Z)V

    .line 264
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v0

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .line 266
    .local v0, "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    const/4 v3, 0x0

    # invokes: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->setDevice(Landroid/hardware/camera2/CameraDevice;)V
    invoke-static {v0, v3}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$1000(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;Landroid/hardware/camera2/CameraDevice;)V

    .line 267
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->destroy()V

    .line 268
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v3

    new-instance v4, Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;

    invoke-direct {v4}, Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;-><init>()V

    invoke-virtual {v3, v4}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 274
    .end local v0    # "camera":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    :goto_0
    return-void

    .line 269
    :catch_0
    move-exception v1

    .line 270
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v3

    new-instance v4, Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;

    invoke-direct {v4, v1}, Lcom/commonsware/cwac/cam2/CameraEngine$ClosedEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v3, v4}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 272
    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->lock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v4}, Ljava/util/concurrent/Semaphore;->release()V

    throw v3
.end method

.method public handleOrientationChange(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "event"    # Lcom/commonsware/cwac/cam2/CameraEngine$OrientationChangedEvent;

    .prologue
    .line 312
    return-void
.end method

.method public loadCameraDescriptors(Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V
    .locals 2
    .param p1, "criteria"    # Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getThreadPool()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;

    invoke-direct {v1, p0, p1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$1;-><init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSelectionCriteria;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 175
    return-void
.end method

.method public open(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V
    .locals 2
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "texture"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getThreadPool()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$2;-><init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraSession;Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 234
    return-void
.end method

.method public recordVideo(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/VideoTransaction;)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "xact"    # Lcom/commonsware/cwac/cam2/VideoTransaction;

    .prologue
    .line 317
    return-void
.end method

.method public stopVideoRecording(Lcom/commonsware/cwac/cam2/CameraSession;Z)V
    .locals 0
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "abandon"    # Z

    .prologue
    .line 322
    return-void
.end method

.method public supportsDynamicFlashModes()Z
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x1

    return v0
.end method

.method public supportsZoom(Lcom/commonsware/cwac/cam2/CameraSession;)Z
    .locals 7
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;

    .prologue
    .line 334
    const/4 v4, 0x0

    .line 335
    .local v4, "result":Z
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .line 338
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    :try_start_0
    iget-object v5, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;

    .line 339
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->cameraId:Ljava/lang/String;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$1100(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v0

    .line 341
    .local v0, "cc":Landroid/hardware/camera2/CameraCharacteristics;
    sget-object v5, Landroid/hardware/camera2/CameraCharacteristics;->SCALER_AVAILABLE_MAX_DIGITAL_ZOOM:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v0, v5}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 344
    .local v3, "maxZoom":F
    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v3, v5

    if-ltz v5, :cond_0

    const/4 v4, 0x1

    .line 349
    .end local v0    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    .end local v3    # "maxZoom":F
    :goto_0
    return v4

    .line 344
    .restart local v0    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    .restart local v3    # "maxZoom":F
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 345
    .end local v0    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    .end local v3    # "maxZoom":F
    :catch_0
    move-exception v2

    .line 346
    .local v2, "e":Landroid/hardware/camera2/CameraAccessException;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v5

    new-instance v6, Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;

    invoke-direct {v6, v2}, Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v5, v6}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public takePicture(Lcom/commonsware/cwac/cam2/CameraSession;Lcom/commonsware/cwac/cam2/PictureTransaction;)V
    .locals 5
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "xact"    # Lcom/commonsware/cwac/cam2/PictureTransaction;

    .prologue
    .line 281
    move-object v0, p1

    check-cast v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .line 283
    .local v0, "s":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;
    iget-object v1, v0, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->reader:Landroid/media/ImageReader;

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;

    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v4

    invoke-direct {v2, v3, v4, p2}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$TakePictureTransaction;-><init>(Landroid/content/Context;Lde/greenrobot/event/EventBus;Lcom/commonsware/cwac/cam2/PictureTransaction;)V

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v2, v3}, Landroid/media/ImageReader;->setOnImageAvailableListener(Landroid/media/ImageReader$OnImageAvailableListener;Landroid/os/Handler;)V

    .line 286
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getThreadPool()Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v1

    new-instance v2, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;

    invoke-direct {v2, p0, v0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$3;-><init>(Lcom/commonsware/cwac/cam2/CameraTwoEngine;Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;)V

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 306
    return-void
.end method

.method public zoomTo(Lcom/commonsware/cwac/cam2/CameraSession;I)Z
    .locals 11
    .param p1, "session"    # Lcom/commonsware/cwac/cam2/CameraSession;
    .param p2, "zoomLevel"    # I

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 355
    move-object v4, p1

    check-cast v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;

    .line 356
    .local v4, "s":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/CameraSession;->getDescriptor()Lcom/commonsware/cwac/cam2/CameraDescriptor;

    move-result-object v1

    check-cast v1, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;

    .line 358
    .local v1, "descriptor":Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;
    iget-object v7, v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequest:Landroid/hardware/camera2/CaptureRequest;

    if-eqz v7, :cond_0

    .line 360
    :try_start_0
    iget-object v7, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->mgr:Landroid/hardware/camera2/CameraManager;

    .line 361
    # getter for: Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->cameraId:Ljava/lang/String;
    invoke-static {v1}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;->access$1100(Lcom/commonsware/cwac/cam2/CameraTwoEngine$Descriptor;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v0

    .line 362
    .local v0, "cc":Landroid/hardware/camera2/CameraCharacteristics;
    sget-object v7, Landroid/hardware/camera2/CameraCharacteristics;->SCALER_AVAILABLE_MAX_DIGITAL_ZOOM:Landroid/hardware/camera2/CameraCharacteristics$Key;

    .line 363
    invoke-virtual {v0, v7}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 367
    .local v3, "maxZoom":F
    cmpl-float v7, v3, v9

    if-lez v7, :cond_0

    .line 368
    int-to-float v7, p2

    sub-float v8, v3, v9

    mul-float/2addr v7, v8

    const/high16 v8, 0x42c80000    # 100.0f

    div-float/2addr v7, v8

    add-float v6, v9, v7

    .line 369
    .local v6, "zoomTo":F
    invoke-static {v0, v6}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->cropRegionForZoom(Landroid/hardware/camera2/CameraCharacteristics;F)Landroid/graphics/Rect;

    move-result-object v5

    .line 371
    .local v5, "zoomRect":Landroid/graphics/Rect;
    iget-object v7, v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    sget-object v8, Landroid/hardware/camera2/CaptureRequest;->SCALER_CROP_REGION:Landroid/hardware/camera2/CaptureRequest$Key;

    .line 372
    invoke-virtual {v7, v8, v5}, Landroid/hardware/camera2/CaptureRequest$Builder;->set(Landroid/hardware/camera2/CaptureRequest$Key;Ljava/lang/Object;)V

    .line 373
    invoke-virtual {v4, v5}, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->setZoomRect(Landroid/graphics/Rect;)V

    .line 374
    iget-object v7, v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequestBuilder:Landroid/hardware/camera2/CaptureRequest$Builder;

    invoke-virtual {v7}, Landroid/hardware/camera2/CaptureRequest$Builder;->build()Landroid/hardware/camera2/CaptureRequest;

    move-result-object v7

    iput-object v7, v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequest:Landroid/hardware/camera2/CaptureRequest;

    .line 375
    iget-object v7, v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->captureSession:Landroid/hardware/camera2/CameraCaptureSession;

    iget-object v8, v4, Lcom/commonsware/cwac/cam2/CameraTwoEngine$Session;->previewRequest:Landroid/hardware/camera2/CaptureRequest;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->handler:Landroid/os/Handler;

    invoke-virtual {v7, v8, v9, v10}, Landroid/hardware/camera2/CameraCaptureSession;->setRepeatingRequest(Landroid/hardware/camera2/CaptureRequest;Landroid/hardware/camera2/CameraCaptureSession$CaptureCallback;Landroid/os/Handler;)I
    :try_end_0
    .catch Landroid/hardware/camera2/CameraAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    .end local v0    # "cc":Landroid/hardware/camera2/CameraCharacteristics;
    .end local v3    # "maxZoom":F
    .end local v5    # "zoomRect":Landroid/graphics/Rect;
    .end local v6    # "zoomTo":F
    :cond_0
    :goto_0
    const/4 v7, 0x0

    return v7

    .line 378
    :catch_0
    move-exception v2

    .line 379
    .local v2, "e":Landroid/hardware/camera2/CameraAccessException;
    invoke-virtual {p0}, Lcom/commonsware/cwac/cam2/CameraTwoEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v7

    new-instance v8, Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;

    invoke-direct {v8, v2}, Lcom/commonsware/cwac/cam2/CameraEngine$DeepImpactEvent;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v7, v8}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
