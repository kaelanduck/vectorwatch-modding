.class Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;
.super Ljava/lang/Object;
.source "ClassicCameraEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->onPictureTaken([BLandroid/hardware/Camera;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

.field final synthetic val$bytes:[B

.field final synthetic val$camera:Landroid/hardware/Camera;


# direct methods
.method constructor <init>(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;Landroid/hardware/Camera;[B)V
    .locals 0
    .param p1, "this$1"    # Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

    .prologue
    .line 459
    iput-object p1, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;->this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

    iput-object p2, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;->val$camera:Landroid/hardware/Camera;

    iput-object p3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;->val$bytes:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 462
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;->val$camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 463
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;->this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

    iget-object v0, v0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->this$0:Lcom/commonsware/cwac/cam2/ClassicCameraEngine;

    invoke-virtual {v0}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine;->getBus()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;

    iget-object v2, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;->this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->xact:Lcom/commonsware/cwac/cam2/PictureTransaction;
    invoke-static {v2}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->access$1100(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;)Lcom/commonsware/cwac/cam2/PictureTransaction;

    move-result-object v2

    iget-object v3, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;->this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

    .line 464
    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->xact:Lcom/commonsware/cwac/cam2/PictureTransaction;
    invoke-static {v3}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->access$1100(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;)Lcom/commonsware/cwac/cam2/PictureTransaction;

    move-result-object v3

    new-instance v4, Lcom/commonsware/cwac/cam2/ImageContext;

    iget-object v5, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;->this$1:Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;

    # getter for: Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->ctxt:Landroid/content/Context;
    invoke-static {v5}, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;->access$1200(Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/commonsware/cwac/cam2/ClassicCameraEngine$TakePictureTransaction$1;->val$bytes:[B

    invoke-direct {v4, v5, v6}, Lcom/commonsware/cwac/cam2/ImageContext;-><init>(Landroid/content/Context;[B)V

    invoke-virtual {v3, v4}, Lcom/commonsware/cwac/cam2/PictureTransaction;->process(Lcom/commonsware/cwac/cam2/ImageContext;)Lcom/commonsware/cwac/cam2/ImageContext;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/commonsware/cwac/cam2/CameraEngine$PictureTakenEvent;-><init>(Lcom/commonsware/cwac/cam2/PictureTransaction;Lcom/commonsware/cwac/cam2/ImageContext;)V

    .line 463
    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 465
    return-void
.end method
