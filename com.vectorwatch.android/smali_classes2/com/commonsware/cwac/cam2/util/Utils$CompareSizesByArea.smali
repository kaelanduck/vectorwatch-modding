.class Lcom/commonsware/cwac/cam2/util/Utils$CompareSizesByArea;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/util/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CompareSizesByArea"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/commonsware/cwac/cam2/util/Size;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/commonsware/cwac/cam2/util/Size;Lcom/commonsware/cwac/cam2/util/Size;)I
    .locals 6
    .param p1, "lhs"    # Lcom/commonsware/cwac/cam2/util/Size;
    .param p2, "rhs"    # Lcom/commonsware/cwac/cam2/util/Size;

    .prologue
    .line 172
    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    .line 173
    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/util/Size;->getWidth()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p2}, Lcom/commonsware/cwac/cam2/util/Size;->getHeight()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    sub-long/2addr v0, v2

    .line 172
    invoke-static {v0, v1}, Ljava/lang/Long;->signum(J)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 167
    check-cast p1, Lcom/commonsware/cwac/cam2/util/Size;

    check-cast p2, Lcom/commonsware/cwac/cam2/util/Size;

    invoke-virtual {p0, p1, p2}, Lcom/commonsware/cwac/cam2/util/Utils$CompareSizesByArea;->compare(Lcom/commonsware/cwac/cam2/util/Size;Lcom/commonsware/cwac/cam2/util/Size;)I

    move-result v0

    return v0
.end method
