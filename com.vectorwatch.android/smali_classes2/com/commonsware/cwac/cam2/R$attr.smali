.class public final Lcom/commonsware/cwac/cam2/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/commonsware/cwac/cam2/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final assetName:I = 0x7f010116

.field public static final fab_colorDisabled:I = 0x7f01008d

.field public static final fab_colorNormal:I = 0x7f01008b

.field public static final fab_colorPressed:I = 0x7f01008c

.field public static final fab_colorRipple:I = 0x7f01008e

.field public static final fab_elevationCompat:I = 0x7f010098

.field public static final fab_hideAnimation:I = 0x7f010096

.field public static final fab_label:I = 0x7f010097

.field public static final fab_progress:I = 0x7f01009d

.field public static final fab_progress_backgroundColor:I = 0x7f01009a

.field public static final fab_progress_color:I = 0x7f010099

.field public static final fab_progress_indeterminate:I = 0x7f01009b

.field public static final fab_progress_max:I = 0x7f01009c

.field public static final fab_progress_showBackground:I = 0x7f01009e

.field public static final fab_shadowColor:I = 0x7f010090

.field public static final fab_shadowRadius:I = 0x7f010091

.field public static final fab_shadowXOffset:I = 0x7f010092

.field public static final fab_shadowYOffset:I = 0x7f010093

.field public static final fab_showAnimation:I = 0x7f010095

.field public static final fab_showShadow:I = 0x7f01008f

.field public static final fab_size:I = 0x7f010094

.field public static final menu_animationDelayPerItem:I = 0x7f0100b6

.field public static final menu_backgroundColor:I = 0x7f0100c5

.field public static final menu_buttonSpacing:I = 0x7f0100a4

.field public static final menu_buttonToggleAnimation:I = 0x7f0100b7

.field public static final menu_colorNormal:I = 0x7f0100c1

.field public static final menu_colorPressed:I = 0x7f0100c2

.field public static final menu_colorRipple:I = 0x7f0100c3

.field public static final menu_fab_hide_animation:I = 0x7f0100c8

.field public static final menu_fab_label:I = 0x7f0100c6

.field public static final menu_fab_show_animation:I = 0x7f0100c7

.field public static final menu_fab_size:I = 0x7f0100bb

.field public static final menu_icon:I = 0x7f0100b5

.field public static final menu_labels_colorNormal:I = 0x7f0100b1

.field public static final menu_labels_colorPressed:I = 0x7f0100b2

.field public static final menu_labels_colorRipple:I = 0x7f0100b3

.field public static final menu_labels_cornerRadius:I = 0x7f0100af

.field public static final menu_labels_ellipsize:I = 0x7f0100b9

.field public static final menu_labels_hideAnimation:I = 0x7f0100a7

.field public static final menu_labels_margin:I = 0x7f0100a5

.field public static final menu_labels_maxLines:I = 0x7f0100ba

.field public static final menu_labels_padding:I = 0x7f0100ac

.field public static final menu_labels_paddingBottom:I = 0x7f0100ab

.field public static final menu_labels_paddingLeft:I = 0x7f0100a9

.field public static final menu_labels_paddingRight:I = 0x7f0100aa

.field public static final menu_labels_paddingTop:I = 0x7f0100a8

.field public static final menu_labels_position:I = 0x7f0100b4

.field public static final menu_labels_showAnimation:I = 0x7f0100a6

.field public static final menu_labels_showShadow:I = 0x7f0100b0

.field public static final menu_labels_singleLine:I = 0x7f0100b8

.field public static final menu_labels_style:I = 0x7f0100bc

.field public static final menu_labels_textColor:I = 0x7f0100ad

.field public static final menu_labels_textSize:I = 0x7f0100ae

.field public static final menu_openDirection:I = 0x7f0100c4

.field public static final menu_shadowColor:I = 0x7f0100bd

.field public static final menu_shadowRadius:I = 0x7f0100be

.field public static final menu_shadowXOffset:I = 0x7f0100bf

.field public static final menu_shadowYOffset:I = 0x7f0100c0

.field public static final menu_showShadow:I = 0x7f0100a3

.field public static final panEnabled:I = 0x7f010117

.field public static final quickScaleEnabled:I = 0x7f010119

.field public static final src:I = 0x7f010115

.field public static final tileBackgroundColor:I = 0x7f01011a

.field public static final zoomEnabled:I = 0x7f010118


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
