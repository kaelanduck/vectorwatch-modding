.class public interface abstract Lcom/commonsware/cwac/cam2/CameraTwoConfigurator;
.super Ljava/lang/Object;
.source "CameraTwoConfigurator.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/CameraConfigurator;


# virtual methods
.method public abstract addToCaptureRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;ZLandroid/hardware/camera2/CaptureRequest$Builder;)V
.end method

.method public abstract addToPreviewRequest(Lcom/commonsware/cwac/cam2/CameraSession;Landroid/hardware/camera2/CameraCharacteristics;Landroid/hardware/camera2/CaptureRequest$Builder;)V
.end method

.method public abstract buildImageReader()Landroid/media/ImageReader;
.end method
