.class public abstract Lcom/commonsware/cwac/cam2/AbstractImageProcessor;
.super Ljava/lang/Object;
.source "AbstractImageProcessor.java"

# interfaces
.implements Lcom/commonsware/cwac/cam2/ImageProcessor;


# instance fields
.field private final ctxt:Landroid/content/Context;

.field private final tag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/commonsware/cwac/cam2/AbstractImageProcessor;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractImageProcessor;->ctxt:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/commonsware/cwac/cam2/AbstractImageProcessor;->tag:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractImageProcessor;->ctxt:Landroid/content/Context;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractImageProcessor;->tag:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/commonsware/cwac/cam2/AbstractImageProcessor;->tag:Ljava/lang/String;

    goto :goto_0
.end method
