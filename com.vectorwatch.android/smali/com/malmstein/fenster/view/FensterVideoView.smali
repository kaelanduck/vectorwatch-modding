.class public Lcom/malmstein/fenster/view/FensterVideoView;
.super Landroid/view/TextureView;
.source "FensterVideoView.java"

# interfaces
.implements Landroid/widget/MediaController$MediaPlayerControl;
.implements Lcom/malmstein/fenster/play/FensterPlayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/malmstein/fenster/view/FensterVideoView$ReplayListener;
    }
.end annotation


# static fields
.field private static final MILLIS_IN_SEC:I = 0x3e8

.field private static final NOTIFY_REPLAY_INTERVAL_MILLIS:J

.field private static final NULL_REPLAY_LISTENER:Lcom/malmstein/fenster/view/FensterVideoView$ReplayListener;

.field private static final STATE_ERROR:I = -0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_PAUSED:I = 0x4

.field private static final STATE_PLAYBACK_COMPLETED:I = 0x5

.field private static final STATE_PLAYING:I = 0x3

.field private static final STATE_PREPARED:I = 0x2

.field private static final STATE_PREPARING:I = 0x1

.field public static final TAG:Ljava/lang/String; = "TextureVideoView"

.field public static final VIDEO_BEGINNING:I


# instance fields
.field private errorDialog:Landroid/app/AlertDialog;

.field private fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

.field private mAudioSession:I

.field private mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private mCanPause:Z

.field private mCanSeekBack:Z

.field private mCanSeekForward:Z

.field private mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mCurrentBufferPercentage:I

.field private mCurrentState:I

.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mSTListener:Landroid/view/TextureView$SurfaceTextureListener;

.field private mSeekWhenPrepared:I

.field private mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTargetState:I

.field private mUri:Landroid/net/Uri;

.field private final onInfoToPlayStateListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private onPlayStateListener:Lcom/malmstein/fenster/play/FensterVideoStateListener;

.field private final videoSizeCalculator:Lcom/malmstein/fenster/view/VideoSizeCalculator;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 66
    new-instance v0, Lcom/malmstein/fenster/view/FensterVideoView$1;

    invoke-direct {v0}, Lcom/malmstein/fenster/view/FensterVideoView$1;-><init>()V

    sput-object v0, Lcom/malmstein/fenster/view/FensterVideoView;->NULL_REPLAY_LISTENER:Lcom/malmstein/fenster/view/FensterVideoView$ReplayListener;

    .line 82
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/malmstein/fenster/view/FensterVideoView;->NOTIFY_REPLAY_INTERVAL_MILLIS:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/malmstein/fenster/view/FensterVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 118
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 121
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    iput v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    .line 93
    iput v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mTargetState:I

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 270
    new-instance v0, Lcom/malmstein/fenster/view/FensterVideoView$2;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/view/FensterVideoView$2;-><init>(Lcom/malmstein/fenster/view/FensterVideoView;)V

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 280
    new-instance v0, Lcom/malmstein/fenster/view/FensterVideoView$3;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/view/FensterVideoView$3;-><init>(Lcom/malmstein/fenster/view/FensterVideoView;)V

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 321
    new-instance v0, Lcom/malmstein/fenster/view/FensterVideoView$4;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/view/FensterVideoView$4;-><init>(Lcom/malmstein/fenster/view/FensterVideoView;)V

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 335
    new-instance v0, Lcom/malmstein/fenster/view/FensterVideoView$5;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/view/FensterVideoView$5;-><init>(Lcom/malmstein/fenster/view/FensterVideoView;)V

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 345
    new-instance v0, Lcom/malmstein/fenster/view/FensterVideoView$6;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/view/FensterVideoView$6;-><init>(Lcom/malmstein/fenster/view/FensterVideoView;)V

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 451
    new-instance v0, Lcom/malmstein/fenster/view/FensterVideoView$8;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/view/FensterVideoView$8;-><init>(Lcom/malmstein/fenster/view/FensterVideoView;)V

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 500
    new-instance v0, Lcom/malmstein/fenster/view/FensterVideoView$9;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/view/FensterVideoView$9;-><init>(Lcom/malmstein/fenster/view/FensterVideoView;)V

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSTListener:Landroid/view/TextureView$SurfaceTextureListener;

    .line 714
    new-instance v0, Lcom/malmstein/fenster/view/FensterVideoView$11;

    invoke-direct {v0, p0}, Lcom/malmstein/fenster/view/FensterVideoView$11;-><init>(Lcom/malmstein/fenster/view/FensterVideoView;)V

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->onInfoToPlayStateListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 122
    new-instance v0, Lcom/malmstein/fenster/view/VideoSizeCalculator;

    invoke-direct {v0}, Lcom/malmstein/fenster/view/VideoSizeCalculator;-><init>()V

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->videoSizeCalculator:Lcom/malmstein/fenster/view/VideoSizeCalculator;

    .line 123
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->initVideoView()V

    .line 124
    return-void
.end method

.method static synthetic access$000(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/view/VideoSizeCalculator;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->videoSizeCalculator:Lcom/malmstein/fenster/view/VideoSizeCalculator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/malmstein/fenster/view/FensterVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    return v0
.end method

.method static synthetic access$1000(Lcom/malmstein/fenster/view/FensterVideoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->showMediaController()V

    return-void
.end method

.method static synthetic access$102(Lcom/malmstein/fenster/view/FensterVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    return p1
.end method

.method static synthetic access$1100(Lcom/malmstein/fenster/view/FensterVideoView;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/malmstein/fenster/view/FensterVideoView;->pausedAt(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/malmstein/fenster/view/FensterVideoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->showStickyMediaController()V

    return-void
.end method

.method static synthetic access$1300(Lcom/malmstein/fenster/view/FensterVideoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->hideMediaController()V

    return-void
.end method

.method static synthetic access$1400(Lcom/malmstein/fenster/view/FensterVideoView;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/malmstein/fenster/view/FensterVideoView;)Landroid/media/MediaPlayer$OnInfoListener;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/malmstein/fenster/view/FensterVideoView;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/malmstein/fenster/view/FensterVideoView;->allowPlayStateToHandle(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/malmstein/fenster/view/FensterVideoView;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/malmstein/fenster/view/FensterVideoView;->allowErrorListenerToHandle(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/malmstein/fenster/view/FensterVideoView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/malmstein/fenster/view/FensterVideoView;->handleError(I)V

    return-void
.end method

.method static synthetic access$1902(Lcom/malmstein/fenster/view/FensterVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentBufferPercentage:I

    return p1
.end method

.method static synthetic access$2002(Lcom/malmstein/fenster/view/FensterVideoView;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object p1
.end method

.method static synthetic access$202(Lcom/malmstein/fenster/view/FensterVideoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCanPause:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/malmstein/fenster/view/FensterVideoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->openVideo()V

    return-void
.end method

.method static synthetic access$2200(Lcom/malmstein/fenster/view/FensterVideoView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/malmstein/fenster/view/FensterVideoView;->release(Z)V

    return-void
.end method

.method static synthetic access$2300(Lcom/malmstein/fenster/view/FensterVideoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->noPlayStateListener()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2400(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/play/FensterVideoStateListener;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->onPlayStateListener:Lcom/malmstein/fenster/play/FensterVideoStateListener;

    return-object v0
.end method

.method static synthetic access$302(Lcom/malmstein/fenster/view/FensterVideoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCanSeekBack:Z

    return p1
.end method

.method static synthetic access$402(Lcom/malmstein/fenster/view/FensterVideoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCanSeekForward:Z

    return p1
.end method

.method static synthetic access$500(Lcom/malmstein/fenster/view/FensterVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/malmstein/fenster/view/FensterVideoView;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$700(Lcom/malmstein/fenster/view/FensterVideoView;)Lcom/malmstein/fenster/controller/FensterPlayerController;
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    return-object v0
.end method

.method static synthetic access$800(Lcom/malmstein/fenster/view/FensterVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSeekWhenPrepared:I

    return v0
.end method

.method static synthetic access$900(Lcom/malmstein/fenster/view/FensterVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;

    .prologue
    .line 49
    iget v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mTargetState:I

    return v0
.end method

.method static synthetic access$902(Lcom/malmstein/fenster/view/FensterVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/malmstein/fenster/view/FensterVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mTargetState:I

    return p1
.end method

.method private allowErrorListenerToHandle(II)Z
    .locals 2
    .param p1, "frameworkError"    # I
    .param p2, "implError"    # I

    .prologue
    .line 392
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1, p1, p2}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    .line 396
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private allowPlayStateToHandle(I)Z
    .locals 2
    .param p1, "frameworkError"    # I

    .prologue
    .line 382
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/16 v0, -0x3ec

    if-ne p1, v0, :cond_1

    .line 383
    :cond_0
    const-string v0, "TextureVideoView"

    const-string v1, "TextureVideoView error. File or network related operation errors."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->hasPlayStateListener()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->onPlayStateListener:Lcom/malmstein/fenster/play/FensterVideoStateListener;

    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    invoke-interface {v0, v1}, Lcom/malmstein/fenster/play/FensterVideoStateListener;->onStopWithExternalError(I)Z

    move-result v0

    .line 388
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private attachMediaController()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-interface {v0, p0}, Lcom/malmstein/fenster/controller/FensterPlayerController;->setMediaPlayer(Lcom/malmstein/fenster/play/FensterPlayer;)V

    .line 266
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isInPlaybackState()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/malmstein/fenster/controller/FensterPlayerController;->setEnabled(Z)V

    .line 268
    :cond_0
    return-void
.end method

.method private static createErrorDialog(Landroid/content/Context;Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer;I)Landroid/app/AlertDialog;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "completionListener"    # Landroid/media/MediaPlayer$OnCompletionListener;
    .param p2, "mediaPlayer"    # Landroid/media/MediaPlayer;
    .param p3, "errorMessage"    # I

    .prologue
    .line 411
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 412
    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/malmstein/fenster/view/FensterVideoView$7;

    invoke-direct {v2, p1, p2}, Lcom/malmstein/fenster/view/FensterVideoView$7;-><init>(Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer;)V

    .line 413
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 425
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 426
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private static getErrorMessage(I)I
    .locals 3
    .param p0, "frameworkError"    # I

    .prologue
    .line 430
    sget v0, Lcom/malmstein/fenster/R$string;->play_error_message:I

    .line 432
    .local v0, "messageId":I
    const/16 v1, -0x3ec

    if-ne p0, v1, :cond_1

    .line 433
    const-string v1, "TextureVideoView"

    const-string v2, "TextureVideoView error. File or network related operation errors."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    :cond_0
    :goto_0
    return v0

    .line 434
    :cond_1
    const/16 v1, -0x3ef

    if-ne p0, v1, :cond_2

    .line 435
    const-string v1, "TextureVideoView"

    const-string v2, "TextureVideoView error. Bitstream is not conforming to the related coding standard or file spec."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 436
    :cond_2
    const/16 v1, 0x64

    if-ne p0, v1, :cond_3

    .line 437
    const-string v1, "TextureVideoView"

    const-string v2, "TextureVideoView error. Media server died. In this case, the application must release the MediaPlayer object and instantiate a new one."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 438
    :cond_3
    const/16 v1, -0x6e

    if-ne p0, v1, :cond_4

    .line 439
    const-string v1, "TextureVideoView"

    const-string v2, "TextureVideoView error. Some operation takes too long to complete, usually more than 3-5 seconds."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 440
    :cond_4
    const/4 v1, 0x1

    if-ne p0, v1, :cond_5

    .line 441
    const-string v1, "TextureVideoView"

    const-string v2, "TextureVideoView error. Unspecified media player error."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 442
    :cond_5
    const/16 v1, -0x3f2

    if-ne p0, v1, :cond_6

    .line 443
    const-string v1, "TextureVideoView"

    const-string v2, "TextureVideoView error. Bitstream is conforming to the related coding standard or file spec, but the media framework does not support the feature."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 444
    :cond_6
    const/16 v1, 0xc8

    if-ne p0, v1, :cond_0

    .line 445
    const-string v1, "TextureVideoView"

    const-string v2, "TextureVideoView error. The video is streamed and its container is not valid for progressive playback i.e the video\'s index (e.g moov atom) is not at the start of the file."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    sget v0, Lcom/malmstein/fenster/R$string;->play_progressive_error_message:I

    goto :goto_0
.end method

.method private handleError(I)V
    .locals 4
    .param p1, "frameworkError"    # I

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->errorDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->errorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    const-string v0, "TextureVideoView"

    const-string v1, "Dismissing last error dialog for a new one"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->errorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 405
    :cond_0
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {p1}, Lcom/malmstein/fenster/view/FensterVideoView;->getErrorMessage(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->createErrorDialog(Landroid/content/Context;Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer;I)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->errorDialog:Landroid/app/AlertDialog;

    .line 406
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->errorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 408
    :cond_1
    return-void
.end method

.method private hasPlayStateListener()Z
    .locals 1

    .prologue
    .line 742
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->onPlayStateListener:Lcom/malmstein/fenster/play/FensterVideoStateListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideMediaController()V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-interface {v0}, Lcom/malmstein/fenster/controller/FensterPlayerController;->hide()V

    .line 373
    :cond_0
    return-void
.end method

.method private initVideoView()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->videoSizeCalculator:Lcom/malmstein/fenster/view/VideoSizeCalculator;

    invoke-virtual {v0, v1, v1}, Lcom/malmstein/fenster/view/VideoSizeCalculator;->setVideoSize(II)V

    .line 151
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSTListener:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 153
    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setFocusable(Z)V

    .line 154
    invoke-virtual {p0, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setFocusableInTouchMode(Z)V

    .line 155
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->requestFocus()Z

    .line 156
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    .line 157
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mTargetState:I

    .line 158
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->onInfoToPlayStateListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-direct {p0, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 159
    return-void
.end method

.method private isInPlaybackState()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 683
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private noPlayStateListener()Z
    .locals 1

    .prologue
    .line 738
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->hasPlayStateListener()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notReadyForPlaybackJustYetWillTryAgainLater()Z
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyUnableToOpenContent(Ljava/lang/Exception;)V
    .locals 4
    .param p1, "ex"    # Ljava/lang/Exception;

    .prologue
    const/4 v2, -0x1

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to open content: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 250
    iput v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    .line 251
    iput v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mTargetState:I

    .line 252
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    .line 253
    return-void
.end method

.method private openVideo()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 199
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->notReadyForPlaybackJustYetWillTryAgainLater()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 235
    :goto_0
    return-void

    .line 202
    :cond_0
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->tellTheMusicPlaybackServiceToPause()V

    .line 205
    invoke-direct {p0, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->release(Z)V

    .line 207
    :try_start_0
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 209
    iget v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mAudioSession:I

    if-eqz v1, :cond_1

    .line 210
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mAudioSession:I

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioSessionId(I)V

    .line 214
    :goto_1
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 215
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 216
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 217
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 218
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 219
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 220
    const/4 v1, 0x0

    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentBufferPercentage:I

    .line 221
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mHeaders:Ljava/util/Map;

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 222
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Landroid/view/Surface;

    iget-object v3, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-direct {v2, v3}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 223
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 224
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 225
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 228
    const/4 v1, 0x1

    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    .line 229
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->attachMediaController()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "ex":Ljava/io/IOException;
    invoke-direct {p0, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->notifyUnableToOpenContent(Ljava/lang/Exception;)V

    goto :goto_0

    .line 212
    .end local v0    # "ex":Ljava/io/IOException;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v1

    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mAudioSession:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 232
    :catch_1
    move-exception v0

    .line 233
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-direct {p0, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->notifyUnableToOpenContent(Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method private pausedAt(I)Z
    .locals 1
    .param p1, "seekToPosition"    # I

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->getCurrentPosition()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private release(Z)V
    .locals 2
    .param p1, "clearTargetState"    # Z

    .prologue
    const/4 v1, 0x0

    .line 537
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 539
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 540
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 541
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    .line 542
    if-eqz p1, :cond_0

    .line 543
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mTargetState:I

    .line 546
    :cond_0
    return-void
.end method

.method private setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 0
    .param p1, "l"    # Landroid/media/MediaPlayer$OnInfoListener;

    .prologue
    .line 497
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 498
    return-void
.end method

.method private setVideoURI(Landroid/net/Uri;Ljava/util/Map;I)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p3, "seekInSeconds"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p2, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "TextureVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start playing: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mUri:Landroid/net/Uri;

    .line 176
    iput-object p2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mHeaders:Ljava/util/Map;

    .line 177
    mul-int/lit16 v0, p3, 0x3e8

    iput v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSeekWhenPrepared:I

    .line 178
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->openVideo()V

    .line 179
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->requestLayout()V

    .line 180
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->invalidate()V

    .line 181
    return-void
.end method

.method private showMediaController()V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-interface {v0}, Lcom/malmstein/fenster/controller/FensterPlayerController;->show()V

    .line 379
    :cond_0
    return-void
.end method

.method private showStickyMediaController()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/malmstein/fenster/controller/FensterPlayerController;->show(I)V

    .line 319
    :cond_0
    return-void
.end method

.method private tellTheMusicPlaybackServiceToPause()V
    .locals 3

    .prologue
    .line 243
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 244
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 246
    return-void
.end method


# virtual methods
.method public canPause()Z
    .locals 1

    .prologue
    .line 691
    iget-boolean v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCanPause:Z

    return v0
.end method

.method public canSeekBackward()Z
    .locals 1

    .prologue
    .line 696
    iget-boolean v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCanSeekBack:Z

    return v0
.end method

.method public canSeekForward()Z
    .locals 1

    .prologue
    .line 701
    iget-boolean v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCanSeekForward:Z

    return v0
.end method

.method public getAudioSessionId()I
    .locals 2

    .prologue
    .line 706
    iget v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mAudioSession:I

    if-nez v1, :cond_0

    .line 707
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    .line 708
    .local v0, "foo":Landroid/media/MediaPlayer;
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v1

    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mAudioSession:I

    .line 709
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 711
    .end local v0    # "foo":Landroid/media/MediaPlayer;
    :cond_0
    iget v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mAudioSession:I

    return v1
.end method

.method public getBufferPercentage()I
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 677
    iget v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentBufferPercentage:I

    .line 679
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 639
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 642
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPositionInSeconds()I
    .locals 1

    .prologue
    .line 646
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->getCurrentPosition()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method public getCurrentStream()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 627
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    .line 631
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 671
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 134
    invoke-super {p0, p1}, Landroid/view/TextureView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 135
    const-class v0, Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 136
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/view/TextureView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 141
    const-class v0, Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 142
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 558
    const/4 v2, 0x4

    if-eq p1, v2, :cond_2

    const/16 v2, 0x18

    if-eq p1, v2, :cond_2

    const/16 v2, 0x19

    if-eq p1, v2, :cond_2

    const/16 v2, 0xa4

    if-eq p1, v2, :cond_2

    const/16 v2, 0x52

    if-eq p1, v2, :cond_2

    const/4 v2, 0x5

    if-eq p1, v2, :cond_2

    const/4 v2, 0x6

    if-eq p1, v2, :cond_2

    move v0, v1

    .line 565
    .local v0, "isKeyCodeSupported":Z
    :goto_0
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isInPlaybackState()Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz v0, :cond_8

    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    if-eqz v2, :cond_8

    .line 566
    const/16 v2, 0x4f

    if-eq p1, v2, :cond_0

    const/16 v2, 0x55

    if-ne p1, v2, :cond_4

    .line 567
    :cond_0
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 568
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->pause()V

    .line 569
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->showMediaController()V

    .line 592
    :cond_1
    :goto_1
    return v1

    .line 558
    .end local v0    # "isKeyCodeSupported":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 571
    .restart local v0    # "isKeyCodeSupported":Z
    :cond_3
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->start()V

    .line 572
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->hideMediaController()V

    goto :goto_1

    .line 575
    :cond_4
    const/16 v2, 0x7e

    if-ne p1, v2, :cond_5

    .line 576
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_1

    .line 577
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->start()V

    .line 578
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->hideMediaController()V

    goto :goto_1

    .line 581
    :cond_5
    const/16 v2, 0x56

    if-eq p1, v2, :cond_6

    const/16 v2, 0x7f

    if-ne p1, v2, :cond_7

    .line 582
    :cond_6
    iget-object v2, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 583
    invoke-virtual {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->pause()V

    .line 584
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->showMediaController()V

    goto :goto_1

    .line 588
    :cond_7
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-interface {v1}, Lcom/malmstein/fenster/controller/FensterPlayerController;->show()V

    .line 592
    :cond_8
    invoke-super {p0, p1, p2}, Landroid/view/TextureView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 128
    iget-object v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->videoSizeCalculator:Lcom/malmstein/fenster/view/VideoSizeCalculator;

    invoke-virtual {v1, p1, p2}, Lcom/malmstein/fenster/view/VideoSizeCalculator;->measure(II)Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;

    move-result-object v0

    .line 129
    .local v0, "dimens":Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;
    invoke-virtual {v0}, Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/VideoSizeCalculator$Dimens;->getHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setMeasuredDimension(II)V

    .line 130
    return-void
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 550
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    invoke-interface {v0}, Lcom/malmstein/fenster/controller/FensterPlayerController;->show()V

    .line 553
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 607
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 610
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    .line 611
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->setKeepScreenOn(Z)V

    .line 614
    :cond_0
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mTargetState:I

    .line 615
    return-void
.end method

.method public resolveAdjustedSize(II)I
    .locals 1
    .param p1, "desiredSize"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 145
    invoke-static {p1, p2}, Lcom/malmstein/fenster/view/FensterVideoView;->getDefaultSize(II)I

    move-result v0

    return v0
.end method

.method public resume()V
    .locals 0

    .prologue
    .line 622
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->openVideo()V

    .line 623
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "millis"    # I

    .prologue
    .line 651
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 652
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 653
    const/4 v0, 0x0

    iput v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSeekWhenPrepared:I

    .line 657
    :goto_0
    return-void

    .line 655
    :cond_0
    iput p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mSeekWhenPrepared:I

    goto :goto_0
.end method

.method public seekToSeconds(I)V
    .locals 2
    .param p1, "seconds"    # I

    .prologue
    .line 660
    mul-int/lit16 v0, p1, 0x3e8

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    .line 661
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/malmstein/fenster/view/FensterVideoView$10;

    invoke-direct {v1, p0}, Lcom/malmstein/fenster/view/FensterVideoView$10;-><init>(Lcom/malmstein/fenster/view/FensterVideoView;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 667
    return-void
.end method

.method public setMediaController(Lcom/malmstein/fenster/controller/FensterPlayerController;)V
    .locals 0
    .param p1, "controller"    # Lcom/malmstein/fenster/controller/FensterPlayerController;

    .prologue
    .line 256
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->hideMediaController()V

    .line 257
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->fensterPlayerController:Lcom/malmstein/fenster/controller/FensterPlayerController;

    .line 258
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->attachMediaController()V

    .line 259
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0
    .param p1, "l"    # Landroid/media/MediaPlayer$OnCompletionListener;

    .prologue
    .line 475
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 476
    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0
    .param p1, "l"    # Landroid/media/MediaPlayer$OnErrorListener;

    .prologue
    .line 487
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 488
    return-void
.end method

.method public setOnPlayStateListener(Lcom/malmstein/fenster/play/FensterVideoStateListener;)V
    .locals 0
    .param p1, "onPlayStateListener"    # Lcom/malmstein/fenster/play/FensterVideoStateListener;

    .prologue
    .line 746
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->onPlayStateListener:Lcom/malmstein/fenster/play/FensterVideoStateListener;

    .line 747
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0
    .param p1, "l"    # Landroid/media/MediaPlayer$OnPreparedListener;

    .prologue
    .line 465
    iput-object p1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 466
    return-void
.end method

.method public setVideo(Landroid/net/Uri;I)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "seekInSeconds"    # I

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/malmstein/fenster/view/FensterVideoView;->setVideoURI(Landroid/net/Uri;Ljava/util/Map;I)V

    .line 171
    return-void
.end method

.method public setVideo(Ljava/lang/String;I)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "seekInSeconds"    # I

    .prologue
    .line 166
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p2}, Lcom/malmstein/fenster/view/FensterVideoView;->setVideoURI(Landroid/net/Uri;Ljava/util/Map;I)V

    .line 167
    return-void
.end method

.method public setVideoFromBeginning(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setVideo(Landroid/net/Uri;I)V

    .line 163
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 597
    invoke-direct {p0}, Lcom/malmstein/fenster/view/FensterVideoView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 599
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->setKeepScreenOn(Z)V

    .line 600
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    .line 602
    :cond_0
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mTargetState:I

    .line 603
    return-void
.end method

.method public stopPlayback()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 188
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 190
    iget-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 192
    invoke-virtual {p0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setKeepScreenOn(Z)V

    .line 193
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mCurrentState:I

    .line 194
    iput v1, p0, Lcom/malmstein/fenster/view/FensterVideoView;->mTargetState:I

    .line 196
    :cond_0
    return-void
.end method

.method public suspend()V
    .locals 1

    .prologue
    .line 618
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/malmstein/fenster/view/FensterVideoView;->release(Z)V

    .line 619
    return-void
.end method
