.class Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$7;
.super Ljava/lang/Object;
.source "UpdateWatchActivity.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->requestLastVersionFull()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/SoftwareUpdateModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 733
    iput-object p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$7;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 5
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 751
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v2

    invoke-virtual {v2}, Lretrofit/client/Response;->getStatus()I

    move-result v2

    const/16 v3, 0x196

    if-ne v2, v3, :cond_0

    .line 752
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getBody()Ljava/lang/Object;

    move-result-object v0

    .line 754
    .local v0, "errorBody":Ljava/lang/Object;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;

    const-string v4, "all"

    invoke-direct {v3, v0, v4}, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 760
    .end local v0    # "errorBody":Ljava/lang/Object;
    :goto_0
    return-void

    .line 756
    :cond_0
    new-instance v1, Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;-><init>()V

    .line 757
    .local v1, "systemUpdate":Lcom/vectorwatch/android/models/SoftwareUpdateModel;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->setErrorDiscovered(Z)V

    .line 758
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;

    invoke-direct {v3, v1}, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;-><init>(Lcom/vectorwatch/android/models/SoftwareUpdateModel;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public success(Lcom/vectorwatch/android/models/SoftwareUpdateModel;Lretrofit/client/Response;)V
    .locals 3
    .param p1, "softwareUpdateModel"    # Lcom/vectorwatch/android/models/SoftwareUpdateModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 736
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getErrorMessage()Lcom/vectorwatch/android/events/CloudMessageModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 737
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getErrorMessage()Lcom/vectorwatch/android/events/CloudMessageModel;

    move-result-object v0

    .line 739
    .local v0, "cloudMessage":Lcom/vectorwatch/android/events/CloudMessageModel;
    invoke-virtual {v0}, Lcom/vectorwatch/android/events/CloudMessageModel;->getCode()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-virtual {v2}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 740
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;

    invoke-direct {v2, p1}, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;-><init>(Lcom/vectorwatch/android/models/SoftwareUpdateModel;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 747
    .end local v0    # "cloudMessage":Lcom/vectorwatch/android/events/CloudMessageModel;
    :goto_0
    return-void

    .line 742
    .restart local v0    # "cloudMessage":Lcom/vectorwatch/android/events/CloudMessageModel;
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;

    invoke-direct {v2, v0}, Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;-><init>(Lcom/vectorwatch/android/events/CloudMessageModel;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 745
    .end local v0    # "cloudMessage":Lcom/vectorwatch/android/events/CloudMessageModel;
    :cond_1
    const-string v1, "Unexpected"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$7;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 733
    check-cast p1, Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$7;->success(Lcom/vectorwatch/android/models/SoftwareUpdateModel;Lretrofit/client/Response;)V

    return-void
.end method
