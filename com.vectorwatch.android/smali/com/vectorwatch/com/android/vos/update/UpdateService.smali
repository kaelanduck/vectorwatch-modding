.class public Lcom/vectorwatch/com/android/vos/update/UpdateService;
.super Landroid/app/IntentService;
.source "UpdateService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;,
        Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;,
        Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    }
.end annotation


# static fields
.field private static final PREFS_SYS_UPDATE_READY:Ljava/lang/String; = "system_update_ready"

.field private static final PREFS_SYS_UPDATE_TYPE:Ljava/lang/String; = "system_update_type"

.field public static final STATUS_ERROR:I = 0x2

.field public static final STATUS_FINISHED:I = 0x1

.field public static final STATUS_FINISHED_PART:I = 0x7

.field public static final STATUS_NOT_CONNECTED:I = 0x4

.field public static final STATUS_NOT_SET:I = -0x1

.field public static final STATUS_RUNNING:I = 0x0

.field public static final STATUS_SET_INSTALLING_UPDATE_IN_PROGRESS:I = 0x9

.field public static final STATUS_UPDATE_PROGRESS:I = 0x3

.field public static final STATUS_VERIFY_UPDATE:I = 0x6

.field public static final STATUS_WAIT_WATCH:I = 0x5

.field public static final STATUS_WAIT_WATCH_FOR_VERIFY_UPDATE:I = 0x8

.field private static final TAG:Ljava/lang/String; = "UpdateIntentService"

.field private static final VECTOR_PREF_FILE:Ljava/lang/String; = "vector_preferences"

.field private static final WAIT_BEFORE_REBOOT_DETECTED:I = 0x222e0

.field private static final WAIT_BEFORE_REBOOT_DETECTED_AFTER_FORCE_CONNECT:I = 0x7530

.field private static final WAIT_BEFORE_SYSTEM_INFO_RECEIVED:I = 0x9c40

.field private static final WAIT_BETWEEN_UPDATE_ACK:I = 0x9c40

.field private static final log:Lorg/slf4j/Logger;

.field private static sHasSentForceConnect:Z

.field private static sUpdateInProgress:Z


# instance fields
.field private currentWatchSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo;

.field private mUpdateVersionName:Ljava/lang/String;

.field private receiver:Landroid/os/ResultReceiver;

.field private softwareUpdate:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

.field private updateProgress:I

.field private updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

.field private updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

.field private waitAfterForceConnect:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

.field private waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

.field private waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    const-class v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    .line 65
    sput-boolean v1, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    .line 67
    sput-boolean v1, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sHasSentForceConnect:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    const-class v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 60
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateService;Lcom/vectorwatch/com/android/vos/update/UpdateService$1;)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    .line 61
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateService;Lcom/vectorwatch/com/android/vos/update/UpdateService$1;)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    .line 62
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateService;Lcom/vectorwatch/com/android/vos/update/UpdateService$1;)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    .line 63
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateService;Lcom/vectorwatch/com/android/vos/update/UpdateService$1;)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitAfterForceConnect:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    .line 73
    return-void
.end method

.method private prepareWaitForReconnect()V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->NONE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->setType(Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;)V

    .line 341
    return-void
.end method

.method private prepareWaitForSystemInfo()V
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->NONE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->setType(Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;)V

    .line 349
    return-void
.end method

.method private sendFinishedStatus(Lcom/vectorwatch/android/models/SoftwareUpdateData;II)V
    .locals 4
    .param p1, "updateData"    # Lcom/vectorwatch/android/models/SoftwareUpdateData;
    .param p2, "crtPart"    # I
    .param p3, "totalParts"    # I

    .prologue
    const/4 v2, 0x1

    .line 408
    if-ne p2, p3, :cond_0

    move v1, v2

    .line 410
    .local v1, "isLast":Z
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 411
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "software_update"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 413
    if-eqz v1, :cond_1

    .line 415
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    invoke-virtual {v3, v2, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 420
    :goto_1
    return-void

    .line 408
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "isLast":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 418
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "isLast":Z
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto :goto_1
.end method

.method private verifyUpdate(Ljava/lang/String;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;)Z
    .locals 7
    .param p1, "updateToBeMade"    # Ljava/lang/String;
    .param p2, "updateType"    # Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 431
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->prepareWaitForSystemInfo()V

    .line 434
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: Send get system info."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 435
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSystemInfoRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 437
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v4, 0x6

    invoke-virtual {v3, v4, v6}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 438
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForWatchSystemInfo()Z

    move-result v3

    if-nez v3, :cond_0

    .line 439
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: At VERIFY UPDATE - system info not received."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 476
    :goto_0
    return v2

    .line 444
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "last_known_system_info"

    const-class v5, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 445
    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 446
    .local v0, "updatedSystemInfo":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UPDATE: crt watch info = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 447
    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v5

    .line 448
    invoke-virtual {v5}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 446
    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 450
    const/4 v1, 0x0

    .line 452
    .local v1, "versionShownOnWatch":Ljava/lang/String;
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne p2, v3, :cond_3

    .line 453
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "kernel"

    invoke-static {v0, v4}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 459
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    .line 460
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 461
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 467
    :cond_2
    :goto_2
    if-eqz p1, :cond_5

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 476
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 454
    :cond_3
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne p2, v3, :cond_1

    .line 455
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "bootloader"

    invoke-static {v0, v4}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 462
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-le v3, v4, :cond_2

    .line 463
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 470
    :cond_5
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UPDATE: At VERIFY UPDATE - version on watch is not the same as the one installed: W = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " C = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 472
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v4, 0x2

    invoke-virtual {v3, v4, v6}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method private waitForPossibleReconnectAtForceConnect()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 529
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitAfterForceConnect:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    monitor-enter v3

    .line 531
    const/4 v4, 0x1

    :try_start_0
    sput-boolean v4, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sHasSentForceConnect:Z

    .line 534
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v4

    if-nez v4, :cond_0

    .line 536
    sget-object v4, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v5, "UPDATE: Force reconnect (before verify)"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 537
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->connect(Landroid/content/Context;)V

    .line 540
    :cond_0
    sget-object v4, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v5, "UPDATE: wait for reboot detected after force connect"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 541
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitAfterForceConnect:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    const-wide/16 v6, 0x7530

    invoke-virtual {v4, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 546
    :goto_0
    :try_start_1
    sget-object v4, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UPDATE: timeout expired for reboot detected after force connect - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitAfterForceConnect:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    .line 547
    invoke-virtual {v6}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getType()Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 546
    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 548
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitAfterForceConnect:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v4}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getType()Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->REBOOT_DETECTED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    if-eq v4, v5, :cond_1

    .line 549
    const/4 v2, 0x0

    sput-boolean v2, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sHasSentForceConnect:Z

    .line 550
    monitor-exit v3

    .line 554
    :goto_1
    return v1

    .line 542
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 552
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_1
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 553
    sput-boolean v1, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sHasSentForceConnect:Z

    move v1, v2

    .line 554
    goto :goto_1
.end method

.method private waitForWatchReconnectEvent()Z
    .locals 6

    .prologue
    .line 509
    iget-object v2, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    monitor-enter v2

    .line 511
    :try_start_0
    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v3, "UPDATE: wait for reboot detected"

    invoke-interface {v1, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 512
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    const-wide/32 v4, 0x222e0

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    :goto_0
    :try_start_1
    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UPDATE: timeout expired for reboot detected - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v4}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getType()Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 518
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getType()Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    move-result-object v1

    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->REBOOT_DETECTED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    if-eq v1, v3, :cond_0

    .line 519
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForPossibleReconnectAtForceConnect()Z

    move-result v1

    if-nez v1, :cond_0

    .line 520
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 521
    const/4 v1, 0x0

    monitor-exit v2

    .line 525
    :goto_1
    return v1

    .line 513
    :catch_0
    move-exception v0

    .line 514
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 524
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 525
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private waitForWatchSystemInfo()Z
    .locals 6

    .prologue
    .line 491
    iget-object v2, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    monitor-enter v2

    .line 493
    :try_start_0
    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v3, "UPDATE: wait for system info"

    invoke-interface {v1, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 494
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    const-wide/32 v4, 0x9c40

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    :goto_0
    :try_start_1
    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UPDATE: timeout expired for system info - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v4}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getType()Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 500
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getType()Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    move-result-object v1

    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->SYSTEM_INFO_RECEIVED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    if-eq v1, v3, :cond_0

    .line 501
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 502
    const/4 v1, 0x0

    monitor-exit v2

    .line 505
    :goto_1
    return v1

    .line 495
    :catch_0
    move-exception v0

    .line 496
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 504
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 505
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private writeUpdateDetailsToFile(Lcom/vectorwatch/android/models/SoftwareUpdateData;Landroid/content/Context;)V
    .locals 15
    .param p1, "update"    # Lcom/vectorwatch/android/models/SoftwareUpdateData;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 352
    const/4 v7, 0x0

    .line 353
    .local v7, "output":Ljava/io/FileOutputStream;
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getContentBase64()Ljava/lang/String;

    move-result-object v1

    .line 355
    .local v1, "base64Data":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->fromString(Ljava/lang/String;)Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    move-result-object v10

    .line 358
    .local v10, "updateType":Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    :try_start_0
    const-string v11, "update.vos"

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v7

    .line 360
    const/4 v6, 0x0

    .line 362
    .local v6, "index":I
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    array-length v9, v11

    .line 363
    .local v9, "totalBytes":I
    :goto_0
    if-ge v6, v9, :cond_1

    .line 365
    sub-int v11, v9, v6

    const/16 v12, 0x400

    if-ge v11, v12, :cond_0

    sub-int v2, v9, v6

    .line 369
    .local v2, "dataToDecodeLength":I
    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v11, v6, v2, v12}, Landroid/util/Base64;->decode([BIII)[B

    move-result-object v3

    .line 373
    .local v3, "dataToWrite":[B
    const/4 v11, 0x0

    array-length v12, v3

    invoke-virtual {v7, v3, v11, v12}, Ljava/io/FileOutputStream;->write([BII)V

    .line 374
    add-int/2addr v6, v2

    .line 375
    goto :goto_0

    .line 365
    .end local v2    # "dataToDecodeLength":I
    .end local v3    # "dataToWrite":[B
    :cond_0
    const/16 v2, 0x400

    goto :goto_1

    .line 376
    :cond_1
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V

    .line 380
    const-string v11, "vector_preferences"

    const/4 v12, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 381
    .local v8, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 382
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v11, "system_update_ready"

    const/4 v12, 0x1

    invoke-interface {v5, v11, v12}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 383
    const-string v11, "system_update_type"

    invoke-virtual {v10}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->ordinal()I

    move-result v12

    invoke-interface {v5, v11, v12}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 384
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    if-eqz v7, :cond_2

    .line 392
    :try_start_1
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 398
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v6    # "index":I
    .end local v8    # "sharedPrefs":Landroid/content/SharedPreferences;
    .end local v9    # "totalBytes":I
    :cond_2
    :goto_2
    return-void

    .line 393
    .restart local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v6    # "index":I
    .restart local v8    # "sharedPrefs":Landroid/content/SharedPreferences;
    .restart local v9    # "totalBytes":I
    :catch_0
    move-exception v4

    .line 394
    .local v4, "e":Ljava/io/IOException;
    const-string v11, "Software update"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to close output stream: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 385
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v6    # "index":I
    .end local v8    # "sharedPrefs":Landroid/content/SharedPreferences;
    .end local v9    # "totalBytes":I
    :catch_1
    move-exception v4

    .line 386
    .local v4, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    sget-object v11, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "UpdateWatchActivity - File download failed: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 390
    if-eqz v7, :cond_2

    .line 392
    :try_start_3
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 393
    :catch_2
    move-exception v4

    .line 394
    .local v4, "e":Ljava/io/IOException;
    const-string v11, "Software update"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to close output stream: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 387
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 388
    .restart local v4    # "e":Ljava/io/IOException;
    :try_start_4
    sget-object v11, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "UpdateWatchActivity - Writing update to file failed: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 390
    if-eqz v7, :cond_2

    .line 392
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_2

    .line 393
    :catch_4
    move-exception v4

    .line 394
    const-string v11, "Software update"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to close output stream: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 390
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    if-eqz v7, :cond_3

    .line 392
    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 395
    :cond_3
    :goto_3
    throw v11

    .line 393
    :catch_5
    move-exception v4

    .line 394
    .restart local v4    # "e":Ljava/io/IOException;
    const-string v12, "Software update"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Failed to close output stream: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method


# virtual methods
.method public handleBleTrulyConnectedReceivedEvent(Lcom/vectorwatch/android/events/BleTrulyConnectedReceivedEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/BleTrulyConnectedReceivedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v1, "UPDATE: handle Ble truly connected event."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v1, "UPDATE: Reboot detected."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 95
    sget-boolean v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sHasSentForceConnect:Z

    if-nez v0, :cond_0

    .line 96
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    monitor-enter v1

    .line 97
    :try_start_0
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->REBOOT_DETECTED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->setType(Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;)V

    .line 98
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForReconnectEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 99
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :cond_0
    sget-boolean v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sHasSentForceConnect:Z

    if-eqz v0, :cond_1

    .line 103
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v1, "UPDATE: Reboot detected after force connect."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitAfterForceConnect:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    monitor-enter v1

    .line 105
    :try_start_1
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitAfterForceConnect:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->REBOOT_DETECTED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->setType(Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;)V

    .line 106
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitAfterForceConnect:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 107
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 110
    :cond_1
    return-void

    .line 99
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 107
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public handleUpdateProgress(Lcom/vectorwatch/android/events/UpdateProgress;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/UpdateProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 114
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/UpdateProgress;->getUpdateStatus()Lcom/vectorwatch/com/android/vos/update/UpdateStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->getProgress()I

    move-result v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/UpdateProgress;->getUpdateStatus()Lcom/vectorwatch/com/android/vos/update/UpdateStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->getStatus()Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->setUpdateProgress(ILcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;)V

    .line 116
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UpdateWatchActivity - Progress update "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/UpdateProgress;->getUpdateStatus()Lcom/vectorwatch/com/android/vos/update/UpdateStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->getProgress()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public handleWatchSystemInfoReceivedEvent(Lcom/vectorwatch/android/events/WatchSystemInfoReceivedEvent;)V
    .locals 3
    .param p1, "systemInfo"    # Lcom/vectorwatch/android/events/WatchSystemInfoReceivedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 481
    sget-boolean v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    if-eqz v0, :cond_0

    .line 482
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v1, "UPDATE: System info received while update in progress."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 483
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    monitor-enter v1

    .line 484
    :try_start_0
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->SYSTEM_INFO_RECEIVED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->setType(Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;)V

    .line 485
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForSystemInfoEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 486
    monitor-exit v1

    .line 488
    :cond_0
    return-void

    .line 486
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 78
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 84
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 85
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 132
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v8, "UPDATE: Service started."

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 133
    const-string v7, "receiver"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/os/ResultReceiver;

    iput-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    .line 134
    const-string v7, "deviceInfo"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    iput-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->currentWatchSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 135
    const-string v7, "lastSynced"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    iput-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->softwareUpdate:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 137
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v6, "updates":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SoftwareUpdateData;>;"
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->softwareUpdate:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getData()Lcom/vectorwatch/android/models/UpdateData;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 140
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->softwareUpdate:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getData()Lcom/vectorwatch/android/models/UpdateData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/UpdateData;->getImages()Ljava/util/List;

    move-result-object v6

    .line 143
    :cond_0
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_16

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/SoftwareUpdateData;

    .line 144
    .local v5, "update":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    .line 145
    .local v4, "totalParts":I
    invoke-interface {v6, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v7

    add-int/lit8 v1, v7, 0x1

    .line 147
    .local v1, "crtPart":I
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "UPDATE: Crt update = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " of total = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ". Update type = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 148
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 147
    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 150
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v7

    if-nez v7, :cond_1

    .line 151
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v8, 0x4

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v7, v8, v9}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 153
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v8, "WatchUpdateThread - Watch not connected, exit update"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->stopSelf()V

    .line 334
    .end local v1    # "crtPart":I
    .end local v4    # "totalParts":I
    .end local v5    # "update":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    :goto_1
    return-void

    .line 159
    .restart local v1    # "crtPart":I
    .restart local v4    # "totalParts":I
    .restart local v5    # "update":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    :cond_1
    const-string v7, "crt_install_part"

    invoke-static {v7, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 160
    const-string v7, "total_install_parts"

    invoke-static {v7, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 164
    const/4 v7, 0x1

    :try_start_0
    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    .line 167
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->fromString(Ljava/lang/String;)Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    move-result-object v7

    iput-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    .line 169
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-eq v7, v9, :cond_2

    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-eq v7, v9, :cond_2

    .line 170
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "Unknown update type."

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 325
    :catch_0
    move-exception v2

    .line 326
    .local v2, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "UPDATE: Status broken - Single update went wrong - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 327
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v9, 0x2

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 328
    const/4 v7, 0x0

    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    goto/16 :goto_0

    .line 177
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v7, 0x1

    if-ne v1, v7, :cond_3

    .line 180
    :try_start_1
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->prepareWaitForSystemInfo()V

    .line 183
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: Send get system info."

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 184
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSystemInfoRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 187
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: wait for system info."

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 188
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v9, 0x5

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 189
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForWatchSystemInfo()Z

    move-result v7

    if-nez v7, :cond_3

    .line 190
    const/4 v7, 0x0

    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    .line 191
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: system info not received."

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->stopSelf()V

    goto/16 :goto_1

    .line 200
    :cond_3
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/16 v9, 0x9

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 202
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v7

    const-string v9, "last_known_system_info"

    const-class v10, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 203
    invoke-virtual {v7, v9, v10}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    iput-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->currentWatchSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 206
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v9, :cond_7

    .line 207
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: send START_KERNEL_UPDATE cmd"

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 209
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->prepareWaitForReconnect()V

    .line 211
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendKernelUpdateStart(Landroid/content/Context;)Ljava/util/UUID;

    .line 221
    :cond_4
    :goto_2
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v9, :cond_5

    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->currentWatchSystemInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-virtual {v7}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getCurrent()B

    move-result v7

    const/4 v9, 0x2

    if-eq v7, v9, :cond_6

    :cond_5
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v9, :cond_8

    .line 224
    :cond_6
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: wait for watch disconnect"

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 227
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForWatchReconnectEvent()Z

    move-result v7

    if-nez v7, :cond_8

    .line 228
    const/4 v7, 0x0

    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    .line 229
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: timeout expired for wait for watch to reconnect."

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 230
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->stopSelf()V

    goto/16 :goto_1

    .line 212
    :cond_7
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v9, :cond_4

    .line 213
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: send START_BOOTLOADER_UPDATE cmd"

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 215
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->prepareWaitForReconnect()V

    .line 217
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBootloaderUpdateStart(Landroid/content/Context;)Ljava/util/UUID;

    goto :goto_2

    .line 235
    :cond_8
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 238
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getVersion()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->mUpdateVersionName:Ljava/lang/String;

    .line 240
    invoke-direct {p0, v5, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->writeUpdateDetailsToFile(Lcom/vectorwatch/android/models/SoftwareUpdateData;Landroid/content/Context;)V

    .line 243
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, "update.vos"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 244
    .local v3, "pathToFile":Ljava/lang/String;
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v9, :cond_f

    .line 245
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: send UPDATE KERNEL cmd"

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 246
    invoke-static {p0, v3}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendKernelUpdate(Landroid/content/Context;Ljava/lang/String;)Ljava/util/UUID;

    .line 252
    :cond_9
    :goto_3
    const/4 v7, 0x0

    iput v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    .line 253
    :goto_4
    iget v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    const/16 v9, 0x64

    if-ge v7, v9, :cond_c

    .line 254
    iget-object v9, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    monitor-enter v9
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 255
    :try_start_2
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    sget-object v10, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->NONE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    invoke-virtual {v7, v10}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->setType(Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 257
    :try_start_3
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    const-wide/32 v10, 0x9c40

    invoke-virtual {v7, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 263
    :goto_5
    :try_start_4
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "UPDATE: Got notify and type is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v11}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getType()Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    .line 264
    invoke-virtual {v11}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getValue()Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 263
    invoke-interface {v7, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 265
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v7}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getType()Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    move-result-object v7

    sget-object v10, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->PROGRESS_AVAILABLE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    if-ne v7, v10, :cond_10

    .line 266
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v7}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->getValue()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    .line 267
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v10, "WatchUpdateThread - progress "

    invoke-interface {v7, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 269
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 270
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v7, "update"

    iget v10, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 271
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v10, 0x3

    invoke-virtual {v7, v10, v0}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 274
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v10, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v10, :cond_a

    iget v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    const/16 v10, 0x63

    if-ge v7, v10, :cond_b

    :cond_a
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v10, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v10, :cond_11

    iget v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    const/16 v10, 0x62

    if-lt v7, v10, :cond_11

    .line 278
    :cond_b
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->prepareWaitForReconnect()V

    .line 280
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 292
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_c
    :try_start_5
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v9, :cond_d

    iget v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    const/16 v9, 0x63

    if-ge v7, v9, :cond_e

    :cond_d
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v9, :cond_15

    iget v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    const/16 v9, 0x62

    if-lt v7, v9, :cond_15

    .line 297
    :cond_e
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 299
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->waitForWatchReconnectEvent()Z

    move-result v7

    if-nez v7, :cond_12

    .line 300
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: At VERIFY UPDATE - timeout expired for wait for watch to reconnect."

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 301
    const/4 v7, 0x0

    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    .line 302
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->stopSelf()V

    goto/16 :goto_1

    .line 247
    :cond_f
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v9, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    if-ne v7, v9, :cond_9

    .line 248
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: send UPDATE BOOTLOADER cmd"

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 249
    invoke-static {p0, v3}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBootloaderUpdate(Landroid/content/Context;Ljava/lang/String;)Ljava/util/UUID;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_3

    .line 258
    :catch_1
    move-exception v2

    .line 259
    .restart local v2    # "e":Ljava/lang/Exception;
    const/4 v7, 0x0

    :try_start_6
    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    .line 260
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5

    .line 289
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v7
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    .line 283
    :cond_10
    :try_start_8
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    .line 284
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v10, "UPDATE: Status broken - Expected to have progress event but it isn\'t."

    invoke-interface {v7, v10}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 285
    const/4 v7, 0x0

    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    .line 286
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->stopSelf()V

    .line 287
    monitor-exit v9

    goto/16 :goto_1

    .line 289
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_11
    monitor-exit v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_4

    .line 306
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_12
    :try_start_9
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->mUpdateVersionName:Ljava/lang/String;

    iget-object v9, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateType:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    invoke-direct {p0, v7, v9}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->verifyUpdate(Ljava/lang/String;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;)Z

    move-result v7

    if-nez v7, :cond_13

    .line 308
    const/4 v7, 0x0

    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    .line 309
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->stopSelf()V

    goto/16 :goto_1

    .line 313
    :cond_13
    invoke-direct {p0, v5, v1, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sendFinishedStatus(Lcom/vectorwatch/android/models/SoftwareUpdateData;II)V

    .line 315
    const/4 v7, 0x0

    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    .line 323
    :cond_14
    :goto_6
    const/4 v7, 0x0

    sput-boolean v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->sUpdateInProgress:Z

    goto/16 :goto_0

    .line 317
    :cond_15
    iget v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgress:I

    const/16 v9, 0x62

    if-ge v7, v9, :cond_14

    .line 318
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v9, "UPDATE: Status broken - Update progress less than 98%"

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 319
    iget-object v7, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->receiver:Landroid/os/ResultReceiver;

    const/4 v9, 0x2

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    goto :goto_6

    .line 332
    .end local v1    # "crtPart":I
    .end local v3    # "pathToFile":Ljava/lang/String;
    .end local v4    # "totalParts":I
    .end local v5    # "update":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    :cond_16
    sget-object v7, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    const-string v8, "UPDATE: Service stopping."

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 333
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateService;->stopSelf()V

    goto/16 :goto_1
.end method

.method public setUpdateProgress(ILcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;)V
    .locals 4
    .param p1, "progress"    # I
    .param p2, "status"    # Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .prologue
    .line 120
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    monitor-enter v1

    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->setValue(Ljava/lang/Integer;)V

    .line 122
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {p2}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->setErrorNo(Ljava/lang/Integer;)V

    .line 123
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->PROGRESS_AVAILABLE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->setType(Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;)V

    .line 124
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->updateProgressEvent:Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 125
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPDATE: WatchUpdateThread - Received notify on progress "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->getValue()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 126
    monitor-exit v1

    .line 127
    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
