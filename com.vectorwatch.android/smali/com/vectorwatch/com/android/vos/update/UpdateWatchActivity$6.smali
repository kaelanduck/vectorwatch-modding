.class Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$6;
.super Ljava/lang/Object;
.source "UpdateWatchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateCompletedScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 652
    iput-object p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$6;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 655
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$6;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    # invokes: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->triggerUpdatesWithCloud()V
    invoke-static {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$1200(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    .line 656
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$6;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    const-class v2, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 657
    .local v0, "updateIntent":Landroid/content/Intent;
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 658
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 659
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 660
    const-string v1, "force_ota"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 661
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$6;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 662
    return-void
.end method
