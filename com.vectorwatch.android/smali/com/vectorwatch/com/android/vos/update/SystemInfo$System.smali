.class public Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;
.super Ljava/lang/Object;
.source "SystemInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/com/android/vos/update/SystemInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "System"
.end annotation


# instance fields
.field private buildVersion:I

.field private hotfixVersion:I

.field private majorVersion:I

.field private minorVersion:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "majorVersion"    # I
    .param p2, "minorVersion"    # I
    .param p3, "hotfixVersion"    # I
    .param p4, "buildVersion"    # I

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput p1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->majorVersion:I

    .line 84
    iput p2, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->minorVersion:I

    .line 85
    iput p3, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->hotfixVersion:I

    .line 86
    iput p4, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->buildVersion:I

    .line 87
    return-void
.end method


# virtual methods
.method public getBuildVersion()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->buildVersion:I

    return v0
.end method

.method public getHotfixVersion()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->hotfixVersion:I

    return v0
.end method

.method public getMajorVersion()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->majorVersion:I

    return v0
.end method

.method public getMinorVersion()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->minorVersion:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->majorVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->minorVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->hotfixVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->buildVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
