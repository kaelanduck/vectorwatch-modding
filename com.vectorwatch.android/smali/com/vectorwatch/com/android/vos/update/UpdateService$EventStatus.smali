.class Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;
.super Ljava/lang/Object;
.source "UpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/com/android/vos/update/UpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventStatus"
.end annotation


# instance fields
.field private errorNo:Ljava/lang/Integer;

.field final synthetic this$0:Lcom/vectorwatch/com/android/vos/update/UpdateService;

.field private type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

.field private value:Ljava/lang/Integer;


# direct methods
.method private constructor <init>(Lcom/vectorwatch/com/android/vos/update/UpdateService;)V
    .locals 1

    .prologue
    .line 598
    iput-object p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 600
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->NONE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    return-void
.end method

.method synthetic constructor <init>(Lcom/vectorwatch/com/android/vos/update/UpdateService;Lcom/vectorwatch/com/android/vos/update/UpdateService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateService;
    .param p2, "x1"    # Lcom/vectorwatch/com/android/vos/update/UpdateService$1;

    .prologue
    .line 598
    invoke-direct {p0, p1}, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateService;)V

    return-void
.end method


# virtual methods
.method public getType()Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    return-object v0
.end method

.method public getValue()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->value:Ljava/lang/Integer;

    return-object v0
.end method

.method public setErrorNo(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "errorNo"    # Ljava/lang/Integer;

    .prologue
    .line 621
    iput-object p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->errorNo:Ljava/lang/Integer;

    .line 622
    return-void
.end method

.method public setType(Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;)V
    .locals 0
    .param p1, "type"    # Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    .prologue
    .line 609
    iput-object p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->type:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    .line 610
    return-void
.end method

.method public setValue(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/Integer;

    .prologue
    .line 617
    iput-object p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$EventStatus;->value:Ljava/lang/Integer;

    .line 618
    return-void
.end method
