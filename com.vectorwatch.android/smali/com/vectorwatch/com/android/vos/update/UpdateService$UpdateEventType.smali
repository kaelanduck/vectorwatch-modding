.class public final enum Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;
.super Ljava/lang/Enum;
.source "UpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/com/android/vos/update/UpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpdateEventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

.field public static final enum NONE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

.field public static final enum PROGRESS_AVAILABLE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

.field public static final enum REBOOT_DETECTED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

.field public static final enum SYSTEM_INFO_RECEIVED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

.field public static final enum UPDATE_FINISHED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 591
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->NONE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    .line 592
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    const-string v1, "REBOOT_DETECTED"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->REBOOT_DETECTED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    .line 593
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    const-string v1, "SYSTEM_INFO_RECEIVED"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->SYSTEM_INFO_RECEIVED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    .line 594
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    const-string v1, "UPDATE_FINISHED"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->UPDATE_FINISHED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    .line 595
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    const-string v1, "PROGRESS_AVAILABLE"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->PROGRESS_AVAILABLE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    .line 590
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->NONE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->REBOOT_DETECTED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->SYSTEM_INFO_RECEIVED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->UPDATE_FINISHED:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->PROGRESS_AVAILABLE:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->$VALUES:[Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 590
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 590
    const-class v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;
    .locals 1

    .prologue
    .line 590
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->$VALUES:[Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    invoke-virtual {v0}, [Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateEventType;

    return-object v0
.end method
