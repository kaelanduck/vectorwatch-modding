.class public Lcom/vectorwatch/com/android/vos/update/UpdateStatus;
.super Ljava/lang/Object;
.source "UpdateStatus.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
    }
.end annotation


# instance fields
.field private progress:I

.field private status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_NOT_INIT:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->progress:I

    .line 49
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "rawData"    # [B

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->progress:I

    .line 53
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    invoke-static {v0}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->fromInt(I)Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 54
    return-void
.end method


# virtual methods
.method public getProgress()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->progress:I

    return v0
.end method

.method public getStatus()Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    return-object v0
.end method

.method public serialize()[B
    .locals 2

    .prologue
    .line 68
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 69
    .local v0, "data":Ljava/nio/ByteBuffer;
    iget v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->progress:I

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 70
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->getValue()I

    move-result v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 72
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "progress: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->progress:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus;->status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
