.class Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;
.super Ljava/lang/Object;
.source "UpdateWatchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v3, 0x7f090283

    .line 190
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 191
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x7d0

    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .line 192
    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$200(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Landroid/content/Context;

    move-result-object v3

    .line 191
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$300(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "buttonLabel":Ljava/lang/String;
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090280

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 199
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandlerBatteryWait:Landroid/os/Handler;
    invoke-static {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$500(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRunnableBatteryWait:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$400(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x2710

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 200
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$200(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBatteryRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 201
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    # invokes: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createWaitForWatchScreen()V
    invoke-static {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$600(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    goto :goto_0

    .line 205
    :cond_2
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090281

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 207
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$200(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBatteryRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 209
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->isInternetEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 210
    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$700()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "UpdateWatchActivity - clicked on try again"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 211
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    # invokes: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createCheckForUpdatesScreen()V
    invoke-static {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$800(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    goto/16 :goto_0

    .line 213
    :cond_3
    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$700()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "UpdateWatchActivity - clicked on download & install - no internet"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 214
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    iget-object v2, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createErrorScreen(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$900(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 219
    :cond_4
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09027f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 221
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->isInternetEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 222
    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$700()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "UpdateWatchActivity - clicked on try again"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    # invokes: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createCheckForUpdatesScreen()V
    invoke-static {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$800(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    goto/16 :goto_0

    .line 225
    :cond_5
    # getter for: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$700()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "UpdateWatchActivity - clicked on download & install - no internet"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 226
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    iget-object v2, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createErrorScreen(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->access$900(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 231
    :cond_6
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09027d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;->this$0:Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    const-string v2, "com.vectorwatch.android"

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/OpenAppMarket;->openOnGooglePlayMarket(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
