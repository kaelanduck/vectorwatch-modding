.class public Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "UpdateWatchActivity.java"


# static fields
.field private static final DELAY_SHOW_CONNECTED_SCREEN:J = 0x9c4L

.field public static final DOWNLOAD_UPDATE_ALL_FROM_CLOUD:Ljava/lang/String; = "all"

.field public static final DOWNLOAD_UPDATE_SUMMARY_FROM_CLOUD:Ljava/lang/String; = "summary"

.field public static final KEY_MANDATORY_APP_UPDATE:Ljava/lang/String; = "force_app_update"

.field public static final UPDATE_FILE:Ljava/lang/String; = "update.vos"

.field public static final UPDATE_KERNEL_FILE_URL:Ljava/lang/String; = "update_kernel_file_url.vos"

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private isForceOTA:Z

.field private isUpToDate:Z

.field private mBuildNumber:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mDeviceInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo;

.field private mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

.field private mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

.field private mExtraDetails:Landroid/widget/TextView;

.field final mHandler:Landroid/os/Handler;

.field private mHandlerBack:Landroid/os/Handler;

.field private mHandlerBatteryWait:Landroid/os/Handler;

.field private mHandlerSystemInfo:Landroid/os/Handler;

.field private mImageLogo:Landroid/widget/ImageView;

.field private mImageVector:Landroid/widget/ImageView;

.field private mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

.field private mProgressStatus:Landroid/widget/TextView;

.field private mRequestUpdateImage:Z

.field private mRunnable:Ljava/lang/Runnable;

.field private mRunnableBatteryWait:Ljava/lang/Runnable;

.field private mRunnableSystemInfo:Ljava/lang/Runnable;

.field mSkipButton:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f10016e
        }
    .end annotation
.end field

.field private mUpdateDetails:Landroid/widget/TextView;

.field private mUpdatePercentage:Landroid/widget/TextView;

.field private mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

.field mUpdateTextScroll:Landroid/widget/ScrollView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100168
        }
    .end annotation
.end field

.field private mUpdateWatchActionButton:Landroid/widget/Button;

.field private mVersionStatus:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 73
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-direct {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDeviceInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 74
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandler:Landroid/os/Handler;

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRequestUpdateImage:Z

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startDownloadAndUpdateWatch()V

    return-void
.end method

.method static synthetic access$100(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateBrokenScreen(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1002(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRequestUpdateImage:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->requestSystemInfo()V

    return-void
.end method

.method static synthetic access$1200(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->triggerUpdatesWithCloud()V

    return-void
.end method

.method static synthetic access$200(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRunnableBatteryWait:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandlerBatteryWait:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createWaitForWatchScreen()V

    return-void
.end method

.method static synthetic access$700()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createCheckForUpdatesScreen()V

    return-void
.end method

.method static synthetic access$900(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createErrorScreen(Ljava/lang/String;)V

    return-void
.end method

.method private changeHomeButtonState(Z)V
    .locals 1
    .param p1, "state"    # Z

    .prologue
    .line 1043
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1044
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1045
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 1046
    return-void
.end method

.method private createCheckForUpdatesScreen()V
    .locals 5

    .prologue
    const v4, 0x3e99999a    # 0.3f

    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 353
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v0

    const-string v1, "last_known_system_info"

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/ComplexPreferences;->remove(Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 358
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v2}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 362
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 363
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 364
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 365
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    const v1, 0x7f090287

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 368
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 369
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 371
    const/4 v0, 0x1

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 373
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$5;

    invoke-direct {v1, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$5;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 380
    return-void
.end method

.method private createDownloadUpdateScreen()V
    .locals 5

    .prologue
    const v4, 0x7f0f00ac

    const v3, 0x3e99999a    # 0.3f

    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 514
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 515
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 516
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 517
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 518
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v1}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 519
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 520
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 521
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 522
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 523
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 524
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 525
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 527
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 528
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 530
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    const v1, 0x7f09028a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 531
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 533
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    const v1, 0x7f090284

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 534
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 536
    const/4 v0, 0x2

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 537
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->requestLastVersionFull()V

    .line 538
    return-void
.end method

.method private createErrorScreen(Ljava/lang/String;)V
    .locals 3
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 329
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 330
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 331
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v1}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 337
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 345
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    const v1, 0x7f090281

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 347
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "INACTIVE set in no internet screen"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 348
    const/4 v0, 0x5

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 349
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 350
    return-void
.end method

.method private createInstallingUpdateScreen(II)V
    .locals 7
    .param p1, "crtPart"    # I
    .param p2, "totalParts"    # I

    .prologue
    const v6, 0x3e99999a    # 0.3f

    const v5, 0x7f0f00ac

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 596
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 597
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 598
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 599
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 600
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v3}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 601
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setFinishedStrokeColor(I)V

    .line 602
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 603
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 604
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 605
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 606
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 607
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 608
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 610
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 611
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 614
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090234

    invoke-virtual {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0901d3

    invoke-virtual {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 615
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09028b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 616
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 618
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    const-string v1, "0%"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 619
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 621
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    const v1, 0x7f090284

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 622
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 624
    invoke-static {v4, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 625
    return-void
.end method

.method private createMandatoryAppUpdateScreen(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 700
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 701
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 702
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 703
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 704
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v1}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 705
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 706
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 707
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 708
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 709
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 710
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 711
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 713
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    const v1, 0x7f09027d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 715
    if-eqz p1, :cond_0

    .line 716
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 720
    :goto_0
    return-void

    .line 718
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901cd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private createUpdateBrokenScreen(Ljava/lang/String;I)V
    .locals 6
    .param p1, "errorMsg"    # Ljava/lang/String;
    .param p2, "lastProgress"    # I

    .prologue
    const v5, 0x3e99999a    # 0.3f

    const/4 v4, 0x4

    const/4 v0, 0x0

    .line 672
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 673
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 674
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 675
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 676
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v1, v0}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 677
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0047

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setFinishedStrokeColor(I)V

    .line 678
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 679
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 680
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v1, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 681
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 682
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 683
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 684
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 686
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 687
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 689
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    if-gtz p2, :cond_0

    move p2, v0

    .end local p2    # "lastProgress":I
    :cond_0
    invoke-virtual {v1, p2}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setProgress(I)V

    .line 691
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 692
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 694
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    const v1, 0x7f090281

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 696
    const/4 v0, 0x6

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 697
    return-void
.end method

.method private createUpdateCompletedScreen()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 628
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 629
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 630
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 631
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 632
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v1, v2}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 633
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 634
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 635
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 636
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 637
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 638
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 639
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 641
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 642
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 644
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    const v2, 0x7f090289

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 645
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00ac

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 647
    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v2, "INACTIVE set in update completed screen"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 648
    const/4 v1, 0x5

    invoke-static {v1, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 651
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 652
    .local v0, "goToMainHandler":Landroid/os/Handler;
    new-instance v1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$6;

    invoke-direct {v1, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$6;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 664
    return-void
.end method

.method private createUpdateFoundScreen(Lcom/vectorwatch/android/models/SoftwareUpdateModel;)V
    .locals 11
    .param p1, "softwareUpdateModel"    # Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .prologue
    const/16 v10, 0x8

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x4

    const/4 v6, 0x0

    .line 402
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 403
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 404
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 405
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 406
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v5, v8}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 407
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 408
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 409
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v5, v6}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 410
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 411
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 412
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 413
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v5, v10}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 415
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    const v6, 0x7f09028e

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 416
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f00b2

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 418
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 419
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 421
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v5

    const-string v6, "last_known_system_info"

    const-class v7, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 422
    invoke-virtual {v5, v6, v7}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 424
    .local v1, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    const-string v0, ""

    .line 426
    .local v0, "finalText":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 427
    .local v4, "updates":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SoftwareUpdateData;>;"
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getData()Lcom/vectorwatch/android/models/UpdateData;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 429
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getData()Lcom/vectorwatch/android/models/UpdateData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/UpdateData;->getImages()Ljava/util/List;

    move-result-object v4

    .line 432
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/SoftwareUpdateData;

    .line 433
    .local v3, "update":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    const-string v2, "unknown"

    .line 435
    .local v2, "previousVersion":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 436
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v6

    const-string v7, "kernel"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 437
    const-string v6, "kernel"

    invoke-static {v1, v6}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 439
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Kernel from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getVersion()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 442
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getSummary()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 445
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getSummary()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 447
    :cond_2
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v6

    const-string v7, "bootloader"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 448
    const-string v6, "bootloader"

    invoke-static {v1, v6}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 450
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Bootloader from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getVersion()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 453
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getSummary()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 456
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getSummary()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 462
    .end local v2    # "previousVersion":Ljava/lang/String;
    .end local v3    # "update":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    :cond_3
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 464
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 466
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    const v6, 0x7f090280

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(I)V

    .line 468
    sget-object v5, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v6, "INACTIVE set in update found screen"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 469
    invoke-static {v10, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 470
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 471
    return-void
.end method

.method private createUpdateNotFoundScreen(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "version"    # Ljava/lang/String;
    .param p2, "messageToDisplay"    # Ljava/lang/String;

    .prologue
    const v4, 0x7f0f00ac

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 474
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 475
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 478
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v2}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 479
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 480
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 481
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 482
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 483
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 484
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 485
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 487
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 488
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 490
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 491
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 493
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 494
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 496
    if-nez p2, :cond_0

    .line 498
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    const v1, 0x7f090282

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 504
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 506
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    const v1, 0x7f09027f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 508
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "INACTIVE set update not found screen"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 509
    const/4 v0, 0x5

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 510
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 511
    return-void

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 502
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private createVerifyUpdateScreen(II)V
    .locals 5
    .param p1, "crtPart"    # I
    .param p2, "totalParts"    # I

    .prologue
    const v4, 0x7f0f00ac

    const v3, 0x3e99999a    # 0.3f

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 568
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 569
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 570
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 571
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 572
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v2}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 573
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v1}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setProgress(I)V

    .line 574
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 575
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 576
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 577
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 578
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 579
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 580
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 582
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 583
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 585
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090234

    invoke-virtual {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0901d3

    invoke-virtual {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 586
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    const v1, 0x7f09028c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 587
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 589
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    const v1, 0x7f090284

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 590
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 592
    const/4 v0, 0x7

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 593
    return-void
.end method

.method private createWaitForWatchScreen()V
    .locals 4

    .prologue
    const v3, 0x3e99999a    # 0.3f

    const/4 v2, 0x0

    const/4 v1, 0x4

    .line 383
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 384
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 385
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 387
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 388
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 389
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v1}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 390
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 392
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 393
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 394
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    const v1, 0x7f09028f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 397
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 398
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 399
    return-void
.end method

.method private createWaitForWatchUpdateScreen(II)V
    .locals 5
    .param p1, "crtPart"    # I
    .param p2, "totalParts"    # I

    .prologue
    const v4, 0x7f0f00ac

    const v3, 0x3e99999a    # 0.3f

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 541
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 542
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 543
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 544
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 545
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v0, v2}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setVisibility(I)V

    .line 546
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 547
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 548
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateTextScroll:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 549
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 550
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 551
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 552
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 554
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 555
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 557
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f090234

    invoke-virtual {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0901d3

    invoke-virtual {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 558
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    const v1, 0x7f09028d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 559
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 561
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    const v1, 0x7f090284

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 562
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 564
    const/4 v0, 0x3

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 565
    return-void
.end method

.method private requestLastVersionFull()V
    .locals 3

    .prologue
    .line 728
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getModelForVosUpdateRequest(Landroid/content/Context;)Lcom/vectorwatch/android/models/WatchOsDetailsModel;

    move-result-object v0

    .line 729
    .local v0, "watchOsDetailsModel":Lcom/vectorwatch/android/models/WatchOsDetailsModel;
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 730
    const-string v1, "all"

    invoke-static {p0, v0, v1}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineSoftwareUpdateModel(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;)V

    .line 763
    :goto_0
    return-void

    .line 732
    :cond_0
    const-string v1, "all"

    new-instance v2, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$7;

    invoke-direct {v2, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$7;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    invoke-static {p0, v0, v1, v2}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveUpdateDetails(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;Lretrofit/Callback;)V

    goto :goto_0
.end method

.method private requestSystemInfo()V
    .locals 4

    .prologue
    .line 723
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandlerSystemInfo:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRunnableSystemInfo:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 724
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSystemInfoRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 725
    return-void
.end method

.method private startDownloadAndUpdateWatch()V
    .locals 8

    .prologue
    const v7, 0x7f0901a2

    const v6, 0x7f0901a1

    const/16 v5, 0x5dc

    const/4 v4, -0x1

    .line 1127
    const-string v2, "last_synced_battery_level"

    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;

    invoke-static {v2, v4, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 1129
    .local v0, "watchBatteryLevel":I
    const-string v2, "last_synced_battery_status"

    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;

    invoke-static {v2, v4, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v1

    .line 1133
    .local v1, "watchStatus":I
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getPhoneBatteryLevel(Landroid/content/Context;)F

    move-result v2

    const/high16 v3, 0x41700000    # 15.0f

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    .line 1134
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1136
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createErrorScreen(Ljava/lang/String;)V

    .line 1156
    :goto_0
    return-void

    .line 1138
    :cond_0
    const/16 v2, 0xf

    if-ge v0, v2, :cond_1

    if-eq v0, v4, :cond_1

    .line 1139
    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 1140
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1142
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createErrorScreen(Ljava/lang/String;)V

    goto :goto_0

    .line 1148
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->isInternetEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1149
    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "UpdateWatchActivity - clicked on download & install"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1150
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 1151
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createDownloadUpdateScreen()V

    goto :goto_0

    .line 1153
    :cond_2
    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v3, "UpdateWatchActivity - clicked on download & install - no internet"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1154
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090283

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createErrorScreen(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private triggerUpdatesWithCloud()V
    .locals 3

    .prologue
    .line 667
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 668
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 669
    return-void
.end method


# virtual methods
.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1160
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 1161
    const/4 v0, 0x5

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 1162
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 1164
    :cond_0
    return-void
.end method

.method public handleCloudSystemUpdateEvent(Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;)V
    .locals 8
    .param p1, "event"    # Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 897
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 898
    .local v0, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SoftwareUpdateData;>;"
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;->getSoftwareUpdate()Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;->getSoftwareUpdate()Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getData()Lcom/vectorwatch/android/models/UpdateData;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 900
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;->getSoftwareUpdate()Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getData()Lcom/vectorwatch/android/models/UpdateData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/UpdateData;->getImages()Ljava/util/List;

    move-result-object v0

    .line 903
    :cond_0
    if-nez v0, :cond_2

    .line 904
    sget-object v4, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v5, "No update found in the payload sent by the server."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 955
    :cond_1
    :goto_0
    return-void

    .line 908
    :cond_2
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;->getSoftwareUpdate()Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->isErrorDiscovered()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 909
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090141

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createErrorScreen(Ljava/lang/String;)V

    goto :goto_0

    .line 913
    :cond_3
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v5

    if-ne v5, v3, :cond_7

    .line 914
    sget-object v5, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v6, "UpdateWatchActivity - handle cloud sys update event for summary"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 915
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;->getSoftwareUpdate()Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    move-result-object v5

    iput-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 917
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v5

    const-string v6, "last_known_system_info"

    const-class v7, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-virtual {v5, v6, v7}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 919
    .local v1, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-eqz v1, :cond_6

    .line 920
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    if-eqz v5, :cond_4

    .line 922
    .local v3, "isValidUpdate":Z
    :goto_1
    if-eqz v3, :cond_5

    .line 923
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDeviceInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-virtual {v4, v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->duplicate(Lcom/vectorwatch/com/android/vos/update/SystemInfo;)V

    .line 924
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-direct {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateFoundScreen(Lcom/vectorwatch/android/models/SoftwareUpdateModel;)V

    goto :goto_0

    .end local v3    # "isValidUpdate":Z
    :cond_4
    move v3, v4

    .line 920
    goto :goto_1

    .line 927
    .restart local v3    # "isValidUpdate":Z
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "V"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "kernel"

    invoke-static {v1, v5}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateNotFoundScreen(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 930
    .end local v3    # "isValidUpdate":Z
    :cond_6
    sget-object v4, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v5, "When trying to update from cloud, the system info about the watch should already exist."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 935
    .end local v1    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :cond_7
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 936
    sget-object v5, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v6, "UpdateWatchActivity - handle cloud sys update event for all"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 937
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;->getSoftwareUpdate()Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    move-result-object v5

    iput-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 939
    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    if-eqz v5, :cond_1

    .line 941
    const-string v5, "check_compatibility"

    invoke-static {v5, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 943
    sput-boolean v4, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    .line 945
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/vectorwatch/com/android/vos/update/UpdateService;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 946
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "receiver"

    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 947
    const-string v4, "deviceInfo"

    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDeviceInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 948
    const-string v4, "lastSynced"

    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 949
    invoke-virtual {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 951
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v4

    const-string v5, "last_update_done"

    iget-object v6, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-virtual {v4, v5, v6}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public handleErrorMessageFromCloud(Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;)V
    .locals 11
    .param p1, "event"    # Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 860
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;->getErrorBody()Ljava/lang/Object;

    move-result-object v1

    .line 861
    .local v1, "errorBody":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;->getUpdateType()Ljava/lang/String;

    move-result-object v7

    .line 863
    .local v7, "updateType":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-nez v7, :cond_1

    .line 893
    :cond_0
    :goto_0
    return-void

    .line 867
    :cond_1
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    .line 868
    .local v3, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v3, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 869
    .local v2, "errorBodyJson":Ljava/lang/String;
    sget-object v8, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Json body: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 870
    const-class v8, Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-virtual {v3, v2, v8}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 872
    .local v6, "model":Lcom/vectorwatch/android/models/SoftwareUpdateModel;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v8

    const-string v9, "last_known_system_info"

    const-class v10, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 873
    invoke-virtual {v8, v9, v10}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 874
    .local v4, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    const-string v0, "unknown"

    .line 875
    .local v0, "crtVersion":Ljava/lang/String;
    if-eqz v4, :cond_3

    if-eqz v7, :cond_3

    const-string v8, "kernel"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 876
    const-string v8, "kernel"

    invoke-static {v4, v8}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 881
    :cond_2
    :goto_1
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getErrorMessage()Lcom/vectorwatch/android/events/CloudMessageModel;

    move-result-object v5

    .line 882
    .local v5, "messageModel":Lcom/vectorwatch/android/events/CloudMessageModel;
    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/vectorwatch/android/events/CloudMessageModel;->getCode()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sget-object v9, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_ALREADY_UP_TO_DATE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    .line 883
    invoke-virtual {v9}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v9

    if-ne v8, v9, :cond_4

    .line 884
    invoke-virtual {v5}, Lcom/vectorwatch/android/events/CloudMessageModel;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v0, v8}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateNotFoundScreen(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 877
    .end local v5    # "messageModel":Lcom/vectorwatch/android/events/CloudMessageModel;
    :cond_3
    if-eqz v4, :cond_2

    if-eqz v7, :cond_2

    const-string v8, "bootloader"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 878
    const-string v8, "bootloader"

    invoke-static {v4, v8}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 888
    .restart local v5    # "messageModel":Lcom/vectorwatch/android/events/CloudMessageModel;
    :cond_4
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/vectorwatch/android/events/CloudMessageModel;->getCode()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sget-object v9, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->APP_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    .line 889
    invoke-virtual {v9}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v9

    if-ne v8, v9, :cond_0

    .line 890
    invoke-virtual {v5}, Lcom/vectorwatch/android/events/CloudMessageModel;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createMandatoryAppUpdateScreen(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public handleReceiveSystemInfo(Lcom/vectorwatch/android/events/WatchSystemInfoReceivedEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/vectorwatch/android/events/WatchSystemInfoReceivedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 767
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandlerSystemInfo:Landroid/os/Handler;

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRunnableSystemInfo:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 768
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getModelForVosUpdateRequest(Landroid/content/Context;)Lcom/vectorwatch/android/models/WatchOsDetailsModel;

    move-result-object v2

    .line 770
    .local v2, "watchOsDetailsModel":Lcom/vectorwatch/android/models/WatchOsDetailsModel;
    iget-boolean v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRequestUpdateImage:Z

    if-eqz v3, :cond_0

    .line 771
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 772
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "last_known_system_info"

    const-class v5, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 773
    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 775
    .local v1, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    invoke-static {p0, v1}, Lcom/vectorwatch/android/utils/OfflineUtils;->checkCompatibilityOffline(Landroid/content/Context;Lcom/vectorwatch/com/android/vos/update/SystemInfo;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 776
    new-instance v0, Lcom/vectorwatch/android/events/CloudMessageModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/events/CloudMessageModel;-><init>()V

    .line 777
    .local v0, "cloudMessage":Lcom/vectorwatch/android/events/CloudMessageModel;
    sget-object v3, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_ALREADY_UP_TO_DATE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-virtual {v3}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/events/CloudMessageModel;->setCode(Ljava/lang/Integer;)V

    .line 778
    const v3, 0x7f09027c

    invoke-virtual {p0, v3}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/events/CloudMessageModel;->setText(Ljava/lang/String;)V

    .line 780
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;

    invoke-direct {v4, v0}, Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;-><init>(Lcom/vectorwatch/android/events/CloudMessageModel;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 818
    .end local v0    # "cloudMessage":Lcom/vectorwatch/android/events/CloudMessageModel;
    .end local v1    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :cond_0
    :goto_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRequestUpdateImage:Z

    .line 819
    return-void

    .line 782
    .restart local v1    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :cond_1
    const-string v3, "summary"

    invoke-static {p0, v2, v3}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineSoftwareUpdateModel(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;)V

    goto :goto_0

    .line 785
    .end local v1    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :cond_2
    const-string v3, "summary"

    new-instance v4, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$8;

    invoke-direct {v4, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$8;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    invoke-static {p0, v2, v3, v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveUpdateDetails(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;Lretrofit/Callback;)V

    goto :goto_0
.end method

.method public handleSuccessMessageFromCloudEvent(Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;)V
    .locals 8
    .param p1, "event"    # Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 823
    if-nez p1, :cond_1

    .line 824
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: Received 200 for download but a null event has been sent over Otto for analyzing the code."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 829
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;->getCloudMessageModel()Lcom/vectorwatch/android/events/CloudMessageModel;

    move-result-object v2

    .line 831
    .local v2, "message":Lcom/vectorwatch/android/events/CloudMessageModel;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/vectorwatch/android/events/CloudMessageModel;->getCode()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_3

    .line 832
    :cond_2
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: Received 200 for download but the message is null or the code is null."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 836
    :cond_3
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "last_known_system_info"

    const-class v5, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 837
    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 840
    .local v1, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    const-string v0, "unknown"

    .line 841
    .local v0, "crtVersion":Ljava/lang/String;
    const-string v3, "kernel"

    invoke-static {v1, v3}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 843
    invoke-virtual {v2}, Lcom/vectorwatch/android/events/CloudMessageModel;->getCode()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v4, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_ALREADY_UP_TO_DATE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-virtual {v4}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v4

    if-ne v3, v4, :cond_4

    .line 844
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/VectorApplication;

    invoke-virtual {v3}, Lcom/vectorwatch/android/VectorApplication;->getUpdateReceiverInstance()Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->setCurrentStatus(I)V

    .line 846
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isUpToDate:Z

    .line 847
    invoke-virtual {v2}, Lcom/vectorwatch/android/events/CloudMessageModel;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateNotFoundScreen(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandlerBack:Landroid/os/Handler;

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v6, 0x1388

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 852
    :cond_4
    invoke-virtual {v2}, Lcom/vectorwatch/android/events/CloudMessageModel;->getCode()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v4, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->APP_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-virtual {v4}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->getVal()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 853
    invoke-virtual {v2}, Lcom/vectorwatch/android/events/CloudMessageModel;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createMandatoryAppUpdateScreen(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleUpdateFinishedEvent(Lcom/vectorwatch/android/events/UpdateFinishedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/UpdateFinishedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1038
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "UpdateWatchActivity - Update finished"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1039
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->changeHomeButtonState(Z)V

    .line 1040
    return-void
.end method

.method public handleUpdateInstallingEvent(Lcom/vectorwatch/android/events/UpdateInstallingEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/UpdateInstallingEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 979
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "UpdateWatchActivity - handleUpdateInstallingEvent - received state change to update installing"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 981
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 982
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/UpdateInstallingEvent;->getCrtPart()I

    move-result v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/UpdateInstallingEvent;->getTotalParts()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createInstallingUpdateScreen(II)V

    .line 983
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->changeHomeButtonState(Z)V

    .line 985
    :cond_0
    return-void
.end method

.method public handleUpdateWatchBrokenEvent(Lcom/vectorwatch/android/events/WatchUpdateBrokenEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/WatchUpdateBrokenEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1003
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "UpdateWatchActivity - handleUpdateWatchBrokenEvent - received update interrupted (unknown reason)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1004
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/WatchUpdateBrokenEvent;->getLastProgress()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateBrokenScreen(Ljava/lang/String;I)V

    .line 1005
    iget-boolean v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isForceOTA:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->changeHomeButtonState(Z)V

    .line 1006
    const/4 v0, 0x5

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 1007
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    if-eqz v0, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->setCurrentStatus(I)V

    .line 1010
    :cond_0
    return-void

    .line 1005
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public handleVerifyUpdateEvent(Lcom/vectorwatch/android/events/VerifyUpdateEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/VerifyUpdateEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 969
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "UpdateWatchActivity - handleVerifyUpdateEvent - received state change to verify update."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 971
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 972
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/VerifyUpdateEvent;->getCrtPart()I

    move-result v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/VerifyUpdateEvent;->getTotalParts()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createVerifyUpdateScreen(II)V

    .line 973
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->changeHomeButtonState(Z)V

    .line 975
    :cond_0
    return-void
.end method

.method public handleWaitingForWatchEvent(Lcom/vectorwatch/android/events/WaitingForWatchEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/WaitingForWatchEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 959
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "UpdateWatchActivity - handleWaitingForWatchEvent - received state change to waiting for watch"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 961
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 962
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/WaitingForWatchEvent;->getCrtPart()I

    move-result v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/WaitingForWatchEvent;->getTotalParts()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createWaitForWatchUpdateScreen(II)V

    .line 963
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->changeHomeButtonState(Z)V

    .line 965
    :cond_0
    return-void
.end method

.method public handleWatchNoOsDataEvent(Lcom/vectorwatch/android/events/WatchNoOsDataEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/WatchNoOsDataEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1030
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "UpdateWatchActivity - handleWatchNoOsDataEvent - received update interrupted (watch did not send os data)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1032
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateBrokenScreen(Ljava/lang/String;I)V

    .line 1033
    iget-boolean v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isForceOTA:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->changeHomeButtonState(Z)V

    .line 1034
    return-void

    .line 1033
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public handleWatchNotConnectedEvent(Lcom/vectorwatch/android/events/WatchNotConnectedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/WatchNotConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1014
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "UpdateWatchActivity - handleWatchNotConnectedEvent - received update interrupted (watch disconnected)"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1016
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateBrokenScreen(Ljava/lang/String;I)V

    .line 1017
    iget-boolean v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isForceOTA:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->changeHomeButtonState(Z)V

    .line 1018
    return-void

    .line 1017
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public handleWatchUpdateCompleted(Lcom/vectorwatch/android/events/WatchUpdateCompletedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/WatchUpdateCompletedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1022
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "UpdateWatchActivity - handleWatchUpdateCompleted - the update was successfully completed."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1024
    iget-boolean v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isForceOTA:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->changeHomeButtonState(Z)V

    .line 1025
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateCompletedScreen()V

    .line 1026
    return-void

    .line 1024
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public handleWatchUpdateProgressIncreasedEvent(Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 989
    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v2, "UpdateWatchActivity - handleWatchUpdateProgressIncreasedEvent - received progress update"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 991
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 992
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 993
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;->getProgressPercentage()I

    move-result v1

    if-gtz v1, :cond_1

    const/4 v0, 0x0

    .line 994
    .local v0, "progress":I
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 995
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    invoke-virtual {v1, v0}, Lcom/github/lzyzsd/circleprogress/DonutProgress;->setProgress(I)V

    .line 996
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 999
    .end local v0    # "progress":I
    :cond_0
    return-void

    .line 993
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;->getProgressPercentage()I

    move-result v0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 1081
    iget-boolean v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isForceOTA:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isUpToDate:Z

    if-eqz v1, :cond_1

    .line 1082
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAppNotification(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1083
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1084
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "allow_screen"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1085
    invoke-virtual {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 1086
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->finish()V

    .line 1124
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1088
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1089
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 1090
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->finish()V

    goto :goto_0

    .line 1094
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iget-boolean v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isForceOTA:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getCurrentStatus()I

    move-result v1

    if-eq v1, v3, :cond_2

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    .line 1095
    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getCurrentStatus()I

    move-result v1

    if-nez v1, :cond_3

    .line 1096
    :cond_2
    invoke-virtual {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->moveTaskToBack(Z)Z

    goto :goto_0

    .line 1097
    :cond_3
    iget-boolean v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isForceOTA:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getCurrentStatus()I

    move-result v1

    if-ne v1, v2, :cond_5

    .line 1098
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAppNotification(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1099
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1100
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "allow_screen"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1101
    invoke-virtual {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 1102
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->finish()V

    goto :goto_0

    .line 1104
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1105
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 1106
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->finish()V

    goto :goto_0

    .line 1108
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getCurrentStatus()I

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    .line 1109
    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getCurrentStatus()I

    move-result v1

    if-ne v1, v3, :cond_7

    .line 1110
    :cond_6
    invoke-virtual {p0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->moveTaskToBack(Z)Z

    goto :goto_0

    .line 1112
    :cond_7
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAppNotification(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1113
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1114
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "allow_screen"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1115
    invoke-virtual {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 1116
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->finish()V

    goto/16 :goto_0

    .line 1118
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_8
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1119
    .restart local v0    # "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1120
    invoke-virtual {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 1121
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->finish()V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x4

    const/4 v7, 0x0

    .line 126
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 127
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 128
    const v4, 0x7f030039

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->setContentView(I)V

    .line 129
    iput-object p0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;

    .line 130
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 131
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 133
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandlerBatteryWait:Landroid/os/Handler;

    .line 134
    new-instance v4, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$1;

    invoke-direct {v4, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$1;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRunnableBatteryWait:Ljava/lang/Runnable;

    .line 141
    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v5, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v6, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_UPDATE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {p0, v4, v5, v6}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    .line 145
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "force_ota"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isForceOTA:Z

    .line 147
    iget-boolean v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->isForceOTA:Z

    if-eqz v4, :cond_0

    .line 148
    invoke-direct {p0, v7}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->changeHomeButtonState(Z)V

    .line 151
    :cond_0
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandlerBack:Landroid/os/Handler;

    .line 152
    new-instance v4, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$2;

    invoke-direct {v4, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$2;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRunnable:Ljava/lang/Runnable;

    .line 159
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandlerSystemInfo:Landroid/os/Handler;

    .line 160
    new-instance v4, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$3;

    invoke-direct {v4, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$3;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRunnableSystemInfo:Ljava/lang/Runnable;

    .line 167
    const v4, 0x7f10016d

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    .line 168
    const v4, 0x7f100143

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mVersionStatus:Landroid/widget/TextView;

    .line 169
    const v4, 0x7f100147

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdatePercentage:Landroid/widget/TextView;

    .line 170
    const v4, 0x7f100167

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mBuildNumber:Landroid/widget/TextView;

    .line 171
    const v4, 0x7f100169

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateDetails:Landroid/widget/TextView;

    .line 172
    const v4, 0x7f10016a

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mProgressStatus:Landroid/widget/TextView;

    .line 173
    const v4, 0x7f10016c

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mExtraDetails:Landroid/widget/TextView;

    .line 174
    const v4, 0x7f100146

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageVector:Landroid/widget/ImageView;

    .line 175
    const v4, 0x7f100144

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mImageLogo:Landroid/widget/ImageView;

    .line 176
    const v4, 0x7f10016b

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgressBar:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 177
    const v4, 0x7f100166

    invoke-virtual {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/github/lzyzsd/circleprogress/DonutProgress;

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mDownloadProgress:Lcom/github/lzyzsd/circleprogress/DonutProgress;

    .line 179
    const-string v4, "account_update_type"

    const/4 v5, 0x0

    invoke-static {v4, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 182
    .local v3, "userUpdateType":Ljava/lang/String;
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 183
    if-eqz v3, :cond_1

    sget-object v4, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->DEV:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->getVal()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 184
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 187
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateWatchActionButton:Landroid/widget/Button;

    new-instance v5, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;

    invoke-direct {v5, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity$4;-><init>(Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/VectorApplication;

    invoke-virtual {v4}, Lcom/vectorwatch/android/VectorApplication;->getUpdateReceiverInstance()Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    move-result-object v4

    iput-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    .line 242
    const-string v4, "mandatory_kernel_update"

    invoke-static {v4, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "mandatory_app_update"

    .line 243
    invoke-static {v4, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_2
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;

    .line 244
    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_7

    .line 248
    const-string v4, "mandatory_app_update"

    invoke-static {v4, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 252
    const/4 v1, 0x0

    .line 253
    .local v1, "message":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 254
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "force_app_update"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 256
    :cond_3
    invoke-direct {p0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createMandatoryAppUpdateScreen(Ljava/lang/String;)V

    .line 261
    .end local v1    # "message":Ljava/lang/String;
    :cond_4
    const-string v4, "mandatory_kernel_update"

    invoke-static {v4, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "mandatory_app_update"

    .line 262
    invoke-static {v4, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 264
    invoke-static {p0}, Lcom/vectorwatch/android/utils/NetworkUtils;->getConnectivityStatus(Landroid/content/Context;)I

    move-result v4

    sget v5, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_NOT_CONNECTED:I

    if-ne v4, v5, :cond_6

    .line 266
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090283

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createErrorScreen(Ljava/lang/String;)V

    .line 305
    :cond_5
    :goto_0
    return-void

    .line 269
    :cond_6
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createCheckForUpdatesScreen()V

    goto :goto_0

    .line 273
    :cond_7
    const-string v4, "crt_install_part"

    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;

    invoke-static {v4, v9, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 275
    .local v0, "crtPart":I
    const-string v4, "total_install_parts"

    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mContext:Landroid/content/Context;

    invoke-static {v4, v9, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v2

    .line 278
    .local v2, "totalParts":I
    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-virtual {v4}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getCurrentStatus()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 301
    :pswitch_0
    invoke-direct {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createCheckForUpdatesScreen()V

    goto :goto_0

    .line 280
    :pswitch_1
    invoke-direct {p0, v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createWaitForWatchUpdateScreen(II)V

    goto :goto_0

    .line 283
    :pswitch_2
    invoke-direct {p0, v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createVerifyUpdateScreen(II)V

    goto :goto_0

    .line 286
    :pswitch_3
    const/4 v4, 0x6

    invoke-static {v4, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 287
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090288

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    .line 288
    invoke-virtual {v5}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getLastProgress()I

    move-result v5

    .line 287
    invoke-direct {p0, v4, v5}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createUpdateBrokenScreen(Ljava/lang/String;I)V

    .line 289
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/WatchUpdateBrokenEvent;

    iget-object v6, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-virtual {v6}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getCurrentStatus()I

    move-result v6

    invoke-direct {v5, v6}, Lcom/vectorwatch/android/events/WatchUpdateBrokenEvent;-><init>(I)V

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 292
    :pswitch_4
    invoke-static {v8, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 293
    invoke-direct {p0, v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createInstallingUpdateScreen(II)V

    .line 294
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;

    iget-object v6, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-virtual {v6}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getLastProgress()I

    move-result v6

    invoke-direct {v5, v6}, Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;-><init>(I)V

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 297
    :pswitch_5
    invoke-static {v8, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 298
    invoke-direct {p0, v0, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->createInstallingUpdateScreen(II)V

    goto :goto_0

    .line 278
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 1075
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 1076
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 1077
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 309
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 314
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 311
    :pswitch_0
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->onBackPressed()V

    .line 312
    const/4 v0, 0x1

    goto :goto_0

    .line 309
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onStart()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1050
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onStart()V

    .line 1051
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/VectorApplication;

    invoke-virtual {v0}, Lcom/vectorwatch/android/VectorApplication;->getUpdateReceiverInstance()Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    .line 1052
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mUpdateResultReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getCurrentStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1063
    :goto_0
    :pswitch_0
    return-void

    .line 1054
    :pswitch_1
    const/4 v0, 0x6

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    goto :goto_0

    .line 1057
    :pswitch_2
    invoke-static {v1, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    goto :goto_0

    .line 1060
    :pswitch_3
    invoke-static {v1, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    goto :goto_0

    .line 1052
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 1067
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onStop()V

    .line 1068
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mLastSyncedSoftwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 1070
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mHandlerBack:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1071
    return-void
.end method

.method public skipButton()V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10016e
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 319
    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->log:Lorg/slf4j/Logger;

    const-string v2, "OTA: Skip button pressed."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 320
    const-string v1, "flag_is_force_ota"

    invoke-static {v1, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 321
    const-string v1, "check_compatibility"

    invoke-static {v1, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 322
    const/4 v1, 0x5

    invoke-static {v1, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 323
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 324
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 325
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;->finish()V

    .line 326
    return-void
.end method
