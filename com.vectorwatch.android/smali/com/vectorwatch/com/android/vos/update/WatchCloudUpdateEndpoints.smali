.class public Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;
.super Ljava/lang/Object;
.source "WatchCloudUpdateEndpoints.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private authToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-static {p0}, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;->getAccountToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method public static confirmUpdateDoneOnWatchWithCpuId(Landroid/content/Context;Ljava/lang/Long;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "versionId"    # Ljava/lang/Long;
    .param p2, "type"    # Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    .param p3, "model"    # Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

    .prologue
    .line 80
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;-><init>(Landroid/content/Context;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Ljava/lang/Long;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 118
    invoke-virtual {v0, v1}, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 119
    const/4 v0, 0x1

    return v0
.end method

.method private static getAccountToken(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 34
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 35
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v5, "com.vectorwatch.android"

    invoke-virtual {v1, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 37
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v5, v2

    if-ge v5, v6, :cond_0

    .line 55
    :goto_0
    return-object v3

    .line 41
    :cond_0
    const/4 v5, 0x0

    aget-object v0, v2, v5

    .line 44
    .local v0, "account":Landroid/accounts/Account;
    :try_start_0
    const-string v5, "Full access"

    const/4 v6, 0x1

    invoke-virtual {v1, v0, v5, v6}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    .line 47
    .local v3, "authToken":Ljava/lang/String;
    goto :goto_0

    .line 48
    .end local v3    # "authToken":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 49
    .local v4, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v4}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 50
    .end local v4    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v4

    .line 51
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v4    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 53
    .local v4, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v4}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_0
.end method
