.class public Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;
.super Landroid/os/ResultReceiver;
.source "UpdateResultReceiver.java"


# static fields
.field public static final STATUS_ERROR:I = 0x2

.field public static final STATUS_FINISHED:I = 0x1

.field public static final STATUS_FINISHED_PART:I = 0x7

.field public static final STATUS_INACTIVE:I = -0x1

.field public static final STATUS_NOT_CONNECTED:I = 0x4

.field public static final STATUS_RUNNING:I = 0x0

.field public static final STATUS_SET_INSTALLING_UPDATE_IN_PROGRESS:I = 0x9

.field public static final STATUS_UPDATE_PROGRESS:I = 0x3

.field public static final STATUS_VERIFY_UPDATE:I = 0x6

.field public static final STATUS_WAIT_WATCH:I = 0x5

.field public static final STATUS_WAIT_WATCH_FOR_VERIFY_UPDATE:I = 0x8

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private lastProgress:I

.field private mContext:Landroid/content/Context;

.field private mCurrentStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/os/ResultReceiver;-><init>(Landroid/os/Handler;)V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mCurrentStatus:I

    .line 49
    return-void
.end method

.method private handleConfirmForAnUpdate(Lcom/vectorwatch/android/models/SoftwareUpdateData;)V
    .locals 5
    .param p1, "softwareUpdate"    # Lcom/vectorwatch/android/models/SoftwareUpdateData;

    .prologue
    const/4 v4, 0x1

    .line 169
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UPDATE: confirm for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 171
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "kernel"

    .line 172
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    const-string v0, "update_id"

    .line 174
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    .line 173
    invoke-static {v0, v2, v3, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 175
    const-string v0, "flag_confirm_update"

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0, v4, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 188
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    const-class v3, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 189
    return-void

    .line 178
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bootloader"

    .line 179
    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "update_id_bootloader"

    .line 181
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->getId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    .line 180
    invoke-static {v0, v2, v3, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 182
    const-string v0, "flag_confirm_update"

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0, v4, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public getCurrentStatus()I
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mCurrentStatus:I

    return v0
.end method

.method public getLastProgress()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->lastProgress:I

    return v0
.end method

.method protected onReceiveResult(ILandroid/os/Bundle;)V
    .locals 11
    .param p1, "resultCode"    # I
    .param p2, "resultData"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x6

    const/4 v9, 0x1

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 53
    const-string v3, "UPDATE STATUS"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->getCurrentStatus()I

    move-result v3

    if-ne v3, v9, :cond_0

    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    .line 55
    :cond_0
    invoke-virtual {p0, p1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->setCurrentStatus(I)V

    .line 58
    :cond_1
    const-string v3, "crt_install_part"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 60
    .local v0, "crtPart":I
    const-string v3, "total_install_parts"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v2

    .line 64
    .local v2, "totalParts":I
    packed-switch p1, :pswitch_data_0

    .line 166
    :goto_0
    return-void

    .line 66
    :pswitch_0
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v8, v3}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    goto :goto_0

    .line 69
    :pswitch_1
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: RUNNING status received"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 70
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "WatchUpdateThread - Entering installing update state"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 71
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/UpdateInstallingEvent;

    invoke-direct {v4, v0, v2}, Lcom/vectorwatch/android/events/UpdateInstallingEvent;-><init>(II)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 72
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v8, v3}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    goto :goto_0

    .line 75
    :pswitch_2
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: FINISHED PART status received"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 76
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "WatchUpdateThread - Entering installing update state after finishing a part of the update."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 77
    const-string v3, "software_update"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/SoftwareUpdateData;

    .line 79
    .local v1, "softwareUpdate":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    invoke-direct {p0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->handleConfirmForAnUpdate(Lcom/vectorwatch/android/models/SoftwareUpdateData;)V

    .line 81
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/UpdateInstallingEvent;

    invoke-direct {v4, v0, v2}, Lcom/vectorwatch/android/events/UpdateInstallingEvent;-><init>(II)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 82
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v8, v3}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    goto :goto_0

    .line 85
    .end local v1    # "softwareUpdate":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    :pswitch_3
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: FINISHED status received"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 87
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 91
    const-string v3, "mandatory_kernel_update"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v7, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 93
    const-string v3, "mandatory_kernel_update"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v7, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 95
    const-string v3, "COMPATIBILITY: "

    const-string v4, "FLAG COMPAT false at update finished"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const-string v3, "check_compatibility"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v7, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 101
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/Helpers;->isForceOta(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 102
    const-string v3, "flag_is_force_ota"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v7, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 106
    :cond_3
    const-string v3, "software_update"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/SoftwareUpdateData;

    .line 107
    .restart local v1    # "softwareUpdate":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    invoke-direct {p0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->handleConfirmForAnUpdate(Lcom/vectorwatch/android/models/SoftwareUpdateData;)V

    .line 109
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "STATE - inactive at update finished"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 110
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/WatchUpdateCompletedEvent;

    invoke-direct {v4}, Lcom/vectorwatch/android/events/WatchUpdateCompletedEvent;-><init>()V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 113
    const-string v3, "crt_install_part"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 114
    const-string v3, "total_install_parts"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 117
    const-string v3, "flag_sync_system_apps"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v9, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 121
    const-string v3, "flag_sync_settings_from_cloud"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v9, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 125
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/Helpers;->resetLocaleFlag(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 128
    .end local v1    # "softwareUpdate":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    :pswitch_4
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: ERROR status received"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 130
    const-string v3, "crt_install_part"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 131
    const-string v3, "total_install_parts"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 132
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/WatchUpdateBrokenEvent;

    iget v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->lastProgress:I

    invoke-direct {v4, v5}, Lcom/vectorwatch/android/events/WatchUpdateBrokenEvent;-><init>(I)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 133
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v10, v3}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    goto/16 :goto_0

    .line 136
    :pswitch_5
    const-string v3, "update"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->lastProgress:I

    .line 137
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UPDATE: UPDATE PROGRESS status received "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->lastProgress:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 138
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;

    iget v5, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->lastProgress:I

    invoke-direct {v4, v5}, Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;-><init>(I)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 139
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v8, v3}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    goto/16 :goto_0

    .line 142
    :pswitch_6
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: NOT CONNECTED status received"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 144
    const-string v3, "crt_install_part"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 145
    const-string v3, "total_install_parts"

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v6, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 146
    iget-object v3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v10, v3}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 147
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/WatchNotConnectedEvent;

    invoke-direct {v4}, Lcom/vectorwatch/android/events/WatchNotConnectedEvent;-><init>()V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 150
    :pswitch_7
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: WAIT WATCH status received"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 151
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 152
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/WaitingForWatchEvent;

    invoke-direct {v4, v0, v2}, Lcom/vectorwatch/android/events/WaitingForWatchEvent;-><init>(II)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 155
    :pswitch_8
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: WAIT WATCH FOR VERIFY UPDATE status received"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 156
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 157
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/VerifyUpdateEvent;

    invoke-direct {v4, v0, v2}, Lcom/vectorwatch/android/events/VerifyUpdateEvent;-><init>(II)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 160
    :pswitch_9
    sget-object v3, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "UPDATE: VERIFY UPDATE status received"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 161
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 162
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/VerifyUpdateEvent;

    invoke-direct {v4, v0, v2}, Lcom/vectorwatch/android/events/VerifyUpdateEvent;-><init>(II)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_2
        :pswitch_8
        :pswitch_0
    .end packed-switch
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mContext:Landroid/content/Context;

    .line 205
    return-void
.end method

.method public setCurrentStatus(I)V
    .locals 0
    .param p1, "mCurrentStatus"    # I

    .prologue
    .line 196
    iput p1, p0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->mCurrentStatus:I

    .line 197
    return-void
.end method
