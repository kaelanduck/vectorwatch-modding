.class public final enum Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
.super Ljava/lang/Enum;
.source "UpdateStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/com/android/vos/update/UpdateStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpdateStatusErrors"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_CRC_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_HEX_TYPE_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_INDEX_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_INITIALIZED:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_NOT_INIT:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_OK:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_SAME_DEST:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_SOURCE_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_TYPE_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

.field public static final enum UPDATE_STATUS_UNDEFINED:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_UNDEFINED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_UNDEFINED:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 13
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_OK"

    invoke-direct {v0, v1, v5, v4}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_OK:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 14
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_INITIALIZED"

    invoke-direct {v0, v1, v6, v5}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_INITIALIZED:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 15
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_INDEX_DIF"

    invoke-direct {v0, v1, v7, v6}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_INDEX_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 16
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_NOT_INIT"

    invoke-direct {v0, v1, v8, v7}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_NOT_INIT:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 17
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_TYPE_DIF"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_TYPE_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 18
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_SOURCE_DIF"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_SOURCE_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 19
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_SAME_DEST"

    const/4 v2, 0x7

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_SAME_DEST:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 20
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_CRC_DIF"

    const/16 v2, 0x8

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_CRC_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 21
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    const-string v1, "UPDATE_STATUS_HEX_TYPE_DIF"

    const/16 v2, 0x9

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_HEX_TYPE_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    .line 11
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_UNDEFINED:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_OK:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_INITIALIZED:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_INDEX_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_NOT_INIT:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_TYPE_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_SOURCE_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_SAME_DEST:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_CRC_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_HEX_TYPE_DIF:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->$VALUES:[Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->value:I

    .line 27
    return-void
.end method

.method public static fromInt(I)Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
    .locals 5
    .param p0, "value"    # I

    .prologue
    .line 35
    invoke-static {}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->values()[Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 36
    .local v0, "d":Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 39
    .end local v0    # "d":Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
    :goto_1
    return-object v0

    .line 35
    .restart local v0    # "d":Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 39
    .end local v0    # "d":Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
    :cond_1
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->UPDATE_STATUS_UNDEFINED:Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->$VALUES:[Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    invoke-virtual {v0}, [Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateStatus$UpdateStatusErrors;->value:I

    return v0
.end method
