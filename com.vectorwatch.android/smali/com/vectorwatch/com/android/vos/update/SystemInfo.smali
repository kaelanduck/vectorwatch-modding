.class public Lcom/vectorwatch/com/android/vos/update/SystemInfo;
.super Ljava/lang/Object;
.source "SystemInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;
    }
.end annotation


# static fields
.field private static final MASK:I = 0xff

.field public static final SYSTEM_BIOS:B = 0x0t

.field public static final SYSTEM_BOOTLOADER:B = 0x1t

.field public static final SYSTEM_KERNEL:B = 0x2t

.field public static final SYSTEM_NONE:B = -0x1t

.field public static final UNSET_VALUE:I = -0x1

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private biosInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

.field private bootloaderInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

.field private current:B

.field private kernelInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-byte v1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->current:B

    .line 26
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->biosInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 27
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->bootloaderInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 28
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->kernelInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 29
    return-void
.end method

.method public constructor <init>([BS)V
    .locals 6
    .param p1, "rawData"    # [B
    .param p2, "version"    # S

    .prologue
    const/4 v5, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v4, -0x1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->current:B

    .line 34
    if-eqz p2, :cond_0

    if-ne p2, v1, :cond_2

    .line 35
    :cond_0
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->biosInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 36
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    aget-byte v1, p1, v5

    and-int/lit16 v1, v1, 0xff

    const/4 v2, 0x5

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x6

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->bootloaderInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 37
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    const/4 v1, 0x7

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0x8

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0x9

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->kernelInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 46
    :cond_1
    :goto_0
    return-void

    .line 38
    :cond_2
    if-ne p2, v2, :cond_1

    .line 39
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    aget-byte v4, p1, v5

    and-int/lit16 v4, v4, 0xff

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->biosInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 40
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    const/4 v1, 0x5

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    const/4 v2, 0x6

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0x8

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->bootloaderInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 41
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    const/16 v1, 0x9

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    const/16 v2, 0xa

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0xb

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0xc

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;-><init>(IIII)V

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->kernelInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    goto :goto_0
.end method


# virtual methods
.method public duplicate(Lcom/vectorwatch/com/android/vos/update/SystemInfo;)V
    .locals 1
    .param p1, "other"    # Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .prologue
    .line 65
    iget-byte v0, p1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->current:B

    iput-byte v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->current:B

    .line 66
    iget-object v0, p1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->biosInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->biosInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 67
    iget-object v0, p1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->bootloaderInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->bootloaderInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 68
    iget-object v0, p1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->kernelInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    iput-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->kernelInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 69
    return-void
.end method

.method public getBiosInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->biosInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    return-object v0
.end method

.method public getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->bootloaderInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    return-object v0
.end method

.method public getCurrent()B
    .locals 1

    .prologue
    .line 49
    iget-byte v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->current:B

    return v0
.end method

.method public getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->kernelInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Current state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->current:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; Bios: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->biosInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; Bootloader: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->bootloaderInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    .line 73
    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; Kernel: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->kernelInfo:Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
