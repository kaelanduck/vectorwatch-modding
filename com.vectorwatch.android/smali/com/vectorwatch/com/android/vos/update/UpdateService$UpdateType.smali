.class public final enum Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
.super Ljava/lang/Enum;
.source "UpdateService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/com/android/vos/update/UpdateService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpdateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

.field public static final enum BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

.field public static final enum KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

.field public static final enum NOT_SET:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 558
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    const-string v1, "KERNEL"

    const-string v2, "kernel"

    invoke-direct {v0, v1, v3, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    const-string v1, "BOOTLOADER"

    const-string v2, "bootloader"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    const-string v1, "NOT_SET"

    const-string v2, "not_set"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->NOT_SET:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    .line 557
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->NOT_SET:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->$VALUES:[Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 561
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 562
    iput-object p3, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->name:Ljava/lang/String;

    .line 563
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 566
    if-eqz p0, :cond_1

    .line 567
    invoke-static {}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->values()[Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 569
    .local v0, "s":Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 574
    .end local v0    # "s":Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    :goto_1
    return-object v0

    .line 567
    .restart local v0    # "s":Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 574
    .end local v0    # "s":Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    :cond_1
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->NOT_SET:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 557
    const-class v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    .locals 1

    .prologue
    .line 557
    sget-object v0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->$VALUES:[Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    invoke-virtual {v0}, [Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    return-object v0
.end method


# virtual methods
.method public equalsName(Ljava/lang/String;)Z
    .locals 1
    .param p1, "otherName"    # Ljava/lang/String;

    .prologue
    .line 578
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 582
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->name:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->name:Ljava/lang/String;

    return-object v0
.end method
