.class public Lcom/vectorwatch/android/RemotePushNotificationsInitializer;
.super Ljava/lang/Object;
.source "RemotePushNotificationsInitializer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;
    }
.end annotation


# static fields
.field private static final PREFS_AMAZON_END_POINT_ARN_DIRTY_KEY:Ljava/lang/String; = "amazon_endpoint_arn_dirty_key"

.field private static final PREFS_AMAZON_END_POINT_ARN_KEY:Ljava/lang/String; = "amazon_endpoint_arn_key"

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-static {p0, p1}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->setEndpointArn(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static getEndpointArn(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 228
    const-string v0, "amazon_endpoint_arn_key"

    const/4 v1, 0x0

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static setEndpointArn(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "endpointArn"    # Ljava/lang/String;

    .prologue
    .line 176
    const-string v0, "amazon_endpoint_arn_key"

    invoke-static {v0, p1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 177
    return-void
.end method

.method public static syncEndpointArnToCloud(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 186
    const-string v1, "amazon_endpoint_arn_dirty_key"

    const/4 v2, 0x1

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 189
    .local v0, "dirty":Z
    if-eqz v0, :cond_0

    .line 190
    new-instance v1, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$1;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->updateAccountInfo(Landroid/content/Context;Lretrofit/Callback;)V

    .line 208
    :cond_0
    return-void
.end method


# virtual methods
.method public registerToRemotePushNotificationSystem(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 217
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$RegistrationIntentService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 218
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 219
    return-void
.end method
