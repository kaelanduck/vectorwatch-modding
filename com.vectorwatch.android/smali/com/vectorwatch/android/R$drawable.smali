.class public final Lcom/vectorwatch/android/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f020000

.field public static final abc_btn_borderless_material:I = 0x7f020001

.field public static final abc_btn_check_material:I = 0x7f020002

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f020003

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f020004

.field public static final abc_btn_default_mtrl_shape:I = 0x7f020005

.field public static final abc_btn_radio_material:I = 0x7f020006

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f020007

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f020008

.field public static final abc_btn_rating_star_off_mtrl_alpha:I = 0x7f020009

.field public static final abc_btn_rating_star_on_mtrl_alpha:I = 0x7f02000a

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f02000b

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f02000c

.field public static final abc_cab_background_internal_bg:I = 0x7f02000d

.field public static final abc_cab_background_top_material:I = 0x7f02000e

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f02000f

.field public static final abc_dialog_material_background_dark:I = 0x7f020010

.field public static final abc_dialog_material_background_light:I = 0x7f020011

.field public static final abc_edit_text_material:I = 0x7f020012

.field public static final abc_ic_ab_back_mtrl_am_alpha:I = 0x7f020013

.field public static final abc_ic_clear_mtrl_alpha:I = 0x7f020014

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f020015

.field public static final abc_ic_go_search_api_mtrl_alpha:I = 0x7f020016

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f020017

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f020018

.field public static final abc_ic_menu_moreoverflow_mtrl_alpha:I = 0x7f020019

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f02001a

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f02001b

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f02001c

.field public static final abc_ic_search_api_mtrl_alpha:I = 0x7f02001d

.field public static final abc_ic_voice_search_api_mtrl_alpha:I = 0x7f02001e

.field public static final abc_item_background_holo_dark:I = 0x7f02001f

.field public static final abc_item_background_holo_light:I = 0x7f020020

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f020021

.field public static final abc_list_focused_holo:I = 0x7f020022

.field public static final abc_list_longpressed_holo:I = 0x7f020023

.field public static final abc_list_pressed_holo_dark:I = 0x7f020024

.field public static final abc_list_pressed_holo_light:I = 0x7f020025

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f020026

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f020027

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f020028

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f020029

.field public static final abc_list_selector_holo_dark:I = 0x7f02002a

.field public static final abc_list_selector_holo_light:I = 0x7f02002b

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f02002c

.field public static final abc_popup_background_mtrl_mult:I = 0x7f02002d

.field public static final abc_ratingbar_full_material:I = 0x7f02002e

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f02002f

.field public static final abc_spinner_textfield_background_material:I = 0x7f020030

.field public static final abc_switch_thumb_material:I = 0x7f020031

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f020032

.field public static final abc_tab_indicator_material:I = 0x7f020033

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f020034

.field public static final abc_text_cursor_mtrl_alpha:I = 0x7f020035

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f020036

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f020037

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f020038

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f020039

.field public static final abc_textfield_search_material:I = 0x7f02003a

.field public static final activity:I = 0x7f02003b

.field public static final activity_active:I = 0x7f02003c

.field public static final activity_line:I = 0x7f02003d

.field public static final activity_line_blue:I = 0x7f02003e

.field public static final activity_quadrant:I = 0x7f02003f

.field public static final activity_rotate_btn:I = 0x7f020040

.field public static final activity_selected:I = 0x7f020041

.field public static final activity_set_goal_btn:I = 0x7f020042

.field public static final activity_set_goal_btn_off:I = 0x7f020043

.field public static final activity_set_goal_btn_on:I = 0x7f020044

.field public static final activity_unselected:I = 0x7f020045

.field public static final alarm_1:I = 0x7f020046

.field public static final alarm_2:I = 0x7f020047

.field public static final alarm_3:I = 0x7f020048

.field public static final alarm_4:I = 0x7f020049

.field public static final alarm_5:I = 0x7f02004a

.field public static final alarm_6:I = 0x7f02004b

.field public static final alarm_7:I = 0x7f02004c

.field public static final alarm_8:I = 0x7f02004d

.field public static final alarm_delete_btn:I = 0x7f02004e

.field public static final alarm_edit_btn:I = 0x7f02004f

.field public static final alarm_line:I = 0x7f020050

.field public static final alarm_quadrant:I = 0x7f020051

.field public static final alarms_button_selected:I = 0x7f020052

.field public static final alarms_button_unselected:I = 0x7f020053

.field public static final alarms_selected:I = 0x7f020054

.field public static final alarms_unselected:I = 0x7f020055

.field public static final apps_group_image:I = 0x7f020056

.field public static final apptheme_btn_check_off_disabled_focused_holo_dark:I = 0x7f020057

.field public static final apptheme_btn_check_off_disabled_holo_dark:I = 0x7f020058

.field public static final apptheme_btn_check_off_focused_holo_dark:I = 0x7f020059

.field public static final apptheme_btn_check_off_holo_dark:I = 0x7f02005a

.field public static final apptheme_btn_check_off_pressed_holo_dark:I = 0x7f02005b

.field public static final apptheme_btn_check_on_disabled_focused_holo_dark:I = 0x7f02005c

.field public static final apptheme_btn_check_on_disabled_holo_dark:I = 0x7f02005d

.field public static final apptheme_btn_check_on_focused_holo_dark:I = 0x7f02005e

.field public static final apptheme_btn_check_on_holo_dark:I = 0x7f02005f

.field public static final apptheme_btn_check_on_pressed_holo_dark:I = 0x7f020060

.field public static final arrow_left:I = 0x7f020061

.field public static final arrow_right:I = 0x7f020062

.field public static final arrowdown:I = 0x7f020063

.field public static final battery_25:I = 0x7f020064

.field public static final battery_50:I = 0x7f020065

.field public static final battery_75:I = 0x7f020066

.field public static final battery_background:I = 0x7f020067

.field public static final battery_empty:I = 0x7f020068

.field public static final battery_full:I = 0x7f020069

.field public static final battery_selected:I = 0x7f02006a

.field public static final bottom_clock_background:I = 0x7f02006b

.field public static final bottom_clock_hour_line:I = 0x7f02006c

.field public static final bottom_clock_minute_line:I = 0x7f02006d

.field public static final bottom_left_button:I = 0x7f02006e

.field public static final c_outline:I = 0x7f02006f

.field public static final c_outline_2:I = 0x7f020070

.field public static final checkbox_vector:I = 0x7f020071

.field public static final choice_selected:I = 0x7f020072

.field public static final choice_unselected:I = 0x7f020073

.field public static final com_facebook_button_background:I = 0x7f020074

.field public static final com_facebook_button_icon:I = 0x7f020075

.field public static final com_facebook_button_like_background:I = 0x7f020076

.field public static final com_facebook_button_like_icon_selected:I = 0x7f020077

.field public static final com_facebook_button_login_silver_background:I = 0x7f020078

.field public static final com_facebook_button_send_background:I = 0x7f020079

.field public static final com_facebook_button_send_icon:I = 0x7f02007a

.field public static final com_facebook_close:I = 0x7f02007b

.field public static final com_facebook_profile_picture_blank_portrait:I = 0x7f02007c

.field public static final com_facebook_profile_picture_blank_square:I = 0x7f02007d

.field public static final com_facebook_tooltip_black_background:I = 0x7f02007e

.field public static final com_facebook_tooltip_black_bottomnub:I = 0x7f02007f

.field public static final com_facebook_tooltip_black_topnub:I = 0x7f020080

.field public static final com_facebook_tooltip_black_xout:I = 0x7f020081

.field public static final com_facebook_tooltip_blue_background:I = 0x7f020082

.field public static final com_facebook_tooltip_blue_bottomnub:I = 0x7f020083

.field public static final com_facebook_tooltip_blue_topnub:I = 0x7f020084

.field public static final com_facebook_tooltip_blue_xout:I = 0x7f020085

.field public static final common_full_open_on_phone:I = 0x7f020086

.field public static final common_google_signin_btn_icon_dark:I = 0x7f020087

.field public static final common_google_signin_btn_icon_dark_disabled:I = 0x7f020088

.field public static final common_google_signin_btn_icon_dark_focused:I = 0x7f020089

.field public static final common_google_signin_btn_icon_dark_normal:I = 0x7f02008a

.field public static final common_google_signin_btn_icon_dark_pressed:I = 0x7f02008b

.field public static final common_google_signin_btn_icon_light:I = 0x7f02008c

.field public static final common_google_signin_btn_icon_light_disabled:I = 0x7f02008d

.field public static final common_google_signin_btn_icon_light_focused:I = 0x7f02008e

.field public static final common_google_signin_btn_icon_light_normal:I = 0x7f02008f

.field public static final common_google_signin_btn_icon_light_pressed:I = 0x7f020090

.field public static final common_google_signin_btn_text_dark:I = 0x7f020091

.field public static final common_google_signin_btn_text_dark_disabled:I = 0x7f020092

.field public static final common_google_signin_btn_text_dark_focused:I = 0x7f020093

.field public static final common_google_signin_btn_text_dark_normal:I = 0x7f020094

.field public static final common_google_signin_btn_text_dark_pressed:I = 0x7f020095

.field public static final common_google_signin_btn_text_light:I = 0x7f020096

.field public static final common_google_signin_btn_text_light_disabled:I = 0x7f020097

.field public static final common_google_signin_btn_text_light_focused:I = 0x7f020098

.field public static final common_google_signin_btn_text_light_normal:I = 0x7f020099

.field public static final common_google_signin_btn_text_light_pressed:I = 0x7f02009a

.field public static final common_ic_googleplayservices:I = 0x7f02009b

.field public static final common_plus_signin_btn_icon_dark:I = 0x7f02009c

.field public static final common_plus_signin_btn_icon_dark_disabled:I = 0x7f02009d

.field public static final common_plus_signin_btn_icon_dark_focused:I = 0x7f02009e

.field public static final common_plus_signin_btn_icon_dark_normal:I = 0x7f02009f

.field public static final common_plus_signin_btn_icon_dark_pressed:I = 0x7f0200a0

.field public static final common_plus_signin_btn_icon_light:I = 0x7f0200a1

.field public static final common_plus_signin_btn_icon_light_disabled:I = 0x7f0200a2

.field public static final common_plus_signin_btn_icon_light_focused:I = 0x7f0200a3

.field public static final common_plus_signin_btn_icon_light_normal:I = 0x7f0200a4

.field public static final common_plus_signin_btn_icon_light_pressed:I = 0x7f0200a5

.field public static final common_plus_signin_btn_text_dark:I = 0x7f0200a6

.field public static final common_plus_signin_btn_text_dark_disabled:I = 0x7f0200a7

.field public static final common_plus_signin_btn_text_dark_focused:I = 0x7f0200a8

.field public static final common_plus_signin_btn_text_dark_normal:I = 0x7f0200a9

.field public static final common_plus_signin_btn_text_dark_pressed:I = 0x7f0200aa

.field public static final common_plus_signin_btn_text_light:I = 0x7f0200ab

.field public static final common_plus_signin_btn_text_light_disabled:I = 0x7f0200ac

.field public static final common_plus_signin_btn_text_light_focused:I = 0x7f0200ad

.field public static final common_plus_signin_btn_text_light_normal:I = 0x7f0200ae

.field public static final common_plus_signin_btn_text_light_pressed:I = 0x7f0200af

.field public static final contextual_motivation_calories:I = 0x7f0200b0

.field public static final contextual_motivation_distance:I = 0x7f0200b1

.field public static final contextual_motivation_steps:I = 0x7f0200b2

.field public static final custom_scrollbar_style:I = 0x7f0200b3

.field public static final cwac_cam2_action_bar_bg_translucent:I = 0x7f0200b4

.field public static final cwac_cam2_action_bar_bg_transparent:I = 0x7f0200b5

.field public static final cwac_cam2_ic_action_camera:I = 0x7f0200b6

.field public static final cwac_cam2_ic_action_facing:I = 0x7f0200b7

.field public static final cwac_cam2_ic_action_settings:I = 0x7f0200b8

.field public static final cwac_cam2_ic_check_white:I = 0x7f0200b9

.field public static final cwac_cam2_ic_close:I = 0x7f0200ba

.field public static final cwac_cam2_ic_close_white:I = 0x7f0200bb

.field public static final cwac_cam2_ic_flash_auto:I = 0x7f0200bc

.field public static final cwac_cam2_ic_flash_off:I = 0x7f0200bd

.field public static final cwac_cam2_ic_flash_on:I = 0x7f0200be

.field public static final cwac_cam2_ic_refresh_white:I = 0x7f0200bf

.field public static final cwac_cam2_ic_remove_red_eye:I = 0x7f0200c0

.field public static final cwac_cam2_ic_stop:I = 0x7f0200c1

.field public static final cwac_cam2_ic_switch_camera:I = 0x7f0200c2

.field public static final cwac_cam2_ic_videocam:I = 0x7f0200c3

.field public static final decrease_suggestion_calories:I = 0x7f0200c4

.field public static final decrease_suggestion_distance:I = 0x7f0200c5

.field public static final decrease_suggestion_steps:I = 0x7f0200c6

.field public static final delete_alarm_button:I = 0x7f0200c7

.field public static final download_button:I = 0x7f0200c8

.field public static final drawer_shadow:I = 0x7f0200c9

.field public static final edit_alarm_button:I = 0x7f0200ca

.field public static final edit_text_stream_background:I = 0x7f0200cb

.field public static final edit_textfield_activated_stream:I = 0x7f0200cc

.field public static final edit_textfield_disabled_stream:I = 0x7f0200cd

.field public static final fab_add:I = 0x7f0200ce

.field public static final fab_background:I = 0x7f0200cf

.field public static final fab_plus_icon:I = 0x7f0200d0

.field public static final fade_yellow:I = 0x7f0200d1

.field public static final goal_achievement_calories:I = 0x7f0200d2

.field public static final goal_achievement_distance:I = 0x7f0200d3

.field public static final goal_achievement_steps:I = 0x7f0200d4

.field public static final graph_arrow:I = 0x7f0200d5

.field public static final header:I = 0x7f0200d6

.field public static final ic_action_add_alarm:I = 0x7f0200d7

.field public static final ic_action_alarms:I = 0x7f0200d8

.field public static final ic_action_bulb:I = 0x7f0200d9

.field public static final ic_action_music_2:I = 0x7f0200da

.field public static final ic_action_new:I = 0x7f0200db

.field public static final ic_action_new_event:I = 0x7f0200dc

.field public static final ic_action_time:I = 0x7f0200dd

.field public static final ic_actv_info:I = 0x7f0200de

.field public static final ic_alarms:I = 0x7f0200df

.field public static final ic_alerts:I = 0x7f0200e0

.field public static final ic_arrow_left_white:I = 0x7f0200e1

.field public static final ic_arrow_right_white:I = 0x7f0200e2

.field public static final ic_camera_alt_white_24dp:I = 0x7f0200e3

.field public static final ic_camera_white_24dp:I = 0x7f0200e4

.field public static final ic_checked_checkbox:I = 0x7f0200e5

.field public static final ic_connected:I = 0x7f0200e6

.field public static final ic_contextual:I = 0x7f0200e7

.field public static final ic_delete:I = 0x7f0200e8

.field public static final ic_delete_inactive:I = 0x7f0200e9

.field public static final ic_disconnected:I = 0x7f0200ea

.field public static final ic_drawer:I = 0x7f0200eb

.field public static final ic_fb_login:I = 0x7f0200ec

.field public static final ic_gallery:I = 0x7f0200ed

.field public static final ic_install:I = 0x7f0200ee

.field public static final ic_installed:I = 0x7f0200ef

.field public static final ic_launcher:I = 0x7f0200f0

.field public static final ic_launcher_r:I = 0x7f0200f1

.field public static final ic_launcher_s:I = 0x7f0200f2

.field public static final ic_logo:I = 0x7f0200f3

.field public static final ic_luna:I = 0x7f0200f4

.field public static final ic_meridian:I = 0x7f0200f5

.field public static final ic_news:I = 0x7f0200f6

.field public static final ic_profile:I = 0x7f0200f7

.field public static final ic_support:I = 0x7f0200f8

.field public static final ic_sync:I = 0x7f0200f9

.field public static final ic_timezone:I = 0x7f0200fa

.field public static final ic_two_watches:I = 0x7f0200fb

.field public static final ic_unchecked_checkbox:I = 0x7f0200fc

.field public static final ic_vector_login:I = 0x7f0200fd

.field public static final ic_vector_logo:I = 0x7f0200fe

.field public static final ic_vector_wheel:I = 0x7f0200ff

.field public static final ic_videocam_white_24dp:I = 0x7f020100

.field public static final ic_watch:I = 0x7f020101

.field public static final ic_watch_set:I = 0x7f020102

.field public static final ic_watchmaker_luna:I = 0x7f020103

.field public static final ic_watchmaker_meridian:I = 0x7f020104

.field public static final ic_yellow_border:I = 0x7f020105

.field public static final ic_yellow_border_button:I = 0x7f020106

.field public static final ic_yellow_border_selected:I = 0x7f020107

.field public static final increase_suggestion_calories:I = 0x7f020108

.field public static final increase_suggestion_distance:I = 0x7f020109

.field public static final increase_suggestion_steps:I = 0x7f02010a

.field public static final item_details_selector:I = 0x7f02010b

.field public static final l_nav_bar:I = 0x7f02010c

.field public static final layout_bg:I = 0x7f02010d

.field public static final link_lost:I = 0x7f02010e

.field public static final list_item_stroke:I = 0x7f02010f

.field public static final list_item_stroke_round:I = 0x7f020110

.field public static final list_item_stroke_round_not_selected:I = 0x7f020111

.field public static final list_item_stroke_store:I = 0x7f020112

.field public static final list_item_stroke_store_round:I = 0x7f020113

.field public static final luna_active:I = 0x7f020114

.field public static final luna_confirm:I = 0x7f020115

.field public static final luna_error:I = 0x7f020116

.field public static final marker2:I = 0x7f020117

.field public static final meridian_active:I = 0x7f020118

.field public static final meridian_confirm:I = 0x7f020119

.field public static final meridian_error:I = 0x7f02011a

.field public static final messenger_bubble_large_blue:I = 0x7f02011b

.field public static final messenger_bubble_large_white:I = 0x7f02011c

.field public static final messenger_bubble_small_blue:I = 0x7f02011d

.field public static final messenger_bubble_small_white:I = 0x7f02011e

.field public static final messenger_button_blue_bg_round:I = 0x7f02011f

.field public static final messenger_button_blue_bg_selector:I = 0x7f020120

.field public static final messenger_button_send_round_shadow:I = 0x7f020121

.field public static final messenger_button_white_bg_round:I = 0x7f020122

.field public static final messenger_button_white_bg_selector:I = 0x7f020123

.field public static final nav_bar_title:I = 0x7f020124

.field public static final new_icon:I = 0x7f020125

.field public static final notification_template_icon_bg:I = 0x7f02018d

.field public static final notifications_btn:I = 0x7f020126

.field public static final notifications_button_off:I = 0x7f020127

.field public static final notifications_button_on:I = 0x7f020128

.field public static final notifications_circle_selected:I = 0x7f020129

.field public static final notifications_circle_unselected:I = 0x7f02012a

.field public static final notifications_toggle:I = 0x7f02012b

.field public static final personal_record_calories:I = 0x7f02012c

.field public static final personal_record_distance:I = 0x7f02012d

.field public static final personal_record_steps:I = 0x7f02012e

.field public static final pixel_divider:I = 0x7f02012f

.field public static final placeholder_round_apps:I = 0x7f020130

.field public static final placeholder_square_apps:I = 0x7f020131

.field public static final places_ic_clear:I = 0x7f020132

.field public static final places_ic_search:I = 0x7f020133

.field public static final plus:I = 0x7f020134

.field public static final plus_dark:I = 0x7f020135

.field public static final powered_by_google_dark:I = 0x7f020136

.field public static final powered_by_google_light:I = 0x7f020137

.field public static final rating_star:I = 0x7f020138

.field public static final round_grey:I = 0x7f020139

.field public static final round_outline_2:I = 0x7f02013a

.field public static final running_man:I = 0x7f02013b

.field public static final search_icon:I = 0x7f02013c

.field public static final select_ripple_oval:I = 0x7f02013d

.field public static final select_ripple_rectangle:I = 0x7f02013e

.field public static final selectable_text_state:I = 0x7f02013f

.field public static final selectable_vector_list_item:I = 0x7f020140

.field public static final selectable_vector_text:I = 0x7f020141

.field public static final selectable_vector_text_login:I = 0x7f020142

.field public static final selectable_vector_text_recover_pass:I = 0x7f020143

.field public static final set_goal:I = 0x7f020144

.field public static final set_goal_selected:I = 0x7f020145

.field public static final set_goal_toggle:I = 0x7f020146

.field public static final set_goal_unselected:I = 0x7f020147

.field public static final settings:I = 0x7f020148

.field public static final settings_active:I = 0x7f020149

.field public static final settings_selected:I = 0x7f02014a

.field public static final settings_unselected:I = 0x7f02014b

.field public static final shadow_btn:I = 0x7f02014c

.field public static final shadow_button:I = 0x7f02014d

.field public static final share_icon:I = 0x7f02014e

.field public static final sleep_star_off:I = 0x7f02014f

.field public static final sleep_star_on:I = 0x7f020150

.field public static final snackbar_background:I = 0x7f020151

.field public static final star_rate_off:I = 0x7f020152

.field public static final star_rate_off_big:I = 0x7f020153

.field public static final star_rate_on:I = 0x7f020154

.field public static final star_rate_on_big:I = 0x7f020155

.field public static final store_button:I = 0x7f020156

.field public static final store_selected:I = 0x7f020157

.field public static final store_unselected:I = 0x7f020158

.field public static final streams_group_image:I = 0x7f020159

.field public static final tab_activity_button:I = 0x7f02015a

.field public static final tab_color_selector:I = 0x7f02015b

.field public static final tab_image_act:I = 0x7f02015c

.field public static final tab_image_alm:I = 0x7f02015d

.field public static final tab_image_set:I = 0x7f02015e

.field public static final tab_image_store:I = 0x7f02015f

.field public static final tab_image_wm:I = 0x7f020160

.field public static final tab_setings_button:I = 0x7f020161

.field public static final tab_watch_maker_button:I = 0x7f020162

.field public static final tab_watch_modes_button:I = 0x7f020163

.field public static final text_selector_activelabel:I = 0x7f020164

.field public static final tile:I = 0x7f020165

.field public static final timezones_background:I = 0x7f020166

.field public static final timezones_button_selected:I = 0x7f020167

.field public static final timezones_button_unselected:I = 0x7f020168

.field public static final timezones_hourline:I = 0x7f020169

.field public static final timezones_local_background:I = 0x7f02016a

.field public static final timezones_minuteline:I = 0x7f02016b

.field public static final timezones_plus:I = 0x7f02016c

.field public static final timezones_remove:I = 0x7f02016d

.field public static final top_clock_background:I = 0x7f02016e

.field public static final top_clock_hour_line:I = 0x7f02016f

.field public static final top_clock_minute_line:I = 0x7f020170

.field public static final top_left_button:I = 0x7f020171

.field public static final top_left_button_inactive:I = 0x7f020172

.field public static final top_middle_button_store:I = 0x7f020173

.field public static final top_right_yellow_button:I = 0x7f020174

.field public static final turn:I = 0x7f020175

.field public static final vector_custom_selector:I = 0x7f020176

.field public static final watch_loading_small:I = 0x7f020177

.field public static final watch_maker:I = 0x7f020178

.field public static final watch_maker_active:I = 0x7f020179

.field public static final watch_maker_selected:I = 0x7f02017a

.field public static final watch_maker_unselected:I = 0x7f02017b

.field public static final watch_modes:I = 0x7f02017c

.field public static final watch_modes_active:I = 0x7f02017d

.field public static final watch_update_logo:I = 0x7f02017e

.field public static final watch_update_vector:I = 0x7f02017f

.field public static final watch_vibrations_off:I = 0x7f020180

.field public static final watch_vibrations_on:I = 0x7f020181

.field public static final watchface_border_yellow_round:I = 0x7f020182

.field public static final watchface_border_yellow_square:I = 0x7f020183

.field public static final watchfaces_group_image:I = 0x7f020184

.field public static final watchmaker_locker_btn:I = 0x7f020185

.field public static final watchmodes_alarm:I = 0x7f020186

.field public static final watchmodes_alarm_selected:I = 0x7f020187

.field public static final watchmodes_battery:I = 0x7f020188

.field public static final watchmodes_clock:I = 0x7f020189

.field public static final watchmodes_down_arrow:I = 0x7f02018a

.field public static final watchmodes_line:I = 0x7f02018b

.field public static final watchmodes_up_arrow:I = 0x7f02018c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
