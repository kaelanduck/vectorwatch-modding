.class public Lcom/vectorwatch/android/models/PushUpdateRoute;
.super Ljava/lang/Object;
.source "PushUpdateRoute.java"


# instance fields
.field private appId:Ljava/lang/Integer;

.field private channelUniqueLabel:Ljava/lang/String;

.field private elementType:Ljava/lang/String;

.field private fieldId:Ljava/lang/Integer;

.field private secondsToLive:Ljava/lang/Integer;

.field private streamType:Ljava/lang/String;

.field private updateData:Ljava/lang/String;

.field private watchfaceId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->appId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getChannelUniqueLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->channelUniqueLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getElementType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->elementType:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->fieldId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSecondsToLive()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->secondsToLive:Ljava/lang/Integer;

    return-object v0
.end method

.method public getStreamType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->streamType:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->updateData:Ljava/lang/String;

    return-object v0
.end method

.method public getWatchfaceId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->watchfaceId:Ljava/lang/Integer;

    return-object v0
.end method

.method public setAppId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/Integer;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->appId:Ljava/lang/Integer;

    .line 46
    return-void
.end method

.method public setChannelUniqueLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "channelUniqueLabel"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->channelUniqueLabel:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setElementType(Ljava/lang/String;)V
    .locals 0
    .param p1, "elementType"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->elementType:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setFieldId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "fieldId"    # Ljava/lang/Integer;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->fieldId:Ljava/lang/Integer;

    .line 62
    return-void
.end method

.method public setSecondsToLive(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "secondsToLive"    # Ljava/lang/Integer;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->secondsToLive:Ljava/lang/Integer;

    .line 70
    return-void
.end method

.method public setStreamType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->streamType:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setUpdateData(Ljava/lang/String;)V
    .locals 0
    .param p1, "updateData"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->updateData:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setWatchfaceId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "watchfaceId"    # Ljava/lang/Integer;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/vectorwatch/android/models/PushUpdateRoute;->watchfaceId:Ljava/lang/Integer;

    .line 54
    return-void
.end method
