.class public Lcom/vectorwatch/android/models/Stream;
.super Ljava/lang/Object;
.source "Stream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/Stream$StreamTypes;,
        Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;,
        Lcom/vectorwatch/android/models/Stream$State;
    }
.end annotation


# static fields
.field public static final STATE_DOWNLOADED:Ljava/lang/String; = "DOWNLOADED"

.field public static final STATE_SUBSCRIBED:Ljava/lang/String; = "SUBSCRIBED"

.field public static final STATE_UNINSTALLED:Ljava/lang/String; = "UNINSTALLED"

.field public static final STATE_UNSUBSCRIBE_FROM_CHANNEL:Ljava/lang/String; = "UNSUBSCRIBE_FROM_CHANNEL"


# instance fields
.field private authCredentials:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private authInvalidated:Ljava/lang/Boolean;

.field private authMethod:Lcom/vectorwatch/android/models/AuthMethod;

.field private channelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

.field public contentVersion:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field private displayAddress:Lcom/vectorwatch/android/models/StreamPlacementModel;

.field private dynamic:Z

.field public id:Ljava/lang/Integer;

.field public img:Ljava/lang/String;

.field public labelText:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field private needsAuth:Ljava/lang/Boolean;

.field public streamData:Lcom/vectorwatch/android/models/StreamValueModel;

.field public streamPossibleSettings:Lcom/vectorwatch/android/models/settings/PossibleSettings;

.field public streamType:Ljava/lang/String;

.field public supportedType:Ljava/lang/String;

.field public uuid:Ljava/lang/String;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->authMethod:Lcom/vectorwatch/android/models/AuthMethod;

    .line 206
    return-void
.end method


# virtual methods
.method public getAuthCredentials()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->authCredentials:Ljava/util/Map;

    return-object v0
.end method

.method public getAuthInvalidated()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->authInvalidated:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->authMethod:Lcom/vectorwatch/android/models/AuthMethod;

    return-object v0
.end method

.method public getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->channelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    return-object v0
.end method

.method public getContentVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->contentVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDynamic()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/Stream;->dynamic:Z

    return v0
.end method

.method public getId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->id:Ljava/lang/Integer;

    return-object v0
.end method

.method public getImg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->img:Ljava/lang/String;

    return-object v0
.end method

.method public getLabelText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->labelText:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getRequireUserAuth()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->needsAuth:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 45
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->needsAuth:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public getStreamData()Lcom/vectorwatch/android/models/StreamValueModel;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->streamData:Lcom/vectorwatch/android/models/StreamValueModel;

    return-object v0
.end method

.method public getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->displayAddress:Lcom/vectorwatch/android/models/StreamPlacementModel;

    return-object v0
.end method

.method public getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->streamPossibleSettings:Lcom/vectorwatch/android/models/settings/PossibleSettings;

    return-object v0
.end method

.method public getStreamType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->streamType:Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->supportedType:Ljava/lang/String;

    return-object v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream;->version:Ljava/lang/Integer;

    return-object v0
.end method

.method public setAuthInvalidated(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "authInvalidated"    # Ljava/lang/Boolean;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->authInvalidated:Ljava/lang/Boolean;

    .line 225
    return-void
.end method

.method public setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V
    .locals 0
    .param p1, "authMethod"    # Lcom/vectorwatch/android/models/AuthMethod;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->authMethod:Lcom/vectorwatch/android/models/AuthMethod;

    .line 176
    return-void
.end method

.method public setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V
    .locals 0
    .param p1, "channelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->channelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 160
    return-void
.end method

.method public setContentVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentVersion"    # Ljava/lang/String;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->contentVersion:Ljava/lang/String;

    .line 144
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->description:Ljava/lang/String;

    .line 136
    return-void
.end method

.method public setDynamic(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/Stream;->dynamic:Z

    .line 62
    return-void
.end method

.method public setId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/Integer;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->id:Ljava/lang/Integer;

    .line 120
    return-void
.end method

.method public setImg(Ljava/lang/String;)V
    .locals 0
    .param p1, "img"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->img:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setLabelText(Ljava/lang/String;)V
    .locals 0
    .param p1, "labelText"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->labelText:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->name:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public setRequireUserAuth(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 51
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/Stream;->needsAuth:Ljava/lang/Boolean;

    .line 52
    return-void
.end method

.method public setStreamPlacement(Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 0
    .param p1, "displayAddress"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->displayAddress:Lcom/vectorwatch/android/models/StreamPlacementModel;

    .line 168
    return-void
.end method

.method public setStreamSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V
    .locals 0
    .param p1, "streamSettings"    # Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->streamPossibleSettings:Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .line 148
    return-void
.end method

.method public setStreamType(Ljava/lang/String;)V
    .locals 0
    .param p1, "streamType"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->streamType:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public setSupportedType(Ljava/lang/String;)V
    .locals 0
    .param p1, "supportedType"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->supportedType:Ljava/lang/String;

    .line 156
    return-void
.end method

.method public setUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->uuid:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setVersion(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/Integer;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/vectorwatch/android/models/Stream;->version:Ljava/lang/Integer;

    .line 180
    return-void
.end method
