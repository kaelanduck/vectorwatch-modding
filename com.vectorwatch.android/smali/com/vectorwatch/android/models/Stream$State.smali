.class public final enum Lcom/vectorwatch/android/models/Stream$State;
.super Ljava/lang/Enum;
.source "Stream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/Stream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/models/Stream$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/models/Stream$State;

.field public static final enum INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

.field public static final enum LOCKER:Lcom/vectorwatch/android/models/Stream$State;

.field public static final enum MARKED_FOR_DELETE:Lcom/vectorwatch/android/models/Stream$State;

.field public static final enum MARKED_FOR_SUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

.field public static final enum MARKED_FOR_UNSUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

.field public static final enum UNINSTALLED:Lcom/vectorwatch/android/models/Stream$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 187
    new-instance v0, Lcom/vectorwatch/android/models/Stream$State;

    const-string v1, "LOCKER"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/models/Stream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$State;->LOCKER:Lcom/vectorwatch/android/models/Stream$State;

    new-instance v0, Lcom/vectorwatch/android/models/Stream$State;

    const-string v1, "INSTALLED"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/models/Stream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    new-instance v0, Lcom/vectorwatch/android/models/Stream$State;

    const-string v1, "UNINSTALLED"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/models/Stream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$State;->UNINSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    new-instance v0, Lcom/vectorwatch/android/models/Stream$State;

    const-string v1, "MARKED_FOR_UNSUBSCRIBE"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/models/Stream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_UNSUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

    new-instance v0, Lcom/vectorwatch/android/models/Stream$State;

    const-string v1, "MARKED_FOR_DELETE"

    invoke-direct {v0, v1, v7}, Lcom/vectorwatch/android/models/Stream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_DELETE:Lcom/vectorwatch/android/models/Stream$State;

    new-instance v0, Lcom/vectorwatch/android/models/Stream$State;

    const-string v1, "MARKED_FOR_SUBSCRIBE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/models/Stream$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_SUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

    .line 186
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vectorwatch/android/models/Stream$State;

    sget-object v1, Lcom/vectorwatch/android/models/Stream$State;->LOCKER:Lcom/vectorwatch/android/models/Stream$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/models/Stream$State;->UNINSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_UNSUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_DELETE:Lcom/vectorwatch/android/models/Stream$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_SUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/models/Stream$State;->$VALUES:[Lcom/vectorwatch/android/models/Stream$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/models/Stream$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 186
    const-class v0, Lcom/vectorwatch/android/models/Stream$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Stream$State;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/models/Stream$State;
    .locals 1

    .prologue
    .line 186
    sget-object v0, Lcom/vectorwatch/android/models/Stream$State;->$VALUES:[Lcom/vectorwatch/android/models/Stream$State;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/models/Stream$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/models/Stream$State;

    return-object v0
.end method
