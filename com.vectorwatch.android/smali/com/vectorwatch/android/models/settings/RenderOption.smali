.class public Lcom/vectorwatch/android/models/settings/RenderOption;
.super Ljava/lang/Object;
.source "RenderOption.java"


# instance fields
.field private asYouType:Z

.field private dataType:Ljava/lang/String;

.field private hint:Ljava/lang/String;

.field private minChars:I

.field private order:I

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAsYouType()Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/settings/RenderOption;->asYouType:Z

    return v0
.end method

.method public getDataType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vectorwatch/android/models/settings/RenderOption;->dataType:Ljava/lang/String;

    return-object v0
.end method

.method public getHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vectorwatch/android/models/settings/RenderOption;->hint:Ljava/lang/String;

    return-object v0
.end method

.method public getMinChars()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/vectorwatch/android/models/settings/RenderOption;->minChars:I

    return v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/vectorwatch/android/models/settings/RenderOption;->order:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/models/settings/RenderOption;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 0
    .param p1, "hint"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vectorwatch/android/models/settings/RenderOption;->hint:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setOrder(I)V
    .locals 0
    .param p1, "order"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/vectorwatch/android/models/settings/RenderOption;->order:I

    .line 40
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/models/settings/RenderOption;->type:Ljava/lang/String;

    .line 32
    return-void
.end method
