.class public Lcom/vectorwatch/android/models/settings/Setting;
.super Ljava/lang/Object;
.source "Setting.java"


# instance fields
.field public name:Ljava/lang/String;

.field public permissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPermission;",
            ">;"
        }
    .end annotation
.end field

.field public refreshInterval:I

.field public type:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/vectorwatch/android/models/settings/Setting;->name:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/vectorwatch/android/models/settings/Setting;->name:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/vectorwatch/android/models/settings/Setting;->value:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/models/settings/Setting;->refreshInterval:I

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "refreshInterval"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/vectorwatch/android/models/settings/Setting;->name:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/vectorwatch/android/models/settings/Setting;->value:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    .line 32
    iput p4, p0, Lcom/vectorwatch/android/models/settings/Setting;->refreshInterval:I

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "refreshInterval"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPermission;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p5, "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPermission;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/vectorwatch/android/models/settings/Setting;->name:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lcom/vectorwatch/android/models/settings/Setting;->value:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    .line 39
    iput p4, p0, Lcom/vectorwatch/android/models/settings/Setting;->refreshInterval:I

    .line 40
    iput-object p5, p0, Lcom/vectorwatch/android/models/settings/Setting;->permissions:Ljava/util/List;

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPermission;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p4, "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPermission;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/vectorwatch/android/models/settings/Setting;->name:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/vectorwatch/android/models/settings/Setting;->value:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    .line 47
    iput-object p4, p0, Lcom/vectorwatch/android/models/settings/Setting;->permissions:Ljava/util/List;

    .line 48
    return-void
.end method
