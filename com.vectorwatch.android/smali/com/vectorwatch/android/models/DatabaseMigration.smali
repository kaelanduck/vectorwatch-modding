.class public Lcom/vectorwatch/android/models/DatabaseMigration;
.super Ljava/lang/Object;
.source "DatabaseMigration.java"

# interfaces
.implements Lio/realm/RealmMigration;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vectorwatch/android/models/DatabaseMigration;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/models/DatabaseMigration;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public migrate(Lio/realm/DynamicRealm;JJ)V
    .locals 8
    .param p1, "realm"    # Lio/realm/DynamicRealm;
    .param p2, "oldVersion"    # J
    .param p4, "newVersion"    # J

    .prologue
    const-wide/16 v6, 0x1

    .line 25
    invoke-virtual {p1}, Lio/realm/DynamicRealm;->getSchema()Lio/realm/RealmSchema;

    move-result-object v1

    .line 27
    .local v1, "schema":Lio/realm/RealmSchema;
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-nez v2, :cond_0

    .line 29
    :try_start_0
    const-string v2, "CloudActivityDay"

    invoke-virtual {v1, v2}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "timestamp"

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "effTime"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 30
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "avgAmpl"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 31
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "avgPeriod"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 32
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "cal"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 33
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "dist"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 34
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "timezone"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 35
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "val"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 36
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "offset"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 37
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "activityType"

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 38
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "dirtySteps"

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 39
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "dirtyCalories"

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 40
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "dirtyDistance"

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 41
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "dirtyCloud"

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 42
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "timestamp"

    .line 43
    invoke-virtual {v2, v3}, Lio/realm/RealmObjectSchema;->addPrimaryKey(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "timestamp"

    invoke-virtual {v2, v3}, Lio/realm/RealmObjectSchema;->addIndex(Ljava/lang/String;)Lio/realm/RealmObjectSchema;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_0
    add-long/2addr p2, v6

    .line 51
    :cond_0
    cmp-long v2, p2, v6

    if-nez v2, :cond_1

    .line 53
    :try_start_1
    const-string v2, "StringObject"

    invoke-virtual {v1, v2}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "string"

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "string"

    .line 54
    invoke-virtual {v2, v3}, Lio/realm/RealmObjectSchema;->addPrimaryKey(Ljava/lang/String;)Lio/realm/RealmObjectSchema;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 59
    :goto_1
    add-long/2addr p2, v6

    .line 62
    :cond_1
    const-wide/16 v2, 0x2

    cmp-long v2, p2, v2

    if-nez v2, :cond_2

    .line 64
    :try_start_2
    const-string v2, "LongObject"

    invoke-virtual {v1, v2}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "longValue"

    const-class v4, Ljava/lang/Long;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "longValue"

    .line 65
    invoke-virtual {v2, v3}, Lio/realm/RealmObjectSchema;->addPrimaryKey(Ljava/lang/String;)Lio/realm/RealmObjectSchema;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 70
    :goto_2
    add-long/2addr p2, v6

    .line 73
    :cond_2
    const-wide/16 v2, 0x3

    cmp-long v2, p2, v2

    if-nez v2, :cond_3

    .line 75
    :try_start_3
    const-string v2, "ActivityAlert"

    invoke-virtual {v1, v2}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "stringValues"

    const-string v4, "StringObject"

    .line 76
    invoke-virtual {v1, v4}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmObjectSchema;->addRealmListField(Ljava/lang/String;Lio/realm/RealmObjectSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "longValues"

    const-string v4, "LongObject"

    .line 77
    invoke-virtual {v1, v4}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmObjectSchema;->addRealmListField(Ljava/lang/String;Lio/realm/RealmObjectSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "message"

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 78
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "timestamp"

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 79
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "timestamp"

    invoke-virtual {v2, v3}, Lio/realm/RealmObjectSchema;->addPrimaryKey(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "uuid"

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 80
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "type"

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 81
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;

    move-result-object v2

    const-string v3, "activityType"

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    new-array v5, v5, [Lio/realm/FieldAttribute;

    .line 82
    invoke-virtual {v2, v3, v4, v5}, Lio/realm/RealmObjectSchema;->addField(Ljava/lang/String;Ljava/lang/Class;[Lio/realm/FieldAttribute;)Lio/realm/RealmObjectSchema;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 87
    :goto_3
    add-long/2addr p2, v6

    .line 89
    :cond_3
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/models/DatabaseMigration;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while performing migration. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 55
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 56
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/models/DatabaseMigration;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while performing migration. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 66
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 67
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/models/DatabaseMigration;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while performing migration. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 83
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    .line 84
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v2, Lcom/vectorwatch/android/models/DatabaseMigration;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while performing migration. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_3
.end method
