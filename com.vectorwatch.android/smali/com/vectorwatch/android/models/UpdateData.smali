.class public Lcom/vectorwatch/android/models/UpdateData;
.super Ljava/lang/Object;
.source "UpdateData.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field images:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SoftwareUpdateData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SoftwareUpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 12
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SoftwareUpdateData;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateData;->images:Ljava/util/List;

    .line 14
    return-void
.end method


# virtual methods
.method public getImages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SoftwareUpdateData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/models/UpdateData;->images:Ljava/util/List;

    return-object v0
.end method

.method public setImages(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SoftwareUpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "images":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SoftwareUpdateData;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/UpdateData;->images:Ljava/util/List;

    .line 22
    return-void
.end method
