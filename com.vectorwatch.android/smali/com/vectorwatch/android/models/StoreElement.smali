.class public Lcom/vectorwatch/android/models/StoreElement;
.super Ljava/lang/Object;
.source "StoreElement.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private contentPVersion:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private dynamicSettings:Ljava/lang/Boolean;

.field private editImg:Ljava/lang/String;

.field private extendedDescription:Ljava/lang/String;

.field private id:I

.field private img:Ljava/lang/String;

.field private imgURL:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private needsAuth:Ljava/lang/Boolean;

.field private needsLocation:Ljava/lang/Boolean;

.field private newInStore:Ljava/lang/Boolean;

.field private orderId:I

.field private rating:F

.field private type:Ljava/lang/String;

.field private uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;FLjava/lang/String;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "orderId"    # I
    .param p3, "contentPVersion"    # Ljava/lang/String;
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "description"    # Ljava/lang/String;
    .param p6, "imgURL"    # Ljava/lang/String;
    .param p7, "uuid"    # Ljava/lang/String;
    .param p8, "type"    # Ljava/lang/String;
    .param p9, "newInStore"    # Ljava/lang/Boolean;
    .param p10, "rating"    # F
    .param p11, "extendedDescription"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput p1, p0, Lcom/vectorwatch/android/models/StoreElement;->id:I

    .line 31
    iput p2, p0, Lcom/vectorwatch/android/models/StoreElement;->orderId:I

    .line 32
    iput-object p3, p0, Lcom/vectorwatch/android/models/StoreElement;->contentPVersion:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lcom/vectorwatch/android/models/StoreElement;->name:Ljava/lang/String;

    .line 34
    iput-object p5, p0, Lcom/vectorwatch/android/models/StoreElement;->description:Ljava/lang/String;

    .line 35
    iput-object p6, p0, Lcom/vectorwatch/android/models/StoreElement;->imgURL:Ljava/lang/String;

    .line 36
    iput-object p7, p0, Lcom/vectorwatch/android/models/StoreElement;->uuid:Ljava/lang/String;

    .line 37
    iput-object p8, p0, Lcom/vectorwatch/android/models/StoreElement;->type:Ljava/lang/String;

    .line 38
    iput-object p9, p0, Lcom/vectorwatch/android/models/StoreElement;->newInStore:Ljava/lang/Boolean;

    .line 39
    iput p10, p0, Lcom/vectorwatch/android/models/StoreElement;->rating:F

    .line 40
    iput-object p11, p0, Lcom/vectorwatch/android/models/StoreElement;->extendedDescription:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public getContentPVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->contentPVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEditImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->editImg:Ljava/lang/String;

    return-object v0
.end method

.method public getExtendedDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->extendedDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/vectorwatch/android/models/StoreElement;->id:I

    return v0
.end method

.method public getImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->img:Ljava/lang/String;

    return-object v0
.end method

.method public getImgURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->imgURL:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNewInStore()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->newInStore:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getOrderId()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/vectorwatch/android/models/StoreElement;->orderId:I

    return v0
.end method

.method public getRating()F
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/vectorwatch/android/models/StoreElement;->rating:F

    return v0
.end method

.method public getRequireLocation()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->needsLocation:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 54
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->needsLocation:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public getRequireUserAuth()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->needsAuth:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 45
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->needsAuth:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public hasDynamicSettings()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->dynamicSettings:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->dynamicSettings:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public isStream()Z
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->type:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->type:Ljava/lang/String;

    const-string v1, "STREAM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContentPVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentPVersion"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->contentPVersion:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->description:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setDynamicSettings(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "dynamicSettings"    # Ljava/lang/Boolean;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->dynamicSettings:Ljava/lang/Boolean;

    .line 169
    return-void
.end method

.method public setEditImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "editImage"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->editImg:Ljava/lang/String;

    .line 185
    return-void
.end method

.method public setExtendedDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "extendedDescription"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->extendedDescription:Ljava/lang/String;

    .line 165
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/vectorwatch/android/models/StoreElement;->id:I

    .line 78
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "img"    # Ljava/lang/String;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->img:Ljava/lang/String;

    .line 177
    return-void
.end method

.method public setImgURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "imgURL"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->imgURL:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->name:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public setNewInStore(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "newInStore"    # Ljava/lang/Boolean;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->newInStore:Ljava/lang/Boolean;

    .line 149
    return-void
.end method

.method public setOrderId(I)V
    .locals 0
    .param p1, "orderId"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/vectorwatch/android/models/StoreElement;->orderId:I

    .line 86
    return-void
.end method

.method public setRating(F)V
    .locals 0
    .param p1, "rating"    # F

    .prologue
    .line 156
    iput p1, p0, Lcom/vectorwatch/android/models/StoreElement;->rating:F

    .line 157
    return-void
.end method

.method public setRequireLocation(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 69
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->needsLocation:Ljava/lang/Boolean;

    .line 70
    return-void
.end method

.method public setRequireUserAuth(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 65
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/StoreElement;->needsAuth:Ljava/lang/Boolean;

    .line 66
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->type:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/vectorwatch/android/models/StoreElement;->uuid:Ljava/lang/String;

    .line 126
    return-void
.end method
