.class public Lcom/vectorwatch/android/models/AppIdentification;
.super Ljava/lang/Object;
.source "AppIdentification.java"


# instance fields
.field appId:Ljava/lang/Integer;

.field appUuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/Integer;
    .param p2, "appUuid"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/vectorwatch/android/models/AppIdentification;->appId:Ljava/lang/Integer;

    .line 15
    iput-object p2, p0, Lcom/vectorwatch/android/models/AppIdentification;->appUuid:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/AppIdentification;->appId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAppUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/models/AppIdentification;->appUuid:Ljava/lang/String;

    return-object v0
.end method

.method public setAppId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/Integer;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/models/AppIdentification;->appId:Ljava/lang/Integer;

    .line 24
    return-void
.end method

.method public setAppUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "appUuid"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/models/AppIdentification;->appUuid:Ljava/lang/String;

    .line 32
    return-void
.end method
