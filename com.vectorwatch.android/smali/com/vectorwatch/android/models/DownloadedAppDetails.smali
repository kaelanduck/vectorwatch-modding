.class public Lcom/vectorwatch/android/models/DownloadedAppDetails;
.super Ljava/lang/Object;
.source "DownloadedAppDetails.java"


# instance fields
.field public appType:Ljava/lang/String;

.field public content:Lcom/vectorwatch/android/models/AppContent;

.field public contentPVersion:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field private dynamicSettings:Ljava/lang/Boolean;

.field public editImg:Ljava/lang/String;

.field public id:Ljava/lang/Integer;

.field public img:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field private needsAuth:Ljava/lang/Boolean;

.field private needsLocation:Ljava/lang/Boolean;

.field public rating:F

.field public streamData:Lcom/vectorwatch/android/models/StreamValueModel;

.field public streams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation
.end field

.field private userSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContent()Lcom/vectorwatch/android/models/AppContent;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    return-object v0
.end method

.method public getRequireLocation()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->needsLocation:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->needsLocation:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public getRequireUserAuth()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->needsAuth:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 59
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->needsAuth:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public getUserSettings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->userSettings:Ljava/util/Map;

    return-object v0
.end method

.method public hasDynamicSettings()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->dynamicSettings:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 48
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->dynamicSettings:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public setContent(Lcom/vectorwatch/android/models/AppContent;)V
    .locals 0
    .param p1, "content"    # Lcom/vectorwatch/android/models/AppContent;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    .line 82
    return-void
.end method

.method public setHasDynamicSettings(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 43
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->dynamicSettings:Ljava/lang/Boolean;

    .line 44
    return-void
.end method

.method public setRequireLocation(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 54
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->needsLocation:Ljava/lang/Boolean;

    .line 55
    return-void
.end method

.method public setRequireUserAuth(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 65
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->needsAuth:Ljava/lang/Boolean;

    .line 66
    return-void
.end method

.method public setUserSettings(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "settings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->userSettings:Ljava/util/Map;

    .line 74
    return-void
.end method
