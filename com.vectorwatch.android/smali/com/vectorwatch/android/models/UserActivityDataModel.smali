.class public Lcom/vectorwatch/android/models/UserActivityDataModel;
.super Ljava/lang/Object;
.source "UserActivityDataModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;
    }
.end annotation


# instance fields
.field private avgAmpl:I

.field private avgPeriod:I

.field private cal:I

.field private dist:I

.field private effTime:I

.field private id:Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;

.field private offset:I

.field private timezone:I

.field private val:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIIIILjava/lang/String;JI)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;
    .param p2, "effTime"    # I
    .param p3, "avgAmpl"    # I
    .param p4, "avgPeriod"    # I
    .param p5, "cal"    # I
    .param p6, "dist"    # I
    .param p7, "timezone"    # I
    .param p8, "type"    # Ljava/lang/String;
    .param p9, "timestamp"    # J
    .param p11, "offset"    # I

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->val:Ljava/lang/String;

    .line 23
    new-instance v0, Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;-><init>(Lcom/vectorwatch/android/models/UserActivityDataModel;)V

    iput-object v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->id:Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;

    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->id:Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;

    invoke-virtual {v0, p8}, Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;->setType(Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->id:Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;

    invoke-virtual {v0, p9, p10}, Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;->setTs(J)V

    .line 26
    iput p3, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->avgAmpl:I

    .line 27
    iput p4, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->avgPeriod:I

    .line 28
    iput p5, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->cal:I

    .line 29
    iput p6, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->dist:I

    .line 30
    iput p2, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->effTime:I

    .line 31
    iput p7, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->timezone:I

    .line 32
    iput p11, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->offset:I

    .line 33
    return-void
.end method


# virtual methods
.method public getAvgAmpl()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->avgAmpl:I

    return v0
.end method

.method public getAvgPeriod()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->avgPeriod:I

    return v0
.end method

.method public getCal()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->cal:I

    return v0
.end method

.method public getDist()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->dist:I

    return v0
.end method

.method public getEffTime()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->effTime:I

    return v0
.end method

.method public getId()Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->id:Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;

    return-object v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->offset:I

    return v0
.end method

.method public getTimezone()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->timezone:I

    return v0
.end method

.method public getVal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->val:Ljava/lang/String;

    return-object v0
.end method

.method public setAvgAmpl(I)V
    .locals 0
    .param p1, "avgAmpl"    # I

    .prologue
    .line 95
    iput p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->avgAmpl:I

    .line 96
    return-void
.end method

.method public setAvgPeriod(I)V
    .locals 0
    .param p1, "avgPeriod"    # I

    .prologue
    .line 103
    iput p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->avgPeriod:I

    .line 104
    return-void
.end method

.method public setCal(I)V
    .locals 0
    .param p1, "cal"    # I

    .prologue
    .line 111
    iput p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->cal:I

    .line 112
    return-void
.end method

.method public setDist(I)V
    .locals 0
    .param p1, "dist"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->dist:I

    .line 120
    return-void
.end method

.method public setEffTime(I)V
    .locals 0
    .param p1, "effTime"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->effTime:I

    .line 88
    return-void
.end method

.method public setId(Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;)V
    .locals 0
    .param p1, "id"    # Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->id:Lcom/vectorwatch/android/models/UserActivityDataModel$Identity;

    .line 80
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->offset:I

    .line 72
    return-void
.end method

.method public setTimezone(I)V
    .locals 0
    .param p1, "timezone"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->timezone:I

    .line 128
    return-void
.end method

.method public setVal(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vectorwatch/android/models/UserActivityDataModel;->val:Ljava/lang/String;

    .line 64
    return-void
.end method
