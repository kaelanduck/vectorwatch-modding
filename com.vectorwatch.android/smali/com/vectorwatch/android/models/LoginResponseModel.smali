.class public Lcom/vectorwatch/android/models/LoginResponseModel;
.super Ljava/lang/Object;
.source "LoginResponseModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;,
        Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;
    }
.end annotation


# instance fields
.field private data:Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;

.field private message:Ljava/lang/String;

.field private token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method


# virtual methods
.method public getData()Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/models/LoginResponseModel;->data:Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/models/LoginResponseModel;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/vectorwatch/android/models/LoginResponseModel;->token:Ljava/lang/String;

    return-object v0
.end method
