.class public Lcom/vectorwatch/android/models/StreamPropertyValueModel;
.super Ljava/lang/Object;
.source "StreamPropertyValueModel.java"


# instance fields
.field private name:Ljava/lang/String;

.field private permissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPermission;",
            ">;"
        }
    .end annotation
.end field

.field private refreshInterval:I

.field private type:Ljava/lang/String;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->name:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPermission;",
            ">;"
        }
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->permissions:Ljava/util/List;

    return-object v0
.end method

.method public getRefreshInterval()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->refreshInterval:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->value:Ljava/lang/String;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->name:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setPermissions(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPermission;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "permissions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPermission;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->permissions:Ljava/util/List;

    .line 24
    return-void
.end method

.method public setRefreshInterval(I)V
    .locals 0
    .param p1, "refreshInterval"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->refreshInterval:I

    .line 68
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->type:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->value:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->name:Ljava/lang/String;

    return-object v0
.end method
