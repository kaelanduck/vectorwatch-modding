.class public Lcom/vectorwatch/android/models/ActivityValueModel;
.super Ljava/lang/Object;
.source "ActivityValueModel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/vectorwatch/android/models/ActivityValueModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private activityType:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

.field private endTime:J

.field private startTime:J

.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/vectorwatch/android/models/ActivityValueModel$1;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/ActivityValueModel$1;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/models/ActivityValueModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->startTime:J

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->endTime:J

    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->value:I

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 26
    .local v0, "activityTypeVal":I
    packed-switch v0, :pswitch_data_0

    .line 38
    :goto_0
    return-void

    .line 28
    :pswitch_0
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    iput-object v1, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->activityType:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    goto :goto_0

    .line 31
    :pswitch_1
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    iput-object v1, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->activityType:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    goto :goto_0

    .line 34
    :pswitch_2
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    iput-object v1, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->activityType:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    goto :goto_0

    .line 26
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public getActivityType()Lcom/vectorwatch/android/utils/Constants$GoogleFitData;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->activityType:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    return-object v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->endTime:J

    return-wide v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->startTime:J

    return-wide v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->value:I

    return v0
.end method

.method public setActivityType(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V
    .locals 0
    .param p1, "activityType"    # Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->activityType:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    .line 70
    return-void
.end method

.method public setEndTime(J)V
    .locals 1
    .param p1, "endTime"    # J

    .prologue
    .line 53
    iput-wide p1, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->endTime:J

    .line 54
    return-void
.end method

.method public setStartTime(J)V
    .locals 1
    .param p1, "startTime"    # J

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->startTime:J

    .line 46
    return-void
.end method

.method public setValue(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->value:I

    .line 62
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->startTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 80
    iget-wide v0, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->endTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 81
    iget v0, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->value:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    iget-object v0, p0, Lcom/vectorwatch/android/models/ActivityValueModel;->activityType:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->getVal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 83
    return-void
.end method
