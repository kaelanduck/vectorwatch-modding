.class public Lcom/vectorwatch/android/models/SleepCloud;
.super Ljava/lang/Object;
.source "SleepCloud.java"


# instance fields
.field private endTs:Ljava/lang/String;

.field private id:Lcom/vectorwatch/android/models/SleepIds;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/SleepIds;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Lcom/vectorwatch/android/models/SleepIds;
    .param p2, "endTs"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/vectorwatch/android/models/SleepCloud;->id:Lcom/vectorwatch/android/models/SleepIds;

    .line 13
    iput-object p2, p0, Lcom/vectorwatch/android/models/SleepCloud;->endTs:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public getEndTs()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/models/SleepCloud;->endTs:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Lcom/vectorwatch/android/models/SleepIds;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/models/SleepCloud;->id:Lcom/vectorwatch/android/models/SleepIds;

    return-object v0
.end method

.method public setEndTs(Ljava/lang/String;)V
    .locals 0
    .param p1, "endTs"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vectorwatch/android/models/SleepCloud;->endTs:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setId(Lcom/vectorwatch/android/models/SleepIds;)V
    .locals 0
    .param p1, "id"    # Lcom/vectorwatch/android/models/SleepIds;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/models/SleepCloud;->id:Lcom/vectorwatch/android/models/SleepIds;

    .line 22
    return-void
.end method
