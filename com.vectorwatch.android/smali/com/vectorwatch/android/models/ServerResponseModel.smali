.class public Lcom/vectorwatch/android/models/ServerResponseModel;
.super Ljava/lang/Object;
.source "ServerResponseModel.java"


# instance fields
.field private error:Ljava/lang/String;

.field private message:Ljava/lang/String;

.field private token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    iget-object v0, p0, Lcom/vectorwatch/android/models/ServerResponseModel;->error:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vectorwatch/android/models/ServerResponseModel;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/models/ServerResponseModel;->token:Ljava/lang/String;

    return-object v0
.end method
