.class public Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
.super Ljava/lang/Object;
.source "AppCallbackProxyResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Callback;,
        Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Item;,
        Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Data;,
        Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;,
        Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponse;,
        Lcom/vectorwatch/android/models/AppCallbackProxyResponse$AppPushData;,
        Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponseRoot;,
        Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;,
        Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;
    }
.end annotation


# instance fields
.field public data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

.field public message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    return-void
.end method

.method public static fromPushResponse(Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponse;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    .locals 4
    .param p0, "response"    # Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponse;

    .prologue
    .line 22
    new-instance v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;-><init>()V

    .line 23
    .local v1, "proxyResponse":Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    new-instance v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;-><init>()V

    .line 24
    .local v0, "baseData":Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;
    new-instance v2, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    invoke-direct {v2}, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;-><init>()V

    iput-object v2, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    .line 25
    iget-object v2, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    iget-object v3, p0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponse;->appPopup:Lcom/vectorwatch/android/models/AlertMessage;

    iput-object v3, v2, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;->appPopup:Lcom/vectorwatch/android/models/AlertMessage;

    .line 26
    iget-object v2, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    iget-object v3, p0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponse;->data:Ljava/util/List;

    iput-object v3, v2, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;->commands:Ljava/util/List;

    .line 27
    iput-object v0, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    .line 28
    return-object v1
.end method

.method public static fromResponse(Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    .locals 2
    .param p0, "response"    # Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    .prologue
    .line 14
    new-instance v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;-><init>()V

    .line 15
    .local v1, "proxyResponse":Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    new-instance v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;-><init>()V

    .line 16
    .local v0, "baseData":Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;
    iput-object p0, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    .line 17
    iput-object v0, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    .line 18
    return-object v1
.end method
