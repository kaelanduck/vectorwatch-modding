.class public Lcom/vectorwatch/android/models/ChannelSettingsChange;
.super Lcom/vectorwatch/android/models/BaseCloudRequestModel;
.source "ChannelSettingsChange.java"


# instance fields
.field private location:Lcom/vectorwatch/android/models/LocationCloudRequestModel;

.field private newChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

.field private oldChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    return-void
.end method


# virtual methods
.method public getLocation()Lcom/vectorwatch/android/models/LocationCloudRequestModel;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/ChannelSettingsChange;->location:Lcom/vectorwatch/android/models/LocationCloudRequestModel;

    return-object v0
.end method

.method public getNewChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/models/ChannelSettingsChange;->newChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    return-object v0
.end method

.method public getOldChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vectorwatch/android/models/ChannelSettingsChange;->oldChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    return-object v0
.end method

.method public setLocation(Lcom/vectorwatch/android/models/LocationCloudRequestModel;)V
    .locals 0
    .param p1, "location"    # Lcom/vectorwatch/android/models/LocationCloudRequestModel;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vectorwatch/android/models/ChannelSettingsChange;->location:Lcom/vectorwatch/android/models/LocationCloudRequestModel;

    .line 34
    return-void
.end method

.method public setNewChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V
    .locals 0
    .param p1, "newChannelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vectorwatch/android/models/ChannelSettingsChange;->newChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 26
    return-void
.end method

.method public setOldChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V
    .locals 0
    .param p1, "oldChannelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/vectorwatch/android/models/ChannelSettingsChange;->oldChannelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 18
    return-void
.end method
