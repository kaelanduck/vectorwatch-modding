.class public Lcom/vectorwatch/android/models/WatchLogDataModel;
.super Ljava/lang/Object;
.source "WatchLogDataModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/models/WatchLogDataModel$Identity;
    }
.end annotation


# instance fields
.field private batteryLevel:I

.field private disconnectPeriod:I

.field private id:Lcom/vectorwatch/android/models/WatchLogDataModel$Identity;

.field private noBacklightActivations:I

.field private noButtonPress:I

.field private noConn:I

.field private noDisconn:I

.field private noGlances:I

.field private noNot:I

.field private noRxBLE:I

.field private noShakerActivations:I

.field private noTxBLE:I

.field private offset:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    return-void
.end method


# virtual methods
.method public getOffset()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->offset:I

    return v0
.end method

.method public setBatteryLevel(I)V
    .locals 0
    .param p1, "batteryLevel"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->batteryLevel:I

    .line 60
    return-void
.end method

.method public setDisconnectPeriod(I)V
    .locals 0
    .param p1, "disconnectPeriod"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->disconnectPeriod:I

    .line 64
    return-void
.end method

.method public setId(I)V
    .locals 2
    .param p1, "ts"    # I

    .prologue
    .line 67
    new-instance v0, Lcom/vectorwatch/android/models/WatchLogDataModel$Identity;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/android/models/WatchLogDataModel$Identity;-><init>(Lcom/vectorwatch/android/models/WatchLogDataModel;Lcom/vectorwatch/android/models/WatchLogDataModel$1;)V

    iput-object v0, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->id:Lcom/vectorwatch/android/models/WatchLogDataModel$Identity;

    .line 68
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->id:Lcom/vectorwatch/android/models/WatchLogDataModel$Identity;

    iput p1, v0, Lcom/vectorwatch/android/models/WatchLogDataModel$Identity;->ts:I

    .line 69
    return-void
.end method

.method public setNoBacklightActivations(I)V
    .locals 0
    .param p1, "noBacklightActivations"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->noBacklightActivations:I

    .line 52
    return-void
.end method

.method public setNoButtonPress(I)V
    .locals 0
    .param p1, "noButtonPress"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->noButtonPress:I

    .line 48
    return-void
.end method

.method public setNoConn(I)V
    .locals 0
    .param p1, "noConn"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->noConn:I

    .line 44
    return-void
.end method

.method public setNoDisconn(I)V
    .locals 0
    .param p1, "noDisconn"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->noDisconn:I

    .line 40
    return-void
.end method

.method public setNoGlances(I)V
    .locals 0
    .param p1, "noGlances"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->noGlances:I

    .line 28
    return-void
.end method

.method public setNoNot(I)V
    .locals 0
    .param p1, "noNot"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->noNot:I

    .line 24
    return-void
.end method

.method public setNoRxBLE(I)V
    .locals 0
    .param p1, "noRxBLE"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->noRxBLE:I

    .line 32
    return-void
.end method

.method public setNoShakerActivations(I)V
    .locals 0
    .param p1, "noShakerActivations"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->noShakerActivations:I

    .line 56
    return-void
.end method

.method public setNoTxBLE(I)V
    .locals 0
    .param p1, "noTxBLE"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->noTxBLE:I

    .line 36
    return-void
.end method

.method public setOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/vectorwatch/android/models/WatchLogDataModel;->offset:I

    .line 20
    return-void
.end method
