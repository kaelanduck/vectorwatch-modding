.class public Lcom/vectorwatch/android/models/StreamChannelSettings;
.super Ljava/lang/Object;
.source "StreamChannelSettings.java"


# instance fields
.field private subscriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPlacementModel;",
            ">;"
        }
    .end annotation
.end field

.field private uniqueLabel:Ljava/lang/String;

.field private userSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSubscriptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPlacementModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamChannelSettings;->subscriptions:Ljava/util/List;

    return-object v0
.end method

.method public getUniqueLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamChannelSettings;->uniqueLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getUserSettings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamChannelSettings;->userSettings:Ljava/util/Map;

    return-object v0
.end method

.method public setSubscriptions(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPlacementModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamChannelSettings;->subscriptions:Ljava/util/List;

    .line 20
    return-void
.end method

.method public setUniqueLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "uniqueLabel"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamChannelSettings;->uniqueLabel:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setUserSettings(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamChannelSettings;->userSettings:Ljava/util/Map;

    .line 40
    return-void
.end method
