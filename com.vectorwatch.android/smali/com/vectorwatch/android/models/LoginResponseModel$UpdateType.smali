.class public final enum Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;
.super Ljava/lang/Enum;
.source "LoginResponseModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/LoginResponseModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpdateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

.field public static final enum ALPHA:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

.field public static final enum DEV:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

.field public static final enum PRODUCTION:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

.field public static final enum STAGE:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;


# instance fields
.field private val:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 65
    new-instance v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    const-string v1, "DEV"

    const-string v2, "DEV"

    invoke-direct {v0, v1, v3, v2}, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->DEV:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    .line 66
    new-instance v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    const-string v1, "ALPHA"

    const-string v2, "ALPHA"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->ALPHA:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    .line 67
    new-instance v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    const-string v1, "STAGE"

    const-string v2, "STAGE"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->STAGE:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    .line 68
    new-instance v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    const-string v1, "PRODUCTION"

    const-string v2, "PRODUCTION"

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->PRODUCTION:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    .line 64
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    sget-object v1, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->DEV:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->ALPHA:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->STAGE:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->PRODUCTION:Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->$VALUES:[Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "val"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 73
    iput-object p3, p0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->val:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->$VALUES:[Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;

    return-object v0
.end method


# virtual methods
.method public getVal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/models/LoginResponseModel$UpdateType;->val:Ljava/lang/String;

    return-object v0
.end method
