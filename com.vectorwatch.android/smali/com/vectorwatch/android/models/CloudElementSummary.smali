.class public Lcom/vectorwatch/android/models/CloudElementSummary;
.super Ljava/lang/Object;
.source "CloudElementSummary.java"


# static fields
.field public static final APP_TYPE_CLOUD:Ljava/lang/String; = "CLOUD"

.field public static final APP_TYPE_SYSTEM:Ljava/lang/String; = "SYSTEM"

.field public static final APP_TYPE_WATCHFACE:Ljava/lang/String; = "WATCHFACE"


# instance fields
.field private appSettings:Lcom/vectorwatch/android/models/settings/PossibleSettings;

.field private appType:Ljava/lang/String;

.field private authInvalidated:Ljava/lang/Boolean;

.field private authMethod:Lcom/vectorwatch/android/models/AuthMethod;

.field private description:Ljava/lang/String;

.field private dynamicSettings:Ljava/lang/Boolean;

.field private editImg:Ljava/lang/String;

.field private id:I

.field private img:Ljava/lang/String;

.field private isStream:Z

.field private name:Ljava/lang/String;

.field private needsAuth:Ljava/lang/Boolean;

.field private needsLocation:Ljava/lang/Boolean;

.field private rating:F

.field private state:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

.field private userSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->authMethod:Lcom/vectorwatch/android/models/AuthMethod;

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->isStream:Z

    return-void
.end method

.method public static fromStoreElement(Lcom/vectorwatch/android/models/StoreElement;)Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 2
    .param p0, "element"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 187
    new-instance v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 188
    .local v0, "summary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 189
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StoreElement;->getRequireLocation()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRequireLocation(Z)V

    .line 191
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StoreElement;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRequireUserAuth(Z)V

    .line 192
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StoreElement;->hasDynamicSettings()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setHasDynamicSettings(Z)V

    .line 193
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setName(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StoreElement;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setDescription(Ljava/lang/String;)V

    .line 195
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setType(Ljava/lang/String;)V

    .line 196
    return-object v0
.end method


# virtual methods
.method public getAppPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->appSettings:Lcom/vectorwatch/android/models/settings/PossibleSettings;

    return-object v0
.end method

.method public getAuthInvalidated()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->authInvalidated:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->authMethod:Lcom/vectorwatch/android/models/AuthMethod;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getEditImg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->editImg:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->id:I

    return v0
.end method

.method public getImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->img:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getRating()F
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->rating:F

    return v0
.end method

.method public getRequireLocation()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->needsLocation:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->needsLocation:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public getRequireUserAuth()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->needsAuth:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 66
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 68
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->needsAuth:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->appType:Ljava/lang/String;

    return-object v0
.end method

.method public getUserSettings()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->userSettings:Ljava/util/Map;

    return-object v0
.end method

.method public getUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public hasDynamicSettings()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->dynamicSettings:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 55
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->dynamicSettings:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public isStream()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->isStream:Z

    return v0
.end method

.method public setAppPossibleSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V
    .locals 0
    .param p1, "settings"    # Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->appSettings:Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .line 104
    return-void
.end method

.method public setAppState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)V
    .locals 0
    .param p1, "state"    # Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->state:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    .line 35
    return-void
.end method

.method public setAuthInvalidated(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "authInvalidated"    # Ljava/lang/Boolean;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->authInvalidated:Ljava/lang/Boolean;

    .line 201
    return-void
.end method

.method public setAuthMethod(Lcom/vectorwatch/android/models/AuthMethod;)V
    .locals 0
    .param p1, "authMethod"    # Lcom/vectorwatch/android/models/AuthMethod;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->authMethod:Lcom/vectorwatch/android/models/AuthMethod;

    .line 100
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->description:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setEditImg(Ljava/lang/String;)V
    .locals 0
    .param p1, "editImg"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->editImg:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setHasDynamicSettings(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 61
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->dynamicSettings:Ljava/lang/Boolean;

    .line 62
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 170
    iput p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->id:I

    .line 171
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "mImage"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->img:Ljava/lang/String;

    .line 151
    return-void
.end method

.method public setIsStream(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->isStream:Z

    .line 115
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->name:Ljava/lang/String;

    .line 175
    return-void
.end method

.method public setRating(F)V
    .locals 0
    .param p1, "rating"    # F

    .prologue
    .line 182
    iput p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->rating:F

    .line 183
    return-void
.end method

.method public setRequireLocation(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 83
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->needsLocation:Ljava/lang/Boolean;

    .line 84
    return-void
.end method

.method public setRequireUserAuth(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 72
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->needsAuth:Ljava/lang/Boolean;

    .line 73
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mAppType"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->appType:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public setUserSettings(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "settings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->userSettings:Ljava/util/Map;

    .line 92
    return-void
.end method

.method public setUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->uuid:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloudApp{mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAppType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->appType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/models/CloudElementSummary;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
