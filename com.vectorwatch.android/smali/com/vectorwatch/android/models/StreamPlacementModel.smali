.class public Lcom/vectorwatch/android/models/StreamPlacementModel;
.super Ljava/lang/Object;
.source "StreamPlacementModel.java"


# instance fields
.field private appId:Ljava/lang/Integer;

.field private appUuid:Ljava/lang/String;

.field private elementId:Ljava/lang/Integer;

.field private watchFaceId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAppId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamPlacementModel;->appId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAppUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamPlacementModel;->appUuid:Ljava/lang/String;

    return-object v0
.end method

.method public getElementId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamPlacementModel;->elementId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getWatchFaceId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamPlacementModel;->watchFaceId:Ljava/lang/Integer;

    return-object v0
.end method

.method public setAppId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "appId"    # Ljava/lang/Integer;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamPlacementModel;->appId:Ljava/lang/Integer;

    .line 26
    return-void
.end method

.method public setAppUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "appUuid"    # Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamPlacementModel;->appUuid:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public setElementId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "elementId"    # Ljava/lang/Integer;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamPlacementModel;->elementId:Ljava/lang/Integer;

    .line 34
    return-void
.end method

.method public setWatchFaceId(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "watchFaceId"    # Ljava/lang/Integer;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamPlacementModel;->watchFaceId:Ljava/lang/Integer;

    .line 42
    return-void
.end method
