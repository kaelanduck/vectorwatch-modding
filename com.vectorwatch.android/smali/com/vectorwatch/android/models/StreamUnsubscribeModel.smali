.class public Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
.super Ljava/lang/Object;
.source "StreamUnsubscribeModel.java"


# instance fields
.field private channelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

.field private stream:Lcom/vectorwatch/android/models/Stream;

.field private streamUuid:Ljava/lang/String;

.field private subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->channelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    return-object v0
.end method

.method public getStream()Lcom/vectorwatch/android/models/Stream;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->stream:Lcom/vectorwatch/android/models/Stream;

    return-object v0
.end method

.method public getStreamUuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->streamUuid:Ljava/lang/String;

    return-object v0
.end method

.method public getSubscriptionModel()Lcom/vectorwatch/android/models/StreamPlacementModel;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    return-object v0
.end method

.method public setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V
    .locals 0
    .param p1, "channelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->channelSettings:Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 43
    return-void
.end method

.method public setStream(Lcom/vectorwatch/android/models/Stream;)V
    .locals 0
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 39
    return-void
.end method

.method public setStreamUuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "streamUuid"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->streamUuid:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setSubscriptionModel(Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 0
    .param p1, "subscriptionModel"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    .line 35
    return-void
.end method
