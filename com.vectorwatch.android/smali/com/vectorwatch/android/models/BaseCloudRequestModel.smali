.class public Lcom/vectorwatch/android/models/BaseCloudRequestModel;
.super Ljava/lang/Object;
.source "BaseCloudRequestModel.java"


# instance fields
.field public appVersion:Ljava/lang/String;

.field public cpuId:Ljava/lang/String;

.field public deviceCompatibility:Ljava/lang/String;

.field public deviceType:Ljava/lang/String;

.field public endpointArn:Ljava/lang/String;

.field public kernelVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->deviceType:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->appVersion:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->kernelVersion:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->endpointArn:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->deviceCompatibility:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->cpuId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->appVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getCpuId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->cpuId:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceCompatibility()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->deviceCompatibility:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->deviceType:Ljava/lang/String;

    return-object v0
.end method

.method public getEndpointArn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->endpointArn:Ljava/lang/String;

    return-object v0
.end method

.method public getKernelVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->kernelVersion:Ljava/lang/String;

    return-object v0
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "appVersion"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->appVersion:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setCpuId(Ljava/lang/String;)V
    .locals 0
    .param p1, "cpuId"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->cpuId:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setDeviceCompatibility(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceCompatibility"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->deviceCompatibility:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setDeviceType(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->deviceType:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setEndpointArn(Ljava/lang/String;)V
    .locals 0
    .param p1, "endpointArn"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->endpointArn:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setKernelVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "kernelVersion"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->kernelVersion:Ljava/lang/String;

    .line 42
    return-void
.end method
