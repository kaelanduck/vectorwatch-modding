.class public final enum Lcom/vectorwatch/android/models/Stream$StreamTypes;
.super Ljava/lang/Enum;
.source "Stream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/models/Stream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StreamTypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/models/Stream$StreamTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/models/Stream$StreamTypes;

.field public static final enum APP_PRIVATE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

.field public static final enum APP_PUBLIC:Lcom/vectorwatch/android/models/Stream$StreamTypes;

.field public static final enum MOBILE_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

.field public static final enum STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

.field public static final enum WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;


# instance fields
.field private val:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 207
    new-instance v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;

    const-string v1, "APP_PRIVATE"

    const-string v2, "APP_PRIVATE"

    invoke-direct {v0, v1, v3, v2}, Lcom/vectorwatch/android/models/Stream$StreamTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PRIVATE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 208
    new-instance v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;

    const-string v1, "APP_PUBLIC"

    const-string v2, "APP_PUBLIC"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/models/Stream$StreamTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PUBLIC:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 209
    new-instance v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;

    const-string v1, "STANDALONE"

    const-string v2, "STANDALONE"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/models/Stream$StreamTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 210
    new-instance v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;

    const-string v1, "WATCH_SOURCE"

    const-string v2, "WATCH_SOURCE"

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/models/Stream$StreamTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;->WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 211
    new-instance v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;

    const-string v1, "MOBILE_SOURCE"

    const-string v2, "MOBILE_SOURCE"

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/models/Stream$StreamTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;->MOBILE_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 206
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vectorwatch/android/models/Stream$StreamTypes;

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PRIVATE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PUBLIC:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->MOBILE_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    aput-object v1, v0, v7

    sput-object v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;->$VALUES:[Lcom/vectorwatch/android/models/Stream$StreamTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "val"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 215
    iput-object p3, p0, Lcom/vectorwatch/android/models/Stream$StreamTypes;->val:Ljava/lang/String;

    .line 216
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/models/Stream$StreamTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 206
    const-class v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/models/Stream$StreamTypes;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/vectorwatch/android/models/Stream$StreamTypes;->$VALUES:[Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/models/Stream$StreamTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/models/Stream$StreamTypes;

    return-object v0
.end method


# virtual methods
.method public getVal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/vectorwatch/android/models/Stream$StreamTypes;->val:Ljava/lang/String;

    return-object v0
.end method
