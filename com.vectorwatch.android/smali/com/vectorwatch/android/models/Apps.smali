.class public Lcom/vectorwatch/android/models/Apps;
.super Ljava/lang/Object;
.source "Apps.java"


# instance fields
.field private apps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 8
    .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/vectorwatch/android/models/Apps;->apps:Ljava/util/List;

    .line 10
    return-void
.end method


# virtual methods
.method public getApps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vectorwatch/android/models/Apps;->apps:Ljava/util/List;

    return-object v0
.end method

.method public setApps(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/Apps;->apps:Ljava/util/List;

    .line 18
    return-void
.end method
