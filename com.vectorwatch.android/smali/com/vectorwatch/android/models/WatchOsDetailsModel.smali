.class public Lcom/vectorwatch/android/models/WatchOsDetailsModel;
.super Ljava/lang/Object;
.source "WatchOsDetailsModel.java"


# instance fields
.field appVersion:Ljava/lang/String;

.field bootloaderVersion:Ljava/lang/String;

.field btName:Ljava/lang/String;

.field cpuId:Ljava/lang/String;

.field currentSystemType:Ljava/lang/String;

.field deviceType:Ljava/lang/String;

.field kernelVersion:Ljava/lang/String;

.field newCpuId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-string v0, "android"

    iput-object v0, p0, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->deviceType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCurrentSystemType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->currentSystemType:Ljava/lang/String;

    return-object v0
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "appVersion"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->appVersion:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setBootloaderVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "bootloaderVersion"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->bootloaderVersion:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setBtName(Ljava/lang/String;)V
    .locals 0
    .param p1, "btName"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->btName:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public setCpuId(Ljava/lang/String;)V
    .locals 0
    .param p1, "cpuId"    # Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->cpuId:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public setCurrentSystemType(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentSystemType"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->currentSystemType:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setKernelVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "kernelVersion"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->kernelVersion:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setNewCpuId(Ljava/lang/String;)V
    .locals 0
    .param p1, "newCpuId"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->newCpuId:Ljava/lang/String;

    .line 22
    return-void
.end method
