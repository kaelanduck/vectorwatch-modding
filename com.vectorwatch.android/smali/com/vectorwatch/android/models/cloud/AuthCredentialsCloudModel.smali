.class public Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;
.super Lcom/vectorwatch/android/models/BaseCloudRequestModel;
.source "AuthCredentialsCloudModel.java"


# instance fields
.field private authInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthInfo()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;->authInfo:Ljava/util/HashMap;

    return-object v0
.end method

.method public setAuthInfo(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "authInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;->authInfo:Ljava/util/HashMap;

    .line 15
    return-void
.end method
