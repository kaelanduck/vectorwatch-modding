.class public final enum Lcom/vectorwatch/android/models/cloud/AppsOption;
.super Ljava/lang/Enum;
.source "AppsOption.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/models/cloud/AppsOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/models/cloud/AppsOption;

.field public static final enum ALL:Lcom/vectorwatch/android/models/cloud/AppsOption;

.field public static final enum APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

.field public static final enum PLACEHOLDER:Lcom/vectorwatch/android/models/cloud/AppsOption;

.field public static final enum STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

.field public static final enum WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/vectorwatch/android/models/cloud/AppsOption;

    const-string v1, "APP"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/models/cloud/AppsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    new-instance v0, Lcom/vectorwatch/android/models/cloud/AppsOption;

    const-string v1, "WATCHFACE"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/models/cloud/AppsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    new-instance v0, Lcom/vectorwatch/android/models/cloud/AppsOption;

    const-string v1, "STREAM"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/models/cloud/AppsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

    new-instance v0, Lcom/vectorwatch/android/models/cloud/AppsOption;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/models/cloud/AppsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->ALL:Lcom/vectorwatch/android/models/cloud/AppsOption;

    new-instance v0, Lcom/vectorwatch/android/models/cloud/AppsOption;

    const-string v1, "PLACEHOLDER"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/models/cloud/AppsOption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->PLACEHOLDER:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 3
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/vectorwatch/android/models/cloud/AppsOption;

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->APP:Lcom/vectorwatch/android/models/cloud/AppsOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->WATCHFACE:Lcom/vectorwatch/android/models/cloud/AppsOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->STREAM:Lcom/vectorwatch/android/models/cloud/AppsOption;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->ALL:Lcom/vectorwatch/android/models/cloud/AppsOption;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->PLACEHOLDER:Lcom/vectorwatch/android/models/cloud/AppsOption;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->$VALUES:[Lcom/vectorwatch/android/models/cloud/AppsOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/models/cloud/AppsOption;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/cloud/AppsOption;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/models/cloud/AppsOption;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/vectorwatch/android/models/cloud/AppsOption;->$VALUES:[Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/models/cloud/AppsOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/models/cloud/AppsOption;

    return-object v0
.end method
