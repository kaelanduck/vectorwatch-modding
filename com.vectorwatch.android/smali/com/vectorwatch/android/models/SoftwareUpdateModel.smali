.class public Lcom/vectorwatch/android/models/SoftwareUpdateModel;
.super Ljava/lang/Object;
.source "SoftwareUpdateModel.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private data:Lcom/vectorwatch/android/models/UpdateData;

.field private errorDiscovered:Z

.field private message:Lcom/vectorwatch/android/events/CloudMessageModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Lcom/vectorwatch/android/models/UpdateData;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->data:Lcom/vectorwatch/android/models/UpdateData;

    return-object v0
.end method

.method public getErrorMessage()Lcom/vectorwatch/android/events/CloudMessageModel;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->message:Lcom/vectorwatch/android/events/CloudMessageModel;

    return-object v0
.end method

.method public isErrorDiscovered()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->errorDiscovered:Z

    return v0
.end method

.method public setData(Lcom/vectorwatch/android/models/UpdateData;)V
    .locals 0
    .param p1, "data"    # Lcom/vectorwatch/android/models/UpdateData;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->data:Lcom/vectorwatch/android/models/UpdateData;

    .line 33
    return-void
.end method

.method public setErrorDiscovered(Z)V
    .locals 0
    .param p1, "errorDiscovered"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->errorDiscovered:Z

    .line 29
    return-void
.end method

.method public setMessage(Lcom/vectorwatch/android/events/CloudMessageModel;)V
    .locals 0
    .param p1, "message"    # Lcom/vectorwatch/android/events/CloudMessageModel;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->message:Lcom/vectorwatch/android/events/CloudMessageModel;

    .line 37
    return-void
.end method
