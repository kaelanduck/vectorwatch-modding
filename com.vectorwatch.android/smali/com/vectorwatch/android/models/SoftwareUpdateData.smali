.class public Lcom/vectorwatch/android/models/SoftwareUpdateData;
.super Ljava/lang/Object;
.source "SoftwareUpdateData.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private contentBase64:Ljava/lang/String;

.field private id:Ljava/lang/Long;

.field private summary:Ljava/lang/String;

.field private type:Ljava/lang/String;

.field private version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContentBase64()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->contentBase64:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->summary:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->version:Ljava/lang/String;

    return-object v0
.end method

.method public setContentBase64(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentBase64"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->contentBase64:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setId(Ljava/lang/Long;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/Long;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->id:Ljava/lang/Long;

    .line 37
    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 0
    .param p1, "summary"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->summary:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->type:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vectorwatch/android/models/SoftwareUpdateData;->version:Ljava/lang/String;

    .line 49
    return-void
.end method
