.class public Lcom/vectorwatch/android/models/CloudStatus;
.super Ljava/lang/Object;
.source "CloudStatus.java"


# instance fields
.field private timestamp:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-wide p1, p0, Lcom/vectorwatch/android/models/CloudStatus;->timestamp:J

    .line 15
    return-void
.end method


# virtual methods
.method public getTimestamp()J
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/vectorwatch/android/models/CloudStatus;->timestamp:J

    return-wide v0
.end method

.method public setTimestamp(J)V
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 22
    iput-wide p1, p0, Lcom/vectorwatch/android/models/CloudStatus;->timestamp:J

    .line 23
    return-void
.end method
