.class public final Lcom/vectorwatch/android/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f030000

.field public static final abc_action_bar_up_container:I = 0x7f030001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f030002

.field public static final abc_action_menu_item_layout:I = 0x7f030003

.field public static final abc_action_menu_layout:I = 0x7f030004

.field public static final abc_action_mode_bar:I = 0x7f030005

.field public static final abc_action_mode_close_item_material:I = 0x7f030006

.field public static final abc_activity_chooser_view:I = 0x7f030007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f030008

.field public static final abc_alert_dialog_material:I = 0x7f030009

.field public static final abc_dialog_title_material:I = 0x7f03000a

.field public static final abc_expanded_menu_layout:I = 0x7f03000b

.field public static final abc_list_menu_item_checkbox:I = 0x7f03000c

.field public static final abc_list_menu_item_icon:I = 0x7f03000d

.field public static final abc_list_menu_item_layout:I = 0x7f03000e

.field public static final abc_list_menu_item_radio:I = 0x7f03000f

.field public static final abc_popup_menu_item_layout:I = 0x7f030010

.field public static final abc_screen_content_include:I = 0x7f030011

.field public static final abc_screen_simple:I = 0x7f030012

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f030013

.field public static final abc_screen_toolbar:I = 0x7f030014

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f030015

.field public static final abc_search_view:I = 0x7f030016

.field public static final abc_select_dialog_material:I = 0x7f030017

.field public static final abc_simple_dropdown_hint:I = 0x7f030018

.field public static final actionbar_custom:I = 0x7f030019

.field public static final activity_about:I = 0x7f03001a

.field public static final activity_alarms:I = 0x7f03001b

.field public static final activity_app_store:I = 0x7f03001c

.field public static final activity_base:I = 0x7f03001d

.field public static final activity_chart:I = 0x7f03001e

.field public static final activity_cloud_app_locker:I = 0x7f03001f

.field public static final activity_cloud_app_watch:I = 0x7f030020

.field public static final activity_connect_watch:I = 0x7f030021

.field public static final activity_goals:I = 0x7f030022

.field public static final activity_graph:I = 0x7f030023

.field public static final activity_info_row_1:I = 0x7f030024

.field public static final activity_language:I = 0x7f030025

.field public static final activity_login:I = 0x7f030026

.field public static final activity_main:I = 0x7f030027

.field public static final activity_newsletter:I = 0x7f030028

.field public static final activity_notifications_alert:I = 0x7f030029

.field public static final activity_oauth:I = 0x7f03002a

.field public static final activity_onboarding:I = 0x7f03002b

.field public static final activity_onboarding_login:I = 0x7f03002c

.field public static final activity_register:I = 0x7f03002d

.field public static final activity_report_bug_layout:I = 0x7f03002e

.field public static final activity_search_for_watch:I = 0x7f03002f

.field public static final activity_setup_watch:I = 0x7f030030

.field public static final activity_store:I = 0x7f030031

.field public static final activity_store_add_rating:I = 0x7f030032

.field public static final activity_store_item_details:I = 0x7f030033

.field public static final activity_store_item_details_rating:I = 0x7f030034

.field public static final activity_store_report_abuse:I = 0x7f030035

.field public static final activity_store_search:I = 0x7f030036

.field public static final activity_stream_details:I = 0x7f030037

.field public static final activity_timezones:I = 0x7f030038

.field public static final activity_update_watch_layout:I = 0x7f030039

.field public static final activity_user_preferences:I = 0x7f03003a

.field public static final activity_watch:I = 0x7f03003b

.field public static final alarm_list_view_item:I = 0x7f03003c

.field public static final alert_cloud_uri:I = 0x7f03003d

.field public static final alert_language:I = 0x7f03003e

.field public static final alert_message:I = 0x7f03003f

.field public static final alert_notification_layout:I = 0x7f030040

.field public static final charts_activity_layout:I = 0x7f030041

.field public static final check:I = 0x7f030042

.field public static final check_settings:I = 0x7f030043

.field public static final com_facebook_activity_layout:I = 0x7f030044

.field public static final com_facebook_login_fragment:I = 0x7f030045

.field public static final com_facebook_tooltip_bubble:I = 0x7f030046

.field public static final custom_marker_view:I = 0x7f030047

.field public static final cwac_cam2_fragment:I = 0x7f030048

.field public static final design_navigation_item:I = 0x7f030049

.field public static final design_navigation_item_header:I = 0x7f03004a

.field public static final design_navigation_item_separator:I = 0x7f03004b

.field public static final design_navigation_item_subheader:I = 0x7f03004c

.field public static final design_navigation_menu:I = 0x7f03004d

.field public static final device_element:I = 0x7f03004e

.field public static final dialog_backlight:I = 0x7f03004f

.field public static final dialog_imperial_height_layout:I = 0x7f030050

.field public static final edit_alarm_menu:I = 0x7f030051

.field public static final edit_goal_menu:I = 0x7f030052

.field public static final fragment_activity:I = 0x7f030053

.field public static final fragment_activity_refactored:I = 0x7f030054

.field public static final fragment_activity_set:I = 0x7f030055

.field public static final fragment_apps:I = 0x7f030056

.field public static final fragment_fenster_gesture:I = 0x7f030057

.field public static final fragment_fenster_video:I = 0x7f030058

.field public static final fragment_graph_steps:I = 0x7f030059

.field public static final fragment_item:I = 0x7f0300bd

.field public static final fragment_item_grid:I = 0x7f03005a

.field public static final fragment_item_list:I = 0x7f03005b

.field public static final fragment_settings:I = 0x7f03005c

.field public static final fragment_store:I = 0x7f03005d

.field public static final fragment_store_apps:I = 0x7f03005e

.field public static final fragment_store_featured:I = 0x7f03005f

.field public static final fragment_store_main:I = 0x7f030060

.field public static final fragment_store_search:I = 0x7f030061

.field public static final fragment_store_watch_faces:I = 0x7f030062

.field public static final fragment_watch:I = 0x7f030063

.field public static final fragment_watch_maker:I = 0x7f030064

.field public static final fragment_watch_modes:I = 0x7f030065

.field public static final fragment_watchfaces:I = 0x7f030066

.field public static final header_activity_view:I = 0x7f030067

.field public static final info_screen_layout:I = 0x7f030068

.field public static final item_alarm:I = 0x7f030069

.field public static final item_locker_app:I = 0x7f03006a

.field public static final item_photo_title_summary:I = 0x7f03006b

.field public static final item_photo_title_summary_toggle:I = 0x7f03006c

.field public static final item_preference_checkbox:I = 0x7f03006d

.field public static final item_preference_notification_enabler:I = 0x7f03006e

.field public static final item_settings_element:I = 0x7f03006f

.field public static final item_settings_info:I = 0x7f030070

.field public static final item_settings_section:I = 0x7f030071

.field public static final item_settings_simple_element:I = 0x7f030072

.field public static final item_settings_simple_toggle_element:I = 0x7f030073

.field public static final item_settings_toggle_with_summary_element:I = 0x7f030074

.field public static final item_settings_value_element:I = 0x7f030075

.field public static final item_store_app:I = 0x7f030076

.field public static final item_stream:I = 0x7f030077

.field public static final item_stream_placeholder:I = 0x7f030078

.field public static final item_system_notification_enabler:I = 0x7f030079

.field public static final item_timezone:I = 0x7f03007a

.field public static final item_watch_app:I = 0x7f03007b

.field public static final item_watchface:I = 0x7f03007c

.field public static final layout_snackbar:I = 0x7f03007d

.field public static final layout_snackbar_include:I = 0x7f03007e

.field public static final layout_tab_icon:I = 0x7f03007f

.field public static final layout_tab_text:I = 0x7f030080

.field public static final legal_terms_webview:I = 0x7f030081

.field public static final list_item_found_device:I = 0x7f030082

.field public static final list_store_rating:I = 0x7f030083

.field public static final list_view_item:I = 0x7f030084

.field public static final list_view_item_author:I = 0x7f030085

.field public static final list_view_item_search:I = 0x7f030086

.field public static final messenger_button_send_blue_large:I = 0x7f030087

.field public static final messenger_button_send_blue_round:I = 0x7f030088

.field public static final messenger_button_send_blue_small:I = 0x7f030089

.field public static final messenger_button_send_white_large:I = 0x7f03008a

.field public static final messenger_button_send_white_round:I = 0x7f03008b

.field public static final messenger_button_send_white_small:I = 0x7f03008c

.field public static final notification_media_action:I = 0x7f03008d

.field public static final notification_media_cancel_action:I = 0x7f03008e

.field public static final notification_template_big_media:I = 0x7f03008f

.field public static final notification_template_big_media_narrow:I = 0x7f030090

.field public static final notification_template_lines:I = 0x7f030091

.field public static final notification_template_media:I = 0x7f030092

.field public static final notification_template_part_chronometer:I = 0x7f030093

.field public static final notification_template_part_time:I = 0x7f030094

.field public static final place_autocomplete_fragment:I = 0x7f030095

.field public static final place_autocomplete_item_powered_by_google:I = 0x7f030096

.field public static final place_autocomplete_item_prediction:I = 0x7f030097

.field public static final place_autocomplete_progress:I = 0x7f030098

.field public static final progress_spokeview:I = 0x7f030099

.field public static final range_control_layout:I = 0x7f03009a

.field public static final recover_pass_webview:I = 0x7f03009b

.field public static final row_notification_alert:I = 0x7f03009c

.field public static final row_store_places:I = 0x7f03009d

.field public static final row_store_places_2:I = 0x7f03009e

.field public static final row_store_places_top:I = 0x7f03009f

.field public static final select_dialog_item_material:I = 0x7f0300a0

.field public static final select_dialog_multichoice_material:I = 0x7f0300a1

.field public static final select_dialog_singlechoice_material:I = 0x7f0300a2

.field public static final service_inter_l:I = 0x7f0300a3

.field public static final settings:I = 0x7f0300a4

.field public static final settings_account_profile_layout:I = 0x7f0300a5

.field public static final settings_activity_info_layout:I = 0x7f0300a6

.field public static final settings_alarms:I = 0x7f0300a7

.field public static final settings_alerts_layout:I = 0x7f0300a8

.field public static final settings_alerts_user_apps:I = 0x7f0300a9

.field public static final settings_contextual_layout:I = 0x7f0300aa

.field public static final settings_main_screen_layout:I = 0x7f0300ab

.field public static final settings_notification_display_option:I = 0x7f0300ac

.field public static final settings_set_alarms:I = 0x7f0300ad

.field public static final single_control_layout:I = 0x7f0300ae

.field public static final stream_autocomplete_fragment:I = 0x7f0300af

.field public static final stream_grid_fragment_layout:I = 0x7f0300b0

.field public static final stream_grid_item:I = 0x7f0300b1

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0300b2

.field public static final tab_layout:I = 0x7f0300b3

.field public static final toolbar_actionbar:I = 0x7f0300b4

.field public static final toolbar_actionbar_logo:I = 0x7f0300b5

.field public static final update_watch:I = 0x7f0300b6

.field public static final view_loading:I = 0x7f0300b7

.field public static final view_media_controller:I = 0x7f0300b8

.field public static final view_simple_media_controller:I = 0x7f0300b9

.field public static final viewpager_recommended_layout:I = 0x7f0300ba

.field public static final watchface:I = 0x7f0300bb

.field public static final watchface_round:I = 0x7f0300bc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
