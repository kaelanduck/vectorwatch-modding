.class public Lcom/vectorwatch/android/receivers/PackageInstalledReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PackageInstalledReceiver.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/vectorwatch/android/receivers/PackageInstalledReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/receivers/PackageInstalledReceiver;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 25
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "package:"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 26
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "app_not_all"

    const-class v5, Ljava/util/List;

    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 28
    .local v1, "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    .line 29
    sget-object v3, Lcom/vectorwatch/android/receivers/PackageInstalledReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "Notification list null"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 34
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 39
    :cond_2
    const-string v3, "flag_mirror_phone"

    invoke-static {v3, v6, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 40
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 41
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "app_not_all"

    invoke-virtual {v3, v4, v1}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 44
    :cond_3
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V

    goto :goto_0
.end method
