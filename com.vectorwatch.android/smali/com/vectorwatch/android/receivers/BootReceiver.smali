.class public Lcom/vectorwatch/android/receivers/BootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BootReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 19
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 21
    .local v1, "bondedWatchName":Ljava/lang/String;
    const/4 v2, 0x5

    invoke-static {v2, p1}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 23
    if-eqz v1, :cond_0

    .line 25
    invoke-static {p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->connect(Landroid/content/Context;)V

    .line 29
    :cond_0
    new-instance v0, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;

    invoke-direct {v0}, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;-><init>()V

    .line 30
    .local v0, "alarm":Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;
    invoke-static {p1}, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->setAlarm(Landroid/content/Context;)V

    .line 31
    return-void
.end method
