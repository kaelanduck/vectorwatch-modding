.class public Lcom/vectorwatch/android/receivers/MusicBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MusicBroadcastReceiver.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/vectorwatch/android/receivers/MusicBroadcastReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/receivers/MusicBroadcastReceiver;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 26
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 31
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 32
    .local v2, "action":Ljava/lang/String;
    const-string v12, "command"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 33
    .local v5, "cmd":Ljava/lang/String;
    sget-object v12, Lcom/vectorwatch/android/receivers/MusicBroadcastReceiver;->log:Lorg/slf4j/Logger;

    const-string v13, "MUSIC: "

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " / "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v12, v13, v14}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 34
    const-string v12, "artist"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 35
    .local v4, "artist":Ljava/lang/String;
    const-string v12, "album"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 36
    .local v3, "album":Ljava/lang/String;
    const-string v12, "track"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 37
    .local v11, "track":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    .line 38
    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    const-wide/16 v14, 0x3e8

    div-long/2addr v12, v14

    long-to-int v12, v12

    add-int/lit8 v12, v12, 0x32

    .line 37
    invoke-static {v12}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v6

    .line 39
    .local v6, "duration":I
    sget-object v12, Lcom/vectorwatch/android/receivers/MusicBroadcastReceiver;->log:Lorg/slf4j/Logger;

    const-string v13, "MUSIC: "

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v12, v13, v14}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Object;)V

    .line 40
    if-eqz v4, :cond_0

    const-string v12, ""

    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 41
    :cond_0
    const-string v12, "Music App"

    sput-object v12, Lcom/vectorwatch/android/VectorApplication;->sLastArtist:Ljava/lang/String;

    .line 46
    :goto_0
    if-eqz v11, :cond_1

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 47
    :cond_1
    const-string v12, ""

    sput-object v12, Lcom/vectorwatch/android/VectorApplication;->sLastTrack:Ljava/lang/String;

    .line 52
    :goto_1
    const-string v12, "audio"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/media/AudioManager;

    .line 53
    .local v8, "manager":Landroid/media/AudioManager;
    invoke-virtual {v8}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 57
    :cond_2
    if-eqz v4, :cond_3

    if-eqz v11, :cond_3

    .line 58
    sget-object v12, Lcom/vectorwatch/android/receivers/MusicBroadcastReceiver;->log:Lorg/slf4j/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "MUSIC: Trying to set up new data: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 59
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/receivers/scheduler/NoMusicTrigger;->cancelAlarm(Landroid/content/Context;)V

    .line 60
    const-string v12, "3906BFAD46D7E6E1BF757891F26AEFB1"

    move-object/from16 v0, p1

    invoke-static {v12, v4, v6, v0}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 61
    const-string v12, "6375B098967A1B6F1C37C891B02295F2"

    move-object/from16 v0, p1

    invoke-static {v12, v11, v6, v0}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 63
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/managers/StreamsManager;->getUpdates(Landroid/content/Context;)Ljava/util/List;

    move-result-object v10

    .line 64
    .local v10, "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 65
    .local v9, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 68
    .end local v2    # "action":Ljava/lang/String;
    .end local v3    # "album":Ljava/lang/String;
    .end local v4    # "artist":Ljava/lang/String;
    .end local v5    # "cmd":Ljava/lang/String;
    .end local v6    # "duration":I
    .end local v8    # "manager":Landroid/media/AudioManager;
    .end local v9    # "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    .end local v10    # "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    .end local v11    # "track":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 69
    .local v7, "e":Ljava/lang/Exception;
    sget-object v12, Lcom/vectorwatch/android/receivers/MusicBroadcastReceiver;->log:Lorg/slf4j/Logger;

    const-string v13, "Exception caught in Music receiver."

    invoke-interface {v12, v13}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 71
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_3
    return-void

    .line 43
    .restart local v2    # "action":Ljava/lang/String;
    .restart local v3    # "album":Ljava/lang/String;
    .restart local v4    # "artist":Ljava/lang/String;
    .restart local v5    # "cmd":Ljava/lang/String;
    .restart local v6    # "duration":I
    .restart local v11    # "track":Ljava/lang/String;
    :cond_4
    :try_start_1
    sput-object v4, Lcom/vectorwatch/android/VectorApplication;->sLastArtist:Ljava/lang/String;

    goto :goto_0

    .line 49
    :cond_5
    sput-object v11, Lcom/vectorwatch/android/VectorApplication;->sLastTrack:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
