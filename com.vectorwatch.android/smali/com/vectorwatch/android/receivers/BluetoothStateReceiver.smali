.class public Lcom/vectorwatch/android/receivers/BluetoothStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothStateReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/vectorwatch/android/receivers/BluetoothStateReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/receivers/BluetoothStateReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, -0x1

    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 23
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/16 v3, 0xc

    if-ne v2, v3, :cond_1

    .line 26
    sget-object v2, Lcom/vectorwatch/android/receivers/BluetoothStateReceiver;->TAG:Ljava/lang/String;

    const-string v3, "Bluetooth ON"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 29
    .local v1, "bondedWatchName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 32
    invoke-static {p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->connect(Landroid/content/Context;)V

    .line 45
    .end local v1    # "bondedWatchName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_0

    .line 35
    sget-object v2, Lcom/vectorwatch/android/receivers/BluetoothStateReceiver;->TAG:Ljava/lang/String;

    const-string v3, "Bluetooth OFF"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 38
    .restart local v1    # "bondedWatchName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 41
    invoke-static {p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->close(Landroid/content/Context;)V

    goto :goto_0
.end method
