.class public Lcom/vectorwatch/android/receivers/LocaleChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocaleChangeReceiver.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vectorwatch/android/receivers/LocaleChangeReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/receivers/LocaleChangeReceiver;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v1, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 27
    .local v1, "current":Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 29
    .local v2, "currentLanguage":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 30
    sget-object v3, Lcom/vectorwatch/android/receivers/LocaleChangeReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "LOCALE - current language is null."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 66
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->isDefaultLanguageSelected(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 36
    const/4 v3, 0x0

    invoke-static {v2, p1, v3}, Lcom/vectorwatch/android/utils/Helpers;->setLocale(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 39
    :cond_1
    sget-object v3, Lcom/vectorwatch/android/receivers/LocaleChangeReceiver;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOTIFICATIONS DISPLAY: locale = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 41
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->updateOptionsNotificationDisplay(Landroid/content/Context;)V

    .line 43
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/LanguageChangedEvent;

    invoke-direct {v4}, Lcom/vectorwatch/android/events/LanguageChangedEvent;-><init>()V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 45
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 46
    sget-object v3, Lcom/vectorwatch/android/receivers/LocaleChangeReceiver;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LOCALE - new locale = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 48
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->syncNotificationDisplayOption(Landroid/content/Context;)V

    .line 51
    const-string v3, "pref_watch_language_selected"

    const v4, 0x7f1000fb

    invoke-static {v3, v4, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 54
    .local v0, "checkedId":I
    const v3, 0x7f1000fa

    if-eq v0, v3, :cond_2

    .line 55
    sget-object v3, Lcom/vectorwatch/android/receivers/LocaleChangeReceiver;->log:Lorg/slf4j/Logger;

    const-string v4, "Language: We don\'t sync because selected option != default."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_2
    invoke-static {v2, p1}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 61
    .end local v0    # "checkedId":I
    :cond_3
    const-string v3, "flag_sync_locale"

    const/4 v4, 0x1

    invoke-static {v3, v4, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 63
    const-string v3, "pref_locale_watch"

    invoke-static {v3, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method
