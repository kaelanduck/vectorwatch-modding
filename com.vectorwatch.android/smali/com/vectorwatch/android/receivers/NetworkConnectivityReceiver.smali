.class public Lcom/vectorwatch/android/receivers/NetworkConnectivityReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NetworkConnectivityReceiver.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/vectorwatch/android/receivers/NetworkConnectivityReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/receivers/NetworkConnectivityReceiver;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 27
    invoke-static {p1}, Lcom/vectorwatch/android/utils/NetworkUtils;->getConnectivityStatusString(Landroid/content/Context;)I

    move-result v0

    .line 28
    .local v0, "status":I
    sget-object v1, Lcom/vectorwatch/android/receivers/NetworkConnectivityReceiver;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Network connectivity status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 29
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 30
    if-eqz v0, :cond_1

    .line 31
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 32
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;-><init>(Z)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;-><init>(Z)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method
