.class public Lcom/vectorwatch/android/receivers/NotificationDismissReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NotificationDismissReceiver.java"


# static fields
.field public static final EXTRA_KEY_NOTIFICATION_TYPE:Ljava/lang/String; = "notification_type"

.field public static final NOTIFICATION_TYPE_AUTH_EXPIRED_DATA_REQUEST:Ljava/lang/String; = "notification_type_auth_expired_data_request"

.field public static final NOTIFICATION_TYPE_GPS_DISABLED_DATA_REQUEST:Ljava/lang/String; = "notification_type_gps_disabled_data_request"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 25
    const-string v1, "notification_type"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "notification_type":Ljava/lang/String;
    const-string v1, "notification_type_auth_expired_data_request"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27
    sput-boolean v2, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_AUTH_EXPIRED_NOTIFICATION:Z

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    const-string v1, "notification_type_gps_disabled_data_request"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    sput-boolean v2, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_LOCATION_NOTIFICATION:Z

    goto :goto_0
.end method
