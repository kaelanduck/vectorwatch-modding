.class public Lcom/vectorwatch/android/receivers/CallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CallReceiver.java"


# static fields
.field private static final CALL_NOTIFICATION_CONTAINER_ID:Ljava/lang/String; = "phone_call_container_it"

.field private static callStartTime:Ljava/util/Date;

.field private static isIncoming:Z

.field private static lastState:I

.field private static final log:Lorg/slf4j/Logger;

.field private static savedNumber:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/vectorwatch/android/receivers/CallReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    .line 25
    const/4 v0, 0x0

    sput v0, Lcom/vectorwatch/android/receivers/CallReceiver;->lastState:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "state"    # I
    .param p3, "number"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 113
    sget v0, Lcom/vectorwatch/android/receivers/CallReceiver;->lastState:I

    if-ne v0, p2, :cond_0

    .line 154
    :goto_0
    return-void

    .line 117
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 153
    :goto_1
    sput p2, Lcom/vectorwatch/android/receivers/CallReceiver;->lastState:I

    goto :goto_0

    .line 119
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "CALL RECEIVER: Call state ringing"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 120
    sput-boolean v2, Lcom/vectorwatch/android/receivers/CallReceiver;->isIncoming:Z

    .line 121
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->callStartTime:Ljava/util/Date;

    .line 122
    sput-object p3, Lcom/vectorwatch/android/receivers/CallReceiver;->savedNumber:Ljava/lang/String;

    .line 123
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->callStartTime:Ljava/util/Date;

    invoke-virtual {p0, p1, p3, v0}, Lcom/vectorwatch/android/receivers/CallReceiver;->onIncomingCallReceived(Landroid/content/Context;Ljava/lang/String;Ljava/util/Date;)V

    goto :goto_1

    .line 127
    :pswitch_1
    sget v0, Lcom/vectorwatch/android/receivers/CallReceiver;->lastState:I

    if-eq v0, v2, :cond_1

    .line 128
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vectorwatch/android/receivers/CallReceiver;->isIncoming:Z

    .line 129
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->callStartTime:Ljava/util/Date;

    .line 130
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "CALL RECEIVER: onOutgoingCallStarted"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 132
    :cond_1
    sput-boolean v2, Lcom/vectorwatch/android/receivers/CallReceiver;->isIncoming:Z

    .line 133
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->callStartTime:Ljava/util/Date;

    .line 134
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "CALL RECEIVER: Call state offhook: isIncoming == true"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 135
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->savedNumber:Ljava/lang/String;

    sget-object v1, Lcom/vectorwatch/android/receivers/CallReceiver;->callStartTime:Ljava/util/Date;

    invoke-virtual {p0, p1, v0, v1}, Lcom/vectorwatch/android/receivers/CallReceiver;->onIncomingCallAnswered(Landroid/content/Context;Ljava/lang/String;Ljava/util/Date;)V

    goto :goto_1

    .line 141
    :pswitch_2
    sget v0, Lcom/vectorwatch/android/receivers/CallReceiver;->lastState:I

    if-ne v0, v2, :cond_2

    .line 142
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "CALL RECEIVER: Call state idle - miss"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 144
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->savedNumber:Ljava/lang/String;

    sget-object v1, Lcom/vectorwatch/android/receivers/CallReceiver;->callStartTime:Ljava/util/Date;

    invoke-virtual {p0, p1, v0, v1}, Lcom/vectorwatch/android/receivers/CallReceiver;->onMissedCall(Landroid/content/Context;Ljava/lang/String;Ljava/util/Date;)V

    goto :goto_1

    .line 145
    :cond_2
    sget-boolean v0, Lcom/vectorwatch/android/receivers/CallReceiver;->isIncoming:Z

    if-eqz v0, :cond_3

    .line 146
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "CALL RECEIVER: Call state idle - incoming call ended"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 147
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->savedNumber:Ljava/lang/String;

    sget-object v1, Lcom/vectorwatch/android/receivers/CallReceiver;->callStartTime:Ljava/util/Date;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/vectorwatch/android/receivers/CallReceiver;->onIncomingCallEnded(Landroid/content/Context;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V

    goto :goto_1

    .line 149
    :cond_3
    sget-object v0, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "CALL RECEIVER: onOutgoingCallEnded"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onIncomingCallAnswered(Landroid/content/Context;Ljava/lang/String;Ljava/util/Date;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "start"    # Ljava/util/Date;

    .prologue
    .line 70
    sget-object v1, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: CALL RECEIVER: call answered"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    const-string v1, "phone_call_container_it"

    const/4 v2, 0x1

    const-string v4, ""

    const-string v5, "phone_call"

    const/4 v6, -0x1

    const-string v7, ""

    const-string v8, ""

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;-><init>(Ljava/lang/String;SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 74
    .local v0, "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    const/4 v1, 0x0

    sput-boolean v1, Lcom/vectorwatch/android/VectorApplication;->sIsIncomingCallActive:Z

    .line 76
    new-instance v9, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v9, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    .local v9, "intentService":Landroid/content/Intent;
    const-string v1, "service_send"

    const-string v2, "service_notification"

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    const-string v1, "notification_payload"

    invoke-virtual {v9, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 79
    invoke-virtual {p1, v9}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 80
    return-void
.end method

.method protected onIncomingCallEnded(Landroid/content/Context;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "start"    # Ljava/util/Date;
    .param p4, "end"    # Ljava/util/Date;

    .prologue
    .line 83
    sget-object v1, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: CALL RECEIVER: call ended"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    const-string v1, "phone_call_container_it"

    const/4 v2, 0x1

    const-string v4, ""

    const-string v5, "phone_call"

    const/4 v6, -0x1

    const-string v7, ""

    const-string v8, ""

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;-><init>(Ljava/lang/String;SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 87
    .local v0, "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    const/4 v1, 0x0

    sput-boolean v1, Lcom/vectorwatch/android/VectorApplication;->sIsIncomingCallActive:Z

    .line 89
    new-instance v9, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v9, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    .local v9, "intentService":Landroid/content/Intent;
    const-string v1, "service_send"

    const-string v2, "service_notification"

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    const-string v1, "notification_payload"

    invoke-virtual {v9, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 92
    invoke-virtual {p1, v9}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 93
    return-void
.end method

.method protected onIncomingCallReceived(Landroid/content/Context;Ljava/lang/String;Ljava/util/Date;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "start"    # Ljava/util/Date;

    .prologue
    .line 56
    sget-object v1, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: CALL RECEIVER: started ringing"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 58
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    const-string v1, "phone_call_container_it"

    const/4 v2, 0x0

    const-string v4, ""

    const-string v5, "phone_call"

    const/4 v6, -0x1

    const-string v7, ""

    const-string v8, ""

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;-><init>(Ljava/lang/String;SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 61
    .local v0, "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    const/4 v1, 0x1

    sput-boolean v1, Lcom/vectorwatch/android/VectorApplication;->sIsIncomingCallActive:Z

    .line 63
    new-instance v9, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v9, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    .local v9, "intentService":Landroid/content/Intent;
    const-string v1, "service_send"

    const-string v2, "service_notification"

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string v1, "notification_payload"

    invoke-virtual {v9, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 66
    invoke-virtual {p1, v9}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 67
    return-void
.end method

.method protected onMissedCall(Landroid/content/Context;Ljava/lang/String;Ljava/util/Date;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "start"    # Ljava/util/Date;

    .prologue
    .line 96
    sget-object v1, Lcom/vectorwatch/android/receivers/CallReceiver;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS: CALL RECEIVER: call ended missed"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 98
    new-instance v0, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    const-string v1, "phone_call_container_it"

    const/4 v2, 0x1

    const-string v4, ""

    const-string v5, "phone_call"

    const/4 v6, -0x1

    const-string v7, ""

    const-string v8, ""

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;-><init>(Ljava/lang/String;SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 100
    .local v0, "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    const/4 v1, 0x0

    sput-boolean v1, Lcom/vectorwatch/android/VectorApplication;->sIsIncomingCallActive:Z

    .line 102
    new-instance v9, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/service/ble/BleService;

    invoke-direct {v9, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 103
    .local v9, "intentService":Landroid/content/Intent;
    const-string v1, "service_send"

    const-string v2, "service_notification"

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string v1, "notification_payload"

    invoke-virtual {v9, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 105
    invoke-virtual {p1, v9}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 106
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 35
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.NEW_OUTGOING_CALL"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 36
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "android.intent.extra.PHONE_NUMBER"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/vectorwatch/android/receivers/CallReceiver;->savedNumber:Ljava/lang/String;

    .line 52
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "state"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "stateStr":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "incoming_number"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "number":Ljava/lang/String;
    const/4 v1, 0x0

    .line 41
    .local v1, "state":I
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 42
    const/4 v1, 0x0

    .line 50
    :cond_1
    :goto_1
    invoke-virtual {p0, p1, v1, v0}, Lcom/vectorwatch/android/receivers/CallReceiver;->onCallStateChanged(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_2
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 44
    const/4 v1, 0x2

    goto :goto_1

    .line 45
    :cond_3
    sget-object v3, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 46
    const/4 v1, 0x1

    goto :goto_1
.end method
