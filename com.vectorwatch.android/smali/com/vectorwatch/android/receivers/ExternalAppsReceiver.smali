.class public Lcom/vectorwatch/android/receivers/ExternalAppsReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ExternalAppsReceiver.java"


# static fields
.field private static final BRIDGE_BLACKBERRY_NOTIF_APP_NAME:Ljava/lang/String; = "APP_TITLE"

.field private static final BRIDGE_BLACKBERRY_NOTIF_TEXT:Ljava/lang/String; = "NOTIFICATION_TEXT"

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/vectorwatch/android/receivers/ExternalAppsReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/receivers/ExternalAppsReceiver;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getRandomId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 12
    .param p1, "type"    # S
    .param p2, "containerId"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "packageName"    # Ljava/lang/String;
    .param p6, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    new-instance v10, Landroid/content/Intent;

    const-class v2, Lcom/vectorwatch/android/service/ble/BleService;

    move-object/from16 v0, p6

    invoke-direct {v10, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    .local v10, "intentService":Landroid/content/Intent;
    const-string v2, "service_send"

    const-string v3, "service_notification"

    invoke-virtual {v10, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    new-instance v11, Ljava/util/Random;

    invoke-direct {v11}, Ljava/util/Random;-><init>()V

    .line 60
    .local v11, "randomGenerator":Ljava/util/Random;
    new-instance v1, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;

    const v2, 0xf4240

    .line 61
    invoke-virtual {v11, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    const/4 v8, 0x0

    const-string v9, ""

    move-object v2, p2

    move v3, p1

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v9}, Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;-><init>(Ljava/lang/String;SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 63
    .local v1, "notification":Lcom/vectorwatch/android/service/ble/messages/NotificationInfoMessage;
    const-string v2, "notification_payload"

    invoke-virtual {v10, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 64
    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 65
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 28
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 30
    .local v8, "extras":Landroid/os/Bundle;
    sget-object v0, Lcom/vectorwatch/android/receivers/ExternalAppsReceiver;->log:Lorg/slf4j/Logger;

    const-string v1, "EXTERNAL APPS - received intent"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 32
    if-nez v8, :cond_1

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    const-string v0, "APP_TITLE"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 38
    .local v7, "appName":Ljava/lang/String;
    const-string v0, "NOTIFICATION_TEXT"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 40
    .local v4, "notificationText":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 42
    const/4 v1, 0x4

    invoke-direct {p0}, Lcom/vectorwatch/android/receivers/ExternalAppsReceiver;->getRandomId()Ljava/lang/String;

    move-result-object v2

    if-nez v7, :cond_2

    const-string v3, "Unknown"

    :goto_1
    if-nez v7, :cond_3

    const-string v5, "Unknown"

    :goto_2
    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/receivers/ExternalAppsReceiver;->sendToVBLService(SLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    move-object v3, v7

    goto :goto_1

    :cond_3
    move-object v5, v7

    goto :goto_2
.end method
