.class public Lcom/vectorwatch/android/receivers/CalendarEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CalendarEventReceiver.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vectorwatch/android/receivers/CalendarEventReceiver;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/receivers/CalendarEventReceiver;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 25
    sget-object v1, Lcom/vectorwatch/android/receivers/CalendarEventReceiver;->log:Lorg/slf4j/Logger;

    const-string v2, "CALENDAR: Changed detected, check for sync"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 27
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/vectorwatch/android/managers/EventManager;->checkCalendarEventsForSync(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    invoke-static {p1}, Lcom/vectorwatch/android/managers/EventManager;->getCalendarEvents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 29
    .local v0, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    invoke-static {p1, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCalendarEvents(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 33
    .end local v0    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    :goto_0
    return-void

    .line 31
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/receivers/CalendarEventReceiver;->log:Lorg/slf4j/Logger;

    const-string v2, "CALENDAR: sync not possible."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method
