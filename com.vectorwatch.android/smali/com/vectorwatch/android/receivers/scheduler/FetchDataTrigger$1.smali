.class Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger$1;
.super Ljava/lang/Object;
.source "FetchDataTrigger.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/ServerResponseModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;

.field final synthetic val$streamLogItemList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger$1;->this$0:Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;

    iput-object p2, p0, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger$1;->val$streamLogItemList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 3
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 103
    # getter for: Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to sync expired streams to cloud: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v2

    invoke-virtual {v2}, Lretrofit/RetrofitError$Kind;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V
    .locals 3
    .param p1, "serverResponse"    # Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 98
    # getter for: Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Synced expired streams to cloud: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger$1;->val$streamLogItemList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 95
    check-cast p1, Lcom/vectorwatch/android/models/ServerResponseModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger$1;->success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V

    return-void
.end method
