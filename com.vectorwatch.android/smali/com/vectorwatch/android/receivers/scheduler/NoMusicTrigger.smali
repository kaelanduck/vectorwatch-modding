.class public Lcom/vectorwatch/android/receivers/scheduler/NoMusicTrigger;
.super Landroid/content/BroadcastReceiver;
.source "NoMusicTrigger.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static cancelAlarm(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/vectorwatch/android/receivers/scheduler/NoMusicTrigger;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    .local v1, "intent":Landroid/content/Intent;
    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {p0, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 60
    .local v2, "sender":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 61
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 62
    return-void
.end method

.method public static setAlarm(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 50
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 51
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    const-class v2, Lcom/vectorwatch/android/receivers/scheduler/NoMusicTrigger;

    invoke-direct {v7, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    .local v7, "i":Landroid/content/Intent;
    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v7, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 53
    .local v6, "pi":Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0xfa0

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 55
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, -0x1

    .line 28
    const-string v6, "power"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 29
    .local v2, "pm":Landroid/os/PowerManager;
    const/4 v6, 0x1

    const-string v7, ""

    invoke-virtual {v2, v6, v7}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    .line 30
    .local v5, "wl":Landroid/os/PowerManager$WakeLock;
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 32
    const-string v6, "3906BFAD46D7E6E1BF757891F26AEFB1"

    const v7, 0x7f0901bd

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v8, p1}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 34
    const-string v6, "6375B098967A1B6F1C37C891B02295F2"

    const v7, 0x7f0901c9

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v8, p1}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 37
    const/4 v6, 0x4

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 38
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 39
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 41
    .local v1, "extraDuration":[B
    invoke-static {p1}, Lcom/vectorwatch/android/managers/StreamsManager;->getUpdates(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 42
    .local v4, "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 43
    .local v3, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    invoke-static {p1, v3}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;

    goto :goto_0

    .line 45
    .end local v3    # "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    :cond_0
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 46
    return-void
.end method
