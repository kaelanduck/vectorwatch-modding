.class public Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;
.super Landroid/content/BroadcastReceiver;
.source "FetchDataTrigger.java"


# static fields
.field public static final TIME_BEFORE_NEXT_ALARM:J = 0xa4cb80L

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method public static cancelAlarm(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 117
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 118
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 119
    .local v2, "sender":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 120
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 121
    return-void
.end method

.method public static setAlarm(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 110
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 111
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    const-class v2, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;

    invoke-direct {v7, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 112
    .local v7, "i":Landroid/content/Intent;
    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v7, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 113
    .local v6, "pi":Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb80

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 114
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 46
    const-string v9, "power"

    invoke-virtual {p1, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PowerManager;

    .line 47
    .local v4, "pm":Landroid/os/PowerManager;
    const-string v9, ""

    invoke-virtual {v4, v10, v9}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v8

    .line 48
    .local v8, "wl":Landroid/os/PowerManager$WakeLock;
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 50
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentUpdateState(Landroid/content/Context;)I

    move-result v3

    .line 52
    .local v3, "ongoingOtaState":I
    const/4 v9, 0x5

    if-ne v3, v9, :cond_0

    const-string v9, "check_compatibility"

    .line 53
    invoke-static {v9, v11, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "flag_sync_system_apps"

    .line 54
    invoke-static {v9, v10, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "flag_sync_defaults_to_cloud"

    .line 55
    invoke-static {v9, v11, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 56
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->getActivitySyncLastTimestamp(Landroid/content/Context;)I

    move-result v6

    .line 57
    .local v6, "startTime":I
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentTime()I

    move-result v1

    .line 58
    .local v1, "endTime":I
    invoke-static {p1, v6, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendActivityRequest(Landroid/content/Context;II)Ljava/util/UUID;

    .line 60
    .end local v1    # "endTime":I
    .end local v6    # "startTime":I
    :cond_0
    invoke-virtual {v8}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 62
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 63
    .local v2, "mAccountManager":Landroid/accounts/AccountManager;
    const-string v9, "com.vectorwatch.android"

    invoke-virtual {v2, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 65
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v0, :cond_1

    array-length v9, v0

    if-ge v9, v10, :cond_2

    .line 107
    :cond_1
    :goto_0
    return-void

    .line 72
    :cond_2
    invoke-static {p1}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->syncEndpointArnToCloud(Landroid/content/Context;)V

    .line 75
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 76
    .local v5, "settingsBundle":Landroid/os/Bundle;
    const-string v9, "force"

    invoke-virtual {v5, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 78
    const-string v9, "expedited"

    invoke-virtual {v5, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 84
    aget-object v9, v0, v11

    const-string v10, "com.vectorwatch.android.database.contentprovider"

    invoke-static {v9, v10, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 87
    const-string v9, "pref_last_triggered_alarm_sync"

    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 87
    invoke-static {v9, v10, v11, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 90
    sget-object v9, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->log:Lorg/slf4j/Logger;

    const-string v10, "Sync expired streams to cloud"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 91
    sget-object v9, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 92
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v9

    .line 91
    invoke-static {p1, v9}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getExpiredStreamsByType(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 94
    .local v7, "streamLogItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamLogItem;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_1

    .line 95
    new-instance v9, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger$1;

    invoke-direct {v9, p0, v7}, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger$1;-><init>(Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;Ljava/util/List;)V

    invoke-static {p1, v7, v9}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->syncStreamTtlExpiredToCloud(Landroid/content/Context;Ljava/util/List;Lretrofit/Callback;)V

    goto :goto_0
.end method
