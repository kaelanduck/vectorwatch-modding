.class public Lcom/vectorwatch/android/VectorInstanceIDListenerService;
.super Lcom/google/firebase/iid/FirebaseInstanceIdService;
.source "VectorInstanceIDListenerService.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/vectorwatch/android/VectorInstanceIDListenerService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/VectorInstanceIDListenerService;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/firebase/iid/FirebaseInstanceIdService;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenRefresh()V
    .locals 2

    .prologue
    .line 17
    invoke-super {p0}, Lcom/google/firebase/iid/FirebaseInstanceIdService;->onTokenRefresh()V

    .line 19
    sget-object v0, Lcom/vectorwatch/android/VectorInstanceIDListenerService;->log:Lorg/slf4j/Logger;

    const-string v1, "VectorInstanceIDListenerService: onTokenRefresh"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 20
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getRemotePushNotificationsInitializer()Lcom/vectorwatch/android/RemotePushNotificationsInitializer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vectorwatch/android/VectorInstanceIDListenerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->registerToRemotePushNotificationSystem(Landroid/content/Context;)V

    .line 21
    return-void
.end method
