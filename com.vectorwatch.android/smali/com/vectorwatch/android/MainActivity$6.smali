.class Lcom/vectorwatch/android/MainActivity$6;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/MainActivity;->buildFitnessClient()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/MainActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/MainActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/MainActivity;

    .prologue
    .line 765
    iput-object p1, p0, Lcom/vectorwatch/android/MainActivity$6;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 768
    # getter for: Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/MainActivity;->access$300()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "FITNESS_API: Connected!!!"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 771
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$6;->this$0:Lcom/vectorwatch/android/MainActivity;

    const-class v2, Lcom/vectorwatch/android/service/FitnessClientService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 772
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$6;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/MainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 773
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 779
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 780
    # getter for: Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/MainActivity;->access$300()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "FITNESS_API: Connection lost.  Cause: Network Lost."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 784
    :cond_0
    :goto_0
    return-void

    .line 781
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 782
    # getter for: Lcom/vectorwatch/android/MainActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/MainActivity;->access$300()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "FITNESS_API: Connection lost.  Reason: Service Disconnected"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    goto :goto_0
.end method
