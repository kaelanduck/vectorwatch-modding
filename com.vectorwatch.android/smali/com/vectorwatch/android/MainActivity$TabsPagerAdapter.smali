.class public Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;
.super Landroid/support/v13/app/FragmentPagerAdapter;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TabsPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/MainActivity;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/MainActivity;Landroid/app/FragmentManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/MainActivity;
    .param p2, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 926
    iput-object p1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    .line 927
    invoke-direct {p0, p2}, Landroid/support/v13/app/FragmentPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    .line 928
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 967
    const/4 v0, 0x4

    return v0
.end method

.method public getItem(I)Landroid/app/Fragment;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 932
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/android/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 934
    .local v0, "fm":Landroid/app/FragmentManager;
    packed-switch p1, :pswitch_data_0

    .line 962
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 936
    :pswitch_0
    iget-object v2, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    const-string v1, "WATCHMAKER"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    iput-object v1, v2, Lcom/vectorwatch/android/MainActivity;->watchMakerFragment:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 937
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/MainActivity;->watchMakerFragment:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    if-nez v1, :cond_0

    .line 938
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-static {}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->newInstance()Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    move-result-object v2

    iput-object v2, v1, Lcom/vectorwatch/android/MainActivity;->watchMakerFragment:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .line 940
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/MainActivity;->watchMakerFragment:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    goto :goto_0

    .line 942
    :pswitch_1
    iget-object v2, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    const-string v1, "MESSAGE_TYPE_ACTIVITY"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    # setter for: Lcom/vectorwatch/android/MainActivity;->activityFragment:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
    invoke-static {v2, v1}, Lcom/vectorwatch/android/MainActivity;->access$402(Lcom/vectorwatch/android/MainActivity;Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    .line 943
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->activityFragment:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$400(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    move-result-object v1

    if-nez v1, :cond_1

    .line 944
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;->newInstance()Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    move-result-object v2

    # setter for: Lcom/vectorwatch/android/MainActivity;->activityFragment:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
    invoke-static {v1, v2}, Lcom/vectorwatch/android/MainActivity;->access$402(Lcom/vectorwatch/android/MainActivity;Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    .line 946
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->activityFragment:Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$400(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/activity/view/ActivityFragment;

    move-result-object v1

    goto :goto_0

    .line 948
    :pswitch_2
    iget-object v2, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    const-string v1, "STORE"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    # setter for: Lcom/vectorwatch/android/MainActivity;->alarmsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;
    invoke-static {v2, v1}, Lcom/vectorwatch/android/MainActivity;->access$502(Lcom/vectorwatch/android/MainActivity;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    .line 949
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->alarmsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$500(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    move-result-object v1

    if-nez v1, :cond_2

    .line 950
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;->newInstance()Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    move-result-object v2

    # setter for: Lcom/vectorwatch/android/MainActivity;->alarmsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;
    invoke-static {v1, v2}, Lcom/vectorwatch/android/MainActivity;->access$502(Lcom/vectorwatch/android/MainActivity;Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    .line 953
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->alarmsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$500(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/store/view/StoreMainFragment;

    move-result-object v1

    goto :goto_0

    .line 955
    :pswitch_3
    iget-object v2, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    const-string v1, "SETTINGS"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    # setter for: Lcom/vectorwatch/android/MainActivity;->settingsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;
    invoke-static {v2, v1}, Lcom/vectorwatch/android/MainActivity;->access$602(Lcom/vectorwatch/android/MainActivity;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    .line 956
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->settingsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$600(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    move-result-object v1

    if-nez v1, :cond_3

    .line 957
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-static {}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->newInstance()Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    move-result-object v2

    # setter for: Lcom/vectorwatch/android/MainActivity;->settingsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;
    invoke-static {v1, v2}, Lcom/vectorwatch/android/MainActivity;->access$602(Lcom/vectorwatch/android/MainActivity;Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;)Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    .line 959
    :cond_3
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$TabsPagerAdapter;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->settingsFragment:Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$600(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    move-result-object v1

    goto/16 :goto_0

    .line 934
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
