.class public Lcom/vectorwatch/android/scheduler/BatteryAlarm;
.super Landroid/content/BroadcastReceiver;
.source "BatteryAlarm.java"


# static fields
.field private static final UNDEFINED:I = -0x1

.field private static final UPDATE_INTERVAL:I = 0xea60

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/vectorwatch/android/scheduler/BatteryAlarm;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/scheduler/BatteryAlarm;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 28
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receivedIntent"    # Landroid/content/Intent;

    .prologue
    .line 38
    const/16 v24, 0x0

    new-instance v25, Landroid/content/IntentFilter;

    const-string v26, "android.intent.action.BATTERY_CHANGED"

    invoke-direct/range {v25 .. v26}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v11

    .line 40
    .local v11, "intent":Landroid/content/Intent;
    if-nez v11, :cond_1

    .line 41
    sget-object v24, Lcom/vectorwatch/android/scheduler/BatteryAlarm;->log:Lorg/slf4j/Logger;

    const-string v25, "[BATTERY ALARM]: Battery level sticky intent is null"

    invoke-interface/range {v24 .. v25}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    const-string v24, "level"

    const/16 v25, -0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 46
    .local v15, "level":I
    const-string v24, "scale"

    const/16 v25, -0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v20

    .line 47
    .local v20, "scale":I
    const-string v24, "voltage"

    const/16 v25, -0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v23

    .line 50
    .local v23, "voltage":I
    int-to-float v0, v15

    move/from16 v24, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v25, v0

    div-float v24, v24, v25

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4059000000000000L    # 100.0

    mul-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v4, v0

    .line 52
    .local v4, "batteryLevel":F
    sget-object v24, Lcom/vectorwatch/android/scheduler/BatteryAlarm;->log:Lorg/slf4j/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "[BATTERY ALARM] : Level = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " voltage = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-interface/range {v24 .. v25}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 54
    const-string v24, "plugged"

    const/16 v25, -0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 55
    .local v6, "chargePlug":I
    const/16 v24, 0x2

    move/from16 v0, v24

    if-ne v6, v0, :cond_5

    const/4 v14, 0x1

    .line 56
    .local v14, "isUsbCharge":Z
    :goto_1
    const/16 v24, 0x1

    move/from16 v0, v24

    if-ne v6, v0, :cond_6

    const/4 v12, 0x1

    .line 57
    .local v12, "isAcCharge":Z
    :goto_2
    const-string v24, "manuallyFired"

    const/16 v25, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    .line 58
    .local v13, "isManuallyFired":Z
    sget-object v24, Lcom/vectorwatch/android/scheduler/BatteryAlarm;->log:Lorg/slf4j/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "[BATTERY ALARM] :ACTION_BATTERY_CHANGE: level = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " isUsbCharge = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " isAcCharge = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " isManuallyFired = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-interface/range {v24 .. v25}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 61
    if-nez v13, :cond_4

    .line 63
    if-nez v14, :cond_2

    if-eqz v12, :cond_4

    .line 64
    :cond_2
    const-string v24, "update_phone_battery"

    const-wide/16 v26, -0x1

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    move-object/from16 v3, p1

    invoke-static {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getLongPreference(Ljava/lang/String;JLandroid/content/Context;)J

    move-result-wide v16

    .line 66
    .local v16, "lastUpdateTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 67
    .local v8, "crtTime":J
    sget-object v24, Lcom/vectorwatch/android/scheduler/BatteryAlarm;->log:Lorg/slf4j/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "[BATTERY ALARM]: Last update "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " current time "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-interface/range {v24 .. v25}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 70
    const-wide/16 v24, -0x1

    cmp-long v24, v16, v24

    if-nez v24, :cond_7

    .line 71
    const-string v24, "update_phone_battery"

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v0, v8, v9, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 77
    :cond_3
    const-string v24, "update_phone_battery"

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v0, v8, v9, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 84
    .end local v8    # "crtTime":J
    .end local v16    # "lastUpdateTime":J
    :cond_4
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v25, "\ue03f "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    float-to-double v0, v4

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->floor(D)D

    move-result-wide v26

    move-wide/from16 v0, v26

    double-to-int v0, v0

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " %"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 85
    .local v10, "iconAndValue":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    .line 86
    .local v5, "byteArray":[B
    const-string v21, ""

    .line 88
    .local v21, "stringValue":Ljava/lang/String;
    :try_start_0
    new-instance v22, Ljava/lang/String;

    const-string v24, "UTF-8"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v5, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v21    # "stringValue":Ljava/lang/String;
    .local v22, "stringValue":Ljava/lang/String;
    move-object/from16 v21, v22

    .line 92
    .end local v22    # "stringValue":Ljava/lang/String;
    .restart local v21    # "stringValue":Ljava/lang/String;
    :goto_3
    sget-object v24, Lcom/vectorwatch/android/scheduler/BatteryAlarm;->log:Lorg/slf4j/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "[BATTERY ALARM] : Sending value "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-interface/range {v24 .. v25}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 93
    const-string v24, "21F1FC164BE69DA33A39BDE17402A120"

    const/16 v25, -0x1

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    move/from16 v2, v25

    move-object/from16 v3, p1

    invoke-static {v0, v1, v2, v3}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    .line 96
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/managers/StreamsManager;->getUpdates(Landroid/content/Context;)Ljava/util/List;

    move-result-object v19

    .line 97
    .local v19, "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_4
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_0

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 98
    .local v18, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;

    goto :goto_4

    .line 55
    .end local v5    # "byteArray":[B
    .end local v10    # "iconAndValue":Ljava/lang/String;
    .end local v12    # "isAcCharge":Z
    .end local v13    # "isManuallyFired":Z
    .end local v14    # "isUsbCharge":Z
    .end local v18    # "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    .end local v19    # "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    .end local v21    # "stringValue":Ljava/lang/String;
    :cond_5
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 56
    .restart local v14    # "isUsbCharge":Z
    :cond_6
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 73
    .restart local v8    # "crtTime":J
    .restart local v12    # "isAcCharge":Z
    .restart local v13    # "isManuallyFired":Z
    .restart local v16    # "lastUpdateTime":J
    :cond_7
    sub-long v24, v8, v16

    const-wide/32 v26, 0xea60

    cmp-long v24, v24, v26

    if-gez v24, :cond_3

    goto/16 :goto_0

    .line 89
    .end local v8    # "crtTime":J
    .end local v16    # "lastUpdateTime":J
    .restart local v5    # "byteArray":[B
    .restart local v10    # "iconAndValue":Ljava/lang/String;
    .restart local v21    # "stringValue":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 90
    .local v7, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v7}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_3
.end method
