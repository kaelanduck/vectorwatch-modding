.class public Lcom/vectorwatch/android/utils/SharedPreferencesHelper;
.super Ljava/lang/Object;
.source "SharedPreferencesHelper.java"


# static fields
.field public static final API_URI_STAGE:Ljava/lang/String; = "pref_api_uri_stage"

.field public static final COUNTER_SERVICE_KILLED:Ljava/lang/String; = "counter_service_killed"

.field public static final COUNTER_WATCH_CONNECTION_NUMBER_PREVIOUS:Ljava/lang/String; = "counter_watch_connection_number_prev"

.field public static final COUNTER_WATCH_CONNECTION_NUMBER_RECEIVED:Ljava/lang/String; = "counter_watch_connection_number_new"

.field public static final COUNTER_WATCH_CRASH_LOGS:Ljava/lang/String; = "crash_logs"

.field public static final CURRENT_INSTALL_PART:Ljava/lang/String; = "crt_install_part"

.field public static final DEFAULT_GOAL_ACHIEVEMENT:Z = true

.field public static final DEFAULT_GOAL_ALMOST:Z = true

.field public static final DEFAULT_GOAL_NEWSLETTER:Z = true

.field public static final DEFAULT_GOAL_REMINDER:Z = true

.field public static final DEFAULT_LINK_LOST_ICON:Z = true

.field public static final DEFAULT_LINK_LOST_NOTIFICATION:Z = false

.field public static final DEFAULT_LOW_BATTERY_ICON:Z = true

.field public static final DEFAULT_LOW_BATTERY_NOTIFICATION:Z = false

.field public static final DEFAULT_NEWS_APP:Z = true

.field public static final DEFAULT_NEWS_NEWS:Z = true

.field public static final DEFAULT_PERS_MESSAGE:Z = true

.field public static final FLAG_ACTION_FORGET_REQUESTED:Ljava/lang/String; = "flag_action_forget_requested"

.field public static final FLAG_CHANGED_ACCOUNT_PROFILE:Ljava/lang/String; = "flag_changed_activity_profile"

.field public static final FLAG_CHANGED_ACTIVITY_INFO:Ljava/lang/String; = "flag_changed_activity_info"

.field public static final FLAG_CHANGED_ALARMS:Ljava/lang/String; = "flag_changed_alarms"

.field public static final FLAG_CHANGED_CONTEXTUAL:Ljava/lang/String; = "flag_changed_contextual"

.field public static final FLAG_CHANGED_GOALS:Ljava/lang/String; = "flag_changed_goals"

.field public static final FLAG_CHECK_COMPATIBILITY:Ljava/lang/String; = "check_compatibility"

.field public static final FLAG_CONFIRM_UPDATE_BOOTLOADER:Ljava/lang/String; = "flag_confirm_update"

.field public static final FLAG_CONFIRM_UPDATE_KERNEL:Ljava/lang/String; = "flag_confirm_update"

.field public static final FLAG_CONTEXTUAL_WAS_DONE:Ljava/lang/String; = "flag_contextual_was_done"

.field public static final FLAG_DIRTY_ORDER_TO_CLOUD:Ljava/lang/String; = "dirty_order_to_cloud"

.field public static final FLAG_GET_CPU_ID:Ljava/lang/String; = "flag_get_cpu_id"

.field public static final FLAG_IS_FORCE_OTA:Ljava/lang/String; = "flag_is_force_ota"

.field public static final FLAG_MANDATORY_APP_UPDATE:Ljava/lang/String; = "mandatory_app_update"

.field public static final FLAG_MANDATORY_VOS_UPDATE:Ljava/lang/String; = "mandatory_kernel_update"

.field public static final FLAG_MIRROR_PHONE:Ljava/lang/String; = "flag_mirror_phone"

.field public static final FLAG_NEEDS_ALL_NOTIFICATIONS_TO_BE_ENABLED:Ljava/lang/String; = "flag_notifications_allowed_from_os"

.field public static final FLAG_NEEDS_DISCONNECT_CONNECT:Ljava/lang/String; = "flag_check_for_connect_disconnect_need"

.field public static final FLAG_SEND_PARSE_INSTALLATION_ID:Ljava/lang/String; = "flag_send_parse_installation_id"

.field public static final FLAG_SHOWED_ONBOARDING:Ljava/lang/String; = "flag_show_onboarding"

.field public static final FLAG_SHOW_STREAM_TUTORIAL:Ljava/lang/String; = "flag_show_stream_tutorial"

.field public static final FLAG_SHOW_SYNC_TO_FIT_POPUP:Ljava/lang/String; = "flag_show_sync_to_fit_popup"

.field public static final FLAG_SHOW_WATCHFACE_STREAM_TUTORIAL:Ljava/lang/String; = "flag_show_watchface_stream_tutorial"

.field public static final FLAG_SHOW_WATCHFACE_TUTORIAL:Ljava/lang/String; = "flag_show_watchface_tutorial"

.field public static final FLAG_SYNC_DEFAULTS_FROM_CLOUD:Ljava/lang/String; = "flag_sync_system_apps"

.field public static final FLAG_SYNC_DEFAULTS_TO_CLOUD:Ljava/lang/String; = "flag_sync_defaults_to_cloud"

.field public static final FLAG_SYNC_LOCALE_TO_WATCH:Ljava/lang/String; = "flag_sync_locale"

.field public static final FLAG_SYNC_SETTINGS_FROM_CLOUD:Ljava/lang/String; = "flag_sync_settings_from_cloud"

.field public static final FLAG_SYNC_SETTINGS_TO_CLOUD:Ljava/lang/String; = "flag_sync_settings_to_cloud"

.field public static final FLAG_SYNC_TO_GOOGLE_FIT:Ljava/lang/String; = "flag_sync_to_google_fit"

.field public static final FLAG_UNPAIRED_FROM_PHONE_SETTINGS:Ljava/lang/String; = "unpaired_from_phone_settings"

.field public static final FLAG_WATCH_PAIRED_BEFORE:Ljava/lang/String; = "flag_first_paired_watch"

.field public static final PAIR_TIME:Ljava/lang/String; = "pref_pair_time"

.field public static final PREFS_ACTIVITY_GOAL_ACHIEVEMENT:Ljava/lang/String; = "pref_goal_achievement"

.field public static final PREFS_ACTIVITY_GOAL_ALMOST:Ljava/lang/String; = "pref_goal_almost"

.field public static final PREFS_ACTIVITY_NEWSLETTER:Ljava/lang/String; = "pref_activity_newsletter"

.field public static final PREFS_ACTIVITY_REMINDER:Ljava/lang/String; = "pref_activity_reminder"

.field public static final PREFS_ACTIVITY_TOTALS_CALORIES:Ljava/lang/String; = "totals_calories"

.field public static final PREFS_ACTIVITY_TOTALS_DISTANCE:Ljava/lang/String; = "totals_distance"

.field public static final PREFS_ACTIVITY_TOTALS_SLEEP:Ljava/lang/String; = "totals_sleep"

.field public static final PREFS_ACTIVITY_TOTALS_STEPS:Ljava/lang/String; = "totals_steps"

.field public static final PREFS_APP_LANGUAGE:Ljava/lang/String; = "prefs_app_language"

.field public static final PREFS_APP_NOTIFICATION:Ljava/lang/String; = "app_not"

.field public static final PREFS_APP_NOTIFICATION_ALL:Ljava/lang/String; = "app_not_all"

.field public static final PREFS_BONDED_WATCH_ADDRESS:Ljava/lang/String; = "vector_ble_address"

.field public static final PREFS_BONDED_WATCH_NAME:Ljava/lang/String; = "vector_ble_name"

.field public static final PREFS_CLOUD_STATUS:Ljava/lang/String; = "prefs_cloud_status"

.field public static final PREFS_DND:Ljava/lang/String; = "dnd_button"

.field public static final PREFS_DND_DIRTY:Ljava/lang/String; = "dnd_dirty"

.field public static final PREFS_GLANCE:Ljava/lang/String; = "glance_mode"

.field public static final PREFS_GLANCE_DIRTY:Ljava/lang/String; = "glance_dirty"

.field public static final PREFS_GOAL_CALORIES_ACTIVE:Ljava/lang/String; = "goal_calories_active_float"

.field public static final PREFS_GOAL_DISTANCE_ACTIVE:Ljava/lang/String; = "goal_distance_active_float"

.field public static final PREFS_GOAL_SLEEP_ACTIVE:Ljava/lang/String; = "goal_sleep_active_float"

.field public static final PREFS_GOAL_STEPS_ACTIVE:Ljava/lang/String; = "goal_steps_active_float"

.field public static final PREFS_IN_PROGRESS_CALORIES:Ljava/lang/String; = "in_progress_calories"

.field public static final PREFS_IN_PROGRESS_DISTANCE:Ljava/lang/String; = "in_progress_distance"

.field public static final PREFS_IN_PROGRESS_STEPS:Ljava/lang/String; = "in_progress_steps"

.field public static final PREFS_LAST_SYNCED_BATTERY_LEVEL:Ljava/lang/String; = "last_synced_battery_level"

.field public static final PREFS_LAST_SYNCED_BATTERY_STATUS:Ljava/lang/String; = "last_synced_battery_status"

.field public static final PREFS_LAST_UPDATE_ACTIVITY:Ljava/lang/String; = "last_updated_activity"

.field public static final PREFS_LAST_UPDATE_DONE:Ljava/lang/String; = "last_update_done"

.field public static final PREFS_LOCALE:Ljava/lang/String; = "prefs_locale"

.field public static final PREFS_NEED_TO_REGISTER_WATCH:Ljava/lang/String; = "bonded_watch_dirty"

.field public static final PREFS_NOTIF_FIRST_TIME:Ljava/lang/String; = "pref_notif_first_time"

.field public static final PREFS_OFFLINE_BADGE:Ljava/lang/String; = "prefs_offline_badge"

.field public static final PREFS_OFFLINE_STATUS:Ljava/lang/String; = "prefs_offline_mode_status"

.field public static final PREFS_PREVIOUS_WATCH_SHAPE:Ljava/lang/String; = "previous_watch_shape"

.field public static final PREFS_SETTINGS_ACCOUNT_PROFILE_ACCOUNT_ADDRESS:Ljava/lang/String; = "settings_account_address"

.field public static final PREFS_SETTINGS_ACCOUNT_PROFILE_CONNECTED_STATUS:Ljava/lang/String; = "settings_connected_status"

.field public static final PREFS_SETTINGS_ACCOUNT_PROFILE_GREETING_NAME:Ljava/lang/String; = "settings_greeting_name"

.field public static final PREFS_SETTINGS_ACCOUNT_PROFILE_GREETING_NAME_DIRTY:Ljava/lang/String; = "settings_greeting_name_dirty"

.field public static final PREFS_SETTINGS_ACCOUNT_PROFILE_TIME_FORMAT:Ljava/lang/String; = "settings_time_format"

.field public static final PREFS_SETTINGS_ACCOUNT_PROFILE_TIME_FORMAT_DIRTY:Ljava/lang/String; = "settings_time_format_dirty"

.field public static final PREFS_SETTINGS_ACCOUNT_PROFILE_UNIT_SYSTEM_FORMAT:Ljava/lang/String; = "settings_unit_system_format"

.field public static final PREFS_SETTINGS_ACCOUNT_PROFILE_UNIT_SYSTEM_FORMAT_DIRTY:Ljava/lang/String; = "settings_unit_system_format_dirty"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_ACTIVITY_LEVEL_DIRTY:Ljava/lang/String; = "activity_info_activity_level_dirty"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_AGE_DIRTY:Ljava/lang/String; = "activity_info_age_dirty"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_DATE_OF_BIRTH:Ljava/lang/String; = "activity_info_age"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_GENDER:Ljava/lang/String; = "activity_info_gender"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_GENDER_DIRTY:Ljava/lang/String; = "activity_info_gender_dirty"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_HEIGHT_CM:Ljava/lang/String; = "activity_info_height_cm"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_HEIGHT_DIRTY:Ljava/lang/String; = "activity_info_height_dirty"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_HEIGHT_FEET:Ljava/lang/String; = "activity_info_height_feet"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_HEIGHT_INCHES:Ljava/lang/String; = "activity_info_height_inches"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_WEIGHT_DIRTY:Ljava/lang/String; = "activity_info_weight_dirty"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_WEIGHT_KG:Ljava/lang/String; = "activity_info_weight_kg"

.field public static final PREFS_SETTINGS_ACTIVITY_INFO_WEIGHT_LBS:Ljava/lang/String; = "activity_info_weight_lbs"

.field public static final PREFS_SETTINGS_CONTEXTUAL_AUTO_DISCREET:Ljava/lang/String; = "settings_contextual_auto_discreet"

.field public static final PREFS_SETTINGS_CONTEXTUAL_AUTO_DISCREET_DIRTY:Ljava/lang/String; = "settings_contextual_auto_discreet_dirty"

.field public static final PREFS_SETTINGS_CONTEXTUAL_AUTO_SLEEP:Ljava/lang/String; = "settings_contextual_auto_sleep"

.field public static final PREFS_SETTINGS_CONTEXTUAL_AUTO_SLEEP_DIRTY:Ljava/lang/String; = "settings_contextual_auto_sleep_dirty"

.field public static final PREFS_SETTINGS_CONTEXTUAL_FLICK_TO_DISMISS:Ljava/lang/String; = "drop_notification_option"

.field public static final PREFS_SETTINGS_CONTEXTUAL_FLICK_TO_DISMISS_DIRTY:Ljava/lang/String; = "drop_notification_option_dirty"

.field public static final PREFS_SETTINGS_CONTEXTUAL_MORNING_FACE:Ljava/lang/String; = "settings_contextual_morning_face"

.field public static final PREFS_SETTINGS_CONTEXTUAL_MORNING_FACE_DIRTY:Ljava/lang/String; = "settings_contextual_morning_face_dirty"

.field public static final PREFS_SETTINGS_CONTEXTUAL_NOTIFICATIONS:Ljava/lang/String; = "prefs_settings_contextual_notifications"

.field public static final PREFS_SETTINGS_CONTEXTUAL_TENTATIVE_MEETINGS:Ljava/lang/String; = "settings_contextual_tentative_meetings"

.field public static final PREFS_SETTINGS_CONTEXTUAL_TENTATIVE_MEETINGS_DIRTY:Ljava/lang/String; = "settings_contextual_tentative_meetings_DIRTY"

.field public static final PREFS_SHOW_ONLINE:Ljava/lang/String; = "prefs_show_online"

.field public static final PREFS_SYSTEM_NOTIFICATION:Ljava/lang/String; = "system_not"

.field public static final PREFS_WATCH_CURRENT:Ljava/lang/String; = "current_watch_selected"

.field public static final PREFS_WATCH_NEW_CPU_ID:Ljava/lang/String; = "watch_new_cpu_id"

.field public static final PREFS_WATCH_OLD_CPU_ID:Ljava/lang/String; = "watch_old_cpu_id"

.field public static final PREFS_WATCH_OS_VERSION:Ljava/lang/String; = "watch_os_version"

.field public static final PREFS_WATCH_SHAPE:Ljava/lang/String; = "watch_shape"

.field public static final PREF_ACCOUNT_UPDATE_TYPE:Ljava/lang/String; = "account_update_type"

.field public static final PREF_BACKLIGHT_INTENSITY:Ljava/lang/String; = "pref_backlight_intensity"

.field public static final PREF_BACKLIGHT_TIMEOUT:Ljava/lang/String; = "pref_backlight_duration"

.field public static final PREF_CURRENT_APP_VERSION:Ljava/lang/String; = "current_app_version"

.field public static final PREF_DISPLAY_SECOND_HAND:Ljava/lang/String; = "pref_watch_second_hand"

.field public static final PREF_GEOFENCE_LAST_KNOWN_LOCATION:Ljava/lang/String; = "pref_geofence_last_known_location"

.field public static final PREF_LAST_KNOWN_SYSTEM_INFO:Ljava/lang/String; = "last_known_system_info"

.field public static final PREF_LINK_LOST_ICON:Ljava/lang/String; = "pref_link_lost_icon"

.field public static final PREF_LINK_LOST_NOTIFICATION:Ljava/lang/String; = "pref_link_lost_notification"

.field public static final PREF_LOCALE_WATCH:Ljava/lang/String; = "pref_locale_watch"

.field public static final PREF_LOW_BATTERY_ICON:Ljava/lang/String; = "pref_low_battery_icon"

.field public static final PREF_LOW_BATTERY_NOTIFICATION:Ljava/lang/String; = "pref_low_battery_notification"

.field public static final PREF_NEWS_APP:Ljava/lang/String; = "pref_news_app"

.field public static final PREF_NEWS_NEWS:Ljava/lang/String; = "pref_news_news"

.field public static final PREF_NOTIFICATION_DISPLAY:Ljava/lang/String; = "pref_notification_display"

.field public static final PREF_PERS_MESSAGE:Ljava/lang/String; = "pref_pers_message"

.field public static final PREF_PHONE_BATTERY_STREAM_LAST_UPDATE_TIME:Ljava/lang/String; = "update_phone_battery"

.field public static final PREF_PHONE_BATTERY_UNDEFINED:I = -0x1

.field public static final PREF_SERIAL_NUMBER:Ljava/lang/String; = "pref_serial_number"

.field public static final PREF_SYNC_ACTIVITY_ALARM_LAST_TRIGGERED_TIMESTAMP:Ljava/lang/String; = "pref_last_triggered_alarm_sync"

.field public static final PREF_UPDATE_ID_BOOTLOADER:Ljava/lang/String; = "update_id_bootloader"

.field public static final PREF_UPDATE_ID_KERNEL:Ljava/lang/String; = "update_id"

.field public static final PREF_UPDATE_WATCH_STATUS:Ljava/lang/String; = "update_watch_status"

.field public static final PREF_VECTOR_FILE:Ljava/lang/String; = "vector_preferences"

.field public static final PREF_WATCH_LANGUAGE_SELECTED:Ljava/lang/String; = "pref_watch_language_selected"

.field public static final SETTINGS_NOTIFICATION_DISPLAY_ALERT:I = 0x2

.field public static final SETTINGS_NOTIFICATION_DISPLAY_CONTENT:I = 0x1

.field public static final SETTINGS_NOTIFICATION_DISPLAY_OFF:I = 0x0

.field public static final STATE_DOWNLOAD_UPDATE:I = 0x2

.field public static final STATE_INACTIVE:I = 0x5

.field public static final STATE_INSTALLING_UPDATE:I = 0x4

.field public static final STATE_REQUEST_SUMMARY:I = 0x1

.field public static final STATE_UPDATE_BROKEN:I = 0x6

.field public static final STATE_UPDATE_FOUND:I = 0x8

.field public static final STATE_VERIFY_UPDATE:I = 0x7

.field public static final STATE_WAITING_FOR_WATCH:I = 0x3

.field public static final TIME_FORMAT_12:I = 0x1

.field public static final TIME_FORMAT_24:I = 0x0

.field public static final TOTAL_INSTALL_PARTS:Ljava/lang/String; = "total_install_parts"

.field public static final UNIT_SYSTEM_IMPERIAL:I = 0x1

.field public static final UNIT_SYSTEM_METRIC:I

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 276
    const-class v0, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 756
    return-void
.end method

.method public static getAccountAddress(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 842
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 844
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "settings_account_address"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F
    .locals 4
    .param p0, "type"    # Lcom/vectorwatch/android/utils/Constants$GoalType;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 771
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 773
    .local v0, "prefs":Landroid/content/SharedPreferences;
    sget-object v2, Lcom/vectorwatch/android/utils/SharedPreferencesHelper$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoalType:[I

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/Constants$GoalType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 783
    sget-object v2, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->log:Lorg/slf4j/Logger;

    const-string v3, "Wrong type of goal. Check for possible goal types."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 787
    :goto_0
    return v1

    .line 775
    :pswitch_0
    const-string v2, "goal_steps_active_float"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    goto :goto_0

    .line 777
    :pswitch_1
    const-string v2, "goal_calories_active_float"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    goto :goto_0

    .line 779
    :pswitch_2
    const-string v2, "goal_distance_active_float"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    goto :goto_0

    .line 781
    :pswitch_3
    const-string v2, "goal_sleep_active_float"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    goto :goto_0

    .line 773
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getActivityInProgressForCalories(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 711
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 712
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "in_progress_calories"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getActivityInProgressForDistance(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 716
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 717
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "in_progress_distance"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getActivityInProgressForSteps(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 706
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 707
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "in_progress_steps"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getActivityInfoDateOfBirth(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1036
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1038
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "activity_info_age"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method public static getActivityInfoGender(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1020
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1022
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "activity_info_gender"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getActivityInfoHeight(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1057
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1059
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "activity_info_height_cm"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getActivityInfoWeight(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1079
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1081
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "activity_info_weight_kg"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getAppNotification(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 647
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 648
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "app_not"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 584
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 585
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "vector_ble_address"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 611
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 612
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "vector_ble_name"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z
    .locals 3
    .param p0, "preferenceName"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1144
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1145
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getContextualAutoDiscreetSettings(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 920
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 922
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "settings_contextual_auto_discreet"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getContextualAutoSleepSettings(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 868
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 869
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "settings_contextual_auto_sleep"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getContextualMorningFaceSettings(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 815
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 817
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "settings_contextual_morning_face"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getCounterWatchCrashLogs(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 348
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 349
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "crash_logs"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getDirtyField(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3
    .param p0, "label"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1106
    const-string v1, "vector_preferences"

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1107
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getDndButtonState(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 425
    const-string v2, "vector_preferences"

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 427
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "dnd_button"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 429
    .local v1, "status":Ljava/lang/Boolean;
    return-object v1
.end method

.method public static getFlagTutorial(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "flag"    # Ljava/lang/String;

    .prologue
    .line 335
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 336
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getGreetingUserName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 948
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 949
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "settings_greeting_name"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I
    .locals 3
    .param p0, "preferenceName"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1157
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1158
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getIsOfflineMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1117
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1118
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const-string v1, "prefs_offline_mode_status"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getLastUpdateActivity(Landroid/content/Context;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 701
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 702
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "last_updated_activity"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method public static getLoggedInStatus(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 894
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 896
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "settings_connected_status"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getLongPreference(Ljava/lang/String;JLandroid/content/Context;)J
    .locals 5
    .param p0, "preferenceName"    # Ljava/lang/String;
    .param p1, "defaultValue"    # J
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 1170
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1171
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method public static getPrefsWatchCurrent(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 361
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 362
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "current_watch_selected"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getSharedPreferencesFile(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 727
    const-string v0, "vector_preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "preferenceName"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1183
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1184
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getSupportedAppNotification(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3
    .param p0, "appIdentity"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 674
    const-string v1, "vector_preferences"

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 675
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getSystemNotification(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 622
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 623
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "system_not"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getTimeFormatPreference(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 976
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 978
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "settings_time_format"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getTotalsCalories(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 410
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 412
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "totals_calories"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getTotalsDistance(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 380
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 382
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "totals_distance"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getTotalsSleep(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 395
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 397
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "totals_sleep"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getTotalsSteps(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 366
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 368
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "totals_steps"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getUnitSystemFormatPreference(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1003
    const-string v1, "vector_preferences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1005
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "settings_unit_system_format"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 533
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 535
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const/4 v1, 0x0

    .line 536
    .local v1, "result":Ljava/lang/String;
    const-string v2, "watch_new_cpu_id"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 537
    if-eqz v1, :cond_0

    .line 538
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 540
    :cond_0
    return-object v1
.end method

.method public static getWatchOSVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 488
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 489
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "watch_os_version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getWatchOldCpuId(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 500
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 502
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const/4 v1, 0x0

    .line 503
    .local v1, "result":Ljava/lang/String;
    const-string v2, "watch_old_cpu_id"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 504
    if-eqz v1, :cond_0

    .line 505
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 507
    :cond_0
    return-object v1
.end method

.method public static getWatchShape(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 448
    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 450
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "watch_shape"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static setAccountAddress(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "accountAddress"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 797
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 798
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 800
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "settings_account_address"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 801
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 802
    return-void
.end method

.method public static setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V
    .locals 4
    .param p0, "value"    # F
    .param p1, "type"    # Lcom/vectorwatch/android/utils/Constants$GoalType;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 739
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 740
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 742
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/vectorwatch/android/utils/SharedPreferencesHelper$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoalType:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/Constants$GoalType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 756
    sget-object v2, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->log:Lorg/slf4j/Logger;

    const-string v3, "Wrong type of goal. Check for possible goal types."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 759
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 760
    return-void

    .line 744
    :pswitch_0
    const-string v2, "goal_steps_active_float"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 747
    :pswitch_1
    const-string v2, "goal_calories_active_float"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 750
    :pswitch_2
    const-string v2, "goal_distance_active_float"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 753
    :pswitch_3
    const-string v2, "goal_sleep_active_float"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 742
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static setActivityInfoDateOfBirth(JLandroid/content/Context;)V
    .locals 4
    .param p0, "dobInMillis"    # J
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1026
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1027
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1028
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "activity_info_age"

    invoke-interface {v0, v2, p0, p1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1029
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1031
    const-string v2, "activity_info_age_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1032
    return-void
.end method

.method public static setActivityInfoGender(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1010
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1011
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1013
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "activity_info_gender"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1014
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1016
    const-string v2, "activity_info_gender_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1017
    return-void
.end method

.method public static setActivityInfoHeight(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1042
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1043
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1044
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "activity_info_height_cm"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1045
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1047
    const-string v2, "activity_info_height_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1048
    return-void
.end method

.method public static setActivityInfoWeight(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1063
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1064
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1066
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "activity_info_weight_kg"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1067
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1069
    const-string v2, "activity_info_weight_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1070
    return-void
.end method

.method public static setAppNotification(ZLandroid/content/Context;)V
    .locals 4
    .param p0, "enableState"    # Z
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 658
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 659
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 661
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "app_not"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 662
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 663
    return-void
.end method

.method public static setBondedDeviceAddress(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "deviceAddress"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 566
    const-string v2, "vector_preferences"

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 567
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 569
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "vector_ble_address"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 570
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 573
    const-string v2, "unpaired_from_phone_settings"

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 575
    return-void
.end method

.method public static setBondedDeviceName(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "deviceName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 595
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 596
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 598
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "vector_ble_name"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 599
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 601
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/PairedDeviceStateChangedEvent;

    invoke-direct {v3}, Lcom/vectorwatch/android/events/PairedDeviceStateChangedEvent;-><init>()V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 602
    return-void
.end method

.method public static declared-synchronized setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V
    .locals 6
    .param p0, "preferenceName"    # Ljava/lang/String;
    .param p1, "isEnabled"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1136
    const-class v3, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Preference = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is set to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1137
    const-string v2, "vector_preferences"

    const/4 v4, 0x0

    invoke-virtual {p2, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1138
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1139
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1140
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1141
    monitor-exit v3

    return-void

    .line 1136
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "prefs":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static setContextualAutoDiscreetSettings(ZLandroid/content/Context;)V
    .locals 4
    .param p0, "isEnabled"    # Z
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 932
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 933
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 935
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "settings_contextual_auto_discreet"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 936
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 938
    const-string v2, "settings_contextual_auto_discreet_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 939
    return-void
.end method

.method public static setContextualAutoSleepSettings(ZLandroid/content/Context;)V
    .locals 4
    .param p0, "isEnabled"    # Z
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 879
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 880
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 881
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "settings_contextual_auto_sleep"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 882
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 884
    const-string v2, "settings_contextual_auto_sleep_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 885
    return-void
.end method

.method public static setContextualMorningFaceSettings(ZLandroid/content/Context;)V
    .locals 4
    .param p0, "isEnabled"    # Z
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 827
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 828
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 829
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "settings_contextual_morning_face"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 830
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 832
    const-string v2, "settings_contextual_morning_face_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 833
    return-void
.end method

.method public static setCounterWatchCrashLogs(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 340
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 341
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 343
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "crash_logs"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 344
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 345
    return-void
.end method

.method public static setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V
    .locals 4
    .param p0, "label"    # Ljava/lang/String;
    .param p1, "isDirty"    # Z
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1092
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1093
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1094
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1095
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1096
    return-void
.end method

.method public static setDndState(Ljava/lang/Boolean;Landroid/content/Context;)V
    .locals 4
    .param p0, "state"    # Ljava/lang/Boolean;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 434
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 435
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 437
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "dnd_button"

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 438
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 439
    return-void
.end method

.method public static setFlagTutorial(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "state"    # Z
    .param p2, "flag"    # Ljava/lang/String;

    .prologue
    .line 327
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 328
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 330
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 331
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 332
    return-void
.end method

.method public static setGreetingUserName(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 906
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 907
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 908
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "settings_greeting_name"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 909
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 911
    const-string v2, "settings_greeting_name_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 912
    return-void
.end method

.method public static setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V
    .locals 5
    .param p0, "preferenceName"    # Ljava/lang/String;
    .param p1, "value"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1149
    sget-object v2, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Preference = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is set to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1150
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1151
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1152
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1153
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1154
    return-void
.end method

.method public static setLoggedInStatus(ZLandroid/content/Context;)V
    .locals 4
    .param p0, "isLoggedIn"    # Z
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 854
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 855
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 857
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "settings_connected_status"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 858
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 859
    return-void
.end method

.method public static setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V
    .locals 5
    .param p0, "preferenceName"    # Ljava/lang/String;
    .param p1, "value"    # J
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 1162
    sget-object v2, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Preference = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is set to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1163
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1164
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1165
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1166
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1167
    return-void
.end method

.method public static setOfflineModeStatus(Landroid/content/Context;Z)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "status"    # Z

    .prologue
    .line 1128
    sget-object v2, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Preference = prefs_offline_mode_status is set to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1129
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1130
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1131
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "prefs_offline_mode_status"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1132
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1133
    return-void
.end method

.method public static setPrefsWatchCurrent(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 353
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 354
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 356
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "current_watch_selected"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 357
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 358
    return-void
.end method

.method public static setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p0, "preferenceName"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1175
    sget-object v2, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Preference = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is set to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1176
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1177
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1178
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1179
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1180
    return-void
.end method

.method public static setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V
    .locals 4
    .param p0, "appIdentity"    # Ljava/lang/String;
    .param p1, "enableState"    # Ljava/lang/Boolean;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 687
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 688
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 689
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v0, p0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 691
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 692
    return-void
.end method

.method public static setSystemNotification(ZLandroid/content/Context;)V
    .locals 4
    .param p0, "enableState"    # Z
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 633
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 634
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 636
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "system_not"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 637
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 638
    return-void
.end method

.method public static setTimeFormatPreference(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 960
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 961
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 963
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "settings_time_format"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 964
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 966
    const-string v2, "settings_time_format_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 967
    return-void
.end method

.method public static setTotalsCalories(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 417
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 418
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 420
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "totals_calories"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 421
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 422
    return-void
.end method

.method public static setTotalsDistance(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 387
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 388
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 390
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "totals_distance"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 391
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 392
    return-void
.end method

.method public static setTotalsSleep(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 402
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 403
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 405
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "totals_sleep"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 406
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 407
    return-void
.end method

.method public static setTotalsSteps(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 372
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 373
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 375
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "totals_steps"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 376
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 377
    return-void
.end method

.method public static setUnitSystemFormatPreference(ILandroid/content/Context;)V
    .locals 4
    .param p0, "value"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 988
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 989
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 991
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "settings_unit_system_format"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 992
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 993
    const-string v2, "settings_unit_system_format_dirty"

    const/4 v3, 0x1

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 994
    return-void
.end method

.method public static setWatchNewCpuId(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "cpuIdBase64"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 551
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 552
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 554
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "watch_new_cpu_id"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 555
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 556
    return-void
.end method

.method public static setWatchOSVersion(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "version"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 475
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 476
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 478
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "watch_os_version"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 479
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 480
    return-void
.end method

.method public static setWatchOldCpuId(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "cpuIdBase64"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 518
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 519
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 521
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "watch_old_cpu_id"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 522
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 523
    return-void
.end method

.method public static setWatchShape(Ljava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p0, "shape"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 461
    const-string v2, "vector_preferences"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 462
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 464
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "watch_shape"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 465
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 466
    return-void
.end method
