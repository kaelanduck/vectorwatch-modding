.class public Lcom/vectorwatch/android/utils/MigrationHelper;
.super Ljava/lang/Object;
.source "MigrationHelper.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/vectorwatch/android/utils/MigrationHelper;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private static executeMigration(ILandroid/content/Context;)V
    .locals 12
    .param p0, "version"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v11, 0x6e

    const/4 v10, 0x1

    .line 93
    sget-object v7, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MIGRATION - execute migration for version : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 94
    const-string v7, "current_app_version"

    const/4 v8, 0x0

    .line 95
    invoke-static {v7, v8, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 94
    invoke-static {v7}, Lcom/vectorwatch/android/utils/MigrationHelper;->getVersionNumberFromName(Ljava/lang/String;)I

    move-result v5

    .line 97
    .local v5, "prevVersion":I
    if-lt p0, v11, :cond_2

    .line 98
    if-lt v5, v11, :cond_0

    const/4 v7, -0x1

    if-ne v5, v7, :cond_1

    .line 99
    :cond_0
    sget-object v7, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    const-string v8, "MIGRATION - version >= 110 and prev < 110 => SYNC DEFAULTS TO CLOUD"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 100
    const-string v7, "flag_sync_defaults_to_cloud"

    invoke-static {v7, v10, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 104
    :cond_1
    const/16 v7, 0x82

    if-gt v5, v7, :cond_2

    .line 106
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v7

    sget-object v8, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/database/DatabaseManager;->setGoogleFitDirtyFlagToFalse(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 107
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v7

    sget-object v8, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/database/DatabaseManager;->setGoogleFitDirtyFlagToFalse(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 108
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v7

    sget-object v8, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/database/DatabaseManager;->setGoogleFitDirtyFlagToFalse(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 112
    :cond_2
    const/16 v7, 0xb4

    if-gt v5, v7, :cond_3

    .line 113
    sget-object v7, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    const-string v8, "MIGRATION: RE-LOGIN: <= 180"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 114
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->needsLogin(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 172
    :cond_3
    :goto_0
    return-void

    .line 119
    :cond_4
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 121
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    if-eqz v1, :cond_3

    .line 125
    const-string v7, "com.vectorwatch.android"

    invoke-virtual {v1, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 126
    .local v2, "accounts":[Landroid/accounts/Account;
    if-eqz v2, :cond_3

    array-length v7, v2

    if-lt v7, v10, :cond_3

    .line 130
    const/4 v7, 0x0

    aget-object v0, v2, v7

    .line 133
    .local v0, "account":Landroid/accounts/Account;
    :try_start_0
    invoke-virtual {v1, v0}, Landroid/accounts/AccountManager;->getPassword(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    .line 135
    .local v4, "password":Ljava/lang/String;
    if-nez v4, :cond_5

    .line 136
    sget-object v7, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    const-string v8, "MIGRATION: RE-LOGIN: Null pass"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 137
    invoke-static {v1, v0}, Lcom/vectorwatch/android/utils/MigrationHelper;->logoutUser(Landroid/accounts/AccountManager;Landroid/accounts/Account;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 167
    .end local v4    # "password":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 168
    .local v3, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MIGRATION: Exception at auto-relogin in migration: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 169
    invoke-static {v1, v0}, Lcom/vectorwatch/android/utils/MigrationHelper;->logoutUser(Landroid/accounts/AccountManager;Landroid/accounts/Account;)V

    goto :goto_0

    .line 139
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v4    # "password":Ljava/lang/String;
    :cond_5
    :try_start_1
    sget-object v7, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    const-string v8, "MIGRATION: RE-LOGIN: Trying re-login"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 142
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v7, v4}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->prepareUserDataForLogin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/UserModel;

    move-result-object v6

    .line 144
    .local v6, "userInfo":Lcom/vectorwatch/android/models/UserModel;
    new-instance v7, Lcom/vectorwatch/android/utils/MigrationHelper$1;

    invoke-direct {v7, v1, v0, p1}, Lcom/vectorwatch/android/utils/MigrationHelper$1;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;)V

    invoke-static {v6, v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->accountLogin(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static getVersionNumberFromName(Ljava/lang/String;)I
    .locals 11
    .param p0, "versionName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const/4 v6, -0x1

    .line 52
    if-nez p0, :cond_0

    .line 55
    sget-object v7, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    const-string v8, "The version name should never be null."

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 83
    :goto_0
    return v6

    .line 59
    :cond_0
    const/16 v7, 0x2d

    invoke-virtual {p0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 61
    .local v4, "pos":I
    move-object v5, p0

    .line 63
    .local v5, "versionPrefix":Ljava/lang/String;
    if-eq v4, v6, :cond_1

    .line 65
    invoke-virtual {p0, v10, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 67
    sget-object v7, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MIGRATION - current version is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ". The prefix is: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 70
    :cond_1
    const-string v7, "\\."

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 72
    .local v3, "parts":[Ljava/lang/String;
    if-eqz v3, :cond_2

    array-length v7, v3

    const/4 v8, 0x3

    if-ge v7, v8, :cond_3

    .line 73
    :cond_2
    sget-object v7, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    const-string v8, "The version name should contain 3 numbers: major, minor and hotfix."

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_3
    aget-object v1, v3, v10

    .line 78
    .local v1, "major":Ljava/lang/String;
    const/4 v6, 0x1

    aget-object v2, v3, v6

    .line 79
    .local v2, "minor":Ljava/lang/String;
    const/4 v6, 0x2

    aget-object v0, v3, v6

    .line 81
    .local v0, "hotfix":Ljava/lang/String;
    sget-object v6, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MIGRATION - version numbers: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 83
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    mul-int/lit8 v6, v6, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    mul-int/lit8 v7, v7, 0xa

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v6, v7

    goto/16 :goto_0
.end method

.method private static logoutUser(Landroid/accounts/AccountManager;Landroid/accounts/Account;)V
    .locals 3
    .param p0, "accountManager"    # Landroid/accounts/AccountManager;
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    const/4 v2, 0x0

    .line 181
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 182
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "MIGRATION: Null account/account manager at logoutUser attempt."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 191
    :goto_0
    return-void

    .line 186
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-ge v0, v1, :cond_2

    .line 187
    invoke-virtual {p0, p1, v2, v2}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0

    .line 189
    :cond_2
    invoke-virtual {p0, p1}, Landroid/accounts/AccountManager;->removeAccountExplicitly(Landroid/accounts/Account;)Z

    goto :goto_0
.end method

.method public static migrateApp(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p0, "versionName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    if-nez p0, :cond_0

    .line 36
    sget-object v1, Lcom/vectorwatch/android/utils/MigrationHelper;->log:Lorg/slf4j/Logger;

    const-string v2, "Trying to decide if migration is needed, but the current app version is null."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 43
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-static {p0}, Lcom/vectorwatch/android/utils/MigrationHelper;->getVersionNumberFromName(Ljava/lang/String;)I

    move-result v0

    .line 42
    .local v0, "versionNumber":I
    invoke-static {v0, p1}, Lcom/vectorwatch/android/utils/MigrationHelper;->executeMigration(ILandroid/content/Context;)V

    goto :goto_0
.end method
