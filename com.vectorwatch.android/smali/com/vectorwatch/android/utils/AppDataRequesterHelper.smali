.class public Lcom/vectorwatch/android/utils/AppDataRequesterHelper;
.super Ljava/lang/Object;
.source "AppDataRequesterHelper.java"

# interfaces
.implements Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;
    }
.end annotation


# static fields
.field public static HAS_AUTH_EXPIRED_NOTIFICATION:Z = false

.field public static HAS_LOCATION_NOTIFICATION:Z = false

.field private static final TTL_ERROR_SECONDS:I = 0xa

.field public static final VERSION:Ljava/lang/Short;

.field private static final WATCH_ERROR:Ljava/lang/String; = "AAAAAQAAAABFcnJvci4uLgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=="

.field private static final WATCH_NO_GPS_ERROR:Ljava/lang/String; = "AAAAAQAAAABObyBpbnRlcm5ldAAAAAAAAAAAAAAAAAAAAAAAAAAA"

.field private static final WATCH_NO_INTERNET_ERROR:Ljava/lang/String; = "AAAAAQAAAABObyBpbnRlcm5ldAAAAAAAAAAAAAAAAAAAAAAAAAAA"

.field private static final WATCH_PLEASE_LOGIN:Ljava/lang/String; = "AAAAAQAAAABQbGVhc2UgbG9naW4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=="

.field private static final log:Lorg/slf4j/Logger;

.field private static queue:Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;


# instance fields
.field public app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

.field private appNeedsLocation:Z

.field public button:Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

.field public buttonEvent:Ljava/lang/String;

.field public buttonType:Ljava/lang/String;

.field callResult:Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

.field public callReturned:Z

.field public callWasLaunched:Z

.field public cloudElementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

.field protected context:Landroid/content/Context;

.field public elementId:I

.field public identifier:I

.field private location:Lcom/vectorwatch/android/models/VectorLocation;

.field private locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

.field public mAppId:I

.field public param:I

.field public receivedAuthError:Z

.field public receivedError:Z

.field public receivedGpsTimeoutError:Z

.field public watchfaceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    const-class v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->log:Lorg/slf4j/Logger;

    .line 52
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->VERSION:Ljava/lang/Short;

    .line 66
    sput-boolean v1, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_LOCATION_NOTIFICATION:Z

    .line 67
    sput-boolean v1, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_AUTH_EXPIRED_NOTIFICATION:Z

    .line 82
    new-instance v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    invoke-direct {v0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->queue:Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->appNeedsLocation:Z

    .line 63
    iput-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 69
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callReturned:Z

    .line 70
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callWasLaunched:Z

    .line 71
    iput-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callResult:Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

    .line 72
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->receivedError:Z

    .line 73
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->receivedAuthError:Z

    .line 74
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->receivedGpsTimeoutError:Z

    .line 92
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    .line 93
    return-void
.end method

.method private _doPerformRequest()V
    .locals 12

    .prologue
    .line 234
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "AppDataRequesterHelper - performing request"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->button:Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    if-nez v0, :cond_0

    .line 236
    new-instance v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->cloudElementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

    iget v3, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    iget v4, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    iget v5, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->param:I

    iget-object v7, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->buttonType:Ljava/lang/String;

    iget-object v10, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->buttonEvent:Ljava/lang/String;

    move-object v6, p0

    invoke-direct/range {v0 .. v10}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;-><init>(Lcom/vectorwatch/android/models/DownloadedAppDetails;Lcom/vectorwatch/android/models/CloudElementSummary;IIILcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 238
    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 244
    :goto_0
    return-void

    .line 240
    :cond_0
    new-instance v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->cloudElementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

    iget v3, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->button:Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    iget v5, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->identifier:I

    iget v6, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->param:I

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->buttonType:Ljava/lang/String;

    iget-object v11, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->buttonEvent:Ljava/lang/String;

    move-object v7, p0

    invoke-direct/range {v0 .. v11}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;-><init>(Lcom/vectorwatch/android/models/DownloadedAppDetails;Lcom/vectorwatch/android/models/CloudElementSummary;ILcom/vectorwatch/android/models/EventDestination$ButtonWatch;IILcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask$Callback;Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 242
    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAdditionalDataAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private _doPerformRequestWithLocation()V
    .locals 2

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->getLocationHelper()Lcom/vectorwatch/android/utils/LocationHelper;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$1;-><init>(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/LocationHelper;->startLocationUpdates(Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;)Lcom/vectorwatch/android/utils/LocationHelper;

    .line 269
    return-void
.end method

.method static synthetic access$002(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/models/VectorLocation;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppDataRequesterHelper;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/VectorLocation;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->location:Lcom/vectorwatch/android/models/VectorLocation;

    return-object p1
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->_doPerformRequest()V

    return-void
.end method

.method public static receiveResponseFromOtherChannels(Landroid/content/Context;ILcom/vectorwatch/android/models/AppCallbackProxyResponse;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # I
    .param p2, "response"    # Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

    .prologue
    .line 85
    new-instance v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;-><init>(Landroid/content/Context;)V

    .line 86
    .local v0, "appDataRequesterHelper":Lcom/vectorwatch/android/utils/AppDataRequesterHelper;
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callWasLaunched:Z

    .line 87
    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->enqueueRequest()V

    .line 88
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->onSuccess(Lcom/vectorwatch/android/models/AppCallbackProxyResponse;I)V

    .line 89
    return-void
.end method


# virtual methods
.method protected canPerformRequest()Z
    .locals 2

    .prologue
    .line 282
    const/4 v0, 0x1

    .line 283
    .local v0, "canPerformRequest":Z
    iget-boolean v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->appNeedsLocation:Z

    if-eqz v1, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->testForGPSConnection()Z

    move-result v1

    and-int/2addr v0, v1

    .line 286
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->testForInternetConnection()Z

    move-result v1

    and-int/2addr v0, v1

    .line 288
    return v0
.end method

.method public enqueueRequest()V
    .locals 1

    .prologue
    .line 213
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->queue:Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->addRequestToQueue(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    .line 214
    return-void
.end method

.method protected getElementType(II)Ljava/lang/String;
    .locals 1
    .param p1, "watchfaceId"    # I
    .param p2, "elementId"    # I

    .prologue
    .line 511
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    .line 513
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v0

    .line 517
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "list"

    goto :goto_0
.end method

.method protected getLocationHelper()Lcom/vectorwatch/android/utils/LocationHelper;
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    if-nez v0, :cond_0

    .line 426
    new-instance v0, Lcom/vectorwatch/android/utils/LocationHelper;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/utils/LocationHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    return-object v0
.end method

.method protected initApp()V
    .locals 2

    .prologue
    .line 537
    iget v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 538
    iget v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppSummaryWithId(ILandroid/content/Context;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->cloudElementSummary:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 539
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->appNeedsLocation:Z

    .line 541
    :try_start_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->button:Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->button:Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    iget-object v0, v0, Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;->requirements:Ljava/util/List;

    const-string v1, "sendLocation"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->appNeedsLocation:Z

    .line 548
    :goto_0
    return-void

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    iget v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getDatasource()Lcom/vectorwatch/android/models/Element$DataSource;

    move-result-object v0

    iget-object v0, v0, Lcom/vectorwatch/android/models/Element$DataSource;->requirements:Ljava/util/List;

    const-string v1, "sendLocation"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->appNeedsLocation:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 546
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected markAppAsAuthExpired()V
    .locals 14

    .prologue
    .line 137
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v8

    new-instance v9, Lcom/vectorwatch/android/events/AppListReloadEvent;

    invoke-direct {v9}, Lcom/vectorwatch/android/events/AppListReloadEvent;-><init>()V

    invoke-virtual {v8, v9}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 138
    sget-boolean v8, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_AUTH_EXPIRED_NOTIFICATION:Z

    if-nez v8, :cond_0

    .line 139
    new-instance v2, Landroid/content/Intent;

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const-class v9, Lcom/vectorwatch/android/receivers/NotificationDismissReceiver;

    invoke-direct {v2, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    .local v2, "deleteIntent":Landroid/content/Intent;
    const-string v8, "notification_type"

    const-string v9, "notification_type_auth_expired_data_request"

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v9, v2, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 144
    .local v3, "deletePendingIntent":Landroid/app/PendingIntent;
    new-instance v8, Landroid/support/v7/app/NotificationCompat$Builder;

    iget-object v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/support/v7/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0200f0

    .line 145
    invoke-virtual {v8, v9}, Landroid/support/v7/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v10, 0x7f09016f

    .line 146
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v10, 0x7f09016e

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v13, v13, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    aput-object v13, v11, v12

    .line 147
    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, 0x3

    .line 148
    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, 0x1

    .line 149
    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    .line 150
    invoke-virtual {v8, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/NotificationCompat$Builder;

    .line 152
    .local v1, "builder":Landroid/support/v7/app/NotificationCompat$Builder;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    long-to-float v8, v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->intValue()I

    move-result v5

    .line 154
    .local v5, "notification_id":I
    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const-class v9, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    .local v7, "resultIntent":Landroid/content/Intent;
    const-string v8, "action"

    const/4 v9, 0x2

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 156
    const-string v8, "app_id"

    iget v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 158
    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const/4 v9, 0x0

    const/high16 v10, 0x8000000

    invoke-static {v8, v9, v7, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 159
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v1, v6}, Landroid/support/v7/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 161
    new-instance v0, Landroid/content/Intent;

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const-class v9, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 162
    .local v0, "actionIntent":Landroid/content/Intent;
    const-string v8, "action"

    const/4 v9, 0x2

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 163
    const-string v8, "app_id"

    iget v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 164
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v10, 0x7f09016d

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {v10, v11, v0, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    invoke-virtual {v1, v8, v9, v10}, Landroid/support/v7/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 166
    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const-string v9, "notification"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 167
    .local v4, "mNotificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v1}, Landroid/support/v7/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 168
    const/4 v8, 0x1

    sput-boolean v8, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_AUTH_EXPIRED_NOTIFICATION:Z

    .line 170
    .end local v0    # "actionIntent":Landroid/content/Intent;
    .end local v1    # "builder":Landroid/support/v7/app/NotificationCompat$Builder;
    .end local v2    # "deleteIntent":Landroid/content/Intent;
    .end local v3    # "deletePendingIntent":Landroid/app/PendingIntent;
    .end local v4    # "mNotificationManager":Landroid/app/NotificationManager;
    .end local v5    # "notification_id":I
    .end local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v7    # "resultIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method protected notifyUserForGpsDisabled()V
    .locals 14

    .prologue
    .line 176
    sget-boolean v8, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_LOCATION_NOTIFICATION:Z

    if-nez v8, :cond_0

    .line 177
    new-instance v2, Landroid/content/Intent;

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const-class v9, Lcom/vectorwatch/android/receivers/NotificationDismissReceiver;

    invoke-direct {v2, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 178
    .local v2, "deleteIntent":Landroid/content/Intent;
    const-string v8, "notification_type"

    const-string v9, "notification_type_gps_disabled_data_request"

    invoke-virtual {v2, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v9, v2, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 182
    .local v3, "deletePendingIntent":Landroid/app/PendingIntent;
    new-instance v8, Landroid/support/v7/app/NotificationCompat$Builder;

    iget-object v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/support/v7/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f0200f0

    .line 183
    invoke-virtual {v8, v9}, Landroid/support/v7/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v10, 0x7f090161

    .line 184
    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    iget-object v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v10, 0x7f090160

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v13, v13, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    aput-object v13, v11, v12

    .line 185
    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, 0x3

    .line 186
    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, 0x1

    .line 187
    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    .line 188
    invoke-virtual {v8, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/NotificationCompat$Builder;

    .line 190
    .local v1, "builder":Landroid/support/v7/app/NotificationCompat$Builder;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    long-to-float v8, v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->intValue()I

    move-result v5

    .line 192
    .local v5, "notification_id":I
    new-instance v7, Landroid/content/Intent;

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const-class v9, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 193
    .local v7, "resultIntent":Landroid/content/Intent;
    const-string v8, "action"

    const/4 v9, 0x3

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 194
    const-string v8, "app_id"

    iget v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 196
    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const/4 v9, 0x0

    const/high16 v10, 0x8000000

    invoke-static {v8, v9, v7, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 197
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v1, v6}, Landroid/support/v7/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 199
    new-instance v0, Landroid/content/Intent;

    const-string v8, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 200
    .local v0, "actionIntent":Landroid/content/Intent;
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v10, 0x7f09015f

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {v10, v11, v0, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    invoke-virtual {v1, v8, v9, v10}, Landroid/support/v7/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 202
    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const-string v9, "notification"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 203
    .local v4, "mNotificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v1}, Landroid/support/v7/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 204
    const/4 v8, 0x1

    sput-boolean v8, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_LOCATION_NOTIFICATION:Z

    .line 206
    .end local v0    # "actionIntent":Landroid/content/Intent;
    .end local v1    # "builder":Landroid/support/v7/app/NotificationCompat$Builder;
    .end local v2    # "deleteIntent":Landroid/content/Intent;
    .end local v3    # "deletePendingIntent":Landroid/app/PendingIntent;
    .end local v4    # "mNotificationManager":Landroid/app/NotificationManager;
    .end local v5    # "notification_id":I
    .end local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v7    # "resultIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onAuthError(Lcom/vectorwatch/android/models/AlertMessage;)V
    .locals 8
    .param p1, "message"    # Lcom/vectorwatch/android/models/AlertMessage;

    .prologue
    const/4 v2, 0x1

    .line 370
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "AppDataRequesterHelper - auth error"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 371
    iput-boolean v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callReturned:Z

    .line 372
    iput-boolean v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->receivedAuthError:Z

    .line 373
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 375
    if-eqz p1, :cond_1

    .line 376
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packMessageForAlerts(Lcom/vectorwatch/android/models/AlertMessage;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    .line 381
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->markAppAsAuthExpired()V

    .line 382
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->queue:Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->removeRequestFromQueue(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    .line 384
    :cond_0
    return-void

    .line 378
    :cond_1
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    iget v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    iget v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    iget v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v4, 0x7f09005b

    .line 379
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v5, 0x7f0901e9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v7, 0x7f090083

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 378
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packCustomAlert(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v6, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    goto :goto_0
.end method

.method public onError(Lcom/vectorwatch/android/models/AlertMessage;)V
    .locals 8
    .param p1, "message"    # Lcom/vectorwatch/android/models/AlertMessage;

    .prologue
    const v7, 0x7f090083

    const v4, 0x7f09005b

    const/4 v2, 0x1

    .line 340
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "AppDataRequesterHelper - cloud error"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 341
    iput-boolean v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callReturned:Z

    .line 342
    iput-boolean v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->receivedError:Z

    .line 343
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/NetworkUtils;->isOnline(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 345
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 346
    if-eqz p1, :cond_1

    .line 347
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packMessageForAlerts(Lcom/vectorwatch/android/models/AlertMessage;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    .line 357
    :goto_0
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->queue:Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->removeRequestFromQueue(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    .line 359
    :cond_0
    return-void

    .line 349
    :cond_1
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    iget v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    iget v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    iget v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    .line 350
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v5, 0x7f090222

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 349
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packCustomAlert(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v6, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    goto :goto_0

    .line 353
    :cond_2
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    iget v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    iget v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    iget v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v5, 0x7f09010f

    .line 354
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 353
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packCustomAlert(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v6, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    goto :goto_0
.end method

.method public onGpsTimeoutError()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 292
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callReturned:Z

    .line 293
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->receivedGpsTimeoutError:Z

    .line 294
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    iget v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    iget v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    iget v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v4, 0x7f09005b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v5, 0x7f0901c4

    .line 296
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v7, 0x7f090083

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 295
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packCustomAlert(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v6, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    .line 297
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->queue:Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->removeRequestFromQueue(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    .line 299
    :cond_0
    return-void
.end method

.method public onSuccess(Lcom/vectorwatch/android/models/AppCallbackProxyResponse;I)V
    .locals 3
    .param p1, "data"    # Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    .param p2, "dataID"    # I

    .prologue
    const/4 v2, 0x0

    .line 310
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "AppDataRequesterHelper - response ok"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 311
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callReturned:Z

    .line 312
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callResult:Lcom/vectorwatch/android/models/AppCallbackProxyResponse;

    .line 313
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 314
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;->commands:Ljava/util/List;

    if-nez v0, :cond_2

    .line 315
    :cond_0
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->onError(Lcom/vectorwatch/android/models/AlertMessage;)V

    .line 329
    :cond_1
    :goto_0
    return-void

    .line 318
    :cond_2
    iput-object v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->location:Lcom/vectorwatch/android/models/VectorLocation;

    .line 320
    iget-object v0, p1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;->appPopup:Lcom/vectorwatch/android/models/AlertMessage;

    if-eqz v0, :cond_3

    .line 321
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    iget-object v1, p1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;->appPopup:Lcom/vectorwatch/android/models/AlertMessage;

    invoke-static {v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packMessageForAlerts(Lcom/vectorwatch/android/models/AlertMessage;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    .line 325
    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->sendReceivedData(Lcom/vectorwatch/android/models/AppCallbackProxyResponse;I)V

    .line 327
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->queue:Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->removeRequestFromQueue(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    goto :goto_0
.end method

.method protected packErrorData(IIILjava/lang/String;Ljava/lang/String;)[B
    .locals 9
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .param p3, "elementId"    # I
    .param p4, "command"    # Ljava/lang/String;
    .param p5, "element_type"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 486
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 487
    .local v2, "now":Landroid/text/format/Time;
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 488
    invoke-virtual {v2, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v4, v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getVectorTimeFromUnixTime(I)I

    move-result v4

    add-int/lit8 v3, v4, 0xa

    .line 489
    .local v3, "ttl":I
    invoke-static {p4, v8}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 490
    .local v1, "commandBytes":[B
    array-length v4, v1

    add-int/lit8 v4, v4, 0xf

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 491
    .local v0, "bytes":Ljava/nio/ByteBuffer;
    sget-object v4, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->MESSAGE_CONTROL_TYPE_PUSH:Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;

    invoke-virtual {v4}, Lcom/vectorwatch/android/service/ble/messages/BaseMessage$BleMessageType;->getValue()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 492
    sget-object v4, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->VERSION:Ljava/lang/Short;

    invoke-virtual {v4}, Ljava/lang/Short;->shortValue()S

    move-result v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 493
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 494
    int-to-byte v4, p2

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 495
    int-to-byte v4, p3

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 496
    invoke-static {p5}, Lcom/vectorwatch/android/utils/Constants;->getElementType(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$AppElementType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/utils/Constants$AppElementType;->getVal()I

    move-result v4

    int-to-byte v4, v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 497
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 498
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 499
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    return-object v4
.end method

.method protected parseData([B)V
    .locals 3
    .param p1, "data"    # [B

    .prologue
    .line 526
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 527
    .local v0, "dataBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    .line 528
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    .line 529
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    .line 530
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->param:I

    .line 531
    return-void
.end method

.method protected performDataRequest()V
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->canPerformRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->callWasLaunched:Z

    .line 222
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->appNeedsLocation:Z

    if-eqz v0, :cond_1

    .line 223
    invoke-direct {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->_doPerformRequestWithLocation()V

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    invoke-direct {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->_doPerformRequest()V

    goto :goto_0
.end method

.method protected sendReceivedData(Lcom/vectorwatch/android/models/AppCallbackProxyResponse;I)V
    .locals 19
    .param p1, "data"    # Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    .param p2, "dataID"    # I

    .prologue
    .line 438
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 439
    .local v18, "messages":Ljava/util/List;, "Ljava/util/List<[B>;"
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    if-eqz v1, :cond_7

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    if-eqz v1, :cond_7

    .line 442
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;->commands:Ljava/util/List;

    if-eqz v1, :cond_7

    .line 443
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;->commands:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move/from16 v0, v17

    if-ge v0, v1, :cond_7

    .line 444
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse;->data:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$BaseData;->response:Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;

    iget-object v1, v1, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Response;->commands:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;

    .line 445
    .local v16, "currentCommand":Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->messageType:Ljava/lang/String;

    if-eqz v1, :cond_0

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->messageType:Ljava/lang/String;

    const-string v4, "element_data"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 447
    :cond_0
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->command:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 448
    .local v2, "commandBytes":[B
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->appUuid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppId(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    .line 449
    .local v3, "appId":I
    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v4}, Lcom/vectorwatch/android/utils/Helpers;->setBit(BB)B

    move-result v9

    .line 451
    .local v9, "extra":B
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    move-object/from16 v0, v16

    iget v4, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->watchfaceId:I

    move-object/from16 v0, v16

    iget v5, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->elementId:I

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->elementType:Ljava/lang/String;

    .line 452
    invoke-static {v6}, Lcom/vectorwatch/android/utils/Constants;->getElementType(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$AppElementType;

    move-result-object v6

    .line 453
    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->getTTLForWatch()I

    move-result v7

    move/from16 v8, p2

    .line 451
    invoke-static/range {v1 .. v9}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendPushData(Landroid/content/Context;[BIIILcom/vectorwatch/android/utils/Constants$AppElementType;III)Ljava/util/UUID;

    .line 443
    .end local v2    # "commandBytes":[B
    .end local v3    # "appId":I
    .end local v9    # "extra":B
    :cond_1
    :goto_1
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 454
    :cond_2
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->messageType:Ljava/lang/String;

    const-string v4, "command"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 455
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->parameters:Ljava/util/Map;

    const-string v4, "mAppId"

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->parameters:Ljava/util/Map;

    const-string v4, "mAppId"

    .line 456
    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 455
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 457
    .restart local v3    # "appId":I
    :goto_2
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->parameters:Ljava/util/Map;

    const-string v4, "animation"

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->parameters:Ljava/util/Map;

    const-string v4, "animation"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->fromString(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->getWatchValue()B

    move-result v13

    .line 458
    .local v13, "animation":I
    :goto_3
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->parameters:Ljava/util/Map;

    const-string v4, "alert"

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->parameters:Ljava/util/Map;

    const-string v4, "alert"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v4, "true"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v15, 0x1

    .line 459
    .local v15, "vibrate":Z
    :goto_4
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;->parameters:Ljava/util/Map;

    const-string v4, "watchfaceId"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 460
    .local v12, "watchfaceId":I
    const/4 v14, 0x1

    .line 462
    .local v14, "force":Z
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    move v11, v3

    invoke-static/range {v10 .. v15}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendChangeActiveAppRequest(Landroid/content/Context;IIIZZ)Ljava/util/UUID;

    goto/16 :goto_1

    .line 455
    .end local v3    # "appId":I
    .end local v12    # "watchfaceId":I
    .end local v13    # "animation":I
    .end local v14    # "force":Z
    .end local v15    # "vibrate":Z
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    goto :goto_2

    .line 457
    .restart local v3    # "appId":I
    :cond_4
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->UP_IN:Lcom/vectorwatch/android/utils/Constants$WatchAnimation;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$WatchAnimation;->getWatchValue()B

    move-result v13

    goto :goto_3

    .line 458
    .restart local v13    # "animation":I
    :cond_5
    const/4 v15, 0x0

    goto :goto_4

    :cond_6
    const/4 v15, 0x0

    goto :goto_4

    .line 467
    .end local v3    # "appId":I
    .end local v13    # "animation":I
    .end local v16    # "currentCommand":Lcom/vectorwatch/android/models/AppCallbackProxyResponse$Command;
    .end local v17    # "i":I
    :cond_7
    return-void
.end method

.method public setBinaryData([B)Lcom/vectorwatch/android/utils/AppDataRequesterHelper;
    .locals 0
    .param p1, "data"    # [B

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->parseData([B)V

    .line 104
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->initApp()V

    .line 105
    return-object p0
.end method

.method public setData(IILcom/vectorwatch/android/models/EventDestination$ButtonWatch;IILjava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper;
    .locals 0
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .param p3, "button"    # Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;
    .param p4, "identifier"    # I
    .param p5, "value"    # I
    .param p6, "buttonIndex"    # Ljava/lang/String;
    .param p7, "buttonEvent"    # Ljava/lang/String;

    .prologue
    .line 121
    iput p1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    .line 122
    iput p2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    .line 123
    iput-object p3, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->button:Lcom/vectorwatch/android/models/EventDestination$ButtonWatch;

    .line 124
    iput p4, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->identifier:I

    .line 125
    iput p5, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->param:I

    .line 126
    iput-object p6, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->buttonType:Ljava/lang/String;

    .line 127
    iput-object p7, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->buttonEvent:Ljava/lang/String;

    .line 128
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->initApp()V

    .line 129
    return-object p0
.end method

.method protected testForGPSConnection()Z
    .locals 8

    .prologue
    .line 413
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->getLocationHelper()Lcom/vectorwatch/android/utils/LocationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    const/4 v0, 0x1

    .line 420
    :goto_0
    return v0

    .line 416
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "AppDataRequesterHelper - will not perform request - no gps"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 417
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    iget v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    iget v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    iget v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v4, 0x7f09005b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v5, 0x7f0901c4

    .line 418
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v7, 0x7f090083

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 417
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packCustomAlert(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v6, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    .line 419
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->notifyUserForGpsDisabled()V

    .line 420
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected testForInternetConnection()Z
    .locals 8

    .prologue
    .line 394
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/NetworkUtils;->isOnline(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    const/4 v0, 0x1

    .line 401
    :goto_0
    return v0

    .line 397
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "AppDataRequesterHelper - will not perform request - no internet"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 398
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    iget v0, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->mAppId:I

    iget v1, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->elementId:I

    iget v2, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->watchfaceId:I

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v4, 0x7f09005b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v5, 0x7f09010f

    .line 399
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->context:Landroid/content/Context;

    const v7, 0x7f090083

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 398
    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/managers/incoming_data_handlers/AppHelper;->packCustomAlert(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v6, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlert(Landroid/content/Context;[B)Ljava/util/UUID;

    .line 400
    sget-object v0, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->queue:Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;->removeRequestFromQueue(Lcom/vectorwatch/android/utils/AppDataRequesterHelper;)Lcom/vectorwatch/android/utils/AppDataRequesterHelper$QueueManager;

    .line 401
    const/4 v0, 0x0

    goto :goto_0
.end method
