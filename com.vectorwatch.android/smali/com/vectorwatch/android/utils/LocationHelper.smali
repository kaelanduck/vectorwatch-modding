.class public Lcom/vectorwatch/android/utils/LocationHelper;
.super Ljava/lang/Object;
.source "LocationHelper.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;,
        Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;
    }
.end annotation


# static fields
.field public static ACCURACY_THRESHOLD:F

.field public static LOCATION_QUERY_TIME_SECONDS:F

.field public static LOCATION_TESTING_POOL_SIZE:F

.field public static LOCATION_TIMEOUT_SECONDS:F

.field public static STARTED_LOCATION_SETTINGS_ACTIVITY:Z

.field public static USE_LOCATION_CACHE_IF_AVAILABLE:Z

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

.field context:Landroid/content/Context;

.field feedbackContext:Landroid/content/Context;

.field private isRequestingSingleLocationUpdate:Z

.field locationManager:Landroid/location/LocationManager;

.field private requestAccuracy:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

.field retrievedLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private startTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    .line 50
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vectorwatch/android/utils/LocationHelper;->USE_LOCATION_CACHE_IF_AVAILABLE:Z

    .line 51
    const/high16 v0, 0x41200000    # 10.0f

    sput v0, Lcom/vectorwatch/android/utils/LocationHelper;->ACCURACY_THRESHOLD:F

    .line 52
    const/high16 v0, 0x40000000    # 2.0f

    sput v0, Lcom/vectorwatch/android/utils/LocationHelper;->LOCATION_TESTING_POOL_SIZE:F

    .line 53
    const/high16 v0, 0x40400000    # 3.0f

    sput v0, Lcom/vectorwatch/android/utils/LocationHelper;->LOCATION_QUERY_TIME_SECONDS:F

    .line 54
    const/high16 v0, 0x41700000    # 15.0f

    sput v0, Lcom/vectorwatch/android/utils/LocationHelper;->LOCATION_TIMEOUT_SECONDS:F

    .line 58
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vectorwatch/android/utils/LocationHelper;->STARTED_LOCATION_SETTINGS_ACTIVITY:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->startTime:J

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->isRequestingSingleLocationUpdate:Z

    .line 59
    sget-object v0, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->ACCURATE_GPS:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    iput-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->requestAccuracy:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->context:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->context:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    .line 81
    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/utils/LocationHelper;)J
    .locals 2
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/LocationHelper;

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->startTime:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/vectorwatch/android/utils/LocationHelper;J)J
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/LocationHelper;
    .param p1, "x1"    # J

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/vectorwatch/android/utils/LocationHelper;->startTime:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/utils/LocationHelper;)Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/LocationHelper;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->requestAccuracy:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/utils/LocationHelper;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/LocationHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/utils/LocationHelper;->setFeedbackText(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/utils/LocationHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/LocationHelper;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->isRequestingSingleLocationUpdate:Z

    return v0
.end method

.method static synthetic access$402(Lcom/vectorwatch/android/utils/LocationHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/LocationHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/vectorwatch/android/utils/LocationHelper;->isRequestingSingleLocationUpdate:Z

    return p1
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/utils/LocationHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/LocationHelper;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/vectorwatch/android/utils/LocationHelper;->queryLocationManagerWithDelay()V

    return-void
.end method

.method private queryLocationManagerWithDelay()V
    .locals 6

    .prologue
    .line 179
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/vectorwatch/android/utils/LocationHelper$4;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/utils/LocationHelper$4;-><init>(Lcom/vectorwatch/android/utils/LocationHelper;)V

    sget v2, Lcom/vectorwatch/android/utils/LocationHelper;->LOCATION_QUERY_TIME_SECONDS:F

    float-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 239
    return-void
.end method

.method private setFeedbackText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->feedbackContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->feedbackContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 92
    :cond_0
    return-void
.end method

.method public static triggerGPSDisabledDialogWithApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/app/Activity;)V
    .locals 1
    .param p0, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/vectorwatch/android/utils/LocationHelper;->triggerGPSDisabledDialogWithApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)V

    .line 119
    return-void
.end method

.method public static triggerGPSDisabledDialogWithApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 5
    .param p0, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "dismissClickListener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 122
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 123
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09015e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 124
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09015d

    new-instance v2, Lcom/vectorwatch/android/utils/LocationHelper$3;

    invoke-direct {v2, p1}, Lcom/vectorwatch/android/utils/LocationHelper$3;-><init>(Landroid/app/Activity;)V

    .line 125
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900ab

    new-instance v2, Lcom/vectorwatch/android/utils/LocationHelper$2;

    invoke-direct {v2, p2}, Lcom/vectorwatch/android/utils/LocationHelper$2;-><init>(Landroid/content/DialogInterface$OnClickListener;)V

    .line 133
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/utils/LocationHelper$1;

    invoke-direct {v1, p2}, Lcom/vectorwatch/android/utils/LocationHelper$1;-><init>(Landroid/content/DialogInterface$OnClickListener;)V

    .line 142
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027

    .line 150
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 152
    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 345
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 346
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/LocationHelper;->isRequestingLocationUpdates()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 350
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 351
    return-void
.end method

.method public getLocation()Landroid/location/Location;
    .locals 11

    .prologue
    .line 359
    const/4 v10, 0x0

    .line 362
    .local v10, "location":Landroid/location/Location;
    :try_start_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v8

    .line 365
    .local v8, "isGPSEnabled":Z
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 367
    .local v9, "isNetworkEnabled":Z
    if-nez v8, :cond_2

    if-nez v9, :cond_2

    .line 399
    .end local v8    # "isGPSEnabled":Z
    .end local v9    # "isNetworkEnabled":Z
    :cond_0
    :goto_0
    if-eqz v10, :cond_1

    .line 400
    sget-object v0, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GPS location latitude "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " longitude "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 401
    invoke-virtual {v10}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " hasSpeed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Landroid/location/Location;->hasSpeed()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " speed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Landroid/location/Location;->getSpeed()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 400
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 404
    :cond_1
    return-object v10

    .line 370
    .restart local v8    # "isGPSEnabled":Z
    .restart local v9    # "isNetworkEnabled":Z
    :cond_2
    if-eqz v9, :cond_3

    .line 371
    :try_start_1
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 373
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_3

    .line 374
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    .line 375
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v10

    .line 376
    sget-object v0, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "LocationHelper: get location from network"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 380
    :cond_3
    if-eqz v8, :cond_0

    .line 381
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 384
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    .line 386
    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v7

    .line 388
    .local v7, "gpsLocation":Landroid/location/Location;
    if-eqz v7, :cond_0

    .line 389
    sget-object v0, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "LocationHelper: get location from gps"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 390
    move-object v10, v7

    goto/16 :goto_0

    .line 395
    .end local v7    # "gpsLocation":Landroid/location/Location;
    .end local v8    # "isGPSEnabled":Z
    .end local v9    # "isNetworkEnabled":Z
    :catch_0
    move-exception v6

    .line 396
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public isAccurate(Landroid/location/Location;)Z
    .locals 2
    .param p1, "l"    # Landroid/location/Location;

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    sget v1, Lcom/vectorwatch/android/utils/LocationHelper;->ACCURACY_THRESHOLD:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAvailable()Z
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/LocationHelper;->requestAccuracy:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->getLocationProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isGPSAvailable()Z
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isRequestingLocationUpdates()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->isRequestingSingleLocationUpdate:Z

    if-eqz v0, :cond_0

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->isRequestingSingleLocationUpdate:Z

    .line 273
    :cond_0
    if-eqz p1, :cond_2

    .line 274
    sget-object v0, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "LOCATION HELPER: Location retrieved by the system"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;->onLocation(Landroid/location/Location;)V

    .line 279
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/LocationHelper;->onNewLocationAdded()V

    .line 281
    :cond_2
    return-void
.end method

.method protected onNewLocationAdded()V
    .locals 6

    .prologue
    .line 319
    iget-object v3, p0, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    int-to-float v3, v3

    sget v4, Lcom/vectorwatch/android/utils/LocationHelper;->LOCATION_TESTING_POOL_SIZE:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_3

    .line 320
    sget-object v3, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    const-string v4, "LOCATION HELPER: Location was added to pool, making comparison"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 321
    const/4 v0, 0x1

    .line 323
    .local v0, "canSendAccurateLocation":Z
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    int-to-float v3, v1

    sget v4, Lcom/vectorwatch/android/utils/LocationHelper;->LOCATION_TESTING_POOL_SIZE:F

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_0

    .line 324
    iget-object v3, p0, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    iget-object v4, p0, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    .line 325
    .local v2, "location":Landroid/location/Location;
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/utils/LocationHelper;->isAccurate(Landroid/location/Location;)Z

    move-result v3

    and-int/2addr v0, v3

    .line 323
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 328
    .end local v2    # "location":Landroid/location/Location;
    :cond_0
    if-eqz v0, :cond_2

    .line 329
    sget-object v3, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    const-string v4, "LOCATION HELPER: We found accurate location"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 330
    const-string v3, "Location found"

    invoke-direct {p0, v3}, Lcom/vectorwatch/android/utils/LocationHelper;->setFeedbackText(Ljava/lang/String;)V

    .line 331
    iget-object v3, p0, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    if-eqz v3, :cond_1

    .line 332
    iget-object v4, p0, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    iget-object v3, p0, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    iget-object v5, p0, Lcom/vectorwatch/android/utils/LocationHelper;->retrievedLocations:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/Location;

    invoke-interface {v4, v3}, Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;->onAccurateLocation(Landroid/location/Location;)V

    .line 341
    .end local v0    # "canSendAccurateLocation":Z
    .end local v1    # "i":I
    :cond_1
    :goto_1
    return-void

    .line 335
    .restart local v0    # "canSendAccurateLocation":Z
    .restart local v1    # "i":I
    :cond_2
    sget-object v3, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    const-string v4, "LOCATION HELPER: Location not accurate enough"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 338
    .end local v0    # "canSendAccurateLocation":Z
    .end local v1    # "i":I
    :cond_3
    sget-object v3, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    const-string v4, "LOCATION HELPER: Location was added to pool, still not enough location objects to make a comparison"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 313
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 303
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "i"    # I
    .param p3, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 293
    return-void
.end method

.method public setFeedbackContext(Landroid/content/Context;)Lcom/vectorwatch/android/utils/LocationHelper;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/vectorwatch/android/utils/LocationHelper;->feedbackContext:Landroid/content/Context;

    .line 85
    return-object p0
.end method

.method public setRequestAccuracy(Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;)Lcom/vectorwatch/android/utils/LocationHelper;
    .locals 0
    .param p1, "value"    # Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/vectorwatch/android/utils/LocationHelper;->requestAccuracy:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    .line 63
    return-object p0
.end method

.method public startLocationUpdates(Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;)Lcom/vectorwatch/android/utils/LocationHelper;
    .locals 6
    .param p1, "callback"    # Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 161
    sget-object v0, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "LOCATION HELPER: Starting location updates"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 162
    if-eqz p1, :cond_0

    .line 163
    iput-object p1, p0, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    .line 164
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 165
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->requestAccuracy:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    sget-object v1, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->ACCURATE_GPS:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    if-ne v0, v1, :cond_0

    .line 166
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 170
    :cond_0
    const-string v0, "Requesting location..."

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/utils/LocationHelper;->setFeedbackText(Ljava/lang/String;)V

    .line 171
    invoke-direct {p0}, Lcom/vectorwatch/android/utils/LocationHelper;->queryLocationManagerWithDelay()V

    .line 172
    return-object p0
.end method

.method public stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;
    .locals 2

    .prologue
    .line 247
    sget-object v0, Lcom/vectorwatch/android/utils/LocationHelper;->log:Lorg/slf4j/Logger;

    const-string v1, "LOCATION HELPER: Stoping location updates"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->locationManager:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 249
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->isRequestingSingleLocationUpdate:Z

    if-eqz v0, :cond_0

    .line 250
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->isRequestingSingleLocationUpdate:Z

    .line 253
    :cond_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->startTime:J

    .line 254
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/LocationHelper;->callback:Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;

    .line 255
    return-object p0
.end method
