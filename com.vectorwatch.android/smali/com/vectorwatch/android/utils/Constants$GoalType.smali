.class public final enum Lcom/vectorwatch/android/utils/Constants$GoalType;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GoalType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$GoalType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$GoalType;

.field public static final enum CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

.field public static final enum DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

.field public static final enum SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

.field public static final enum STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 775
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$GoalType;

    const-string v1, "STEPS"

    invoke-direct {v0, v1, v2, v2}, Lcom/vectorwatch/android/utils/Constants$GoalType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$GoalType;

    const-string v1, "CALORIES"

    invoke-direct {v0, v1, v3, v3}, Lcom/vectorwatch/android/utils/Constants$GoalType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$GoalType;

    const-string v1, "DISTANCE"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$GoalType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$GoalType;

    const-string v1, "SLEEP"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$GoalType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    .line 774
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$GoalType;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$GoalType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 779
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 780
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$GoalType;->val:I

    .line 781
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$GoalType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 774
    const-class v0, Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$GoalType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$GoalType;
    .locals 1

    .prologue
    .line 774
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$GoalType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$GoalType;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 784
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$GoalType;->val:I

    return v0
.end method
