.class public Lcom/vectorwatch/android/utils/AppInstallManager;
.super Ljava/lang/Object;
.source "AppInstallManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;,
        Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;,
        Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;
    }
.end annotation


# static fields
.field private static final REQUEST_CODE_APP_AUTHENTICATION:I = 0x66

.field private static final REQUEST_CODE_APP_SETTINGS:I = 0x65

.field private static final REQUEST_CODE_STREAM_AUTHENTICATION:I = 0x67

.field private static final log:Lorg/slf4j/Logger;

.field private static mApplicationContext:Landroid/content/Context;

.field private static mCurrentElementInstalling:Lcom/vectorwatch/android/models/CloudElementSummary;

.field private static sCurrentInstallingAppId:I

.field private static sCurrentInstallingPosition:I

.field private static sInstallAppFinishedRequestId:Ljava/util/UUID;

.field private static sResourcesToSendCount:Ljava/lang/Integer;

.field private static sUninstallAppId:I

.field private static sUninstallRequestId:Ljava/util/UUID;


# instance fields
.field private mCurrentActivity:Landroid/app/Activity;

.field private mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

.field private mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

.field private mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

.field private mEventBus:Lcom/vectorwatch/android/MainThreadBus;

.field private mIsInSetupMode:Z

.field private mIsInstalling:Z

.field private mProgressBar:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 113
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sResourcesToSendCount:Ljava/lang/Integer;

    .line 115
    const-class v0, Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    .line 117
    sput v1, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingAppId:I

    .line 118
    const/4 v0, 0x0

    sput v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingPosition:I

    .line 120
    sput v1, Lcom/vectorwatch/android/utils/AppInstallManager;->sUninstallAppId:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInstalling:Z

    .line 112
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    .line 127
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppListReloadEvent(Lcom/vectorwatch/android/models/StoreElement;)V

    return-void
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/utils/AppInstallManager;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/Stream;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerStreamListReloadEvent(Lcom/vectorwatch/android/models/Stream;)V

    return-void
.end method

.method static synthetic access$200()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/utils/AppInstallManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    return v0
.end method

.method static synthetic access$400()Landroid/content/Context;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500()Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentElementInstalling:Lcom/vectorwatch/android/models/CloudElementSummary;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p2, "x2"    # Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/utils/AppInstallManager;->cancelAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    return-void
.end method

.method static synthetic access$702(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    return-object p1
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    return-object v0
.end method

.method static synthetic access$802(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    return-object p1
.end method

.method static synthetic access$900(Lcom/vectorwatch/android/utils/AppInstallManager;)Lcom/vectorwatch/android/models/Stream;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    return-object v0
.end method

.method static synthetic access$902(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/Stream;)Lcom/vectorwatch/android/models/Stream;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/utils/AppInstallManager;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    return-object p1
.end method

.method private cancelAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V
    .locals 3
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p2, "type"    # Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    .prologue
    .line 383
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - cancel app install "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | reason = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 384
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->isAppInstalled(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->NOT_ENOUGH_SPACE:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    if-ne p2, v0, :cond_1

    .line 385
    :cond_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v0

    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->hardDeleteApp(ILandroid/content/Context;)V

    .line 388
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 389
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/CookieUtils;->clearWebviewCookies(Landroid/content/Context;)V

    .line 392
    :cond_2
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppListReloadEvent(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 393
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 394
    return-void
.end method

.method public static computeInstallMessageSize(Lcom/vectorwatch/android/models/InstallMessageModel;)I
    .locals 5
    .param p0, "installMessageModel"    # Lcom/vectorwatch/android/models/InstallMessageModel;

    .prologue
    .line 1341
    const/4 v1, 0x0

    .line 1344
    .local v1, "size":I
    add-int/lit8 v1, v1, 0x1

    .line 1347
    add-int/lit8 v1, v1, 0x4

    .line 1350
    add-int/lit8 v1, v1, 0x1

    .line 1353
    add-int/lit8 v1, v1, 0x1

    .line 1356
    add-int/lit8 v1, v1, 0x1

    .line 1359
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getNameLength()B

    move-result v2

    add-int/lit8 v1, v2, 0x8

    .line 1362
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getMetadataList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/FileMetadataModel;

    .line 1364
    .local v0, "fileMetadata":Lcom/vectorwatch/android/models/FileMetadataModel;
    add-int/lit8 v1, v1, 0x5

    .line 1367
    add-int/lit8 v1, v1, 0x2

    .line 1370
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/FileMetadataModel;->getMd5()[B

    move-result-object v3

    array-length v3, v3

    add-int/2addr v1, v3

    .line 1371
    goto :goto_0

    .line 1373
    .end local v0    # "fileMetadata":Lcom/vectorwatch/android/models/FileMetadataModel;
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "APP INSTALL MANAGER - size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1374
    return v1
.end method

.method public static getInstallDetails(I)Lcom/vectorwatch/android/models/InstallMessageModel;
    .locals 22
    .param p0, "appId"    # I

    .prologue
    .line 1162
    sget-object v18, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    move/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v4

    .line 1164
    .local v4, "cloudAppInstallObject":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getContent()Lcom/vectorwatch/android/models/AppContent;

    move-result-object v18

    if-nez v18, :cond_1

    .line 1165
    :cond_0
    sget-object v18, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v19, "APP INSTALL MANAGER - null app details or null app content"

    invoke-interface/range {v18 .. v19}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1166
    const/4 v11, 0x0

    .line 1267
    :goto_0
    return-object v11

    .line 1169
    :cond_1
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getContent()Lcom/vectorwatch/android/models/AppContent;

    move-result-object v5

    .line 1171
    .local v5, "content":Lcom/vectorwatch/android/models/AppContent;
    sget-object v18, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "APP INSTALL MANAGER - in get install details for app = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v4, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " mAppId = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " installPosition = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget v20, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingPosition:I

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1174
    new-instance v11, Lcom/vectorwatch/android/models/InstallMessageModel;

    invoke-direct {v11}, Lcom/vectorwatch/android/models/InstallMessageModel;-><init>()V

    .line 1177
    .local v11, "installMessage":Lcom/vectorwatch/android/models/InstallMessageModel;
    sget-object v18, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_PUT:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/vectorwatch/android/models/InstallMessageModel;->setType(Lcom/vectorwatch/android/utils/Constants$MessageInstallType;)V

    .line 1179
    new-instance v3, Lcom/vectorwatch/android/models/ApplicationPackageModel;

    invoke-direct {v3}, Lcom/vectorwatch/android/models/ApplicationPackageModel;-><init>()V

    .line 1182
    .local v3, "applicationPackage":Lcom/vectorwatch/android/models/ApplicationPackageModel;
    move/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setAppId(I)V

    .line 1185
    sget v18, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingPosition:I

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setPosition(B)V

    .line 1189
    iget-object v2, v5, Lcom/vectorwatch/android/models/AppContent;->appFile:Lcom/vectorwatch/android/models/AppFile;

    .line 1191
    .local v2, "appFile":Lcom/vectorwatch/android/models/AppFile;
    if-nez v2, :cond_4

    const/16 v18, 0x0

    :goto_1
    iget-object v0, v5, Lcom/vectorwatch/android/models/AppContent;->localResources:Ljava/util/List;

    move-object/from16 v19, v0

    if-nez v19, :cond_5

    const/16 v19, 0x0

    .line 1192
    :goto_2
    add-int v18, v18, v19

    move/from16 v0, v18

    int-to-byte v15, v0

    .line 1193
    .local v15, "numberOfFiles":B
    invoke-virtual {v3, v15}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setNumberOfFiles(B)V

    .line 1196
    iget-object v0, v4, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->getBytes()[B

    move-result-object v18

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setNameLength(B)V

    .line 1199
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v4, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setName(Ljava/lang/String;)V

    .line 1202
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1205
    .local v10, "fileMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    if-eqz v2, :cond_7

    .line 1206
    new-instance v9, Lcom/vectorwatch/android/models/FileMetadataModel;

    invoke-direct {v9}, Lcom/vectorwatch/android/models/FileMetadataModel;-><init>()V

    .line 1207
    .local v9, "fileMetadataApp":Lcom/vectorwatch/android/models/FileMetadataModel;
    new-instance v7, Lcom/vectorwatch/android/models/FileIdModel;

    invoke-direct {v7}, Lcom/vectorwatch/android/models/FileIdModel;-><init>()V

    .line 1208
    .local v7, "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    move/from16 v0, p0

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/models/FileIdModel;->setId(I)V

    .line 1209
    sget-object v18, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_APPLICATION:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/models/FileIdModel;->setType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V

    .line 1210
    invoke-virtual {v9, v7}, Lcom/vectorwatch/android/models/FileMetadataModel;->setFileId(Lcom/vectorwatch/android/models/FileIdModel;)V

    .line 1212
    iget-wide v0, v2, Lcom/vectorwatch/android/models/AppFile;->realSize:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-gez v18, :cond_2

    iget-boolean v0, v2, Lcom/vectorwatch/android/models/AppFile;->compressed:Z

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_3

    :cond_2
    iget-object v0, v2, Lcom/vectorwatch/android/models/AppFile;->md5:Ljava/lang/String;

    move-object/from16 v18, v0

    if-nez v18, :cond_6

    .line 1213
    :cond_3
    sget-object v18, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "APP INSTALL MANAGER - problem with fields from cloud "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/AppFile;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " for app = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v4, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1215
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 1191
    .end local v7    # "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    .end local v9    # "fileMetadataApp":Lcom/vectorwatch/android/models/FileMetadataModel;
    .end local v10    # "fileMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    .end local v15    # "numberOfFiles":B
    :cond_4
    const/16 v18, 0x1

    goto/16 :goto_1

    :cond_5
    iget-object v0, v5, Lcom/vectorwatch/android/models/AppContent;->localResources:Ljava/util/List;

    move-object/from16 v19, v0

    .line 1192
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    goto/16 :goto_2

    .line 1218
    .restart local v7    # "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    .restart local v9    # "fileMetadataApp":Lcom/vectorwatch/android/models/FileMetadataModel;
    .restart local v10    # "fileMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    .restart local v15    # "numberOfFiles":B
    :cond_6
    iget-boolean v0, v2, Lcom/vectorwatch/android/models/AppFile;->compressed:Z

    move/from16 v18, v0

    if-eqz v18, :cond_b

    .line 1219
    iget-wide v0, v2, Lcom/vectorwatch/android/models/AppFile;->realSize:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-short v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/vectorwatch/android/models/FileMetadataModel;->setSize(S)V

    .line 1225
    :goto_3
    iget-object v0, v5, Lcom/vectorwatch/android/models/AppContent;->appFile:Lcom/vectorwatch/android/models/AppFile;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppFile;->md5:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/vectorwatch/android/utils/Helpers;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v13

    .line 1226
    .local v13, "md5":[B
    sget-object v18, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "APP INSTALL MANAGER - install details - MD5 received = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v5, Lcom/vectorwatch/android/models/AppContent;->appFile:Lcom/vectorwatch/android/models/AppFile;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppFile;->md5:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " byte array = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    .line 1227
    invoke-static {v13}, Lcom/vectorwatch/android/utils/ByteManipulationUtils;->byteArrayToString([B)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 1226
    invoke-interface/range {v18 .. v19}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1228
    invoke-virtual {v9, v13}, Lcom/vectorwatch/android/models/FileMetadataModel;->setMd5([B)V

    .line 1230
    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1234
    .end local v7    # "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    .end local v9    # "fileMetadataApp":Lcom/vectorwatch/android/models/FileMetadataModel;
    .end local v13    # "md5":[B
    :cond_7
    iget-object v12, v5, Lcom/vectorwatch/android/models/AppContent;->localResources:Ljava/util/List;

    .line 1236
    .local v12, "localResources":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/LocalResource;>;"
    if-eqz v12, :cond_10

    .line 1237
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_8
    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_10

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vectorwatch/android/models/LocalResource;

    .line 1238
    .local v16, "resource":Lcom/vectorwatch/android/models/LocalResource;
    if-eqz v16, :cond_8

    .line 1239
    new-instance v14, Lcom/vectorwatch/android/models/FileMetadataModel;

    invoke-direct {v14}, Lcom/vectorwatch/android/models/FileMetadataModel;-><init>()V

    .line 1240
    .local v14, "metadataModel":Lcom/vectorwatch/android/models/FileMetadataModel;
    sget-object v19, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "APP INSTALL MANAGER - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/LocalResource;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1241
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->decompressedSize:Ljava/lang/Short;

    move-object/from16 v19, v0

    if-nez v19, :cond_9

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->compressed:Ljava/lang/Boolean;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-nez v19, :cond_a

    :cond_9
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->md5:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_a

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    if-gez v19, :cond_d

    .line 1242
    :cond_a
    sget-object v18, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v19, "APP INSTALL MANAGER - null md5 or real size < 0 or undefined id"

    invoke-interface/range {v18 .. v19}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1243
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 1221
    .end local v12    # "localResources":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/LocalResource;>;"
    .end local v14    # "metadataModel":Lcom/vectorwatch/android/models/FileMetadataModel;
    .end local v16    # "resource":Lcom/vectorwatch/android/models/LocalResource;
    .restart local v7    # "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    .restart local v9    # "fileMetadataApp":Lcom/vectorwatch/android/models/FileMetadataModel;
    :cond_b
    iget-object v0, v2, Lcom/vectorwatch/android/models/AppFile;->data:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v6

    .line 1222
    .local v6, "contentBytes":[B
    if-nez v6, :cond_c

    const/16 v17, 0x0

    .line 1223
    .local v17, "size":I
    :goto_5
    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/vectorwatch/android/models/FileMetadataModel;->setSize(S)V

    goto/16 :goto_3

    .line 1222
    .end local v17    # "size":I
    :cond_c
    array-length v0, v6

    move/from16 v17, v0

    goto :goto_5

    .line 1246
    .end local v6    # "contentBytes":[B
    .end local v7    # "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    .end local v9    # "fileMetadataApp":Lcom/vectorwatch/android/models/FileMetadataModel;
    .restart local v12    # "localResources":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/LocalResource;>;"
    .restart local v14    # "metadataModel":Lcom/vectorwatch/android/models/FileMetadataModel;
    .restart local v16    # "resource":Lcom/vectorwatch/android/models/LocalResource;
    :cond_d
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->compressed:Ljava/lang/Boolean;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_e

    .line 1247
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->decompressedSize:Ljava/lang/Short;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Short;->shortValue()S

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/FileMetadataModel;->setSize(S)V

    .line 1253
    :goto_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->md5:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/vectorwatch/android/utils/Helpers;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/FileMetadataModel;->setMd5([B)V

    .line 1254
    new-instance v8, Lcom/vectorwatch/android/models/FileIdModel;

    invoke-direct {v8}, Lcom/vectorwatch/android/models/FileIdModel;-><init>()V

    .line 1255
    .local v8, "fileIdModel":Lcom/vectorwatch/android/models/FileIdModel;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Lcom/vectorwatch/android/models/FileIdModel;->setId(I)V

    .line 1256
    sget-object v19, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_RESOURCE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Lcom/vectorwatch/android/models/FileIdModel;->setType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V

    .line 1257
    invoke-virtual {v14, v8}, Lcom/vectorwatch/android/models/FileMetadataModel;->setFileId(Lcom/vectorwatch/android/models/FileIdModel;)V

    .line 1259
    invoke-interface {v10, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 1249
    .end local v8    # "fileIdModel":Lcom/vectorwatch/android/models/FileIdModel;
    :cond_e
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->content:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {v19 .. v20}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v6

    .line 1250
    .restart local v6    # "contentBytes":[B
    if-nez v6, :cond_f

    const/16 v17, 0x0

    .line 1251
    .restart local v17    # "size":I
    :goto_7
    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/FileMetadataModel;->setSize(S)V

    goto :goto_6

    .line 1250
    .end local v17    # "size":I
    :cond_f
    array-length v0, v6

    move/from16 v17, v0

    goto :goto_7

    .line 1264
    .end local v6    # "contentBytes":[B
    .end local v14    # "metadataModel":Lcom/vectorwatch/android/models/FileMetadataModel;
    .end local v16    # "resource":Lcom/vectorwatch/android/models/LocalResource;
    :cond_10
    invoke-virtual {v3, v10}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->setMetadataList(Ljava/util/List;)V

    .line 1266
    invoke-virtual {v11, v3}, Lcom/vectorwatch/android/models/InstallMessageModel;->setApplicationPackage(Lcom/vectorwatch/android/models/ApplicationPackageModel;)V

    goto/16 :goto_0
.end method

.method public static packInstallMessage(Lcom/vectorwatch/android/models/InstallMessageModel;)[B
    .locals 12
    .param p0, "installMessageModel"    # Lcom/vectorwatch/android/models/InstallMessageModel;

    .prologue
    const/4 v7, 0x0

    .line 1277
    sget-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v9, "APP INSTALL MANAGER - in pack install message"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1278
    invoke-static {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->computeInstallMessageSize(Lcom/vectorwatch/android/models/InstallMessageModel;)I

    move-result v5

    .line 1279
    .local v5, "size":I
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1281
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/InstallMessageModel;->getApplicationPackage()Lcom/vectorwatch/android/models/ApplicationPackageModel;

    move-result-object v0

    .line 1282
    .local v0, "applicationPackage":Lcom/vectorwatch/android/models/ApplicationPackageModel;
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/InstallMessageModel;->getType()Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    move-result-object v6

    .line 1285
    .local v6, "type":Lcom/vectorwatch/android/utils/Constants$MessageInstallType;
    invoke-virtual {v6}, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->getVal()I

    move-result v8

    int-to-byte v8, v8

    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1288
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getAppId()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1291
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getPosition()B

    move-result v8

    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1294
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getNumberOfFiles()B

    move-result v8

    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1297
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getNameLength()B

    move-result v8

    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1300
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1303
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/ApplicationPackageModel;->getMetadataList()Ljava/util/List;

    move-result-object v4

    .line 1305
    .local v4, "fileMetadataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileMetadataModel;>;"
    if-nez v4, :cond_0

    .line 1306
    sget-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v9, "APP INSTALL MANAGER - file metadata list = null in pack install message."

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1331
    :goto_0
    return-object v7

    .line 1310
    :cond_0
    sget-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "APP INSTALL MANAGER - file metadata list size = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1312
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/FileMetadataModel;

    .line 1313
    .local v3, "fileMetadata":Lcom/vectorwatch/android/models/FileMetadataModel;
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/FileMetadataModel;->getFileId()Lcom/vectorwatch/android/models/FileIdModel;

    move-result-object v2

    .line 1314
    .local v2, "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    if-nez v2, :cond_1

    .line 1315
    sget-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v9, "APP INSTALL MANAGER - fileId is null for resource"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 1319
    :cond_1
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/FileIdModel;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->getVal()I

    move-result v9

    int-to-byte v9, v9

    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1321
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/FileIdModel;->getId()I

    move-result v9

    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1323
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/FileMetadataModel;->getSize()S

    move-result v9

    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1325
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/FileMetadataModel;->getMd5()[B

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1327
    sget-object v9, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "APP INSTALL MANAGER - pack - file metadata: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/FileIdModel;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " | "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 1328
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/FileIdModel;->getId()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " | "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/FileMetadataModel;->getSize()S

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1327
    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 1331
    .end local v2    # "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    .end local v3    # "fileMetadata":Lcom/vectorwatch/android/models/FileMetadataModel;
    :cond_2
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    goto/16 :goto_0
.end method

.method public static receivedInstallMessageFromWatch(Lcom/vectorwatch/android/service/ble/messages/DataMessage;)V
    .locals 10
    .param p0, "message"    # Lcom/vectorwatch/android/service/ble/messages/DataMessage;

    .prologue
    const/4 v9, -0x1

    .line 1383
    sget v5, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingAppId:I

    if-ne v5, v9, :cond_1

    .line 1384
    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v6, "APP INSTALL MANAGER - received install message from watch, but there is no current installing element recorded."

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1422
    :cond_0
    :goto_0
    return-void

    .line 1389
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/messages/DataMessage;->getData()[B

    move-result-object v2

    .line 1391
    .local v2, "payload":[B
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1392
    .local v0, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    .line 1395
    .local v1, "installMessageType":B
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_STATUS:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->getVal()I

    move-result v5

    if-ne v1, v5, :cond_3

    .line 1397
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    .line 1399
    .local v4, "status":B
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->INSTALL_STATUS_SUCCESS:Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->getVal()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 1400
    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v6, "APP INSTALL MANAGER - Received app install status success."

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1402
    sget v5, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingAppId:I

    invoke-static {v5}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerInstallFinish(I)V

    goto :goto_0

    .line 1403
    :cond_2
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->INSTALL_STATUS_NO_SPACE:Lcom/vectorwatch/android/utils/Constants$InstallStatus;

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/Constants$InstallStatus;->getVal()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 1404
    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v6, "APP INSTALL MANAGER - Received app install status no space."

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1407
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v5

    new-instance v6, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;

    sget-object v7, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentElementInstalling:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->NOT_ENOUGH_SPACE:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-direct {v6, v7, v8}, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;-><init>(Ljava/lang/String;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 1409
    sput v9, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingAppId:I

    .line 1410
    const/4 v5, 0x0

    sput v5, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingPosition:I

    .line 1411
    const/4 v5, 0x0

    sput-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->sInstallAppFinishedRequestId:Ljava/util/UUID;

    goto :goto_0

    .line 1414
    .end local v4    # "status":B
    :cond_3
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_REQ:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->getVal()I

    move-result v5

    if-ne v1, v5, :cond_0

    .line 1415
    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v6, "APP INSTALL MANAGER - Received app install request."

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1418
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    new-array v3, v5, [B

    .line 1419
    .local v3, "requestInfo":[B
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 1420
    invoke-static {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerSendRequestedFiles([B)V

    goto :goto_0
.end method

.method private static sendFile(IB)V
    .locals 10
    .param p0, "id"    # I
    .param p1, "type"    # B

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v6, 0x1

    .line 1491
    packed-switch p1, :pswitch_data_0

    .line 1524
    :goto_0
    return-void

    .line 1493
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {p0, v0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getDynamicResource(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DynamicResource;

    move-result-object v9

    .line 1494
    .local v9, "resource":Lcom/vectorwatch/android/models/DynamicResource;
    if-nez v9, :cond_0

    .line 1495
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v2, "APP INSTALL MANAGER - missing resource at send resource."

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1496
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v2, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 1499
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "APP INSTALL MANAGER - send file - RESOURCE - compressed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/DynamicResource;->isCompressed()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uncompressed size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1500
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/DynamicResource;->getSize()S

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1499
    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1501
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/DynamicResource;->getContentBytes()[B

    move-result-object v1

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/DynamicResource;->getId()I

    move-result v2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_RESOURCE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 1502
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/DynamicResource;->getSize()S

    move-result v4

    invoke-virtual {v9}, Lcom/vectorwatch/android/models/DynamicResource;->isCompressed()Z

    move-result v5

    .line 1501
    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile([BILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    goto :goto_0

    .line 1505
    .end local v9    # "resource":Lcom/vectorwatch/android/models/DynamicResource;
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {p0, v0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v7

    .line 1506
    .local v7, "app":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getContent()Lcom/vectorwatch/android/models/AppContent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getContent()Lcom/vectorwatch/android/models/AppContent;

    move-result-object v0

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->appFile:Lcom/vectorwatch/android/models/AppFile;

    if-eqz v0, :cond_1

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getContent()Lcom/vectorwatch/android/models/AppContent;

    move-result-object v0

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->appFile:Lcom/vectorwatch/android/models/AppFile;

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppFile;->data:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 1508
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v2, "APP INSTALL MANAGER - missing app/app content/app file/ app file data at send app file."

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1510
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v2, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v2, v3, v4}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1515
    :cond_2
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getContent()Lcom/vectorwatch/android/models/AppContent;

    move-result-object v0

    iget-object v8, v0, Lcom/vectorwatch/android/models/AppContent;->appFile:Lcom/vectorwatch/android/models/AppFile;

    .line 1516
    .local v8, "appFile":Lcom/vectorwatch/android/models/AppFile;
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "APP INSTALL MANAGER - send file - APPFILE - compressed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, v8, Lcom/vectorwatch/android/models/AppFile;->compressed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "uncompressed size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v8, Lcom/vectorwatch/android/models/AppFile;->realSize:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1518
    iget-object v0, v8, Lcom/vectorwatch/android/models/AppFile;->data:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v1

    .line 1519
    .local v1, "dataDecoded":[B
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    iget-object v2, v7, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_APPLICATION:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    iget-wide v4, v8, Lcom/vectorwatch/android/models/AppFile;->realSize:J

    long-to-int v4, v4

    int-to-short v4, v4

    iget-boolean v5, v8, Lcom/vectorwatch/android/models/AppFile;->compressed:Z

    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile([BILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    goto/16 :goto_0

    .line 1491
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private triggerAppListReloadEvent(Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 3
    .param p1, "elem"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 216
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - trigger app list reload = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mEventBus:Lcom/vectorwatch/android/MainThreadBus;

    new-instance v1, Lcom/vectorwatch/android/events/AppListReloadEvent;

    invoke-direct {v1, p1}, Lcom/vectorwatch/android/events/AppListReloadEvent;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 218
    return-void
.end method

.method private triggerAppListReloadEvent(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 3
    .param p1, "elem"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 237
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - trigger app list reload with store elem = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 238
    invoke-static {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->fromStoreElement(Lcom/vectorwatch/android/models/StoreElement;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppListReloadEvent(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 239
    return-void
.end method

.method private static triggerInstallFinish(I)V
    .locals 3
    .param p0, "appId"    # I

    .prologue
    .line 1430
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "APP INSTALL MANAGER - trigger install finished for mAppId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1432
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendInstallFinish(Landroid/content/Context;I)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sInstallAppFinishedRequestId:Ljava/util/UUID;

    .line 1433
    return-void
.end method

.method private static triggerSendRequestedFiles([B)V
    .locals 11
    .param p0, "request"    # [B

    .prologue
    .line 1443
    new-instance v6, Lcom/vectorwatch/android/models/RequestedFilesModel;

    invoke-direct {v6}, Lcom/vectorwatch/android/models/RequestedFilesModel;-><init>()V

    .line 1445
    .local v6, "requestFiles":Lcom/vectorwatch/android/models/RequestedFilesModel;
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1446
    .local v0, "data":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    .line 1447
    .local v5, "numberOfFiles":B
    sget-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "APP INSTALL MANAGER - Requested number of files = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1448
    invoke-virtual {v6, v5}, Lcom/vectorwatch/android/models/RequestedFilesModel;->setNumberOfFiles(B)V

    .line 1450
    sget-object v9, Lcom/vectorwatch/android/utils/AppInstallManager;->sResourcesToSendCount:Ljava/lang/Integer;

    monitor-enter v9

    .line 1451
    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sput-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->sResourcesToSendCount:Ljava/lang/Integer;

    .line 1452
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1454
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1455
    .local v2, "fileIdModelList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileIdModel;>;"
    const/4 v3, 0x0

    .local v3, "i":B
    :goto_0
    if-ge v3, v5, :cond_0

    .line 1456
    new-instance v1, Lcom/vectorwatch/android/models/FileIdModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/FileIdModel;-><init>()V

    .line 1459
    .local v1, "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v7

    .line 1460
    .local v7, "type":B
    packed-switch v7, :pswitch_data_0

    .line 1473
    :goto_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 1474
    .local v4, "id":I
    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/FileIdModel;->setId(I)V

    .line 1476
    sget-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "APP INSTALL MANAGER - file id = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1478
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1480
    invoke-static {v4, v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->sendFile(IB)V

    .line 1455
    add-int/lit8 v8, v3, 0x1

    int-to-byte v3, v8

    goto :goto_0

    .line 1452
    .end local v1    # "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    .end local v2    # "fileIdModelList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileIdModel;>;"
    .end local v3    # "i":B
    .end local v4    # "id":I
    .end local v7    # "type":B
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 1462
    .restart local v1    # "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    .restart local v2    # "fileIdModelList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/FileIdModel;>;"
    .restart local v3    # "i":B
    .restart local v7    # "type":B
    :pswitch_0
    sget-object v8, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_RESOURCE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    invoke-virtual {v1, v8}, Lcom/vectorwatch/android/models/FileIdModel;->setType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V

    .line 1463
    sget-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "APP INSTALL MANAGER - File type = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_RESOURCE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    invoke-virtual {v10}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 1466
    :pswitch_1
    sget-object v8, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_APPLICATION:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    invoke-virtual {v1, v8}, Lcom/vectorwatch/android/models/FileIdModel;->setType(Lcom/vectorwatch/android/utils/Constants$VftpFileType;)V

    .line 1467
    sget-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "APP INSTALL MANAGER - File type = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_APPLICATION:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    .line 1468
    invoke-virtual {v10}, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1467
    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 1482
    .end local v1    # "fileId":Lcom/vectorwatch/android/models/FileIdModel;
    .end local v7    # "type":B
    :cond_0
    return-void

    .line 1460
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private triggerStreamListReloadEvent(Lcom/vectorwatch/android/models/Stream;)V
    .locals 3
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 226
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - trigger stream list reload = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mEventBus:Lcom/vectorwatch/android/MainThreadBus;

    new-instance v1, Lcom/vectorwatch/android/events/StreamListReloadEvent;

    invoke-direct {v1, p1}, Lcom/vectorwatch/android/events/StreamListReloadEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 228
    return-void
.end method

.method private static triggerVibrateAndJumpToFace(I)V
    .locals 9
    .param p0, "applicationId"    # I

    .prologue
    const/4 v4, 0x1

    .line 1552
    move v1, p0

    .line 1553
    .local v1, "appId":I
    const/4 v7, 0x1

    .line 1554
    .local v7, "force":B
    const/4 v2, 0x0

    .line 1555
    .local v2, "watchFaceId":B
    const/4 v3, 0x0

    .line 1556
    .local v3, "animationType":B
    const/4 v8, 0x1

    .line 1558
    .local v8, "vibration":B
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1559
    .local v6, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v6, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1560
    invoke-virtual {v6, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1561
    invoke-virtual {v6, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1562
    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1563
    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1565
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    move v5, v4

    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendChangeActiveAppRequest(Landroid/content/Context;IIIZZ)Ljava/util/UUID;

    .line 1566
    return-void
.end method


# virtual methods
.method public getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentElementInstalling:Lcom/vectorwatch/android/models/CloudElementSummary;

    return-object v0
.end method

.method public handleAppInstallInterrupted(Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const v3, 0x7f090114

    const/16 v2, 0xbb8

    .line 918
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v1, "INSTALL MNGR - app install interrupted"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 919
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;->getCrtInstalling()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 920
    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;->getCrtInstalling()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 922
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 923
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager$8;->$SwitchMap$com$vectorwatch$android$utils$Constants$AppInstallFailReasons:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;->getType()Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 946
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;->getType()Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->cancelAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 948
    :cond_1
    return-void

    .line 925
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 930
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const v1, 0x7f090111

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 935
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const v1, 0x7f090221

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 940
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 923
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public handleAppInstalledOnWatchEvent(Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;)V
    .locals 9
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 957
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;->getAppId()I

    move-result v0

    .line 959
    .local v0, "appId":I
    sget-object v3, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    .line 960
    invoke-static {v0, v3}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppSummaryWithId(ILandroid/content/Context;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v3

    sget v4, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingPosition:I

    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    .line 959
    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/managers/CloudAppsManager;->installAppOnWatchFinished(Lcom/vectorwatch/android/models/CloudElementSummary;ILandroid/content/Context;)V

    .line 962
    sput v7, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingAppId:I

    .line 963
    sput v8, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingPosition:I

    .line 964
    sput-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->sInstallAppFinishedRequestId:Ljava/util/UUID;

    .line 967
    sget-object v3, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v2

    .line 969
    .local v2, "fullApp":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    new-instance v1, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 970
    .local v1, "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    if-eqz v2, :cond_4

    .line 971
    iget-object v3, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->description:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setDescription(Ljava/lang/String;)V

    .line 972
    iget-object v3, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->editImg:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setEditImg(Ljava/lang/String;)V

    .line 973
    iget-object v3, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 974
    iget-object v3, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->img:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setImage(Ljava/lang/String;)V

    .line 975
    iget-object v3, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setName(Ljava/lang/String;)V

    .line 976
    iget-object v3, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->appType:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setType(Ljava/lang/String;)V

    .line 977
    iget v3, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->rating:F

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRating(F)V

    .line 978
    iget-object v3, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 985
    sget-object v3, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v4, "INSTALL MNGR - app installed on watch"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 986
    iget-boolean v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    if-nez v3, :cond_0

    if-eqz v2, :cond_2

    .line 987
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 988
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 989
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpDefaultsOnWatchface(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)V

    .line 990
    sget v3, Lcom/vectorwatch/android/VectorApplication;->sCorrectlyInstalledApps:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/vectorwatch/android/VectorApplication;->sCorrectlyInstalledApps:I

    .line 991
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/InstallAppEvent;

    const/4 v5, 0x1

    invoke-direct {v4, v5, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 992
    sget v3, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    if-eqz v3, :cond_1

    sget v3, Lcom/vectorwatch/android/VectorApplication;->sCorrectlyInstalledApps:I

    sget v4, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    if-ne v3, v4, :cond_1

    .line 993
    const-string v3, "flag_sync_system_apps"

    sget-object v4, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v3, v8, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 997
    :cond_1
    iget-boolean v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    if-nez v3, :cond_2

    .line 998
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->handleWatchFaceTutorial(Lcom/vectorwatch/android/models/DownloadedAppDetails;)V

    .line 1002
    :cond_2
    sget-object v3, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v4, "APP INSTALL MANAGER - app installed on watch."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1003
    iget-boolean v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1004
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v3

    .line 1005
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1006
    sget-object v3, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v4, "APP INSTALL MANAGER - app installed on watch - reset currently installing."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1007
    invoke-virtual {p0, v6, v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 1008
    invoke-direct {p0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppListReloadEvent(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 1011
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v3

    invoke-static {v3}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerVibrateAndJumpToFace(I)V

    .line 1013
    :cond_3
    :goto_0
    return-void

    .line 980
    :cond_4
    sget-object v3, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "App install manager - Full app is null "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 981
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "App install manager - Full app is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/crashlytics/android/Crashlytics;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleAppUninstalledOnWatchEvent(Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1022
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v1, "INSTALL MNGR - app uninstalled on watch"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1023
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    if-nez v0, :cond_0

    .line 1024
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;->getElementSummary()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;->getElementSummary()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1025
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;->getElementSummary()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppListReloadEvent(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 1027
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;->getElementSummary()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1028
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/CookieUtils;->clearWebviewCookies(Landroid/content/Context;)V

    .line 1032
    :cond_0
    return-void
.end method

.method public handleCommandStatusEvent(Lcom/vectorwatch/android/events/CommandStatusEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/vectorwatch/android/events/CommandStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 1570
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "APP INSTALL MANAGER: Command status received for uuid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getCommandUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "currently "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "sending for uuid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->sInstallAppFinishedRequestId:Ljava/util/UUID;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1572
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getStatus()Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1570
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1573
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "APP INSTALL MANAGER: installAppFinishes = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->sInstallAppFinishedRequestId:Ljava/util/UUID;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/vectorwatch/android/utils/AppInstallManager;->sUninstallAppId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1574
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getStatus()Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/events/CommandStatusEvent$Status;->SUCCESS:Lcom/vectorwatch/android/events/CommandStatusEvent$Status;

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getCommandUuid()Ljava/util/UUID;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1576
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sInstallAppFinishedRequestId:Ljava/util/UUID;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sInstallAppFinishedRequestId:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1577
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getCommandUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1576
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1578
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v1, "APP INSTALL MANAGER: Received app install finished."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1580
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;

    sget v2, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingAppId:I

    sget v3, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingPosition:I

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 1584
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sUninstallRequestId:Ljava/util/UUID;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sUninstallRequestId:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CommandStatusEvent;->getCommandUuid()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1585
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v1, "APP INSTALL MANAGER: Received app uninstall finished."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1586
    sget v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sUninstallAppId:I

    if-eq v0, v4, :cond_2

    .line 1587
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AppUninstallCommandSentEvent;

    sget v2, Lcom/vectorwatch/android/utils/AppInstallManager;->sUninstallAppId:I

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/AppUninstallCommandSentEvent;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 1588
    sput v4, Lcom/vectorwatch/android/utils/AppInstallManager;->sUninstallAppId:I

    .line 1594
    :cond_1
    :goto_0
    return-void

    .line 1590
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v1, "APP INSTALL MANAGER: The uninstall app id is -1 at callback."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleFullAppDetailsSavedToDbEvent(Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1041
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v3, "INSTALL MNGR - app details saved to db"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1042
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "APP INSTALL MANAGER - HANDLE SAVE TO DB for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;->getApp()Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v4

    iget-object v4, v4, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " position = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;->getPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1043
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;->getApp()Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v0

    .line 1044
    .local v0, "app":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    new-instance v1, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 1045
    .local v1, "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->appType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setType(Ljava/lang/String;)V

    .line 1046
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setName(Ljava/lang/String;)V

    .line 1047
    iget v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->rating:F

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRating(F)V

    .line 1048
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->img:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setImage(Ljava/lang/String;)V

    .line 1049
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 1050
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->editImg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setEditImg(Ljava/lang/String;)V

    .line 1051
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->description:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setDescription(Ljava/lang/String;)V

    .line 1052
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 1053
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRequireUserAuth(Z)V

    .line 1054
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getRequireLocation()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRequireLocation(Z)V

    .line 1055
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->hasDynamicSettings()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setHasDynamicSettings(Z)V

    .line 1056
    iget-boolean v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    .line 1057
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1059
    :cond_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/FullAppDetailsSavedToDbEvent;->getPosition()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 1062
    :cond_1
    iget-boolean v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1063
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->startConfiguringAppCredentials(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 1068
    :goto_0
    return-void

    .line 1066
    :cond_2
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 904
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 905
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 906
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->cancelAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 909
    :cond_0
    return-void
.end method

.method public handleStreamDeletedEvent(Lcom/vectorwatch/android/events/StreamDeletedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamDeletedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1100
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    if-nez v0, :cond_0

    .line 1101
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamDeletedEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerStreamListReloadEvent(Lcom/vectorwatch/android/models/Stream;)V

    .line 1102
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 1104
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamDeletedEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamDeletedEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/CookieUtils;->clearWebviewCookies(Landroid/content/Context;)V

    .line 1109
    :cond_0
    return-void
.end method

.method public handleStreamDownloadCompletedEvent(Lcom/vectorwatch/android/events/StreamDownloadCompletedEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamDownloadCompletedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1077
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamDownloadCompletedEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    .line 1078
    .local v0, "stream":Lcom/vectorwatch/android/models/Stream;
    iget-boolean v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    if-nez v1, :cond_0

    .line 1079
    if-eqz v0, :cond_0

    .line 1080
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1081
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v2, "INSTALL MNGR - stream req auth"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1082
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->startConfiguringStreamCredentials(Lcom/vectorwatch/android/models/Stream;)V

    .line 1091
    :cond_0
    :goto_0
    return-void

    .line 1084
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v2, "INSTALL MNGR - stream doens\'t req auth"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1085
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerStreamListReloadEvent(Lcom/vectorwatch/android/models/Stream;)V

    .line 1086
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->handleStreamTutorial(Lcom/vectorwatch/android/models/Stream;)V

    .line 1087
    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    goto :goto_0
.end method

.method public handleStreamTutorial(Lcom/vectorwatch/android/models/Stream;)V
    .locals 4
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    const/4 v3, 0x1

    .line 872
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    if-nez v1, :cond_1

    .line 873
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v2, "Current activity null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 891
    :cond_0
    :goto_0
    return-void

    .line 877
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const-string v2, "flag_show_watchface_stream_tutorial"

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getFlagTutorial(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 878
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const-class v2, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 879
    .local v0, "resultIntent":Landroid/content/Intent;
    const-string v1, "stream_name"

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 880
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const-string v2, "flag_show_stream_tutorial"

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getFlagTutorial(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 882
    sput-boolean v3, Lcom/vectorwatch/android/VectorApplication;->sShowStreamDropped:Z

    .line 883
    const-string v1, "show_stream_dropped"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 885
    :cond_2
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const-string v2, "flag_show_watchface_stream_tutorial"

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getFlagTutorial(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 887
    const-string v1, "show_stream_tutorial"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 889
    :cond_3
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public handleVftpFileTransferredToWatchCompletedEvent(Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1528
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/VftpFileTransferredToWatchCompletedEvent;->getFile()Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/transfer_manager/VftpFile;->getType()Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    if-ne v0, v1, :cond_0

    .line 1530
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v1, "APP INSTALL MANAGER - Received finish for localization while trying to install apps."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 1543
    :goto_0
    return-void

    .line 1534
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v1, "APP INSTALL MANAGER - received VFTP file transferred to watch."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1535
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->sResourcesToSendCount:Ljava/lang/Integer;

    monitor-enter v1

    .line 1536
    :try_start_0
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sResourcesToSendCount:Ljava/lang/Integer;

    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sResourcesToSendCount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sResourcesToSendCount:Ljava/lang/Integer;

    .line 1537
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "APP INSTALL - Vftp transferred - remaining resources = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/AppInstallManager;->sResourcesToSendCount:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1539
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sResourcesToSendCount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 1540
    sget v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingAppId:I

    invoke-static {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerInstallFinish(I)V

    .line 1542
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public handleWatchFaceTutorial(Lcom/vectorwatch/android/models/DownloadedAppDetails;)V
    .locals 8
    .param p1, "app"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .prologue
    const/4 v7, 0x1

    .line 822
    const/4 v0, 0x0

    .line 823
    .local v0, "found":Z
    iget-object v5, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->appType:Ljava/lang/String;

    const-string v6, "WATCHFACE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 824
    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const-string v6, "flag_show_watchface_tutorial"

    invoke-static {v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getFlagTutorial(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const-string v6, "flag_show_stream_tutorial"

    .line 826
    invoke-static {v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getFlagTutorial(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 827
    :cond_0
    iget-object v5, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-eqz v5, :cond_3

    iget-object v5, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v5, v5, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    if-eqz v5, :cond_3

    .line 828
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v5, v5, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 829
    iget-object v5, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v5, v5, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 830
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget-object v5, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v5, v5, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 831
    iget-object v5, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v5, v5, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "TEXT_ELEMENT"

    .line 832
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p1, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v5, v5, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Watchface;

    .line 833
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "LONG_TEXT_ELEMENT"

    .line 834
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 835
    :cond_1
    const/4 v0, 0x1

    .line 840
    .end local v2    # "j":I
    :cond_2
    if-eqz v0, :cond_8

    .line 846
    .end local v1    # "i":I
    :cond_3
    if-eqz v0, :cond_6

    .line 847
    const/4 v4, 0x0

    .line 848
    .local v4, "tutorialNeeded":Z
    new-instance v3, Landroid/content/Intent;

    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const-class v6, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 849
    .local v3, "resultIntent":Landroid/content/Intent;
    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const-string v6, "flag_show_stream_tutorial"

    invoke-static {v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getFlagTutorial(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 851
    const-string v5, "show_stream_dropped"

    invoke-virtual {v3, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 853
    :cond_4
    sget-object v5, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const-string v6, "flag_show_watchface_tutorial"

    invoke-static {v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getFlagTutorial(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 855
    const/4 v4, 0x1

    .line 856
    const-string v5, "show_watchface_tutorial"

    invoke-virtual {v3, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 859
    :cond_5
    if-eqz v4, :cond_6

    .line 860
    const/high16 v5, 0x4000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 861
    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v5, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 864
    .end local v3    # "resultIntent":Landroid/content/Intent;
    .end local v4    # "tutorialNeeded":Z
    :cond_6
    return-void

    .line 830
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 828
    .end local v2    # "j":I
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public installAppFromStore(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 6
    .param p1, "element"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    const/4 v5, 0x0

    .line 247
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - install app from store = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | needs location = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getRequireLocation()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getRequireLocation()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f09017c

    .line 250
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const v2, 0x7f0900a7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 251
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090079

    new-instance v2, Lcom/vectorwatch/android/utils/AppInstallManager$3;

    invoke-direct {v2, p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager$3;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/StoreElement;)V

    .line 252
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09007b

    new-instance v2, Lcom/vectorwatch/android/utils/AppInstallManager$2;

    invoke-direct {v2, p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager$2;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/StoreElement;)V

    .line 259
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/utils/AppInstallManager$1;

    invoke-direct {v1, p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager$1;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/StoreElement;)V

    .line 265
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027

    .line 272
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 273
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 278
    :goto_0
    return-void

    .line 275
    :cond_0
    new-instance v0, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;-><init>(Ljava/lang/String;Landroid/app/Activity;)V

    new-array v1, v5, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/DownloadFullStoreElementAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 276
    invoke-static {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->fromStoreElement(Lcom/vectorwatch/android/models/StoreElement;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    goto :goto_0
.end method

.method public installAppOffline(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/models/cloud/AppsOption;)V
    .locals 5
    .param p1, "element"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "appsOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;

    .prologue
    const/4 v4, 0x0

    .line 287
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Install MNGR - install app in offline "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 288
    invoke-static {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->fromStoreElement(Lcom/vectorwatch/android/models/StoreElement;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 290
    sget-object v1, Lcom/vectorwatch/android/models/cloud/AppsOption;->PLACEHOLDER:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {p2, v1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 291
    new-instance v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/DownloadedAppDetails;-><init>()V

    .line 292
    .local v0, "downloadedAppDetails":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    .line 293
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    .line 294
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->appType:Ljava/lang/String;

    .line 295
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    .line 296
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->img:Ljava/lang/String;

    .line 297
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getEditImage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->editImg:Ljava/lang/String;

    .line 299
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->deleteStreamsCoommand(Landroid/content/Context;I)Ljava/util/UUID;

    .line 304
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v0, v4, v1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->saveAppToDatabase(Lcom/vectorwatch/android/models/DownloadedAppDetails;ILandroid/content/Context;)V

    .line 305
    return-void

    .line 301
    .end local v0    # "downloadedAppDetails":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    .line 302
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    .line 301
    invoke-static {v1, p2, v2}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineAppInstallDetails(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;Ljava/lang/String;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v0

    .restart local v0    # "downloadedAppDetails":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    goto :goto_0
.end method

.method public installStreamFromStore(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 4
    .param p1, "element"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 324
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - instal stream from store = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 327
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->MOBILE_APPLICATION_ERROR:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/utils/asyncTasks/AppInstallInterruptedEvent;-><init>(Ljava/lang/String;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 329
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v1, "Stream other error at download!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 363
    :goto_0
    return-void

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/utils/AppInstallManager$4;

    invoke-direct {v2, p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager$4;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/StoreElement;)V

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->downloadStream(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V

    .line 362
    invoke-static {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->fromStoreElement(Lcom/vectorwatch/android/models/StoreElement;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    goto :goto_0
.end method

.method public installStreamOffline(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 3
    .param p1, "element"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 313
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Install MNGR - install stream in offline "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 314
    invoke-static {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->fromStoreElement(Lcom/vectorwatch/android/models/StoreElement;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 315
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineStream(Landroid/content/Context;Lcom/vectorwatch/android/models/StoreElement;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/StreamsManager;->saveStreamToDatabase(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;)Z

    .line 316
    return-void
.end method

.method public isInSetupMode()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    return v0
.end method

.method public isInstalling()Z
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInstalling:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 690
    const/4 v6, -0x1

    if-ne p2, v6, :cond_a

    if-eqz p3, :cond_a

    .line 691
    packed-switch p1, :pswitch_data_0

    .line 814
    :goto_0
    return-void

    .line 693
    :pswitch_0
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v7, "INSTALL MNGR - on result ok - app_auth"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 697
    const-string v6, "oauth_version"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 698
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 700
    .local v0, "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 701
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 702
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->hasDynamicSettings()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 703
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->startConfiguringAppSettings(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 721
    .end local v0    # "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    :cond_0
    :goto_1
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    goto :goto_0

    .line 705
    .restart local v0    # "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    :cond_1
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    goto :goto_1

    .line 708
    :cond_2
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppListReloadEvent(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    goto :goto_1

    .line 712
    .end local v0    # "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    :cond_3
    const-string v6, "error_msg"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 713
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 714
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 715
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const v7, 0x7f0901a4

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0xbb8

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v6, v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_1

    .line 718
    :cond_4
    const-string v6, "error_msg"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0xbb8

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v6, v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_1

    .line 724
    :pswitch_1
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v7, "INSTALL MNGR - on result ok - app_settings"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 728
    const-string v6, "result_map"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "app_uuid"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 729
    const-string v6, "result_map"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 731
    .local v3, "result":Landroid/os/Bundle;
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 733
    .local v4, "settingNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 734
    .local v1, "appSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v4, :cond_5

    .line 735
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 737
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 738
    .local v5, "values":[Ljava/lang/String;
    const/4 v7, 0x1

    aget-object v7, v5, v7

    invoke-interface {v1, v2, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 743
    .end local v2    # "key":Ljava/lang/String;
    .end local v5    # "values":[Ljava/lang/String;
    :cond_5
    const-string v6, "app_uuid"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v6, v7}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppSummaryWithUuid(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    .line 744
    .restart local v0    # "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v6

    sget-object v7, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v1, v6, v7}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->updateAppUserSettings(Ljava/util/Map;ILandroid/content/Context;)V

    .line 745
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v6

    .line 746
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 747
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 750
    .end local v0    # "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    .end local v1    # "appSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "result":Landroid/os/Bundle;
    .end local v4    # "settingNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_6
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    goto/16 :goto_0

    .line 753
    :pswitch_2
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v7, "INSTALL MNGR - on result ok - stream_auth"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 757
    const-string v6, "oauth_version"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 758
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    if-eqz v6, :cond_7

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 759
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 760
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    invoke-direct {p0, v6}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerStreamListReloadEvent(Lcom/vectorwatch/android/models/Stream;)V

    .line 761
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/utils/AppInstallManager;->handleStreamTutorial(Lcom/vectorwatch/android/models/Stream;)V

    .line 762
    const/4 v6, 0x0

    const/4 v7, -0x1

    invoke-virtual {p0, v6, v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 771
    :cond_7
    :goto_3
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    goto/16 :goto_0

    .line 765
    :cond_8
    const-string v6, "error_msg"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 766
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const v7, 0x7f0901a9

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0xbb8

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v6, v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_3

    .line 768
    :cond_9
    const-string v6, "error_msg"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0xbb8

    sget-object v8, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v6, v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_3

    .line 775
    :cond_a
    packed-switch p1, :pswitch_data_1

    goto/16 :goto_0

    .line 788
    :pswitch_3
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v7, "INSTALL MNGR - on result - app_settings"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 792
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const v7, 0x7f0901a5

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0xbb8

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v6, v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 793
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v6

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v6, :cond_b

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentElementInstalling:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 794
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    sget-object v7, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-direct {p0, v6, v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->cancelAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 796
    :cond_b
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    goto/16 :goto_0

    .line 777
    :pswitch_4
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v7, "INSTALL MNGR - on result - app_auth"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 781
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const v7, 0x7f0901a4

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0xbb8

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v6, v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 782
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v6

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentElementInstalling:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 783
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    sget-object v7, Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;->UNKNOWN:Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;

    invoke-direct {p0, v6, v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->cancelAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;)V

    .line 785
    :cond_c
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    goto/16 :goto_0

    .line 799
    :pswitch_5
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v7, "INSTALL MNGR - on result - stream_auth"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 803
    sget-object v6, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    const v7, 0x7f0901a9

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0xbb8

    iget-object v8, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-static {v6, v7, v8}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 804
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    if-eqz v6, :cond_d

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v6

    if-eqz v6, :cond_d

    .line 805
    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/AppInstallManager;->getCurrentElementInstalling()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 806
    const/4 v6, 0x0

    const/4 v7, -0x1

    invoke-virtual {p0, v6, v7}, Lcom/vectorwatch/android/utils/AppInstallManager;->setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 807
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    invoke-direct {p0, v6}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerStreamListReloadEvent(Lcom/vectorwatch/android/models/Stream;)V

    .line 808
    iget-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/utils/AppInstallManager;->handleStreamTutorial(Lcom/vectorwatch/android/models/Stream;)V

    .line 810
    :cond_d
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    goto/16 :goto_0

    .line 691
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 775
    :pswitch_data_1
    .packed-switch 0x65
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 400
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getRequireLocation()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/vectorwatch/android/utils/LocationHelper;->STARTED_LOCATION_SETTINGS_ACTIVITY:Z

    if-eqz v0, :cond_0

    .line 401
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - on resume - crt app setting up "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 402
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vectorwatch/android/utils/LocationHelper;->STARTED_LOCATION_SETTINGS_ACTIVITY:Z

    .line 403
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->startConfiguringAppSettings(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 405
    :cond_0
    return-void
.end method

.method public setApplicationContext(Landroid/content/Context;)Lcom/vectorwatch/android/utils/AppInstallManager;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 141
    sput-object p1, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    .line 142
    return-object p0
.end method

.method public setCurrentActivity(Landroid/app/Activity;)Lcom/vectorwatch/android/utils/AppInstallManager;
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    .line 134
    return-object p0
.end method

.method public setCurrentElementInstall(Lcom/vectorwatch/android/models/CloudElementSummary;I)Lcom/vectorwatch/android/utils/AppInstallManager;
    .locals 1
    .param p1, "currentApp"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p2, "position"    # I

    .prologue
    .line 192
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInstalling:Z

    .line 193
    sput-object p1, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentElementInstalling:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 194
    sput p2, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingPosition:I

    .line 195
    return-object p0

    .line 192
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEventBus(Lcom/vectorwatch/android/MainThreadBus;)Lcom/vectorwatch/android/utils/AppInstallManager;
    .locals 0
    .param p1, "eventBus"    # Lcom/vectorwatch/android/MainThreadBus;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mEventBus:Lcom/vectorwatch/android/MainThreadBus;

    .line 150
    return-object p0
.end method

.method public setIsInSetupMode(Z)Lcom/vectorwatch/android/utils/AppInstallManager;
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    .line 205
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - set is in setup = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 206
    iput-boolean p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mIsInSetupMode:Z

    .line 207
    return-object p0
.end method

.method public setProgressBar(Landroid/view/View;)Lcom/vectorwatch/android/utils/AppInstallManager;
    .locals 0
    .param p1, "progressBar"    # Landroid/view/View;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mProgressBar:Landroid/view/View;

    .line 158
    return-object p0
.end method

.method public startConfiguringAppCredentials(Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 4
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 465
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - stat config app cred: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 466
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppAuthenticating:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 467
    new-instance v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;

    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    new-instance v2, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;

    invoke-direct {v2, p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager$AppAuthMethodStatusListener;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)V

    iget-object v3, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mProgressBar:Landroid/view/View;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;Landroid/view/View;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppAuthMethodAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 468
    return-void
.end method

.method public startConfiguringAppSettings(Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 6
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 618
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentAppSettingUp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 619
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getRequireLocation()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 620
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "INSTALL MNGR - config app settings - app needs location = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 621
    new-instance v0, Lcom/vectorwatch/android/utils/LocationHelper;

    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/vectorwatch/android/utils/LocationHelper;-><init>(Landroid/content/Context;)V

    .line 622
    .local v0, "locationHelper":Lcom/vectorwatch/android/utils/LocationHelper;
    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/utils/LocationHelper;->setFeedbackContext(Landroid/content/Context;)Lcom/vectorwatch/android/utils/LocationHelper;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;->NORMAL_WIFI:Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/utils/LocationHelper;->setRequestAccuracy(Lcom/vectorwatch/android/utils/LocationHelper$RequestAccuracy;)Lcom/vectorwatch/android/utils/LocationHelper;

    .line 623
    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 624
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "INSTALL MNGR - config app settings - location available for app = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 625
    new-instance v1, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    new-instance v3, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;

    invoke-direct {v3, p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)V

    iget-object v4, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mProgressBar:Landroid/view/View;

    invoke-direct {v1, p1, v2, v3, v4}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;Landroid/view/View;)V

    .line 626
    .local v1, "task":Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;
    new-instance v2, Lcom/vectorwatch/android/utils/AppInstallManager$6;

    invoke-direct {v2, p0, p1, v0, v1}, Lcom/vectorwatch/android/utils/AppInstallManager$6;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/LocationHelper;Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;)V

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/utils/LocationHelper;->startLocationUpdates(Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;)Lcom/vectorwatch/android/utils/LocationHelper;

    .line 679
    .end local v0    # "locationHelper":Lcom/vectorwatch/android/utils/LocationHelper;
    .end local v1    # "task":Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;
    :goto_0
    return-void

    .line 662
    .restart local v0    # "locationHelper":Lcom/vectorwatch/android/utils/LocationHelper;
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "INSTALL MNGR - config app settings - location not available for = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 663
    iget-object v2, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    new-instance v3, Lcom/vectorwatch/android/utils/AppInstallManager$7;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/utils/AppInstallManager$7;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;)V

    invoke-static {p1, v2, v3}, Lcom/vectorwatch/android/utils/LocationHelper;->triggerGPSDisabledDialogWithApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 676
    .end local v0    # "locationHelper":Lcom/vectorwatch/android/utils/LocationHelper;
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "INSTALL MNGR - config app settings - app doesn\'t need location = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 677
    new-instance v2, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;

    sget-object v3, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    new-instance v4, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;

    invoke-direct {v4, p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager$AppPossibleSettingsStatusListener;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/CloudElementSummary;)V

    iget-object v5, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mProgressBar:Landroid/view/View;

    invoke-direct {v2, p1, v3, v4, v5}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;Lcom/vectorwatch/android/ui/listeners/SimpleBooleanListener;Landroid/view/View;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppPossibleSettingsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public startConfiguringStreamCredentials(Lcom/vectorwatch/android/models/Stream;)V
    .locals 4
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 534
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "INSTALL MNGR - start config stream cred "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 535
    iput-object p1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentStreamAuthenticating:Lcom/vectorwatch/android/models/Stream;

    .line 536
    new-instance v0, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;

    invoke-direct {v0, p0, p1}, Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/models/Stream;)V

    .line 537
    .local v0, "listener":Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 539
    sget-object v1, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    const-string v2, "INSTALL MNGR - Activity context is null"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 540
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090102

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 541
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 559
    :goto_0
    return-void

    .line 543
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/utils/AppInstallManager;->mCurrentActivity:Landroid/app/Activity;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/utils/AppInstallManager$5;

    invoke-direct {v3, p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager$5;-><init>(Lcom/vectorwatch/android/utils/AppInstallManager;Lcom/vectorwatch/android/utils/AppInstallManager$StreamAuthMethodStatusListener;)V

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getStreamAuthMethod(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V

    goto :goto_0
.end method

.method public triggerAppInstall(Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 3
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 371
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INSTALL MNGR - trigger app install "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 372
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->isAppInstalled(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/utils/AppInstallManager;->triggerInstallApp(I)V

    .line 375
    :cond_0
    return-void
.end method

.method public triggerInstallApp(I)V
    .locals 7
    .param p1, "appId"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 1119
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "APP INSTALL MANAGER: sending  app install command for app = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1121
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {p1, v2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v0

    .line 1123
    .local v0, "cloudAppInstallObject":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    if-nez v0, :cond_0

    .line 1124
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v3, v5, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 1140
    :goto_0
    return-void

    .line 1128
    :cond_0
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-nez v2, :cond_1

    .line 1129
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "INSTALL_V2: app content is null + app id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1130
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v3, v5, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 1135
    :cond_1
    invoke-static {p1}, Lcom/vectorwatch/android/utils/AppInstallManager;->getInstallDetails(I)Lcom/vectorwatch/android/models/InstallMessageModel;

    move-result-object v1

    .line 1137
    .local v1, "installMessageModel":Lcom/vectorwatch/android/models/InstallMessageModel;
    sget-object v2, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAppInstall(Landroid/content/Context;Lcom/vectorwatch/android/models/InstallMessageModel;)Ljava/util/UUID;

    .line 1139
    sput p1, Lcom/vectorwatch/android/utils/AppInstallManager;->sCurrentInstallingAppId:I

    goto :goto_0
.end method

.method public triggerUninstallApp(I)V
    .locals 3
    .param p1, "appId"    # I

    .prologue
    .line 1149
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "APP INSTALL MANAGER - trigger uninstall finished for mAppId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1151
    sput p1, Lcom/vectorwatch/android/utils/AppInstallManager;->sUninstallAppId:I

    .line 1152
    sget-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->mApplicationContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAppUninstall(Landroid/content/Context;I)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/AppInstallManager;->sUninstallRequestId:Ljava/util/UUID;

    .line 1153
    return-void
.end method
