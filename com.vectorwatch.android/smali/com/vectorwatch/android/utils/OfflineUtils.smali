.class public Lcom/vectorwatch/android/utils/OfflineUtils;
.super Ljava/lang/Object;
.source "OfflineUtils.java"


# static fields
.field private static final ASSET_ROUND_APPS:Ljava/lang/String; = "offline/OfflineRoundApps.json"

.field private static final ASSET_ROUND_BOOTLOADER:Ljava/lang/String; = "vos/VECTOR_ROUND_BOOT"

.field private static final ASSET_ROUND_KERNEL:Ljava/lang/String; = "vos/VECTOR_ROUND_KERNEL"

.field private static final ASSET_ROUND_WATCHFACE:Ljava/lang/String; = "offline/OfflineRoundWatchfaces.json"

.field private static final ASSET_SQUARE_APPS:Ljava/lang/String; = "offline/OfflineSquareApps.json"

.field private static final ASSET_SQUARE_BOOTLOADER:Ljava/lang/String; = "vos/VECTOR_SQUARE_BOOT"

.field private static final ASSET_SQUARE_KERNEL:Ljava/lang/String; = "vos/VECTOR_SQUARE_KERNEL"

.field private static final ASSET_SQUARE_WATCHFACE:Ljava/lang/String; = "offline/OfflineSquareWatchfaces.json"

.field private static final ASSET_STREAMS:Ljava/lang/String; = "offline/OfflineStreams.json"

.field private static final DIGITAL_STACK_ROUND:I = 0x1118

.field private static final DIGITAL_STACK_SQUARE:I = 0x112b

.field private static final WATCH_TYPE_BOOTLOADER:Ljava/lang/String; = "bootloader"

.field private static final WATCH_TYPE_KERNEL:Ljava/lang/String; = "kernel"

.field private static final log:Lorg/slf4j/Logger;

.field private static final sBlackList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 71
    const-class v0, Lcom/vectorwatch/android/utils/OfflineUtils;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/OfflineUtils;->log:Lorg/slf4j/Logger;

    .line 73
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0xb0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, 0xb1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const/16 v3, 0xb2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/16 v3, 0xb3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const/16 v3, 0x7e7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const/16 v3, 0x7ed

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const/16 v3, 0x7ee

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0x809

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/vectorwatch/android/utils/OfflineUtils;->sBlackList:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    return-void
.end method

.method public static checkCompatibilityOffline(Landroid/content/Context;Lcom/vectorwatch/com/android/vos/update/SystemInfo;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "info"    # Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .prologue
    .line 260
    invoke-virtual {p1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v0

    .line 261
    .local v0, "infoWatch":Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;
    invoke-static {}, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->getKernelSystemInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    .line 263
    .local v2, "offlineInfo":Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;
    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v7

    mul-int/lit8 v7, v7, 0x64

    add-int/2addr v6, v7

    .line 264
    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v7

    mul-int/lit8 v7, v7, 0xa

    add-int/2addr v6, v7

    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v7

    add-int v5, v6, v7

    .line 266
    .local v5, "watchKernelNumber":I
    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v7

    mul-int/lit8 v7, v7, 0x64

    add-int/2addr v6, v7

    .line 267
    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v7

    mul-int/lit8 v7, v7, 0xa

    add-int/2addr v6, v7

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v7

    add-int v3, v6, v7

    .line 269
    .local v3, "offlineKernelNumber":I
    invoke-virtual {p1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v0

    .line 270
    invoke-static {}, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->getBootloaderSystemInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    .line 272
    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v7

    mul-int/lit8 v7, v7, 0x64

    add-int/2addr v6, v7

    .line 273
    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v7

    mul-int/lit8 v7, v7, 0xa

    add-int/2addr v6, v7

    invoke-virtual {v0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v7

    add-int v4, v6, v7

    .line 275
    .local v4, "watchBootloaderNumber":I
    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v7

    mul-int/lit8 v7, v7, 0x64

    add-int/2addr v6, v7

    .line 276
    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v7

    mul-int/lit8 v7, v7, 0xa

    add-int/2addr v6, v7

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v7

    add-int v1, v6, v7

    .line 278
    .local v1, "offlineBootloaderNumber":I
    if-ne v5, v3, :cond_0

    if-eq v4, v1, :cond_1

    .line 279
    :cond_0
    sget-object v6, Lcom/vectorwatch/android/utils/OfflineUtils;->log:Lorg/slf4j/Logger;

    const-string v7, "COMPATIBILITY RESPONSE OFFLINE - WATCH UPDATE AVAILABLE"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 280
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v6

    new-instance v7, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;

    sget-object v8, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->INCOMPATIBLE_OLD_WATCH_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    const v9, 0x7f0901b7

    .line 281
    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;-><init>(Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;Ljava/lang/String;)V

    .line 280
    invoke-virtual {v6, v7}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 283
    const/4 v6, 0x0

    .line 289
    :goto_0
    return v6

    .line 285
    :cond_1
    sget-object v6, Lcom/vectorwatch/android/utils/OfflineUtils;->log:Lorg/slf4j/Logger;

    const-string v7, "COMPATIBILITY RESPONSE OFFLINE - ALREADY UP TO DATE"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 286
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v6

    new-instance v7, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;

    sget-object v8, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->COMPATIBLE:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    const v9, 0x7f09027c

    .line 287
    invoke-virtual {p0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;-><init>(Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;Ljava/lang/String;)V

    .line 286
    invoke-virtual {v6, v7}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 289
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public static getAppListOffline([B)Ljava/util/List;
    .locals 6
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 405
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 408
    .local v1, "commandBytesOrder":Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    .line 409
    .local v3, "numberOfApps":B
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 410
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 411
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 414
    :cond_0
    return-object v0
.end method

.method public static getOfflineAppInstallDetails(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;Ljava/lang/String;)Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appsOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;
    .param p2, "appUuid"    # Ljava/lang/String;

    .prologue
    .line 140
    const/4 v3, 0x0

    .line 141
    .local v3, "jsonArray":Lorg/json/JSONArray;
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 143
    .local v5, "watchShape":Ljava/lang/String;
    sget-object v6, Lcom/vectorwatch/android/utils/OfflineUtils$7;->$SwitchMap$com$vectorwatch$android$models$cloud$AppsOption:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 160
    :goto_0
    :pswitch_0
    if-eqz v3, :cond_3

    .line 161
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 162
    .local v1, "gson":Lcom/google/gson/Gson;
    new-instance v6, Lcom/vectorwatch/android/utils/OfflineUtils$2;

    invoke-direct {v6}, Lcom/vectorwatch/android/utils/OfflineUtils$2;-><init>()V

    .line 163
    invoke-virtual {v6}, Lcom/vectorwatch/android/utils/OfflineUtils$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v4

    .line 164
    .local v4, "listType":Ljava/lang/reflect/Type;
    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 166
    .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DownloadedAppDetails;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 167
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v6, v6, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 168
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 173
    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DownloadedAppDetails;>;"
    .end local v1    # "gson":Lcom/google/gson/Gson;
    .end local v2    # "i":I
    .end local v4    # "listType":Ljava/lang/reflect/Type;
    :goto_2
    return-object v6

    .line 145
    :pswitch_1
    const-string v6, "round"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 146
    const-string v6, "offline/OfflineRoundWatchfaces.json"

    invoke-static {p0, v6}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    goto :goto_0

    .line 148
    :cond_0
    const-string v6, "offline/OfflineSquareWatchfaces.json"

    invoke-static {p0, v6}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 150
    goto :goto_0

    .line 152
    :pswitch_2
    const-string v6, "round"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 153
    const-string v6, "offline/OfflineRoundApps.json"

    invoke-static {p0, v6}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    goto :goto_0

    .line 155
    :cond_1
    const-string v6, "offline/OfflineSquareApps.json"

    invoke-static {p0, v6}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    goto :goto_0

    .line 166
    .restart local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DownloadedAppDetails;>;"
    .restart local v1    # "gson":Lcom/google/gson/Gson;
    .restart local v2    # "i":I
    .restart local v4    # "listType":Ljava/lang/reflect/Type;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 173
    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DownloadedAppDetails;>;"
    .end local v1    # "gson":Lcom/google/gson/Gson;
    .end local v2    # "i":I
    .end local v4    # "listType":Ljava/lang/reflect/Type;
    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    .line 143
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getOfflineApps(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;)Lcom/vectorwatch/android/models/StoreQuery;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appsOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;

    .prologue
    .line 85
    new-instance v7, Lcom/vectorwatch/android/models/StoreQuery;

    const-string v9, ""

    new-instance v10, Lcom/vectorwatch/android/models/Apps;

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v10, v11}, Lcom/vectorwatch/android/models/Apps;-><init>(Ljava/util/List;)V

    invoke-direct {v7, v9, v10}, Lcom/vectorwatch/android/models/StoreQuery;-><init>(Ljava/lang/String;Lcom/vectorwatch/android/models/Apps;)V

    .line 88
    .local v7, "storeQuery":Lcom/vectorwatch/android/models/StoreQuery;
    const/4 v5, 0x0

    .line 89
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 91
    .local v8, "watchShape":Ljava/lang/String;
    sget-object v9, Lcom/vectorwatch/android/utils/OfflineUtils$7;->$SwitchMap$com$vectorwatch$android$models$cloud$AppsOption:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 111
    :goto_0
    if-eqz v5, :cond_3

    .line 112
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    .line 113
    .local v3, "gson":Lcom/google/gson/Gson;
    new-instance v9, Lcom/vectorwatch/android/utils/OfflineUtils$1;

    invoke-direct {v9}, Lcom/vectorwatch/android/utils/OfflineUtils$1;-><init>()V

    .line 114
    invoke-virtual {v9}, Lcom/vectorwatch/android/utils/OfflineUtils$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v6

    .line 115
    .local v6, "listType":Ljava/lang/reflect/Type;
    invoke-virtual {v5}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9, v6}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 117
    .local v1, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    if-ge v4, v9, :cond_2

    .line 118
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/StoreElement;

    .line 119
    .local v2, "element":Lcom/vectorwatch/android/models/StoreElement;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/vectorwatch/android/models/StoreElement;->setType(Ljava/lang/String;)V

    .line 120
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v2, v9}, Lcom/vectorwatch/android/models/StoreElement;->setNewInStore(Ljava/lang/Boolean;)V

    .line 117
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 93
    .end local v1    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    .end local v2    # "element":Lcom/vectorwatch/android/models/StoreElement;
    .end local v3    # "gson":Lcom/google/gson/Gson;
    .end local v4    # "i":I
    .end local v6    # "listType":Ljava/lang/reflect/Type;
    :pswitch_0
    const-string v9, "round"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 94
    const-string v9, "offline/OfflineRoundWatchfaces.json"

    invoke-static {p0, v9}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    goto :goto_0

    .line 96
    :cond_0
    const-string v9, "offline/OfflineSquareWatchfaces.json"

    invoke-static {p0, v9}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 98
    goto :goto_0

    .line 100
    :pswitch_1
    const-string v9, "offline/OfflineStreams.json"

    invoke-static {p0, v9}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 101
    goto :goto_0

    .line 103
    :pswitch_2
    const-string v9, "round"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 104
    const-string v9, "offline/OfflineRoundApps.json"

    invoke-static {p0, v9}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    goto :goto_0

    .line 106
    :cond_1
    const-string v9, "offline/OfflineSquareApps.json"

    invoke-static {p0, v9}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    goto :goto_0

    .line 123
    .restart local v1    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    .restart local v3    # "gson":Lcom/google/gson/Gson;
    .restart local v4    # "i":I
    .restart local v6    # "listType":Ljava/lang/reflect/Type;
    :cond_2
    new-instance v0, Lcom/vectorwatch/android/models/Apps;

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/models/Apps;-><init>(Ljava/util/List;)V

    .line 124
    .local v0, "apps":Lcom/vectorwatch/android/models/Apps;
    invoke-virtual {v7, v0}, Lcom/vectorwatch/android/models/StoreQuery;->setData(Lcom/vectorwatch/android/models/Apps;)V

    .line 128
    .end local v0    # "apps":Lcom/vectorwatch/android/models/Apps;
    .end local v1    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    .end local v3    # "gson":Lcom/google/gson/Gson;
    .end local v4    # "i":I
    .end local v6    # "listType":Ljava/lang/reflect/Type;
    :cond_3
    return-object v7

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getOfflineSoftwareUpdateModel(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "watchOsDetailsModel"    # Lcom/vectorwatch/android/models/WatchOsDetailsModel;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 378
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/vectorwatch/android/utils/OfflineUtils$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/vectorwatch/android/utils/OfflineUtils$4;-><init>(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 396
    return-void
.end method

.method public static getOfflineStream(Landroid/content/Context;Lcom/vectorwatch/android/models/StoreElement;)Lcom/vectorwatch/android/models/Stream;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "element"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 235
    const-string v5, "offline/OfflineStreams.json"

    invoke-static {p0, v5}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 236
    .local v3, "jsonArray":Lorg/json/JSONArray;
    if-eqz v3, :cond_1

    .line 237
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 238
    .local v1, "gson":Lcom/google/gson/Gson;
    new-instance v5, Lcom/vectorwatch/android/utils/OfflineUtils$3;

    invoke-direct {v5}, Lcom/vectorwatch/android/utils/OfflineUtils$3;-><init>()V

    .line 239
    invoke-virtual {v5}, Lcom/vectorwatch/android/utils/OfflineUtils$3;->getType()Ljava/lang/reflect/Type;

    move-result-object v4

    .line 240
    .local v4, "listType":Ljava/lang/reflect/Type;
    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 242
    .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 243
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Stream;

    iget-object v5, v5, Lcom/vectorwatch/android/models/Stream;->uuid:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 244
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Stream;

    .line 249
    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .end local v1    # "gson":Lcom/google/gson/Gson;
    .end local v2    # "i":I
    .end local v4    # "listType":Ljava/lang/reflect/Type;
    :goto_1
    return-object v5

    .line 242
    .restart local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .restart local v1    # "gson":Lcom/google/gson/Gson;
    .restart local v2    # "i":I
    .restart local v4    # "listType":Ljava/lang/reflect/Type;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 249
    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .end local v1    # "gson":Lcom/google/gson/Gson;
    .end local v2    # "i":I
    .end local v4    # "listType":Ljava/lang/reflect/Type;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private static getOfflineWatchData(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchType;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "watchType"    # Lcom/vectorwatch/android/models/WatchType;
    .param p2, "fileType"    # Ljava/lang/String;

    .prologue
    .line 355
    sget-object v0, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {p1, v0}, Lcom/vectorwatch/android/models/WatchType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    const-string v0, "kernel"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    const-string v0, "vos/VECTOR_ROUND_KERNEL"

    invoke-static {p0, v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 365
    :goto_0
    return-object v0

    .line 359
    :cond_0
    const-string v0, "vos/VECTOR_ROUND_BOOT"

    invoke-static {p0, v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 362
    :cond_1
    const-string v0, "kernel"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 363
    const-string v0, "vos/VECTOR_SQUARE_KERNEL"

    invoke-static {p0, v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 365
    :cond_2
    const-string v0, "vos/VECTOR_SQUARE_BOOT"

    invoke-static {p0, v0}, Lcom/vectorwatch/android/utils/OfflineUtils;->loadDataFromFileToString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getOfflineWatchVersionList(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "watchOsDetailsModel"    # Lcom/vectorwatch/android/models/WatchOsDetailsModel;
    .param p2, "listType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vectorwatch/android/models/WatchOsDetailsModel;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SoftwareUpdateData;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 303
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 304
    .local v2, "softwareUpdateDataList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SoftwareUpdateData;>;"
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 306
    .local v3, "watchShape":Ljava/lang/String;
    const-string v4, "summary"

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 343
    :goto_0
    return-object v2

    .line 310
    :cond_0
    new-instance v1, Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/SoftwareUpdateData;-><init>()V

    .line 311
    .local v1, "softwareUpdateDataKernel":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setId(Ljava/lang/Long;)V

    .line 312
    const-string v4, ""

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setSummary(Ljava/lang/String;)V

    .line 313
    const-string v4, "kernel"

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setType(Ljava/lang/String;)V

    .line 314
    invoke-static {}, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->getKernelVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setVersion(Ljava/lang/String;)V

    .line 316
    const-string v4, "round"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 317
    sget-object v4, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    const-string v5, "kernel"

    invoke-static {p0, v4, v5}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineWatchData(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setContentBase64(Ljava/lang/String;)V

    .line 322
    :goto_1
    new-instance v0, Lcom/vectorwatch/android/models/SoftwareUpdateData;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/SoftwareUpdateData;-><init>()V

    .line 323
    .local v0, "softwareUpdateDataBootloader":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setId(Ljava/lang/Long;)V

    .line 324
    const-string v4, ""

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setSummary(Ljava/lang/String;)V

    .line 325
    const-string v4, "bootloader"

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setType(Ljava/lang/String;)V

    .line 326
    invoke-static {}, Lcom/vectorwatch/android/utils/EmbeddedVosDetails;->getBootloaderVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setVersion(Ljava/lang/String;)V

    .line 328
    const-string v4, "round"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 329
    sget-object v4, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    const-string v5, "bootloader"

    invoke-static {p0, v4, v5}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineWatchData(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setContentBase64(Ljava/lang/String;)V

    .line 334
    :goto_2
    sget-object v4, Lcom/vectorwatch/android/utils/OfflineUtils;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Offline: Current system type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->getCurrentSystemType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 335
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->getCurrentSystemType()Ljava/lang/String;

    move-result-object v4

    const-string v5, "kernel"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 336
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 319
    .end local v0    # "softwareUpdateDataBootloader":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    :cond_1
    sget-object v4, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    const-string v5, "kernel"

    invoke-static {p0, v4, v5}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineWatchData(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setContentBase64(Ljava/lang/String;)V

    goto :goto_1

    .line 331
    .restart local v0    # "softwareUpdateDataBootloader":Lcom/vectorwatch/android/models/SoftwareUpdateData;
    :cond_2
    sget-object v4, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    const-string v5, "bootloader"

    invoke-static {p0, v4, v5}, Lcom/vectorwatch/android/utils/OfflineUtils;->getOfflineWatchData(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/SoftwareUpdateData;->setContentBase64(Ljava/lang/String;)V

    goto :goto_2

    .line 339
    :cond_3
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public static installAppsToDb(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 424
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/vectorwatch/android/utils/OfflineUtils$5;

    invoke-direct {v1, p0, p1}, Lcom/vectorwatch/android/utils/OfflineUtils$5;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 474
    return-void
.end method

.method private static loadDataFromFileToJSONArray(Landroid/content/Context;Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 185
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 186
    .local v3, "stream":Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v2

    .line 188
    .local v2, "size":I
    new-array v0, v2, [B

    .line 189
    .local v0, "bytes":[B
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    .line 190
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 192
    new-instance v4, Lorg/json/JSONArray;

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v4, v5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 199
    .end local v0    # "bytes":[B
    .end local v2    # "size":I
    .end local v3    # "stream":Ljava/io/InputStream;
    :goto_0
    return-object v4

    .line 194
    :catch_0
    move-exception v1

    .line 195
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 199
    .end local v1    # "e":Ljava/io/IOException;
    :goto_1
    const/4 v4, 0x0

    goto :goto_0

    .line 196
    :catch_1
    move-exception v1

    .line 197
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method private static loadDataFromFileToString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 211
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 212
    .local v3, "stream":Ljava/io/InputStream;
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v2

    .line 214
    .local v2, "size":I
    new-array v0, v2, [B

    .line 215
    .local v0, "bytes":[B
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    .line 216
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 218
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    .end local v0    # "bytes":[B
    .end local v2    # "size":I
    .end local v3    # "stream":Ljava/io/InputStream;
    :goto_0
    return-object v4

    .line 220
    :catch_0
    move-exception v1

    .line 221
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 223
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static refreshOfflineCloudStatus(Z)V
    .locals 5
    .param p0, "isCloudOnline"    # Z

    .prologue
    .line 566
    if-eqz p0, :cond_1

    .line 567
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "prefs_cloud_status"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 569
    :cond_1
    new-instance v0, Lcom/vectorwatch/android/models/CloudStatus;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/vectorwatch/android/models/CloudStatus;-><init>(J)V

    .line 570
    .local v0, "cloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "prefs_cloud_status"

    const-class v4, Lcom/vectorwatch/android/models/CloudStatus;

    invoke-virtual {v2, v3, v4}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/CloudStatus;

    .line 573
    .local v1, "oldCloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    if-nez v1, :cond_0

    .line 574
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "prefs_cloud_status"

    invoke-virtual {v2, v3, v0}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static removeBlacklistedApps(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 498
    .local p1, "appIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 499
    .local v1, "validAppList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 500
    sget-object v4, Lcom/vectorwatch/android/utils/OfflineUtils;->sBlackList:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 501
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 499
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 503
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3, v0, p0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->uninstallAppFromWatch(IILandroid/content/Context;)V

    goto :goto_1

    .line 508
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 509
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 510
    .local v2, "watchShape":Ljava/lang/String;
    const-string v3, "round"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 511
    const/16 v3, 0x1118

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 516
    :goto_2
    invoke-static {p0, v1}, Lcom/vectorwatch/android/utils/OfflineUtils;->installAppsToDb(Landroid/content/Context;Ljava/util/List;)V

    .line 519
    .end local v2    # "watchShape":Ljava/lang/String;
    :cond_2
    return-object v1

    .line 513
    .restart local v2    # "watchShape":Ljava/lang/String;
    :cond_3
    const/16 v3, 0x112b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public static removeStreamsOffline(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 482
    invoke-static {p0}, Lcom/vectorwatch/android/managers/StreamsManager;->getSavedStreams(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 483
    .local v1, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 484
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Stream;

    invoke-static {v2}, Lcom/vectorwatch/android/utils/OfflineUtils;->validStream(Lcom/vectorwatch/android/models/Stream;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 485
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/managers/StreamsManager;->deleteStreamFromDatabase(Ljava/lang/String;Landroid/content/Context;)Z

    .line 483
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 488
    :cond_1
    return-void
.end method

.method public static switchToOfflineMode(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 543
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/vectorwatch/android/utils/OfflineUtils$6;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/utils/OfflineUtils$6;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 558
    return-void
.end method

.method private static validStream(Lcom/vectorwatch/android/models/Stream;)Z
    .locals 2
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 529
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PRIVATE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PUBLIC:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 530
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 531
    :cond_0
    const/4 v0, 0x0

    .line 534
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
