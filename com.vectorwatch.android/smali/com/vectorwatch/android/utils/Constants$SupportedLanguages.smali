.class public final enum Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SupportedLanguages"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

.field public static final enum DUTCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

.field public static final enum ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

.field public static final enum FRENCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

.field public static final enum GERMAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

.field public static final enum ROMANIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

.field public static final enum RUSSIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

.field public static final enum SPANISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

.field public static final enum TURKISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;


# instance fields
.field val:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 283
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    const-string v1, "ENGLISH"

    const-string v2, "en"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    .line 284
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    const-string v1, "GERMAN"

    const-string v2, "de"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->GERMAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    .line 285
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    const-string v1, "FRENCH"

    const-string v2, "fr"

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->FRENCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    .line 286
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    const-string v1, "SPANISH"

    const-string v2, "es"

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->SPANISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    .line 287
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    const-string v1, "TURKISH"

    const-string v2, "tr"

    invoke-direct {v0, v1, v8, v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->TURKISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    .line 288
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    const-string v1, "RUSSIAN"

    const/4 v2, 0x5

    const-string v3, "ru"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->RUSSIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    .line 289
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    const-string v1, "ROMANIAN"

    const/4 v2, 0x6

    const-string v3, "ro"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ROMANIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    .line 290
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    const-string v1, "DUTCH"

    const/4 v2, 0x7

    const-string v3, "nl"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->DUTCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    .line 282
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->GERMAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->FRENCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->SPANISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->TURKISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->RUSSIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ROMANIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->DUTCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "val"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 294
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 295
    iput-object p3, p0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->val:Ljava/lang/String;

    .line 296
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 282
    const-class v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;
    .locals 1

    .prologue
    .line 282
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    return-object v0
.end method


# virtual methods
.method public getShortName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->val:Ljava/lang/String;

    return-object v0
.end method
