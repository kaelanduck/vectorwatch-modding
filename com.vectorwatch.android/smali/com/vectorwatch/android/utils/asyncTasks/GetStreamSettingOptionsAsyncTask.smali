.class public Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;
.super Landroid/os/AsyncTask;
.source "GetStreamSettingOptionsAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private authExpired:Z

.field private callback:Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;

.field currentValue:Ljava/lang/String;

.field private mProgressBar:Landroid/view/View;

.field pContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field settingName:Ljava/lang/String;

.field stream:Lcom/vectorwatch/android/models/Stream;

.field userSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/Stream;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "settingName"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .param p5, "callback"    # Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;
    .param p6, "context"    # Landroid/content/Context;
    .param p7, "progressBar"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/Stream;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;",
            "Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    .local p4, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->authExpired:Z

    .line 50
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    .line 51
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 52
    iput-object p2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->settingName:Ljava/lang/String;

    .line 53
    iput-object p3, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->currentValue:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->userSettings:Ljava/util/Map;

    .line 55
    iput-object p7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    .line 56
    iput-object p5, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;

    .line 57
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 63
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    const-string v9, "AT_GetStreamAuthMethodAsyncTask"

    invoke-virtual {v8, v9}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 65
    iget-object v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_1

    .line 108
    :cond_0
    :goto_0
    return-object v7

    .line 71
    :cond_1
    :try_start_0
    iget-object v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v8}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 72
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v8, "com.vectorwatch.android"

    invoke-virtual {v1, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 74
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v8, v2

    if-ge v8, v10, :cond_2

    .line 75
    sget-object v8, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v9, "There should be at least one account in Accounts & sync"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 100
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    :catch_0
    move-exception v5

    .line 101
    .local v5, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v5}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 80
    .end local v5    # "e":Landroid/accounts/OperationCanceledException;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_2
    const/4 v8, 0x0

    :try_start_1
    aget-object v0, v2, v8

    .line 82
    .local v0, "account":Landroid/accounts/Account;
    const-string v8, "Full access"

    const/4 v9, 0x1

    invoke-virtual {v1, v0, v8, v9}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "authToken":Ljava/lang/String;
    iget-object v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_4

    .line 85
    .local v4, "context":Landroid/content/Context;
    if-eqz v4, :cond_0

    .line 89
    :try_start_2
    iget-object v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->settingName:Ljava/lang/String;

    iget-object v10, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->currentValue:Ljava/lang/String;

    iget-object v11, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->userSettings:Ljava/util/Map;

    .line 90
    invoke-static {v8, v9, v10, v11, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->getStreamSettingOptions(Lcom/vectorwatch/android/models/Stream;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/util/List;
    :try_end_2
    .catch Lretrofit/RetrofitError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v7

    .line 91
    .local v7, "options":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    goto :goto_0

    .line 92
    .end local v7    # "options":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    :catch_1
    move-exception v6

    .line 93
    .local v6, "error":Lretrofit/RetrofitError;
    :try_start_3
    invoke-virtual {v6}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v6}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v8

    invoke-virtual {v8}, Lretrofit/client/Response;->getStatus()I

    move-result v8

    sget v9, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->STREAM_APP_AUTH_ERROR_STATUS_CODE:I

    if-ne v8, v9, :cond_0

    .line 94
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->authExpired:Z
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    .line 102
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v6    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v5

    .line 103
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 96
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    .restart local v3    # "authToken":Ljava/lang/String;
    .restart local v4    # "context":Landroid/content/Context;
    :catch_3
    move-exception v5

    .line 97
    .local v5, "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v8, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v9, "Error in getting stream auth method "

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/accounts/OperationCanceledException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    .line 104
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    .line 105
    .local v5, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v5}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    :cond_0
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->authExpired:Z

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;

    invoke-interface {v0}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;->onOptionsReceivedError()V

    .line 126
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 139
    :cond_1
    :goto_0
    return-void

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 130
    if-eqz p1, :cond_3

    .line 131
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;->onOptionsReceivedSuccess(Ljava/util/List;)V

    goto :goto_0

    .line 133
    :cond_3
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;

    invoke-interface {v0}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;->onOptionsReceivedError()V

    goto :goto_0

    .line 136
    :cond_4
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;

    invoke-interface {v0}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;->onOptionsReceivedError()V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 116
    :cond_0
    return-void
.end method
