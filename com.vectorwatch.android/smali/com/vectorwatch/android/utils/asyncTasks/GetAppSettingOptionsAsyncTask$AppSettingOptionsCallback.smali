.class public interface abstract Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;
.super Ljava/lang/Object;
.source "GetAppSettingOptionsAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AppSettingOptionsCallback"
.end annotation


# virtual methods
.method public abstract onOptionsReceivedError()V
.end method

.method public abstract onOptionsReceivedSuccess(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;)V"
        }
    .end annotation
.end method
