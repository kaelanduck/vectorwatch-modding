.class public Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;
.super Landroid/os/AsyncTask;
.source "GetAppSettingOptionsAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field app:Lcom/vectorwatch/android/models/CloudElementSummary;

.field private callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;

.field currentValue:Ljava/lang/String;

.field private hasAuthError:Z

.field private mProgressBar:Landroid/view/View;

.field private model:Lcom/vectorwatch/android/models/AppOptionsRequest;

.field pContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field settingName:Ljava/lang/String;

.field userSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/models/CloudElementSummary;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p2, "settingName"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .param p5, "callback"    # Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;
    .param p6, "context"    # Landroid/content/Context;
    .param p7, "progressBar"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;",
            "Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    .local p4, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 52
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    .line 53
    iput-object p1, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 54
    iput-object p2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->settingName:Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->currentValue:Ljava/lang/String;

    .line 56
    iput-object p4, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->userSettings:Ljava/util/Map;

    .line 57
    iput-object p7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    .line 58
    iput-object p5, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;

    .line 59
    new-instance v0, Lcom/vectorwatch/android/models/AppOptionsRequest;

    invoke-direct {v0, p2, p3, p4}, Lcom/vectorwatch/android/models/AppOptionsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->model:Lcom/vectorwatch/android/models/AppOptionsRequest;

    .line 60
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->doInBackground([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1, "params"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 72
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    const-string v9, "AT_GetStreamAuthMethodAsyncTask"

    invoke-virtual {v7, v9}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 74
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_0

    move-object v7, v8

    .line 114
    :goto_0
    return-object v7

    .line 79
    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v7}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 80
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v7, "com.vectorwatch.android"

    invoke-virtual {v1, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 82
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v7, v2

    if-ge v7, v10, :cond_1

    .line 83
    sget-object v7, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v9, "There should be at least one account in Accounts & sync"

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    move-object v7, v8

    .line 85
    goto :goto_0

    .line 88
    :cond_1
    const/4 v7, 0x0

    aget-object v0, v2, v7

    .line 90
    .local v0, "account":Landroid/accounts/Account;
    const-string v7, "Full access"

    const/4 v9, 0x1

    invoke-virtual {v1, v0, v7, v9}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 92
    .local v3, "authToken":Ljava/lang/String;
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 93
    .local v4, "context":Landroid/content/Context;
    if-nez v4, :cond_2

    move-object v7, v8

    .line 94
    goto :goto_0

    .line 97
    :cond_2
    :try_start_1
    iget-object v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v7

    iget-object v9, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->model:Lcom/vectorwatch/android/models/AppOptionsRequest;

    invoke-static {v7, v9, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->getAppSettingOptions(Ljava/lang/String;Lcom/vectorwatch/android/models/AppOptionsRequest;Ljava/lang/String;)Ljava/util/List;
    :try_end_1
    .catch Lretrofit/RetrofitError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v7

    goto :goto_0

    .line 98
    :catch_0
    move-exception v6

    .line 99
    .local v6, "retrofitError":Lretrofit/RetrofitError;
    :try_start_2
    invoke-virtual {v6}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v7

    invoke-virtual {v7}, Lretrofit/client/Response;->getStatus()I

    move-result v7

    const/16 v9, 0x385

    if-ne v7, v9, :cond_3

    .line 100
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->hasAuthError:Z

    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v6    # "retrofitError":Lretrofit/RetrofitError;
    :cond_3
    :goto_1
    move-object v7, v8

    .line 114
    goto :goto_0

    .line 102
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    .restart local v3    # "authToken":Ljava/lang/String;
    .restart local v4    # "context":Landroid/content/Context;
    :catch_1
    move-exception v5

    .line 103
    .local v5, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->log:Lorg/slf4j/Logger;

    const-string v9, "Error in getting app setting options method "

    invoke-interface {v7, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 106
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "context":Landroid/content/Context;
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v5

    .line 107
    .local v5, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v5}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_1

    .line 108
    .end local v5    # "e":Landroid/accounts/OperationCanceledException;
    :catch_3
    move-exception v5

    .line 109
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 110
    .end local v5    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 111
    .local v5, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v5}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 34
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->pContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 130
    if-eqz p1, :cond_1

    .line 131
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;

    invoke-interface {v0, p1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;->onOptionsReceivedSuccess(Ljava/util/List;)V

    .line 144
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->hasAuthError:Z

    if-eqz v0, :cond_2

    .line 134
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;

    invoke-interface {v0}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;->onOptionsReceivedError()V

    goto :goto_0

    .line 139
    :cond_3
    iget-boolean v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->hasAuthError:Z

    if-eqz v0, :cond_4

    .line 140
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 142
    :cond_4
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->callback:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;

    invoke-interface {v0}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;->onOptionsReceivedError()V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->mProgressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 122
    :cond_0
    return-void
.end method

.method public setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;
    .locals 1
    .param p1, "location"    # Lcom/vectorwatch/android/models/VectorLocation;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->model:Lcom/vectorwatch/android/models/AppOptionsRequest;

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/AppOptionsRequest;->setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/models/AppOptionsRequest;

    .line 65
    return-object p0
.end method
