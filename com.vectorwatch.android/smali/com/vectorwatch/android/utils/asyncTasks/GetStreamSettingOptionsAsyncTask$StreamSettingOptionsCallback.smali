.class public interface abstract Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;
.super Ljava/lang/Object;
.source "GetStreamSettingOptionsAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StreamSettingOptionsCallback"
.end annotation


# virtual methods
.method public abstract onOptionsReceivedError()V
.end method

.method public abstract onOptionsReceivedSuccess(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;)V"
        }
    .end annotation
.end method
