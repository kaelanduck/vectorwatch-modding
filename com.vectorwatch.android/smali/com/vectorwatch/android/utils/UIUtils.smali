.class public Lcom/vectorwatch/android/utils/UIUtils;
.super Ljava/lang/Object;
.source "UIUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;
    }
.end annotation


# static fields
.field private static log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/vectorwatch/android/utils/UIUtils;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/UIUtils;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    return-void
.end method

.method public static crossFadePanels(Landroid/view/View;Landroid/view/View;I)V
    .locals 5
    .param p0, "panelToShow"    # Landroid/view/View;
    .param p1, "panelToHide"    # Landroid/view/View;
    .param p2, "mShortAnimationDuration"    # I

    .prologue
    const/4 v4, 0x0

    .line 39
    invoke-virtual {p0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 42
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 43
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p2

    .line 44
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 45
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p2

    .line 49
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/utils/UIUtils$1;

    invoke-direct {v1, p1}, Lcom/vectorwatch/android/utils/UIUtils$1;-><init>(Landroid/view/View;)V

    .line 50
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 56
    return-void
.end method

.method public static getCloudAppFaceCacheKey(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Ljava/lang/String;
    .locals 2
    .param p0, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "type"    # Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCloudAppFaceCacheKey(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Ljava/lang/String;
    .locals 2
    .param p0, "app"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p1, "type"    # Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCloudStreamFaceCacheKey(Lcom/vectorwatch/android/models/Stream;)Ljava/lang/String;
    .locals 2
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p2, "type"    # Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            "Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .local p0, "cache":Landroid/util/LruCache;, "Landroid/util/LruCache<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v7, 0x0

    .line 102
    :try_start_0
    invoke-static {p1, p2}, Lcom/vectorwatch/android/utils/UIUtils;->getCloudAppFaceCacheKey(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Ljava/lang/String;

    move-result-object v1

    .line 103
    .local v1, "cacheKey":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 104
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 105
    const/4 v4, 0x0

    .line 106
    .local v4, "image":Ljava/lang/String;
    sget-object v5, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    if-ne p2, v5, :cond_2

    .line 107
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getImage()Ljava/lang/String;

    move-result-object v4

    .line 116
    :cond_0
    :goto_0
    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 117
    .local v2, "decodedString":[B
    const/4 v5, 0x0

    array-length v6, v2

    invoke-static {v2, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 118
    invoke-virtual {p0, v1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "cacheKey":Ljava/lang/String;
    .end local v2    # "decodedString":[B
    .end local v4    # "image":Ljava/lang/String;
    :cond_1
    :goto_1
    return-object v0

    .line 109
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "cacheKey":Ljava/lang/String;
    .restart local v4    # "image":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getEditImg()Ljava/lang/String;

    move-result-object v4

    .line 111
    if-nez v4, :cond_0

    .line 112
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getImage()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 122
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "cacheKey":Ljava/lang/String;
    .end local v4    # "image":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 123
    .local v3, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 124
    .restart local v4    # "image":Ljava/lang/String;
    sget-object v5, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    if-ne p2, v5, :cond_4

    .line 125
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getImage()Ljava/lang/String;

    move-result-object v4

    .line 134
    :cond_3
    :goto_2
    if-eqz v4, :cond_5

    .line 135
    invoke-static {v4, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 136
    .restart local v2    # "decodedString":[B
    array-length v5, v2

    invoke-static {v2, v7, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 137
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 127
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "decodedString":[B
    :cond_4
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getEditImg()Ljava/lang/String;

    move-result-object v4

    .line 129
    if-nez v4, :cond_3

    .line 130
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getImage()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 139
    :cond_5
    sget-object v5, Lcom/vectorwatch/android/utils/UIUtils;->log:Lorg/slf4j/Logger;

    const-string v6, "Display image is null"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 142
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static getImageFromBitmapOrDecode(Landroid/util/LruCache;Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "cloudElementSummary"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "type"    # Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/vectorwatch/android/models/StoreElement;",
            "Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 156
    .local p0, "cache":Landroid/util/LruCache;, "Landroid/util/LruCache<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    :try_start_0
    invoke-static {p1, p2}, Lcom/vectorwatch/android/utils/UIUtils;->getCloudAppFaceCacheKey(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "cacheKey":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 158
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 159
    const/4 v4, 0x0

    .line 160
    .local v4, "image":Ljava/lang/String;
    sget-object v5, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    if-ne p2, v5, :cond_2

    .line 161
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v4

    .line 170
    :cond_0
    :goto_0
    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 171
    .local v2, "decodedString":[B
    const/4 v5, 0x0

    array-length v6, v2

    invoke-static {v2, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 172
    invoke-virtual {p0, v1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "cacheKey":Ljava/lang/String;
    .end local v2    # "decodedString":[B
    .end local v4    # "image":Ljava/lang/String;
    :cond_1
    :goto_1
    return-object v0

    .line 163
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "cacheKey":Ljava/lang/String;
    .restart local v4    # "image":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getEditImage()Ljava/lang/String;

    move-result-object v4

    .line 165
    if-nez v4, :cond_0

    .line 166
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 176
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "cacheKey":Ljava/lang/String;
    .end local v4    # "image":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 177
    .local v3, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 178
    .restart local v4    # "image":Ljava/lang/String;
    sget-object v5, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    if-ne p2, v5, :cond_4

    .line 179
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v4

    .line 188
    :cond_3
    :goto_2
    if-eqz v4, :cond_5

    .line 190
    const/4 v5, 0x0

    :try_start_1
    invoke-static {v4, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 191
    .restart local v2    # "decodedString":[B
    const/4 v5, 0x0

    array-length v6, v2

    invoke-static {v2, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 192
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_1

    .line 181
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "decodedString":[B
    :cond_4
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getEditImage()Ljava/lang/String;

    move-result-object v4

    .line 183
    if-nez v4, :cond_3

    .line 184
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getImage()Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 195
    :cond_5
    sget-object v5, Lcom/vectorwatch/android/utils/UIUtils;->log:Lorg/slf4j/Logger;

    const-string v6, "Display image is null"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 198
    :goto_3
    const/4 v0, 0x0

    goto :goto_1

    .line 193
    :catch_1
    move-exception v5

    goto :goto_3
.end method

.method public static getImageFromBitmapOrDecodeStream(Landroid/util/LruCache;Lcom/vectorwatch/android/models/Stream;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/vectorwatch/android/models/Stream;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .local p0, "cache":Landroid/util/LruCache;, "Landroid/util/LruCache<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v7, 0x0

    .line 211
    :try_start_0
    invoke-static {p1}, Lcom/vectorwatch/android/utils/UIUtils;->getCloudStreamFaceCacheKey(Lcom/vectorwatch/android/models/Stream;)Ljava/lang/String;

    move-result-object v1

    .line 212
    .local v1, "cacheKey":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 213
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 214
    const/4 v4, 0x0

    .line 215
    .local v4, "image":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getImg()Ljava/lang/String;

    move-result-object v4

    .line 217
    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 218
    .local v2, "decodedString":[B
    const/4 v5, 0x0

    array-length v6, v2

    invoke-static {v2, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 219
    invoke-virtual {p0, v1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "cacheKey":Ljava/lang/String;
    .end local v2    # "decodedString":[B
    .end local v4    # "image":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 223
    :catch_0
    move-exception v3

    .line 224
    .local v3, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 225
    .restart local v4    # "image":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getImg()Ljava/lang/String;

    move-result-object v4

    .line 227
    if-eqz v4, :cond_1

    .line 228
    invoke-static {v4, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 229
    .restart local v2    # "decodedString":[B
    array-length v5, v2

    invoke-static {v2, v7, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 230
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 232
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "decodedString":[B
    :cond_1
    sget-object v5, Lcom/vectorwatch/android/utils/UIUtils;->log:Lorg/slf4j/Logger;

    const-string v6, "Display image is null"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 235
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getImageSize(Landroid/content/Context;I)[I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceId"    # I

    .prologue
    const/4 v5, 0x1

    .line 29
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 30
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 31
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, p1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 32
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 33
    .local v0, "imageHeight":I
    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 35
    .local v1, "imageWidth":I
    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v1, v3, v4

    aput v0, v3, v5

    return-object v3
.end method

.method public static hasJellyBeanMR2()Z
    .locals 2

    .prologue
    .line 240
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasLollipop()Z
    .locals 2

    .prologue
    .line 244
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
