.class public final enum Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;
.super Ljava/lang/Enum;
.source "AnalyticsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/AnalyticsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnalyticsCategory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_ACCOUNT_INFO:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_ACCOUNT_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_ACTIVITY:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_ACTIVITY_CHART:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_ACTIVITY_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_ALARM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_CALORIES_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_DISTANCE_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_GOOGLE_FIT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_LINK_LOST_SETTING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_LOW_BATTERY_SETTING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_MENU_TAB:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_NOTIFICATION:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_RATING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_REVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_SLEEP_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_STEPS_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_STORE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_STORE_ITEM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_STREAM_SETTINGS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_SUPPORT_TICKET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_TOUR:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_VIBRATION:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_WATCH:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_WATCH_APP:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_WATCH_APP_SETTINGS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_WATCH_BACKLIGHT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

.field public static final enum ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 152
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_MENU_TAB"

    const-string v2, "Menu tab"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_MENU_TAB:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 153
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_SCREEN"

    const-string v2, "Screen"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 154
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_WATCH_APP"

    const-string v2, "Watch app"

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_APP:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 155
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_STREAM"

    const-string v2, "Stream"

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 156
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_WATCH_FACE"

    const-string v2, "Watch face"

    invoke-direct {v0, v1, v8, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 157
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_STORE"

    const/4 v2, 0x5

    const-string v3, "Store"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STORE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 158
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_STORE_ITEM"

    const/4 v2, 0x6

    const-string v3, "Store item"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STORE_ITEM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 159
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_STREAM_SETTINGS"

    const/4 v2, 0x7

    const-string v3, "Stream settings"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM_SETTINGS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 160
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_WATCH_APP_SETTINGS"

    const/16 v2, 0x8

    const-string v3, "Watch app settings"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_APP_SETTINGS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 161
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_VIBRATION"

    const/16 v2, 0x9

    const-string v3, "Vibration"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_VIBRATION:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 162
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_CALORIES_GOAL"

    const/16 v2, 0xa

    const-string v3, "Calories goal"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_CALORIES_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 163
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_STEPS_GOAL"

    const/16 v2, 0xb

    const-string v3, "Steps goal"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STEPS_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 164
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_DISTANCE_GOAL"

    const/16 v2, 0xc

    const-string v3, "Distance goal"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_DISTANCE_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 165
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_SLEEP_GOAL"

    const/16 v2, 0xd

    const-string v3, "Sleep goal"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SLEEP_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 166
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_ACTIVITY_CHART"

    const/16 v2, 0xe

    const-string v3, "Activity chart"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY_CHART:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 167
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_ACTIVITY"

    const/16 v2, 0xf

    const-string v3, "Activity"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 168
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_SUPPORT_TICKET"

    const/16 v2, 0x10

    const-string v3, "Support ticket"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SUPPORT_TICKET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 169
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_ACCOUNT_PROFILE"

    const/16 v2, 0x11

    const-string v3, "Account profile"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACCOUNT_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 170
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_ACTIVITY_PROFILE"

    const/16 v2, 0x12

    const-string v3, "Activity profile"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 171
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_ACCOUNT_INFO"

    const/16 v2, 0x13

    const-string v3, "Account info"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACCOUNT_INFO:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 172
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_WATCH_BACKLIGHT"

    const/16 v2, 0x14

    const-string v3, "Watch backlight"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_BACKLIGHT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 173
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_TOUR"

    const/16 v2, 0x15

    const-string v3, "Tour"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_TOUR:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 174
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_ALARM"

    const/16 v2, 0x16

    const-string v3, "Alarm"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ALARM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 175
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_GOOGLE_FIT"

    const/16 v2, 0x17

    const-string v3, "Google Fit"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_GOOGLE_FIT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 176
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_NOTIFICATION"

    const/16 v2, 0x18

    const-string v3, "Notification"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_NOTIFICATION:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 177
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_REVIEW"

    const/16 v2, 0x19

    const-string v3, "Review"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_REVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 178
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_RATING"

    const/16 v2, 0x1a

    const-string v3, "Rating"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_RATING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 179
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_LINK_LOST_SETTING"

    const/16 v2, 0x1b

    const-string v3, "Link lost settings"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_LINK_LOST_SETTING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 180
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_LOW_BATTERY_SETTING"

    const/16 v2, 0x1c

    const-string v3, "Low battery settings"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_LOW_BATTERY_SETTING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 181
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    const-string v1, "ANALYTICS_CATEGORY_WATCH"

    const/16 v2, 0x1d

    const-string v3, "Watch"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    .line 151
    const/16 v0, 0x1e

    new-array v0, v0, [Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_MENU_TAB:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_APP:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STORE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STORE_ITEM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM_SETTINGS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_APP_SETTINGS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_VIBRATION:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_CALORIES_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STEPS_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_DISTANCE_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SLEEP_GOAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY_CHART:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SUPPORT_TICKET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACCOUNT_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACTIVITY_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ACCOUNT_INFO:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_BACKLIGHT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_TOUR:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_ALARM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_GOOGLE_FIT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_NOTIFICATION:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_REVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_RATING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_LINK_LOST_SETTING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_LOW_BATTERY_SETTING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->$VALUES:[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 185
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 186
    iput-object p3, p0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->name:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 151
    const-class v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->$VALUES:[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->name:Ljava/lang/String;

    return-object v0
.end method
