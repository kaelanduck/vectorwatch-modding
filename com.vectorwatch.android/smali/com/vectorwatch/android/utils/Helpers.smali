.class public Lcom/vectorwatch/android/utils/Helpers;
.super Ljava/lang/Object;
.source "Helpers.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/Helpers$TableType;
    }
.end annotation


# static fields
.field public static final ADJUSTED_CALORIES:I = 0x1

.field public static final ADJUSTED_DISTANCE:I = 0x2

.field public static final ADJUSTED_STEPS:I = 0x0

.field public static final BASE_UUID:Landroid/os/ParcelUuid;

.field public static final BATTERY_NOT_SYNCED:I = -0x1

.field public static final CONNECTED_NORMALLY:I = 0x2

.field public static final CONTEXTUAL_AUTO_DISCREET_MODE:Ljava/lang/String; = "AUTO_DISCREET_MODE"

.field public static final CONTEXTUAL_AUTO_SLEEP_DETECTION:Ljava/lang/String; = "AUTO_SLEEP_DETECTION"

.field public static final CONTEXTUAL_FLICK_TO_DISMISS:Ljava/lang/String; = "FLICK_TO_DISMISS"

.field public static final CONTEXTUAL_GLANCE:Ljava/lang/String; = "GLANCE"

.field public static final CONTEXTUAL_MORNING_FACE:Ljava/lang/String; = "MORNING_FACE"

.field public static final CONTEXTUAL_OPTIMIZE_NOTIFICATIONS:Ljava/lang/String; = "OPTIMIZE_NOTIFICATIONS"

.field public static final CONTEXTUAL_SHOW_TENTATIVE_MEETINGS:Ljava/lang/String; = "SHOW_TENTATIVE_MEETINGS"

.field public static final CONTEXTUAL_VIBRATION:Ljava/lang/String; = "VIBRATE"

.field public static final COUNT_ELEMENTS:I = 0x5

.field private static final DATA_TYPE_FLAGS:I = 0x1

.field private static final DATA_TYPE_LOCAL_NAME_COMPLETE:I = 0x9

.field private static final DATA_TYPE_LOCAL_NAME_SHORT:I = 0x8

.field private static final DATA_TYPE_MANUFACTURER_SPECIFIC_DATA:I = 0xff

.field private static final DATA_TYPE_SERVICE_DATA:I = 0x16

.field private static final DATA_TYPE_SERVICE_UUIDS_128_BIT_COMPLETE:I = 0x7

.field private static final DATA_TYPE_SERVICE_UUIDS_128_BIT_PARTIAL:I = 0x6

.field private static final DATA_TYPE_SERVICE_UUIDS_16_BIT_COMPLETE:I = 0x3

.field private static final DATA_TYPE_SERVICE_UUIDS_16_BIT_PARTIAL:I = 0x2

.field private static final DATA_TYPE_SERVICE_UUIDS_32_BIT_COMPLETE:I = 0x5

.field private static final DATA_TYPE_SERVICE_UUIDS_32_BIT_PARTIAL:I = 0x4

.field private static final DATA_TYPE_TX_POWER_LEVEL:I = 0xa

.field public static final DELAY_TIME_1500:I = 0x5dc

.field public static final DELAY_TIME_2000:I = 0x7d0

.field public static final DELAY_TIME_3000:I = 0xbb8

.field public static final FALSE:I = 0x0

.field public static final FIRST_CONNECT:I = 0x0

.field public static final IN_BOOTLOADER:I = -0x1

.field public static final LIMIT_MAX_INSTALLED_APPS:I = 0xc

.field public static final LIMIT_MIN_INSTALLED_APPS:I = 0x1

.field public static final LOW_PHONE_BATTERY_LEVEL:F = 15.0f

.field public static final LOW_WATCH_BATTERY_LEVEL:I = 0xf

.field private static final METERS_TO_KM_COEFFICIENT:I = 0x3e8

.field private static final METERS_TO_MILES_COEFFICIENT:I = 0x261

.field public static final PARSE_SET_NEW_CPU_ID:I = 0x0

.field public static final PARSE_SET_WATCH_SHAPE:I = 0x2

.field public static final PARSE_SET_WATCH_SN:I = 0x1

.field public static final RESTART_NOTIFICATION_INTENT:Ljava/lang/String; = "com.vectorwatch.android.NOTIFICATION_RESTART"

.field public static final RUNNING_APPS_LIMIT:I = 0xc

.field public static final SENSOR_DELAY_KEY:Ljava/lang/String; = "sensor_delay"

.field public static final SHARE_TYPE_GOAL_ACHIEVEMENT:Ljava/lang/String; = "goalAchievement"

.field public static final TIMESTAMP_NOT_DEFINED:I = -0x1

.field public static final TIMEZONE_A:I = 0x1

.field public static final TIMEZONE_B:I = 0x2

.field public static final TIME_OF_DAY_PM:I = 0x1

.field public static final TRUE:I = 0x1

.field public static final UNSET_VALUE:I = -0x1

.field public static final UUID_BYTES_128_BIT:I = 0x10

.field public static final UUID_BYTES_16_BIT:I = 0x2

.field public static final UUID_BYTES_32_BIT:I = 0x4

.field public static final UUID_SERVICE_WATCH_1:Ljava/lang/String; = "81a50000-9ebd-0436-3358-bb370c7da4c5"

.field public static final UUID_SERVICE_WATCH_2:Ljava/lang/String; = "9e3b0000-d7ab-adf3-f683-baa2a0e81612"

.field public static final WATCH_STATUS_CHARGING:I = 0x1

.field public static final WATCH_STATUS_DISCHARGING:I = 0x0

.field public static final WATCH_STATUS_FULLY_CHARGED:I = 0x2

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    const-class v0, Lcom/vectorwatch/android/utils/Helpers;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    .line 1841
    const-string v0, "00000000-0000-1000-8000-00805F9B34FB"

    .line 1842
    invoke-static {v0}, Landroid/os/ParcelUuid;->fromString(Ljava/lang/String;)Landroid/os/ParcelUuid;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/Helpers;->BASE_UUID:Landroid/os/ParcelUuid;

    .line 1841
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1368
    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method public static calculateAgeBasedOnBirthdayInMillis(J)I
    .locals 4
    .param p0, "timeStampMillis"    # J

    .prologue
    .line 561
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 562
    .local v0, "dateOfBirth":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 563
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 564
    .local v1, "now":Ljava/util/Calendar;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 565
    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/Helpers;->getYearsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v2

    return v2
.end method

.method public static checkAppStreams(Lcom/vectorwatch/android/models/DownloadedAppContainer;)Z
    .locals 7
    .param p0, "mApp"    # Lcom/vectorwatch/android/models/DownloadedAppContainer;

    .prologue
    const/4 v6, 0x1

    .line 2261
    const/4 v0, 0x0

    .line 2262
    .local v0, "appHasDefaultStreams":Z
    iget-object v5, p0, Lcom/vectorwatch/android/models/DownloadedAppContainer;->data:Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;

    iget-object v5, v5, Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v5, v5, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/vectorwatch/android/models/DownloadedAppContainer;->data:Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;

    iget-object v5, v5, Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v5, v5, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v5, v5, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    if-eqz v5, :cond_6

    .line 2263
    iget-object v5, p0, Lcom/vectorwatch/android/models/DownloadedAppContainer;->data:Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;

    iget-object v5, v5, Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v5, v5, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v4, v5, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    .line 2265
    .local v4, "watchfaces":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 2266
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v1

    .line 2267
    .local v1, "elements":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    if-eqz v1, :cond_0

    .line 2268
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 2269
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamUUID()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Element;->getDefaultStreamUUID()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2270
    const/4 v0, 0x1

    .line 2276
    .end local v3    # "j":I
    :cond_0
    if-eqz v0, :cond_3

    .line 2281
    .end local v1    # "elements":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    :cond_1
    if-eqz v0, :cond_4

    iget-object v5, p0, Lcom/vectorwatch/android/models/DownloadedAppContainer;->data:Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;

    iget-object v5, v5, Lcom/vectorwatch/android/models/DownloadedAppContainer$DownloadedAppData;->app:Lcom/vectorwatch/android/models/DownloadedAppDetails;

    iget-object v5, v5, Lcom/vectorwatch/android/models/DownloadedAppDetails;->streams:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_4

    move v5, v6

    .line 2289
    .end local v2    # "i":I
    .end local v4    # "watchfaces":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    :goto_2
    return v5

    .line 2268
    .restart local v1    # "elements":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    .restart local v2    # "i":I
    .restart local v3    # "j":I
    .restart local v4    # "watchfaces":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2265
    .end local v3    # "j":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2283
    .end local v1    # "elements":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    :cond_4
    if-nez v0, :cond_5

    move v5, v6

    .line 2284
    goto :goto_2

    .line 2286
    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    .end local v2    # "i":I
    .end local v4    # "watchfaces":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Watchface;>;"
    :cond_6
    move v5, v6

    .line 2289
    goto :goto_2
.end method

.method public static checkWatchConnectionWithAlert(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 191
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5dc

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 194
    const/4 v0, 0x0

    .line 196
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static clearBit(BB)B
    .locals 1
    .param p0, "value"    # B
    .param p1, "position"    # B

    .prologue
    .line 2541
    const/4 v0, 0x1

    shl-int/2addr v0, p1

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, p0

    int-to-byte v0, v0

    return v0
.end method

.method public static clearSettingsPreferencesDirtyFields(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 2697
    const-string v0, "activity_info_age_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2699
    const-string v0, "activity_info_gender_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2701
    const-string v0, "activity_info_height_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2703
    const-string v0, "activity_info_weight_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2705
    const-string v0, "activity_info_activity_level_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2708
    const-string v0, "settings_time_format_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2710
    const-string v0, "settings_unit_system_format_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2712
    const-string v0, "settings_greeting_name_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2715
    const-string v0, "settings_contextual_morning_face_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2717
    const-string v0, "settings_contextual_auto_discreet_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2719
    const-string v0, "settings_contextual_auto_sleep_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2721
    const-string v0, "drop_notification_option_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2723
    const-string v0, "dnd_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2724
    const-string v0, "glance_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2725
    return-void
.end method

.method public static final convertCmToKm(J)F
    .locals 6
    .param p0, "valueInCm"    # J

    .prologue
    const/high16 v5, 0x42c80000    # 100.0f

    .line 949
    long-to-float v3, p0

    const v4, 0x47c35000    # 100000.0f

    div-float v2, v3, v4

    .line 950
    .local v2, "km":F
    mul-float v3, v2, v5

    float-to-long v0, v3

    .line 951
    .local v0, "aux":J
    long-to-float v3, v0

    div-float/2addr v3, v5

    return v3
.end method

.method public static convertCmToMiles(J)F
    .locals 6
    .param p0, "valueInCm"    # J

    .prologue
    const/high16 v5, 0x42c80000    # 100.0f

    .line 937
    long-to-float v3, p0

    const v4, 0x47c35000    # 100000.0f

    div-float/2addr v3, v4

    const v4, 0x3fcdfeda

    div-float v2, v3, v4

    .line 938
    .local v2, "miles":F
    mul-float v3, v2, v5

    float-to-long v0, v3

    .line 939
    .local v0, "aux":J
    long-to-float v3, v0

    div-float/2addr v3, v5

    return v3
.end method

.method public static convertHeightFromImperialToMetric(II)I
    .locals 4
    .param p0, "feet"    # I
    .param p1, "inches"    # I

    .prologue
    .line 895
    int-to-float v1, p0

    const v2, 0x41f3d70a    # 30.48f

    mul-float/2addr v1, v2

    int-to-float v2, p1

    const v3, 0x40228f5c    # 2.54f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 896
    .local v0, "heightInCm":I
    return v0
.end method

.method public static convertHeightFromMetricToImperial(I)F
    .locals 2
    .param p0, "cms"    # I

    .prologue
    .line 906
    int-to-float v0, p0

    const v1, 0x41f3d70a    # 30.48f

    div-float/2addr v0, v1

    return v0
.end method

.method public static convertWeightFromImperialToMetric(I)I
    .locals 2
    .param p0, "valueLbs"    # I

    .prologue
    .line 883
    int-to-float v0, p0

    const v1, 0x400ccccd    # 2.2f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static convertWeightFromMetricToImperial(I)I
    .locals 2
    .param p0, "valueKg"    # I

    .prologue
    .line 873
    const v0, 0x400ccccd    # 2.2f

    int-to-float v1, p0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static createBond(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 6
    .param p0, "btDevice"    # Landroid/bluetooth/BluetoothDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1168
    if-nez p0, :cond_0

    .line 1179
    :goto_0
    return v3

    .line 1172
    :cond_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x13

    if-lt v4, v5, :cond_1

    .line 1173
    const-string v3, "CreateBond"

    const-string v4, "Bond created without reflection."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    move-result v3

    goto :goto_0

    .line 1176
    :cond_1
    const-string v4, "android.bluetooth.BluetoothDevice"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1177
    .local v0, "class1":Ljava/lang/Class;
    const-string v4, "createBond"

    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1178
    .local v1, "createBondMethod":Ljava/lang/reflect/Method;
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 1179
    .local v2, "returnValue":Ljava/lang/Boolean;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    goto :goto_0
.end method

.method public static dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V
    .locals 10
    .param p0, "currentLanguage"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v4, 0x961

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 2460
    const-string v0, "pref_locale_watch"

    invoke-static {v0, p0, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2462
    sget-object v0, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VFTP - Dispatch localization file for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2463
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2464
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    const-string v1, "localization/locale_en_uk.compressed"

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    .line 2494
    :goto_0
    return-void

    .line 2466
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ROMANIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2467
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v3

    const-string v4, "localization/locale_ro_ro.compressed"

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const/16 v7, 0xa94

    move v8, v5

    move v9, v5

    invoke-interface/range {v3 .. v9}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    goto :goto_0

    .line 2469
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->GERMAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2470
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    const-string v1, "localization/locale_de_de.compressed"

    const/4 v2, 0x2

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const/16 v4, 0xae1

    move v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    goto :goto_0

    .line 2472
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->SPANISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2473
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    const-string v1, "localization/locale_es_es.compressed"

    const/4 v2, 0x4

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const/16 v4, 0xb1a

    move v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    goto :goto_0

    .line 2475
    :cond_3
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->FRENCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2476
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    const-string v1, "localization/locale_fr_fr.compressed"

    const/4 v2, 0x3

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const/16 v4, 0xb6c

    move v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    goto :goto_0

    .line 2478
    :cond_4
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->TURKISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2479
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    const-string v1, "localization/locale_tr_tr.compressed"

    const/4 v2, 0x5

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const/16 v4, 0xa92

    move v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    goto/16 :goto_0

    .line 2481
    :cond_5
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->DUTCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2482
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    const-string v1, "localization/locale_nl_nl.compressed"

    const/16 v2, 0x8

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    const/16 v4, 0xa40

    move v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    goto/16 :goto_0

    .line 2491
    :cond_6
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v0

    const-string v1, "localization/locale_en_uk.compressed"

    sget-object v3, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    move v6, v5

    invoke-interface/range {v0 .. v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->sendFile(Ljava/lang/String;ILcom/vectorwatch/android/utils/Constants$VftpFileType;SZZ)V

    goto/16 :goto_0
.end method

.method public static displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V
    .locals 8
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "delay"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1221
    invoke-static {p2}, Lcom/vectorwatch/android/utils/Helpers;->isBluetoothEnabled(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1222
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900c1

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1225
    :cond_0
    const-string v5, "layout_inflater"

    .line 1226
    invoke-virtual {p2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 1229
    .local v3, "inflater":Landroid/view/LayoutInflater;
    invoke-static {p2, v3, p0}, Lcom/vectorwatch/android/utils/CustomNotificationDialog;->create(Landroid/content/Context;Landroid/view/LayoutInflater;Ljava/lang/String;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 1231
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 1232
    .local v0, "alert":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 1234
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    new-instance v6, Landroid/graphics/drawable/ColorDrawable;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v5, v6}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1237
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 1238
    .local v2, "handler":Landroid/os/Handler;
    new-instance v4, Lcom/vectorwatch/android/utils/Helpers$1;

    invoke-direct {v4, v0}, Lcom/vectorwatch/android/utils/Helpers$1;-><init>(Landroid/support/v7/app/AlertDialog;)V

    .line 1253
    .local v4, "runnable":Ljava/lang/Runnable;
    new-instance v5, Lcom/vectorwatch/android/utils/Helpers$2;

    invoke-direct {v5, v2, v4}, Lcom/vectorwatch/android/utils/Helpers$2;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v5}, Landroid/support/v7/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1260
    int-to-long v6, p1

    invoke-virtual {v2, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1261
    return-void
.end method

.method public static dropTable(Lcom/vectorwatch/android/utils/Helpers$TableType;Landroid/content/Context;)V
    .locals 3
    .param p0, "tableType"    # Lcom/vectorwatch/android/utils/Helpers$TableType;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1344
    sget-object v0, Lcom/vectorwatch/android/utils/Helpers$4;->$SwitchMap$com$vectorwatch$android$utils$Helpers$TableType:[I

    invoke-virtual {p0}, Lcom/vectorwatch/android/utils/Helpers$TableType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1366
    :goto_0
    return-void

    .line 1346
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_ACTIVITY:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 1350
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 1354
    :pswitch_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 1358
    :pswitch_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_ALARM:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 1362
    :pswitch_4
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_LOGS:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 1344
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static enableAllByDefault(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x1

    .line 1066
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSystemNotification(ZLandroid/content/Context;)V

    .line 1069
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAppNotification(ZLandroid/content/Context;)V

    .line 1070
    return-void
.end method

.method private static extractBytes([BII)[B
    .locals 2
    .param p0, "scanRecord"    # [B
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 1849
    new-array v0, p2, [B

    .line 1850
    .local v0, "bytes":[B
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1851
    return-object v0
.end method

.method public static getActiveGoals(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/GoalModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2596
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-static {v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v5

    float-to-int v3, v5

    .line 2597
    .local v3, "activeStepsGoal":I
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-static {v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v5

    float-to-int v0, v5

    .line 2598
    .local v0, "activeCaloriesGoal":I
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-static {v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v5

    float-to-int v1, v5

    .line 2599
    .local v1, "activeDistanceGoal":I
    sget-object v5, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-static {v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v5

    float-to-int v2, v5

    .line 2600
    .local v2, "activeSleepGoal":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2602
    .local v4, "goalList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/GoalModel;>;"
    new-instance v5, Lcom/vectorwatch/android/models/GoalModel;

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-direct {v5, v3, v6}, Lcom/vectorwatch/android/models/GoalModel;-><init>(ILcom/vectorwatch/android/utils/Constants$GoalType;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2603
    new-instance v5, Lcom/vectorwatch/android/models/GoalModel;

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-direct {v5, v0, v6}, Lcom/vectorwatch/android/models/GoalModel;-><init>(ILcom/vectorwatch/android/utils/Constants$GoalType;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2604
    new-instance v5, Lcom/vectorwatch/android/models/GoalModel;

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-direct {v5, v1, v6}, Lcom/vectorwatch/android/models/GoalModel;-><init>(ILcom/vectorwatch/android/utils/Constants$GoalType;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2605
    new-instance v5, Lcom/vectorwatch/android/models/GoalModel;

    sget-object v6, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-direct {v5, v2, v6}, Lcom/vectorwatch/android/models/GoalModel;-><init>(ILcom/vectorwatch/android/utils/Constants$GoalType;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2607
    return-object v4
.end method

.method public static getActivitySyncLastTimestamp(Landroid/content/Context;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2665
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v4

    .line 2666
    .local v4, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/vectorwatch/android/database/DatabaseManager;->getActivitySyncLastTimestamp(Lio/realm/Realm;)J

    move-result-wide v6

    long-to-int v1, v6

    .line 2669
    .local v1, "lastTsInDb":I
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 2670
    .local v2, "now":J
    const/4 v5, -0x1

    if-ne v1, v5, :cond_0

    .line 2671
    const-string v5, "pref_pair_time"

    invoke-static {v5, v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getLongPreference(Ljava/lang/String;JLandroid/content/Context;)J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v0, v6

    .line 2677
    .local v0, "lastSyncedTimestamp":I
    :goto_0
    return v0

    .line 2674
    .end local v0    # "lastSyncedTimestamp":I
    :cond_0
    move v0, v1

    .restart local v0    # "lastSyncedTimestamp":I
    goto :goto_0
.end method

.method public static getAppLanguage(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "defaultLanguage"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2219
    const-string v1, "prefs_app_language"

    invoke-static {v1, p0, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2222
    .local v0, "language":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2223
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v1}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v0

    .line 2226
    .end local v0    # "language":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public static getCaloriesBase(Landroid/content/Context;)D
    .locals 16
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 811
    const-wide/16 v4, 0x0

    .line 814
    .local v4, "base":D
    const v0, 0x1e13380

    .line 815
    .local v0, "YEAR_INTERVAL":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    .line 816
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoDateOfBirth(Landroid/content/Context;)J

    move-result-wide v12

    sub-long/2addr v10, v12

    const-wide/32 v12, 0x1e13380

    div-long/2addr v10, v12

    const-wide/16 v12, 0x3e8

    div-long v2, v10, v12

    .line 818
    .local v2, "age":J
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v9

    const-string v10, "last_known_system_info"

    const-class v11, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-virtual {v9, v10, v11}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 819
    .local v8, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-eqz v8, :cond_3

    .line 820
    invoke-virtual {v8}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v9

    invoke-virtual {v9}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v9

    mul-int/lit8 v9, v9, 0x64

    invoke-virtual {v8}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v10

    mul-int/lit8 v10, v10, 0xa

    add-int/2addr v9, v10

    .line 821
    invoke-virtual {v8}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v10

    add-int v1, v9, v10

    .line 822
    .local v1, "build":I
    const/16 v9, 0x4d

    if-lt v1, v9, :cond_1

    .line 823
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v9

    if-nez v9, :cond_0

    .line 824
    const-wide/high16 v10, 0x4019000000000000L    # 6.25

    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v12, v9

    mul-double/2addr v10, v12

    .line 825
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v9

    mul-int/lit8 v9, v9, 0xa

    int-to-double v12, v9

    add-double/2addr v10, v12

    const-wide/16 v12, 0x5

    mul-long/2addr v12, v2

    long-to-double v12, v12

    sub-double/2addr v10, v12

    const-wide v12, 0x4064200000000000L    # 161.0

    sub-double v4, v10, v12

    :goto_0
    move-wide v6, v4

    .line 851
    .end local v1    # "build":I
    .end local v4    # "base":D
    .local v6, "base":D
    :goto_1
    return-wide v6

    .line 827
    .end local v6    # "base":D
    .restart local v1    # "build":I
    .restart local v4    # "base":D
    :cond_0
    const-wide/high16 v10, 0x4019000000000000L    # 6.25

    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v12, v9

    mul-double/2addr v10, v12

    .line 828
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v9

    mul-int/lit8 v9, v9, 0xa

    int-to-double v12, v9

    add-double/2addr v10, v12

    const-wide/16 v12, 0x5

    mul-long/2addr v12, v2

    long-to-double v12, v12

    sub-double/2addr v10, v12

    const-wide/high16 v12, 0x4014000000000000L    # 5.0

    add-double v4, v10, v12

    goto :goto_0

    .line 833
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v9

    if-nez v9, :cond_2

    .line 834
    const-wide v10, 0x408478c395810625L    # 655.0955

    const-wide v12, 0x3ffd97f62b6ae7d5L    # 1.8496

    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v14, v9

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    const-wide v12, 0x40232075f6fd21ffL    # 9.5634

    .line 835
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v14, v9

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    const-wide v12, 0x4012b3d07c84b5ddL    # 4.6756

    long-to-double v14, v2

    mul-double/2addr v12, v14

    sub-double v4, v10, v12

    :goto_2
    move-wide v6, v4

    .line 841
    .end local v4    # "base":D
    .restart local v6    # "base":D
    goto :goto_1

    .line 837
    .end local v6    # "base":D
    .restart local v4    # "base":D
    :cond_2
    const-wide v10, 0x40509e45a1cac083L    # 66.473

    const-wide v12, 0x4014036113404ea5L    # 5.0033

    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v14, v9

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    const-wide v12, 0x402b80d1b71758e2L    # 13.7516

    .line 838
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v14, v9

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    const-wide v12, 0x401b051eb851eb85L    # 6.755

    long-to-double v14, v2

    mul-double/2addr v12, v14

    sub-double v4, v10, v12

    goto :goto_2

    .line 844
    .end local v1    # "build":I
    :cond_3
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v9

    if-nez v9, :cond_4

    .line 845
    const-wide v10, 0x408478c395810625L    # 655.0955

    const-wide v12, 0x3ffd97f62b6ae7d5L    # 1.8496

    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v14, v9

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    const-wide v12, 0x40232075f6fd21ffL    # 9.5634

    .line 846
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v14, v9

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    const-wide v12, 0x4012b3d07c84b5ddL    # 4.6756

    long-to-double v14, v2

    mul-double/2addr v12, v14

    sub-double v4, v10, v12

    :goto_3
    move-wide v6, v4

    .line 851
    .end local v4    # "base":D
    .restart local v6    # "base":D
    goto/16 :goto_1

    .line 848
    .end local v6    # "base":D
    .restart local v4    # "base":D
    :cond_4
    const-wide v10, 0x40509e45a1cac083L    # 66.473

    const-wide v12, 0x4014036113404ea5L    # 5.0033

    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v14, v9

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    const-wide v12, 0x402b80d1b71758e2L    # 13.7516

    .line 849
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v9

    int-to-double v14, v9

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    const-wide v12, 0x401b051eb851eb85L    # 6.755

    long-to-double v14, v2

    mul-double/2addr v12, v14

    sub-double v4, v10, v12

    goto :goto_3
.end method

.method public static getConnectionDetails(ILandroid/content/Context;)I
    .locals 5
    .param p0, "crtConnectionNumber"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 1460
    const-string v4, "counter_watch_connection_number_prev"

    invoke-static {v4, v1, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 1464
    .local v0, "prevConnectionNumber":I
    const-string v4, "counter_watch_connection_number_prev"

    invoke-static {v4, p0, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1467
    const/4 v4, 0x1

    if-eq p0, v4, :cond_0

    if-eq v0, v1, :cond_0

    if-le v0, p0, :cond_2

    if-eq p0, v1, :cond_2

    :cond_0
    move v1, v3

    .line 1481
    :cond_1
    :goto_0
    return v1

    .line 1473
    :cond_2
    if-eq p0, v1, :cond_1

    .line 1477
    if-lt p0, v2, :cond_3

    move v1, v2

    .line 1478
    goto :goto_0

    :cond_3
    move v1, v3

    .line 1481
    goto :goto_0
.end method

.method public static getContactName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 200
    const/4 v7, 0x0

    .line 202
    .local v7, "name":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "_id"

    aput-object v4, v2, v0

    .line 206
    .local v2, "projection":[Ljava/lang/String;
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 209
    .local v1, "contactUri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 211
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 212
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    const-string v0, "display_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 214
    sget-object v0, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Started uploadcontactphoto: Contact Found @ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 219
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object p1, v7

    .line 221
    .end local p1    # "number":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 216
    .restart local p1    # "number":Ljava/lang/String;
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Contact Not Found @ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getCurrentTime()I
    .locals 4

    .prologue
    .line 2686
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 2687
    .local v0, "now":J
    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    long-to-int v2, v2

    return v2
.end method

.method public static getCurrentUpdateState(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1044
    const-string v0, "update_watch_status"

    const/4 v1, -0x1

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public static getCurrentWatchAppsState(Landroid/content/Context;)Lcom/vectorwatch/android/models/UpdateDefaultsModel;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2088
    new-instance v15, Lcom/vectorwatch/android/models/UpdateDefaultsModel;

    invoke-direct {v15}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;-><init>()V

    .line 2091
    .local v15, "updateDefaultsModel":Lcom/vectorwatch/android/models/UpdateDefaultsModel;
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 2092
    .local v10, "shape":Ljava/lang/String;
    const-string v18, "round"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 2093
    sget-object v18, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->setCompatibility(Ljava/lang/String;)V

    .line 2099
    :goto_0
    const-string v18, "2.0.2"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->setAppVersion(Ljava/lang/String;)V

    .line 2102
    const-string v18, "android"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->setDeviceType(Ljava/lang/String;)V

    .line 2105
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v18

    const-string v19, "last_known_system_info"

    const-class v20, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-virtual/range {v18 .. v20}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 2107
    .local v17, "watchOsVersion":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    const-string v18, "kernel"

    invoke-static/range {v17 .. v18}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->setKernelVersion(Ljava/lang/String;)V

    .line 2110
    const-string v18, "b5957a0"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->setBuildNumber(Ljava/lang/String;)V

    .line 2112
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppIdsInRunningOrder(Landroid/content/Context;)Ljava/util/List;

    move-result-object v5

    .line 2113
    .local v5, "appIdsInOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    sget-object v18, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamsWithGivenState(Lcom/vectorwatch/android/models/Stream$State;Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 2116
    .local v2, "activeStreams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2118
    .local v8, "currentApps":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateAppDetailsModel;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 2119
    .local v4, "appId":Ljava/lang/Integer;
    sget-object v19, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SETUP: App = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2120
    new-instance v3, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;

    invoke-direct {v3}, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;-><init>()V

    .line 2123
    .local v3, "appDetails":Lcom/vectorwatch/android/models/UpdateAppDetailsModel;
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppUuid(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 2124
    .local v6, "appUuid":Ljava/lang/String;
    invoke-virtual {v3, v6}, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;->setAppUuid(Ljava/lang/String;)V

    .line 2127
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 2129
    .local v13, "streamChannels":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateStreamChannelModel;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_0
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/vectorwatch/android/models/Stream;

    .line 2130
    .local v11, "stream":Lcom/vectorwatch/android/models/Stream;
    sget-object v20, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "SETUP: Stream = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget-object v0, v11, Lcom/vectorwatch/android/models/Stream;->name:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2131
    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v9

    .line 2133
    .local v9, "placementModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    if-eqz v9, :cond_0

    .line 2135
    invoke-virtual {v9}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v7

    .line 2136
    .local v7, "channelAppId":Ljava/lang/Integer;
    if-eqz v7, :cond_0

    if-ne v7, v4, :cond_0

    .line 2138
    new-instance v16, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;

    invoke-direct/range {v16 .. v16}, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;-><init>()V

    .line 2139
    .local v16, "updateStreamChannelModel":Lcom/vectorwatch/android/models/UpdateStreamChannelModel;
    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;->setStreamUuid(Ljava/lang/String;)V

    .line 2142
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 2143
    .local v14, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    invoke-interface {v14, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2146
    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    .line 2148
    .local v12, "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    if-eqz v12, :cond_2

    .line 2149
    invoke-virtual {v12, v14}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setSubscriptions(Ljava/util/List;)V

    .line 2150
    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UpdateStreamChannelModel;->setStreamChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 2152
    sget-object v20, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "SETUP: SYNC DEFAULTS: App = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " stream: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget-object v0, v11, Lcom/vectorwatch/android/models/Stream;->name:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " placement: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    .line 2153
    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " unique label: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    .line 2154
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 2152
    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2156
    move-object/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2095
    .end local v2    # "activeStreams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .end local v3    # "appDetails":Lcom/vectorwatch/android/models/UpdateAppDetailsModel;
    .end local v4    # "appId":Ljava/lang/Integer;
    .end local v5    # "appIdsInOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "appUuid":Ljava/lang/String;
    .end local v7    # "channelAppId":Ljava/lang/Integer;
    .end local v8    # "currentApps":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateAppDetailsModel;>;"
    .end local v9    # "placementModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    .end local v11    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v12    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v13    # "streamChannels":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateStreamChannelModel;>;"
    .end local v14    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    .end local v16    # "updateStreamChannelModel":Lcom/vectorwatch/android/models/UpdateStreamChannelModel;
    .end local v17    # "watchOsVersion":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :cond_1
    sget-object v18, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->setCompatibility(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2158
    .restart local v2    # "activeStreams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    .restart local v3    # "appDetails":Lcom/vectorwatch/android/models/UpdateAppDetailsModel;
    .restart local v4    # "appId":Ljava/lang/Integer;
    .restart local v5    # "appIdsInOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "appUuid":Ljava/lang/String;
    .restart local v7    # "channelAppId":Ljava/lang/Integer;
    .restart local v8    # "currentApps":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateAppDetailsModel;>;"
    .restart local v9    # "placementModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    .restart local v11    # "stream":Lcom/vectorwatch/android/models/Stream;
    .restart local v12    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .restart local v13    # "streamChannels":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateStreamChannelModel;>;"
    .restart local v14    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    .restart local v16    # "updateStreamChannelModel":Lcom/vectorwatch/android/models/UpdateStreamChannelModel;
    .restart local v17    # "watchOsVersion":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :cond_2
    sget-object v20, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v21, "Stream channel settings null"

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2164
    .end local v7    # "channelAppId":Ljava/lang/Integer;
    .end local v9    # "placementModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    .end local v11    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v12    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v14    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    .end local v16    # "updateStreamChannelModel":Lcom/vectorwatch/android/models/UpdateStreamChannelModel;
    :cond_3
    invoke-virtual {v3, v13}, Lcom/vectorwatch/android/models/UpdateAppDetailsModel;->setStreamChannels(Ljava/util/List;)V

    .line 2166
    invoke-interface {v8, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2169
    .end local v3    # "appDetails":Lcom/vectorwatch/android/models/UpdateAppDetailsModel;
    .end local v4    # "appId":Ljava/lang/Integer;
    .end local v6    # "appUuid":Ljava/lang/String;
    .end local v13    # "streamChannels":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UpdateStreamChannelModel;>;"
    :cond_4
    invoke-virtual {v15, v8}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->setCurrentApps(Ljava/util/List;)V

    .line 2171
    return-object v15
.end method

.method public static getDistanceWithCrtUnitSystem(Landroid/content/Context;F)F
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "distance"    # F

    .prologue
    .line 2755
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2756
    const/high16 v1, 0x447a0000    # 1000.0f

    div-float v0, p1, v1

    .line 2760
    .local v0, "distanceTransformed":F
    :goto_0
    return v0

    .line 2758
    .end local v0    # "distanceTransformed":F
    :cond_0
    const v1, 0x44184000    # 609.0f

    div-float v0, p1, v1

    .restart local v0    # "distanceTransformed":F
    goto :goto_0
.end method

.method public static getIntNumberOfFeet(F)I
    .locals 1
    .param p0, "feet"    # F

    .prologue
    .line 927
    float-to-int v0, p0

    return v0
.end method

.method public static getIntensityPercentage(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2617
    const-string v2, "pref_backlight_intensity"

    const/4 v3, 0x1

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 2619
    .local v0, "intensity":I
    add-int/lit8 v2, v0, 0x1

    sget v3, Lcom/vectorwatch/android/utils/Constants;->BACKLIGHT_INTENSITY_STEP_SIZE:I

    mul-int v1, v2, v3

    .line 2620
    .local v1, "intensityPercentage":I
    return v1
.end method

.method public static getLocaleString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2294
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2295
    .local v0, "current":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getMaxValueForField(Landroid/content/Context;I)F
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "adjustedMax"    # I

    .prologue
    .line 1405
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/Helpers;->hasPersonalInfo(Landroid/content/Context;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 1406
    const v8, 0x48127c00    # 150000.0f

    .line 1433
    :cond_0
    :goto_0
    return v8

    .line 1409
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v20

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    mul-double v4, v20, v22

    .line 1410
    .local v4, "adjustedValue":D
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v9

    .line 1412
    .local v9, "height":I
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/Helpers;->getCaloriesBase(Landroid/content/Context;)D

    move-result-wide v6

    .line 1413
    .local v6, "bmr":D
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v15

    if-nez v15, :cond_2

    const-wide v10, 0x3fda8f5c28f5c28fL    # 0.415

    .line 1415
    .local v10, "distanceFactor":D
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v15

    int-to-double v0, v15

    move-wide/from16 v20, v0

    mul-double v16, v10, v20

    .line 1416
    .local v16, "stride":D
    const-wide/high16 v20, 0x3ff8000000000000L    # 1.5

    mul-double v20, v20, v16

    const-wide/high16 v22, 0x4059000000000000L    # 100.0

    div-double v20, v20, v22

    const-wide v22, 0x400ccccccccccccdL    # 3.6

    mul-double v20, v20, v22

    const-wide/high16 v22, 0x3fe4000000000000L    # 0.625

    mul-double v12, v20, v22

    .line 1417
    .local v12, "speed":D
    const-wide v20, 0x3f654c985f06f694L    # 0.0026

    const-wide/high16 v22, 0x4010000000000000L    # 4.0

    move-wide/from16 v0, v22

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v22

    mul-double v20, v20, v22

    const-wide v22, 0x3fb23a29c779a6b5L    # 0.0712

    const-wide/high16 v24, 0x4008000000000000L    # 3.0

    move-wide/from16 v0, v24

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    mul-double v22, v22, v24

    sub-double v20, v20, v22

    const-wide v22, 0x3fe3cfaacd9e83e4L    # 0.6191

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    move-wide/from16 v0, v24

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    mul-double v22, v22, v24

    add-double v20, v20, v22

    const-wide v22, 0x3fd56872b020c49cL    # 0.3345

    mul-double v22, v22, v12

    sub-double v20, v20, v22

    const-wide v22, 0x3ff0b780346dc5d6L    # 1.0448

    add-double v2, v20, v22

    .line 1420
    .local v2, "MET":D
    const-wide v20, 0x4096800000000000L    # 1440.0

    sub-double v22, v4, v6

    mul-double v20, v20, v22

    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    sub-double v22, v2, v22

    mul-double v22, v22, v6

    div-double v18, v20, v22

    .line 1421
    .local v18, "walkingMinutes":D
    const-wide/high16 v20, 0x3ff8000000000000L    # 1.5

    mul-double v20, v20, v18

    const-wide/high16 v22, 0x404e000000000000L    # 60.0

    mul-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v14, v0

    .line 1423
    .local v14, "steps":F
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/Helpers;->isMetricFormat(Landroid/content/Context;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1424
    float-to-double v0, v14

    move-wide/from16 v20, v0

    mul-double v20, v20, v10

    int-to-double v0, v9

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    const-wide v22, 0x40f86a0000000000L    # 100000.0

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v15, v0

    const/high16 v20, 0x447a0000    # 1000.0f

    mul-float v15, v15, v20

    const/high16 v20, 0x447a0000    # 1000.0f

    div-float v8, v15, v20

    .line 1429
    .local v8, "distance":F
    :goto_2
    const/4 v15, 0x2

    move/from16 v0, p1

    if-eq v0, v15, :cond_0

    move v8, v14

    .line 1433
    goto/16 :goto_0

    .line 1413
    .end local v2    # "MET":D
    .end local v8    # "distance":F
    .end local v10    # "distanceFactor":D
    .end local v12    # "speed":D
    .end local v14    # "steps":F
    .end local v16    # "stride":D
    .end local v18    # "walkingMinutes":D
    :cond_2
    const-wide v10, 0x3fda6e978d4fdf3bL    # 0.413

    goto/16 :goto_1

    .line 1426
    .restart local v2    # "MET":D
    .restart local v10    # "distanceFactor":D
    .restart local v12    # "speed":D
    .restart local v14    # "steps":F
    .restart local v16    # "stride":D
    .restart local v18    # "walkingMinutes":D
    :cond_3
    float-to-double v0, v14

    move-wide/from16 v20, v0

    mul-double v20, v20, v10

    int-to-double v0, v9

    move-wide/from16 v22, v0

    mul-double v20, v20, v22

    const-wide v22, 0x40f86a0000000000L    # 100000.0

    div-double v20, v20, v22

    const-wide v22, 0x4083080000000000L    # 609.0

    mul-double v20, v20, v22

    const-wide v22, 0x408f400000000000L    # 1000.0

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v8, v0

    .restart local v8    # "distance":F
    goto :goto_2
.end method

.method public static getModelForVosUpdateRequest(Landroid/content/Context;)Lcom/vectorwatch/android/models/WatchOsDetailsModel;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 1856
    if-nez p0, :cond_0

    move-object v5, v6

    .line 1904
    :goto_0
    return-object v5

    .line 1860
    :cond_0
    new-instance v5, Lcom/vectorwatch/android/models/WatchOsDetailsModel;

    invoke-direct {v5}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;-><init>()V

    .line 1864
    .local v5, "watchOsDetailsModel":Lcom/vectorwatch/android/models/WatchOsDetailsModel;
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchOldCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1865
    .local v3, "watchCPUId":Ljava/lang/String;
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 1868
    .local v4, "watchNewCpuId":Ljava/lang/String;
    sget-object v7, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Update CPU ID "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " | new cpu: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1869
    invoke-virtual {v5, v3}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->setCpuId(Ljava/lang/String;)V

    .line 1870
    invoke-virtual {v5, v4}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->setNewCpuId(Ljava/lang/String;)V

    .line 1873
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v7

    const-string v8, "last_known_system_info"

    const-class v9, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 1874
    invoke-virtual {v7, v8, v9}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 1876
    .local v1, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-nez v1, :cond_1

    move-object v5, v6

    .line 1878
    goto :goto_0

    .line 1882
    :cond_1
    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getCurrent()B

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    .line 1883
    const-string v6, "kernel"

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->setCurrentSystemType(Ljava/lang/String;)V

    .line 1891
    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    .line 1892
    const-string v6, "kernel"

    invoke-static {v1, v6}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1893
    .local v2, "kernelVersion":Ljava/lang/String;
    invoke-virtual {v5, v2}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->setKernelVersion(Ljava/lang/String;)V

    .line 1895
    const-string v6, "bootloader"

    invoke-static {v1, v6}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1896
    .local v0, "bootVersion":Ljava/lang/String;
    invoke-virtual {v5, v0}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->setBootloaderVersion(Ljava/lang/String;)V

    .line 1900
    .end local v0    # "bootVersion":Ljava/lang/String;
    .end local v2    # "kernelVersion":Ljava/lang/String;
    :cond_3
    const-string v6, "2.0.2"

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->setAppVersion(Ljava/lang/String;)V

    .line 1901
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getUniqueSerialNumber(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->setBtName(Ljava/lang/String;)V

    goto :goto_0

    .line 1885
    :cond_4
    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getCurrent()B

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 1886
    const-string v6, "bootloader"

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/models/WatchOsDetailsModel;->setCurrentSystemType(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getNeedleAngleForAlarm(II)F
    .locals 4
    .param p0, "hours"    # I
    .param p1, "minutes"    # I

    .prologue
    .line 2421
    move v1, p0

    .line 2425
    .local v1, "numberOfHours":I
    const/16 v3, 0xc

    if-le p0, v3, :cond_0

    .line 2426
    rsub-int/lit8 v1, p0, 0xc

    .line 2429
    :cond_0
    mul-int/lit8 v3, v1, 0x3c

    add-int v2, p1, v3

    .line 2430
    .local v2, "numberOfMinutes":I
    div-int/lit8 v3, v2, 0x2

    add-int/lit16 v3, v3, -0x168

    int-to-float v0, v3

    .line 2432
    .local v0, "angle":F
    return v0
.end method

.method public static getNoInchesFromNoFeet(F)I
    .locals 1
    .param p0, "feet"    # F

    .prologue
    .line 917
    const/high16 v0, 0x41400000    # 12.0f

    mul-float/2addr v0, p0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static getPhoneBatteryLevel(Landroid/content/Context;)F
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/high16 v7, 0x41800000    # 16.0f

    const/high16 v6, 0x41600000    # 14.0f

    const/4 v11, -0x1

    .line 1630
    const/4 v8, 0x0

    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 1631
    .local v1, "batteryIntent":Landroid/content/Intent;
    if-nez v1, :cond_1

    move v0, v6

    .line 1660
    :cond_0
    :goto_0
    return v0

    .line 1635
    :cond_1
    const-string v8, "level"

    invoke-virtual {v1, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1636
    .local v3, "level":I
    const-string v8, "scale"

    invoke-virtual {v1, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 1638
    .local v5, "scale":I
    const-string v8, "plugged"

    invoke-virtual {v1, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 1639
    .local v4, "plugged":I
    if-eq v4, v2, :cond_2

    const/4 v8, 0x2

    if-eq v4, v8, :cond_2

    const/4 v8, 0x4

    if-ne v4, v8, :cond_4

    .line 1643
    .local v2, "isPlugged":Z
    :cond_2
    :goto_1
    if-eq v3, v11, :cond_3

    if-ne v5, v11, :cond_6

    .line 1644
    :cond_3
    if-eqz v2, :cond_5

    move v0, v7

    .line 1645
    goto :goto_0

    .line 1639
    .end local v2    # "isPlugged":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .restart local v2    # "isPlugged":Z
    :cond_5
    move v0, v6

    .line 1647
    goto :goto_0

    .line 1651
    :cond_6
    int-to-float v8, v3

    int-to-float v9, v5

    div-float/2addr v8, v9

    const/high16 v9, 0x42c80000    # 100.0f

    mul-float v0, v8, v9

    .line 1652
    .local v0, "battery":F
    const/high16 v8, 0x41700000    # 15.0f

    cmpg-float v8, v0, v8

    if-gez v8, :cond_0

    .line 1653
    if-eqz v2, :cond_7

    move v0, v7

    .line 1654
    goto :goto_0

    :cond_7
    move v0, v6

    .line 1656
    goto :goto_0
.end method

.method public static getRetrofitResponseBody(Lretrofit/client/Response;)Ljava/lang/String;
    .locals 8
    .param p0, "response"    # Lretrofit/client/Response;

    .prologue
    .line 2794
    const/4 v2, 0x0

    .line 2795
    .local v2, "reader":Ljava/io/BufferedReader;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2798
    .local v5, "sb":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Lretrofit/client/Response;->getBody()Lretrofit/mime/TypedInput;

    move-result-object v7

    invoke-interface {v7}, Lretrofit/mime/TypedInput;->in()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2803
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .local v1, "line":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 2804
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2806
    .end local v1    # "line":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2807
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    move-object v2, v3

    .line 2814
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2815
    .local v4, "result":Ljava/lang/String;
    return-object v4

    .line 2809
    .end local v4    # "result":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 2810
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 2809
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_2
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method public static getSecondsDifferenceBetweenTimezones(Ljava/lang/String;Ljava/lang/String;)I
    .locals 12
    .param p0, "timeZoneA"    # Ljava/lang/String;
    .param p1, "timeZoneB"    # Ljava/lang/String;

    .prologue
    .line 1151
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1152
    .local v6, "systemTimeInMillis":J
    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    int-to-long v8, v3

    .line 1153
    .local v8, "timezoneTimeInMillis":J
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    int-to-long v4, v3

    .line 1154
    .local v4, "localTimeMillis":J
    sub-long v0, v4, v8

    .line 1155
    .local v0, "differenceMillis":J
    long-to-int v3, v0

    div-int/lit16 v2, v3, 0x3e8

    .line 1156
    .local v2, "differenceSecs":I
    sget-object v3, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "time difference between "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " and "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1157
    return v2
.end method

.method public static getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "info"    # Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v3, -0x1

    .line 1944
    if-nez p0, :cond_1

    .line 1945
    sget-object v1, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v2, "System info should not be null when trying to get the version."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1946
    const/4 v0, 0x0

    .line 1984
    :cond_0
    :goto_0
    return-object v0

    .line 1949
    :cond_1
    const/4 v0, 0x0

    .line 1950
    .local v0, "version":Ljava/lang/String;
    if-eqz p1, :cond_3

    .line 1951
    const-string v1, "kernel"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1952
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1953
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1955
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 1957
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1963
    :cond_2
    const-string v1, "bootloader"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1964
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1965
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1967
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 1969
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1977
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMinorVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1978
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getHotfixVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1979
    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 1981
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getBuildVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static getTimeOfDay(I)I
    .locals 1
    .param p0, "hour"    # I

    .prologue
    .line 2406
    const/16 v0, 0xc

    if-lt p0, v0, :cond_0

    .line 2407
    const/4 v0, 0x1

    .line 2410
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static getTimezoneOffset()I
    .locals 6

    .prologue
    .line 2582
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    .line 2583
    .local v2, "tz":Ljava/util/TimeZone;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 2584
    .local v0, "now":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    div-int/lit16 v1, v3, 0x3e8

    .line 2586
    .local v1, "offsetFromUtc":I
    return v1
.end method

.method public static getUniqueSerialNumber(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1022
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWatchShapeFromName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "watchName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1270
    if-eqz p0, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x53

    if-ne v0, v1, :cond_0

    .line 1271
    const-string v0, "square"

    .line 1280
    :goto_0
    return-object v0

    .line 1273
    :cond_0
    if-eqz p0, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x52

    if-ne v0, v1, :cond_1

    .line 1274
    const-string v0, "round"

    goto :goto_0

    .line 1276
    :cond_1
    sget-object v0, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v1, "Should be able to determine a watch shape based on its name."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1280
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public static getWatchTimeoutDuration(I)I
    .locals 1
    .param p0, "timeoutIndex"    # I

    .prologue
    .line 2631
    packed-switch p0, :pswitch_data_0

    .line 2653
    const/4 v0, 0x2

    .line 2655
    .local v0, "watchTimeout":I
    :goto_0
    return v0

    .line 2633
    .end local v0    # "watchTimeout":I
    :pswitch_0
    const/4 v0, 0x2

    .line 2634
    .restart local v0    # "watchTimeout":I
    goto :goto_0

    .line 2637
    .end local v0    # "watchTimeout":I
    :pswitch_1
    const/4 v0, 0x5

    .line 2638
    .restart local v0    # "watchTimeout":I
    goto :goto_0

    .line 2641
    .end local v0    # "watchTimeout":I
    :pswitch_2
    const/16 v0, 0xa

    .line 2642
    .restart local v0    # "watchTimeout":I
    goto :goto_0

    .line 2645
    .end local v0    # "watchTimeout":I
    :pswitch_3
    const/16 v0, 0x14

    .line 2646
    .restart local v0    # "watchTimeout":I
    goto :goto_0

    .line 2649
    .end local v0    # "watchTimeout":I
    :pswitch_4
    const/16 v0, 0x1e

    .line 2650
    .restart local v0    # "watchTimeout":I
    goto :goto_0

    .line 2631
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getYearsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 5
    .param p0, "first"    # Ljava/util/Calendar;
    .param p1, "last"    # Ljava/util/Calendar;

    .prologue
    const/4 v4, 0x5

    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 576
    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int v0, v1, v2

    .line 577
    .local v0, "diff":I
    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 578
    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 579
    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-le v1, v2, :cond_1

    .line 580
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 582
    :cond_1
    return v0
.end method

.method public static goToLogin(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2311
    if-nez p0, :cond_0

    .line 2318
    :goto_0
    return-void

    .line 2315
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2316
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2317
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static goToMain(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2300
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2301
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2302
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2303
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 2304
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 2305
    return-void
.end method

.method public static handleRetrofitError(Landroid/content/Context;Lretrofit/RetrofitError;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    const v7, 0x7f090114

    const/16 v6, 0xbb8

    .line 2770
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v0

    .line 2771
    .local v0, "response":Lretrofit/client/Response;
    if-eqz v0, :cond_1

    .line 2772
    invoke-virtual {v0}, Lretrofit/client/Response;->getStatus()I

    move-result v1

    .line 2773
    .local v1, "status":I
    invoke-static {v1}, Lcom/vectorwatch/android/utils/CloudStatusCodes;->getDefaultStringIdForStatus(I)I

    move-result v2

    .line 2774
    .local v2, "statusCodeMessageResId":I
    sget-object v3, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SIGNUP: error = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2776
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 2777
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v6, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 2789
    .end local v1    # "status":I
    .end local v2    # "statusCodeMessageResId":I
    :goto_0
    return-void

    .line 2781
    .restart local v1    # "status":I
    .restart local v2    # "statusCodeMessageResId":I
    :cond_0
    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v6, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 2786
    .end local v1    # "status":I
    .end local v2    # "statusCodeMessageResId":I
    :cond_1
    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v6, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0
.end method

.method public static hasPersonalInfo(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, -0x1

    const/4 v0, 0x0

    .line 1374
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v6, :cond_1

    .line 1390
    :cond_0
    :goto_0
    return v0

    .line 1378
    :cond_1
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoDateOfBirth(Landroid/content/Context;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1382
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v1

    if-eq v1, v6, :cond_0

    .line 1386
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v1

    if-eq v1, v6, :cond_0

    .line 1390
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static hexStringToByteArray(Ljava/lang/String;)[B
    .locals 7
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x10

    .line 2566
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 2568
    .local v2, "len":I
    rem-int/lit8 v3, v2, 0x2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 2569
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2572
    :cond_0
    div-int/lit8 v3, v2, 0x2

    rem-int/lit8 v4, v2, 0x2

    add-int/2addr v3, v4

    new-array v0, v3, [B

    .line 2574
    .local v0, "data":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 2575
    div-int/lit8 v3, v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 2574
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 2578
    :cond_1
    return-object v0
.end method

.method public static is24hFormat(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 260
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTimeFormatPreference(Landroid/content/Context;)I

    move-result v0

    .line 261
    .local v0, "timeFormat":I
    if-ne v0, v1, :cond_0

    .line 263
    const/4 v1, 0x0

    .line 267
    :cond_0
    return v1
.end method

.method public static isAlphaBuild()Z
    .locals 2

    .prologue
    .line 2350
    const-string v0, "2.0.2"

    const-string v1, "a"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2351
    const/4 v0, 0x1

    .line 2353
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBetaBuild()Z
    .locals 2

    .prologue
    .line 2358
    const-string v0, "2.0.2"

    const-string v1, "b"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2359
    const/4 v0, 0x1

    .line 2361
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBitSet(BB)Z
    .locals 2
    .param p0, "value"    # B
    .param p1, "position"    # B

    .prologue
    const/4 v0, 0x1

    .line 2552
    shl-int v1, v0, p1

    and-int/2addr v1, p0

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isBluetoothEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1670
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 1671
    .local v0, "mBluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-nez v0, :cond_1

    .line 1680
    :cond_0
    :goto_0
    return v1

    .line 1675
    :cond_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1680
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isCurrentLanguageSupported(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2825
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getLocaleString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2827
    .local v1, "locale":Ljava/lang/String;
    sget-object v5, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NOTIFICATIONS DISPLAY: supported language? current = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2829
    const/16 v5, 0x9

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "ja"

    aput-object v5, v2, v3

    const-string v5, "ko"

    aput-object v5, v2, v4

    const/4 v5, 0x2

    const-string v6, "he"

    aput-object v6, v2, v5

    const/4 v5, 0x3

    const-string v6, "hi"

    aput-object v6, v2, v5

    const/4 v5, 0x4

    const-string/jumbo v6, "zh"

    aput-object v6, v2, v5

    const/4 v5, 0x5

    const-string v6, "ar"

    aput-object v6, v2, v5

    const/4 v5, 0x6

    const-string v6, "iw"

    aput-object v6, v2, v5

    const/4 v5, 0x7

    const-string v6, "mn"

    aput-object v6, v2, v5

    const/16 v5, 0x8

    const-string v6, "ur"

    aput-object v6, v2, v5

    .line 2831
    .local v2, "unsupportedLanguages":[Ljava/lang/String;
    array-length v6, v2

    move v5, v3

    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v0, v2, v5

    .line 2832
    .local v0, "language":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2837
    .end local v0    # "language":Ljava/lang/String;
    :goto_1
    return v3

    .line 2831
    .restart local v0    # "language":Ljava/lang/String;
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .end local v0    # "language":Ljava/lang/String;
    :cond_1
    move v3, v4

    .line 2837
    goto :goto_1
.end method

.method public static isDefaultLanguageSelected(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 2870
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->getAppLanguage(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2872
    .local v0, "language":Ljava/lang/String;
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ENGLISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2896
    :cond_0
    :goto_0
    return v1

    .line 2876
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->TURKISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2880
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->SPANISH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2884
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->FRENCH:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2888
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->GERMAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2892
    sget-object v2, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->ROMANIAN:Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;->getShortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2896
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isDevBuild()Z
    .locals 2

    .prologue
    .line 2342
    const-string v0, "2.0.2"

    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2343
    const/4 v0, 0x1

    .line 2345
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isForceOta(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1908
    const-string v0, "flag_is_force_ota"

    const/4 v1, 0x0

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static isInternetEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 996
    invoke-static {p0}, Lcom/vectorwatch/android/utils/NetworkUtils;->getConnectivityStatus(Landroid/content/Context;)I

    move-result v0

    sget v1, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_NOT_CONNECTED:I

    if-ne v0, v1, :cond_0

    .line 997
    const/4 v0, 0x0

    .line 999
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isLiveStream(Lcom/vectorwatch/android/models/Stream;)Z
    .locals 4
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 2734
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2735
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/settings/Setting;

    .line 2736
    .local v0, "setting":Lcom/vectorwatch/android/models/settings/Setting;
    iget-object v2, v0, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    sget-object v3, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->LIVE_STREAM:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->getVal()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2737
    const/4 v1, 0x1

    .line 2742
    .end local v0    # "setting":Lcom/vectorwatch/android/models/settings/Setting;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isMetricFormat(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 277
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getUnitSystemFormatPreference(Landroid/content/Context;)I

    move-result v0

    .line 278
    .local v0, "unitSystemFormat":I
    if-ne v0, v1, :cond_0

    .line 279
    const/4 v1, 0x0

    .line 283
    :cond_0
    return v1
.end method

.method public static isMobilePairedToWatch(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1009
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1010
    const/4 v0, 0x1

    .line 1012
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1609
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1610
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const-string v3, "enabled_notification_listeners"

    invoke-static {v0, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1611
    .local v1, "enabledNotificationListeners":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 1615
    .local v2, "packageName":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1618
    :cond_0
    const/4 v3, 0x0

    .line 1620
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static isProductionBuild()Z
    .locals 1

    .prologue
    .line 2366
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isDevBuild()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2367
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isAlphaBuild()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2368
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isBetaBuild()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2369
    const/4 v0, 0x1

    .line 2371
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSystemApp(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 7
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2183
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 2187
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v2, p0, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 2189
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    iget v5, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    .line 2190
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found system app: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2199
    .end local v0    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    return v3

    .restart local v0    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :cond_0
    move v3, v4

    .line 2194
    goto :goto_0

    .line 2195
    .end local v0    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v1

    .line 2196
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isSystemInfoWith4Decimals()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1918
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v5

    const-string v6, "last_known_system_info"

    const-class v7, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 1919
    invoke-virtual {v5, v6, v7}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 1921
    .local v1, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-nez v1, :cond_1

    .line 1932
    :cond_0
    :goto_0
    return v3

    .line 1926
    :cond_1
    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getKernelInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v2

    .line 1927
    .local v2, "kernelMajor":I
    invoke-virtual {v1}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->getBootloaderInfo()Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vectorwatch/com/android/vos/update/SystemInfo$System;->getMajorVersion()I

    move-result v0

    .line 1928
    .local v0, "bootloaderMajor":I
    if-lt v2, v4, :cond_0

    if-lt v0, v4, :cond_0

    move v3, v4

    .line 1929
    goto :goto_0
.end method

.method public static isWatchConnected()Z
    .locals 1

    .prologue
    .line 1078
    invoke-static {}, Lcom/vectorwatch/android/service/ble/BleService;->getIsConnectedToWatch()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1079
    const/4 v0, 0x1

    .line 1081
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static logoutDrivenResets(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 2030
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAccountAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2031
    .local v1, "username":Ljava/lang/String;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->userLogOut(Ljava/lang/String;Landroid/accounts/AccountManager;)Z

    move-result v0

    .line 2033
    .local v0, "isLoggedOut":Z
    if-eqz v0, :cond_1

    .line 2034
    const-string v3, "account_update_type"

    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2037
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->isStreamLiveManagerInitialized()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2038
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/managers/LiveStreamManager;->clearLiveStreamManager()V

    .line 2041
    :cond_0
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->resetUserSettings(Landroid/content/Context;)V

    .line 2042
    sget-object v3, Lcom/vectorwatch/android/utils/Helpers$TableType;->ALARMS:Lcom/vectorwatch/android/utils/Helpers$TableType;

    invoke-static {v3, p0}, Lcom/vectorwatch/android/utils/Helpers;->dropTable(Lcom/vectorwatch/android/utils/Helpers$TableType;Landroid/content/Context;)V

    .line 2043
    sget-object v3, Lcom/vectorwatch/android/utils/Helpers$TableType;->STREAM:Lcom/vectorwatch/android/utils/Helpers$TableType;

    invoke-static {v3, p0}, Lcom/vectorwatch/android/utils/Helpers;->dropTable(Lcom/vectorwatch/android/utils/Helpers$TableType;Landroid/content/Context;)V

    .line 2044
    sget-object v3, Lcom/vectorwatch/android/utils/Helpers$TableType;->CLOUD:Lcom/vectorwatch/android/utils/Helpers$TableType;

    invoke-static {v3, p0}, Lcom/vectorwatch/android/utils/Helpers;->dropTable(Lcom/vectorwatch/android/utils/Helpers$TableType;Landroid/content/Context;)V

    .line 2047
    invoke-static {v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAccountAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 2048
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLoggedInStatus(ZLandroid/content/Context;)V

    .line 2049
    const-string v3, "flag_changed_contextual"

    invoke-static {v3, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2050
    const-string v3, "flag_sync_settings_to_cloud"

    invoke-static {v3, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2052
    const/4 v2, 0x1

    .line 2054
    :cond_1
    return v2
.end method

.method public static mandatoryRequestsAtConnect(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, -0x1

    .line 1994
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendTime(Landroid/content/Context;)Ljava/util/UUID;

    .line 1997
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchOldCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "flag_get_cpu_id"

    const/4 v2, 0x0

    .line 1998
    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1999
    :cond_0
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendUuidRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 2003
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v1

    const-string v2, "last_known_system_info"

    const-class v3, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    invoke-virtual {v1, v2, v3}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 2005
    .local v0, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-nez v0, :cond_2

    .line 2006
    sget-object v1, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v2, "REQUEST - get system info mandatory request at connect"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2007
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSystemInfoRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 2010
    :cond_2
    const-string v1, "pref_serial_number"

    const/4 v2, 0x0

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2012
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSerialNumberRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 2016
    :cond_3
    const-string v1, "last_synced_battery_status"

    invoke-static {v1, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v1

    if-ne v1, v4, :cond_4

    .line 2018
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBatteryRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 2020
    :cond_4
    return-void
.end method

.method public static needsLogin(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2321
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 2322
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v4, "com.vectorwatch.android"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 2324
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v4, v1

    if-ge v4, v2, :cond_0

    .line 2329
    :goto_0
    return v2

    .line 2327
    :cond_0
    aget-object v4, v1, v3

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAccountAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 2328
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLoggedInStatus(ZLandroid/content/Context;)V

    move v2, v3

    .line 2329
    goto :goto_0
.end method

.method public static notifyCloud(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1399
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1400
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1401
    return-void
.end method

.method public static packContextualSettingsForUpload(Landroid/content/Context;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/ContextualItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 789
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 791
    .local v0, "contextualItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/ContextualItem;>;"
    new-instance v1, Lcom/vectorwatch/android/models/ContextualItem;

    const-string v2, "MORNING_FACE"

    .line 792
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualMorningFaceSettings(Landroid/content/Context;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/models/ContextualItem;-><init>(Ljava/lang/String;Z)V

    .line 791
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 793
    new-instance v1, Lcom/vectorwatch/android/models/ContextualItem;

    const-string v2, "AUTO_DISCREET_MODE"

    .line 794
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualAutoDiscreetSettings(Landroid/content/Context;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/models/ContextualItem;-><init>(Ljava/lang/String;Z)V

    .line 793
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 795
    new-instance v1, Lcom/vectorwatch/android/models/ContextualItem;

    const-string v2, "AUTO_SLEEP_DETECTION"

    .line 796
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getContextualAutoSleepSettings(Landroid/content/Context;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/models/ContextualItem;-><init>(Ljava/lang/String;Z)V

    .line 795
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 797
    new-instance v1, Lcom/vectorwatch/android/models/ContextualItem;

    const-string v2, "FLICK_TO_DISMISS"

    const-string v3, "drop_notification_option"

    .line 798
    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/models/ContextualItem;-><init>(Ljava/lang/String;Z)V

    .line 797
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 799
    new-instance v1, Lcom/vectorwatch/android/models/ContextualItem;

    const-string v2, "VIBRATE"

    const-string v3, "dnd_button"

    invoke-static {v3, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/models/ContextualItem;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 800
    new-instance v1, Lcom/vectorwatch/android/models/ContextualItem;

    const-string v2, "SHOW_TENTATIVE_MEETINGS"

    const-string v3, "settings_contextual_tentative_meetings"

    .line 801
    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/models/ContextualItem;-><init>(Ljava/lang/String;Z)V

    .line 800
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 802
    new-instance v1, Lcom/vectorwatch/android/models/ContextualItem;

    const-string v2, "OPTIMIZE_NOTIFICATIONS"

    const-string v3, "prefs_settings_contextual_notifications"

    .line 803
    invoke-static {v3, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/models/ContextualItem;-><init>(Ljava/lang/String;Z)V

    .line 802
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 804
    new-instance v1, Lcom/vectorwatch/android/models/ContextualItem;

    const-string v2, "GLANCE"

    const-string v3, "glance_mode"

    .line 805
    invoke-static {v3, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vectorwatch/android/models/ContextualItem;-><init>(Ljava/lang/String;Z)V

    .line 804
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 807
    return-object v0
.end method

.method public static packUserSettingsForUpload(Landroid/content/Context;)Lcom/vectorwatch/android/models/LocalSettingsModel;
    .locals 36
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 592
    new-instance v17, Lcom/vectorwatch/android/models/LocalSettingsModel;

    invoke-direct/range {v17 .. v17}, Lcom/vectorwatch/android/models/LocalSettingsModel;-><init>()V

    .line 595
    .local v17, "localSettingsModel":Lcom/vectorwatch/android/models/LocalSettingsModel;
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getGreetingUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    .line 596
    .local v18, "name":Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getGreetingUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v32

    if-eqz v32, :cond_8

    .line 597
    invoke-virtual/range {v17 .. v18}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setName(Ljava/lang/String;)V

    .line 601
    :goto_0
    sget-object v32, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Packing settings - Name - "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-interface/range {v32 .. v33}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 604
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoDateOfBirth(Landroid/content/Context;)J

    move-result-wide v10

    .line 605
    .local v10, "dateOfBirth":J
    const-wide/16 v32, -0x1

    cmp-long v32, v10, v32

    if-eqz v32, :cond_9

    .line 607
    const-wide/16 v32, 0x3e8

    div-long v32, v10, v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setBirthday(Ljava/lang/Long;)V

    .line 611
    :goto_1
    sget-object v32, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Packing settings - Age - "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-wide/16 v34, 0x3e8

    div-long v34, v10, v34

    invoke-virtual/range {v33 .. v35}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-interface/range {v32 .. v33}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 614
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoGender(Landroid/content/Context;)I

    move-result v12

    .line 615
    .local v12, "gender":I
    const/16 v32, -0x1

    move/from16 v0, v32

    if-eq v12, v0, :cond_0

    .line 616
    if-nez v12, :cond_a

    .line 617
    const-string v32, "F"

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setGender(Ljava/lang/String;)V

    .line 622
    :cond_0
    :goto_2
    sget-object v33, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Packing settings - Gender - "

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    if-nez v12, :cond_b

    const-string v32, "female"

    :goto_3
    move-object/from16 v0, v34

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 625
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getTimeFormatPreference(Landroid/content/Context;)I

    move-result v27

    .line 626
    .local v27, "timeFormat":I
    const/16 v32, -0x1

    move/from16 v0, v27

    move/from16 v1, v32

    if-eq v0, v1, :cond_1

    .line 627
    const/16 v32, 0x1

    move/from16 v0, v27

    move/from16 v1, v32

    if-ne v0, v1, :cond_c

    .line 628
    const-string v32, "h12"

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setTimeFormat(Ljava/lang/String;)V

    .line 633
    :cond_1
    :goto_4
    sget-object v33, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Packing settings - timeFormat - "

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v32, 0x1

    move/from16 v0, v27

    move/from16 v1, v32

    if-ne v0, v1, :cond_d

    const-string v32, "12"

    :goto_5
    move-object/from16 v0, v34

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 637
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getUnitSystemFormatPreference(Landroid/content/Context;)I

    move-result v29

    .line 638
    .local v29, "unitSystem":I
    const/16 v32, -0x1

    move/from16 v0, v29

    move/from16 v1, v32

    if-eq v0, v1, :cond_2

    .line 639
    const/16 v32, 0x1

    move/from16 v0, v29

    move/from16 v1, v32

    if-ne v0, v1, :cond_e

    .line 640
    const-string v32, "I"

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setUnitSystem(Ljava/lang/String;)V

    .line 645
    :cond_2
    :goto_6
    sget-object v33, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Packing settings - unitSystem - "

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v32, 0x1

    move/from16 v0, v29

    move/from16 v1, v32

    if-ne v0, v1, :cond_f

    const-string v32, "imperial"

    :goto_7
    move-object/from16 v0, v34

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 649
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoHeight(Landroid/content/Context;)I

    move-result v13

    .line 650
    .local v13, "height":I
    const/16 v32, -0x1

    move/from16 v0, v32

    if-eq v13, v0, :cond_10

    .line 651
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setHeight(Ljava/lang/Integer;)V

    .line 655
    :goto_8
    sget-object v32, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Packing settings - Height - "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-interface/range {v32 .. v33}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 658
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActivityInfoWeight(Landroid/content/Context;)I

    move-result v31

    .line 659
    .local v31, "weight":I
    const/16 v32, -0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_11

    .line 660
    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setWeight(Ljava/lang/Integer;)V

    .line 664
    :goto_9
    sget-object v32, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "Packing settings - Weight - "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-interface/range {v32 .. v33}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 667
    sget-object v32, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v32

    move/from16 v0, v32

    float-to-int v8, v0

    .line 668
    .local v8, "caloriesGoal":I
    sget-object v32, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v32

    move/from16 v0, v32

    float-to-int v0, v0

    move/from16 v25, v0

    .line 669
    .local v25, "sleepGoal":I
    sget-object v32, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v9

    .line 670
    .local v9, "distanceGoal":F
    sget-object v32, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getActiveGoal(Lcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)F

    move-result v32

    move/from16 v0, v32

    float-to-int v0, v0

    move/from16 v26, v0

    .line 672
    .local v26, "stepsGoal":I
    const/16 v32, -0x1

    move/from16 v0, v32

    if-eq v8, v0, :cond_3

    .line 673
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setCaloriesGoal(Ljava/lang/Integer;)V

    .line 675
    :cond_3
    const/16 v32, -0x1

    move/from16 v0, v25

    move/from16 v1, v32

    if-eq v0, v1, :cond_4

    .line 676
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setSleepGoal(Ljava/lang/Integer;)V

    .line 678
    :cond_4
    const/high16 v32, -0x40800000    # -1.0f

    cmpl-float v32, v9, v32

    if-eqz v32, :cond_5

    .line 679
    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v32

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setDistanceGoal(Ljava/lang/Float;)V

    .line 681
    :cond_5
    const/16 v32, -0x1

    move/from16 v0, v26

    move/from16 v1, v32

    if-eq v0, v1, :cond_6

    .line 682
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setStepsGoal(Ljava/lang/Integer;)V

    .line 685
    :cond_6
    const-string v32, "pref_backlight_intensity"

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v14

    .line 686
    .local v14, "intensity":I
    const-string v32, "pref_backlight_duration"

    const/16 v33, 0x2

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v28

    .line 687
    .local v28, "timeout":I
    const-string v32, "pref_watch_second_hand"

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v23

    .line 689
    .local v23, "secondHand":Z
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setIntensity(I)V

    .line 691
    packed-switch v28, :pswitch_data_0

    .line 712
    const/16 v28, 0x5

    .line 715
    :goto_a
    move-object/from16 v0, v17

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setTimeout(I)V

    .line 716
    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setSecondsHand(Z)V

    .line 718
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v32

    const-string v33, "app_not_all"

    const-class v34, Ljava/util/List;

    invoke-virtual/range {v32 .. v34}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/List;

    .line 719
    .local v19, "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v19, :cond_7

    .line 720
    new-instance v19, Ljava/util/ArrayList;

    .end local v19    # "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 723
    .restart local v19    # "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_7
    invoke-static/range {p0 .. p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getSystemNotification(Landroid/content/Context;)Z

    move-result v16

    .line 724
    .local v16, "isNotifEnabled":Z
    new-instance v20, Lcom/vectorwatch/android/models/NotificationSettingsItem;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/models/NotificationSettingsItem;-><init>(Ljava/util/List;Z)V

    .line 725
    .local v20, "notificationSettingsItem":Lcom/vectorwatch/android/models/NotificationSettingsItem;
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setNotifList(Lcom/vectorwatch/android/models/NotificationSettingsItem;)V

    .line 727
    new-instance v24, Lcom/vectorwatch/android/models/UserSettings;

    invoke-direct/range {v24 .. v24}, Lcom/vectorwatch/android/models/UserSettings;-><init>()V

    .line 729
    .local v24, "settings":Lcom/vectorwatch/android/models/UserSettings;
    const-string v32, "pref_link_lost_icon"

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v32

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserSettings;->setLinkLostIcon(Z)V

    .line 731
    const-string v32, "pref_link_lost_notification"

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v32

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserSettings;->setLinkLostNotification(Z)V

    .line 733
    const-string v32, "pref_low_battery_icon"

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v32

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserSettings;->setLowBatteryIcon(Z)V

    .line 735
    const-string v32, "pref_low_battery_notification"

    const/16 v33, 0x0

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v32

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserSettings;->setLowBatteryNotification(Z)V

    .line 737
    const-string v32, "pref_goal_achievement"

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v32

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserSettings;->setGoalAchievementAlert(Z)V

    .line 739
    const-string v32, "pref_goal_almost"

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v32

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserSettings;->setGoalAlmostAlert(Z)V

    .line 741
    const-string v32, "pref_activity_reminder"

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v32

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserSettings;->setActivityReminder(Z)V

    .line 743
    const-string v32, "pref_activity_newsletter"

    const/16 v33, 0x1

    move-object/from16 v0, v32

    move/from16 v1, v33

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v32

    move-object/from16 v0, v24

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserSettings;->setActivityNewsletter(Z)V

    .line 746
    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setUserSettings(Lcom/vectorwatch/android/models/UserSettings;)V

    .line 749
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v21

    .line 750
    .local v21, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/database/DatabaseManager;->getAllAlarms(Lio/realm/Realm;)Ljava/util/List;

    move-result-object v7

    .line 752
    .local v7, "alarmList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v32

    if-lez v32, :cond_14

    .line 753
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 755
    .local v6, "alarmCloudDetailsModelList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v32

    :goto_b
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->hasNext()Z

    move-result v33

    if-eqz v33, :cond_13

    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .line 757
    .local v4, "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    new-instance v5, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;

    invoke-direct {v5}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;-><init>()V

    .line 758
    .local v5, "alarmCloudDetailsModel":Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfHours()I

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->setHour(Ljava/lang/Integer;)V

    .line 759
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfMinutes()I

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->setMinute(Ljava/lang/Integer;)V

    .line 760
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getIsEnabled()B

    move-result v22

    .line 761
    .local v22, "repeatPattern":B
    if-eqz v22, :cond_12

    const/4 v15, 0x1

    .line 762
    .local v15, "isEnabled":Z
    :goto_c
    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->setIsEnabled(Ljava/lang/Boolean;)V

    .line 763
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getName()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v5, v0}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->setTitle(Ljava/lang/String;)V

    .line 764
    invoke-static/range {v22 .. v22}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/AlarmHelpers;->getWeekDaysRepeatPattern(B)Ljava/util/List;

    move-result-object v30

    .line 765
    .local v30, "weekDays":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->setWeekDays(Ljava/util/List;)V

    .line 767
    sget-object v33, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Alarm to sync: "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getName()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " | "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfHours()I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, ":"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    .line 768
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfMinutes()I

    move-result v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " | "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getIsEnabled()B

    move-result v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 767
    invoke-interface/range {v33 .. v34}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 769
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    .line 599
    .end local v4    # "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .end local v5    # "alarmCloudDetailsModel":Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;
    .end local v6    # "alarmCloudDetailsModelList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    .end local v7    # "alarmList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    .end local v8    # "caloriesGoal":I
    .end local v9    # "distanceGoal":F
    .end local v10    # "dateOfBirth":J
    .end local v12    # "gender":I
    .end local v13    # "height":I
    .end local v14    # "intensity":I
    .end local v15    # "isEnabled":Z
    .end local v16    # "isNotifEnabled":Z
    .end local v19    # "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v20    # "notificationSettingsItem":Lcom/vectorwatch/android/models/NotificationSettingsItem;
    .end local v21    # "realm":Lio/realm/Realm;
    .end local v22    # "repeatPattern":B
    .end local v23    # "secondHand":Z
    .end local v24    # "settings":Lcom/vectorwatch/android/models/UserSettings;
    .end local v25    # "sleepGoal":I
    .end local v26    # "stepsGoal":I
    .end local v27    # "timeFormat":I
    .end local v28    # "timeout":I
    .end local v29    # "unitSystem":I
    .end local v30    # "weekDays":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v31    # "weight":I
    :cond_8
    const/16 v32, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setName(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 609
    .restart local v10    # "dateOfBirth":J
    :cond_9
    const/16 v32, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setBirthday(Ljava/lang/Long;)V

    goto/16 :goto_1

    .line 618
    .restart local v12    # "gender":I
    :cond_a
    const/16 v32, 0x1

    move/from16 v0, v32

    if-ne v12, v0, :cond_0

    .line 619
    const-string v32, "M"

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setGender(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 622
    :cond_b
    const-string v32, "male"

    goto/16 :goto_3

    .line 629
    .restart local v27    # "timeFormat":I
    :cond_c
    if-nez v27, :cond_1

    .line 630
    const-string v32, "h24"

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setTimeFormat(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 633
    :cond_d
    const-string v32, "24"

    goto/16 :goto_5

    .line 641
    .restart local v29    # "unitSystem":I
    :cond_e
    if-nez v29, :cond_2

    .line 642
    const-string v32, "M"

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setUnitSystem(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 645
    :cond_f
    const-string v32, "metric"

    goto/16 :goto_7

    .line 653
    .restart local v13    # "height":I
    :cond_10
    const/16 v32, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setHeight(Ljava/lang/Integer;)V

    goto/16 :goto_8

    .line 662
    .restart local v31    # "weight":I
    :cond_11
    const/16 v32, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setWeight(Ljava/lang/Integer;)V

    goto/16 :goto_9

    .line 693
    .restart local v8    # "caloriesGoal":I
    .restart local v9    # "distanceGoal":F
    .restart local v14    # "intensity":I
    .restart local v23    # "secondHand":Z
    .restart local v25    # "sleepGoal":I
    .restart local v26    # "stepsGoal":I
    .restart local v28    # "timeout":I
    :pswitch_0
    const/16 v28, 0x2

    .line 694
    goto/16 :goto_a

    .line 697
    :pswitch_1
    const/16 v28, 0x5

    .line 698
    goto/16 :goto_a

    .line 701
    :pswitch_2
    const/16 v28, 0xa

    .line 702
    goto/16 :goto_a

    .line 705
    :pswitch_3
    const/16 v28, 0x14

    .line 706
    goto/16 :goto_a

    .line 709
    :pswitch_4
    const/16 v28, 0x1e

    .line 710
    goto/16 :goto_a

    .line 761
    .restart local v4    # "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .restart local v5    # "alarmCloudDetailsModel":Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;
    .restart local v6    # "alarmCloudDetailsModelList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    .restart local v7    # "alarmList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    .restart local v16    # "isNotifEnabled":Z
    .restart local v19    # "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v20    # "notificationSettingsItem":Lcom/vectorwatch/android/models/NotificationSettingsItem;
    .restart local v21    # "realm":Lio/realm/Realm;
    .restart local v22    # "repeatPattern":B
    .restart local v24    # "settings":Lcom/vectorwatch/android/models/UserSettings;
    :cond_12
    const/4 v15, 0x0

    goto/16 :goto_c

    .line 772
    .end local v4    # "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .end local v5    # "alarmCloudDetailsModel":Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;
    .end local v22    # "repeatPattern":B
    :cond_13
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setAlarmsSettings(Ljava/util/List;)V

    .line 778
    :goto_d
    invoke-virtual/range {v21 .. v21}, Lio/realm/Realm;->close()V

    .line 779
    return-object v17

    .line 774
    .end local v6    # "alarmCloudDetailsModelList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    :cond_14
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 775
    .restart local v6    # "alarmCloudDetailsModelList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setAlarmsSettings(Ljava/util/List;)V

    goto :goto_d

    .line 691
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static parseFromBytes([B)Ljava/util/List;
    .locals 17
    .param p0, "scanRecord"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Landroid/os/ParcelUuid;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1706
    if-nez p0, :cond_1

    .line 1707
    const/4 v12, 0x0

    .line 1783
    :cond_0
    :goto_0
    return-object v12

    .line 1710
    :cond_1
    const/4 v2, 0x0

    .line 1711
    .local v2, "currentPos":I
    const/4 v1, -0x1

    .line 1712
    .local v1, "advertiseFlag":I
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1713
    .local v12, "serviceUuids":Ljava/util/List;, "Ljava/util/List<Landroid/os/ParcelUuid;>;"
    const/4 v8, 0x0

    .line 1714
    .local v8, "localName":Ljava/lang/String;
    const/high16 v13, -0x80000000

    .line 1716
    .local v13, "txPowerLevel":I
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    .local v9, "manufacturerData":Landroid/util/SparseArray;, "Landroid/util/SparseArray<[B>;"
    move v3, v2

    .line 1719
    .end local v2    # "currentPos":I
    .local v3, "currentPos":I
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    array-length v14, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ge v3, v14, :cond_3

    .line 1721
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "currentPos":I
    .restart local v2    # "currentPos":I
    :try_start_1
    aget-byte v14, p0, v3

    and-int/lit16 v7, v14, 0xff

    .line 1722
    .local v7, "length":I
    if-nez v7, :cond_2

    .line 1775
    .end local v7    # "length":I
    :goto_2
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v14

    if-eqz v14, :cond_0

    .line 1776
    const/4 v12, 0x0

    goto :goto_0

    .line 1726
    .restart local v7    # "length":I
    :cond_2
    add-int/lit8 v4, v7, -0x1

    .line 1728
    .local v4, "dataLength":I
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "currentPos":I
    .restart local v3    # "currentPos":I
    :try_start_2
    aget-byte v14, p0, v2

    and-int/lit16 v6, v14, 0xff

    .line 1729
    .local v6, "fieldType":I
    sparse-switch v6, :sswitch_data_0

    .line 1772
    :goto_3
    :sswitch_0
    add-int v2, v3, v4

    .end local v3    # "currentPos":I
    .restart local v2    # "currentPos":I
    move v3, v2

    .line 1773
    .end local v2    # "currentPos":I
    .restart local v3    # "currentPos":I
    goto :goto_1

    .line 1731
    :sswitch_1
    aget-byte v14, p0, v3

    and-int/lit16 v1, v14, 0xff

    .line 1732
    goto :goto_3

    .line 1735
    :sswitch_2
    const/4 v14, 0x2

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4, v14, v12}, Lcom/vectorwatch/android/utils/Helpers;->parseServiceUuid([BIIILjava/util/List;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 1779
    .end local v4    # "dataLength":I
    .end local v6    # "fieldType":I
    .end local v7    # "length":I
    .end local v8    # "localName":Ljava/lang/String;
    :catch_0
    move-exception v5

    move v2, v3

    .line 1780
    .end local v3    # "currentPos":I
    .restart local v2    # "currentPos":I
    .local v5, "e":Ljava/lang/Exception;
    :goto_4
    const-string v14, "ScanRecord"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "unable to parse scan record: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static/range {p0 .. p0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1783
    new-instance v12, Ljava/util/ArrayList;

    .end local v12    # "serviceUuids":Ljava/util/List;, "Ljava/util/List<Landroid/os/ParcelUuid;>;"
    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 1740
    .end local v2    # "currentPos":I
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v3    # "currentPos":I
    .restart local v4    # "dataLength":I
    .restart local v6    # "fieldType":I
    .restart local v7    # "length":I
    .restart local v8    # "localName":Ljava/lang/String;
    .restart local v12    # "serviceUuids":Ljava/util/List;, "Ljava/util/List<Landroid/os/ParcelUuid;>;"
    :sswitch_3
    const/4 v14, 0x4

    :try_start_3
    move-object/from16 v0, p0

    invoke-static {v0, v3, v4, v14, v12}, Lcom/vectorwatch/android/utils/Helpers;->parseServiceUuid([BIIILjava/util/List;)I

    goto :goto_3

    .line 1745
    :sswitch_4
    const/16 v14, 0x10

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4, v14, v12}, Lcom/vectorwatch/android/utils/Helpers;->parseServiceUuid([BIIILjava/util/List;)I

    goto :goto_3

    .line 1750
    :sswitch_5
    new-instance v8, Ljava/lang/String;

    .line 1751
    .end local v8    # "localName":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/vectorwatch/android/utils/Helpers;->extractBytes([BII)[B

    move-result-object v14

    invoke-direct {v8, v14}, Ljava/lang/String;-><init>([B)V

    .line 1752
    .restart local v8    # "localName":Ljava/lang/String;
    goto :goto_3

    .line 1754
    :sswitch_6
    aget-byte v13, p0, v3

    .line 1755
    goto :goto_3

    .line 1762
    :sswitch_7
    add-int/lit8 v14, v3, 0x1

    aget-byte v14, p0, v14

    and-int/lit16 v14, v14, 0xff

    shl-int/lit8 v14, v14, 0x8

    aget-byte v15, p0, v3

    and-int/lit16 v15, v15, 0xff

    add-int v11, v14, v15

    .line 1764
    .local v11, "manufacturerId":I
    add-int/lit8 v14, v3, 0x2

    add-int/lit8 v15, v4, -0x2

    move-object/from16 v0, p0

    invoke-static {v0, v14, v15}, Lcom/vectorwatch/android/utils/Helpers;->extractBytes([BII)[B

    move-result-object v10

    .line 1766
    .local v10, "manufacturerDataBytes":[B
    invoke-virtual {v9, v11, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    .line 1779
    .end local v3    # "currentPos":I
    .end local v4    # "dataLength":I
    .end local v6    # "fieldType":I
    .end local v7    # "length":I
    .end local v10    # "manufacturerDataBytes":[B
    .end local v11    # "manufacturerId":I
    .restart local v2    # "currentPos":I
    :catch_1
    move-exception v5

    goto :goto_4

    .end local v2    # "currentPos":I
    .restart local v3    # "currentPos":I
    :cond_3
    move v2, v3

    .end local v3    # "currentPos":I
    .restart local v2    # "currentPos":I
    goto/16 :goto_2

    .line 1729
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_4
        0x8 -> :sswitch_5
        0x9 -> :sswitch_5
        0xa -> :sswitch_6
        0x16 -> :sswitch_0
        0xff -> :sswitch_7
    .end sparse-switch
.end method

.method private static parseServiceUuid([BIIILjava/util/List;)I
    .locals 2
    .param p0, "scanRecord"    # [B
    .param p1, "currentPos"    # I
    .param p2, "dataLength"    # I
    .param p3, "uuidLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BIII",
            "Ljava/util/List",
            "<",
            "Landroid/os/ParcelUuid;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1789
    .local p4, "serviceUuids":Ljava/util/List;, "Ljava/util/List<Landroid/os/ParcelUuid;>;"
    :goto_0
    if-lez p2, :cond_0

    .line 1790
    invoke-static {p0, p1, p3}, Lcom/vectorwatch/android/utils/Helpers;->extractBytes([BII)[B

    move-result-object v0

    .line 1792
    .local v0, "uuidBytes":[B
    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->parseUuidFrom([B)Landroid/os/ParcelUuid;

    move-result-object v1

    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1793
    sub-int/2addr p2, p3

    .line 1794
    add-int/2addr p1, p3

    .line 1795
    goto :goto_0

    .line 1796
    .end local v0    # "uuidBytes":[B
    :cond_0
    return p1
.end method

.method public static parseUuidFrom([B)Landroid/os/ParcelUuid;
    .locals 13
    .param p0, "uuidBytes"    # [B

    .prologue
    const/16 v12, 0x10

    const/4 v9, 0x1

    const/4 v11, 0x2

    const/4 v10, 0x0

    .line 1809
    if-nez p0, :cond_0

    .line 1810
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "uuidBytes cannot be null"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1812
    :cond_0
    array-length v1, p0

    .line 1813
    .local v1, "length":I
    if-eq v1, v11, :cond_1

    const/4 v8, 0x4

    if-eq v1, v8, :cond_1

    if-eq v1, v12, :cond_1

    .line 1815
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "uuidBytes length invalid - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1818
    :cond_1
    if-ne v1, v12, :cond_2

    .line 1819
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v8, v9}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1820
    .local v0, "buf":Ljava/nio/ByteBuffer;
    const/16 v8, 0x8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v4

    .line 1821
    .local v4, "msb":J
    invoke-virtual {v0, v10}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v2

    .line 1822
    .local v2, "lsb":J
    new-instance v8, Landroid/os/ParcelUuid;

    new-instance v9, Ljava/util/UUID;

    invoke-direct {v9, v4, v5, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    invoke-direct {v8, v9}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    .line 1838
    .end local v0    # "buf":Ljava/nio/ByteBuffer;
    :goto_0
    return-object v8

    .line 1827
    .end local v2    # "lsb":J
    .end local v4    # "msb":J
    :cond_2
    if-ne v1, v11, :cond_3

    .line 1828
    aget-byte v8, p0, v10

    and-int/lit16 v8, v8, 0xff

    int-to-long v6, v8

    .line 1829
    .local v6, "shortUuid":J
    aget-byte v8, p0, v9

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    int-to-long v8, v8

    add-long/2addr v6, v8

    .line 1836
    :goto_1
    sget-object v8, Lcom/vectorwatch/android/utils/Helpers;->BASE_UUID:Landroid/os/ParcelUuid;

    invoke-virtual {v8}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->getMostSignificantBits()J

    move-result-wide v8

    const/16 v10, 0x20

    shl-long v10, v6, v10

    add-long v4, v8, v10

    .line 1837
    .restart local v4    # "msb":J
    sget-object v8, Lcom/vectorwatch/android/utils/Helpers;->BASE_UUID:Landroid/os/ParcelUuid;

    invoke-virtual {v8}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->getLeastSignificantBits()J

    move-result-wide v2

    .line 1838
    .restart local v2    # "lsb":J
    new-instance v8, Landroid/os/ParcelUuid;

    new-instance v9, Ljava/util/UUID;

    invoke-direct {v9, v4, v5, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    invoke-direct {v8, v9}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    goto :goto_0

    .line 1831
    .end local v2    # "lsb":J
    .end local v4    # "msb":J
    .end local v6    # "shortUuid":J
    :cond_3
    aget-byte v8, p0, v10

    and-int/lit16 v8, v8, 0xff

    int-to-long v6, v8

    .line 1832
    .restart local v6    # "shortUuid":J
    aget-byte v8, p0, v9

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    int-to-long v8, v8

    add-long/2addr v6, v8

    .line 1833
    aget-byte v8, p0, v11

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x10

    int-to-long v8, v8

    add-long/2addr v6, v8

    .line 1834
    const/4 v8, 0x3

    aget-byte v8, p0, v8

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x18

    int-to-long v8, v8

    add-long/2addr v6, v8

    goto :goto_1
.end method

.method public static resetDatabase(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 1138
    if-eqz p0, :cond_0

    .line 1139
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1140
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1141
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_ACTIVITY:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1142
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_ALARM:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1143
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_LOGS:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1144
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_APPS_RESOURCES:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1145
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_RESOURCES:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1146
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1148
    :cond_0
    return-void
.end method

.method public static resetDefaultAppsTables(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2561
    sget-object v0, Lcom/vectorwatch/android/utils/Helpers$TableType;->CLOUD:Lcom/vectorwatch/android/utils/Helpers$TableType;

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->dropTable(Lcom/vectorwatch/android/utils/Helpers$TableType;Landroid/content/Context;)V

    .line 2562
    sget-object v0, Lcom/vectorwatch/android/utils/Helpers$TableType;->STREAM:Lcom/vectorwatch/android/utils/Helpers$TableType;

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/Helpers;->dropTable(Lcom/vectorwatch/android/utils/Helpers$TableType;Landroid/content/Context;)V

    .line 2563
    return-void
.end method

.method public static resetFlagsForChangedSettings(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1440
    const-string v0, "flag_changed_alarms"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1441
    const-string v0, "flag_changed_goals"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1442
    const-string v0, "flag_changed_activity_profile"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1443
    const-string v0, "flag_changed_activity_info"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1444
    return-void
.end method

.method public static resetFlagsForContextual(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1450
    const-string v0, "flag_changed_contextual"

    const/4 v1, 0x0

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1451
    return-void
.end method

.method public static resetLocaleFlag(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2501
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v0, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2502
    .local v0, "current":Ljava/util/Locale;
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 2504
    .local v1, "currentLanguage":Ljava/lang/String;
    sget-object v2, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LOCALE - reset locale flag to true and language "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2505
    const-string v2, "flag_sync_locale"

    const/4 v3, 0x1

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2506
    const-string v2, "pref_locale_watch"

    invoke-static {v2, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2508
    return-void
.end method

.method public static resetUserSettings(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v2, -0x1

    .line 1085
    if-eqz p0, :cond_0

    .line 1087
    const-wide/16 v0, -0x1

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoDateOfBirth(JLandroid/content/Context;)V

    .line 1090
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoGender(ILandroid/content/Context;)V

    .line 1093
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoWeight(ILandroid/content/Context;)V

    .line 1096
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoHeight(ILandroid/content/Context;)V

    .line 1099
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setUnitSystemFormatPreference(ILandroid/content/Context;)V

    .line 1102
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTimeFormatPreference(ILandroid/content/Context;)V

    .line 1105
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setGreetingUserName(Ljava/lang/String;Landroid/content/Context;)V

    .line 1108
    invoke-static {v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setContextualMorningFaceSettings(ZLandroid/content/Context;)V

    .line 1111
    invoke-static {v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setContextualAutoDiscreetSettings(ZLandroid/content/Context;)V

    .line 1114
    invoke-static {v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setContextualAutoSleepSettings(ZLandroid/content/Context;)V

    .line 1117
    const-string v0, "drop_notification_option"

    invoke-static {v0, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1121
    const-string v0, "settings_contextual_tentative_meetings"

    invoke-static {v0, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1125
    const-string v0, "dnd_button"

    invoke-static {v0, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1129
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-static {v3, v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 1130
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-static {v3, v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 1131
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-static {v3, v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 1132
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    invoke-static {v3, v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 1135
    :cond_0
    return-void
.end method

.method public static returnIntTwoByte(I)[B
    .locals 3
    .param p0, "val"    # I

    .prologue
    .line 231
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 232
    .local v0, "data":[B
    const/4 v1, 0x0

    int-to-byte v2, p0

    aput-byte v2, v0, v1

    .line 233
    const/4 v1, 0x1

    ushr-int/lit8 v2, p0, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 235
    return-object v0
.end method

.method public static setApiUriByAppVersion(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2338
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->updateApiUri(Landroid/content/Context;)V

    .line 2339
    return-void
.end method

.method public static setAppLanguage(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p0, "languageCode"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2209
    const-string v0, "prefs_app_language"

    invoke-static {v0, p0, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2210
    return-void
.end method

.method public static setBit(BB)B
    .locals 1
    .param p0, "value"    # B
    .param p1, "position"    # B

    .prologue
    .line 2530
    const/4 v0, 0x1

    shl-int/2addr v0, p1

    or-int/2addr v0, p0

    int-to-byte v0, v0

    return v0
.end method

.method public static setCurrentUpdateState(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "state"    # I

    .prologue
    .line 1056
    const-string v0, "update_watch_status"

    invoke-static {v0, p1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1057
    return-void
.end method

.method public static setFlagsForSyncingUserSettingsToWatch(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 960
    const-string v0, "activity_info_age_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 962
    const-string v0, "activity_info_gender_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 964
    const-string v0, "activity_info_height_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 966
    const-string v0, "activity_info_weight_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 968
    const-string v0, "activity_info_activity_level_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 971
    const-string v0, "settings_time_format_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 973
    const-string v0, "settings_unit_system_format_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 975
    const-string v0, "settings_greeting_name_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 978
    const-string v0, "settings_contextual_morning_face_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 980
    const-string v0, "settings_contextual_auto_discreet_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 982
    const-string v0, "settings_contextual_auto_sleep_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 984
    const-string v0, "drop_notification_option_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 986
    const-string v0, "dnd_dirty"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 987
    return-void
.end method

.method public static setLocale(Ljava/lang/String;Landroid/content/Context;Z)V
    .locals 6
    .param p0, "localeCode"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reset"    # Z

    .prologue
    .line 2233
    const-string v3, ""

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2252
    :cond_0
    :goto_0
    return-void

    .line 2236
    :cond_1
    new-instance v2, Ljava/util/Locale;

    invoke-direct {v2, p0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 2237
    .local v2, "locale":Ljava/util/Locale;
    invoke-static {v2}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 2238
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    .line 2239
    .local v0, "config":Landroid/content/res/Configuration;
    iput-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2240
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 2241
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 2243
    const-string v3, "prefs_locale"

    invoke-static {v3, p0, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2244
    sget-object v3, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Locale - set locale = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2246
    if-eqz p2, :cond_0

    .line 2247
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v1, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2248
    .local v1, "i":Landroid/content/Intent;
    const v3, 0x8000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2249
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2250
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static setUpdateModeState(ILandroid/content/Context;)V
    .locals 1
    .param p0, "state"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1033
    const-string v0, "update_watch_status"

    invoke-static {v0, p0, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1034
    return-void
.end method

.method public static startConnectionEstablishedDrivenSyncs(ILandroid/content/Context;)V
    .locals 5
    .param p0, "connectionDetails"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1488
    packed-switch p0, :pswitch_data_0

    .line 1502
    :goto_0
    :pswitch_0
    const-string v2, "flag_sync_locale"

    const/4 v3, 0x0

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 1504
    .local v1, "syncNeeded":Z
    sget-object v2, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LOCALE - Connection syncs - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1505
    if-eqz v1, :cond_0

    .line 1506
    sget-object v2, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v3, "LOCALE - sync locale flag is set."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1507
    const-string v2, "pref_locale_watch"

    const/4 v3, 0x0

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1509
    .local v0, "currentLanguage":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1510
    invoke-static {v0, p1}, Lcom/vectorwatch/android/utils/Helpers;->dispatchLocaleFile(Ljava/lang/String;Landroid/content/Context;)V

    .line 1515
    .end local v0    # "currentLanguage":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 1490
    .end local v1    # "syncNeeded":Z
    :pswitch_1
    const-string v2, "BLE CALLS"

    const-string v3, "first time connect"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1491
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->startFirstConnect(Landroid/content/Context;)V

    goto :goto_0

    .line 1494
    :pswitch_2
    const-string v2, "BLE CALLS"

    const-string v3, "normal connect"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1495
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->startNormalConnectDrivenSyncs(Landroid/content/Context;)V

    goto :goto_0

    .line 1512
    .restart local v0    # "currentLanguage":Ljava/lang/String;
    .restart local v1    # "syncNeeded":Z
    :cond_1
    sget-object v2, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v3, "LOCALE - flag to set locale to watch is set, but the locale is null."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 1488
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static startFirstConnect(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1566
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->setFlagsForSyncingUserSettingsToWatch(Landroid/content/Context;)V

    .line 1568
    sget-object v6, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v7, "Syncs - Reset"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1571
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBatteryRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 1573
    invoke-static {p0}, Lcom/vectorwatch/android/managers/StreamsManager;->triggerRestoreStreams(Landroid/content/Context;)V

    .line 1576
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->setFlagsForSyncingUserSettingsToWatch(Landroid/content/Context;)V

    .line 1577
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSettings(Landroid/content/Context;)Ljava/util/UUID;

    .line 1578
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getIntensityPercentage(Landroid/content/Context;)I

    move-result v1

    .line 1579
    .local v1, "intensityPercentage":I
    invoke-static {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBacklightIntensity(Landroid/content/Context;I)Ljava/util/UUID;

    .line 1580
    const-string v6, "pref_backlight_duration"

    const/4 v7, 0x2

    .line 1581
    invoke-static {v6, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v6

    .line 1580
    invoke-static {v6}, Lcom/vectorwatch/android/utils/Helpers;->getWatchTimeoutDuration(I)I

    move-result v5

    .line 1582
    .local v5, "timeoutInSeconds":I
    invoke-static {p0, v5}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBacklightTimeout(Landroid/content/Context;I)Ljava/util/UUID;

    .line 1583
    const-string v6, "pref_watch_second_hand"

    const/4 v7, 0x1

    invoke-static {v6, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    .line 1585
    .local v2, "isEnabled":Z
    invoke-static {p0, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendDisplaySecondHand(Landroid/content/Context;Z)Ljava/util/UUID;

    .line 1588
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->syncNotificationDisplayOption(Landroid/content/Context;)V

    .line 1591
    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->getCalendarEvents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 1592
    .local v0, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCalendarEvents(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 1595
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getActiveGoals(Landroid/content/Context;)Ljava/util/List;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendGoals(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 1598
    invoke-static {p0}, Lcom/vectorwatch/android/managers/StreamsManager;->getLastUpdates(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 1599
    .local v4, "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 1600
    .local v3, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    invoke-static {p0, v3}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;

    goto :goto_0

    .line 1604
    .end local v3    # "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    :cond_0
    invoke-static {p0}, Lcom/vectorwatch/android/receivers/scheduler/FetchDataTrigger;->setAlarm(Landroid/content/Context;)V

    .line 1605
    return-void
.end method

.method private static startNormalConnectDrivenSyncs(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1521
    sget-object v6, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v7, "Syncs - Normal connect"

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1523
    const-string v6, "flag_get_cpu_id"

    const/4 v7, 0x0

    invoke-static {v6, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1525
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendUuidRequest(Landroid/content/Context;)Ljava/util/UUID;

    .line 1532
    :cond_0
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSettings(Landroid/content/Context;)Ljava/util/UUID;

    .line 1533
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getIntensityPercentage(Landroid/content/Context;)I

    move-result v1

    .line 1534
    .local v1, "intensityPercentage":I
    invoke-static {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBacklightIntensity(Landroid/content/Context;I)Ljava/util/UUID;

    .line 1535
    const-string v6, "pref_backlight_duration"

    const/4 v7, 0x2

    .line 1536
    invoke-static {v6, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v6

    .line 1535
    invoke-static {v6}, Lcom/vectorwatch/android/utils/Helpers;->getWatchTimeoutDuration(I)I

    move-result v5

    .line 1537
    .local v5, "timeoutInSeconds":I
    invoke-static {p0, v5}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendBacklightTimeout(Landroid/content/Context;I)Ljava/util/UUID;

    .line 1538
    const-string v6, "pref_watch_second_hand"

    const/4 v7, 0x1

    invoke-static {v6, v7, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    .line 1540
    .local v2, "isEnabled":Z
    invoke-static {p0, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendDisplaySecondHand(Landroid/content/Context;Z)Ljava/util/UUID;

    .line 1543
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->syncNotificationDisplayOption(Landroid/content/Context;)V

    .line 1546
    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->checkCalendarEventsForSync(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1547
    invoke-static {p0}, Lcom/vectorwatch/android/managers/EventManager;->getCalendarEvents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 1548
    .local v0, "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendCalendarEvents(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 1555
    .end local v0    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CalendarEventModel;>;"
    :cond_1
    invoke-static {p0}, Lcom/vectorwatch/android/managers/StreamsManager;->getUpdates(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 1556
    .local v4, "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 1557
    .local v3, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    invoke-static {p0, v3}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;

    goto :goto_0

    .line 1559
    .end local v3    # "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    :cond_2
    return-void
.end method

.method public static stringsAreTheSame(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "uuidA"    # Ljava/lang/String;
    .param p1, "uuidB"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2066
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 2067
    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 2069
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 2074
    .end local v0    # "result":I
    :cond_0
    :goto_0
    return v1

    .restart local v0    # "result":I
    :cond_1
    move v1, v2

    .line 2069
    goto :goto_0

    .line 2071
    .end local v0    # "result":I
    :cond_2
    if-nez p1, :cond_3

    if-eqz p0, :cond_0

    :cond_3
    move v1, v2

    .line 2074
    goto :goto_0
.end method

.method public static syncChangesFromCloudToWatch(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1691
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->setFlagsForSyncingUserSettingsToWatch(Landroid/content/Context;)V

    .line 1692
    invoke-static {p0}, Lcom/vectorwatch/android/managers/SyncWatchSettingsManager;->syncAll(Landroid/content/Context;)V

    .line 1693
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendSettings(Landroid/content/Context;)Ljava/util/UUID;

    .line 1696
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v1

    .line 1697
    .local v1, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vectorwatch/android/database/DatabaseManager;->getAllAlarms(Lio/realm/Realm;)Ljava/util/List;

    move-result-object v0

    .line 1698
    .local v0, "alarmList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendAlarms(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 1699
    invoke-virtual {v1}, Lio/realm/Realm;->close()V

    .line 1702
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->getActiveGoals(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendGoals(Landroid/content/Context;Ljava/util/List;)Ljava/util/UUID;

    .line 1703
    return-void
.end method

.method public static syncNotificationDisplayOption(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2439
    const-string v1, "pref_notification_display"

    const/4 v2, 0x1

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v0

    .line 2441
    .local v0, "option":I
    packed-switch v0, :pswitch_data_0

    .line 2452
    :goto_0
    return-void

    .line 2443
    :pswitch_0
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_SHOW_CONTENTS:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    invoke-static {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendNotificationMode(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$NotificationMode;)Ljava/util/UUID;

    goto :goto_0

    .line 2446
    :pswitch_1
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_SHOW_ALERT:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    invoke-static {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendNotificationMode(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$NotificationMode;)Ljava/util/UUID;

    goto :goto_0

    .line 2449
    :pswitch_2
    sget-object v1, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_OFF:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    invoke-static {p0, v1}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendNotificationMode(Landroid/content/Context;Lcom/vectorwatch/android/utils/Constants$NotificationMode;)Ljava/util/UUID;

    goto :goto_0

    .line 2441
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static triggerDisplayOfPairingScreen(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2514
    if-nez p0, :cond_0

    .line 2520
    :goto_0
    return-void

    .line 2518
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2519
    .local v0, "deviceListIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static triggerUserSettingsSyncToCloud(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 861
    const-string v0, "flag_sync_settings_to_cloud"

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 863
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 864
    return-void
.end method

.method public static unpairDevice(Landroid/bluetooth/BluetoothDevice;)V
    .locals 5
    .param p0, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 246
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "removeBond"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 247
    .local v1, "method":Ljava/lang/reflect/Method;
    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    .end local v1    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 248
    :catch_0
    move-exception v0

    .line 249
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static unpairDrivenReset(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x5

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1288
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "last_system_info"

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/utils/ComplexPreferences;->remove(Ljava/lang/String;)V

    .line 1289
    const-string v2, "flag_check_for_connect_disconnect_need"

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1292
    sget-object v2, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v3, "FLAG COMPAT true at BOND"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1293
    const-string v2, "check_compatibility"

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1295
    invoke-static {p0, v7}, Lcom/vectorwatch/android/utils/Helpers;->setCurrentUpdateState(Landroid/content/Context;I)V

    .line 1300
    const-string v2, "flag_show_onboarding"

    invoke-static {v2, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1303
    const-string v2, "previous_watch_shape"

    invoke-static {v2, v8, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1305
    .local v1, "previousWatchShape":Ljava/lang/String;
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1310
    .local v0, "currentWatchShape":Ljava/lang/String;
    sget-object v2, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PAIR: previous watch shape = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current watch shape = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1311
    const-string v2, "flag_sync_system_apps"

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1314
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->resetDatabase(Landroid/content/Context;)V

    .line 1316
    const-string v2, "flag_get_cpu_id"

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1317
    invoke-static {v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setPrefsWatchCurrent(ILandroid/content/Context;)V

    .line 1320
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "last_known_system_info"

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/utils/ComplexPreferences;->remove(Ljava/lang/String;)V

    .line 1321
    invoke-static {v7, p0}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 1324
    const-string v2, "pref_serial_number"

    invoke-static {v2, v8, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1327
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->setFlagsForSyncingUserSettingsToWatch(Landroid/content/Context;)V

    .line 1330
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->resetLocaleFlag(Landroid/content/Context;)V

    .line 1333
    const-string v2, "last_synced_battery_status"

    const/4 v3, -0x1

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1336
    const-string v2, "flag_sync_settings_from_cloud"

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1339
    sput v6, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    .line 1340
    sput v6, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    .line 1341
    return-void
.end method

.method public static updateApiUri(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2376
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isAlphaBuild()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isDevBuild()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2377
    :cond_0
    const-string v0, "https://api.vector.watch"

    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->setApiUri(Ljava/lang/String;)V

    .line 2378
    const-string v0, "pref_api_uri_stage"

    const-string v1, "https://api.vector.watch"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2384
    :goto_0
    new-instance v0, Lcom/vectorwatch/android/utils/Helpers$3;

    invoke-direct {v0}, Lcom/vectorwatch/android/utils/Helpers$3;-><init>()V

    invoke-static {p0, v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->updateAccountInfo(Landroid/content/Context;Lretrofit/Callback;)V

    .line 2395
    return-void

    .line 2380
    :cond_1
    const-string v0, "https://endpoint.vector.watch"

    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->setApiUri(Ljava/lang/String;)V

    .line 2381
    const-string v0, "pref_api_uri_stage"

    const-string v1, "https://endpoint.vector.watch"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static updateContextualSettings(Lcom/vectorwatch/android/models/ResponseContextual;Landroid/content/Context;)V
    .locals 5
    .param p0, "settings"    # Lcom/vectorwatch/android/models/ResponseContextual;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 506
    if-eqz p0, :cond_0

    iget-object v3, p0, Lcom/vectorwatch/android/models/ResponseContextual;->data:Lcom/vectorwatch/android/models/ResponseContextual$ContextualData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vectorwatch/android/models/ResponseContextual;->data:Lcom/vectorwatch/android/models/ResponseContextual$ContextualData;

    iget-object v3, v3, Lcom/vectorwatch/android/models/ResponseContextual$ContextualData;->contextualDataList:Ljava/util/List;

    if-nez v3, :cond_1

    .line 552
    :cond_0
    return-void

    .line 510
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vectorwatch/android/models/ResponseContextual;->data:Lcom/vectorwatch/android/models/ResponseContextual$ContextualData;

    iget-object v3, v3, Lcom/vectorwatch/android/models/ResponseContextual$ContextualData;->contextualDataList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 511
    iget-object v3, p0, Lcom/vectorwatch/android/models/ResponseContextual;->data:Lcom/vectorwatch/android/models/ResponseContextual$ContextualData;

    iget-object v3, v3, Lcom/vectorwatch/android/models/ResponseContextual$ContextualData;->contextualDataList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/ContextualItem;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ContextualItem;->getName()Ljava/lang/String;

    move-result-object v0

    .line 512
    .local v0, "field":Ljava/lang/String;
    iget-object v3, p0, Lcom/vectorwatch/android/models/ResponseContextual;->data:Lcom/vectorwatch/android/models/ResponseContextual$ContextualData;

    iget-object v3, v3, Lcom/vectorwatch/android/models/ResponseContextual$ContextualData;->contextualDataList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/ContextualItem;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/ContextualItem;->isValue()Z

    move-result v2

    .line 514
    .local v2, "value":Z
    const-string v3, "AUTO_DISCREET_MODE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 515
    invoke-static {v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setContextualAutoDiscreetSettings(ZLandroid/content/Context;)V

    .line 516
    const-string v3, "settings_contextual_auto_discreet_dirty"

    invoke-static {v3, v4, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 510
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 518
    :cond_3
    const-string v3, "AUTO_SLEEP_DETECTION"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 519
    invoke-static {v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setContextualAutoSleepSettings(ZLandroid/content/Context;)V

    .line 520
    const-string v3, "settings_contextual_auto_sleep_dirty"

    invoke-static {v3, v4, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_1

    .line 522
    :cond_4
    const-string v3, "FLICK_TO_DISMISS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 523
    const-string v3, "drop_notification_option"

    invoke-static {v3, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 525
    const-string v3, "drop_notification_option_dirty"

    invoke-static {v3, v4, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_1

    .line 527
    :cond_5
    const-string v3, "GLANCE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 528
    const-string v3, "glance_mode"

    invoke-static {v3, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 530
    const-string v3, "glance_dirty"

    invoke-static {v3, v4, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_1

    .line 532
    :cond_6
    const-string v3, "MORNING_FACE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 533
    const-string v3, "settings_contextual_morning_face"

    invoke-static {v3, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 535
    const-string v3, "settings_contextual_morning_face_dirty"

    invoke-static {v3, v4, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_1

    .line 537
    :cond_7
    const-string v3, "OPTIMIZE_NOTIFICATIONS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 538
    const-string v3, "prefs_settings_contextual_notifications"

    invoke-static {v3, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_1

    .line 540
    :cond_8
    const-string v3, "SHOW_TENTATIVE_MEETINGS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 541
    const-string v3, "settings_contextual_tentative_meetings"

    invoke-static {v3, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 543
    const-string v3, "settings_contextual_tentative_meetings_DIRTY"

    invoke-static {v3, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_1

    .line 545
    :cond_9
    const-string v3, "VIBRATE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 546
    const-string v3, "dnd_button"

    invoke-static {v3, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 548
    const-string v3, "dnd_dirty"

    invoke-static {v3, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setDirtyField(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_1
.end method

.method public static updateOptionsNotificationDisplay(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2846
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z

    move-result v0

    .line 2849
    .local v0, "isNotificationAccessAllowed":Z
    if-nez v0, :cond_0

    .line 2850
    sget-object v1, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS DISPLAY: update to off"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2853
    const-string v1, "pref_notification_display"

    const/4 v2, 0x0

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 2867
    :goto_0
    return-void

    .line 2857
    :cond_0
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isCurrentLanguageSupported(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2858
    sget-object v1, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS DISPLAY: update to alert"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2859
    const-string v1, "pref_notification_display"

    const/4 v2, 0x2

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 2862
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    const-string v2, "NOTIFICATIONS DISPLAY: update to content"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2863
    const-string v1, "pref_notification_display"

    const/4 v2, 0x1

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0
.end method

.method public static updateUserSettings(Lcom/vectorwatch/android/models/RemoteSettingsModel;Landroid/content/Context;)V
    .locals 26
    .param p0, "settings"    # Lcom/vectorwatch/android/models/RemoteSettingsModel;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 294
    if-nez p0, :cond_1

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 300
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setGreetingUserName(Ljava/lang/String;Landroid/content/Context;)V

    .line 301
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Unpacking settings - Name - "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 305
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getBirthday()Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 306
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getBirthday()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoDateOfBirth(JLandroid/content/Context;)V

    .line 307
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Unpacking settings - DateOfBirth - "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getBirthday()Ljava/lang/Long;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 311
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getGender()Ljava/lang/String;

    move-result-object v14

    .line 312
    .local v14, "gender":Ljava/lang/String;
    if-eqz v14, :cond_5

    .line 313
    const-string v4, "M"

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 314
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoGender(ILandroid/content/Context;)V

    .line 318
    :cond_4
    :goto_1
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Unpacking settings - Gender - "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 322
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getTimeFormat()Ljava/lang/String;

    move-result-object v20

    .line 323
    .local v20, "timeFormat":Ljava/lang/String;
    if-eqz v20, :cond_7

    .line 324
    const-string v4, "h12"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 325
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTimeFormatPreference(ILandroid/content/Context;)V

    .line 329
    :cond_6
    :goto_2
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Unpacking settings - TimeFormat - "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 333
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUnitSystem()Ljava/lang/String;

    move-result-object v22

    .line 334
    .local v22, "unitSystem":Ljava/lang/String;
    if-eqz v22, :cond_9

    .line 335
    const-string v4, "I"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 336
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setUnitSystemFormatPreference(ILandroid/content/Context;)V

    .line 342
    :cond_8
    :goto_3
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Unpacking settings - UnitSystem - "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 346
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getHeight()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 348
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getHeight()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoHeight(ILandroid/content/Context;)V

    .line 351
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getHeight()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 354
    .local v15, "heightInCm":I
    invoke-static {v15}, Lcom/vectorwatch/android/utils/Helpers;->convertHeightFromMetricToImperial(I)F

    move-result v12

    .line 355
    .local v12, "feet":F
    invoke-static {v12}, Lcom/vectorwatch/android/utils/Helpers;->getIntNumberOfFeet(F)I

    move-result v13

    .line 356
    .local v13, "feetRounded":I
    int-to-float v4, v13

    sub-float v4, v12, v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->getNoInchesFromNoFeet(F)I

    move-result v17

    .line 358
    .local v17, "inches":I
    const-string v4, "activity_info_height_feet"

    move-object/from16 v0, p1

    invoke-static {v4, v13, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 360
    const-string v4, "activity_info_height_inches"

    move/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 363
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Unpacking settings - Height (metric) - "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 364
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Unpacking settings - Height (imperial) - "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "\'"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "\""

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 368
    .end local v12    # "feet":F
    .end local v13    # "feetRounded":I
    .end local v15    # "heightInCm":I
    .end local v17    # "inches":I
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getWeight()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_b

    .line 369
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getWeight()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoWeight(ILandroid/content/Context;)V

    .line 370
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Unpacking settings - Weight - "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getWeight()Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 371
    const-string v4, "activity_info_weight_lbs"

    .line 373
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getWeight()Ljava/lang/Integer;

    move-result-object v24

    .line 372
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Integer;->intValue()I

    move-result v24

    invoke-static/range {v24 .. v24}, Lcom/vectorwatch/android/utils/Helpers;->convertWeightFromMetricToImperial(I)I

    move-result v24

    .line 371
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 377
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getCaloriesGoal()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 378
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getCaloriesGoal()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    sget-object v24, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 382
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getStepsGoal()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_d

    .line 383
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getStepsGoal()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    sget-object v24, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 387
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getDistanceGoal()Ljava/lang/Float;

    move-result-object v4

    if-eqz v4, :cond_e

    .line 388
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getDistanceGoal()Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sget-object v24, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 392
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getSleepGoal()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_f

    .line 393
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getSleepGoal()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v4, v4

    sget-object v24, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActiveGoal(FLcom/vectorwatch/android/utils/Constants$GoalType;Landroid/content/Context;)V

    .line 396
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getBacklightSettings()Lcom/vectorwatch/android/models/BackLightItem;

    move-result-object v4

    if-eqz v4, :cond_10

    .line 397
    const-string v4, "pref_backlight_intensity"

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getBacklightSettings()Lcom/vectorwatch/android/models/BackLightItem;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vectorwatch/android/models/BackLightItem;->getIntensity()I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 399
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getBacklightSettings()Lcom/vectorwatch/android/models/BackLightItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/BackLightItem;->getTimeout()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 421
    const/16 v21, 0x0

    .line 424
    .local v21, "timeout":I
    :goto_4
    const-string v4, "pref_backlight_duration"

    move/from16 v0, v21

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setIntPreference(Ljava/lang/String;ILandroid/content/Context;)V

    .line 427
    .end local v21    # "timeout":I
    :cond_10
    const-string v4, "pref_watch_second_hand"

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getSecondsHandActive()Z

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 430
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getAlarmsSettings()Ljava/util/List;

    move-result-object v11

    .line 431
    .local v11, "alarmCloudDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    if-eqz v11, :cond_17

    .line 432
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v5

    .line 434
    .local v5, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/database/DatabaseManager;->deleteAllAlarms(Lio/realm/Realm;)V

    .line 436
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_11
    :goto_5
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;

    .line 437
    .local v10, "alarmCloudDetails":Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;
    if-eqz v10, :cond_11

    .line 438
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->getHour()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 439
    .local v7, "hour":I
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->getMinute()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 440
    .local v8, "minute":I
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->getTitle()Ljava/lang/String;

    move-result-object v6

    .line 441
    .local v6, "alarmName":Ljava/lang/String;
    if-nez v6, :cond_12

    .line 442
    const-string v6, ""

    .line 444
    :cond_12
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->getWeekDays()Ljava/util/List;

    move-result-object v23

    .line 446
    .local v23, "weekDays":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v10}, Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;->getIsEnabled()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    .line 448
    .local v18, "isEnabled":Z
    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/helpers/AlarmHelpers;->getAlarmStatusFromCloudInfo(Ljava/util/List;Z)B

    move-result v9

    .line 450
    .local v9, "status":B
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v4

    invoke-virtual/range {v4 .. v9}, Lcom/vectorwatch/android/database/DatabaseManager;->createAlarm(Lio/realm/Realm;Ljava/lang/String;IIB)V

    goto :goto_5

    .line 315
    .end local v5    # "realm":Lio/realm/Realm;
    .end local v6    # "alarmName":Ljava/lang/String;
    .end local v7    # "hour":I
    .end local v8    # "minute":I
    .end local v9    # "status":B
    .end local v10    # "alarmCloudDetails":Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;
    .end local v11    # "alarmCloudDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    .end local v18    # "isEnabled":Z
    .end local v20    # "timeFormat":Ljava/lang/String;
    .end local v22    # "unitSystem":Ljava/lang/String;
    .end local v23    # "weekDays":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_13
    const-string v4, "F"

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 316
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setActivityInfoGender(ILandroid/content/Context;)V

    goto/16 :goto_1

    .line 326
    .restart local v20    # "timeFormat":Ljava/lang/String;
    :cond_14
    const-string v4, "h24"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 327
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setTimeFormatPreference(ILandroid/content/Context;)V

    goto/16 :goto_2

    .line 338
    .restart local v22    # "unitSystem":Ljava/lang/String;
    :cond_15
    const-string v4, "M"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 339
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setUnitSystemFormatPreference(ILandroid/content/Context;)V

    goto/16 :goto_3

    .line 401
    :sswitch_0
    const/16 v21, 0x0

    .line 402
    .restart local v21    # "timeout":I
    goto/16 :goto_4

    .line 405
    .end local v21    # "timeout":I
    :sswitch_1
    const/16 v21, 0x1

    .line 406
    .restart local v21    # "timeout":I
    goto/16 :goto_4

    .line 409
    .end local v21    # "timeout":I
    :sswitch_2
    const/16 v21, 0x2

    .line 410
    .restart local v21    # "timeout":I
    goto/16 :goto_4

    .line 413
    .end local v21    # "timeout":I
    :sswitch_3
    const/16 v21, 0x3

    .line 414
    .restart local v21    # "timeout":I
    goto/16 :goto_4

    .line 417
    .end local v21    # "timeout":I
    :sswitch_4
    const/16 v21, 0x4

    .line 418
    .restart local v21    # "timeout":I
    goto/16 :goto_4

    .line 454
    .end local v21    # "timeout":I
    .restart local v5    # "realm":Lio/realm/Realm;
    .restart local v11    # "alarmCloudDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AlarmCloudDetailsModel;>;"
    :cond_16
    invoke-virtual {v5}, Lio/realm/Realm;->close()V

    .line 455
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v24, Lcom/vectorwatch/android/events/AlarmSyncFromCloudEvent;

    invoke-direct/range {v24 .. v24}, Lcom/vectorwatch/android/events/AlarmSyncFromCloudEvent;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 458
    .end local v5    # "realm":Lio/realm/Realm;
    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getNotifs()Lcom/vectorwatch/android/models/NotificationSettingsItem;

    move-result-object v4

    if-eqz v4, :cond_1c

    .line 459
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getNotifs()Lcom/vectorwatch/android/models/NotificationSettingsItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/NotificationSettingsItem;->getList()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_1b

    .line 460
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v4

    const-string v24, "app_not_all"

    const-class v25, Ljava/util/List;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/List;

    .line 461
    .local v19, "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v19, :cond_18

    .line 462
    new-instance v19, Ljava/util/ArrayList;

    .end local v19    # "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 464
    .restart local v19    # "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_18
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_6
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v16

    if-ge v0, v4, :cond_19

    .line 465
    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/16 v24, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 464
    add-int/lit8 v16, v16, 0x1

    goto :goto_6

    .line 468
    :cond_19
    const/16 v16, 0x0

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getNotifs()Lcom/vectorwatch/android/models/NotificationSettingsItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/NotificationSettingsItem;->getList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v16

    if-ge v0, v4, :cond_1a

    .line 469
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getNotifs()Lcom/vectorwatch/android/models/NotificationSettingsItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/NotificationSettingsItem;->getList()Ljava/util/List;

    move-result-object v4

    move/from16 v0, v16

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/16 v24, 0x1

    invoke-static/range {v24 .. v24}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 468
    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    .line 471
    :cond_1a
    sget-object v4, Lcom/vectorwatch/android/utils/Helpers;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "alert size list: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getNotifs()Lcom/vectorwatch/android/models/NotificationSettingsItem;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/vectorwatch/android/models/NotificationSettingsItem;->getList()Ljava/util/List;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ""

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 472
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v4

    const-string v24, "app_not_all"

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getNotifs()Lcom/vectorwatch/android/models/NotificationSettingsItem;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/vectorwatch/android/models/NotificationSettingsItem;->getList()Ljava/util/List;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 475
    .end local v16    # "i":I
    .end local v19    # "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getNotifs()Lcom/vectorwatch/android/models/NotificationSettingsItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/NotificationSettingsItem;->isEnable()Z

    move-result v4

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSystemNotification(ZLandroid/content/Context;)V

    .line 478
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUserSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 479
    const-string v4, "pref_link_lost_icon"

    .line 480
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUserSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vectorwatch/android/models/UserSettings;->isLinkLostIcon()Z

    move-result v24

    .line 479
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 481
    const-string v4, "pref_link_lost_notification"

    .line 482
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUserSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vectorwatch/android/models/UserSettings;->isLinkLostNotification()Z

    move-result v24

    .line 481
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 483
    const-string v4, "pref_low_battery_icon"

    .line 484
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUserSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vectorwatch/android/models/UserSettings;->isLowBatteryIcon()Z

    move-result v24

    .line 483
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 485
    const-string v4, "pref_low_battery_notification"

    .line 486
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUserSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vectorwatch/android/models/UserSettings;->isLowBatteryNotification()Z

    move-result v24

    .line 485
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 487
    const-string v4, "pref_goal_achievement"

    .line 488
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUserSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vectorwatch/android/models/UserSettings;->isGoalAchievementAlert()Z

    move-result v24

    .line 487
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 489
    const-string v4, "pref_goal_almost"

    .line 490
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUserSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vectorwatch/android/models/UserSettings;->isGoalAlmostAlert()Z

    move-result v24

    .line 489
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 491
    const-string v4, "pref_activity_reminder"

    .line 492
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUserSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vectorwatch/android/models/UserSettings;->isActivityReminder()Z

    move-result v24

    .line 491
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 493
    const-string v4, "pref_activity_newsletter"

    .line 494
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/RemoteSettingsModel;->getUserSettings()Lcom/vectorwatch/android/models/UserSettings;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/vectorwatch/android/models/UserSettings;->isActivityNewsletter()Z

    move-result v24

    .line 493
    move/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v4, v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    goto/16 :goto_0

    .line 399
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0x14 -> :sswitch_3
        0x1e -> :sswitch_4
    .end sparse-switch
.end method
