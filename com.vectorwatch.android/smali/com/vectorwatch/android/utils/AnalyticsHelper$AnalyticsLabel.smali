.class public final enum Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;
.super Ljava/lang/Enum;
.source "AnalyticsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/AnalyticsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnalyticsLabel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_ACTIVITY_CHART:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_ACTIVITY_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_ALARMS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_ONBOARDING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_SETTINGS_ACCOUNT_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_SETTINGS_ACTIVITY_INFO:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_SETTINGS_ALERTS_NOTIFICATIONS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_SETTINGS_CONTEXTUAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_SETTINGS_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_SETTINGS_WATCH:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_STORE_APPLICATIONS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_STORE_DETAILS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_STORE_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_STORE_STREAMS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_STORE_WATCHFACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_SUPPORT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_UPDATE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

.field public static final enum ANALYTICS_LABEL_WATCHMAKER:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 238
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_WATCHMAKER"

    const-string v2, "Watchmaker"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_WATCHMAKER:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 239
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_STORE_OVERVIEW"

    const-string v2, "Store - Overview"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 240
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_STORE_WATCHFACE"

    const-string v2, "Store - Watchface"

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_WATCHFACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 241
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_STORE_STREAMS"

    const-string v2, "Store - Streams"

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_STREAMS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 242
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_STORE_APPLICATIONS"

    const-string v2, "Store - Applications"

    invoke-direct {v0, v1, v8, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_APPLICATIONS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 243
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_STORE_DETAILS"

    const/4 v2, 0x5

    const-string v3, "Store details"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_DETAILS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 244
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_SETTINGS_OVERVIEW"

    const/4 v2, 0x6

    const-string v3, "Settings - Overview"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 245
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_SETTINGS_ACCOUNT_PROFILE"

    const/4 v2, 0x7

    const-string v3, "Settings - Account Profile"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_ACCOUNT_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 246
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_SETTINGS_ACTIVITY_INFO"

    const/16 v2, 0x8

    const-string v3, "Settings - Activity Info"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_ACTIVITY_INFO:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 247
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_SETTINGS_ALERTS_NOTIFICATIONS"

    const/16 v2, 0x9

    const-string v3, "Settings - Alerts / Notifications"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_ALERTS_NOTIFICATIONS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 248
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_SETTINGS_CONTEXTUAL"

    const/16 v2, 0xa

    const-string v3, "Settings - Contextual"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_CONTEXTUAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 249
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_SETTINGS_WATCH"

    const/16 v2, 0xb

    const-string v3, "Settings - Watch"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_WATCH:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 250
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_ALARMS"

    const/16 v2, 0xc

    const-string v3, "Alarms"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ALARMS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 251
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_ACTIVITY_OVERVIEW"

    const/16 v2, 0xd

    const-string v3, "Activity - Overview"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ACTIVITY_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 252
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_ACTIVITY_CHART"

    const/16 v2, 0xe

    const-string v3, "Activity chart"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ACTIVITY_CHART:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 253
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_ONBOARDING"

    const/16 v2, 0xf

    const-string v3, "Onboarding"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ONBOARDING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 254
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_SUPPORT"

    const/16 v2, 0x10

    const-string v3, "Support"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SUPPORT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 255
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    const-string v1, "ANALYTICS_LABEL_UPDATE"

    const/16 v2, 0x11

    const-string v3, "Update"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_UPDATE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .line 237
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_WATCHMAKER:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_WATCHFACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_STREAMS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_APPLICATIONS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_DETAILS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_ACCOUNT_PROFILE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_ACTIVITY_INFO:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_ALERTS_NOTIFICATIONS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_CONTEXTUAL:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_WATCH:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ALARMS:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ACTIVITY_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ACTIVITY_CHART:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ONBOARDING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SUPPORT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_UPDATE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->$VALUES:[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 259
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 260
    iput-object p3, p0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->name:Ljava/lang/String;

    .line 261
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 237
    const-class v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->$VALUES:[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->name:Ljava/lang/String;

    return-object v0
.end method
