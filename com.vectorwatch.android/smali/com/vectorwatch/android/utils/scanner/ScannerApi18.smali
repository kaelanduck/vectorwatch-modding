.class public Lcom/vectorwatch/android/utils/scanner/ScannerApi18;
.super Ljava/lang/Object;
.source "ScannerApi18.java"

# interfaces
.implements Lcom/vectorwatch/android/utils/scanner/VectorScanner;


# instance fields
.field private mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1;

    invoke-direct {v0, p0, p1}, Lcom/vectorwatch/android/utils/scanner/ScannerApi18$1;-><init>(Lcom/vectorwatch/android/utils/scanner/ScannerApi18;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 57
    return-void
.end method


# virtual methods
.method public startScan(Landroid/bluetooth/BluetoothAdapter;)V
    .locals 1
    .param p1, "bluetoothAdapter"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    .line 62
    return-void
.end method

.method public stopScan(Landroid/bluetooth/BluetoothAdapter;)V
    .locals 1
    .param p1, "bluetoothAdapter"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi18;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 67
    return-void
.end method
