.class public Lcom/vectorwatch/android/utils/scanner/ScannerApi21;
.super Ljava/lang/Object;
.source "ScannerApi21.java"

# interfaces
.implements Lcom/vectorwatch/android/utils/scanner/VectorScanner;


# instance fields
.field private scanCallback:Landroid/bluetooth/le/ScanCallback;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 26
    new-instance v0, Lcom/vectorwatch/android/utils/scanner/ScannerApi21$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/utils/scanner/ScannerApi21$1;-><init>(Lcom/vectorwatch/android/utils/scanner/ScannerApi21;)V

    iput-object v0, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi21;->scanCallback:Landroid/bluetooth/le/ScanCallback;

    .line 68
    :cond_0
    return-void
.end method


# virtual methods
.method public startScan(Landroid/bluetooth/BluetoothAdapter;)V
    .locals 2
    .param p1, "bluetoothAdapter"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 72
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 73
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi21;->scanCallback:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Landroid/bluetooth/le/ScanCallback;)V

    .line 75
    :cond_0
    return-void
.end method

.method public stopScan(Landroid/bluetooth/BluetoothAdapter;)V
    .locals 2
    .param p1, "bluetoothAdapter"    # Landroid/bluetooth/BluetoothAdapter;

    .prologue
    .line 79
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 80
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeScanner()Landroid/bluetooth/le/BluetoothLeScanner;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/utils/scanner/ScannerApi21;->scanCallback:Landroid/bluetooth/le/ScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V

    .line 83
    :cond_0
    return-void
.end method
