.class public interface abstract Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;
.super Ljava/lang/Object;
.source "LocationHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/LocationHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VectorLocationCallback"
.end annotation


# virtual methods
.method public abstract onAccurateLocation(Landroid/location/Location;)V
.end method

.method public abstract onLocation(Landroid/location/Location;)V
.end method

.method public abstract onTimeoutError()V
.end method
