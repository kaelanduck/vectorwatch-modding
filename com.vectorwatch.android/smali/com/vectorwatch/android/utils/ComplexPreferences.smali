.class public Lcom/vectorwatch/android/utils/ComplexPreferences;
.super Ljava/lang/Object;
.source "ComplexPreferences.java"


# static fields
.field private static GSON:Lcom/google/gson/Gson;

.field private static complexPreferences:Lcom/vectorwatch/android/utils/ComplexPreferences;


# instance fields
.field private final context:Landroid/content/Context;

.field private final editor:Landroid/content/SharedPreferences$Editor;

.field private final preferences:Landroid/content/SharedPreferences;

.field typeOfObject:Ljava/lang/reflect/Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/utils/ComplexPreferences;->GSON:Lcom/google/gson/Gson;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "namePreferences"    # Ljava/lang/String;
    .param p3, "mode"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Lcom/vectorwatch/android/utils/ComplexPreferences$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/utils/ComplexPreferences$1;-><init>(Lcom/vectorwatch/android/utils/ComplexPreferences;)V

    .line 20
    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/ComplexPreferences$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->typeOfObject:Ljava/lang/reflect/Type;

    .line 23
    iput-object p1, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->context:Landroid/content/Context;

    .line 24
    if-eqz p2, :cond_0

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25
    :cond_0
    const-string p2, "noKey"

    .line 27
    :cond_1
    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->preferences:Landroid/content/SharedPreferences;

    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    .line 29
    return-void
.end method

.method public static getComplexPreferences(Landroid/content/Context;Ljava/lang/String;I)Lcom/vectorwatch/android/utils/ComplexPreferences;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "namePreferences"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .prologue
    .line 33
    sget-object v0, Lcom/vectorwatch/android/utils/ComplexPreferences;->complexPreferences:Lcom/vectorwatch/android/utils/ComplexPreferences;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/vectorwatch/android/utils/ComplexPreferences;

    invoke-direct {v0, p0, p1, p2}, Lcom/vectorwatch/android/utils/ComplexPreferences;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/ComplexPreferences;->complexPreferences:Lcom/vectorwatch/android/utils/ComplexPreferences;

    .line 37
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/utils/ComplexPreferences;->complexPreferences:Lcom/vectorwatch/android/utils/ComplexPreferences;

    return-object v0
.end method


# virtual methods
.method public getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .local p2, "a":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v2, 0x0

    .line 62
    iget-object v3, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v3, p1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "gson":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 67
    :goto_0
    return-object v2

    :cond_0
    :try_start_0
    sget-object v2, Lcom/vectorwatch/android/utils/ComplexPreferences;->GSON:Lcom/google/gson/Gson;

    invoke-virtual {v2, v1, p2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object stored with key "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is instance of other class"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public putObject(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 41
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 42
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Key is empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_1
    if-nez p2, :cond_2

    .line 45
    iget-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 49
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 50
    return-void

    .line 47
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    sget-object v1, Lcom/vectorwatch/android/utils/ComplexPreferences;->GSON:Lcom/google/gson/Gson;

    invoke-virtual {v1, p2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 58
    iget-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 59
    return-void
.end method

.method public save()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/vectorwatch/android/utils/ComplexPreferences;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 54
    return-void
.end method
