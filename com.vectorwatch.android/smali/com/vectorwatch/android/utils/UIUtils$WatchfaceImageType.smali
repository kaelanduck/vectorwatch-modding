.class public final enum Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;
.super Ljava/lang/Enum;
.source "UIUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/UIUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WatchfaceImageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

.field public static final enum WATCHMAKER_EDIT_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

.field public static final enum WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    new-instance v0, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    const-string v1, "WATCHMAKER_IMAGE"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    new-instance v0, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    const-string v1, "WATCHMAKER_EDIT_IMAGE"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_EDIT_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    .line 87
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    sget-object v1, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_EDIT_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->$VALUES:[Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    const-class v0, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->$VALUES:[Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    return-object v0
.end method
