.class public Lcom/vectorwatch/android/utils/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/Constants$GoalType;,
        Lcom/vectorwatch/android/utils/Constants$CurrentAppOption;,
        Lcom/vectorwatch/android/utils/Constants$VftpMessageType;,
        Lcom/vectorwatch/android/utils/Constants$VftpFileType;,
        Lcom/vectorwatch/android/utils/Constants$VftpStatus;,
        Lcom/vectorwatch/android/utils/Constants$InstallStatus;,
        Lcom/vectorwatch/android/utils/Constants$RepeatOption;,
        Lcom/vectorwatch/android/utils/Constants$AppInstallFailReasons;,
        Lcom/vectorwatch/android/utils/Constants$ButtonCallbackIndex;,
        Lcom/vectorwatch/android/utils/Constants$SystemButtonCallbackIndex;,
        Lcom/vectorwatch/android/utils/Constants$WatchAnimation;,
        Lcom/vectorwatch/android/utils/Constants$NotificationMode;,
        Lcom/vectorwatch/android/utils/Constants$SettingsType;,
        Lcom/vectorwatch/android/utils/Constants$GoogleFitData;,
        Lcom/vectorwatch/android/utils/Constants$SupportedLanguages;,
        Lcom/vectorwatch/android/utils/Constants$ElementAlignment;,
        Lcom/vectorwatch/android/utils/Constants$AppElementType;,
        Lcom/vectorwatch/android/utils/Constants$ElementType;,
        Lcom/vectorwatch/android/utils/Constants$MessageInstallType;
    }
.end annotation


# static fields
.field public static final APP_NAME:Ljava/lang/String; = "MOBILE_APP_NEWSLETTER_LIST"

.field public static BACKLIGHT_INTENSITY_MAX:I = 0x0

.field public static BACKLIGHT_INTENSITY_STEP_SIZE:I = 0x0

.field public static final BACKLIGHT_TIMEOUT_DURATION_10:I = 0xa

.field public static final BACKLIGHT_TIMEOUT_DURATION_2:I = 0x2

.field public static final BACKLIGHT_TIMEOUT_DURATION_20:I = 0x14

.field public static final BACKLIGHT_TIMEOUT_DURATION_30:I = 0x1e

.field public static final BACKLIGHT_TIMEOUT_DURATION_5:I = 0x5

.field public static final BACKLIGH_INTENSITY_UNSET:I = 0x0

.field public static final BACKLIGH_TIMEOUT_10_SEC:I = 0x2

.field public static final BACKLIGH_TIMEOUT_20_SEC:I = 0x3

.field public static final BACKLIGH_TIMEOUT_2_SEC:I = 0x0

.field public static final BACKLIGH_TIMEOUT_30_SEC:I = 0x4

.field public static final BACKLIGH_TIMEOUT_5_SEC:I = 0x1

.field public static final BACKLIGH_TIMEOUT_UNSET:I = -0x1

.field public static final BATTERY_INTENT_FIRE_INTERVAL_MILLIS:J = 0xdbba0L

.field public static final COMMAND_ACTIVITY_ANNOUNCE:S = 0x1as

.field public static final COMMAND_BEGIN_UPDATE:S = 0x1s

.field public static final COMMAND_BOOT:S = 0x4s

.field public static final COMMAND_CHANGE_APP:S = 0x12s

.field public static final COMMAND_CHANGE_TIMING_FAST:S = 0xas

.field public static final COMMAND_CHANGE_TIMING_SLOW:S = 0xbs

.field public static final COMMAND_CRASH_ANNOUNCE:S = 0x13s

.field public static final COMMAND_CRASH_LOGS_ERASE:S = 0x15s

.field public static final COMMAND_CRASH_LOGS_SEND:S = 0x14s

.field public static final COMMAND_DELETE_CLOUD_STREAMS:S = 0x1ds

.field public static final COMMAND_FACTORY_RESET:S = 0x6s

.field public static final COMMAND_FIND_MY_PHONE:S = 0x18s

.field public static final COMMAND_GET_ACTIVITY:S = 0xcs

.field public static final COMMAND_GET_BATTERY:S = 0xds

.field public static final COMMAND_GET_BLE_STATUS:S = 0xes

.field public static final COMMAND_GET_SERIAL_NUMBER:S = 0x17s

.field public static final COMMAND_GET_SYSTEM_INFO:S = 0x2s

.field public static final COMMAND_GET_UPDATE_STATUS:S = 0x11s

.field public static final COMMAND_GET_UUID:S = 0xfs

.field public static final COMMAND_NOP:S = 0x0s

.field public static final COMMAND_REBOOT:S = 0x3s

.field public static final COMMAND_SHARE_ACTIVITY:S = 0x19s

.field public static final COMMAND_SYNC_APP_ORDER:S = 0x1bs

.field public static final COMMAND_UNPAIR:S = 0x5s

.field public static final COMMAND_UPDATE_BIOS:S = 0x9s

.field public static final COMMAND_UPDATE_BOOTLOADER:S = 0x8s

.field public static final COMMAND_UPDATE_KERNEL:S = 0x7s

.field public static final DEFAULT_BACKLIGHT_INTENSITY:I = 0x1

.field public static final DEFAULT_BACKLIGHT_TIMEOUT:I = 0x2

.field public static final DEVICE_TYPE:Ljava/lang/String; = "android"

.field public static final DUMMY2:S = 0x10s

.field public static final NEWS_NAME:Ljava/lang/String; = "MOBILE_APP_MARKETING_NEWSLETTER_LIST"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 598
    const/4 v0, 0x2

    sput v0, Lcom/vectorwatch/android/utils/Constants;->BACKLIGHT_INTENSITY_MAX:I

    .line 599
    const/16 v0, 0x64

    sget v1, Lcom/vectorwatch/android/utils/Constants;->BACKLIGHT_INTENSITY_MAX:I

    add-int/lit8 v1, v1, 0x1

    div-int/2addr v0, v1

    sput v0, Lcom/vectorwatch/android/utils/Constants;->BACKLIGHT_INTENSITY_STEP_SIZE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 774
    return-void
.end method

.method public static getElementType(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$AppElementType;
    .locals 1
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 215
    const-string v0, "DYNAMIC_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    .line 236
    :goto_0
    return-object v0

    .line 218
    :cond_0
    const-string v0, "TEXT_ELEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_COMPLICATION:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 221
    :cond_1
    const-string v0, "CLOUD_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_COMPLICATION:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 224
    :cond_2
    const-string v0, "LONG_TEXT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 225
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_TEXT_MULTIPLE_LINES:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 227
    :cond_3
    const-string v0, "LIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 228
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_LIST:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 230
    :cond_4
    const-string v0, "GAUGE_ELEMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 231
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_GAUGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 233
    :cond_5
    const-string v0, "DYNAMIC_IMAGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 234
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0

    .line 236
    :cond_6
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$AppElementType;->ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$AppElementType;

    goto :goto_0
.end method

.method public static getElementTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$ElementType;
    .locals 1
    .param p0, "option"    # I

    .prologue
    .line 142
    packed-switch p0, :pswitch_data_0

    .line 185
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 144
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 146
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 148
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_HOUR_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 150
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_MINUTE_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 152
    :pswitch_4
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_BITMAP:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 154
    :pswitch_5
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_BACKGROUND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 156
    :pswitch_6
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_DIGITAL_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 158
    :pswitch_7
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_LONG_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 160
    :pswitch_8
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_LINE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 162
    :pswitch_9
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NORMAL_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 164
    :pswitch_a
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NORMAL_MINUTE_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 166
    :pswitch_b
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_SYSTEM_VARS:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 168
    :pswitch_c
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_NORMAL_HOUR_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 170
    :pswitch_d
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 172
    :pswitch_e
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_STATIC_TIME:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 174
    :pswitch_f
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_CLOUD_TEXT:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 176
    :pswitch_10
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_ANALOG_DAY:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 178
    :pswitch_11
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_DYNAMIC_IMAGE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 180
    :pswitch_12
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_TIMEZONE:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 182
    :pswitch_13
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$ElementType;->PROT_ELEMENT_TYPE_LIVE_STREAM:Lcom/vectorwatch/android/utils/Constants$ElementType;

    goto :goto_0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public static getGoalTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$GoalType;
    .locals 1
    .param p0, "option"    # I

    .prologue
    .line 789
    packed-switch p0, :pswitch_data_0

    .line 799
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 791
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoalType;

    goto :goto_0

    .line 793
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoalType;

    goto :goto_0

    .line 795
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoalType;

    goto :goto_0

    .line 797
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoalType;->SLEEP:Lcom/vectorwatch/android/utils/Constants$GoalType;

    goto :goto_0

    .line 789
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getMessageInstallTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$MessageInstallType;
    .locals 1
    .param p0, "option"    # I

    .prologue
    .line 92
    packed-switch p0, :pswitch_data_0

    .line 104
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    :goto_0
    return-object v0

    .line 94
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_PUT:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    goto :goto_0

    .line 96
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_REQ:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    goto :goto_0

    .line 98
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_STATUS:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    goto :goto_0

    .line 100
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_FINISH:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    goto :goto_0

    .line 102
    :pswitch_4
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$MessageInstallType;->INSTALL_MESSAGE_TYPE_REMOVE:Lcom/vectorwatch/android/utils/Constants$MessageInstallType;

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getNotificationModeFromValue(I)Lcom/vectorwatch/android/utils/Constants$NotificationMode;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 426
    packed-switch p0, :pswitch_data_0

    .line 432
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_SHOW_ALERT:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    :goto_0
    return-object v0

    .line 428
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_OFF:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    goto :goto_0

    .line 430
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$NotificationMode;->NOTIFICATIONS_MODE_SHOW_CONTENTS:Lcom/vectorwatch/android/utils/Constants$NotificationMode;

    goto :goto_0

    .line 426
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getSettingsTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$SettingsType;
    .locals 1
    .param p0, "option"    # I

    .prologue
    .line 361
    packed-switch p0, :pswitch_data_0

    .line 399
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 363
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_NAME:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 365
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AGE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 367
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_GENDER:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 369
    :pswitch_4
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_WEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 371
    :pswitch_5
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_HEIGHT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 373
    :pswitch_6
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AF:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 375
    :pswitch_7
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_UNIT_SYSTEM:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 377
    :pswitch_8
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_HOUR_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 379
    :pswitch_9
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_MORNING_GREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 381
    :pswitch_a
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_DISCREET_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 383
    :pswitch_b
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_AUTO_SLEEP_ENABLE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 385
    :pswitch_c
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_DROP_MODE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 387
    :pswitch_d
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_DND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 389
    :pswitch_e
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_GLANCE:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 391
    :pswitch_f
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_BACKLIGHT_LEVEL:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 393
    :pswitch_10
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_BACKLIGHT_TIMEOUT:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 395
    :pswitch_11
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_TYPE_SHOW_SECOND_HAND:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 397
    :pswitch_12
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$SettingsType;->SETTING_LOCALIZATION:Lcom/vectorwatch/android/utils/Constants$SettingsType;

    goto :goto_0

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public static getVftpFileTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpFileType;
    .locals 1
    .param p0, "option"    # I

    .prologue
    .line 694
    packed-switch p0, :pswitch_data_0

    .line 708
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    :goto_0
    return-object v0

    .line 696
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_ANY:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    goto :goto_0

    .line 698
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_RESOURCE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    goto :goto_0

    .line 700
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_APPLICATION:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    goto :goto_0

    .line 702
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    goto :goto_0

    .line 704
    :pswitch_4
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_LOCALE:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    goto :goto_0

    .line 706
    :pswitch_5
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpFileType;->FILE_TYPE_TMP:Lcom/vectorwatch/android/utils/Constants$VftpFileType;

    goto :goto_0

    .line 694
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getVftpMessageTypeFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpMessageType;
    .locals 1
    .param p0, "option"    # I

    .prologue
    .line 749
    packed-switch p0, :pswitch_data_0

    .line 759
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    :goto_0
    return-object v0

    .line 751
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_REQ:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    goto :goto_0

    .line 753
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_PUT:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    goto :goto_0

    .line 755
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_DATA:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    goto :goto_0

    .line 757
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpMessageType;->VFTP_MESSAGE_TYPE_STATUS:Lcom/vectorwatch/android/utils/Constants$VftpMessageType;

    goto :goto_0

    .line 749
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getVftpStatusFromValue(I)Lcom/vectorwatch/android/utils/Constants$VftpStatus;
    .locals 1
    .param p0, "option"    # I

    .prologue
    .line 653
    packed-switch p0, :pswitch_data_0

    .line 665
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_UNKNOWN:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    :goto_0
    return-object v0

    .line 655
    :pswitch_0
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_SUCCESS:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    goto :goto_0

    .line 657
    :pswitch_1
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_NO_SPACE:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    goto :goto_0

    .line 659
    :pswitch_2
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_FILE_NOT_FOUND:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    goto :goto_0

    .line 661
    :pswitch_3
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_FILE_EXISTS:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    goto :goto_0

    .line 663
    :pswitch_4
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$VftpStatus;->VFTP_STATUS_ERROR:Lcom/vectorwatch/android/utils/Constants$VftpStatus;

    goto :goto_0

    .line 653
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
