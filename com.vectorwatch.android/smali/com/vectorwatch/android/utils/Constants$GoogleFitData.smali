.class public final enum Lcom/vectorwatch/android/utils/Constants$GoogleFitData;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GoogleFitData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/Constants$GoogleFitData;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

.field public static final enum ALL:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

.field public static final enum CALORIES:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

.field public static final enum DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

.field public static final enum STEPS:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 310
    new-instance v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    const-string v1, "STEPS"

    invoke-direct {v0, v1, v3, v3}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    const-string v1, "CALORIES"

    invoke-direct {v0, v1, v4, v4}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    const-string v1, "DISTANCE"

    invoke-direct {v0, v1, v5, v5}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    new-instance v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    const-string v1, "ALL"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->ALL:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    .line 309
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->ALL:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    aput-object v1, v0, v6

    sput-object v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 314
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 315
    iput p3, p0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->val:I

    .line 316
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/Constants$GoogleFitData;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 309
    const-class v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/Constants$GoogleFitData;
    .locals 1

    .prologue
    .line 309
    sget-object v0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->$VALUES:[Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->val:I

    return v0
.end method
