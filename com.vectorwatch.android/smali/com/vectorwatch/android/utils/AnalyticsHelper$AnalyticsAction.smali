.class public final enum Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;
.super Ljava/lang/Enum;
.source "AnalyticsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/utils/AnalyticsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnalyticsAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_ADDED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_CLOSED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_CONNECTED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_CREATED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_CUSTOMIZED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_DECREASED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_DELETED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_DISABLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_DISCONNECTED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_DOWNLOADED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_ENABLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_FILLED_OUT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_FILTERED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_INCREASED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_LOGGED_IN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_OPENED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_REMOVED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_REORDERED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_SEARCHED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_SENT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_SHARED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_SORTED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_SWITCHED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_TAPPED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

.field public static final enum ANALYTICS_ACTION_UPDATED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 196
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_DOWNLOADED"

    const-string v2, "downloaded"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DOWNLOADED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 197
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_INSTALLED"

    const-string v2, "installed"

    invoke-direct {v0, v1, v5, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 198
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_UNINSTALLED"

    const-string v2, "uninstalled"

    invoke-direct {v0, v1, v6, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 199
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_DELETED"

    const-string v2, "deleted"

    invoke-direct {v0, v1, v7, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DELETED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 200
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_UPDATED"

    const-string v2, "updated"

    invoke-direct {v0, v1, v8, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UPDATED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 201
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_ENABLED"

    const/4 v2, 0x5

    const-string v3, "enabled"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_ENABLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 202
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_DISABLED"

    const/4 v2, 0x6

    const-string v3, "disabled"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DISABLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 203
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_SET"

    const/4 v2, 0x7

    const-string v3, "set"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 204
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_CREATED"

    const/16 v2, 0x8

    const-string v3, "created"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CREATED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 205
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_REMOVED"

    const/16 v2, 0x9

    const-string v3, "removed"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_REMOVED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 206
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_FILTERED"

    const/16 v2, 0xa

    const-string v3, "filtered"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_FILTERED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 207
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_SORTED"

    const/16 v2, 0xb

    const-string v3, "sorted"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SORTED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 208
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_CLOSED"

    const/16 v2, 0xc

    const-string v3, "closed"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CLOSED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 209
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_OPENED"

    const/16 v2, 0xd

    const-string v3, "opened"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_OPENED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 210
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_SWITCHED"

    const/16 v2, 0xe

    const-string v3, "switched"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SWITCHED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 211
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_TAPPED"

    const/16 v2, 0xf

    const-string v3, "tapped"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_TAPPED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 212
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_INCREASED"

    const/16 v2, 0x10

    const-string v3, "increased"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INCREASED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 213
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_DECREASED"

    const/16 v2, 0x11

    const-string v3, "decreased"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DECREASED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 214
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_FILLED_OUT"

    const/16 v2, 0x12

    const-string v3, "filled out"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_FILLED_OUT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 215
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_ADDED"

    const/16 v2, 0x13

    const-string v3, "added"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_ADDED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 216
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_SENT"

    const/16 v2, 0x14

    const-string v3, "sent"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SENT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 217
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_CHANGED"

    const/16 v2, 0x15

    const-string v3, "changed"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 218
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_LOGGED_IN"

    const/16 v2, 0x16

    const-string v3, "logged in"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_LOGGED_IN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 219
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_SEARCHED"

    const/16 v2, 0x17

    const-string v3, "searched"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SEARCHED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 220
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_CUSTOMIZED"

    const/16 v2, 0x18

    const-string v3, "customized"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CUSTOMIZED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 221
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_CONNECTED"

    const/16 v2, 0x19

    const-string v3, "connected"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CONNECTED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 222
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_REORDERED"

    const/16 v2, 0x1a

    const-string v3, "reordered"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_REORDERED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 223
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_SHARED"

    const/16 v2, 0x1b

    const-string v3, "shared"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SHARED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 224
    new-instance v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const-string v1, "ANALYTICS_ACTION_DISCONNECTED"

    const/16 v2, 0x1c

    const-string v3, "disconnected"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DISCONNECTED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 194
    const/16 v0, 0x1d

    new-array v0, v0, [Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DOWNLOADED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UNINSTALLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DELETED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v1, v0, v7

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_UPDATED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_ENABLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DISABLED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CREATED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_REMOVED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_FILTERED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SORTED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CLOSED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_OPENED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SWITCHED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_TAPPED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_INCREASED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DECREASED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_FILLED_OUT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_ADDED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SENT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_LOGGED_IN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SEARCHED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CUSTOMIZED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CONNECTED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_REORDERED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SHARED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_DISCONNECTED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->$VALUES:[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 229
    iput-object p3, p0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->name:Ljava/lang/String;

    .line 230
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 194
    const-class v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;
    .locals 1

    .prologue
    .line 194
    sget-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->$VALUES:[Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->name:Ljava/lang/String;

    return-object v0
.end method
