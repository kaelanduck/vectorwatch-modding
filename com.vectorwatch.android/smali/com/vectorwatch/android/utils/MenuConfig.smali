.class public Lcom/vectorwatch/android/utils/MenuConfig;
.super Ljava/lang/Object;
.source "MenuConfig.java"


# static fields
.field private static mInstance:Lcom/vectorwatch/android/utils/MenuConfig;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/vectorwatch/android/utils/MenuConfig;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/vectorwatch/android/utils/MenuConfig;->mInstance:Lcom/vectorwatch/android/utils/MenuConfig;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/vectorwatch/android/utils/MenuConfig;

    invoke-direct {v0}, Lcom/vectorwatch/android/utils/MenuConfig;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/utils/MenuConfig;->mInstance:Lcom/vectorwatch/android/utils/MenuConfig;

    .line 27
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/utils/MenuConfig;->mInstance:Lcom/vectorwatch/android/utils/MenuConfig;

    return-object v0
.end method


# virtual methods
.method public getChartsTabMenu(Landroid/content/Context;)[Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
