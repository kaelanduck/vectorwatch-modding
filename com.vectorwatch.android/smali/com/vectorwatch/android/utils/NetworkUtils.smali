.class public Lcom/vectorwatch/android/utils/NetworkUtils;
.super Ljava/lang/Object;
.source "NetworkUtils.java"


# static fields
.field public static final NETWORK_STATUS_MOBILE:I = 0x2

.field public static final NETWORK_STATUS_NOT_CONNECTED:I = 0x0

.field public static final NETWORK_STATUS_WIFI:I = 0x1

.field public static TYPE_MOBILE:I

.field public static TYPE_NOT_CONNECTED:I

.field public static TYPE_WIFI:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x1

    sput v0, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_WIFI:I

    .line 15
    const/4 v0, 0x2

    sput v0, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_MOBILE:I

    .line 16
    const/4 v0, 0x0

    sput v0, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_NOT_CONNECTED:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getConnectivityStatus(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 27
    .local v1, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 28
    .local v0, "activeNetwork":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_1

    .line 29
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 30
    sget v2, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_WIFI:I

    .line 35
    :goto_0
    return v2

    .line 32
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-nez v2, :cond_1

    .line 33
    sget v2, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_MOBILE:I

    goto :goto_0

    .line 35
    :cond_1
    sget v2, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_NOT_CONNECTED:I

    goto :goto_0
.end method

.method public static getConnectivityStatusString(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-static {p0}, Lcom/vectorwatch/android/utils/NetworkUtils;->getConnectivityStatus(Landroid/content/Context;)I

    move-result v0

    .line 49
    .local v0, "conn":I
    const/4 v1, 0x0

    .line 50
    .local v1, "status":I
    sget v2, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_WIFI:I

    if-ne v0, v2, :cond_1

    .line 51
    const/4 v1, 0x1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 52
    :cond_1
    sget v2, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_MOBILE:I

    if-ne v0, v2, :cond_2

    .line 53
    const/4 v1, 0x2

    goto :goto_0

    .line 54
    :cond_2
    sget v2, Lcom/vectorwatch/android/utils/NetworkUtils;->TYPE_NOT_CONNECTED:I

    if-ne v0, v2, :cond_0

    .line 55
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isOnline(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    :try_start_0
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 41
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 43
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    :goto_0
    return v2

    .line 42
    :catch_0
    move-exception v1

    .line 43
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method
