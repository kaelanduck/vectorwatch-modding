.class public Lcom/vectorwatch/android/utils/CloudStatusCodes;
.super Ljava/lang/Object;
.source "CloudStatusCodes.java"


# static fields
.field public static final ACCEPTED:I = 0xca

.field public static final ALREADY_REPORTED:I = 0xd0

.field public static final BAD_GATEWAY:I = 0x1f6

.field public static final BAD_REQUEST:I = 0x190

.field public static final CONFLICT:I = 0x199

.field public static final CREATED:I = 0xc9

.field public static final DEV_SERVER_ERROR:I = 0x384

.field public static final EXPECTATION_FAILED:I = 0x1a1

.field public static final FAILED_DEPENDENCY:I = 0x1a8

.field public static final FORBIDDEN:I = 0x193

.field public static final FOUND:I = 0x12e

.field public static final GATEWAY_TIMEOUT:I = 0x1f8

.field public static final GONE:I = 0x19a

.field public static final HTTP_VERSION_NOT_SUPPORTED:I = 0x1f9

.field public static final IM_USED:I = 0xe2

.field public static final INSUFFICIENT_STORAGE:I = 0x1fb

.field public static final INTERNAL_SERVER_ERROR:I = 0x1f4

.field public static final LENGTH_REQUIRED:I = 0x19b

.field public static final LOCKED:I = 0x1a7

.field public static final LOOP_DETECTED:I = 0x1fc

.field public static final METHOD_NOT_ALLOWED:I = 0x195

.field public static final MISDIRECTED_REQUEST:I = 0x1a5

.field public static final MOVED_PERMANENTLY:I = 0x12d

.field public static final MULTIPLE_CHOICE:I = 0x12c

.field public static final MULTI_STATUS:I = 0xcf

.field public static final NETWORK_AUTH_REQUIRED:I = 0x1ff

.field public static final NON_AUTHORITATIVE_INFORMATION:I = 0xcb

.field public static final NOT_ACCEPTABLE:I = 0x196

.field public static final NOT_EXTENDED:I = 0x1fe

.field public static final NOT_FOUND:I = 0x194

.field public static final NOT_FOUND_EXTRA:I = 0x388

.field public static final NOT_IMPLEMENTED:I = 0x1f5

.field public static final NOT_MODIFIED:I = 0x130

.field public static final NOT_SATISFIABLE:I = 0x1a0

.field public static final NO_CONTENT:I = 0xcc

.field public static final OK:I = 0xc8

.field public static final PARTIAL_CONTENT:I = 0xce

.field public static final PAYLOAD_TOO_LARGE:I = 0x19d

.field public static final PERMANENT_REDIRECT:I = 0x134

.field public static final PRECONDITION_FAILED:I = 0x19c

.field public static final PRECONDITION_REQUIRED:I = 0x1ac

.field public static final PROXY_AUTH_REQUIRED:I = 0x197

.field public static final RATE_LIMIT:I = 0x3a1

.field public static final REQUESTED_TIMEOUT:I = 0x198

.field public static final REQUIRED:I = 0x192

.field public static final REQ_HEADER_FIELDS_TOO_LARGE:I = 0x1af

.field public static final RESET_CONTENT:I = 0xcd

.field public static final SEE_OTHER:I = 0x12f

.field public static final SERVICE_UNAVAILABLE:I = 0x1f7

.field public static final SWITCH_PROXY:I = 0x132

.field public static final TEMPORARY_REDIRECT:I = 0x133

.field public static final TOKEN_EXPIRED:I = 0x385

.field public static final TOO_MANY_REQUESTS:I = 0x1ad

.field public static final UNAUTHORIZED:I = 0x191

.field public static final UNAVAILABLE_FOR_LEGAL_REASONS:I = 0x1c3

.field public static final UNDEFINED:I = -0x1

.field public static final UNPROCESSABLE_ENTITY:I = 0x1a6

.field public static final UNSUPPORTED_MEDIA_TYPE:I = 0x19f

.field public static final UPGRADE_REQUIRED:I = 0x1aa

.field public static final URI_TOO_LONG:I = 0x19e

.field public static final USE_PROXY:I = 0x131

.field public static final VARIANT_ALSO_NEGOTIATES:I = 0x1fa


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultStringIdForStatus(I)I
    .locals 1
    .param p0, "status"    # I

    .prologue
    .line 91
    const/4 v0, -0x1

    .line 92
    .local v0, "messageStringId":I
    sparse-switch p0, :sswitch_data_0

    .line 277
    :goto_0
    return v0

    .line 94
    :sswitch_0
    const v0, 0x7f0900c2

    .line 95
    goto :goto_0

    .line 97
    :sswitch_1
    const v0, 0x7f0900c3

    .line 98
    goto :goto_0

    .line 100
    :sswitch_2
    const v0, 0x7f0900c4

    .line 101
    goto :goto_0

    .line 103
    :sswitch_3
    const v0, 0x7f0900c5

    .line 104
    goto :goto_0

    .line 106
    :sswitch_4
    const v0, 0x7f0900c6

    .line 107
    goto :goto_0

    .line 109
    :sswitch_5
    const v0, 0x7f0900c7

    .line 110
    goto :goto_0

    .line 112
    :sswitch_6
    const v0, 0x7f0900c8

    .line 113
    goto :goto_0

    .line 115
    :sswitch_7
    const v0, 0x7f0900c9

    .line 116
    goto :goto_0

    .line 118
    :sswitch_8
    const v0, 0x7f0900ca

    .line 119
    goto :goto_0

    .line 121
    :sswitch_9
    const v0, 0x7f0900cb

    .line 122
    goto :goto_0

    .line 124
    :sswitch_a
    const v0, 0x7f0900cc

    .line 125
    goto :goto_0

    .line 127
    :sswitch_b
    const v0, 0x7f0900cd

    .line 128
    goto :goto_0

    .line 130
    :sswitch_c
    const v0, 0x7f0900ce

    .line 131
    goto :goto_0

    .line 133
    :sswitch_d
    const v0, 0x7f0900cf

    .line 134
    goto :goto_0

    .line 136
    :sswitch_e
    const v0, 0x7f0900d0

    .line 137
    goto :goto_0

    .line 139
    :sswitch_f
    const v0, 0x7f0900d1

    .line 140
    goto :goto_0

    .line 142
    :sswitch_10
    const v0, 0x7f0900d2

    .line 143
    goto :goto_0

    .line 145
    :sswitch_11
    const v0, 0x7f0900d3

    .line 146
    goto :goto_0

    .line 148
    :sswitch_12
    const v0, 0x7f0900d4

    .line 149
    goto :goto_0

    .line 151
    :sswitch_13
    const v0, 0x7f0900d5

    .line 152
    goto :goto_0

    .line 154
    :sswitch_14
    const v0, 0x7f0900d6

    .line 155
    goto :goto_0

    .line 157
    :sswitch_15
    const v0, 0x7f0900d7

    .line 158
    goto :goto_0

    .line 160
    :sswitch_16
    const v0, 0x7f0900d8

    .line 161
    goto :goto_0

    .line 163
    :sswitch_17
    const v0, 0x7f0900d9

    .line 164
    goto :goto_0

    .line 166
    :sswitch_18
    const v0, 0x7f0900da

    .line 167
    goto :goto_0

    .line 169
    :sswitch_19
    const v0, 0x7f0900db

    .line 170
    goto :goto_0

    .line 172
    :sswitch_1a
    const v0, 0x7f0900dc

    .line 173
    goto :goto_0

    .line 175
    :sswitch_1b
    const v0, 0x7f0900dd

    .line 176
    goto :goto_0

    .line 178
    :sswitch_1c
    const v0, 0x7f0900de

    .line 179
    goto :goto_0

    .line 181
    :sswitch_1d
    const v0, 0x7f0900df

    .line 182
    goto :goto_0

    .line 184
    :sswitch_1e
    const v0, 0x7f0900e0

    .line 185
    goto :goto_0

    .line 187
    :sswitch_1f
    const v0, 0x7f0900e1

    .line 188
    goto :goto_0

    .line 190
    :sswitch_20
    const v0, 0x7f0900e2

    .line 191
    goto/16 :goto_0

    .line 193
    :sswitch_21
    const v0, 0x7f0900e3

    .line 194
    goto/16 :goto_0

    .line 196
    :sswitch_22
    const v0, 0x7f0900e4

    .line 197
    goto/16 :goto_0

    .line 199
    :sswitch_23
    const v0, 0x7f0900e5

    .line 200
    goto/16 :goto_0

    .line 202
    :sswitch_24
    const v0, 0x7f0900e6

    .line 203
    goto/16 :goto_0

    .line 205
    :sswitch_25
    const v0, 0x7f0900e7

    .line 206
    goto/16 :goto_0

    .line 208
    :sswitch_26
    const v0, 0x7f0900e8

    .line 209
    goto/16 :goto_0

    .line 211
    :sswitch_27
    const v0, 0x7f0900e9

    .line 212
    goto/16 :goto_0

    .line 214
    :sswitch_28
    const v0, 0x7f0900ea

    .line 215
    goto/16 :goto_0

    .line 217
    :sswitch_29
    const v0, 0x7f0900eb

    .line 218
    goto/16 :goto_0

    .line 220
    :sswitch_2a
    const v0, 0x7f0900ec

    .line 221
    goto/16 :goto_0

    .line 223
    :sswitch_2b
    const v0, 0x7f0900ed

    .line 224
    goto/16 :goto_0

    .line 226
    :sswitch_2c
    const v0, 0x7f0900ee

    .line 227
    goto/16 :goto_0

    .line 229
    :sswitch_2d
    const v0, 0x7f0900ef

    .line 230
    goto/16 :goto_0

    .line 232
    :sswitch_2e
    const v0, 0x7f0900f0

    .line 233
    goto/16 :goto_0

    .line 235
    :sswitch_2f
    const v0, 0x7f0900f1

    .line 236
    goto/16 :goto_0

    .line 238
    :sswitch_30
    const v0, 0x7f0900f2

    .line 239
    goto/16 :goto_0

    .line 241
    :sswitch_31
    const v0, 0x7f0900f3

    .line 242
    goto/16 :goto_0

    .line 244
    :sswitch_32
    const v0, 0x7f0900f4

    .line 245
    goto/16 :goto_0

    .line 247
    :sswitch_33
    const v0, 0x7f0900f5

    .line 248
    goto/16 :goto_0

    .line 250
    :sswitch_34
    const v0, 0x7f0900f6

    .line 251
    goto/16 :goto_0

    .line 253
    :sswitch_35
    const v0, 0x7f0900f7

    .line 254
    goto/16 :goto_0

    .line 256
    :sswitch_36
    const v0, 0x7f0900f8

    .line 257
    goto/16 :goto_0

    .line 259
    :sswitch_37
    const v0, 0x7f0900f9

    .line 260
    goto/16 :goto_0

    .line 262
    :sswitch_38
    const v0, 0x7f0900fa

    .line 263
    goto/16 :goto_0

    .line 265
    :sswitch_39
    const v0, 0x7f0900fb

    .line 266
    goto/16 :goto_0

    .line 268
    :sswitch_3a
    const v0, 0x7f0900fc

    .line 269
    goto/16 :goto_0

    .line 271
    :sswitch_3b
    const v0, 0x7f0900fd

    .line 272
    goto/16 :goto_0

    .line 274
    :sswitch_3c
    const v0, 0x7f0900fe

    goto/16 :goto_0

    .line 92
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0xc9 -> :sswitch_1
        0xca -> :sswitch_2
        0xcb -> :sswitch_3
        0xcc -> :sswitch_4
        0xcd -> :sswitch_5
        0xce -> :sswitch_6
        0xcf -> :sswitch_7
        0xd0 -> :sswitch_8
        0xe2 -> :sswitch_9
        0x12c -> :sswitch_a
        0x12d -> :sswitch_b
        0x12e -> :sswitch_c
        0x12f -> :sswitch_d
        0x130 -> :sswitch_e
        0x131 -> :sswitch_f
        0x132 -> :sswitch_10
        0x133 -> :sswitch_11
        0x134 -> :sswitch_12
        0x190 -> :sswitch_13
        0x191 -> :sswitch_14
        0x192 -> :sswitch_15
        0x193 -> :sswitch_16
        0x194 -> :sswitch_17
        0x195 -> :sswitch_18
        0x196 -> :sswitch_19
        0x197 -> :sswitch_1a
        0x198 -> :sswitch_1b
        0x199 -> :sswitch_1c
        0x19a -> :sswitch_1d
        0x19b -> :sswitch_1e
        0x19c -> :sswitch_1f
        0x19d -> :sswitch_20
        0x19e -> :sswitch_21
        0x19f -> :sswitch_22
        0x1a0 -> :sswitch_23
        0x1a1 -> :sswitch_24
        0x1a5 -> :sswitch_25
        0x1a6 -> :sswitch_26
        0x1a7 -> :sswitch_27
        0x1a8 -> :sswitch_28
        0x1aa -> :sswitch_29
        0x1ac -> :sswitch_2a
        0x1ad -> :sswitch_2b
        0x1af -> :sswitch_2c
        0x1c3 -> :sswitch_2d
        0x1f4 -> :sswitch_2e
        0x1f5 -> :sswitch_2f
        0x1f6 -> :sswitch_30
        0x1f7 -> :sswitch_31
        0x1f8 -> :sswitch_32
        0x1f9 -> :sswitch_33
        0x1fa -> :sswitch_34
        0x1fb -> :sswitch_35
        0x1fc -> :sswitch_36
        0x1fe -> :sswitch_37
        0x1ff -> :sswitch_38
        0x384 -> :sswitch_39
        0x385 -> :sswitch_3a
        0x388 -> :sswitch_3b
        0x3a1 -> :sswitch_3c
    .end sparse-switch
.end method
