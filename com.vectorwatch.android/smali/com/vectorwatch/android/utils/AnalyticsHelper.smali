.class public Lcom/vectorwatch/android/utils/AnalyticsHelper;
.super Ljava/lang/Object;
.source "AnalyticsHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;,
        Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;,
        Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vectorwatch/android/utils/AnalyticsHelper;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    return-void
.end method

.method public static logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;
    .param p2, "action"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;
    .param p3, "label"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    .prologue
    .line 46
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;J)V

    .line 47
    return-void
.end method

.method public static logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;
    .param p2, "action"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;
    .param p3, "label"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;
    .param p4, "value"    # J

    .prologue
    .line 83
    if-nez p0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 87
    :cond_0
    if-eqz p3, :cond_1

    .line 88
    invoke-virtual {p3}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logGoogleAnalyticsEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto :goto_0

    .line 90
    :cond_1
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logGoogleAnalyticsEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public static logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;
    .param p2, "action"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;
    .param p3, "label"    # Ljava/lang/String;

    .prologue
    .line 33
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    .line 34
    return-void
.end method

.method public static logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;
    .param p2, "action"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "value"    # J

    .prologue
    .line 61
    if-nez p0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 65
    :cond_0
    if-eqz p3, :cond_1

    .line 66
    invoke-static/range {p0 .. p5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logGoogleAnalyticsEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto :goto_0

    .line 68
    :cond_1
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logGoogleAnalyticsEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private static logGoogleAnalyticsEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;
    .param p2, "action"    # Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "value"    # J

    .prologue
    .line 106
    const-string v3, "Analytics"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "log event "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    if-nez p0, :cond_0

    .line 109
    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper;->log:Lorg/slf4j/Logger;

    const-string v4, "logGoogleAnalyticsEvent: Context is null!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 149
    :goto_0
    return-void

    .line 113
    :cond_0
    if-nez p1, :cond_1

    .line 114
    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper;->log:Lorg/slf4j/Logger;

    const-string v4, "logGoogleAnalyticsEvent: Category is null!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_1
    if-nez p2, :cond_2

    .line 119
    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper;->log:Lorg/slf4j/Logger;

    const-string v4, "logGoogleAnalyticsEvent: Label is null!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/VectorApplication;

    .line 126
    .local v0, "app":Lcom/vectorwatch/android/VectorApplication;
    if-nez v0, :cond_3

    .line 127
    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper;->log:Lorg/slf4j/Logger;

    const-string v4, "logGoogleAnalyticsEvent: app is null!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_3
    sget-object v3, Lcom/vectorwatch/android/VectorApplication$TrackerName;->ANDROID_APP_TRACKER:Lcom/vectorwatch/android/VectorApplication$TrackerName;

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/VectorApplication;->getTracker(Lcom/vectorwatch/android/VectorApplication$TrackerName;)Lcom/google/android/gms/analytics/Tracker;

    move-result-object v2

    .line 133
    .local v2, "t":Lcom/google/android/gms/analytics/Tracker;
    if-nez v2, :cond_4

    .line 134
    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper;->log:Lorg/slf4j/Logger;

    const-string v4, "logGoogleAnalyticsEvent: Tracker is null!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :cond_4
    new-instance v3, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v3}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    .line 139
    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCategory(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v3

    .line 140
    invoke-virtual {p2}, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setAction(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v3

    .line 141
    invoke-virtual {v3, p4, p5}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setValue(J)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v1

    .line 143
    .local v1, "eventBuilder":Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
    if-eqz p3, :cond_5

    .line 144
    invoke-virtual {p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setLabel(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    .line 148
    :cond_5
    invoke-virtual {v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    goto :goto_0
.end method
