.class public Lcom/vectorwatch/android/utils/DateTimeUtils;
.super Ljava/lang/Object;
.source "DateTimeUtils.java"


# static fields
.field private static final VECTOR_TIME_DIFFERENCE_CONST:I = 0x386d4380


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentTimeInSeconds()J
    .locals 4

    .prologue
    .line 56
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 57
    .local v0, "crtTime":J
    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    return-wide v2
.end method

.method public static getDstInfo(J)Lcom/vectorwatch/android/models/DstInfoModel;
    .locals 14
    .param p0, "crtTime"    # J

    .prologue
    const-wide/16 v12, 0x1

    .line 67
    const-wide/16 v10, 0x3e8

    mul-long/2addr p0, v10

    .line 69
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    .line 72
    .local v8, "tz":Ljava/util/TimeZone;
    invoke-virtual {v8}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v10

    const-string v11, "Europe/Astrakhan"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 73
    const-string v10, "Europe/Samara"

    invoke-static {v10}, Lorg/joda/time/DateTimeZone;->forID(Ljava/lang/String;)Lorg/joda/time/DateTimeZone;

    move-result-object v9

    .line 78
    .local v9, "zone":Lorg/joda/time/DateTimeZone;
    :goto_0
    if-nez v9, :cond_1

    .line 79
    const/4 v0, 0x0

    .line 104
    :goto_1
    return-object v0

    .line 75
    .end local v9    # "zone":Lorg/joda/time/DateTimeZone;
    :cond_0
    invoke-virtual {v8}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lorg/joda/time/DateTimeZone;->forID(Ljava/lang/String;)Lorg/joda/time/DateTimeZone;

    move-result-object v9

    .restart local v9    # "zone":Lorg/joda/time/DateTimeZone;
    goto :goto_0

    .line 82
    :cond_1
    invoke-virtual {v9, p0, p1}, Lorg/joda/time/DateTimeZone;->nextTransition(J)J

    move-result-wide v4

    .line 84
    .local v4, "nextDstTime":J
    new-instance v0, Lcom/vectorwatch/android/models/DstInfoModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/DstInfoModel;-><init>()V

    .line 87
    .local v0, "dstInfo":Lcom/vectorwatch/android/models/DstInfoModel;
    invoke-virtual {v9, v4, v5}, Lorg/joda/time/DateTimeZone;->isStandardOffset(J)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 89
    const-wide/16 v6, 0x0

    .line 90
    .local v6, "start":J
    sub-long v10, v4, v12

    invoke-virtual {v8, v10, v11}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v10

    int-to-long v10, v10

    add-long v2, v4, v10

    .line 98
    .local v2, "end":J
    :goto_2
    invoke-virtual {v0, v6, v7}, Lcom/vectorwatch/android/models/DstInfoModel;->setStart(J)V

    .line 99
    invoke-virtual {v0, v2, v3}, Lcom/vectorwatch/android/models/DstInfoModel;->setEnd(J)V

    .line 101
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    .line 102
    .local v1, "timeZone":Ljava/util/TimeZone;
    invoke-virtual {v1}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v0, v10, v11}, Lcom/vectorwatch/android/models/DstInfoModel;->setDstOffset(J)V

    goto :goto_1

    .line 93
    .end local v1    # "timeZone":Ljava/util/TimeZone;
    .end local v2    # "end":J
    .end local v6    # "start":J
    :cond_2
    sub-long v10, v4, v12

    invoke-virtual {v8, v10, v11}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v10

    int-to-long v10, v10

    add-long v6, v4, v10

    .line 95
    .restart local v6    # "start":J
    invoke-virtual {v9, v4, v5}, Lorg/joda/time/DateTimeZone;->nextTransition(J)J

    move-result-wide v4

    .line 96
    sub-long v10, v4, v12

    invoke-virtual {v8, v10, v11}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v10

    int-to-long v10, v10

    add-long v2, v4, v10

    .restart local v2    # "end":J
    goto :goto_2
.end method

.method public static getUnixTimeFromVectorTime(I)I
    .locals 1
    .param p0, "vectorTimeSecs"    # I

    .prologue
    .line 34
    const v0, 0x386d4380

    add-int/2addr v0, p0

    return v0
.end method

.method public static getUserTimezone()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "Z"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVectorDecimalFormat()Ljava/text/DecimalFormat;
    .locals 2

    .prologue
    .line 38
    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    .line 39
    .local v0, "format":Ljava/text/DecimalFormat;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setGroupingUsed(Z)V

    .line 40
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setGroupingSize(I)V

    .line 41
    return-object v0
.end method

.method public static getVectorDecimalFormat(Ljava/util/Locale;)Ljava/text/DecimalFormat;
    .locals 2
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 45
    invoke-static {p0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    .line 46
    .local v0, "format":Ljava/text/DecimalFormat;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setGroupingUsed(Z)V

    .line 47
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setGroupingSize(I)V

    .line 48
    return-object v0
.end method

.method public static getVectorTimeFromUnixTime(I)I
    .locals 1
    .param p0, "unixTimeSecs"    # I

    .prologue
    .line 29
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    const/4 v0, -0x2

    if-eq p0, v0, :cond_0

    if-nez p0, :cond_1

    .line 30
    .end local p0    # "unixTimeSecs":I
    :cond_0
    :goto_0
    return p0

    .restart local p0    # "unixTimeSecs":I
    :cond_1
    const v0, 0x386d4380

    sub-int/2addr p0, v0

    goto :goto_0
.end method
