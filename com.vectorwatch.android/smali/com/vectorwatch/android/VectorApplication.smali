.class public Lcom/vectorwatch/android/VectorApplication;
.super Landroid/support/multidex/MultiDexApplication;
.source "VectorApplication.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/VectorApplication$TrackerName;
    }
.end annotation


# static fields
.field private static final DATABASE_VERSION:I = 0x4

.field private static final KEEN_PROJECT_ID:Ljava/lang/String; = "557c180f96773d2313f96bf2"

.field private static final KEEN_READ_KEY:Ljava/lang/String; = "f3d5d4db8ab1dce0823247d517f51a7cf9c593b9660d582cdff0871f229de9597e45a8c0564807b1742a913d906fbe623301700f991c24d2c8d0ab6b8b00cdca01f6b1ebef9edf860e51b5e2603c6e3b9ec221888b63b7e66551588da1b1f1b9556e3fa19d831b25da91dd60dc90779c"

.field private static final KEEN_WRITE_KEY:Ljava/lang/String; = "a6c50e2d4b241738bfaa03437500413bb0f6aab1e2a5558c9319d6700ce98708a941d3557638d5d8ffdccce7a3f7d20fbe47e0375677d91a9204a77c8d3d3045216458d5ac6889af75368c0afcb416779a2b92ee43e06cbe6d4f59105cdd89916975fb03f4bb4a8e35991cfdcebc2737"

.field private static complexPreferences:Lcom/vectorwatch/android/utils/ComplexPreferences;

.field private static eventBus:Lcom/vectorwatch/android/MainThreadBus;

.field public static hasError:Z

.field private static final log:Lorg/slf4j/Logger;

.field public static mHandleGmailNotif:Z

.field public static mHandlerCalendar:Landroid/os/Handler;

.field public static mHandlerGK:Landroid/os/Handler;

.field public static mHandlerGMAIL:Landroid/os/Handler;

.field public static mHandlerKKO:Landroid/os/Handler;

.field public static mHandlerMail:Landroid/os/Handler;

.field public static mHandlerSkype:Landroid/os/Handler;

.field public static mHandlerSlack:Landroid/os/Handler;

.field public static mHandlerWA:Landroid/os/Handler;

.field public static mLastWhatsAppMessage:Ljava/lang/String;

.field public static mRunnableCalendar:Ljava/lang/Runnable;

.field public static mRunnableGK:Ljava/lang/Runnable;

.field public static mRunnableGMAIL:Ljava/lang/Runnable;

.field public static mRunnableKKO:Ljava/lang/Runnable;

.field public static mRunnableMail:Ljava/lang/Runnable;

.field public static mRunnableSkype:Ljava/lang/Runnable;

.field public static mRunnableSlack:Ljava/lang/Runnable;

.field public static mRunnableWA:Ljava/lang/Runnable;

.field private static remotePushNotificaiton:Lcom/vectorwatch/android/RemotePushNotificationsInitializer;

.field private static sAppContext:Landroid/content/Context;

.field private static sAppInstallManagerInstance:Lcom/vectorwatch/android/utils/AppInstallManager;

.field public static sAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static sCorrectlyInstalledApps:I

.field public static sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field public static sInstalledCount:I

.field public static sIsIncomingCallActive:Z

.field public static sLastArtist:Ljava/lang/String;

.field public static sLastCalendarMessage:Ljava/lang/String;

.field public static sLastGKMessage:Ljava/lang/String;

.field public static sLastGMAILMessage:Ljava/lang/String;

.field public static sLastKKOMessage:Ljava/lang/String;

.field public static sLastMAILMessage:Ljava/lang/String;

.field public static sLastSLACKMessage:Ljava/lang/String;

.field public static sLastSkypeMessage:Ljava/lang/String;

.field public static sLastTrack:Ljava/lang/String;

.field private static sLiveStreamManager:Lcom/vectorwatch/android/managers/LiveStreamManager;

.field public static sSetupWatchActive:Z

.field public static sShowStreamDropped:Z

.field private static sSocialMediaManager:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

.field public static sStoreCount:I

.field public static sTotalInstalledApps:I

.field private static sTransferManager:Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;


# instance fields
.field private alarm:Lcom/vectorwatch/android/scheduler/BatteryAlarm;

.field private mTrackers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/vectorwatch/android/VectorApplication$TrackerName;",
            "Lcom/google/android/gms/analytics/Tracker;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    const-class v0, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/VectorApplication;->log:Lorg/slf4j/Logger;

    .line 74
    const/4 v0, 0x1

    sput-boolean v0, Lcom/vectorwatch/android/VectorApplication;->mHandleGmailNotif:Z

    .line 84
    sput-boolean v1, Lcom/vectorwatch/android/VectorApplication;->sIsIncomingCallActive:Z

    .line 88
    sput v1, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    .line 91
    sput v1, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    .line 92
    sput-boolean v1, Lcom/vectorwatch/android/VectorApplication;->hasError:Z

    .line 101
    sput v1, Lcom/vectorwatch/android/VectorApplication;->sStoreCount:I

    .line 103
    const/4 v0, 0x0

    sput-object v0, Lcom/vectorwatch/android/VectorApplication;->sFitnessClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/support/multidex/MultiDexApplication;-><init>()V

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/VectorApplication;->mTrackers:Ljava/util/HashMap;

    .line 388
    return-void
.end method

.method private checkIfAppNewlyUpdated()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 395
    const-string v2, "current_app_version"

    const/4 v3, 0x0

    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 398
    .local v1, "lastKnownAppVersion":Ljava/lang/String;
    const-string v2, "2.0.2"

    if-eqz v2, :cond_2

    const-string v2, "2.0.2"

    const-string v3, "-d"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 400
    const-string v0, "2.0.2.b5957a0"

    .line 405
    .local v0, "crtAppVersion":Ljava/lang/String;
    :goto_0
    const-string v2, "App"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SETUP: Crt app version | last known app version | CHECK COMPAT FLAG: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mandatory_app_update"

    .line 406
    invoke-static {v4, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 405
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 410
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v2

    const-string v3, "last_known_system_info"

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/utils/ComplexPreferences;->remove(Ljava/lang/String;)V

    .line 412
    const-string v2, "mandatory_app_update"

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 416
    const-string v2, "MIGRATION: "

    const-string v3, "Needs to check for migration actions"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    invoke-virtual {p0}, Lcom/vectorwatch/android/VectorApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/vectorwatch/android/utils/MigrationHelper;->migrateApp(Ljava/lang/String;Landroid/content/Context;)V

    .line 419
    const-string v2, "COMPATIBILITY: "

    const-string v3, "FLAG COMPAT true at INSTALL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    const-string v2, "check_compatibility"

    invoke-static {v2, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 425
    const-string v2, "flag_send_parse_installation_id"

    invoke-static {v2, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 430
    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/vectorwatch/android/VectorApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->setUpdateModeState(ILandroid/content/Context;)V

    .line 432
    const-string v2, "flag_contextual_was_done"

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 434
    const-string v2, "flag_sync_settings_to_cloud"

    invoke-static {v2, v6, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 436
    const-string v2, "flag_contextual_was_done"

    invoke-static {v2, v5, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 441
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->resetLocaleFlag(Landroid/content/Context;)V

    .line 443
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->updateApiUri(Landroid/content/Context;)V

    .line 447
    :cond_1
    const-string v2, "current_app_version"

    invoke-static {v2, v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 448
    const-string v2, "App"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Changed crt app to - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    return-void

    .line 402
    .end local v0    # "crtAppVersion":Ljava/lang/String;
    :cond_2
    const-string v0, "2.0.2"

    .restart local v0    # "crtAppVersion":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method public static getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 318
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppContext:Landroid/content/Context;

    return-object v0
.end method

.method public static declared-synchronized getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;
    .locals 3

    .prologue
    .line 291
    const-class v1, Lcom/vectorwatch/android/VectorApplication;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppInstallManagerInstance:Lcom/vectorwatch/android/utils/AppInstallManager;

    if-nez v0, :cond_0

    .line 292
    new-instance v0, Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-direct {v0}, Lcom/vectorwatch/android/utils/AppInstallManager;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppInstallManagerInstance:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 293
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppInstallManagerInstance:Lcom/vectorwatch/android/utils/AppInstallManager;

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sAppContext:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->setApplicationContext(Landroid/content/Context;)Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v0

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->setEventBus(Lcom/vectorwatch/android/MainThreadBus;)Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 294
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    sget-object v2, Lcom/vectorwatch/android/VectorApplication;->sAppInstallManagerInstance:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 297
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppInstallManagerInstance:Lcom/vectorwatch/android/utils/AppInstallManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getEventBus()Lcom/vectorwatch/android/MainThreadBus;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->eventBus:Lcom/vectorwatch/android/MainThreadBus;

    return-object v0
.end method

.method public static getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;
    .locals 2

    .prologue
    .line 340
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sLiveStreamManager:Lcom/vectorwatch/android/managers/LiveStreamManager;

    if-nez v0, :cond_0

    .line 341
    new-instance v0, Lcom/vectorwatch/android/managers/LiveStreamManager;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/managers/LiveStreamManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vectorwatch/android/VectorApplication;->sLiveStreamManager:Lcom/vectorwatch/android/managers/LiveStreamManager;

    .line 344
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sLiveStreamManager:Lcom/vectorwatch/android/managers/LiveStreamManager;

    return-object v0
.end method

.method public static getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;
    .locals 3

    .prologue
    .line 322
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->complexPreferences:Lcom/vectorwatch/android/utils/ComplexPreferences;

    if-nez v0, :cond_0

    .line 323
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppContext:Landroid/content/Context;

    const-string v1, "vector_preferences"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getComplexPreferences(Landroid/content/Context;Ljava/lang/String;I)Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/VectorApplication;->complexPreferences:Lcom/vectorwatch/android/utils/ComplexPreferences;

    .line 327
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->complexPreferences:Lcom/vectorwatch/android/utils/ComplexPreferences;

    return-object v0
.end method

.method public static getRemotePushNotificationsInitializer()Lcom/vectorwatch/android/RemotePushNotificationsInitializer;
    .locals 1

    .prologue
    .line 482
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->remotePushNotificaiton:Lcom/vectorwatch/android/RemotePushNotificationsInitializer;

    if-nez v0, :cond_0

    .line 483
    new-instance v0, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;

    invoke-direct {v0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/VectorApplication;->remotePushNotificaiton:Lcom/vectorwatch/android/RemotePushNotificationsInitializer;

    .line 486
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->remotePushNotificaiton:Lcom/vectorwatch/android/RemotePushNotificationsInitializer;

    return-object v0
.end method

.method public static getSocialMediaManager()Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;
    .locals 2

    .prologue
    .line 361
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sSocialMediaManager:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    if-nez v0, :cond_0

    .line 362
    new-instance v0, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/vectorwatch/android/VectorApplication;->sSocialMediaManager:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    .line 365
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sSocialMediaManager:Lcom/vectorwatch/android/managers/incoming_data_handlers/SocialMediaManager;

    return-object v0
.end method

.method public static getTransferManager()Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;
    .locals 1

    .prologue
    .line 301
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sTransferManager:Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    return-object v0
.end method

.method public static isStreamLiveManagerInitialized()Z
    .locals 1

    .prologue
    .line 353
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sLiveStreamManager:Lcom/vectorwatch/android/managers/LiveStreamManager;

    if-nez v0, :cond_0

    .line 354
    const/4 v0, 0x0

    .line 356
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getTracker(Lcom/vectorwatch/android/VectorApplication$TrackerName;)Lcom/google/android/gms/analytics/Tracker;
    .locals 3
    .param p1, "trackerId"    # Lcom/vectorwatch/android/VectorApplication$TrackerName;

    .prologue
    .line 369
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/vectorwatch/android/VectorApplication;->mTrackers:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 371
    invoke-static {p0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v0

    .line 372
    .local v0, "analytics":Lcom/google/android/gms/analytics/GoogleAnalytics;
    sget-object v2, Lcom/vectorwatch/android/VectorApplication$TrackerName;->ANDROID_APP_TRACKER:Lcom/vectorwatch/android/VectorApplication$TrackerName;

    if-ne p1, v2, :cond_1

    const v2, 0x7f060001

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/GoogleAnalytics;->newTracker(I)Lcom/google/android/gms/analytics/Tracker;

    move-result-object v1

    .line 375
    .local v1, "t":Lcom/google/android/gms/analytics/Tracker;
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/VectorApplication;->mTrackers:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    .end local v0    # "analytics":Lcom/google/android/gms/analytics/GoogleAnalytics;
    .end local v1    # "t":Lcom/google/android/gms/analytics/Tracker;
    :cond_0
    iget-object v2, p0, Lcom/vectorwatch/android/VectorApplication;->mTrackers:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/analytics/Tracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v2

    .line 372
    .restart local v0    # "analytics":Lcom/google/android/gms/analytics/GoogleAnalytics;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 369
    .end local v0    # "analytics":Lcom/google/android/gms/analytics/GoogleAnalytics;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getUpdateReceiverInstance()Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/vectorwatch/android/VectorApplication;->mUpdateReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    if-nez v0, :cond_0

    .line 332
    new-instance v0, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/vectorwatch/android/VectorApplication;->mUpdateReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    .line 333
    iget-object v0, p0, Lcom/vectorwatch/android/VectorApplication;->mUpdateReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    invoke-virtual {v0, p0}, Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;->setContext(Landroid/content/Context;)V

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/VectorApplication;->mUpdateReceiver:Lcom/vectorwatch/com/android/vos/update/UpdateResultReceiver;

    return-object v0
.end method

.method initBatteryBroadcastReceiver(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 460
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 461
    .local v7, "calendar":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 464
    new-instance v9, Landroid/content/Intent;

    const-string v1, "com.vectorwatch.intent.CUSTOM_ACTION_BATTERY_CHANGED"

    invoke-direct {v9, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 465
    .local v9, "intent":Landroid/content/Intent;
    invoke-static {p1, v4, v9, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 466
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    const-string v1, "alarm"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 467
    .local v0, "manager":Landroid/app/AlarmManager;
    const/4 v1, 0x1

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/32 v4, 0xdbba0

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 469
    sget-object v1, Lcom/vectorwatch/android/VectorApplication;->log:Lorg/slf4j/Logger;

    const-string v2, "[BATTERY ALARM]: Registering pending intent"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 471
    new-instance v1, Lcom/vectorwatch/android/scheduler/BatteryAlarm;

    invoke-direct {v1}, Lcom/vectorwatch/android/scheduler/BatteryAlarm;-><init>()V

    iput-object v1, p0, Lcom/vectorwatch/android/VectorApplication;->alarm:Lcom/vectorwatch/android/scheduler/BatteryAlarm;

    .line 472
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    .line 473
    .local v8, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.vectorwatch.intent.CUSTOM_ACTION_BATTERY_CHANGED"

    invoke-virtual {v8, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 474
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v8, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 475
    const-string v1, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v8, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 476
    const-string v1, "android.intent.action.BATTERY_OKAY"

    invoke-virtual {v8, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 477
    iget-object v1, p0, Lcom/vectorwatch/android/VectorApplication;->alarm:Lcom/vectorwatch/android/scheduler/BatteryAlarm;

    invoke-virtual {p1, v1, v8}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 478
    sget-object v1, Lcom/vectorwatch/android/VectorApplication;->log:Lorg/slf4j/Logger;

    const-string v2, "[BATTERY ALARM]: Registering battery broadcast receiver"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 479
    return-void
.end method

.method public onCreate()V
    .locals 8

    .prologue
    .line 119
    invoke-super {p0}, Landroid/support/multidex/MultiDexApplication;->onCreate()V

    .line 120
    sput-object p0, Lcom/vectorwatch/android/VectorApplication;->sAppContext:Landroid/content/Context;

    .line 122
    const/4 v5, 0x1

    new-array v5, v5, [Lio/fabric/sdk/android/Kit;

    const/4 v6, 0x0

    new-instance v7, Lcom/crashlytics/android/Crashlytics;

    invoke-direct {v7}, Lcom/crashlytics/android/Crashlytics;-><init>()V

    aput-object v7, v5, v6

    invoke-static {p0, v5}, Lio/fabric/sdk/android/Fabric;->with(Landroid/content/Context;[Lio/fabric/sdk/android/Kit;)Lio/fabric/sdk/android/Fabric;

    .line 124
    invoke-virtual {p0}, Lcom/vectorwatch/android/VectorApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/FacebookSdk;->sdkInitialize(Landroid/content/Context;)V

    .line 125
    invoke-static {p0}, Lcom/facebook/appevents/AppEventsLogger;->activateApp(Landroid/content/Context;)V

    .line 127
    invoke-static {p0}, Lnet/danlew/android/joda/JodaTimeAndroid;->init(Landroid/content/Context;)V

    .line 130
    new-instance v5, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v5, p0}, Lio/realm/RealmConfiguration$Builder;-><init>(Landroid/content/Context;)V

    const-wide/16 v6, 0x4

    .line 131
    invoke-virtual {v5, v6, v7}, Lio/realm/RealmConfiguration$Builder;->schemaVersion(J)Lio/realm/RealmConfiguration$Builder;

    move-result-object v5

    new-instance v6, Lcom/vectorwatch/android/models/DatabaseMigration;

    invoke-direct {v6}, Lcom/vectorwatch/android/models/DatabaseMigration;-><init>()V

    .line 132
    invoke-virtual {v5, v6}, Lio/realm/RealmConfiguration$Builder;->migration(Lio/realm/RealmMigration;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v5

    .line 133
    invoke-virtual {v5}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v4

    .line 135
    .local v4, "realmConfiguration":Lio/realm/RealmConfiguration;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getRemotePushNotificationsInitializer()Lcom/vectorwatch/android/RemotePushNotificationsInitializer;

    move-result-object v5

    invoke-virtual {p0}, Lcom/vectorwatch/android/VectorApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->registerToRemotePushNotificationSystem(Landroid/content/Context;)V

    .line 138
    :try_start_0
    new-instance v5, Lcom/vectorwatch/android/models/DatabaseMigration;

    invoke-direct {v5}, Lcom/vectorwatch/android/models/DatabaseMigration;-><init>()V

    invoke-static {v4, v5}, Lio/realm/Realm;->migrateRealm(Lio/realm/RealmConfiguration;Lio/realm/RealmMigration;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 145
    :goto_0
    invoke-static {v4}, Lio/realm/Realm;->setDefaultConfiguration(Lio/realm/RealmConfiguration;)V

    .line 148
    :try_start_1
    invoke-static {v4}, Lio/realm/Realm;->compactRealm(Lio/realm/RealmConfiguration;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 153
    :goto_1
    const v5, 0x7f0901b3

    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/VectorApplication;->getString(I)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->sLastArtist:Ljava/lang/String;

    .line 155
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    .line 157
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mHandlerGMAIL:Landroid/os/Handler;

    .line 158
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mHandlerWA:Landroid/os/Handler;

    .line 159
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mHandlerSkype:Landroid/os/Handler;

    .line 160
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mHandlerGK:Landroid/os/Handler;

    .line 161
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mHandlerKKO:Landroid/os/Handler;

    .line 162
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mHandlerMail:Landroid/os/Handler;

    .line 163
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mHandlerSlack:Landroid/os/Handler;

    .line 164
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mHandlerCalendar:Landroid/os/Handler;

    .line 165
    new-instance v5, Lcom/vectorwatch/android/VectorApplication$1;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/VectorApplication$1;-><init>(Lcom/vectorwatch/android/VectorApplication;)V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mRunnableGMAIL:Ljava/lang/Runnable;

    .line 171
    new-instance v5, Lcom/vectorwatch/android/VectorApplication$2;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/VectorApplication$2;-><init>(Lcom/vectorwatch/android/VectorApplication;)V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mRunnableWA:Ljava/lang/Runnable;

    .line 177
    new-instance v5, Lcom/vectorwatch/android/VectorApplication$3;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/VectorApplication$3;-><init>(Lcom/vectorwatch/android/VectorApplication;)V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mRunnableSkype:Ljava/lang/Runnable;

    .line 183
    new-instance v5, Lcom/vectorwatch/android/VectorApplication$4;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/VectorApplication$4;-><init>(Lcom/vectorwatch/android/VectorApplication;)V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mRunnableGK:Ljava/lang/Runnable;

    .line 189
    new-instance v5, Lcom/vectorwatch/android/VectorApplication$5;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/VectorApplication$5;-><init>(Lcom/vectorwatch/android/VectorApplication;)V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mRunnableKKO:Ljava/lang/Runnable;

    .line 195
    new-instance v5, Lcom/vectorwatch/android/VectorApplication$6;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/VectorApplication$6;-><init>(Lcom/vectorwatch/android/VectorApplication;)V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mRunnableMail:Ljava/lang/Runnable;

    .line 201
    new-instance v5, Lcom/vectorwatch/android/VectorApplication$7;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/VectorApplication$7;-><init>(Lcom/vectorwatch/android/VectorApplication;)V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mRunnableSlack:Ljava/lang/Runnable;

    .line 207
    new-instance v5, Lcom/vectorwatch/android/VectorApplication$8;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/VectorApplication$8;-><init>(Lcom/vectorwatch/android/VectorApplication;)V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->mRunnableCalendar:Ljava/lang/Runnable;

    .line 216
    new-instance v5, Lcom/vectorwatch/android/MainThreadBus;

    invoke-direct {v5}, Lcom/vectorwatch/android/MainThreadBus;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->eventBus:Lcom/vectorwatch/android/MainThreadBus;

    .line 217
    new-instance v5, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;

    invoke-direct {v5}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManagerImpl;-><init>()V

    sput-object v5, Lcom/vectorwatch/android/VectorApplication;->sTransferManager:Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    .line 218
    sget-object v5, Lcom/vectorwatch/android/VectorApplication;->sTransferManager:Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    invoke-virtual {p0}, Lcom/vectorwatch/android/VectorApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->setApplicationContext(Landroid/content/Context;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    move-result-object v5

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;->setEventBus(Lcom/vectorwatch/android/MainThreadBus;)Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    .line 219
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/VectorApplication;->sTransferManager:Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 222
    new-instance v5, Lio/keen/client/android/AndroidKeenClientBuilder;

    invoke-direct {v5, p0}, Lio/keen/client/android/AndroidKeenClientBuilder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5}, Lio/keen/client/android/AndroidKeenClientBuilder;->build()Lio/keen/client/java/KeenClient;

    move-result-object v0

    .line 223
    .local v0, "client":Lio/keen/client/java/KeenClient;
    new-instance v3, Lio/keen/client/java/KeenProject;

    const-string v5, "557c180f96773d2313f96bf2"

    const-string v6, "a6c50e2d4b241738bfaa03437500413bb0f6aab1e2a5558c9319d6700ce98708a941d3557638d5d8ffdccce7a3f7d20fbe47e0375677d91a9204a77c8d3d3045216458d5ac6889af75368c0afcb416779a2b92ee43e06cbe6d4f59105cdd89916975fb03f4bb4a8e35991cfdcebc2737"

    const-string v7, "f3d5d4db8ab1dce0823247d517f51a7cf9c593b9660d582cdff0871f229de9597e45a8c0564807b1742a913d906fbe623301700f991c24d2c8d0ab6b8b00cdca01f6b1ebef9edf860e51b5e2603c6e3b9ec221888b63b7e66551588da1b1f1b9556e3fa19d831b25da91dd60dc90779c"

    invoke-direct {v3, v5, v6, v7}, Lio/keen/client/java/KeenProject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .local v3, "project":Lio/keen/client/java/KeenProject;
    invoke-virtual {v0, v3}, Lio/keen/client/java/KeenClient;->setDefaultProject(Lio/keen/client/java/KeenProject;)V

    .line 226
    invoke-static {v0}, Lio/keen/client/java/KeenClient;->initialize(Lio/keen/client/java/KeenClient;)V

    .line 228
    new-instance v5, Luk/co/chrisjenx/calligraphy/CalligraphyConfig$Builder;

    invoke-direct {v5}, Luk/co/chrisjenx/calligraphy/CalligraphyConfig$Builder;-><init>()V

    const-string v6, "fonts/Reader-Regular.otf"

    .line 229
    invoke-virtual {v5, v6}, Luk/co/chrisjenx/calligraphy/CalligraphyConfig$Builder;->setDefaultFontPath(Ljava/lang/String;)Luk/co/chrisjenx/calligraphy/CalligraphyConfig$Builder;

    move-result-object v5

    const v6, 0x7f010002

    .line 230
    invoke-virtual {v5, v6}, Luk/co/chrisjenx/calligraphy/CalligraphyConfig$Builder;->setFontAttrId(I)Luk/co/chrisjenx/calligraphy/CalligraphyConfig$Builder;

    move-result-object v5

    .line 231
    invoke-virtual {v5}, Luk/co/chrisjenx/calligraphy/CalligraphyConfig$Builder;->build()Luk/co/chrisjenx/calligraphy/CalligraphyConfig;

    move-result-object v5

    .line 228
    invoke-static {v5}, Luk/co/chrisjenx/calligraphy/CalligraphyConfig;->initDefault(Luk/co/chrisjenx/calligraphy/CalligraphyConfig;)V

    .line 233
    invoke-direct {p0}, Lcom/vectorwatch/android/VectorApplication;->checkIfAppNewlyUpdated()V

    .line 235
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 237
    new-instance v5, Lcom/vectorwatch/android/VectorApplication$9;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/VectorApplication$9;-><init>(Lcom/vectorwatch/android/VectorApplication;)V

    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/VectorApplication;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 275
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->setApiUriByAppVersion(Landroid/content/Context;)V

    .line 278
    invoke-virtual {p0, p0}, Lcom/vectorwatch/android/VectorApplication;->initBatteryBroadcastReceiver(Landroid/content/Context;)V

    .line 283
    return-void

    .line 139
    .end local v0    # "client":Lio/keen/client/java/KeenClient;
    .end local v3    # "project":Lio/keen/client/java/KeenProject;
    :catch_0
    move-exception v2

    .line 140
    .local v2, "ignored":Ljava/io/FileNotFoundException;
    sget-object v5, Lcom/vectorwatch/android/VectorApplication;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception while migrating "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    .end local v2    # "ignored":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 142
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/vectorwatch/android/VectorApplication;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "General exception while migrating "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 149
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v1

    .line 150
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v5, Lcom/vectorwatch/android/VectorApplication;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error while trying to compact realm. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onTerminate()V
    .locals 2

    .prologue
    .line 306
    invoke-super {p0}, Landroid/support/multidex/MultiDexApplication;->onTerminate()V

    .line 307
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 309
    sget-object v0, Lcom/vectorwatch/android/VectorApplication;->sAppInstallManagerInstance:Lcom/vectorwatch/android/utils/AppInstallManager;

    if-eqz v0, :cond_0

    .line 310
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/VectorApplication;->sAppInstallManagerInstance:Lcom/vectorwatch/android/utils/AppInstallManager;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 313
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/VectorApplication;->sTransferManager:Lcom/vectorwatch/android/managers/transfer_manager/TransferManager;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 314
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/managers/LiveStreamManager;->closeEventBus()V

    .line 315
    return-void
.end method
