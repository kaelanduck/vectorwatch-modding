.class final Lcom/vectorwatch/android/RemotePushNotificationsInitializer$1;
.super Ljava/lang/Object;
.source "RemotePushNotificationsInitializer.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->syncEndpointArnToCloud(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/SyncResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 2
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 204
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "RemotePushNotificationsInitializer: Sync endpointARN to cloud with error!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/SyncResponse;Lretrofit/client/Response;)V
    .locals 3
    .param p1, "syncResponse"    # Lcom/vectorwatch/android/models/SyncResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 193
    invoke-virtual {p2}, Lretrofit/client/Response;->getStatus()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 194
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "RemotePushNotificationsInitializer: Sync endpointARN to cloud with success!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 195
    const-string v0, "amazon_endpoint_arn_dirty_key"

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$1;->val$context:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 200
    :goto_0
    return-void

    .line 198
    :cond_0
    # getter for: Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "RemotePushNotificationsInitializer: Sync endpointARN to cloud with error!"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 190
    check-cast p1, Lcom/vectorwatch/android/models/SyncResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer$1;->success(Lcom/vectorwatch/android/models/SyncResponse;Lretrofit/client/Response;)V

    return-void
.end method
