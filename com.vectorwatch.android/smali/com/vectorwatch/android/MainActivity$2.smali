.class Lcom/vectorwatch/android/MainActivity$2;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/support/design/widget/TabLayout$OnTabSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/MainActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/MainActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/MainActivity;

    .prologue
    .line 316
    iput-object p1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTabReselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .locals 0
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .prologue
    .line 378
    return-void
.end method

.method public onTabSelected(Landroid/support/design/widget/TabLayout$Tab;)V
    .locals 5
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .prologue
    .line 319
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/MainActivity;->mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->getCurrentItem()I

    move-result v0

    .line 320
    .local v0, "previousPage":I
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/MainActivity;->mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getPosition()I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setCurrentItem(IZ)V

    .line 321
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getPosition()I

    move-result v2

    # setter for: Lcom/vectorwatch/android/MainActivity;->currentPage:I
    invoke-static {v1, v2}, Lcom/vectorwatch/android/MainActivity;->access$002(Lcom/vectorwatch/android/MainActivity;I)I

    .line 323
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$100(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/helper/OrientationManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 325
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->currentPage:I
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$000(Lcom/vectorwatch/android/MainActivity;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 326
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$100(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/helper/OrientationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/helper/OrientationManager;->enable()V

    .line 332
    :cond_0
    :goto_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/TabSelectedEvent;

    iget-object v3, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->currentPage:I
    invoke-static {v3}, Lcom/vectorwatch/android/MainActivity;->access$000(Lcom/vectorwatch/android/MainActivity;)I

    move-result v3

    invoke-direct {v2, v3, v0}, Lcom/vectorwatch/android/events/TabSelectedEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 334
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->currentPage:I
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$000(Lcom/vectorwatch/android/MainActivity;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 368
    :goto_1
    return-void

    .line 328
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->mOrientationManager:Lcom/vectorwatch/android/ui/helper/OrientationManager;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$100(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/helper/OrientationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/helper/OrientationManager;->disable()V

    goto :goto_0

    .line 336
    :pswitch_0
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_MENU_TAB:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ACTIVITY_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v1, v2, v3, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    .line 339
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/ActivityTabSelectedEvent;

    invoke-direct {v2}, Lcom/vectorwatch/android/events/ActivityTabSelectedEvent;-><init>()V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_1

    .line 343
    :pswitch_1
    const/4 v1, 0x0

    sput v1, Lcom/vectorwatch/android/VectorApplication;->sStoreCount:I

    .line 344
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    # getter for: Lcom/vectorwatch/android/MainActivity;->mBadgeView:Lcom/vectorwatch/android/ui/view/BadgeView;
    invoke-static {v1}, Lcom/vectorwatch/android/MainActivity;->access$200(Lcom/vectorwatch/android/MainActivity;)Lcom/vectorwatch/android/ui/view/BadgeView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/view/BadgeView;->hide()V

    .line 345
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    invoke-static {v1}, Lme/leolin/shortcutbadger/ShortcutBadger;->removeCount(Landroid/content/Context;)Z

    .line 347
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_MENU_TAB:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_STORE_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v1, v2, v3, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    goto :goto_1

    .line 353
    :pswitch_2
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_MENU_TAB:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SETTINGS_OVERVIEW:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v1, v2, v3, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    goto :goto_1

    .line 359
    :pswitch_3
    iget-object v1, p0, Lcom/vectorwatch/android/MainActivity$2;->this$0:Lcom/vectorwatch/android/MainActivity;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_MENU_TAB:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CHANGED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v4, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_WATCHMAKER:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {v1, v2, v3, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    goto :goto_1

    .line 334
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onTabUnselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .locals 0
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .prologue
    .line 373
    return-void
.end method
