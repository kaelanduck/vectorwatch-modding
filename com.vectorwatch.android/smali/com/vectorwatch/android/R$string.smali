.class public final Lcom/vectorwatch/android/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f090000

.field public static final abc_action_bar_home_description_format:I = 0x7f090297

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f090298

.field public static final abc_action_bar_up_description:I = 0x7f090001

.field public static final abc_action_menu_overflow_description:I = 0x7f090002

.field public static final abc_action_mode_done:I = 0x7f090003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f090004

.field public static final abc_activitychooserview_choose_application:I = 0x7f090005

.field public static final abc_search_hint:I = 0x7f090299

.field public static final abc_searchview_description_clear:I = 0x7f090006

.field public static final abc_searchview_description_query:I = 0x7f090007

.field public static final abc_searchview_description_search:I = 0x7f090008

.field public static final abc_searchview_description_submit:I = 0x7f090009

.field public static final abc_searchview_description_voice:I = 0x7f09000a

.field public static final abc_shareactionprovider_share_with:I = 0x7f09000b

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f09000c

.field public static final abc_toolbar_collapse_description:I = 0x7f09029a

.field public static final account_credentials_section_title:I = 0x7f090043

.field public static final account_details_no_account:I = 0x7f090044

.field public static final account_title:I = 0x7f090045

.field public static final action_add:I = 0x7f090046

.field public static final action_settings:I = 0x7f090047

.field public static final activate_android_notifications:I = 0x7f090048

.field public static final activate_apps_notif_summary:I = 0x7f090049

.field public static final activate_apps_notif_title:I = 0x7f09004a

.field public static final activate_call_notif_summary:I = 0x7f09004b

.field public static final activate_call_notif_title:I = 0x7f09004c

.field public static final activate_sms_notif_summary:I = 0x7f09004d

.field public static final activate_sms_notif_title:I = 0x7f09004e

.field public static final activate_system_notifications_summary:I = 0x7f09004f

.field public static final activate_system_notifications_title:I = 0x7f090050

.field public static final active:I = 0x7f090051

.field public static final activity_reminder:I = 0x7f090052

.field public static final activity_share_no_internet:I = 0x7f090053

.field public static final addGoal:I = 0x7f090054

.field public static final add_new:I = 0x7f090055

.field public static final advanced_button:I = 0x7f090056

.field public static final alarm_delete_info_message:I = 0x7f090057

.field public static final alarm_edit_summary:I = 0x7f090058

.field public static final alarm_no_title:I = 0x7f090059

.field public static final alarms_limit_reached:I = 0x7f09005a

.field public static final alert:I = 0x7f09005b

.field public static final alert_activity_profile_not_set:I = 0x7f09005c

.field public static final alert_download_in_progress:I = 0x7f09005d

.field public static final alert_no_watch_found:I = 0x7f09005e

.field public static final alert_no_watch_found_sdk_23:I = 0x7f09005f

.field public static final alert_share_activity_data_with_google_fit:I = 0x7f090060

.field public static final alert_watch_no_app_settings_available:I = 0x7f090061

.field public static final alert_watch_no_stream_available:I = 0x7f090062

.field public static final alert_watch_not_connected:I = 0x7f090063

.field public static final alert_watch_not_found_sdk_23:I = 0x7f090064

.field public static final app:I = 0x7f09029b

.field public static final app_icon_label:I = 0x7f090065

.field public static final app_id:I = 0x7f090066

.field public static final app_install_error:I = 0x7f090067

.field public static final app_name:I = 0x7f090068

.field public static final app_name_video:I = 0x7f09029c

.field public static final app_notifications_section_title:I = 0x7f090069

.field public static final appbar_scrolling_view_behavior:I = 0x7f09029d

.field public static final apps:I = 0x7f09006a

.field public static final apps_summary:I = 0x7f09006b

.field public static final auth_label:I = 0x7f09006c

.field public static final avg:I = 0x7f09006d

.field public static final awake:I = 0x7f09006e

.field public static final backlight_duration_title:I = 0x7f09006f

.field public static final backlight_intensity:I = 0x7f090070

.field public static final backlight_timeout:I = 0x7f090071

.field public static final base:I = 0x7f090072

.field public static final best_charts:I = 0x7f090073

.field public static final birthday:I = 0x7f090074

.field public static final birthday_error:I = 0x7f090075

.field public static final ble_not_supported:I = 0x7f090076

.field public static final bug_list_error:I = 0x7f090077

.field public static final bug_summary_error:I = 0x7f090078

.field public static final button_allow:I = 0x7f090079

.field public static final button_ask_register:I = 0x7f09007a

.field public static final button_cancel:I = 0x7f09007b

.field public static final button_connect:I = 0x7f09007c

.field public static final button_login:I = 0x7f09007d

.field public static final button_login_fb:I = 0x7f09007e

.field public static final button_login_vector:I = 0x7f09007f

.field public static final button_recover_pass:I = 0x7f090080

.field public static final button_register:I = 0x7f090081

.field public static final button_report_bug:I = 0x7f090082

.field public static final button_retry:I = 0x7f090083

.field public static final button_send_bug_report:I = 0x7f090084

.field public static final button_skip_login:I = 0x7f090085

.field public static final button_skip_to_login:I = 0x7f090086

.field public static final calories:I = 0x7f090087

.field public static final calories_charts:I = 0x7f090088

.field public static final calories_label:I = 0x7f090089

.field public static final cancel:I = 0x7f09008a

.field public static final change_user_name:I = 0x7f09008b

.field public static final chart_y_max_label_format:I = 0x7f09008c

.field public static final check_the_number_shown_on_your_watch_and_select_it_from_the_list:I = 0x7f09008d

.field public static final clear:I = 0x7f09008e

.field public static final clear_all:I = 0x7f09008f

.field public static final close:I = 0x7f090090

.field public static final com_crashlytics_android_build_id:I = 0x7f09029e

.field public static final com_facebook_image_download_unknown_error:I = 0x7f09000d

.field public static final com_facebook_internet_permission_error_message:I = 0x7f09000e

.field public static final com_facebook_internet_permission_error_title:I = 0x7f09000f

.field public static final com_facebook_like_button_liked:I = 0x7f090010

.field public static final com_facebook_like_button_not_liked:I = 0x7f090011

.field public static final com_facebook_loading:I = 0x7f090012

.field public static final com_facebook_loginview_cancel_action:I = 0x7f090013

.field public static final com_facebook_loginview_log_in_button:I = 0x7f090014

.field public static final com_facebook_loginview_log_in_button_long:I = 0x7f090015

.field public static final com_facebook_loginview_log_out_action:I = 0x7f090016

.field public static final com_facebook_loginview_log_out_button:I = 0x7f090017

.field public static final com_facebook_loginview_logged_in_as:I = 0x7f090018

.field public static final com_facebook_loginview_logged_in_using_facebook:I = 0x7f090019

.field public static final com_facebook_send_button_text:I = 0x7f09001a

.field public static final com_facebook_share_button_text:I = 0x7f09001b

.field public static final com_facebook_tooltip_default:I = 0x7f09001c

.field public static final comment:I = 0x7f090091

.field public static final common_google_play_services_api_unavailable_text:I = 0x7f09001d

.field public static final common_google_play_services_enable_button:I = 0x7f09001e

.field public static final common_google_play_services_enable_text:I = 0x7f09001f

.field public static final common_google_play_services_enable_title:I = 0x7f090020

.field public static final common_google_play_services_install_button:I = 0x7f090021

.field public static final common_google_play_services_install_text_phone:I = 0x7f090022

.field public static final common_google_play_services_install_text_tablet:I = 0x7f090023

.field public static final common_google_play_services_install_title:I = 0x7f090024

.field public static final common_google_play_services_invalid_account_text:I = 0x7f090025

.field public static final common_google_play_services_invalid_account_title:I = 0x7f090026

.field public static final common_google_play_services_network_error_text:I = 0x7f090027

.field public static final common_google_play_services_network_error_title:I = 0x7f090028

.field public static final common_google_play_services_notification_ticker:I = 0x7f090029

.field public static final common_google_play_services_resolution_required_text:I = 0x7f09002a

.field public static final common_google_play_services_resolution_required_title:I = 0x7f09002b

.field public static final common_google_play_services_restricted_profile_text:I = 0x7f09002c

.field public static final common_google_play_services_restricted_profile_title:I = 0x7f09002d

.field public static final common_google_play_services_sign_in_failed_text:I = 0x7f09002e

.field public static final common_google_play_services_sign_in_failed_title:I = 0x7f09002f

.field public static final common_google_play_services_unknown_issue:I = 0x7f090030

.field public static final common_google_play_services_unsupported_text:I = 0x7f090031

.field public static final common_google_play_services_unsupported_title:I = 0x7f090032

.field public static final common_google_play_services_update_button:I = 0x7f090033

.field public static final common_google_play_services_update_text:I = 0x7f090034

.field public static final common_google_play_services_update_title:I = 0x7f090035

.field public static final common_google_play_services_updating_text:I = 0x7f090036

.field public static final common_google_play_services_updating_title:I = 0x7f090037

.field public static final common_google_play_services_wear_update_text:I = 0x7f090038

.field public static final common_open_on_phone:I = 0x7f090039

.field public static final common_signin_button_text:I = 0x7f09003a

.field public static final common_signin_button_text_long:I = 0x7f09003b

.field public static final connect_button:I = 0x7f090092

.field public static final connected_to_watch:I = 0x7f090093

.field public static final contextual_element_summary_line_count:I = 0x7f090094

.field public static final contextual_summary_offline:I = 0x7f090095

.field public static final contextual_title_offline:I = 0x7f090096

.field public static final cpb_default_rotation_speed:I = 0x7f09029f

.field public static final cpb_default_sweep_speed:I = 0x7f0902a0

.field public static final create:I = 0x7f090097

.field public static final customize_watch:I = 0x7f090098

.field public static final cwac_cam2_ok:I = 0x7f0902a1

.field public static final cwac_cam2_retry:I = 0x7f0902a2

.field public static final day:I = 0x7f090099

.field public static final deep:I = 0x7f09009a

.field public static final deep_sleep:I = 0x7f09009b

.field public static final default_activity_alert:I = 0x7f09009c

.field public static final default_alarm_name:I = 0x7f09009d

.field public static final default_status_bar_battery_charged_text:I = 0x7f09009e

.field public static final default_text:I = 0x7f09009f

.field public static final default_value:I = 0x7f0900a0

.field public static final default_web_client_id:I = 0x7f0902a3

.field public static final delete:I = 0x7f0900a1

.field public static final delete_item:I = 0x7f0900a2

.field public static final delete_warning_message:I = 0x7f0900a3

.field public static final description:I = 0x7f0900a4

.field public static final description_text:I = 0x7f0900a5

.field public static final dialog_summary_bluetooth:I = 0x7f0900a6

.field public static final dialog_summary_permission_location:I = 0x7f0900a7

.field public static final dialog_title_bluetooth:I = 0x7f0900a8

.field public static final disclaimer_activity_info:I = 0x7f0900a9

.field public static final disconnected_from_watch:I = 0x7f0900aa

.field public static final dismiss:I = 0x7f0900ab

.field public static final distance_label:I = 0x7f0900ac

.field public static final dnd_off_notification:I = 0x7f0900ad

.field public static final dnd_on_notification:I = 0x7f0900ae

.field public static final done:I = 0x7f0900af

.field public static final download:I = 0x7f0900b0

.field public static final download_in_progress:I = 0x7f0900b1

.field public static final drag_stream:I = 0x7f0900b2

.field public static final edit:I = 0x7f0900b3

.field public static final edit_alarm:I = 0x7f0900b4

.field public static final edit_app_title:I = 0x7f0900b5

.field public static final edit_goal:I = 0x7f0900b6

.field public static final edit_text_bug_details:I = 0x7f0900b7

.field public static final edit_text_bug_summary:I = 0x7f0900b8

.field public static final edit_text_other_mail:I = 0x7f0900b9

.field public static final edit_watchface:I = 0x7f0900ba

.field public static final email:I = 0x7f0900bb

.field public static final email_check:I = 0x7f0900bc

.field public static final email_format_error:I = 0x7f0900bd

.field public static final enable_notifs:I = 0x7f0900be

.field public static final error_agree_to_terms_not_checked:I = 0x7f0900bf

.field public static final error_all_input_numbers_start_with_0:I = 0x7f0900c0

.field public static final error_bluetooth_is_off:I = 0x7f0900c1

.field public static final error_code_200:I = 0x7f0900c2

.field public static final error_code_201:I = 0x7f0900c3

.field public static final error_code_202:I = 0x7f0900c4

.field public static final error_code_203:I = 0x7f0900c5

.field public static final error_code_204:I = 0x7f0900c6

.field public static final error_code_205:I = 0x7f0900c7

.field public static final error_code_206:I = 0x7f0900c8

.field public static final error_code_207:I = 0x7f0900c9

.field public static final error_code_208:I = 0x7f0900ca

.field public static final error_code_226:I = 0x7f0900cb

.field public static final error_code_300:I = 0x7f0900cc

.field public static final error_code_301:I = 0x7f0900cd

.field public static final error_code_302:I = 0x7f0900ce

.field public static final error_code_303:I = 0x7f0900cf

.field public static final error_code_304:I = 0x7f0900d0

.field public static final error_code_305:I = 0x7f0900d1

.field public static final error_code_306:I = 0x7f0900d2

.field public static final error_code_307:I = 0x7f0900d3

.field public static final error_code_308:I = 0x7f0900d4

.field public static final error_code_400:I = 0x7f0900d5

.field public static final error_code_401:I = 0x7f0900d6

.field public static final error_code_402:I = 0x7f0900d7

.field public static final error_code_403:I = 0x7f0900d8

.field public static final error_code_404:I = 0x7f0900d9

.field public static final error_code_405:I = 0x7f0900da

.field public static final error_code_406:I = 0x7f0900db

.field public static final error_code_407:I = 0x7f0900dc

.field public static final error_code_408:I = 0x7f0900dd

.field public static final error_code_409:I = 0x7f0900de

.field public static final error_code_410:I = 0x7f0900df

.field public static final error_code_411:I = 0x7f0900e0

.field public static final error_code_412:I = 0x7f0900e1

.field public static final error_code_413:I = 0x7f0900e2

.field public static final error_code_414:I = 0x7f0900e3

.field public static final error_code_415:I = 0x7f0900e4

.field public static final error_code_416:I = 0x7f0900e5

.field public static final error_code_417:I = 0x7f0900e6

.field public static final error_code_421:I = 0x7f0900e7

.field public static final error_code_422:I = 0x7f0900e8

.field public static final error_code_423:I = 0x7f0900e9

.field public static final error_code_424:I = 0x7f0900ea

.field public static final error_code_426:I = 0x7f0900eb

.field public static final error_code_428:I = 0x7f0900ec

.field public static final error_code_429:I = 0x7f0900ed

.field public static final error_code_431:I = 0x7f0900ee

.field public static final error_code_451:I = 0x7f0900ef

.field public static final error_code_500:I = 0x7f0900f0

.field public static final error_code_501:I = 0x7f0900f1

.field public static final error_code_502:I = 0x7f0900f2

.field public static final error_code_503:I = 0x7f0900f3

.field public static final error_code_504:I = 0x7f0900f4

.field public static final error_code_505:I = 0x7f0900f5

.field public static final error_code_506:I = 0x7f0900f6

.field public static final error_code_507:I = 0x7f0900f7

.field public static final error_code_508:I = 0x7f0900f8

.field public static final error_code_510:I = 0x7f0900f9

.field public static final error_code_511:I = 0x7f0900fa

.field public static final error_code_900:I = 0x7f0900fb

.field public static final error_code_901:I = 0x7f0900fc

.field public static final error_code_904:I = 0x7f0900fd

.field public static final error_code_929:I = 0x7f0900fe

.field public static final error_default_streams_download:I = 0x7f0900ff

.field public static final error_email_password_invalid:I = 0x7f090100

.field public static final error_generic_default_apps:I = 0x7f090101

.field public static final error_generic_something_went_wrong:I = 0x7f090102

.field public static final error_input_empty:I = 0x7f090103

.field public static final error_input_height_imperial:I = 0x7f090104

.field public static final error_input_height_metric:I = 0x7f090105

.field public static final error_input_number_starts_with_0:I = 0x7f090106

.field public static final error_input_number_too_big:I = 0x7f090107

.field public static final error_input_weight_kg:I = 0x7f090108

.field public static final error_input_weight_lbs:I = 0x7f090109

.field public static final error_installation_id:I = 0x7f09010a

.field public static final error_installed_apps_limit_reached:I = 0x7f09010b

.field public static final error_missing_system_info:I = 0x7f09010c

.field public static final error_no_account_found:I = 0x7f09010d

.field public static final error_no_internet_connection:I = 0x7f09010e

.field public static final error_no_internet_connection_small:I = 0x7f09010f

.field public static final error_not_connected_to_google_fit:I = 0x7f090110

.field public static final error_not_enough_space_on_watch:I = 0x7f090111

.field public static final error_oauth_general:I = 0x7f090112

.field public static final error_oauth_no_url:I = 0x7f090113

.field public static final error_occurred_retry:I = 0x7f090114

.field public static final error_server_generic_message:I = 0x7f090115

.field public static final error_setup_apps_list_not_ok:I = 0x7f090116

.field public static final error_token_missing:I = 0x7f090117

.field public static final error_try_to_delete_last_app:I = 0x7f090118

.field public static final every_friday:I = 0x7f090119

.field public static final every_monday:I = 0x7f09011a

.field public static final every_saturday:I = 0x7f09011b

.field public static final every_sunday:I = 0x7f09011c

.field public static final every_thursday:I = 0x7f09011d

.field public static final every_tuesday:I = 0x7f09011e

.field public static final every_wednesday:I = 0x7f09011f

.field public static final example_height:I = 0x7f090120

.field public static final example_weight_imperial:I = 0x7f090121

.field public static final example_weight_metric:I = 0x7f090122

.field public static final export_activity_data:I = 0x7f090123

.field public static final facebook:I = 0x7f090124

.field public static final facebook_app_id:I = 0x7f090125

.field public static final facebook_login_warning:I = 0x7f090126

.field public static final failed_share_link:I = 0x7f090127

.field public static final failed_to_communicate_with_watch:I = 0x7f090128

.field public static final fairly_active:I = 0x7f090129

.field public static final fb_app_id:I = 0x7f09012a

.field public static final female:I = 0x7f09012b

.field public static final filters:I = 0x7f09012c

.field public static final firebase_database_url:I = 0x7f0902a4

.field public static final from:I = 0x7f09012d

.field public static final gcm_defaultSenderId:I = 0x7f0902a5

.field public static final gender:I = 0x7f09012e

.field public static final get_started:I = 0x7f09012f

.field public static final glanceble_info:I = 0x7f090130

.field public static final goal_achievement_alert:I = 0x7f090131

.field public static final goal_almost_alert:I = 0x7f090132

.field public static final goal_calories:I = 0x7f090133

.field public static final goal_charts:I = 0x7f090134

.field public static final goal_steps:I = 0x7f090135

.field public static final google_api_key:I = 0x7f0902a6

.field public static final google_app_id:I = 0x7f0902a7

.field public static final google_crash_reporting_api_key:I = 0x7f0902a8

.field public static final google_storage_bucket:I = 0x7f0902a9

.field public static final gps_off:I = 0x7f090136

.field public static final height:I = 0x7f090137

.field public static final hello_blank_fragment:I = 0x7f090138

.field public static final hello_world:I = 0x7f090139

.field public static final highly_active:I = 0x7f09013a

.field public static final hint_options_stream_list:I = 0x7f09013b

.field public static final hours_charts:I = 0x7f09013c

.field public static final hours_short:I = 0x7f09013d

.field public static final i_agree_to_the:I = 0x7f09013e

.field public static final info_screen_check_compatibility_text:I = 0x7f09013f

.field public static final info_screen_no_internet_text:I = 0x7f090140

.field public static final info_screen_server_problem:I = 0x7f090141

.field public static final info_screen_setting_up_watch_text:I = 0x7f090142

.field public static final install_on_watch:I = 0x7f090143

.field public static final invalid_token:I = 0x7f090144

.field public static final joda_time_android_date_time:I = 0x7f09003c

.field public static final joda_time_android_preposition_for_date:I = 0x7f09003d

.field public static final joda_time_android_preposition_for_time:I = 0x7f09003e

.field public static final joda_time_android_relative_time:I = 0x7f09003f

.field public static final kg:I = 0x7f090145

.field public static final km_short:I = 0x7f090146

.field public static final label_about:I = 0x7f090147

.field public static final label_ac:I = 0x7f090148

.field public static final label_account_profile:I = 0x7f090149

.field public static final label_activity_info:I = 0x7f09014a

.field public static final label_alarm_name_option:I = 0x7f09014b

.field public static final label_alarm_repeat_option:I = 0x7f09014c

.field public static final label_alarms:I = 0x7f09014d

.field public static final label_alarms_delete_option:I = 0x7f09014e

.field public static final label_allow_info:I = 0x7f09014f

.field public static final label_allow_notifications:I = 0x7f090150

.field public static final label_auto_dismiss_note:I = 0x7f090151

.field public static final label_connect_connecting:I = 0x7f090152

.field public static final label_connect_found_it:I = 0x7f090153

.field public static final label_connect_info:I = 0x7f090154

.field public static final label_connect_searching:I = 0x7f090155

.field public static final label_connect_tour:I = 0x7f090156

.field public static final label_connect_watch:I = 0x7f090157

.field public static final label_connect_watch_connected:I = 0x7f090158

.field public static final label_connectivity_issues:I = 0x7f090159

.field public static final label_contextual:I = 0x7f09015a

.field public static final label_found_many:I = 0x7f09015b

.field public static final label_found_one:I = 0x7f09015c

.field public static final label_gps_disabled_confirm_ok:I = 0x7f09015d

.field public static final label_gps_disabled_confirm_text:I = 0x7f09015e

.field public static final label_gps_disabled_notification_action_text:I = 0x7f09015f

.field public static final label_gps_disabled_notification_text:I = 0x7f090160

.field public static final label_gps_disabled_notification_title:I = 0x7f090161

.field public static final label_login_info:I = 0x7f090162

.field public static final label_logout:I = 0x7f090163

.field public static final label_newsletter:I = 0x7f090164

.field public static final label_notification_display_options:I = 0x7f090165

.field public static final label_notifications:I = 0x7f090166

.field public static final label_oauth_app_login_expired_confirm_text:I = 0x7f090167

.field public static final label_oauth_login:I = 0x7f090168

.field public static final label_oauth_login_activity_title:I = 0x7f090169

.field public static final label_oauth_login_activity_title_default:I = 0x7f09016a

.field public static final label_oauth_login_expired_cancel:I = 0x7f09016b

.field public static final label_oauth_login_expired_confirm_text:I = 0x7f09016c

.field public static final label_oauth_login_expired_notification_action_text:I = 0x7f09016d

.field public static final label_oauth_login_expired_notification_text:I = 0x7f09016e

.field public static final label_oauth_login_expired_notification_title:I = 0x7f09016f

.field public static final label_oauth_login_expired_ok:I = 0x7f090170

.field public static final label_pref_version:I = 0x7f090171

.field public static final label_repeat:I = 0x7f090172

.field public static final label_reset_settings:I = 0x7f090173

.field public static final label_set_alarm:I = 0x7f090174

.field public static final label_settings_alerts_mirror_phone_summary:I = 0x7f090175

.field public static final label_settings_alerts_mirror_phone_title:I = 0x7f090176

.field public static final label_settings_section_1:I = 0x7f090177

.field public static final label_settings_section_2:I = 0x7f090178

.field public static final label_settings_section_3:I = 0x7f090179

.field public static final label_settings_section_4:I = 0x7f09017a

.field public static final label_support:I = 0x7f09017b

.field public static final label_title_permission_location:I = 0x7f09017c

.field public static final label_tour:I = 0x7f09017d

.field public static final label_unpaired_from_settings:I = 0x7f09017e

.field public static final label_update_watch:I = 0x7f09017f

.field public static final label_version:I = 0x7f090180

.field public static final label_walkthrough_elegance:I = 0x7f090181

.field public static final label_walkthrough_features:I = 0x7f090182

.field public static final label_walkthrough_greeting:I = 0x7f090183

.field public static final label_walkthrough_howto:I = 0x7f090184

.field public static final label_walkthrough_intelligence:I = 0x7f090185

.field public static final label_walkthrough_power:I = 0x7f090186

.field public static final label_walkthrough_simplicity:I = 0x7f090187

.field public static final label_walkthrough_statement:I = 0x7f090188

.field public static final label_watch:I = 0x7f090189

.field public static final language_de:I = 0x7f09018a

.field public static final language_default:I = 0x7f09018b

.field public static final language_en:I = 0x7f09018c

.field public static final language_es:I = 0x7f09018d

.field public static final language_fr:I = 0x7f09018e

.field public static final language_nl:I = 0x7f09018f

.field public static final language_options_title:I = 0x7f090190

.field public static final language_ro:I = 0x7f090191

.field public static final language_tr:I = 0x7f090192

.field public static final language_watch_only:I = 0x7f090193

.field public static final lbs:I = 0x7f090194

.field public static final lets_connect_your_vector_watch:I = 0x7f090195

.field public static final light:I = 0x7f090196

.field public static final light_day:I = 0x7f090197

.field public static final light_sleep:I = 0x7f090198

.field public static final limited_internet_access:I = 0x7f090199

.field public static final link_lost:I = 0x7f09019a

.field public static final live_stream_warning:I = 0x7f09019b

.field public static final logged_into:I = 0x7f09019c

.field public static final login:I = 0x7f09019d

.field public static final login_with_facebook:I = 0x7f09019e

.field public static final logout_account_question:I = 0x7f09019f

.field public static final low_battery:I = 0x7f0901a0

.field public static final low_phone_battery_level:I = 0x7f0901a1

.field public static final low_watch_battery_level:I = 0x7f0901a2

.field public static final male:I = 0x7f0901a3

.field public static final menu_switch_picture:I = 0x7f0902aa

.field public static final menu_switch_video:I = 0x7f0902ab

.field public static final message_app_authentication_next_time:I = 0x7f0901a4

.field public static final message_app_settings_canceled:I = 0x7f0901a5

.field public static final message_authentication_next_time:I = 0x7f0901a6

.field public static final message_authentication_successful:I = 0x7f0901a7

.field public static final message_option_not_found:I = 0x7f0901a8

.field public static final message_stream_authentication_next_time:I = 0x7f0901a9

.field public static final messages:I = 0x7f0901aa

.field public static final messenger_send_button_text:I = 0x7f090040

.field public static final miles:I = 0x7f0901ab

.field public static final miles_short:I = 0x7f0901ac

.field public static final missed_call_from:I = 0x7f0901ad

.field public static final missing_login_info:I = 0x7f0901ae

.field public static final missing_registration_information:I = 0x7f0901af

.field public static final month:I = 0x7f0901b0

.field public static final morning_face_off:I = 0x7f0901b1

.field public static final morning_face_on:I = 0x7f0901b2

.field public static final music_app:I = 0x7f0901b3

.field public static final name:I = 0x7f0901b4

.field public static final name_not_empy:I = 0x7f0901b5

.field public static final name_not_set:I = 0x7f0901b6

.field public static final new_watch_update:I = 0x7f0901b7

.field public static final newsletter_app:I = 0x7f0901b8

.field public static final newsletter_news:I = 0x7f0901b9

.field public static final next:I = 0x7f0901ba

.field public static final no:I = 0x7f0901bb

.field public static final no_apps_available:I = 0x7f0901bc

.field public static final no_artist_found:I = 0x7f0901bd

.field public static final no_bond_detected:I = 0x7f0901be

.field public static final no_browser:I = 0x7f0901bf

.field public static final no_data:I = 0x7f0901c0

.field public static final no_email_clients:I = 0x7f0901c1

.field public static final no_faces_available:I = 0x7f0901c2

.field public static final no_gps_data:I = 0x7f0901c3

.field public static final no_gps_error:I = 0x7f0901c4

.field public static final no_label:I = 0x7f0901c5

.field public static final no_new_notifications:I = 0x7f0901c6

.field public static final no_ratings_available:I = 0x7f0901c7

.field public static final no_streams_available:I = 0x7f0901c8

.field public static final no_track_found:I = 0x7f0901c9

.field public static final no_value_pace:I = 0x7f0901ca

.field public static final no_value_speed:I = 0x7f0901cb

.field public static final not_supported:I = 0x7f0901cc

.field public static final note_mandatory_app_update:I = 0x7f0901cd

.field public static final note_mandatory_watch_update:I = 0x7f0901ce

.field public static final notification_button:I = 0x7f0901cf

.field public static final notification_display_alert:I = 0x7f0901d0

.field public static final notification_display_content:I = 0x7f0901d1

.field public static final notification_display_off:I = 0x7f0901d2

.field public static final of:I = 0x7f0901d3

.field public static final off:I = 0x7f0901d4

.field public static final offline:I = 0x7f0901d5

.field public static final offline_mode:I = 0x7f0901d6

.field public static final offline_slide:I = 0x7f0901d7

.field public static final ok:I = 0x7f0901d8

.field public static final on:I = 0x7f0901d9

.field public static final open:I = 0x7f0901da

.field public static final pair_button:I = 0x7f0901db

.field public static final pair_from_bt:I = 0x7f0901dc

.field public static final pair_watch:I = 0x7f0901dd

.field public static final parse_inet_error:I = 0x7f0901de

.field public static final pass_length:I = 0x7f0901df

.field public static final password:I = 0x7f0901e0

.field public static final password_check:I = 0x7f0901e1

.field public static final permission_denied_explanation:I = 0x7f0901e2

.field public static final permission_rationale:I = 0x7f0901e3

.field public static final personalized_message:I = 0x7f0901e4

.field public static final phone:I = 0x7f0901e5

.field public static final phone_active_notifications:I = 0x7f0901e6

.field public static final phone_notifications_section_title:I = 0x7f0901e7

.field public static final place_autocomplete_clear_button:I = 0x7f090041

.field public static final place_autocomplete_search_hint:I = 0x7f090042

.field public static final play_error_message:I = 0x7f0902ac

.field public static final play_progressive_error_message:I = 0x7f0902ad

.field public static final please_add_comment:I = 0x7f0901e8

.field public static final please_login:I = 0x7f0901e9

.field public static final prb_default_symbolic_string:I = 0x7f0901ea

.field public static final pref_summary_contextual:I = 0x7f0901eb

.field public static final pref_summary_time_zones:I = 0x7f0901ec

.field public static final pref_summary_watch:I = 0x7f0901ed

.field public static final pref_title_alerts_phone_related:I = 0x7f0901ee

.field public static final pref_title_alerts_system_apps:I = 0x7f0901ef

.field public static final pref_title_alerts_user_apps:I = 0x7f0901f0

.field public static final pref_title_time_zones:I = 0x7f0901f1

.field public static final pref_version:I = 0x7f0901f2

.field public static final preference_account_in_use:I = 0x7f0901f3

.field public static final preference_activity:I = 0x7f0901f4

.field public static final preference_category_device_management:I = 0x7f0901f5

.field public static final preference_category_information:I = 0x7f0901f6

.field public static final preference_category_personalize:I = 0x7f0901f7

.field public static final preference_summary_account_info:I = 0x7f0901f8

.field public static final preference_summary_account_profile:I = 0x7f0901f9

.field public static final preference_summary_alerts_notifications:I = 0x7f0901fa

.field public static final preference_title_developer_mode:I = 0x7f0901fb

.field public static final preference_title_logs_details:I = 0x7f0901fc

.field public static final previous:I = 0x7f0901fd

.field public static final profile_change_name:I = 0x7f0901fe

.field public static final profile_imperial:I = 0x7f0901ff

.field public static final profile_language:I = 0x7f090200

.field public static final profile_logged_as:I = 0x7f090201

.field public static final profile_logged_out:I = 0x7f090202

.field public static final profile_login_with_facebook_or_email:I = 0x7f090203

.field public static final profile_metric:I = 0x7f090204

.field public static final profile_name_set:I = 0x7f090205

.field public static final profile_removed_name:I = 0x7f090206

.field public static final profile_set:I = 0x7f090207

.field public static final provider_label:I = 0x7f090208

.field public static final rating:I = 0x7f090209

.field public static final recover_error:I = 0x7f09020a

.field public static final register_for_updates:I = 0x7f09020b

.field public static final relaxed:I = 0x7f09020c

.field public static final repeat_friday:I = 0x7f09020d

.field public static final repeat_monday:I = 0x7f09020e

.field public static final repeat_once:I = 0x7f09020f

.field public static final repeat_saturday:I = 0x7f090210

.field public static final repeat_sunday:I = 0x7f090211

.field public static final repeat_thursday:I = 0x7f090212

.field public static final repeat_tuesday:I = 0x7f090213

.field public static final repeat_wednesday:I = 0x7f090214

.field public static final report_abuse:I = 0x7f090215

.field public static final sample_hide_log:I = 0x7f090216

.field public static final sample_show_log:I = 0x7f090217

.field public static final save:I = 0x7f090218

.field public static final scanning:I = 0x7f090219

.field public static final searching:I = 0x7f09021a

.field public static final seconds:I = 0x7f09021b

.field public static final see_reviews:I = 0x7f09021c

.field public static final select_device:I = 0x7f09021d

.field public static final select_gender:I = 0x7f09021e

.field public static final select_time_format:I = 0x7f09021f

.field public static final select_unit_system:I = 0x7f090220

.field public static final server_error:I = 0x7f090221

.field public static final server_unavailable:I = 0x7f090222

.field public static final set:I = 0x7f090223

.field public static final set_goal:I = 0x7f090224

.field public static final set_height:I = 0x7f090225

.field public static final set_height_cm:I = 0x7f090226

.field public static final set_weight_imperial:I = 0x7f090227

.field public static final set_weight_metric:I = 0x7f090228

.field public static final set_your_personal_data:I = 0x7f090229

.field public static final setting_up_you_watch_please_wait:I = 0x7f09022a

.field public static final share_fail:I = 0x7f09022b

.field public static final share_fail_no_permissions:I = 0x7f09022c

.field public static final share_success:I = 0x7f09022d

.field public static final share_your_activity_data:I = 0x7f09022e

.field public static final show_icon_link_lost:I = 0x7f09022f

.field public static final show_notification_link_lost:I = 0x7f090230

.field public static final sign_up_error:I = 0x7f090231

.field public static final signup:I = 0x7f090232

.field public static final sleep_label:I = 0x7f090233

.field public static final status_bar_notification_info_overflow:I = 0x7f0902ae

.field public static final step:I = 0x7f090234

.field public static final steps:I = 0x7f090235

.field public static final steps_button:I = 0x7f090236

.field public static final steps_charts:I = 0x7f090237

.field public static final steps_label:I = 0x7f090238

.field public static final store:I = 0x7f090239

.field public static final stream:I = 0x7f09023a

.field public static final stream_config_error:I = 0x7f09023b

.field public static final stream_inet_error:I = 0x7f09023c

.field public static final summary_auto_discreet:I = 0x7f09023d

.field public static final summary_auto_sleep:I = 0x7f09023e

.field public static final summary_enable_google_fit_syn_off:I = 0x7f09023f

.field public static final summary_enable_google_fit_syn_on:I = 0x7f090240

.field public static final summary_file:I = 0x7f0902af

.field public static final summary_flick_dismiss:I = 0x7f090241

.field public static final summary_glance:I = 0x7f090242

.field public static final summary_morning_face:I = 0x7f090243

.field public static final summary_notifications_experience:I = 0x7f090244

.field public static final summary_second_hand:I = 0x7f090245

.field public static final summary_tentative_meetings:I = 0x7f090246

.field public static final supported_apps_section_title:I = 0x7f090247

.field public static final switch_to_offline_message:I = 0x7f090248

.field public static final switch_to_online_message:I = 0x7f090249

.field public static final tab_activity:I = 0x7f09024a

.field public static final tab_alarms:I = 0x7f09024b

.field public static final tab_settings:I = 0x7f09024c

.field public static final tab_store_apps:I = 0x7f09024d

.field public static final tab_store_faces:I = 0x7f09024e

.field public static final tab_store_streams:I = 0x7f09024f

.field public static final tab_watchmaker:I = 0x7f090250

.field public static final tag_no_title:I = 0x7f090251

.field public static final take_picture:I = 0x7f0902b0

.field public static final take_tour:I = 0x7f090252

.field public static final tap_alarm_msg:I = 0x7f090253

.field public static final tap_stream:I = 0x7f090254

.field public static final tap_watchface:I = 0x7f090255

.field public static final terms_error:I = 0x7f090256

.field public static final terms_of_use:I = 0x7f090257

.field public static final text_attach_file_for_bug:I = 0x7f090258

.field public static final text_use_login_mail:I = 0x7f090259

.field public static final time_format:I = 0x7f09025a

.field public static final time_not_known:I = 0x7f09025b

.field public static final timezone_not_selected:I = 0x7f09025c

.field public static final title_activity_base:I = 0x7f09025d

.field public static final title_activity_graph:I = 0x7f09025e

.field public static final title_activity_locker:I = 0x7f09025f

.field public static final title_activity_oauth_login:I = 0x7f090260

.field public static final title_activity_onboarding:I = 0x7f090261

.field public static final title_activity_setup_watch:I = 0x7f090262

.field public static final title_activity_store:I = 0x7f090263

.field public static final title_activity_store_apps:I = 0x7f090264

.field public static final title_activity_store_featured:I = 0x7f090265

.field public static final title_activity_store_search:I = 0x7f090266

.field public static final title_activity_store_watch_faces:I = 0x7f090267

.field public static final title_activity_watch:I = 0x7f090268

.field public static final title_allow_switch_flash_mode:I = 0x7f0902b1

.field public static final title_auto_discreet:I = 0x7f090269

.field public static final title_auto_sleep:I = 0x7f09026a

.field public static final title_confirm:I = 0x7f0902b2

.field public static final title_confirmation_quality:I = 0x7f0902b3

.field public static final title_debug:I = 0x7f0902b4

.field public static final title_duration:I = 0x7f0902b5

.field public static final title_enable_google_fit_syn:I = 0x7f09026b

.field public static final title_exact_match:I = 0x7f0902b6

.field public static final title_ffc:I = 0x7f0902b7

.field public static final title_file:I = 0x7f0902b8

.field public static final title_flash_mode:I = 0x7f0902b9

.field public static final title_flick_dismiss:I = 0x7f09026c

.field public static final title_focus_mode:I = 0x7f0902ba

.field public static final title_force_classic:I = 0x7f0902bb

.field public static final title_glance:I = 0x7f09026d

.field public static final title_mirror_preview:I = 0x7f0902bc

.field public static final title_morning_face:I = 0x7f09026e

.field public static final title_notifications_experience:I = 0x7f09026f

.field public static final title_quality:I = 0x7f0902bd

.field public static final title_save_preview:I = 0x7f0902be

.field public static final title_second_hand:I = 0x7f090270

.field public static final title_section1:I = 0x7f090271

.field public static final title_section2:I = 0x7f090272

.field public static final title_section3:I = 0x7f090273

.field public static final title_size:I = 0x7f0902bf

.field public static final title_tentative_meetings:I = 0x7f090274

.field public static final title_update_media_store:I = 0x7f0902c0

.field public static final title_use_chooser:I = 0x7f0902c1

.field public static final title_video:I = 0x7f0902c2

.field public static final title_zoom_style:I = 0x7f0902c3

.field public static final toggle_off_contextual:I = 0x7f090275

.field public static final toggle_on_contextual:I = 0x7f090276

.field public static final total:I = 0x7f090277

.field public static final type_city:I = 0x7f0902c4

.field public static final uninstall:I = 0x7f090278

.field public static final unit_system:I = 0x7f090279

.field public static final unpair_watch_question:I = 0x7f09027a

.field public static final unpaired_from_settings_message:I = 0x7f09027b

.field public static final up_to_date:I = 0x7f09027c

.field public static final update_app_mandatory_install:I = 0x7f09027d

.field public static final update_language_watch:I = 0x7f09027e

.field public static final update_watch_button_check_for_updates:I = 0x7f09027f

.field public static final update_watch_button_download_and_install:I = 0x7f090280

.field public static final update_watch_button_try_again:I = 0x7f090281

.field public static final update_watch_current_version:I = 0x7f090282

.field public static final update_watch_error_no_internet:I = 0x7f090283

.field public static final update_watch_extra_waiting_time_disclaimer:I = 0x7f090284

.field public static final update_watch_mandatory_install:I = 0x7f090285

.field public static final update_watch_no_summary:I = 0x7f090286

.field public static final update_watch_search_for_update:I = 0x7f090287

.field public static final update_watch_something_went_wrong:I = 0x7f090288

.field public static final update_watch_status_completed:I = 0x7f090289

.field public static final update_watch_status_downloading:I = 0x7f09028a

.field public static final update_watch_status_installing:I = 0x7f09028b

.field public static final update_watch_status_verifying:I = 0x7f09028c

.field public static final update_watch_status_waiting:I = 0x7f09028d

.field public static final update_watch_update_available:I = 0x7f09028e

.field public static final wait_for_watch:I = 0x7f09028f

.field public static final week:I = 0x7f090290

.field public static final weekly_activity_newsletter:I = 0x7f090291

.field public static final weight:I = 0x7f090292

.field public static final welcome:I = 0x7f090293

.field public static final wrong_stream_option:I = 0x7f090294

.field public static final year:I = 0x7f090295

.field public static final yes:I = 0x7f090296


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
