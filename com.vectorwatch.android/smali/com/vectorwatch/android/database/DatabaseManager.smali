.class public Lcom/vectorwatch/android/database/DatabaseManager;
.super Ljava/lang/Object;
.source "DatabaseManager.java"


# static fields
.field private static instance:Lcom/vectorwatch/android/database/DatabaseManager;

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/vectorwatch/android/database/DatabaseManager;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method private getAlarmNextKey(Lio/realm/Realm;)J
    .locals 8
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 124
    const-class v4, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v4}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v3

    .line 125
    .local v3, "query":Lio/realm/RealmQuery;, "Lio/realm/RealmQuery<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    const-string v4, "id"

    invoke-virtual {v3, v4}, Lio/realm/RealmQuery;->max(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v2

    .line 127
    .local v2, "idInRealm":Ljava/lang/Number;
    const-wide/16 v0, 0x1

    .line 129
    .local v0, "id":J
    if-eqz v2, :cond_0

    .line 130
    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long v0, v4, v6

    .line 132
    :cond_0
    return-wide v0
.end method

.method public static declared-synchronized getInstance()Lcom/vectorwatch/android/database/DatabaseManager;
    .locals 2

    .prologue
    .line 51
    const-class v1, Lcom/vectorwatch/android/database/DatabaseManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/vectorwatch/android/database/DatabaseManager;->instance:Lcom/vectorwatch/android/database/DatabaseManager;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/vectorwatch/android/database/DatabaseManager;

    invoke-direct {v0}, Lcom/vectorwatch/android/database/DatabaseManager;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/database/DatabaseManager;->instance:Lcom/vectorwatch/android/database/DatabaseManager;

    .line 54
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/database/DatabaseManager;->instance:Lcom/vectorwatch/android/database/DatabaseManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getStartTimeForGoogleFitChanges(Landroid/content/Context;)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 586
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_ACTIVITY:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "timestamp"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "activity_google_fit_dirty_cal"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "activity_google_fit_dirty_dist"

    aput-object v5, v2, v3

    const/4 v3, 0x3

    const-string v5, "activity_google_fit_dirty"

    aput-object v5, v2, v3

    const-string v3, "activity_google_fit_dirty = 1 or activity_google_fit_dirty_dist = 1 or activity_google_fit_dirty_cal = 1"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 595
    .local v6, "cursor":Landroid/database/Cursor;
    const-wide/16 v8, 0x0

    .line 596
    .local v8, "timestamp":J
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    const-string v0, "timestamp"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v8, v0

    .line 602
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 604
    return-wide v8

    .line 599
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    const-string v1, "FITNESS_API: Can\'t get first timestamp for syncing user info."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static previousEntryInDatabase(ILjava/lang/String;Landroid/net/Uri;Landroid/content/Context;)Z
    .locals 9
    .param p0, "columnValue"    # I
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "table"    # Landroid/net/Uri;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 567
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 568
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-array v2, v7, [Ljava/lang/String;

    aput-object p1, v2, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v1, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 570
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 571
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v7

    .line 576
    :goto_0
    return v1

    .line 575
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v8

    .line 576
    goto :goto_0
.end method


# virtual methods
.method public createActivityAlert(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "activityAlert"    # Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    .prologue
    .line 142
    invoke-virtual {p1}, Lio/realm/Realm;->beginTransaction()V

    .line 145
    :try_start_0
    invoke-virtual {p1, p2}, Lio/realm/Realm;->copyToRealmOrUpdate(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 146
    sget-object v1, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Create activity alert: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->getUuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->getActivityType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p1}, Lio/realm/Realm;->commitTransaction()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Realm - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p1}, Lio/realm/Realm;->cancelTransaction()V

    goto :goto_0
.end method

.method public createAlarm(Lio/realm/Realm;Ljava/lang/String;IIB)V
    .locals 8
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "hour"    # I
    .param p4, "min"    # I
    .param p5, "enabledStatus"    # B

    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/database/DatabaseManager;->getAlarmNextKey(Lio/realm/Realm;)J

    move-result-wide v2

    .line 180
    .local v2, "id":J
    invoke-virtual {p1}, Lio/realm/Realm;->beginTransaction()V

    .line 182
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->createObject(Ljava/lang/Class;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .line 183
    .local v0, "alarmToAdd":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    invoke-virtual {v0, p5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setIsEnabled(B)V

    .line 184
    invoke-virtual {v0, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setNumberOfHours(I)V

    .line 185
    invoke-virtual {v0, p4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setNumberOfMinutes(I)V

    .line 186
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setName(Ljava/lang/String;)V

    .line 187
    invoke-virtual {v0, v2, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setId(J)V

    .line 189
    sget-object v1, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ALARMS - saving alarm: ALARM_ID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | ALARM_NAME = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 190
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ALARM_TIME = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfHours()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 191
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getNumberOfMinutes()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ALARM_ENABLED = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->getIsEnabled()B

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 189
    invoke-interface {v1, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p1}, Lio/realm/Realm;->commitTransaction()V

    .line 194
    return-void
.end method

.method public deleteAlarm(Lio/realm/Realm;J)V
    .locals 6
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "id"    # J

    .prologue
    .line 66
    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v2}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "id"

    .line 67
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 68
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 70
    .local v0, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    if-eqz v0, :cond_1

    .line 71
    invoke-virtual {p1}, Lio/realm/Realm;->beginTransaction()V

    .line 74
    invoke-virtual {v0}, Lio/realm/RealmResults;->deleteAllFromRealm()Z

    move-result v1

    .line 76
    .local v1, "ret":Z
    if-nez v1, :cond_0

    .line 77
    sget-object v2, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    const-string v3, "Delete all alarms failed."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 80
    :cond_0
    invoke-virtual {p1}, Lio/realm/Realm;->commitTransaction()V

    .line 82
    .end local v1    # "ret":Z
    :cond_1
    return-void
.end method

.method public deleteAllAlarms(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 91
    const-class v2, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v2}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 93
    .local v0, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    invoke-virtual {p1}, Lio/realm/Realm;->beginTransaction()V

    .line 96
    invoke-virtual {v0}, Lio/realm/RealmResults;->deleteAllFromRealm()Z

    move-result v1

    .line 98
    .local v1, "ret":Z
    if-nez v1, :cond_0

    .line 99
    sget-object v2, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    const-string v3, "Error while removing alarms!"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 102
    :cond_0
    invoke-virtual {p1}, Lio/realm/Realm;->commitTransaction()V

    .line 103
    return-void
.end method

.method public getActivity(Lio/realm/Realm;)Ljava/util/List;
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            ">;"
        }
    .end annotation

    .prologue
    .line 359
    const-class v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 360
    .local v0, "result":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    return-object v0
.end method

.method public getActivity(Lio/realm/Realm;JJ)Ljava/util/List;
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "currentStart"    # J
    .param p4, "currentEnd"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    const-class v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "timestamp"

    .line 348
    invoke-virtual {v1, v2, p4, p5}, Lio/realm/RealmQuery;->lessThanOrEqualTo(Ljava/lang/String;J)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "timestamp"

    .line 349
    invoke-virtual {v1, v2, p2, p3}, Lio/realm/RealmQuery;->greaterThanOrEqualTo(Ljava/lang/String;J)Lio/realm/RealmQuery;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 350
    .local v0, "result":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    return-object v0
.end method

.method public getActivitySyncLastTimestamp(Lio/realm/Realm;)J
    .locals 5
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 296
    const-class v2, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v2}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "timestamp"

    sget-object v4, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 297
    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->findAllSorted(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v1

    .line 299
    .local v1, "result":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lio/realm/RealmResults;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 300
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 301
    .local v0, "lastReceivedActivity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v2

    .line 306
    .end local v0    # "lastReceivedActivity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getAlarmWithId(Lio/realm/Realm;J)Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "id"    # J

    .prologue
    .line 247
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .line 249
    .local v0, "alarm":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    return-object v0
.end method

.method public getAllActivityAlerts(Lio/realm/Realm;)Lio/realm/RealmResults;
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            ")",
            "Lio/realm/RealmResults",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "timestamp"

    sget-object v3, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 162
    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->findAllSorted(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 164
    .local v0, "result":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;>;"
    return-object v0
.end method

.method public getAllActivityForCloudSync(Lio/realm/Realm;)Ljava/util/List;
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            ">;"
        }
    .end annotation

    .prologue
    .line 315
    const-class v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "dirtyCloud"

    const/4 v3, 0x1

    .line 316
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/RealmQuery;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 318
    .local v0, "result":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    return-object v0
.end method

.method public getAllAlarms(Lio/realm/Realm;)Ljava/util/List;
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 114
    .local v0, "alarms":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;>;"
    return-object v0
.end method

.method public getEntriesForGoogleFit(Lio/realm/Realm;Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)Ljava/util/List;
    .locals 13
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "activityType"    # Lcom/vectorwatch/android/utils/Constants$GoogleFitData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/vectorwatch/android/utils/Constants$GoogleFitData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/ActivityValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    .line 422
    const/4 v6, 0x0

    .line 425
    .local v6, "result":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    sget-object v10, Lcom/vectorwatch/android/database/DatabaseManager$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoogleFitData:[I

    invoke-virtual {p2}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 436
    sget-object v10, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    const-string v11, "FITNESS_API: Unknown activity type. (get entries for google fit)."

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 437
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 487
    :cond_0
    :goto_0
    return-object v0

    .line 427
    :pswitch_0
    const-class v10, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v10}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v10

    const-string v11, "dirtySteps"

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/RealmQuery;

    move-result-object v10

    invoke-virtual {v10}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 440
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 442
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/ActivityValueModel;>;"
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lio/realm/RealmResults;->size()I

    move-result v10

    if-lez v10, :cond_0

    .line 444
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    invoke-virtual {v6}, Lio/realm/RealmResults;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x4

    if-ge v5, v10, :cond_0

    .line 445
    invoke-virtual {v6, v5}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 449
    .local v4, "entry":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v8

    .line 450
    .local v8, "startTime":J
    const-wide/16 v10, 0x384

    add-long v2, v8, v10

    .line 452
    .local v2, "endTime":J
    new-instance v1, Lcom/vectorwatch/android/models/ActivityValueModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/ActivityValueModel;-><init>()V

    .line 453
    .local v1, "activityValueModel":Lcom/vectorwatch/android/models/ActivityValueModel;
    invoke-virtual {v1, v8, v9}, Lcom/vectorwatch/android/models/ActivityValueModel;->setStartTime(J)V

    .line 454
    invoke-virtual {v1, v2, v3}, Lcom/vectorwatch/android/models/ActivityValueModel;->setEndTime(J)V

    .line 456
    sget-object v10, Lcom/vectorwatch/android/database/DatabaseManager$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoogleFitData:[I

    invoke-virtual {p2}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_1

    .line 475
    invoke-virtual {p1}, Lio/realm/Realm;->close()V

    .line 476
    sget-object v10, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    const-string v11, "FITNESS_API: Unknown activity type. (get entries for google fit)."

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 477
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/ActivityValueModel;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 430
    .end local v1    # "activityValueModel":Lcom/vectorwatch/android/models/ActivityValueModel;
    .end local v2    # "endTime":J
    .end local v4    # "entry":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .end local v5    # "i":I
    .end local v8    # "startTime":J
    :pswitch_1
    const-class v10, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v10}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v10

    const-string v11, "dirtyCalories"

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/RealmQuery;

    move-result-object v10

    invoke-virtual {v10}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 431
    goto :goto_1

    .line 433
    :pswitch_2
    const-class v10, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {p1, v10}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v10

    const-string v11, "dirtyDistance"

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/RealmQuery;

    move-result-object v10

    invoke-virtual {v10}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 434
    goto :goto_1

    .line 458
    .restart local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/ActivityValueModel;>;"
    .restart local v1    # "activityValueModel":Lcom/vectorwatch/android/models/ActivityValueModel;
    .restart local v2    # "endTime":J
    .restart local v4    # "entry":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .restart local v5    # "i":I
    .restart local v8    # "startTime":J
    :pswitch_3
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getVal()I

    move-result v7

    .line 459
    .local v7, "value":I
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->STEPS:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-virtual {v1, v10}, Lcom/vectorwatch/android/models/ActivityValueModel;->setActivityType(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 460
    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/models/ActivityValueModel;->setValue(I)V

    .line 480
    :goto_3
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 444
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 464
    .end local v7    # "value":I
    :pswitch_4
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getDist()I

    move-result v7

    .line 465
    .restart local v7    # "value":I
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->DISTANCE:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-virtual {v1, v10}, Lcom/vectorwatch/android/models/ActivityValueModel;->setActivityType(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 466
    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/models/ActivityValueModel;->setValue(I)V

    goto :goto_3

    .line 470
    .end local v7    # "value":I
    :pswitch_5
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getCal()I

    move-result v7

    .line 471
    .restart local v7    # "value":I
    sget-object v10, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->CALORIES:Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    invoke-virtual {v1, v10}, Lcom/vectorwatch/android/models/ActivityValueModel;->setActivityType(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V

    .line 472
    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/models/ActivityValueModel;->setValue(I)V

    goto :goto_3

    .line 425
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 456
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public insertActivityData(Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;)V
    .locals 6
    .param p1, "activityDay"    # Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .prologue
    .line 280
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v0

    .line 281
    .local v0, "realm":Lio/realm/Realm;
    sget-object v1, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Activity - Insert activity data "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 282
    if-eqz p1, :cond_0

    .line 283
    invoke-virtual {v0}, Lio/realm/Realm;->beginTransaction()V

    .line 284
    invoke-virtual {v0, p1}, Lio/realm/Realm;->copyToRealmOrUpdate(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 285
    invoke-virtual {v0}, Lio/realm/Realm;->commitTransaction()V

    .line 287
    :cond_0
    invoke-virtual {v0}, Lio/realm/Realm;->close()V

    .line 288
    return-void
.end method

.method public insertActivityDataList(Lio/realm/Realm;Ljava/util/List;)V
    .locals 8
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 261
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    sget-object v3, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Activity - Insert activity data "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " elements"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 262
    if-eqz p2, :cond_1

    .line 263
    invoke-virtual {p1}, Lio/realm/Realm;->beginTransaction()V

    .line 264
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 265
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 266
    .local v1, "dataItem":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setTimestamp(J)V

    .line 267
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/RealmModel;

    invoke-virtual {p1, v3}, Lio/realm/Realm;->copyToRealmOrUpdate(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 268
    .local v0, "activityDay":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    sget-object v3, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Activity - Insert after: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 264
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 270
    .end local v0    # "activityDay":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .end local v1    # "dataItem":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :cond_0
    invoke-virtual {p1}, Lio/realm/Realm;->commitTransaction()V

    .line 272
    .end local v2    # "i":I
    :cond_1
    return-void
.end method

.method public insertSleepList(Landroid/content/Context;Ljava/util/List;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudSleep;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 525
    .local p2, "sleep":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudSleep;>;"
    if-eqz p2, :cond_3

    .line 526
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 527
    .local v2, "resolver":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 528
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 530
    .local v3, "valuesSleepData":Landroid/content/ContentValues;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 531
    .local v0, "calendar":Ljava/util/Calendar;
    new-instance v5, Ljava/util/Date;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getId()Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->getStartTs()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 532
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/16 v5, 0x14

    if-ge v4, v5, :cond_0

    .line 533
    const/4 v4, 0x6

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 535
    :cond_0
    const/16 v4, 0xb

    const/16 v5, 0x14

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 536
    const/16 v4, 0xc

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 537
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getEndTs()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v6, v4, v6

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getId()Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->getStartTs()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    div-long/2addr v4, v8

    sub-long v4, v6, v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 538
    sget-object v5, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SyncActivity: Sleep: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getId()Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    move-result-object v4

    .line 539
    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->getStartTs()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getEndTs()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 538
    invoke-interface {v5, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 541
    const-string v5, "start_time"

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getId()Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->getStartTs()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 542
    const-string v5, "stop_time"

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getEndTs()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 543
    const-string v4, "timestamp"

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    long-to-int v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 544
    const-string v4, "dirty_field"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 546
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getId()Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->getStartTs()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v4, v4

    const-string v5, "start_time"

    sget-object v6, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    invoke-static {v4, v5, v6, p1}, Lcom/vectorwatch/android/database/DatabaseManager;->previousEntryInDatabase(ILjava/lang/String;Landroid/net/Uri;Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 548
    sget-object v5, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "start_time="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 549
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleep;->getId()Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/chart/model/CloudSleepId;->getStartTs()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v4, v8

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    .line 548
    invoke-virtual {v2, v5, v3, v4, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 527
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 551
    :cond_2
    sget-object v4, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1

    .line 556
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v1    # "i":I
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v3    # "valuesSleepData":Landroid/content/ContentValues;
    :cond_3
    return-void
.end method

.method public setGoogleFitDirtyFlagToFalse(Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V
    .locals 8
    .param p1, "type"    # Lcom/vectorwatch/android/utils/Constants$GoogleFitData;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 369
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v2

    .line 371
    .local v2, "realm":Lio/realm/Realm;
    const/4 v3, 0x0

    .line 374
    .local v3, "result":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    sget-object v4, Lcom/vectorwatch/android/database/DatabaseManager$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoogleFitData:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 386
    :goto_0
    if-nez v3, :cond_0

    .line 387
    sget-object v4, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    const-string v5, "FITNESS_API: Didn\'t find any dirty fields at set google fit dirty flag to false."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 388
    invoke-virtual {v2}, Lio/realm/Realm;->close()V

    .line 413
    :goto_1
    return-void

    .line 376
    :pswitch_0
    const-class v4, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v2, v4}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v4

    const-string v5, "dirtySteps"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/RealmQuery;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v3

    .line 377
    goto :goto_0

    .line 379
    :pswitch_1
    const-class v4, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v2, v4}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v4

    const-string v5, "dirtyDistance"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/RealmQuery;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v3

    .line 380
    goto :goto_0

    .line 382
    :pswitch_2
    const-class v4, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v2, v4}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v4

    const-string v5, "dirtyCalories"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Boolean;)Lio/realm/RealmQuery;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v3

    goto :goto_0

    .line 393
    :cond_0
    invoke-virtual {v2}, Lio/realm/Realm;->beginTransaction()V

    .line 395
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    invoke-virtual {v3}, Lio/realm/RealmResults;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    .line 397
    invoke-virtual {v3, v1}, Lio/realm/RealmResults;->get(I)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 399
    .local v0, "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    sget-object v4, Lcom/vectorwatch/android/database/DatabaseManager$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoogleFitData:[I

    invoke-virtual {p1}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 395
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 401
    :pswitch_3
    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtySteps(Z)V

    goto :goto_3

    .line 404
    :pswitch_4
    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyDistance(Z)V

    goto :goto_3

    .line 407
    :pswitch_5
    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyCalories(Z)V

    goto :goto_3

    .line 411
    .end local v0    # "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :cond_1
    invoke-virtual {v2}, Lio/realm/Realm;->commitTransaction()V

    .line 412
    invoke-virtual {v2}, Lio/realm/Realm;->close()V

    goto :goto_1

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 399
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public updateAlarm(Lio/realm/Realm;Ljava/lang/String;IIBJ)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "hour"    # I
    .param p4, "min"    # I
    .param p5, "enabledStatus"    # B
    .param p6, "id"    # J

    .prologue
    .line 206
    sget-object v1, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ALARMS - Updating alarm name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " hour = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 208
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    .line 209
    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .line 211
    .local v0, "alarmToEdit":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {p1}, Lio/realm/Realm;->beginTransaction()V

    .line 213
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setName(Ljava/lang/String;)V

    .line 214
    invoke-virtual {v0, p3}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setNumberOfHours(I)V

    .line 215
    invoke-virtual {v0, p4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setNumberOfMinutes(I)V

    .line 216
    invoke-virtual {v0, p5}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setIsEnabled(B)V

    .line 217
    invoke-virtual {p1}, Lio/realm/Realm;->commitTransaction()V

    .line 219
    :cond_0
    return-void
.end method

.method public updateAlarmStatus(Lio/realm/Realm;JB)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "id"    # J
    .param p4, "enabledValue"    # B

    .prologue
    .line 229
    sget-object v1, Lcom/vectorwatch/android/database/DatabaseManager;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ALARMS - Updating alarm id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 231
    const-class v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    .line 232
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;

    .line 234
    .local v0, "alarmToEdit":Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;
    invoke-virtual {p1}, Lio/realm/Realm;->beginTransaction()V

    .line 235
    invoke-virtual {v0, p4}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/Alarm;->setIsEnabled(B)V

    .line 236
    invoke-virtual {p1}, Lio/realm/Realm;->commitTransaction()V

    .line 237
    return-void
.end method

.method public updateCloudDirty(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 327
    .local p1, "timestampsToClear":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v1

    .line 329
    .local v1, "realm":Lio/realm/Realm;
    invoke-virtual {v1}, Lio/realm/Realm;->beginTransaction()V

    .line 330
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 331
    .local v2, "start":Ljava/lang/Long;
    const-class v4, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v1, v4}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v4

    const-string v5, "timestamp"

    invoke-virtual {v4, v5, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 332
    .local v0, "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyCloud(Z)V

    goto :goto_0

    .line 334
    .end local v0    # "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .end local v2    # "start":Ljava/lang/Long;
    :cond_0
    invoke-virtual {v1}, Lio/realm/Realm;->commitTransaction()V

    .line 335
    invoke-virtual {v1}, Lio/realm/Realm;->close()V

    .line 336
    return-void
.end method

.method public updateDirtyFieldForActivityInDb(Ljava/util/List;Lcom/vectorwatch/android/utils/Constants$GoogleFitData;)V
    .locals 7
    .param p2, "type"    # Lcom/vectorwatch/android/utils/Constants$GoogleFitData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/vectorwatch/android/utils/Constants$GoogleFitData;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "startTimes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v6, 0x0

    .line 497
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v1

    .line 499
    .local v1, "realm":Lio/realm/Realm;
    invoke-virtual {v1}, Lio/realm/Realm;->beginTransaction()V

    .line 500
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 501
    .local v2, "start":Ljava/lang/Long;
    const-class v4, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    invoke-virtual {v1, v4}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v4

    const-string v5, "timestamp"

    invoke-virtual {v4, v5, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v4

    invoke-virtual {v4}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 502
    .local v0, "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    sget-object v4, Lcom/vectorwatch/android/database/DatabaseManager$1;->$SwitchMap$com$vectorwatch$android$utils$Constants$GoogleFitData:[I

    invoke-virtual {p2}, Lcom/vectorwatch/android/utils/Constants$GoogleFitData;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 504
    :pswitch_0
    invoke-virtual {v0, v6}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtySteps(Z)V

    goto :goto_0

    .line 507
    :pswitch_1
    invoke-virtual {v0, v6}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyCalories(Z)V

    goto :goto_0

    .line 510
    :pswitch_2
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->setDirtyDistance(Z)V

    goto :goto_0

    .line 514
    .end local v0    # "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    .end local v2    # "start":Ljava/lang/Long;
    :cond_0
    invoke-virtual {v1}, Lio/realm/Realm;->commitTransaction()V

    .line 515
    invoke-virtual {v1}, Lio/realm/Realm;->close()V

    .line 516
    return-void

    .line 502
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
