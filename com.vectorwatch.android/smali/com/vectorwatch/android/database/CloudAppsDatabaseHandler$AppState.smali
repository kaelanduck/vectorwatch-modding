.class public final enum Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;
.super Ljava/lang/Enum;
.source "CloudAppsDatabaseHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

.field public static final enum CHANGE_TO_LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

.field public static final enum CHANGE_TO_RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

.field public static final enum DISABLED:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

.field public static final enum LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

.field public static final enum RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

.field public static final enum UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1235
    new-instance v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    const-string v1, "LOCKER"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    new-instance v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    new-instance v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    const-string v1, "UNINSTALL"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    new-instance v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->DISABLED:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    new-instance v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    const-string v1, "CHANGE_TO_RUNNING"

    invoke-direct {v0, v1, v7}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    new-instance v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    const-string v1, "CHANGE_TO_LOCKER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    .line 1234
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->DISABLED:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->$VALUES:[Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1234
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1234
    const-class v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;
    .locals 1

    .prologue
    .line 1234
    sget-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->$VALUES:[Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    return-object v0
.end method
