.class public Lcom/vectorwatch/android/database/StreamsDatabaseHandler;
.super Ljava/lang/Object;
.source "StreamsDatabaseHandler.java"


# static fields
.field public static final MODIFIED:I = 0x1

.field public static final NOT_DEFINED:I = -0x2

.field public static final NOT_MODIFIED:I

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static activateStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILandroid/content/Context;)V
    .locals 15
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "streamChannelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;
    .param p2, "appId"    # I
    .param p3, "watchfaceId"    # I
    .param p4, "elementId"    # I
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 344
    if-eqz p1, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v3

    .line 346
    .local v3, "channelLabelString":Ljava/lang/String;
    :goto_0
    sget-object v12, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Setup - Activate stream with name = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " on ( "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " ) with channel label = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " type = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 348
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 346
    invoke-interface {v12, v13}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 350
    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 352
    .local v8, "resolver":Landroid/content/ContentResolver;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 353
    .local v11, "values":Landroid/content/ContentValues;
    const-string v12, "stream_name"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v12, "stream_description"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getDescription()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v12, "stream_img"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getImg()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v12, "stream_version"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 357
    const-string v12, "stream_type"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v12, "stream_supported_type"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getSupportedType()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const-string v12, "stream_id"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getId()Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 360
    const-string v12, "stream_uuid"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string v12, "stream_label"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getLabelText()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string v12, "stream_cont_version"

    iget-object v13, p0, Lcom/vectorwatch/android/models/Stream;->contentVersion:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v9

    .line 364
    .local v9, "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    new-instance v6, Lcom/google/gson/Gson;

    invoke-direct {v6}, Lcom/google/gson/Gson;-><init>()V

    .line 365
    .local v6, "gsonPossibleSettings":Lcom/google/gson/Gson;
    invoke-virtual {v6, v9}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 366
    .local v10, "streamSettingsJsonAsString":Ljava/lang/String;
    const-string v12, "stream_possible_settings"

    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 368
    const-string v12, "stream_active_app"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 369
    const-string v12, "stream_active_face"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 370
    const-string v12, "stream_active_field"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 371
    invoke-static/range {p5 .. p5}, Lcom/vectorwatch/android/utils/NetworkUtils;->isOnline(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 372
    const-string v12, "stream_state"

    sget-object v13, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    invoke-virtual {v13}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    :goto_1
    if-nez p1, :cond_7

    .line 378
    sget-object v12, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v13, "At activate stream channel settings is null"

    invoke-interface {v12, v13}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 380
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_1

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/vectorwatch/android/models/Stream$StreamTypes;->WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 381
    invoke-virtual {v13}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v13

    .line 380
    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 382
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 383
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 384
    const-string v12, "stream_channel_label"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v13

    .line 385
    invoke-virtual {v13}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v13

    .line 384
    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    if-eqz v12, :cond_1

    .line 390
    new-instance v7, Lcom/google/gson/Gson;

    invoke-direct {v7}, Lcom/google/gson/Gson;-><init>()V

    .line 393
    .local v7, "gsonSettings":Lcom/google/gson/Gson;
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v4

    .line 394
    .local v4, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    invoke-virtual {v7, v4}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 395
    .local v5, "channelSettingsAsString":Ljava/lang/String;
    const-string v12, "stream_settings"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 400
    .end local v4    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v5    # "channelSettingsAsString":Ljava/lang/String;
    .end local v7    # "gsonSettings":Lcom/google/gson/Gson;
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_2

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/vectorwatch/android/models/Stream$StreamTypes;->WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 401
    invoke-virtual {v13}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v13

    .line 400
    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 405
    const-string v12, "stream_channel_label"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ":"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    :cond_2
    :goto_2
    const-string v12, "needs_auth"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 425
    invoke-static/range {p2 .. p5}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getActiveStream(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v2

    .line 427
    .local v2, "activeStream":Lcom/vectorwatch/android/models/Stream;
    if-nez v2, :cond_8

    .line 428
    sget-object v12, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    invoke-virtual {v8, v12, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 437
    :goto_3
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInSetupMode()Z

    move-result v12

    if-nez v12, :cond_3

    .line 440
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getLiveStreamManager()Lcom/vectorwatch/android/managers/LiveStreamManager;

    move-result-object v12

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v12, v0, v1}, Lcom/vectorwatch/android/managers/LiveStreamManager;->refreshLiveStreamManager(II)V

    .line 443
    :cond_3
    return-void

    .line 344
    .end local v2    # "activeStream":Lcom/vectorwatch/android/models/Stream;
    .end local v3    # "channelLabelString":Ljava/lang/String;
    .end local v6    # "gsonPossibleSettings":Lcom/google/gson/Gson;
    .end local v8    # "resolver":Landroid/content/ContentResolver;
    .end local v9    # "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .end local v10    # "streamSettingsJsonAsString":Ljava/lang/String;
    .end local v11    # "values":Landroid/content/ContentValues;
    :cond_4
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    if-eqz v12, :cond_5

    .line 345
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v12

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_5
    const-string v3, " null"

    goto/16 :goto_0

    .line 374
    .restart local v3    # "channelLabelString":Ljava/lang/String;
    .restart local v6    # "gsonPossibleSettings":Lcom/google/gson/Gson;
    .restart local v8    # "resolver":Landroid/content/ContentResolver;
    .restart local v9    # "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .restart local v10    # "streamSettingsJsonAsString":Ljava/lang/String;
    .restart local v11    # "values":Landroid/content/ContentValues;
    :cond_6
    const-string v12, "stream_state"

    sget-object v13, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_SUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

    invoke-virtual {v13}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 411
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_2

    .line 412
    const-string v12, "stream_channel_label"

    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    new-instance v7, Lcom/google/gson/Gson;

    invoke-direct {v7}, Lcom/google/gson/Gson;-><init>()V

    .line 416
    .restart local v7    # "gsonSettings":Lcom/google/gson/Gson;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 417
    .restart local v5    # "channelSettingsAsString":Ljava/lang/String;
    sget-object v12, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Setup - stream settings = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 418
    const-string v12, "stream_settings"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto/16 :goto_2

    .line 431
    .end local v5    # "channelSettingsAsString":Ljava/lang/String;
    .end local v7    # "gsonSettings":Lcom/google/gson/Gson;
    .restart local v2    # "activeStream":Lcom/vectorwatch/android/models/Stream;
    :cond_8
    sget-object v12, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "stream_active_app = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " and "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "stream_active_face"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " and "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "stream_active_field"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v8, v12, v11, v13, v14}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_3
.end method

.method public static cleanDirtyFieldForPlacement(IIILandroid/content/Context;)V
    .locals 6
    .param p0, "appId"    # I
    .param p1, "faceId"    # I
    .param p2, "elementId"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 1330
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1331
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1332
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "stream_dirty"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1334
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_active_app = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_active_face"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_active_field"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1338
    .local v1, "rowsChangedCount":I
    sget-object v3, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Marked as unmodified for app = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " face = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " element = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1339
    return-void
.end method

.method public static deactivateStream(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)V
    .locals 6
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .param p3, "elementId"    # I
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, -0x2

    .line 446
    invoke-virtual {p4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 448
    .local v1, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 449
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "stream_state"

    sget-object v4, Lcom/vectorwatch/android/models/Stream$State;->LOCKER:Lcom/vectorwatch/android/models/Stream$State;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const-string v3, "stream_active_app"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 451
    const-string v3, "stream_active_face"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 452
    const-string v3, "stream_active_field"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 456
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_uuid = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 457
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_active_app"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_active_face"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_active_field"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_state"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_UNSUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

    .line 461
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 456
    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 463
    .local v0, "countChangedRows":I
    return-void
.end method

.method public static getActiveLiveStreamsForAppAndWatchface(Landroid/content/Context;II)Ljava/util/List;
    .locals 30
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # I
    .param p2, "watchfaceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 750
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 751
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/16 v4, 0x12

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "stream_state"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "stream_type"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "stream_id"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "stream_possible_settings"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "stream_img"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "stream_description"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "stream_name"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "stream_uuid"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "stream_label"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "stream_cont_version"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "stream_possible_settings"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const-string v6, "stream_supported_type"

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "stream_active_face"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    const-string v6, "stream_active_app"

    aput-object v6, v4, v5

    const/16 v5, 0xe

    const-string v6, "stream_active_field"

    aput-object v6, v4, v5

    const/16 v5, 0xf

    const-string v6, "stream_settings"

    aput-object v6, v4, v5

    const/16 v5, 0x10

    const-string v6, "auth_invalidated"

    aput-object v6, v4, v5

    const/16 v5, 0x11

    const-string v6, "needs_auth"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stream_state=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 760
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "stream_active_app"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "stream_active_face"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 751
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 764
    .local v13, "cursor":Landroid/database/Cursor;
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 780
    .local v25, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    if-eqz v13, :cond_4

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 783
    :cond_0
    const-string v3, "stream_name"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 784
    .local v26, "streamName":Ljava/lang/String;
    const-string v3, "stream_description"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 785
    .local v21, "streamDescription":Ljava/lang/String;
    const-string v3, "stream_img"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 786
    .local v23, "streamImg":Ljava/lang/String;
    const-string v3, "stream_type"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 787
    .local v28, "streamType":Ljava/lang/String;
    const-string v3, "stream_uuid"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 788
    .local v29, "streamUuid":Ljava/lang/String;
    const-string v3, "stream_label"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 789
    .local v24, "streamLabel":Ljava/lang/String;
    const-string v3, "stream_cont_version"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 790
    .local v20, "streamContVersion":Ljava/lang/String;
    const-string v3, "stream_id"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 791
    .local v22, "streamId":I
    const-string v3, "stream_supported_type"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 793
    .local v27, "streamSupportedType":Ljava/lang/String;
    const-string v3, "stream_possible_settings"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 794
    .local v11, "content":[B
    const-string v3, "stream_settings"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    .line 795
    .local v9, "channelSettings":[B
    const-string v3, "needs_auth"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_6

    const/4 v15, 0x1

    .line 796
    .local v15, "needsAuth":Z
    :goto_0
    const-string v3, "auth_invalidated"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_7

    const/4 v8, 0x1

    .line 799
    .local v8, "authInvalidated":Z
    :goto_1
    new-instance v18, Lcom/vectorwatch/android/models/Stream;

    invoke-direct/range {v18 .. v18}, Lcom/vectorwatch/android/models/Stream;-><init>()V

    .line 800
    .local v18, "stream":Lcom/vectorwatch/android/models/Stream;
    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setName(Ljava/lang/String;)V

    .line 801
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setDescription(Ljava/lang/String;)V

    .line 802
    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setImg(Ljava/lang/String;)V

    .line 803
    move-object/from16 v0, v18

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setStreamType(Ljava/lang/String;)V

    .line 804
    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setUuid(Ljava/lang/String;)V

    .line 805
    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setLabelText(Ljava/lang/String;)V

    .line 806
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setContentVersion(Ljava/lang/String;)V

    .line 807
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/Stream;->setId(Ljava/lang/Integer;)V

    .line 808
    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setSupportedType(Ljava/lang/String;)V

    .line 809
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/vectorwatch/android/models/Stream;->setRequireUserAuth(Z)V

    .line 810
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/Stream;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 812
    new-instance v14, Lcom/google/gson/Gson;

    invoke-direct {v14}, Lcom/google/gson/Gson;-><init>()V

    .line 814
    .local v14, "gson":Lcom/google/gson/Gson;
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v11}, Ljava/lang/String;-><init>([B)V

    .line 816
    .local v12, "contentString":Ljava/lang/String;
    const-class v3, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    invoke-virtual {v14, v12, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .line 817
    .local v17, "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setStreamSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V

    .line 820
    new-instance v14, Lcom/google/gson/Gson;

    .end local v14    # "gson":Lcom/google/gson/Gson;
    invoke-direct {v14}, Lcom/google/gson/Gson;-><init>()V

    .line 821
    .restart local v14    # "gson":Lcom/google/gson/Gson;
    if-eqz v9, :cond_1

    .line 822
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v9}, Ljava/lang/String;-><init>([B)V

    .line 823
    .local v10, "channelSettingsString":Ljava/lang/String;
    const-class v3, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-virtual {v14, v10, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 825
    .local v19, "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    invoke-virtual/range {v18 .. v19}, Lcom/vectorwatch/android/models/Stream;->setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 828
    .end local v10    # "channelSettingsString":Ljava/lang/String;
    .end local v19    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    :cond_1
    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 829
    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUserSettings()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vectorwatch/android/models/settings/Setting;

    .line 830
    .local v16, "setting":Lcom/vectorwatch/android/models/settings/Setting;
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    if-eqz v4, :cond_2

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/vectorwatch/android/models/settings/Setting;->type:Ljava/lang/String;

    sget-object v5, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->LIVE_STREAM:Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$SettingsDefaultTypes;->getVal()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 831
    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 836
    .end local v16    # "setting":Lcom/vectorwatch/android/models/settings/Setting;
    :cond_3
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 839
    .end local v8    # "authInvalidated":Z
    .end local v9    # "channelSettings":[B
    .end local v11    # "content":[B
    .end local v12    # "contentString":Ljava/lang/String;
    .end local v14    # "gson":Lcom/google/gson/Gson;
    .end local v15    # "needsAuth":Z
    .end local v17    # "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .end local v18    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v20    # "streamContVersion":Ljava/lang/String;
    .end local v21    # "streamDescription":Ljava/lang/String;
    .end local v22    # "streamId":I
    .end local v23    # "streamImg":Ljava/lang/String;
    .end local v24    # "streamLabel":Ljava/lang/String;
    .end local v26    # "streamName":Ljava/lang/String;
    .end local v27    # "streamSupportedType":Ljava/lang/String;
    .end local v28    # "streamType":Ljava/lang/String;
    .end local v29    # "streamUuid":Ljava/lang/String;
    :cond_4
    if-eqz v13, :cond_5

    .line 840
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 843
    :cond_5
    return-object v25

    .line 795
    .restart local v9    # "channelSettings":[B
    .restart local v11    # "content":[B
    .restart local v20    # "streamContVersion":Ljava/lang/String;
    .restart local v21    # "streamDescription":Ljava/lang/String;
    .restart local v22    # "streamId":I
    .restart local v23    # "streamImg":Ljava/lang/String;
    .restart local v24    # "streamLabel":Ljava/lang/String;
    .restart local v26    # "streamName":Ljava/lang/String;
    .restart local v27    # "streamSupportedType":Ljava/lang/String;
    .restart local v28    # "streamType":Ljava/lang/String;
    .restart local v29    # "streamUuid":Ljava/lang/String;
    :cond_6
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 796
    .restart local v15    # "needsAuth":Z
    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_1
.end method

.method public static getActiveStream(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;
    .locals 30
    .param p0, "appId"    # I
    .param p1, "faceId"    # I
    .param p2, "elementId"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 857
    sget-object v3, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Looking for active stream on ( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 859
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 860
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/16 v4, 0x13

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "stream_state"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "stream_type"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "stream_id"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "stream_version"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "stream_possible_settings"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "stream_img"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "stream_description"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "stream_name"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "stream_uuid"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "stream_label"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "stream_cont_version"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const-string v6, "stream_possible_settings"

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "stream_supported_type"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    const-string v6, "stream_active_face"

    aput-object v6, v4, v5

    const/16 v5, 0xe

    const-string v6, "stream_active_app"

    aput-object v6, v4, v5

    const/16 v5, 0xf

    const-string v6, "stream_active_field"

    aput-object v6, v4, v5

    const/16 v5, 0x10

    const-string v6, "stream_settings"

    aput-object v6, v4, v5

    const/16 v5, 0x11

    const-string v6, "auth_invalidated"

    aput-object v6, v4, v5

    const/16 v5, 0x12

    const-string v6, "needs_auth"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stream_state=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 880
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "stream_active_app"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "stream_active_face"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "stream_active_field"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 860
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 885
    .local v13, "cursor":Landroid/database/Cursor;
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 902
    .local v24, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    if-eqz v13, :cond_2

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 905
    :cond_0
    const-string v3, "stream_name"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 906
    .local v25, "streamName":Ljava/lang/String;
    const-string v3, "stream_description"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 907
    .local v20, "streamDescription":Ljava/lang/String;
    const-string v3, "stream_img"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 908
    .local v22, "streamImg":Ljava/lang/String;
    const-string v3, "stream_version"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    .line 909
    .local v29, "streamVersion":Ljava/lang/Integer;
    const-string v3, "stream_type"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 910
    .local v27, "streamType":Ljava/lang/String;
    const-string v3, "stream_uuid"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 911
    .local v28, "streamUuid":Ljava/lang/String;
    const-string v3, "stream_label"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 912
    .local v23, "streamLabel":Ljava/lang/String;
    const-string v3, "stream_cont_version"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 913
    .local v19, "streamContVersion":Ljava/lang/String;
    const-string v3, "stream_id"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 914
    .local v21, "streamId":I
    const-string v3, "stream_supported_type"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 916
    .local v26, "streamSupportedType":Ljava/lang/String;
    const-string v3, "stream_possible_settings"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 917
    .local v11, "content":[B
    const-string v3, "stream_settings"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    .line 918
    .local v9, "channelSettings":[B
    const-string v3, "needs_auth"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_4

    const/4 v15, 0x1

    .line 919
    .local v15, "needsAuth":Z
    :goto_0
    const-string v3, "auth_invalidated"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_5

    const/4 v8, 0x1

    .line 922
    .local v8, "authInvalidated":Z
    :goto_1
    new-instance v17, Lcom/vectorwatch/android/models/Stream;

    invoke-direct/range {v17 .. v17}, Lcom/vectorwatch/android/models/Stream;-><init>()V

    .line 923
    .local v17, "stream":Lcom/vectorwatch/android/models/Stream;
    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setName(Ljava/lang/String;)V

    .line 924
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setDescription(Ljava/lang/String;)V

    .line 925
    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setImg(Ljava/lang/String;)V

    .line 926
    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setStreamType(Ljava/lang/String;)V

    .line 927
    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setUuid(Ljava/lang/String;)V

    .line 928
    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setLabelText(Ljava/lang/String;)V

    .line 929
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setContentVersion(Ljava/lang/String;)V

    .line 930
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/Stream;->setId(Ljava/lang/Integer;)V

    .line 931
    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setSupportedType(Ljava/lang/String;)V

    .line 932
    move-object/from16 v0, v17

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setVersion(Ljava/lang/Integer;)V

    .line 933
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/vectorwatch/android/models/Stream;->setRequireUserAuth(Z)V

    .line 934
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/Stream;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 936
    new-instance v14, Lcom/google/gson/Gson;

    invoke-direct {v14}, Lcom/google/gson/Gson;-><init>()V

    .line 938
    .local v14, "gson":Lcom/google/gson/Gson;
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v11}, Ljava/lang/String;-><init>([B)V

    .line 940
    .local v12, "contentString":Ljava/lang/String;
    const-class v3, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    invoke-virtual {v14, v12, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .line 941
    .local v16, "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setStreamSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V

    .line 943
    if-eqz v9, :cond_1

    .line 944
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v9}, Ljava/lang/String;-><init>([B)V

    .line 945
    .local v10, "channelSettingsString":Ljava/lang/String;
    const-class v3, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-virtual {v14, v10, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 947
    .local v18, "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    invoke-virtual/range {v17 .. v18}, Lcom/vectorwatch/android/models/Stream;->setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 950
    .end local v10    # "channelSettingsString":Ljava/lang/String;
    .end local v18    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    :cond_1
    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 951
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 954
    .end local v8    # "authInvalidated":Z
    .end local v9    # "channelSettings":[B
    .end local v11    # "content":[B
    .end local v12    # "contentString":Ljava/lang/String;
    .end local v14    # "gson":Lcom/google/gson/Gson;
    .end local v15    # "needsAuth":Z
    .end local v16    # "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .end local v17    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v19    # "streamContVersion":Ljava/lang/String;
    .end local v20    # "streamDescription":Ljava/lang/String;
    .end local v21    # "streamId":I
    .end local v22    # "streamImg":Ljava/lang/String;
    .end local v23    # "streamLabel":Ljava/lang/String;
    .end local v25    # "streamName":Ljava/lang/String;
    .end local v26    # "streamSupportedType":Ljava/lang/String;
    .end local v27    # "streamType":Ljava/lang/String;
    .end local v28    # "streamUuid":Ljava/lang/String;
    .end local v29    # "streamVersion":Ljava/lang/Integer;
    :cond_2
    if-eqz v13, :cond_3

    .line 955
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 958
    :cond_3
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_6

    .line 959
    const/4 v3, 0x0

    .line 972
    :goto_2
    return-object v3

    .line 918
    .restart local v9    # "channelSettings":[B
    .restart local v11    # "content":[B
    .restart local v19    # "streamContVersion":Ljava/lang/String;
    .restart local v20    # "streamDescription":Ljava/lang/String;
    .restart local v21    # "streamId":I
    .restart local v22    # "streamImg":Ljava/lang/String;
    .restart local v23    # "streamLabel":Ljava/lang/String;
    .restart local v25    # "streamName":Ljava/lang/String;
    .restart local v26    # "streamSupportedType":Ljava/lang/String;
    .restart local v27    # "streamType":Ljava/lang/String;
    .restart local v28    # "streamUuid":Ljava/lang/String;
    .restart local v29    # "streamVersion":Ljava/lang/Integer;
    :cond_4
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 919
    .restart local v15    # "needsAuth":Z
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 961
    .end local v9    # "channelSettings":[B
    .end local v11    # "content":[B
    .end local v15    # "needsAuth":Z
    .end local v19    # "streamContVersion":Ljava/lang/String;
    .end local v20    # "streamDescription":Ljava/lang/String;
    .end local v21    # "streamId":I
    .end local v22    # "streamImg":Ljava/lang/String;
    .end local v23    # "streamLabel":Ljava/lang/String;
    .end local v25    # "streamName":Ljava/lang/String;
    .end local v26    # "streamSupportedType":Ljava/lang/String;
    .end local v27    # "streamType":Ljava/lang/String;
    .end local v28    # "streamUuid":Ljava/lang/String;
    .end local v29    # "streamVersion":Ljava/lang/Integer;
    :cond_6
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_7

    .line 962
    sget-object v3, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "There should be just one installed stream on a given face from one given app at a given element at the same time."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 964
    const/4 v3, 0x0

    goto :goto_2

    .line 968
    :cond_7
    if-eqz v24, :cond_8

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_8

    .line 969
    const/4 v3, 0x0

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/Stream;

    goto :goto_2

    .line 972
    :cond_8
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public static getActiveStreamChannelLabel(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "appId"    # I
    .param p2, "faceId"    # I
    .param p3, "elementId"    # I
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 1011
    invoke-virtual {p4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1012
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "stream_state"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "stream_channel_label"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_state=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 1014
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_uuid"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1015
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_active_app"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_active_face"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_active_field"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 1012
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1020
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 1021
    .local v7, "uniqueLabel":Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1023
    const-string v1, "stream_channel_label"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1026
    :cond_0
    if-eqz v6, :cond_1

    .line 1027
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1030
    :cond_1
    return-object v7
.end method

.method public static getActiveStreams(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;
    .locals 29
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 606
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 607
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/16 v4, 0x12

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "stream_state"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "stream_type"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "stream_id"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "stream_possible_settings"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "stream_img"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "stream_description"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "stream_name"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "stream_uuid"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "stream_label"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "stream_cont_version"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "stream_possible_settings"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const-string v6, "stream_supported_type"

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "stream_active_face"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    const-string v6, "stream_active_app"

    aput-object v6, v4, v5

    const/16 v5, 0xe

    const-string v6, "stream_active_field"

    aput-object v6, v4, v5

    const/16 v5, 0xf

    const-string v6, "stream_settings"

    aput-object v6, v4, v5

    const/16 v5, 0x10

    const-string v6, "auth_invalidated"

    aput-object v6, v4, v5

    const/16 v5, 0x11

    const-string v6, "needs_auth"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stream_state=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 616
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "stream_uuid"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 607
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 619
    .local v13, "cursor":Landroid/database/Cursor;
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 635
    .local v24, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    if-eqz v13, :cond_2

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 638
    :cond_0
    const-string v3, "stream_name"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 639
    .local v25, "streamName":Ljava/lang/String;
    const-string v3, "stream_description"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 640
    .local v20, "streamDescription":Ljava/lang/String;
    const-string v3, "stream_img"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 641
    .local v22, "streamImg":Ljava/lang/String;
    const-string v3, "stream_type"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 642
    .local v27, "streamType":Ljava/lang/String;
    const-string v3, "stream_uuid"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 643
    .local v28, "streamUuid":Ljava/lang/String;
    const-string v3, "stream_label"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 644
    .local v23, "streamLabel":Ljava/lang/String;
    const-string v3, "stream_cont_version"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 645
    .local v19, "streamContVersion":Ljava/lang/String;
    const-string v3, "stream_id"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 646
    .local v21, "streamId":I
    const-string v3, "stream_supported_type"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 648
    .local v26, "streamSupportedType":Ljava/lang/String;
    const-string v3, "stream_possible_settings"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 649
    .local v11, "content":[B
    const-string v3, "stream_settings"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    .line 650
    .local v9, "channelSettings":[B
    const-string v3, "needs_auth"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_4

    const/4 v15, 0x1

    .line 651
    .local v15, "needsAuth":Z
    :goto_0
    const-string v3, "auth_invalidated"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_5

    const/4 v8, 0x1

    .line 654
    .local v8, "authInvalidated":Z
    :goto_1
    new-instance v17, Lcom/vectorwatch/android/models/Stream;

    invoke-direct/range {v17 .. v17}, Lcom/vectorwatch/android/models/Stream;-><init>()V

    .line 655
    .local v17, "stream":Lcom/vectorwatch/android/models/Stream;
    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setName(Ljava/lang/String;)V

    .line 656
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setDescription(Ljava/lang/String;)V

    .line 657
    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setImg(Ljava/lang/String;)V

    .line 658
    move-object/from16 v0, v17

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setStreamType(Ljava/lang/String;)V

    .line 659
    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setUuid(Ljava/lang/String;)V

    .line 660
    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setLabelText(Ljava/lang/String;)V

    .line 661
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setContentVersion(Ljava/lang/String;)V

    .line 662
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/Stream;->setId(Ljava/lang/Integer;)V

    .line 663
    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setSupportedType(Ljava/lang/String;)V

    .line 664
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/vectorwatch/android/models/Stream;->setRequireUserAuth(Z)V

    .line 665
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/Stream;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 667
    new-instance v14, Lcom/google/gson/Gson;

    invoke-direct {v14}, Lcom/google/gson/Gson;-><init>()V

    .line 669
    .local v14, "gson":Lcom/google/gson/Gson;
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v11}, Ljava/lang/String;-><init>([B)V

    .line 671
    .local v12, "contentString":Ljava/lang/String;
    const-class v3, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    invoke-virtual {v14, v12, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .line 672
    .local v16, "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setStreamSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V

    .line 675
    new-instance v14, Lcom/google/gson/Gson;

    .end local v14    # "gson":Lcom/google/gson/Gson;
    invoke-direct {v14}, Lcom/google/gson/Gson;-><init>()V

    .line 676
    .restart local v14    # "gson":Lcom/google/gson/Gson;
    if-eqz v9, :cond_1

    .line 677
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v9}, Ljava/lang/String;-><init>([B)V

    .line 678
    .local v10, "channelSettingsString":Ljava/lang/String;
    const-class v3, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-virtual {v14, v10, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 680
    .local v18, "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    invoke-virtual/range {v17 .. v18}, Lcom/vectorwatch/android/models/Stream;->setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 683
    .end local v10    # "channelSettingsString":Ljava/lang/String;
    .end local v18    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    :cond_1
    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 684
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 687
    .end local v8    # "authInvalidated":Z
    .end local v9    # "channelSettings":[B
    .end local v11    # "content":[B
    .end local v12    # "contentString":Ljava/lang/String;
    .end local v14    # "gson":Lcom/google/gson/Gson;
    .end local v15    # "needsAuth":Z
    .end local v16    # "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .end local v17    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v19    # "streamContVersion":Ljava/lang/String;
    .end local v20    # "streamDescription":Ljava/lang/String;
    .end local v21    # "streamId":I
    .end local v22    # "streamImg":Ljava/lang/String;
    .end local v23    # "streamLabel":Ljava/lang/String;
    .end local v25    # "streamName":Ljava/lang/String;
    .end local v26    # "streamSupportedType":Ljava/lang/String;
    .end local v27    # "streamType":Ljava/lang/String;
    .end local v28    # "streamUuid":Ljava/lang/String;
    :cond_2
    if-eqz v13, :cond_3

    .line 688
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 691
    :cond_3
    return-object v24

    .line 650
    .restart local v9    # "channelSettings":[B
    .restart local v11    # "content":[B
    .restart local v19    # "streamContVersion":Ljava/lang/String;
    .restart local v20    # "streamDescription":Ljava/lang/String;
    .restart local v21    # "streamId":I
    .restart local v22    # "streamImg":Ljava/lang/String;
    .restart local v23    # "streamLabel":Ljava/lang/String;
    .restart local v25    # "streamName":Ljava/lang/String;
    .restart local v26    # "streamSupportedType":Ljava/lang/String;
    .restart local v27    # "streamType":Ljava/lang/String;
    .restart local v28    # "streamUuid":Ljava/lang/String;
    :cond_4
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 651
    .restart local v15    # "needsAuth":Z
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_1
.end method

.method public static getExpiredStreamsByType(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamLogItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 703
    sget-object v3, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Get expired streams"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 704
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 705
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "stream_state"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "stream_type"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "stream_channel_label"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "stream_seconds_to_live"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "stream_timestamp"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "stream_uuid"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stream_state=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 709
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "stream_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 705
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 715
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 717
    .local v12, "expiredStreamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamLogItem;>;"
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 720
    :cond_0
    const-string v3, "stream_seconds_to_live"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 721
    .local v14, "secondsToLive":I
    const-string v3, "stream_timestamp"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 723
    .local v16, "timestamp":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    .line 725
    .local v10, "currentTime":J
    const/4 v3, -0x1

    if-eq v14, v3, :cond_1

    mul-int/lit16 v3, v14, 0x3e8

    int-to-long v4, v3

    add-long v4, v4, v16

    cmp-long v3, v10, v4

    if-lez v3, :cond_1

    .line 726
    const-string v3, "stream_uuid"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 727
    .local v15, "uuid":Ljava/lang/String;
    const-string v3, "stream_channel_label"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 728
    .local v8, "channelLabel":Ljava/lang/String;
    new-instance v13, Lcom/vectorwatch/android/models/StreamLogItem;

    const-wide/16 v4, 0x3e8

    div-long v4, v16, v4

    invoke-direct {v13, v8, v15, v4, v5}, Lcom/vectorwatch/android/models/StreamLogItem;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 729
    .local v13, "item":Lcom/vectorwatch/android/models/StreamLogItem;
    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 732
    .end local v8    # "channelLabel":Ljava/lang/String;
    .end local v13    # "item":Lcom/vectorwatch/android/models/StreamLogItem;
    .end local v15    # "uuid":Ljava/lang/String;
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 735
    .end local v10    # "currentTime":J
    .end local v14    # "secondsToLive":I
    .end local v16    # "timestamp":J
    :cond_2
    if-eqz v9, :cond_3

    .line 736
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 739
    :cond_3
    return-object v12
.end method

.method public static getLastUpdatedValues(Landroid/content/Context;)Ljava/util/List;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/PushUpdateRoute;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v14, -0x2

    .line 1271
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1272
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "stream_state"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "stream_dirty"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "stream_active_face"

    aput-object v5, v2, v3

    const/4 v3, 0x3

    const-string v5, "stream_active_field"

    aput-object v5, v2, v3

    const/4 v3, 0x4

    const-string v5, "stream_active_app"

    aput-object v5, v2, v3

    const/4 v3, 0x5

    const-string v5, "stream_update_value"

    aput-object v5, v2, v3

    const/4 v3, 0x6

    const-string v5, "stream_type"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_state=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 1278
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_type"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PRIVATE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 1280
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " or "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_type"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 1282
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " or "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_type"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PUBLIC:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 1284
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ")"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 1272
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1293
    .local v7, "cursor":Landroid/database/Cursor;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1295
    .local v11, "pushUpdateRouteList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1298
    :cond_0
    const-string v1, "stream_active_app"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1299
    .local v6, "appId":Ljava/lang/Integer;
    const-string v1, "stream_active_face"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 1300
    .local v9, "faceId":Ljava/lang/Integer;
    const-string v1, "stream_active_field"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 1301
    .local v8, "elementId":Ljava/lang/Integer;
    const-string v1, "stream_update_value"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1302
    .local v13, "updateValue":Ljava/lang/String;
    const-string v1, "stream_type"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1303
    .local v12, "streamType":Ljava/lang/String;
    sget-object v1, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Updated values = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1305
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v14, :cond_1

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v14, :cond_1

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v14, :cond_1

    .line 1307
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v2, v3, p0}, Lcom/vectorwatch/android/managers/StreamsManager;->markAsUnmodified(IIILandroid/content/Context;)V

    .line 1309
    new-instance v10, Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-direct {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;-><init>()V

    .line 1310
    .local v10, "pushUpdateRoute":Lcom/vectorwatch/android/models/PushUpdateRoute;
    invoke-virtual {v10, v6}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setAppId(Ljava/lang/Integer;)V

    .line 1311
    invoke-virtual {v10, v8}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setFieldId(Ljava/lang/Integer;)V

    .line 1312
    invoke-virtual {v10, v9}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setWatchfaceId(Ljava/lang/Integer;)V

    .line 1313
    invoke-virtual {v10, v13}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setUpdateData(Ljava/lang/String;)V

    .line 1314
    invoke-virtual {v10, v12}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setStreamType(Ljava/lang/String;)V

    .line 1315
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {p0, v1, v2, v3}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamElementType(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setElementType(Ljava/lang/String;)V

    .line 1317
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1319
    .end local v10    # "pushUpdateRoute":Lcom/vectorwatch/android/models/PushUpdateRoute;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1322
    .end local v6    # "appId":Ljava/lang/Integer;
    .end local v8    # "elementId":Ljava/lang/Integer;
    .end local v9    # "faceId":Ljava/lang/Integer;
    .end local v12    # "streamType":Ljava/lang/String;
    .end local v13    # "updateValue":Ljava/lang/String;
    :cond_2
    if-eqz v7, :cond_3

    .line 1323
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1326
    :cond_3
    return-object v11
.end method

.method public static getPushUpdateRoutes(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;
    .locals 13
    .param p0, "channelUniqueLabel"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/PushUpdateRoute;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v12, -0x2

    .line 1041
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1042
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "stream_state"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "stream_channel_label"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "stream_active_face"

    aput-object v5, v2, v3

    const/4 v3, 0x3

    const-string v5, "stream_active_field"

    aput-object v5, v2, v3

    const/4 v3, 0x4

    const-string v5, "stream_active_app"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_state=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 1046
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_channel_label"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 1042
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1053
    .local v7, "cursor":Landroid/database/Cursor;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1055
    .local v11, "pushUpdateRouteList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1058
    :cond_0
    const-string v1, "stream_active_app"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1059
    .local v6, "appId":Ljava/lang/Integer;
    const-string v1, "stream_active_face"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 1060
    .local v9, "faceId":Ljava/lang/Integer;
    const-string v1, "stream_active_field"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 1062
    .local v8, "elementId":Ljava/lang/Integer;
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v12, :cond_1

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v12, :cond_1

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v12, :cond_1

    .line 1063
    new-instance v10, Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-direct {v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;-><init>()V

    .line 1064
    .local v10, "pushUpdateRoute":Lcom/vectorwatch/android/models/PushUpdateRoute;
    invoke-virtual {v10, v6}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setAppId(Ljava/lang/Integer;)V

    .line 1065
    invoke-virtual {v10, v9}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setFieldId(Ljava/lang/Integer;)V

    .line 1066
    invoke-virtual {v10, v8}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setWatchfaceId(Ljava/lang/Integer;)V

    .line 1067
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {p1, v1, v2, v3}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamElementType(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setElementType(Ljava/lang/String;)V

    .line 1069
    invoke-virtual {v10, p0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setChannelUniqueLabel(Ljava/lang/String;)V

    .line 1071
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1073
    .end local v10    # "pushUpdateRoute":Lcom/vectorwatch/android/models/PushUpdateRoute;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1076
    .end local v6    # "appId":Ljava/lang/Integer;
    .end local v8    # "elementId":Ljava/lang/Integer;
    .end local v9    # "faceId":Ljava/lang/Integer;
    :cond_2
    if-eqz v7, :cond_3

    .line 1077
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1080
    :cond_3
    return-object v11
.end method

.method public static getPushUpdateRoutesAssociatedToStream(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;
    .locals 14
    .param p0, "streamUuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/PushUpdateRoute;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v13, -0x2

    .line 1084
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1085
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "stream_state"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "stream_channel_label"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "stream_active_face"

    aput-object v5, v2, v3

    const/4 v3, 0x3

    const-string v5, "stream_active_field"

    aput-object v5, v2, v3

    const/4 v3, 0x4

    const-string v5, "stream_active_app"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_state=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 1089
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_uuid"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 1085
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1096
    .local v8, "cursor":Landroid/database/Cursor;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1098
    .local v12, "pushUpdateRouteList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1101
    :cond_0
    const-string v1, "stream_active_app"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1102
    .local v6, "appId":Ljava/lang/Integer;
    const-string v1, "stream_active_face"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 1103
    .local v10, "faceId":Ljava/lang/Integer;
    const-string v1, "stream_active_field"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 1104
    .local v9, "elementId":Ljava/lang/Integer;
    const-string v1, "stream_channel_label"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1106
    .local v7, "channelUniqueLabel":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v13, :cond_1

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v13, :cond_1

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v13, :cond_1

    .line 1107
    new-instance v11, Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-direct {v11}, Lcom/vectorwatch/android/models/PushUpdateRoute;-><init>()V

    .line 1108
    .local v11, "pushUpdateRoute":Lcom/vectorwatch/android/models/PushUpdateRoute;
    invoke-virtual {v11, v6}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setAppId(Ljava/lang/Integer;)V

    .line 1109
    invoke-virtual {v11, v9}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setFieldId(Ljava/lang/Integer;)V

    .line 1110
    invoke-virtual {v11, v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setWatchfaceId(Ljava/lang/Integer;)V

    .line 1111
    invoke-virtual {v11, v7}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setChannelUniqueLabel(Ljava/lang/String;)V

    .line 1112
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {p1, v1, v2, v3}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamElementType(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setElementType(Ljava/lang/String;)V

    .line 1114
    invoke-interface {v12, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1116
    .end local v11    # "pushUpdateRoute":Lcom/vectorwatch/android/models/PushUpdateRoute;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1119
    .end local v6    # "appId":Ljava/lang/Integer;
    .end local v7    # "channelUniqueLabel":Ljava/lang/String;
    .end local v9    # "elementId":Ljava/lang/Integer;
    .end local v10    # "faceId":Ljava/lang/Integer;
    :cond_2
    if-eqz v8, :cond_3

    .line 1120
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1123
    :cond_3
    return-object v12
.end method

.method public static getStreamChannelActiveCount(Ljava/lang/String;Landroid/content/Context;)I
    .locals 8
    .param p0, "streamChannelLabel"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 984
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 985
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "stream_state"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "stream_channel_label"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_state=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 987
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "stream_channel_label"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 985
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 990
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 991
    .local v6, "count":I
    if-eqz v7, :cond_0

    .line 992
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 993
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 996
    :cond_0
    return v6
.end method

.method private static getStreamElementType(Landroid/content/Context;III)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # I
    .param p2, "faceId"    # I
    .param p3, "elementId"    # I

    .prologue
    .line 1138
    const-string v1, ""

    .line 1139
    .local v1, "elementType":Ljava/lang/String;
    invoke-static {p1, p0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v0

    .line 1140
    .local v0, "app":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 1141
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, p2, :cond_0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, p3, :cond_0

    .line 1142
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    .line 1143
    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1144
    iget-object v2, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    iget-object v2, v2, Lcom/vectorwatch/android/models/AppContent;->watchfaces:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Watchface;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Watchface;->getElements()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Element;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Element;->getType()Ljava/lang/String;

    move-result-object v1

    .line 1148
    :cond_0
    return-object v1
.end method

.method public static getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;
    .locals 25
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 241
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 242
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/16 v3, 0xf

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "stream_state"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "stream_type"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "stream_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "stream_version"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "stream_possible_settings"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "stream_img"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "stream_description"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "stream_name"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "stream_uuid"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "stream_label"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "stream_cont_version"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string v5, "stream_possible_settings"

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "stream_supported_type"

    aput-object v5, v3, v4

    const/16 v4, 0xd

    const-string v5, "auth_invalidated"

    aput-object v5, v3, v4

    const/16 v4, 0xe

    const-string v5, "needs_auth"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_uuid=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_state"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->LOCKER:Lcom/vectorwatch/android/models/Stream$State;

    .line 259
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 242
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 275
    .local v10, "cursor":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 276
    .local v14, "stream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v10, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 277
    const-string v2, "stream_name"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 278
    .local v20, "streamName":Ljava/lang/String;
    const-string v2, "stream_description"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 279
    .local v16, "streamDescription":Ljava/lang/String;
    const-string v2, "stream_img"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 280
    .local v18, "streamImg":Ljava/lang/String;
    const-string v2, "stream_type"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 281
    .local v22, "streamType":Ljava/lang/String;
    const-string v2, "stream_uuid"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 282
    .local v23, "streamUuid":Ljava/lang/String;
    const-string v2, "stream_label"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 283
    .local v19, "streamLabel":Ljava/lang/String;
    const-string v2, "stream_cont_version"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 284
    .local v15, "streamContVersion":Ljava/lang/String;
    const-string v2, "stream_id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 285
    .local v17, "streamId":I
    const-string v2, "stream_version"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    .line 286
    .local v24, "streamVersion":Ljava/lang/Integer;
    const-string v2, "stream_supported_type"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 288
    .local v21, "streamSupportedType":Ljava/lang/String;
    const-string v2, "needs_auth"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_2

    const/4 v12, 0x1

    .line 289
    .local v12, "needsAuth":Z
    :goto_0
    const-string v2, "stream_possible_settings"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    .line 290
    .local v8, "content":[B
    const-string v2, "auth_invalidated"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_3

    const/4 v7, 0x1

    .line 292
    .local v7, "authInvalidated":Z
    :goto_1
    new-instance v14, Lcom/vectorwatch/android/models/Stream;

    .end local v14    # "stream":Lcom/vectorwatch/android/models/Stream;
    invoke-direct {v14}, Lcom/vectorwatch/android/models/Stream;-><init>()V

    .line 293
    .restart local v14    # "stream":Lcom/vectorwatch/android/models/Stream;
    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setName(Ljava/lang/String;)V

    .line 294
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setDescription(Ljava/lang/String;)V

    .line 295
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setImg(Ljava/lang/String;)V

    .line 296
    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setStreamType(Ljava/lang/String;)V

    .line 297
    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setUuid(Ljava/lang/String;)V

    .line 298
    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setLabelText(Ljava/lang/String;)V

    .line 299
    invoke-virtual {v14, v15}, Lcom/vectorwatch/android/models/Stream;->setContentVersion(Ljava/lang/String;)V

    .line 300
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/vectorwatch/android/models/Stream;->setId(Ljava/lang/Integer;)V

    .line 301
    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setSupportedType(Ljava/lang/String;)V

    .line 302
    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setVersion(Ljava/lang/Integer;)V

    .line 303
    invoke-virtual {v14, v12}, Lcom/vectorwatch/android/models/Stream;->setRequireUserAuth(Z)V

    .line 304
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/vectorwatch/android/models/Stream;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 306
    new-instance v11, Lcom/google/gson/Gson;

    invoke-direct {v11}, Lcom/google/gson/Gson;-><init>()V

    .line 308
    .local v11, "gson":Lcom/google/gson/Gson;
    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v8}, Ljava/lang/String;-><init>([B)V

    .line 309
    .local v9, "contentString":Ljava/lang/String;
    const-class v2, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    invoke-virtual {v11, v9, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .line 310
    .local v13, "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    invoke-virtual {v14, v13}, Lcom/vectorwatch/android/models/Stream;->setStreamSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V

    .line 313
    .end local v7    # "authInvalidated":Z
    .end local v8    # "content":[B
    .end local v9    # "contentString":Ljava/lang/String;
    .end local v11    # "gson":Lcom/google/gson/Gson;
    .end local v12    # "needsAuth":Z
    .end local v13    # "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .end local v15    # "streamContVersion":Ljava/lang/String;
    .end local v16    # "streamDescription":Ljava/lang/String;
    .end local v17    # "streamId":I
    .end local v18    # "streamImg":Ljava/lang/String;
    .end local v19    # "streamLabel":Ljava/lang/String;
    .end local v20    # "streamName":Ljava/lang/String;
    .end local v21    # "streamSupportedType":Ljava/lang/String;
    .end local v22    # "streamType":Ljava/lang/String;
    .end local v23    # "streamUuid":Ljava/lang/String;
    .end local v24    # "streamVersion":Ljava/lang/Integer;
    :cond_0
    if-eqz v10, :cond_1

    .line 314
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 317
    :cond_1
    if-nez v14, :cond_4

    .line 318
    sget-object v2, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Tried to delete stream with uuid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Stream not found."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 323
    :goto_2
    return-object v14

    .line 288
    .restart local v15    # "streamContVersion":Ljava/lang/String;
    .restart local v16    # "streamDescription":Ljava/lang/String;
    .restart local v17    # "streamId":I
    .restart local v18    # "streamImg":Ljava/lang/String;
    .restart local v19    # "streamLabel":Ljava/lang/String;
    .restart local v20    # "streamName":Ljava/lang/String;
    .restart local v21    # "streamSupportedType":Ljava/lang/String;
    .restart local v22    # "streamType":Ljava/lang/String;
    .restart local v23    # "streamUuid":Ljava/lang/String;
    .restart local v24    # "streamVersion":Ljava/lang/Integer;
    :cond_2
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 290
    .restart local v8    # "content":[B
    .restart local v12    # "needsAuth":Z
    :cond_3
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 320
    .end local v8    # "content":[B
    .end local v12    # "needsAuth":Z
    .end local v15    # "streamContVersion":Ljava/lang/String;
    .end local v16    # "streamDescription":Ljava/lang/String;
    .end local v17    # "streamId":I
    .end local v18    # "streamImg":Ljava/lang/String;
    .end local v19    # "streamLabel":Ljava/lang/String;
    .end local v20    # "streamName":Ljava/lang/String;
    .end local v21    # "streamSupportedType":Ljava/lang/String;
    .end local v22    # "streamType":Ljava/lang/String;
    .end local v23    # "streamUuid":Ljava/lang/String;
    .end local v24    # "streamVersion":Ljava/lang/Integer;
    :cond_4
    sget-object v2, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Deleted stream with uuid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static getStreamsWithGivenState(Lcom/vectorwatch/android/models/Stream$State;Landroid/content/Context;)Ljava/util/List;
    .locals 37
    .param p0, "state"    # Lcom/vectorwatch/android/models/Stream$State;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/Stream$State;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 468
    .local v2, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/16 v4, 0x12

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "stream_state"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "stream_type"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "stream_id"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "stream_version"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "stream_possible_settings"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "stream_img"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "stream_description"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "stream_name"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "stream_uuid"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "stream_label"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "stream_cont_version"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const-string v6, "stream_supported_type"

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "stream_active_face"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    const-string v6, "stream_active_app"

    aput-object v6, v4, v5

    const/16 v5, 0xe

    const-string v6, "stream_settings"

    aput-object v6, v4, v5

    const/16 v5, 0xf

    const-string v6, "stream_active_field"

    aput-object v6, v4, v5

    const/16 v5, 0x10

    const-string v6, "auth_invalidated"

    aput-object v6, v4, v5

    const/16 v5, 0x11

    const-string v6, "needs_auth"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stream_state=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 487
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 468
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 489
    .local v15, "cursor":Landroid/database/Cursor;
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 510
    .local v29, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    if-eqz v15, :cond_4

    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 513
    :cond_0
    const-string v3, "stream_name"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v30

    .line 514
    .local v30, "streamName":Ljava/lang/String;
    const-string v3, "stream_description"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 515
    .local v25, "streamDescription":Ljava/lang/String;
    const-string v3, "stream_img"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 516
    .local v27, "streamImg":Ljava/lang/String;
    const-string v3, "stream_version"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v36

    .line 517
    .local v36, "streamVersion":Ljava/lang/Integer;
    const-string v3, "stream_type"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 518
    .local v34, "streamType":Ljava/lang/String;
    const-string v3, "stream_uuid"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v35

    .line 519
    .local v35, "streamUuid":Ljava/lang/String;
    const-string v3, "stream_label"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 520
    .local v28, "streamLabel":Ljava/lang/String;
    const-string v3, "stream_cont_version"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 521
    .local v24, "streamContVersion":Ljava/lang/String;
    const-string v3, "stream_id"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    .line 522
    .local v26, "streamId":I
    const-string v3, "stream_supported_type"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    .line 524
    .local v33, "streamSupportedType":Ljava/lang/String;
    const-string v3, "stream_possible_settings"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v12

    .line 525
    .local v12, "content":[B
    const-string v3, "stream_settings"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    .line 526
    .local v13, "contentSettings":[B
    const-string v3, "stream_state"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 527
    .local v32, "streamState":Ljava/lang/String;
    const-string v3, "stream_active_app"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 528
    .local v8, "appId":Ljava/lang/Integer;
    const-string v3, "stream_active_face"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 529
    .local v17, "faceId":Ljava/lang/Integer;
    const-string v3, "stream_active_field"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 530
    .local v16, "elementId":Ljava/lang/Integer;
    const-string v3, "needs_auth"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_6

    const/16 v21, 0x1

    .line 531
    .local v21, "needsAuth":Z
    :goto_0
    const-string v3, "auth_invalidated"

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_7

    const/4 v10, 0x1

    .line 534
    .local v10, "authInvalidated":Z
    :goto_1
    sget-object v3, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stream with state "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " active in mAppId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on faceId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " on elementId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 537
    new-instance v31, Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-direct/range {v31 .. v31}, Lcom/vectorwatch/android/models/StreamPlacementModel;-><init>()V

    .line 538
    .local v31, "streamPlacement":Lcom/vectorwatch/android/models/StreamPlacementModel;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setElementId(Ljava/lang/Integer;)V

    .line 539
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setWatchFaceId(Ljava/lang/Integer;)V

    .line 540
    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setAppId(Ljava/lang/Integer;)V

    .line 541
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppUuid(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 542
    .local v9, "appUuid":Ljava/lang/String;
    move-object/from16 v0, v31

    invoke-virtual {v0, v9}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setAppUuid(Ljava/lang/String;)V

    .line 544
    new-instance v23, Lcom/vectorwatch/android/models/Stream;

    invoke-direct/range {v23 .. v23}, Lcom/vectorwatch/android/models/Stream;-><init>()V

    .line 545
    .local v23, "stream":Lcom/vectorwatch/android/models/Stream;
    move-object/from16 v0, v23

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setName(Ljava/lang/String;)V

    .line 546
    move-object/from16 v0, v23

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setVersion(Ljava/lang/Integer;)V

    .line 547
    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setDescription(Ljava/lang/String;)V

    .line 548
    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setImg(Ljava/lang/String;)V

    .line 549
    move-object/from16 v0, v23

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setStreamType(Ljava/lang/String;)V

    .line 550
    move-object/from16 v0, v23

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setUuid(Ljava/lang/String;)V

    .line 551
    move-object/from16 v0, v23

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setLabelText(Ljava/lang/String;)V

    .line 552
    invoke-virtual/range {v23 .. v24}, Lcom/vectorwatch/android/models/Stream;->setContentVersion(Ljava/lang/String;)V

    .line 553
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/Stream;->setId(Ljava/lang/Integer;)V

    .line 554
    move-object/from16 v0, v23

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setSupportedType(Ljava/lang/String;)V

    .line 555
    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setStreamPlacement(Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    .line 556
    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setRequireUserAuth(Z)V

    .line 557
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Lcom/vectorwatch/android/models/Stream;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 559
    new-instance v19, Lcom/google/gson/Gson;

    invoke-direct/range {v19 .. v19}, Lcom/google/gson/Gson;-><init>()V

    .line 561
    .local v19, "gson":Lcom/google/gson/Gson;
    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v12}, Ljava/lang/String;-><init>([B)V

    .line 563
    .local v14, "contentString":Ljava/lang/String;
    const-class v3, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .line 564
    .local v22, "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/Stream;->setStreamSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V

    .line 566
    if-eqz v13, :cond_1

    .line 567
    new-instance v14, Ljava/lang/String;

    .end local v14    # "contentString":Ljava/lang/String;
    invoke-direct {v14, v13}, Ljava/lang/String;-><init>([B)V

    .line 568
    .restart local v14    # "contentString":Ljava/lang/String;
    const-class v3, Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/vectorwatch/android/models/StreamChannelSettings;

    .line 569
    .local v11, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/vectorwatch/android/models/Stream;->setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 573
    .end local v11    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    :cond_1
    sget-object v3, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_DELETE:Lcom/vectorwatch/android/models/Stream$State;

    move-object/from16 v0, p0

    if-ne v0, v3, :cond_9

    .line 574
    const/16 v18, 0x0

    .line 575
    .local v18, "found":Z
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_2
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v20

    if-ge v0, v3, :cond_2

    .line 576
    invoke-virtual/range {v23 .. v23}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v29

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 577
    const/16 v18, 0x1

    .line 581
    :cond_2
    if-nez v18, :cond_3

    .line 582
    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 587
    .end local v18    # "found":Z
    .end local v20    # "i":I
    :cond_3
    :goto_3
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 590
    .end local v8    # "appId":Ljava/lang/Integer;
    .end local v9    # "appUuid":Ljava/lang/String;
    .end local v10    # "authInvalidated":Z
    .end local v12    # "content":[B
    .end local v13    # "contentSettings":[B
    .end local v14    # "contentString":Ljava/lang/String;
    .end local v16    # "elementId":Ljava/lang/Integer;
    .end local v17    # "faceId":Ljava/lang/Integer;
    .end local v19    # "gson":Lcom/google/gson/Gson;
    .end local v21    # "needsAuth":Z
    .end local v22    # "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .end local v23    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v24    # "streamContVersion":Ljava/lang/String;
    .end local v25    # "streamDescription":Ljava/lang/String;
    .end local v26    # "streamId":I
    .end local v27    # "streamImg":Ljava/lang/String;
    .end local v28    # "streamLabel":Ljava/lang/String;
    .end local v30    # "streamName":Ljava/lang/String;
    .end local v31    # "streamPlacement":Lcom/vectorwatch/android/models/StreamPlacementModel;
    .end local v32    # "streamState":Ljava/lang/String;
    .end local v33    # "streamSupportedType":Ljava/lang/String;
    .end local v34    # "streamType":Ljava/lang/String;
    .end local v35    # "streamUuid":Ljava/lang/String;
    .end local v36    # "streamVersion":Ljava/lang/Integer;
    :cond_4
    if-eqz v15, :cond_5

    .line 591
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 594
    :cond_5
    return-object v29

    .line 530
    .restart local v8    # "appId":Ljava/lang/Integer;
    .restart local v12    # "content":[B
    .restart local v13    # "contentSettings":[B
    .restart local v16    # "elementId":Ljava/lang/Integer;
    .restart local v17    # "faceId":Ljava/lang/Integer;
    .restart local v24    # "streamContVersion":Ljava/lang/String;
    .restart local v25    # "streamDescription":Ljava/lang/String;
    .restart local v26    # "streamId":I
    .restart local v27    # "streamImg":Ljava/lang/String;
    .restart local v28    # "streamLabel":Ljava/lang/String;
    .restart local v30    # "streamName":Ljava/lang/String;
    .restart local v32    # "streamState":Ljava/lang/String;
    .restart local v33    # "streamSupportedType":Ljava/lang/String;
    .restart local v34    # "streamType":Ljava/lang/String;
    .restart local v35    # "streamUuid":Ljava/lang/String;
    .restart local v36    # "streamVersion":Ljava/lang/Integer;
    :cond_6
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 531
    .restart local v21    # "needsAuth":Z
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 575
    .restart local v9    # "appUuid":Ljava/lang/String;
    .restart local v10    # "authInvalidated":Z
    .restart local v14    # "contentString":Ljava/lang/String;
    .restart local v18    # "found":Z
    .restart local v19    # "gson":Lcom/google/gson/Gson;
    .restart local v20    # "i":I
    .restart local v22    # "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .restart local v23    # "stream":Lcom/vectorwatch/android/models/Stream;
    .restart local v31    # "streamPlacement":Lcom/vectorwatch/android/models/StreamPlacementModel;
    :cond_8
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 585
    .end local v18    # "found":Z
    .end local v20    # "i":I
    :cond_9
    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method public static getUniqueAvailableStreams(Landroid/content/Context;)Ljava/util/List;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 137
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/16 v3, 0x10

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "stream_state"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "stream_type"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "stream_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "stream_version"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "stream_possible_settings"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "stream_img"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "stream_description"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "stream_name"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "stream_uuid"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "stream_label"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "stream_cont_version"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string v5, "stream_possible_settings"

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "stream_supported_type"

    aput-object v5, v3, v4

    const/16 v4, 0xd

    const-string v5, "auth_invalidated"

    aput-object v5, v3, v4

    const/16 v4, 0xe

    const-string v5, "needs_auth"

    aput-object v5, v3, v4

    const/16 v4, 0xf

    const-string v5, "stream_dynamic"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_state!=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->UNINSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 154
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_type"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PRIVATE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 155
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 137
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 158
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 175
    .local v20, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 178
    :cond_0
    const-string v2, "stream_name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 179
    .local v21, "streamName":Ljava/lang/String;
    const-string v2, "stream_description"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 180
    .local v16, "streamDescription":Ljava/lang/String;
    const-string v2, "stream_img"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 181
    .local v18, "streamImg":Ljava/lang/String;
    const-string v2, "stream_type"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 182
    .local v23, "streamType":Ljava/lang/String;
    const-string v2, "stream_uuid"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 183
    .local v24, "streamUuid":Ljava/lang/String;
    const-string v2, "stream_label"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 184
    .local v19, "streamLabel":Ljava/lang/String;
    const-string v2, "stream_cont_version"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 185
    .local v15, "streamContVersion":Ljava/lang/String;
    const-string v2, "stream_id"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 186
    .local v17, "streamId":I
    const-string v2, "stream_version"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    .line 187
    .local v25, "streamVersion":Ljava/lang/Integer;
    const-string v2, "stream_supported_type"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 189
    .local v22, "streamSupportedType":Ljava/lang/String;
    const-string v2, "stream_possible_settings"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    .line 190
    .local v8, "content":[B
    const-string v2, "stream_dynamic"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_4

    const/4 v10, 0x1

    .line 191
    .local v10, "dynamic":Z
    :goto_0
    const-string v2, "needs_auth"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_5

    const/4 v12, 0x1

    .line 192
    .local v12, "needsAuth":Z
    :goto_1
    const-string v2, "auth_invalidated"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_6

    const/4 v7, 0x1

    .line 195
    .local v7, "authInvalidated":Z
    :goto_2
    new-instance v14, Lcom/vectorwatch/android/models/Stream;

    invoke-direct {v14}, Lcom/vectorwatch/android/models/Stream;-><init>()V

    .line 196
    .local v14, "stream":Lcom/vectorwatch/android/models/Stream;
    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setName(Ljava/lang/String;)V

    .line 197
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setDescription(Ljava/lang/String;)V

    .line 198
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setImg(Ljava/lang/String;)V

    .line 199
    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setStreamType(Ljava/lang/String;)V

    .line 200
    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setUuid(Ljava/lang/String;)V

    .line 201
    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setLabelText(Ljava/lang/String;)V

    .line 202
    invoke-virtual {v14, v15}, Lcom/vectorwatch/android/models/Stream;->setContentVersion(Ljava/lang/String;)V

    .line 203
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/vectorwatch/android/models/Stream;->setId(Ljava/lang/Integer;)V

    .line 204
    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setSupportedType(Ljava/lang/String;)V

    .line 205
    invoke-virtual {v14, v10}, Lcom/vectorwatch/android/models/Stream;->setDynamic(Z)V

    .line 206
    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/Stream;->setVersion(Ljava/lang/Integer;)V

    .line 207
    invoke-virtual {v14, v12}, Lcom/vectorwatch/android/models/Stream;->setRequireUserAuth(Z)V

    .line 208
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/vectorwatch/android/models/Stream;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 210
    new-instance v11, Lcom/google/gson/Gson;

    invoke-direct {v11}, Lcom/google/gson/Gson;-><init>()V

    .line 213
    .local v11, "gson":Lcom/google/gson/Gson;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/lang/String;-><init>([B)V

    const-class v3, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    invoke-virtual {v11, v2, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/vectorwatch/android/models/settings/PossibleSettings;

    .line 214
    .local v13, "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    invoke-virtual {v14, v13}, Lcom/vectorwatch/android/models/Stream;->setStreamSettings(Lcom/vectorwatch/android/models/settings/PossibleSettings;)V

    .line 216
    move-object/from16 v0, v20

    invoke-static {v14, v0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->isStreamInList(Lcom/vectorwatch/android/models/Stream;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 217
    move-object/from16 v0, v20

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 222
    .end local v7    # "authInvalidated":Z
    .end local v8    # "content":[B
    .end local v10    # "dynamic":Z
    .end local v11    # "gson":Lcom/google/gson/Gson;
    .end local v12    # "needsAuth":Z
    .end local v13    # "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .end local v14    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v15    # "streamContVersion":Ljava/lang/String;
    .end local v16    # "streamDescription":Ljava/lang/String;
    .end local v17    # "streamId":I
    .end local v18    # "streamImg":Ljava/lang/String;
    .end local v19    # "streamLabel":Ljava/lang/String;
    .end local v21    # "streamName":Ljava/lang/String;
    .end local v22    # "streamSupportedType":Ljava/lang/String;
    .end local v23    # "streamType":Ljava/lang/String;
    .end local v24    # "streamUuid":Ljava/lang/String;
    .end local v25    # "streamVersion":Ljava/lang/Integer;
    :cond_2
    if-eqz v9, :cond_3

    .line 223
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 226
    :cond_3
    return-object v20

    .line 190
    .restart local v8    # "content":[B
    .restart local v15    # "streamContVersion":Ljava/lang/String;
    .restart local v16    # "streamDescription":Ljava/lang/String;
    .restart local v17    # "streamId":I
    .restart local v18    # "streamImg":Ljava/lang/String;
    .restart local v19    # "streamLabel":Ljava/lang/String;
    .restart local v21    # "streamName":Ljava/lang/String;
    .restart local v22    # "streamSupportedType":Ljava/lang/String;
    .restart local v23    # "streamType":Ljava/lang/String;
    .restart local v24    # "streamUuid":Ljava/lang/String;
    .restart local v25    # "streamVersion":Ljava/lang/Integer;
    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 191
    .restart local v10    # "dynamic":Z
    :cond_5
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 192
    .restart local v12    # "needsAuth":Z
    :cond_6
    const/4 v7, 0x0

    goto :goto_2
.end method

.method public static getUpdatedValues(Landroid/content/Context;)Ljava/util/List;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/PushUpdateRoute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1211
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1212
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "stream_state"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "stream_dirty"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "stream_active_face"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "stream_active_field"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "stream_active_app"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "stream_update_value"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "stream_type"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "stream_channel_label"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "stream_seconds_to_live"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_state=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 1218
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_dirty"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 1212
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1229
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1231
    .local v13, "pushUpdateRouteList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1234
    :cond_0
    const-string v2, "stream_active_app"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 1235
    .local v7, "appId":Ljava/lang/Integer;
    const-string v2, "stream_active_face"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 1236
    .local v11, "faceId":Ljava/lang/Integer;
    const-string v2, "stream_active_field"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 1237
    .local v10, "elementId":Ljava/lang/Integer;
    const-string v2, "stream_update_value"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1238
    .local v16, "updateValue":Ljava/lang/String;
    const-string v2, "stream_type"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1239
    .local v15, "streamType":Ljava/lang/String;
    const-string v2, "stream_channel_label"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1241
    .local v8, "channelLabel":Ljava/lang/String;
    const-string v2, "stream_seconds_to_live"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    .line 1242
    .local v14, "secondsToLive":Ljava/lang/Integer;
    sget-object v2, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updated values = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " | "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1244
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, -0x2

    if-eq v2, v3, :cond_1

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, -0x2

    if-eq v2, v3, :cond_1

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, -0x2

    if-eq v2, v3, :cond_1

    .line 1246
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p0

    invoke-static {v2, v3, v4, v0}, Lcom/vectorwatch/android/managers/StreamsManager;->markAsUnmodified(IIILandroid/content/Context;)V

    .line 1248
    new-instance v12, Lcom/vectorwatch/android/models/PushUpdateRoute;

    invoke-direct {v12}, Lcom/vectorwatch/android/models/PushUpdateRoute;-><init>()V

    .line 1249
    .local v12, "pushUpdateRoute":Lcom/vectorwatch/android/models/PushUpdateRoute;
    invoke-virtual {v12, v7}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setAppId(Ljava/lang/Integer;)V

    .line 1250
    invoke-virtual {v12, v10}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setFieldId(Ljava/lang/Integer;)V

    .line 1251
    invoke-virtual {v12, v11}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setWatchfaceId(Ljava/lang/Integer;)V

    .line 1252
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setUpdateData(Ljava/lang/String;)V

    .line 1253
    invoke-virtual {v12, v15}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setStreamType(Ljava/lang/String;)V

    .line 1254
    invoke-virtual {v12, v8}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setChannelUniqueLabel(Ljava/lang/String;)V

    .line 1255
    invoke-virtual {v12, v14}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setSecondsToLive(Ljava/lang/Integer;)V

    .line 1256
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamElementType(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/vectorwatch/android/models/PushUpdateRoute;->setElementType(Ljava/lang/String;)V

    .line 1258
    invoke-interface {v13, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1260
    .end local v12    # "pushUpdateRoute":Lcom/vectorwatch/android/models/PushUpdateRoute;
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1263
    .end local v7    # "appId":Ljava/lang/Integer;
    .end local v8    # "channelLabel":Ljava/lang/String;
    .end local v10    # "elementId":Ljava/lang/Integer;
    .end local v11    # "faceId":Ljava/lang/Integer;
    .end local v14    # "secondsToLive":Ljava/lang/Integer;
    .end local v15    # "streamType":Ljava/lang/String;
    .end local v16    # "updateValue":Ljava/lang/String;
    :cond_2
    if-eqz v9, :cond_3

    .line 1264
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1267
    :cond_3
    return-object v13
.end method

.method public static hardDeleteStream(Ljava/lang/String;Landroid/content/Context;)I
    .locals 5
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 327
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 328
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stream_uuid=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 330
    .local v0, "deletedCount":I
    return v0
.end method

.method private static isStreamInDatabase(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 9
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1176
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1177
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "stream_uuid"

    aput-object v3, v2, v8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_state=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1181
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1182
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v7

    .line 1190
    :goto_0
    return v1

    .line 1186
    :cond_0
    if-eqz v6, :cond_1

    .line 1187
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move v1, v8

    .line 1190
    goto :goto_0
.end method

.method private static isStreamInList(Lcom/vectorwatch/android/models/Stream;Ljava/util/List;)Z
    .locals 4
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/Stream;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1159
    .local p1, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Stream;

    .line 1160
    .local v0, "s":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1161
    const/4 v1, 0x1

    .line 1165
    .end local v0    # "s":Lcom/vectorwatch/android/models/Stream;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static markStreamForRemoval(Ljava/lang/String;Landroid/content/Context;)V
    .locals 6
    .param p0, "streamUuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1348
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1349
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1350
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "stream_state"

    sget-object v4, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_DELETE:Lcom/vectorwatch/android/models/Stream$State;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1352
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_uuid = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1354
    .local v1, "rowsChangedCount":I
    return-void
.end method

.method public static markStreamForUnsubscribe(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/content/Context;)V
    .locals 6
    .param p0, "streamUuid"    # Ljava/lang/String;
    .param p1, "appId"    # Ljava/lang/Integer;
    .param p2, "faceId"    # Ljava/lang/Integer;
    .param p3, "elementId"    # Ljava/lang/Integer;
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 1365
    sget-object v3, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Debug - Unsubscribing from stream from mAppId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " faceId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1367
    invoke-virtual {p4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1368
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1369
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "stream_state"

    sget-object v4, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_UNSUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1373
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_uuid = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_active_face"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_active_app"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_active_field"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1380
    .local v1, "rowsChangedCount":I
    sget-object v3, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Debug - Marked for unsubscribe rows changed = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1381
    return-void
.end method

.method public static saveStream(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;)Z
    .locals 23
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    sget-object v20, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Saving stream to DB. Stream name = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " | Stream id = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getId()Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 54
    if-nez p0, :cond_0

    .line 55
    sget-object v20, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v21, "Stream couldn\'t be saved because it is null!"

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 56
    const/16 v20, 0x0

    .line 126
    :goto_0
    return v20

    .line 59
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v20

    if-nez v20, :cond_1

    .line 60
    sget-object v20, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v21, "Stream couldn\'t be saved because version is null!"

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 61
    const/16 v20, 0x0

    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 65
    .local v5, "resolver":Landroid/content/ContentResolver;
    new-instance v4, Lcom/google/gson/Gson;

    invoke-direct {v4}, Lcom/google/gson/Gson;-><init>()V

    .line 68
    .local v4, "gson":Lcom/google/gson/Gson;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v7

    .line 69
    .local v7, "settings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    invoke-virtual {v4, v7}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 74
    .local v14, "streamSettingsJsonAsString":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getId()Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 75
    .local v10, "streamId":I
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v13

    .line 76
    .local v13, "streamName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 77
    .local v18, "streamVersion":I
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getDescription()Ljava/lang/String;

    move-result-object v9

    .line 78
    .local v9, "streamDescription":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getImg()Ljava/lang/String;

    move-result-object v11

    .line 79
    .local v11, "streamImg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v16

    .line 80
    .local v16, "streamType":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getSupportedType()Ljava/lang/String;

    move-result-object v15

    .line 81
    .local v15, "streamSupportedType":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v17

    .line 82
    .local v17, "streamUuid":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getLabelText()Ljava/lang/String;

    move-result-object v12

    .line 83
    .local v12, "streamLabel":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getContentVersion()Ljava/lang/String;

    move-result-object v8

    .line 87
    .local v8, "streamContVersion":Ljava/lang/String;
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 88
    .local v19, "values":Landroid/content/ContentValues;
    const-string v20, "stream_name"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v20, "stream_version"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 90
    const-string v20, "stream_description"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v20, "stream_img"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v20, "stream_type"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v20, "stream_supported_type"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v20, "stream_id"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 95
    const-string v20, "stream_uuid"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v20, "stream_label"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v20, "stream_cont_version"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v20, "stream_possible_settings"

    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 99
    sget-object v20, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Stream possible settings: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 100
    const-string v20, "stream_settings"

    const/16 v21, 0x0

    move/from16 v0, v21

    new-array v0, v0, [B

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 102
    const-string v20, "stream_state"

    sget-object v21, Lcom/vectorwatch/android/models/Stream$State;->LOCKER:Lcom/vectorwatch/android/models/Stream$State;

    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v20, "stream_dirty"

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 104
    const-string v20, "stream_active_face"

    const/16 v21, -0x2

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 105
    const-string v20, "stream_active_field"

    const/16 v21, -0x2

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 106
    const-string v20, "stream_active_app"

    const/16 v21, -0x2

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 107
    const-string v20, "stream_channel_label"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v20, "stream_dynamic"

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getDynamic()Z

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 109
    const-string v20, "needs_auth"

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/Stream;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 111
    const/4 v6, 0x0

    .line 114
    .local v6, "row":Landroid/net/Uri;
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->isStreamInDatabase(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 115
    sget-object v20, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "stream_uuid = \'"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "\'"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 117
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 119
    :cond_2
    sget-object v20, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v6

    .line 122
    if-nez v6, :cond_3

    .line 123
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 126
    :cond_3
    const/16 v20, 0x1

    goto/16 :goto_0
.end method

.method public static setAuthInvalidated(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "invalidated"    # Z

    .prologue
    .line 230
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 232
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 233
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "auth_invalidated"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 235
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stream_uuid=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 237
    return-void
.end method

.method public static setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I
    .locals 6
    .param p0, "channelLabel"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "secondsToLive"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 1194
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1195
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1196
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "stream_update_value"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    const-string v3, "stream_dirty"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1198
    const-string v3, "stream_seconds_to_live"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1199
    const-string v3, "stream_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1201
    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_STREAM:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stream_channel_label = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_state"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/models/Stream$State;->INSTALLED:Lcom/vectorwatch/android/models/Stream$State;

    .line 1204
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/Stream$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 1201
    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1205
    .local v1, "rowsChangedCount":I
    sget-object v3, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Update channel data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | rows changing = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1207
    return v1
.end method
