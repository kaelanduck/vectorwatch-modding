.class public Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;
.super Ljava/lang/Object;
.source "CloudAppsDatabaseHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;
    }
.end annotation


# static fields
.field public static final MODIFIED:I = 0x1

.field public static final NOT_DEFINED:I = -0x2

.field public static final NOT_MODIFIED:I

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1234
    return-void
.end method

.method public static getAllDownloadedApps(Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 583
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 585
    .local v8, "idsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 586
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "app_state"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "cloud_app_modified"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "app_global_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_state!=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    .line 589
    invoke-virtual {v5}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 586
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 591
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 596
    :cond_0
    const-string v1, "app_global_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 597
    .local v7, "id":I
    const-string v1, "app_state"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 599
    .local v9, "state":Ljava/lang/String;
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 600
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 602
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 605
    .end local v7    # "id":I
    .end local v9    # "state":Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 606
    return-object v8
.end method

.method public static getAllInstalledApps(Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 550
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 552
    .local v9, "uuidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 553
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "app_state"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "cloud_app_modified"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "app_uuid"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_state!=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    .line 556
    invoke-virtual {v5}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 553
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 558
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 563
    :cond_0
    const-string v1, "app_uuid"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 564
    .local v8, "uuid":Ljava/lang/String;
    const-string v1, "app_state"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 566
    .local v7, "state":Ljava/lang/String;
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 567
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 569
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 572
    .end local v7    # "state":Ljava/lang/String;
    .end local v8    # "uuid":Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 573
    return-object v9
.end method

.method public static getAppId(Ljava/lang/String;Landroid/content/Context;)I
    .locals 8
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1169
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1170
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "app_uuid"

    aput-object v3, v2, v7

    const/4 v3, 0x1

    const-string v5, "app_global_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_uuid = \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1174
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1177
    const-string v1, "app_global_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1179
    .local v7, "id":I
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1185
    .end local v7    # "id":I
    :goto_0
    return v7

    .line 1183
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getAppIdsInRunningOrder(Landroid/content/Context;)Ljava/util/List;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 994
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 996
    .local v8, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 997
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "app_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "app_global_id"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "app_state"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "cloud_app_order_index"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app_state = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    .line 1000
    invoke-virtual {v4}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "cloud_app_order_index"

    .line 997
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1003
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1008
    :cond_0
    const-string v1, "app_name"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1009
    .local v9, "name":Ljava/lang/String;
    const-string v1, "app_global_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1010
    .local v7, "id":I
    const-string v1, "cloud_app_order_index"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1015
    .local v10, "orderIndex":I
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1016
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1018
    .end local v7    # "id":I
    .end local v9    # "name":Ljava/lang/String;
    .end local v10    # "orderIndex":I
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1021
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 1022
    .local v7, "id":Ljava/lang/Integer;
    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAppsInOrder "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v8, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 1024
    .end local v7    # "id":Ljava/lang/Integer;
    :cond_2
    return-object v8
.end method

.method public static getAppSummaryWithId(ILandroid/content/Context;)Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 17
    .param p0, "id"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 810
    new-instance v10, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v10}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 812
    .local v10, "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 813
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/16 v3, 0x11

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "app_name"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "app_description"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "app_global_id"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "app_rating"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "app_state"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "cloud_app_modified"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "app_image"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "app_edit_image"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "app_type"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "cloud_app_order_index"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string v5, "app_uuid"

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "auth_invalidated"

    aput-object v5, v3, v4

    const/16 v4, 0xd

    const-string v5, "needs_auth"

    aput-object v5, v3, v4

    const/16 v4, 0xe

    const-string v5, "needs_location"

    aput-object v5, v3, v4

    const/16 v4, 0xf

    const-string v5, "dynamic_settings"

    aput-object v5, v3, v4

    const/16 v4, 0x10

    const-string v5, "user_settings"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_global_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "cloud_app_order_index"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 825
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_0

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 835
    const-string v2, "app_name"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 836
    .local v15, "name":Ljava/lang/String;
    const-string v2, "app_description"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 837
    .local v12, "description":Ljava/lang/String;
    const-string v2, "app_rating"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 838
    .local v16, "rating":I
    const-string v2, "app_image"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 839
    .local v14, "img":Ljava/lang/String;
    const-string v2, "app_edit_image"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 840
    .local v13, "editImg":Ljava/lang/String;
    const-string v2, "app_type"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 841
    .local v7, "appType":Ljava/lang/String;
    const-string v2, "app_uuid"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 842
    .local v8, "appUuid":Ljava/lang/String;
    const-string v2, "auth_invalidated"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_1

    const/4 v9, 0x1

    .line 844
    .local v9, "authInvalidated":Z
    :goto_0
    invoke-virtual {v10, v15}, Lcom/vectorwatch/android/models/CloudElementSummary;->setName(Ljava/lang/String;)V

    .line 845
    invoke-virtual {v10, v12}, Lcom/vectorwatch/android/models/CloudElementSummary;->setDescription(Ljava/lang/String;)V

    .line 846
    move/from16 v0, p0

    invoke-virtual {v10, v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 847
    move/from16 v0, v16

    int-to-float v2, v0

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRating(F)V

    .line 848
    invoke-virtual {v10, v14}, Lcom/vectorwatch/android/models/CloudElementSummary;->setImage(Ljava/lang/String;)V

    .line 849
    invoke-virtual {v10, v13}, Lcom/vectorwatch/android/models/CloudElementSummary;->setEditImg(Ljava/lang/String;)V

    .line 850
    invoke-virtual {v10, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setType(Ljava/lang/String;)V

    .line 851
    invoke-virtual {v10, v8}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 852
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 854
    invoke-static {v10, v11}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putAdditionalFieldsToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 855
    invoke-static {v10, v11}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putUserSettingsToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 856
    invoke-static {v10, v11}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putAppStateToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 858
    .end local v7    # "appType":Ljava/lang/String;
    .end local v8    # "appUuid":Ljava/lang/String;
    .end local v9    # "authInvalidated":Z
    .end local v12    # "description":Ljava/lang/String;
    .end local v13    # "editImg":Ljava/lang/String;
    .end local v14    # "img":Ljava/lang/String;
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "rating":I
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 860
    return-object v10

    .line 842
    .restart local v7    # "appType":Ljava/lang/String;
    .restart local v8    # "appUuid":Ljava/lang/String;
    .restart local v12    # "description":Ljava/lang/String;
    .restart local v13    # "editImg":Ljava/lang/String;
    .restart local v14    # "img":Ljava/lang/String;
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v16    # "rating":I
    :cond_1
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public static getAppSummaryWithUuid(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/CloudElementSummary;
    .locals 18
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 755
    new-instance v10, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v10}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 757
    .local v10, "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 758
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/16 v3, 0x11

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "app_name"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "app_description"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "app_global_id"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "app_rating"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "app_state"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "cloud_app_modified"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "app_image"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "app_edit_image"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "app_type"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "cloud_app_order_index"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string v5, "app_uuid"

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "auth_invalidated"

    aput-object v5, v3, v4

    const/16 v4, 0xd

    const-string v5, "needs_auth"

    aput-object v5, v3, v4

    const/16 v4, 0xe

    const-string v5, "needs_location"

    aput-object v5, v3, v4

    const/16 v4, 0xf

    const-string v5, "dynamic_settings"

    aput-object v5, v3, v4

    const/16 v4, 0x10

    const-string v5, "user_settings"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_uuid = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "cloud_app_order_index"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 770
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_0

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 780
    const-string v2, "app_global_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 781
    .local v14, "id":I
    const-string v2, "app_name"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 782
    .local v16, "name":Ljava/lang/String;
    const-string v2, "app_description"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 783
    .local v12, "description":Ljava/lang/String;
    const-string v2, "app_rating"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 784
    .local v17, "rating":I
    const-string v2, "app_image"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 785
    .local v15, "img":Ljava/lang/String;
    const-string v2, "app_edit_image"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 786
    .local v13, "editImg":Ljava/lang/String;
    const-string v2, "app_type"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 787
    .local v7, "appType":Ljava/lang/String;
    const-string v2, "app_uuid"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 788
    .local v8, "appUuid":Ljava/lang/String;
    const-string v2, "auth_invalidated"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_1

    const/4 v9, 0x1

    .line 790
    .local v9, "authInvalidated":Z
    :goto_0
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->setName(Ljava/lang/String;)V

    .line 791
    invoke-virtual {v10, v12}, Lcom/vectorwatch/android/models/CloudElementSummary;->setDescription(Ljava/lang/String;)V

    .line 792
    invoke-virtual {v10, v14}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 793
    move/from16 v0, v17

    int-to-float v2, v0

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRating(F)V

    .line 794
    invoke-virtual {v10, v15}, Lcom/vectorwatch/android/models/CloudElementSummary;->setImage(Ljava/lang/String;)V

    .line 795
    invoke-virtual {v10, v13}, Lcom/vectorwatch/android/models/CloudElementSummary;->setEditImg(Ljava/lang/String;)V

    .line 796
    invoke-virtual {v10, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setType(Ljava/lang/String;)V

    .line 797
    invoke-virtual {v10, v8}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 798
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 800
    invoke-static {v10, v11}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putAdditionalFieldsToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 801
    invoke-static {v10, v11}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putUserSettingsToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 802
    invoke-static {v10, v11}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putAppStateToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 804
    .end local v7    # "appType":Ljava/lang/String;
    .end local v8    # "appUuid":Ljava/lang/String;
    .end local v9    # "authInvalidated":Z
    .end local v12    # "description":Ljava/lang/String;
    .end local v13    # "editImg":Ljava/lang/String;
    .end local v14    # "id":I
    .end local v15    # "img":Ljava/lang/String;
    .end local v16    # "name":Ljava/lang/String;
    .end local v17    # "rating":I
    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 806
    return-object v10

    .line 788
    .restart local v7    # "appType":Ljava/lang/String;
    .restart local v8    # "appUuid":Ljava/lang/String;
    .restart local v12    # "description":Ljava/lang/String;
    .restart local v13    # "editImg":Ljava/lang/String;
    .restart local v14    # "id":I
    .restart local v15    # "img":Ljava/lang/String;
    .restart local v16    # "name":Ljava/lang/String;
    .restart local v17    # "rating":I
    :cond_1
    const/4 v9, 0x0

    goto :goto_0
.end method

.method public static getAppType(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;
    .locals 8
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 352
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 353
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "app_type"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "app_global_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_global_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 357
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 358
    .local v6, "appType":Ljava/lang/String;
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 361
    const-string v1, "app_type"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 367
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 369
    if-nez v6, :cond_2

    .line 382
    :cond_1
    :goto_0
    return-object v4

    .line 373
    :cond_2
    sget-object v1, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->CLOUD:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 374
    sget-object v4, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->CLOUD:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    goto :goto_0

    .line 375
    :cond_3
    sget-object v1, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 376
    sget-object v4, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    goto :goto_0

    .line 377
    :cond_4
    sget-object v1, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->SYSTEM_WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 378
    sget-object v4, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->SYSTEM_WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    goto :goto_0

    .line 379
    :cond_5
    sget-object v1, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->SYSTEM_APP:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 380
    sget-object v4, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->SYSTEM_APP:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    goto :goto_0
.end method

.method public static getAppUuid(ILandroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 1189
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1190
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "app_uuid"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "app_global_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_global_id = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1194
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1197
    const-string v1, "app_uuid"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1199
    .local v7, "uuid":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1205
    .end local v7    # "uuid":Ljava/lang/String;
    :goto_0
    return-object v7

    .line 1203
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v7, v4

    .line 1205
    goto :goto_0
.end method

.method public static getAppUuidsInRunningOrder(Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 959
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 961
    .local v8, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 962
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "app_name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "app_uuid"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "app_state"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "cloud_app_order_index"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app_state = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    .line 965
    invoke-virtual {v4}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "cloud_app_order_index"

    .line 962
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 968
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 972
    :cond_0
    const-string v1, "app_uuid"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 975
    .local v7, "id":Ljava/lang/String;
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 976
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 978
    .end local v7    # "id":Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 981
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 982
    .restart local v7    # "id":Ljava/lang/String;
    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAppsInOrder "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v8, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 984
    .end local v7    # "id":Ljava/lang/String;
    :cond_2
    return-object v8
.end method

.method public static getAppsWithGivenState(Landroid/content/Context;Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appState"    # Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 864
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppsWithGivenState(Landroid/content/Context;Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static getAppsWithGivenState(Landroid/content/Context;Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appState"    # Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;
    .param p2, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 868
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 870
    .local v11, "cloudElementSummaryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 871
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/16 v3, 0x11

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "app_name"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "app_description"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "app_global_id"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "app_rating"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "app_state"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "cloud_app_modified"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "app_image"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "app_edit_image"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "app_type"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "cloud_app_order_index"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string v5, "app_uuid"

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "auth_invalidated"

    aput-object v5, v3, v4

    const/16 v4, 0xd

    const-string v5, "needs_location"

    aput-object v5, v3, v4

    const/16 v4, 0xe

    const-string v5, "needs_auth"

    aput-object v5, v3, v4

    const/16 v4, 0xf

    const-string v5, "user_settings"

    aput-object v5, v3, v4

    const/16 v4, 0x10

    const-string v5, "dynamic_settings"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_state = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 881
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p2, :cond_3

    const-string v4, ""

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "cloud_app_order_index"

    .line 871
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 885
    .local v12, "cursor":Landroid/database/Cursor;
    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 897
    :cond_0
    const-string v2, "app_name"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 898
    .local v17, "name":Ljava/lang/String;
    const-string v2, "app_description"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 899
    .local v13, "description":Ljava/lang/String;
    const-string v2, "app_rating"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 900
    .local v18, "rating":I
    const-string v2, "app_global_id"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 901
    .local v15, "id":I
    const-string v2, "app_state"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 902
    .local v19, "state":Ljava/lang/String;
    const-string v2, "app_image"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 903
    .local v16, "img":Ljava/lang/String;
    const-string v2, "app_edit_image"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 904
    .local v14, "editImg":Ljava/lang/String;
    const-string v2, "app_type"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 905
    .local v7, "appType":Ljava/lang/String;
    const-string v2, "app_uuid"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 906
    .local v8, "appUuid":Ljava/lang/String;
    const-string v2, "auth_invalidated"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_4

    const/4 v9, 0x1

    .line 909
    .local v9, "authInvalidated":Z
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 910
    new-instance v10, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v10}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 911
    .local v10, "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->setName(Ljava/lang/String;)V

    .line 912
    invoke-virtual {v10, v13}, Lcom/vectorwatch/android/models/CloudElementSummary;->setDescription(Ljava/lang/String;)V

    .line 913
    invoke-virtual {v10, v15}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 914
    move/from16 v0, v18

    int-to-float v2, v0

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRating(F)V

    .line 915
    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->setImage(Ljava/lang/String;)V

    .line 916
    invoke-virtual {v10, v14}, Lcom/vectorwatch/android/models/CloudElementSummary;->setEditImg(Ljava/lang/String;)V

    .line 917
    invoke-virtual {v10, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setType(Ljava/lang/String;)V

    .line 918
    invoke-virtual {v10, v8}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUuid(Ljava/lang/String;)V

    .line 919
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 921
    invoke-static {v10, v12}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putAdditionalFieldsToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 922
    invoke-static {v10, v12}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putUserSettingsToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 923
    invoke-static {v10, v12}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putAppStateToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 925
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 927
    .end local v10    # "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    :cond_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 929
    .end local v7    # "appType":Ljava/lang/String;
    .end local v8    # "appUuid":Ljava/lang/String;
    .end local v9    # "authInvalidated":Z
    .end local v13    # "description":Ljava/lang/String;
    .end local v14    # "editImg":Ljava/lang/String;
    .end local v15    # "id":I
    .end local v16    # "img":Ljava/lang/String;
    .end local v17    # "name":Ljava/lang/String;
    .end local v18    # "rating":I
    .end local v19    # "state":Ljava/lang/String;
    :cond_2
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 931
    return-object v11

    .line 881
    .end local v12    # "cursor":Landroid/database/Cursor;
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " AND app_uuid=\'"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "\'"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 906
    .restart local v7    # "appType":Ljava/lang/String;
    .restart local v8    # "appUuid":Ljava/lang/String;
    .restart local v12    # "cursor":Landroid/database/Cursor;
    .restart local v13    # "description":Ljava/lang/String;
    .restart local v14    # "editImg":Ljava/lang/String;
    .restart local v15    # "id":I
    .restart local v16    # "img":Ljava/lang/String;
    .restart local v17    # "name":Ljava/lang/String;
    .restart local v18    # "rating":I
    .restart local v19    # "state":Ljava/lang/String;
    :cond_4
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public static getContentForApp(ILandroid/content/Context;)[B
    .locals 8
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 1209
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1211
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "app_global_id"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "app_watchfaces"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_global_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1215
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 1218
    .local v6, "content":[B
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1219
    const-string v1, "app_watchfaces"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 1222
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1224
    return-object v6
.end method

.method public static getDynamicResource(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DynamicResource;
    .locals 13
    .param p0, "resourceId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 1144
    const/4 v8, 0x0

    .line 1146
    .local v8, "resource":Lcom/vectorwatch/android/models/DynamicResource;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1147
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_RESOURCES:Landroid/net/Uri;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "resource_global_id="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1150
    .local v9, "resourceCursor":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1152
    const-string v11, "resource_global_id"

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1153
    .local v2, "id":I
    const-string v11, "resource_size"

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getShort(I)S

    move-result v3

    .line 1154
    .local v3, "size":S
    const-string v11, "resource_compressed_size"

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getShort(I)S

    move-result v6

    .line 1155
    .local v6, "compressedSize":S
    const-string v11, "resource_content"

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1157
    .local v4, "content":Ljava/lang/String;
    const-string v11, "resource_compressed"

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v9, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1158
    .local v7, "comp":I
    if-ne v7, v10, :cond_0

    move v5, v10

    .line 1160
    .local v5, "compressed":Z
    :goto_0
    sget-object v10, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "added resource "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " to list"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1161
    new-instance v1, Lcom/vectorwatch/android/models/DynamicResource;

    invoke-direct/range {v1 .. v6}, Lcom/vectorwatch/android/models/DynamicResource;-><init>(ISLjava/lang/String;ZS)V

    .line 1164
    .end local v2    # "id":I
    .end local v3    # "size":S
    .end local v4    # "content":Ljava/lang/String;
    .end local v5    # "compressed":Z
    .end local v6    # "compressedSize":S
    .end local v7    # "comp":I
    .end local v8    # "resource":Lcom/vectorwatch/android/models/DynamicResource;
    .local v1, "resource":Lcom/vectorwatch/android/models/DynamicResource;
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1165
    return-object v1

    .line 1158
    .end local v1    # "resource":Lcom/vectorwatch/android/models/DynamicResource;
    .restart local v2    # "id":I
    .restart local v3    # "size":S
    .restart local v4    # "content":Ljava/lang/String;
    .restart local v6    # "compressedSize":S
    .restart local v7    # "comp":I
    .restart local v8    # "resource":Lcom/vectorwatch/android/models/DynamicResource;
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .end local v2    # "id":I
    .end local v3    # "size":S
    .end local v4    # "content":Ljava/lang/String;
    .end local v6    # "compressedSize":S
    .end local v7    # "comp":I
    :cond_1
    move-object v1, v8

    .end local v8    # "resource":Lcom/vectorwatch/android/models/DynamicResource;
    .restart local v1    # "resource":Lcom/vectorwatch/android/models/DynamicResource;
    goto :goto_1
.end method

.method public static getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .locals 15
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 263
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Get full app with app id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 264
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 265
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "app_global_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "app_watchfaces"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "user_settings"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app_global_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 270
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v12, Lcom/google/gson/Gson;

    invoke-direct {v12}, Lcom/google/gson/Gson;-><init>()V

    .line 272
    .local v12, "gson":Lcom/google/gson/Gson;
    const/4 v10, 0x0

    .line 273
    .local v10, "fullApp":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    const-string v1, "app_watchfaces"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 278
    .local v11, "fullAppBytes":[B
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v11}, Ljava/lang/String;-><init>([B)V

    .line 280
    .local v7, "contentString":Ljava/lang/String;
    const-class v1, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    invoke-virtual {v12, v7, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "fullApp":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    check-cast v10, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 286
    .end local v7    # "contentString":Ljava/lang/String;
    .end local v11    # "fullAppBytes":[B
    .restart local v10    # "fullApp":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    :cond_0
    const-string v1, "user_settings"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 287
    .local v6, "columnIndex":I
    if-ltz v6, :cond_1

    .line 289
    :try_start_0
    invoke-interface {v9, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    .line 290
    .local v8, "credentialBytes":[B
    if-eqz v8, :cond_1

    array-length v1, v8

    if-lez v1, :cond_1

    .line 291
    new-instance v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$1;

    invoke-direct {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$1;-><init>()V

    .line 292
    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v13

    .line 293
    .local v13, "stringStringMap":Ljava/lang/reflect/Type;
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>([B)V

    .line 294
    .restart local v7    # "contentString":Ljava/lang/String;
    invoke-virtual {v12, v7, v13}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map;

    .line 295
    .local v14, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v10, v14}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->setUserSettings(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    .end local v7    # "contentString":Ljava/lang/String;
    .end local v8    # "credentialBytes":[B
    .end local v13    # "stringStringMap":Ljava/lang/reflect/Type;
    .end local v14    # "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 305
    return-object v10

    .line 298
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static getLockerEntriesByType(Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 18
    .param p0, "entryType"    # Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1067
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1069
    .local v10, "cloudElementSummaryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1071
    .local v1, "resolver":Landroid/content/ContentResolver;
    const/4 v11, 0x0

    .line 1073
    .local v11, "cursor":Landroid/database/Cursor;
    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$3;->$SwitchMap$com$vectorwatch$android$models$DownloadedAppContainer$StoreAppType:[I

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1098
    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v3, "Should not get here. Check entry type for Locker."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1101
    :goto_0
    if-eqz v11, :cond_2

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1112
    :cond_0
    const-string v2, "app_name"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1113
    .local v15, "name":Ljava/lang/String;
    const-string v2, "app_description"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1114
    .local v12, "description":Ljava/lang/String;
    const-string v2, "app_rating"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 1115
    .local v16, "rating":I
    const-string v2, "app_global_id"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 1116
    .local v13, "id":I
    const-string v2, "app_state"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1117
    .local v17, "state":Ljava/lang/String;
    const-string v2, "app_image"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1118
    .local v14, "img":Ljava/lang/String;
    const-string v2, "app_type"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1119
    .local v7, "appType":Ljava/lang/String;
    const-string v2, "auth_invalidated"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_3

    const/4 v8, 0x1

    .line 1122
    .local v8, "authInvalidated":Z
    :goto_1
    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1123
    new-instance v9, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-direct {v9}, Lcom/vectorwatch/android/models/CloudElementSummary;-><init>()V

    .line 1124
    .local v9, "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {v9, v15}, Lcom/vectorwatch/android/models/CloudElementSummary;->setName(Ljava/lang/String;)V

    .line 1125
    invoke-virtual {v9, v12}, Lcom/vectorwatch/android/models/CloudElementSummary;->setDescription(Ljava/lang/String;)V

    .line 1126
    invoke-virtual {v9, v13}, Lcom/vectorwatch/android/models/CloudElementSummary;->setId(I)V

    .line 1127
    move/from16 v0, v16

    int-to-float v2, v0

    invoke-virtual {v9, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRating(F)V

    .line 1128
    invoke-virtual {v9, v14}, Lcom/vectorwatch/android/models/CloudElementSummary;->setImage(Ljava/lang/String;)V

    .line 1129
    invoke-virtual {v9, v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->setType(Ljava/lang/String;)V

    .line 1130
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAuthInvalidated(Ljava/lang/Boolean;)V

    .line 1132
    invoke-static {v9, v11}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putAdditionalFieldsToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 1133
    invoke-static {v9, v11}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->putAppStateToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V

    .line 1135
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1137
    .end local v9    # "cloudElementSummary":Lcom/vectorwatch/android/models/CloudElementSummary;
    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1139
    .end local v7    # "appType":Ljava/lang/String;
    .end local v8    # "authInvalidated":Z
    .end local v12    # "description":Ljava/lang/String;
    .end local v13    # "id":I
    .end local v14    # "img":Ljava/lang/String;
    .end local v15    # "name":Ljava/lang/String;
    .end local v16    # "rating":I
    .end local v17    # "state":Ljava/lang/String;
    :cond_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1140
    return-object v10

    .line 1075
    :pswitch_0
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/16 v3, 0xc

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "app_name"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "app_description"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "app_global_id"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "app_rating"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "app_state"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "cloud_app_modified"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "app_image"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "app_type"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "auth_invalidated"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "needs_auth"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string v5, "needs_location"

    aput-object v5, v3, v4

    const-string v4, "app_type=\'WATCHFACE\' or app_type=\'SYSTEM\'"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1085
    goto/16 :goto_0

    .line 1087
    :pswitch_1
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/16 v3, 0xd

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "app_name"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "app_description"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "app_global_id"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "app_rating"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "app_state"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "cloud_app_modified"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "app_image"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "app_type"

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "auth_invalidated"

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const-string v5, "needs_auth"

    aput-object v5, v3, v4

    const/16 v4, 0xb

    const-string v5, "dynamic_settings"

    aput-object v5, v3, v4

    const/16 v4, 0xc

    const-string v5, "needs_location"

    aput-object v5, v3, v4

    const-string v4, "app_type=\'CLOUD\'"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1096
    goto/16 :goto_0

    .line 1119
    .restart local v7    # "appType":Ljava/lang/String;
    .restart local v12    # "description":Ljava/lang/String;
    .restart local v13    # "id":I
    .restart local v14    # "img":Ljava/lang/String;
    .restart local v15    # "name":Ljava/lang/String;
    .restart local v16    # "rating":I
    .restart local v17    # "state":Ljava/lang/String;
    :cond_3
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 1073
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getModifiedCloudAppsList(Landroid/content/Context;)Ljava/util/HashMap;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Lcom/vectorwatch/android/models/AppIdentification;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 515
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 517
    .local v8, "modifiedAppsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vectorwatch/android/models/AppIdentification;Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 518
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "app_state"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "cloud_app_modified"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "app_global_id"

    aput-object v5, v2, v3

    const/4 v3, 0x3

    const-string v5, "app_uuid"

    aput-object v5, v2, v3

    const-string v3, "cloud_app_modified=1"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 523
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 530
    :cond_0
    const-string v1, "app_global_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 531
    .local v7, "id":I
    const-string v1, "app_state"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 532
    .local v9, "state":Ljava/lang/String;
    const-string v1, "app_uuid"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 534
    .local v10, "uuid":Ljava/lang/String;
    new-instance v1, Lcom/vectorwatch/android/models/AppIdentification;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v2, v10}, Lcom/vectorwatch/android/models/AppIdentification;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-virtual {v8, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 537
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 540
    .end local v7    # "id":I
    .end local v9    # "state":Ljava/lang/String;
    .end local v10    # "uuid":Ljava/lang/String;
    :cond_1
    return-object v8
.end method

.method public static getResourcesForCloudApp(Ljava/lang/Integer;Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p0, "appId"    # Ljava/lang/Integer;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/DynamicResource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1242
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1243
    .local v7, "dynamicResources":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DynamicResource;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_APPS_RESOURCES:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "apps_resources_res_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "apps_resources_app_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1248
    .local v6, "appsResCursor":Landroid/database/Cursor;
    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1249
    const-string v0, "apps_resources_res_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 1250
    .local v8, "resId":I
    invoke-static {v8, p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getDynamicResource(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DynamicResource;

    move-result-object v9

    .line 1251
    .local v9, "resource":Lcom/vectorwatch/android/models/DynamicResource;
    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1255
    .end local v8    # "resId":I
    .end local v9    # "resource":Lcom/vectorwatch/android/models/DynamicResource;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1257
    return-object v7
.end method

.method public static hardDeleteApp(ILandroid/content/Context;)V
    .locals 5
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 430
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 433
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app_global_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 435
    .local v0, "deletedAppsCount":I
    return-void
.end method

.method public static hardDeleteAppByUuid(Ljava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p0, "appUuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 444
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 447
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app_uuid=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 449
    .local v0, "deletedAppsCount":I
    return-void
.end method

.method private static isAppInDatabase(ILandroid/content/Context;)Z
    .locals 9
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 309
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 310
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "app_global_id"

    aput-object v3, v2, v8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_global_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 314
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 315
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v7

    .line 319
    :goto_0
    return v1

    .line 318
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v8

    .line 319
    goto :goto_0
.end method

.method private static isResourceForAppInDatabase(IILandroid/content/Context;)Z
    .locals 9
    .param p0, "resourceId"    # I
    .param p1, "appId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 337
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 338
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_APPS_RESOURCES:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "apps_resources_app_id"

    aput-object v3, v2, v8

    const-string v3, "apps_resources_res_id"

    aput-object v3, v2, v7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "apps_resources_app_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "apps_resources_res_id"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 343
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 344
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v7

    .line 348
    :goto_0
    return v1

    .line 347
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v8

    .line 348
    goto :goto_0
.end method

.method private static isResourceInDatabase(ILandroid/content/Context;)Z
    .locals 9
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 323
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 324
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_RESOURCES:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "resource_global_id"

    aput-object v3, v2, v8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resource_global_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 328
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 329
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v7

    .line 333
    :goto_0
    return v1

    .line 332
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v8

    .line 333
    goto :goto_0
.end method

.method public static markAppStateChanging(IILcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)V
    .locals 5
    .param p0, "appId"    # I
    .param p1, "positionInList"    # I
    .param p2, "stateInChanging"    # Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 1050
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1051
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1052
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "cloud_app_order_index"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1053
    const-string v2, "app_state"

    invoke-virtual {p2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app_global_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1057
    return-void
.end method

.method public static markCloudAppAsUnmodified(Ljava/lang/String;Landroid/content/Context;)V
    .locals 12
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 626
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 627
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "cloud_app_modified"

    aput-object v3, v2, v10

    const/4 v3, 0x1

    const-string v5, "app_type"

    aput-object v5, v2, v3

    const-string v3, "app_global_id"

    aput-object v3, v2, v11

    const/4 v3, 0x3

    const-string v5, "app_uuid"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_uuid=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 632
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 633
    .local v6, "crt":I
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 637
    :cond_0
    add-int/lit8 v6, v6, 0x1

    .line 639
    const-string v1, "cloud_app_modified"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 641
    .local v8, "modified":I
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 643
    .local v9, "values":Landroid/content/ContentValues;
    if-nez v8, :cond_1

    .line 644
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The app with id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " should have been marked as modified in database"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 647
    :cond_1
    const-string v1, "cloud_app_modified"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 649
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CLOUD CALL - Marked as unmodified "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 650
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "app_uuid=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v9, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 654
    if-lt v6, v11, :cond_2

    .line 655
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 656
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v2, "The database contains multiple entries for the same cloud app. This should not happen."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 663
    .end local v8    # "modified":I
    .end local v9    # "values":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 659
    .restart local v8    # "modified":I
    .restart local v9    # "values":Landroid/content/ContentValues;
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 662
    .end local v8    # "modified":I
    .end local v9    # "values":Landroid/content/ContentValues;
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static printCloudAppsDatabase(Landroid/content/Context;)V
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v14, 0x1

    const/4 v3, 0x0

    .line 669
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 670
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "app_name"

    aput-object v5, v2, v4

    const-string v4, "app_description"

    aput-object v4, v2, v14

    const/4 v4, 0x2

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "app_global_id"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "app_type"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "app_state"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "cloud_app_modified"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 675
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 682
    const/4 v6, 0x0

    .line 684
    .local v6, "crt":I
    :cond_0
    add-int/lit8 v6, v6, 0x1

    .line 685
    const-string v1, "app_name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 686
    .local v11, "name":Ljava/lang/String;
    const-string v1, "app_description"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 687
    .local v8, "description":Ljava/lang/String;
    const-string v1, "app_type"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 688
    .local v13, "type":Ljava/lang/String;
    const-string v1, "app_global_id"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 689
    .local v9, "id":I
    const-string v1, "app_state"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 690
    .local v12, "state":Ljava/lang/String;
    const-string v1, "cloud_app_modified"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 692
    .local v10, "modified":I
    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cloud app "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-ne v10, v14, :cond_2

    const-string v1, "modified"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 694
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 696
    .end local v6    # "crt":I
    .end local v8    # "description":Ljava/lang/String;
    .end local v9    # "id":I
    .end local v10    # "modified":I
    .end local v11    # "name":Ljava/lang/String;
    .end local v12    # "state":Ljava/lang/String;
    .end local v13    # "type":Ljava/lang/String;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 697
    return-void

    .line 692
    .restart local v6    # "crt":I
    .restart local v8    # "description":Ljava/lang/String;
    .restart local v9    # "id":I
    .restart local v10    # "modified":I
    .restart local v11    # "name":Ljava/lang/String;
    .restart local v12    # "state":Ljava/lang/String;
    .restart local v13    # "type":Ljava/lang/String;
    :cond_2
    const-string v1, "unmodified"

    goto :goto_0
.end method

.method public static putAdditionalFieldsToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V
    .locals 4
    .param p0, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 700
    const-string v1, "needs_auth"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 701
    .local v0, "columnIndex":I
    if-ltz v0, :cond_0

    .line 702
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_3

    move v1, v2

    :goto_0
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRequireUserAuth(Z)V

    .line 705
    :cond_0
    const-string v1, "needs_location"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 706
    if-ltz v0, :cond_1

    .line 707
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setRequireLocation(Z)V

    .line 710
    :cond_1
    const-string v1, "dynamic_settings"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 711
    if-ltz v0, :cond_2

    .line 712
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_5

    :goto_2
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->setHasDynamicSettings(Z)V

    .line 714
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 702
    goto :goto_0

    :cond_4
    move v1, v3

    .line 707
    goto :goto_1

    :cond_5
    move v2, v3

    .line 712
    goto :goto_2
.end method

.method public static putAppStateToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V
    .locals 2
    .param p0, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 738
    const-string v1, "app_state"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 739
    .local v0, "state":Ljava/lang/String;
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 740
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAppState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)V

    .line 752
    :cond_0
    :goto_0
    return-void

    .line 741
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 742
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->CHANGE_TO_RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAppState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)V

    goto :goto_0

    .line 743
    :cond_2
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->DISABLED:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 744
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->DISABLED:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAppState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)V

    goto :goto_0

    .line 745
    :cond_3
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 746
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAppState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)V

    goto :goto_0

    .line 747
    :cond_4
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 748
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAppState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)V

    goto :goto_0

    .line 749
    :cond_5
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 750
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->setAppState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;)V

    goto :goto_0
.end method

.method public static putUserSettingsToApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/database/Cursor;)V
    .locals 7
    .param p0, "cloudElementSummary"    # Lcom/vectorwatch/android/models/CloudElementSummary;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 718
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    .line 719
    .local v3, "gson":Lcom/google/gson/Gson;
    const-string v6, "user_settings"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 720
    .local v0, "columnIndex":I
    if-ltz v0, :cond_0

    .line 721
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 722
    .local v1, "content":[B
    if-eqz v1, :cond_0

    array-length v6, v1

    if-lez v6, :cond_0

    .line 723
    new-instance v6, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$2;

    invoke-direct {v6}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$2;-><init>()V

    .line 724
    invoke-virtual {v6}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v4

    .line 725
    .local v4, "stringStringMap":Ljava/lang/reflect/Type;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    .line 727
    .local v2, "contentString":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v3, v2, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    .line 728
    .local v5, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->setUserSettings(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 735
    .end local v1    # "content":[B
    .end local v2    # "contentString":Ljava/lang/String;
    .end local v4    # "stringStringMap":Ljava/lang/reflect/Type;
    .end local v5    # "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-void

    .line 729
    .restart local v1    # "content":[B
    .restart local v2    # "contentString":Ljava/lang/String;
    .restart local v4    # "stringStringMap":Ljava/lang/reflect/Type;
    :catch_0
    move-exception v6

    goto :goto_0
.end method

.method public static saveApp(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)Z
    .locals 34
    .param p0, "app"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    sget-object v30, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Save app - App name, description and id: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " | "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->description:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " | "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-interface/range {v30 .. v31}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 47
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    .line 48
    .local v24, "resolver":Landroid/content/ContentResolver;
    new-instance v18, Lcom/google/gson/Gson;

    invoke-direct/range {v18 .. v18}, Lcom/google/gson/Gson;-><init>()V

    .line 51
    .local v18, "gson":Lcom/google/gson/Gson;
    const/16 v26, 0x0

    .line 53
    .local v26, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/LocalResource;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    move-object/from16 v30, v0

    if-eqz v30, :cond_0

    .line 54
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->localResources:Ljava/util/List;

    move-object/from16 v26, v0

    .line 60
    :cond_0
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 61
    .local v6, "appContentAsJsonToString":Ljava/lang/String;
    sget-object v30, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "appcontentjson="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-interface/range {v30 .. v31}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 64
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 65
    .local v9, "appId":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    .line 66
    .local v10, "appName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->description:Ljava/lang/String;

    .line 67
    .local v7, "appDescription":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->img:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 68
    .local v20, "image":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->appType:Ljava/lang/String;

    .line 69
    .local v12, "appType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->editImg:Ljava/lang/String;

    .line 70
    .local v8, "appEditImage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    .line 72
    .local v13, "appUuid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->rating:F

    move/from16 v30, v0

    move/from16 v0, v30

    float-to-int v0, v0

    move/from16 v21, v0

    .line 75
    .local v21, "rating":I
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 76
    .local v27, "values":Landroid/content/ContentValues;
    const-string v30, "app_name"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v30, "app_description"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v30, "app_image"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v30, "app_rating"

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 80
    const-string v30, "app_type"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v30, "app_global_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 82
    const-string v30, "app_edit_image"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v30, "app_uuid"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v30, "app_watchfaces"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v31

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 86
    const-string v30, "app_state"

    sget-object v31, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual/range {v31 .. v31}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v30, "cloud_app_modified"

    const/16 v31, 0x0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 89
    const-string v30, "needs_auth"

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v31

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 90
    const-string v30, "needs_location"

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->getRequireLocation()Ljava/lang/Boolean;

    move-result-object v31

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 91
    const-string v30, "dynamic_settings"

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/DownloadedAppDetails;->hasDynamicSettings()Ljava/lang/Boolean;

    move-result-object v31

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 93
    const/4 v11, 0x0

    .line 95
    .local v11, "appRow":Landroid/net/Uri;
    move-object/from16 v0, p1

    invoke-static {v9, v0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->isAppInDatabase(ILandroid/content/Context;)Z

    move-result v30

    if-nez v30, :cond_1

    .line 96
    sget-object v30, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v11

    .line 103
    if-nez v11, :cond_2

    .line 104
    const/16 v30, 0x0

    .line 157
    :goto_0
    return v30

    .line 98
    :cond_1
    sget-object v30, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "app_global_id="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    const/16 v32, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v27

    move-object/from16 v3, v31

    move-object/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 100
    const/16 v30, 0x1

    goto :goto_0

    .line 108
    :cond_2
    new-instance v19, Lcom/google/gson/Gson;

    invoke-direct/range {v19 .. v19}, Lcom/google/gson/Gson;-><init>()V

    .line 109
    .local v19, "helperGson":Lcom/google/gson/Gson;
    if-eqz v26, :cond_7

    .line 110
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v30

    :cond_3
    :goto_1
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-eqz v31, :cond_7

    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/vectorwatch/android/models/LocalResource;

    .line 111
    .local v25, "resource":Lcom/vectorwatch/android/models/LocalResource;
    sget-object v31, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "DEBUG Resources to save in DB = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    move-object/from16 v33, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 113
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->compressed:Ljava/lang/Boolean;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v31

    if-eqz v31, :cond_4

    const/4 v15, 0x1

    .line 116
    .local v15, "compressed":I
    :goto_2
    const/16 v31, 0x1

    move/from16 v0, v31

    if-ne v15, v0, :cond_5

    .line 117
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->content:Ljava/lang/String;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-static/range {v31 .. v32}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v31

    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 118
    .local v16, "compressedSize":J
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->decompressedSize:Ljava/lang/Short;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Short;->shortValue()S

    move-result v31

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v28, v0

    .line 124
    .local v28, "size":J
    :goto_3
    new-instance v23, Landroid/content/ContentValues;

    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    .line 125
    .local v23, "resValues":Landroid/content/ContentValues;
    const-string v31, "resource_global_id"

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v32, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 126
    const-string v31, "resource_size"

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 127
    const-string v31, "resource_compressed_size"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 128
    const-string v31, "resource_compressed"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 129
    const-string v31, "resource_content"

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->content:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Integer;->intValue()I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->isResourceInDatabase(ILandroid/content/Context;)Z

    move-result v31

    if-nez v31, :cond_6

    .line 133
    sget-object v31, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_RESOURCES:Landroid/net/Uri;

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v22

    .line 135
    .local v22, "resRow":Landroid/net/Uri;
    if-eqz v22, :cond_3

    .line 139
    sget-object v31, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "APP RESOURCES - Inserted resource "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " into database"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 146
    .end local v22    # "resRow":Landroid/net/Uri;
    :goto_4
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Integer;->intValue()I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p1

    invoke-static {v0, v9, v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->isResourceForAppInDatabase(IILandroid/content/Context;)Z

    move-result v31

    if-nez v31, :cond_3

    .line 147
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 148
    .local v14, "appsResValues":Landroid/content/ContentValues;
    const-string v31, "apps_resources_app_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 149
    const-string v31, "apps_resources_res_id"

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 150
    sget-object v31, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "inserted app resource. app="

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " resId="

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 151
    sget-object v31, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_APPS_RESOURCES:Landroid/net/Uri;

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    invoke-virtual {v0, v1, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_1

    .line 113
    .end local v14    # "appsResValues":Landroid/content/ContentValues;
    .end local v15    # "compressed":I
    .end local v16    # "compressedSize":J
    .end local v23    # "resValues":Landroid/content/ContentValues;
    .end local v28    # "size":J
    :cond_4
    const/4 v15, 0x0

    goto/16 :goto_2

    .line 120
    .restart local v15    # "compressed":I
    :cond_5
    const-wide/16 v16, 0x0

    .line 121
    .restart local v16    # "compressedSize":J
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->content:Ljava/lang/String;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-static/range {v31 .. v32}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v31

    move-object/from16 v0, v31

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v31

    int-to-long v0, v0

    move-wide/from16 v28, v0

    .restart local v28    # "size":J
    goto/16 :goto_3

    .line 141
    .restart local v23    # "resValues":Landroid/content/ContentValues;
    :cond_6
    sget-object v31, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_RESOURCES:Landroid/net/Uri;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "resource_global_id="

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    const/16 v33, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    move-object/from16 v2, v23

    move-object/from16 v3, v32

    move-object/from16 v4, v33

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 143
    sget-object v31, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "APP RESOURCES - Update resource "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " into database"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 157
    .end local v15    # "compressed":I
    .end local v16    # "compressedSize":J
    .end local v23    # "resValues":Landroid/content/ContentValues;
    .end local v25    # "resource":Lcom/vectorwatch/android/models/LocalResource;
    .end local v28    # "size":J
    :cond_7
    const/16 v30, 0x1

    goto/16 :goto_0
.end method

.method public static setAuthInvalidated(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "invalidated"    # Z

    .prologue
    .line 610
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 612
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 613
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "auth_invalidated"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 615
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "app_uuid=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 617
    return-void
.end method

.method public static softDeleteApp(ILandroid/content/Context;)V
    .locals 11
    .param p0, "appId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 393
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 394
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-array v2, v10, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "app_type"

    aput-object v5, v2, v3

    const-string v3, "app_global_id"

    aput-object v3, v2, v9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_global_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 398
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 399
    .local v6, "crt":I
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 401
    :cond_0
    add-int/lit8 v6, v6, 0x1

    .line 403
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 405
    .local v8, "values":Landroid/content/ContentValues;
    const-string v1, "cloud_app_modified"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 406
    const-string v1, "app_state"

    sget-object v2, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "app_global_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v8, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 412
    if-lt v6, v10, :cond_1

    .line 413
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 414
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v2, "The database contains multiple entries for the same cloud app. This should not happen."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 421
    .end local v8    # "values":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 417
    .restart local v8    # "values":Landroid/content/ContentValues;
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 420
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static updateApp(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)Z
    .locals 33
    .param p0, "app"    # Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v23

    .line 162
    .local v23, "resolver":Landroid/content/ContentResolver;
    new-instance v18, Lcom/google/gson/Gson;

    invoke-direct/range {v18 .. v18}, Lcom/google/gson/Gson;-><init>()V

    .line 164
    .local v18, "gson":Lcom/google/gson/Gson;
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 165
    .local v6, "appContentAsJsonToString":Ljava/lang/String;
    sget-object v29, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "appcontentjson="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->id:Ljava/lang/Integer;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 169
    .local v9, "appId":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    .line 170
    .local v10, "appName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->description:Ljava/lang/String;

    .line 171
    .local v7, "appDescription":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->img:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 172
    .local v19, "image":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->appType:Ljava/lang/String;

    .line 173
    .local v11, "appType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->editImg:Ljava/lang/String;

    .line 174
    .local v8, "appEditImage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->uuid:Ljava/lang/String;

    .line 176
    .local v12, "appUuid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->rating:F

    move/from16 v29, v0

    move/from16 v0, v29

    float-to-int v0, v0

    move/from16 v20, v0

    .line 179
    .local v20, "rating":I
    new-instance v28, Landroid/content/ContentValues;

    invoke-direct/range {v28 .. v28}, Landroid/content/ContentValues;-><init>()V

    .line 180
    .local v28, "values":Landroid/content/ContentValues;
    const-string v29, "app_name"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v29, "app_description"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v29, "app_image"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v29, "app_rating"

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 184
    const-string v29, "app_type"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v29, "app_global_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 186
    const-string v29, "app_edit_image"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v29, "app_uuid"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v29, "app_watchfaces"

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 190
    const-string v29, "app_state"

    sget-object v30, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->LOCKER:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual/range {v30 .. v30}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v29, "cloud_app_modified"

    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    invoke-virtual/range {v28 .. v30}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 195
    move-object/from16 v0, p1

    invoke-static {v9, v0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->isAppInDatabase(ILandroid/content/Context;)Z

    move-result v29

    if-nez v29, :cond_0

    .line 196
    sget-object v29, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v30, "UPDATE APP - trying to update an app which is not in the DB"

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 197
    const/16 v29, 0x0

    .line 258
    :goto_0
    return v29

    .line 199
    :cond_0
    sget-object v29, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "app_global_id="

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    const/16 v31, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    move-object/from16 v2, v28

    move-object/from16 v3, v30

    move-object/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v14

    .line 202
    .local v14, "changedRows":I
    const/16 v29, 0x1

    move/from16 v0, v29

    if-eq v14, v0, :cond_1

    .line 203
    sget-object v29, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v30, "UPDATE APP - Multiple apps found with the same id."

    invoke-interface/range {v29 .. v30}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 204
    const/16 v29, 0x0

    goto :goto_0

    .line 208
    :cond_1
    const/16 v25, 0x0

    .line 210
    .local v25, "resources":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/LocalResource;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    move-object/from16 v29, v0

    if-eqz v29, :cond_2

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/vectorwatch/android/models/AppContent;->localResources:Ljava/util/List;

    move-object/from16 v25, v0

    .line 214
    :cond_2
    if-eqz v25, :cond_7

    .line 215
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v29

    :cond_3
    :goto_1
    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_7

    invoke-interface/range {v29 .. v29}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/vectorwatch/android/models/LocalResource;

    .line 216
    .local v24, "resource":Lcom/vectorwatch/android/models/LocalResource;
    sget-object v30, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "DEBUG Resources to save in DB = "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/models/DownloadedAppDetails;->content:Lcom/vectorwatch/android/models/AppContent;

    move-object/from16 v32, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-interface/range {v30 .. v31}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 218
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v30

    move/from16 v0, v30

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->isResourceInDatabase(ILandroid/content/Context;)Z

    move-result v30

    if-nez v30, :cond_4

    .line 220
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->compressed:Ljava/lang/Boolean;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v30

    if-eqz v30, :cond_5

    const/4 v15, 0x1

    .line 223
    .local v15, "compressed":I
    :goto_2
    const/16 v30, 0x1

    move/from16 v0, v30

    if-ne v15, v0, :cond_6

    .line 224
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->content:Ljava/lang/String;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-static/range {v30 .. v31}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v30

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v16, v0

    .line 225
    .local v16, "compressedSize":J
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->decompressedSize:Ljava/lang/Short;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Short;->shortValue()S

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .line 231
    .local v26, "size":J
    :goto_3
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 232
    .local v22, "resValues":Landroid/content/ContentValues;
    const-string v30, "resource_global_id"

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v31, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 233
    const-string v30, "resource_size"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 234
    const-string v30, "resource_compressed_size"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 235
    const-string v30, "resource_compressed"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 236
    const-string v30, "resource_content"

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->content:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    sget-object v30, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_RESOURCES:Landroid/net/Uri;

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v21

    .line 240
    .local v21, "resRow":Landroid/net/Uri;
    if-eqz v21, :cond_3

    .line 244
    sget-object v30, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "Inserted resource "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " into database"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-interface/range {v30 .. v31}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 247
    .end local v15    # "compressed":I
    .end local v16    # "compressedSize":J
    .end local v21    # "resRow":Landroid/net/Uri;
    .end local v22    # "resValues":Landroid/content/ContentValues;
    .end local v26    # "size":J
    :cond_4
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v30

    move/from16 v0, v30

    move-object/from16 v1, p1

    invoke-static {v0, v9, v1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->isResourceForAppInDatabase(IILandroid/content/Context;)Z

    move-result v30

    if-nez v30, :cond_3

    .line 248
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 249
    .local v13, "appsResValues":Landroid/content/ContentValues;
    const-string v30, "apps_resources_app_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 250
    const-string v30, "apps_resources_res_id"

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v13, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 251
    sget-object v30, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "inserted app resource. app="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " resId="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->id:Ljava/lang/Integer;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-interface/range {v30 .. v31}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 252
    sget-object v30, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_APPS_RESOURCES:Landroid/net/Uri;

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_1

    .line 220
    .end local v13    # "appsResValues":Landroid/content/ContentValues;
    :cond_5
    const/4 v15, 0x0

    goto/16 :goto_2

    .line 227
    .restart local v15    # "compressed":I
    :cond_6
    const-wide/16 v16, 0x0

    .line 228
    .restart local v16    # "compressedSize":J
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/vectorwatch/android/models/LocalResource;->content:Ljava/lang/String;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-static/range {v30 .. v31}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v30

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v30, v0

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v26, v0

    .restart local v26    # "size":J
    goto/16 :goto_3

    .line 258
    .end local v15    # "compressed":I
    .end local v16    # "compressedSize":J
    .end local v24    # "resource":Lcom/vectorwatch/android/models/LocalResource;
    .end local v26    # "size":J
    :cond_7
    const/16 v29, 0x1

    goto/16 :goto_0
.end method

.method public static updateAppState(ILcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)V
    .locals 13
    .param p0, "appId"    # I
    .param p1, "newState"    # Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v4, 0x0

    .line 460
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 461
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "app_name"

    aput-object v5, v2, v3

    const-string v3, "app_description"

    aput-object v3, v2, v11

    const-string v3, "app_state"

    aput-object v3, v2, v12

    const/4 v3, 0x3

    const-string v5, "cloud_app_modified"

    aput-object v5, v2, v3

    const/4 v3, 0x4

    const-string v5, "app_type"

    aput-object v5, v2, v3

    const/4 v3, 0x5

    const-string v5, "app_image"

    aput-object v5, v2, v3

    const/4 v3, 0x6

    const-string v5, "app_rating"

    aput-object v5, v2, v3

    const/4 v3, 0x7

    const-string v5, "app_watchfaces"

    aput-object v5, v2, v3

    const/16 v3, 0x8

    const-string v5, "app_global_id"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "app_global_id="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 469
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 470
    .local v7, "crt":I
    if-eqz v8, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 475
    :cond_0
    add-int/lit8 v7, v7, 0x1

    .line 477
    const-string v1, "app_type"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 478
    .local v6, "appType":Ljava/lang/String;
    const-string v1, "app_state"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 480
    .local v9, "state":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 481
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 483
    .local v10, "values":Landroid/content/ContentValues;
    const-string v1, "app_state"

    invoke-virtual {p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    if-eqz v6, :cond_1

    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    if-eq p1, v1, :cond_1

    .line 487
    const-string v1, "cloud_app_modified"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 490
    :cond_1
    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "app_global_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v10, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 492
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Updating cloud app state for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 496
    .end local v10    # "values":Landroid/content/ContentValues;
    :cond_2
    if-lt v7, v12, :cond_3

    .line 497
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 498
    sget-object v1, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v2, "The database contains multiple entries for the same cloud app. This should not happen."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 505
    .end local v6    # "appType":Ljava/lang/String;
    .end local v9    # "state":Ljava/lang/String;
    :goto_0
    return-void

    .line 501
    .restart local v6    # "appType":Ljava/lang/String;
    .restart local v9    # "state":Ljava/lang/String;
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 504
    .end local v6    # "appType":Ljava/lang/String;
    .end local v9    # "state":Ljava/lang/String;
    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static updateAppUserSettings(Ljava/util/Map;ILandroid/content/Context;)V
    .locals 7
    .param p1, "appId"    # I
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1028
    .local p0, "settings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1029
    .local v1, "resolver":Landroid/content/ContentResolver;
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 1030
    .local v0, "gson":Lcom/google/gson/Gson;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1031
    .local v3, "values":Landroid/content/ContentValues;
    if-eqz p0, :cond_0

    .line 1032
    invoke-virtual {v0, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1033
    .local v2, "userSettingsAsString":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 1034
    const-string v4, "user_settings"

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1038
    .end local v2    # "userSettingsAsString":Ljava/lang/String;
    :cond_0
    sget-object v4, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "app_global_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1040
    return-void
.end method

.method public static updatePositionsForApps(Ljava/util/List;Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 941
    .local p0, "appIdsInOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-nez p0, :cond_0

    .line 942
    sget-object v3, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Null app ids order list."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 945
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 947
    .local v1, "resolver":Landroid/content/ContentResolver;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 948
    .local v0, "appId":Ljava/lang/Integer;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 949
    .local v2, "values":Landroid/content/ContentValues;
    const-string v4, "cloud_app_order_index"

    invoke-interface {p0, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 951
    sget-object v4, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_CLOUD:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "app_global_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 954
    sget-object v4, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Update position for mAppId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p0, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 956
    .end local v0    # "appId":Ljava/lang/Integer;
    .end local v2    # "values":Landroid/content/ContentValues;
    :cond_1
    return-void
.end method
