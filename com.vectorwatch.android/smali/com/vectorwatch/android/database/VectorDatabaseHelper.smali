.class public Lcom/vectorwatch/android/database/VectorDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "VectorDatabaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/database/VectorDatabaseHelper$LogType;,
        Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualArea;,
        Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualType;,
        Lcom/vectorwatch/android/database/VectorDatabaseHelper$ContextualPeriodOfDay;,
        Lcom/vectorwatch/android/database/VectorDatabaseHelper$ActivityType;
    }
.end annotation


# static fields
.field public static final ACTIVITY_AVG_AMPLITUDE:Ljava/lang/String; = "amplitude"

.field public static final ACTIVITY_AVG_PERIOD:Ljava/lang/String; = "avg_period"

.field public static final ACTIVITY_CALORIES:Ljava/lang/String; = "calories"

.field public static final ACTIVITY_DISTANCE:Ljava/lang/String; = "distance"

.field public static final ACTIVITY_EFFECTIVE_TIME:Ljava/lang/String; = "active_time"

.field public static final ACTIVITY_GOOGLE_FIT_DIRTY_CALORIES:Ljava/lang/String; = "activity_google_fit_dirty_cal"

.field public static final ACTIVITY_GOOGLE_FIT_DIRTY_DISTANCE:Ljava/lang/String; = "activity_google_fit_dirty_dist"

.field public static final ACTIVITY_GOOGLE_FIT_DIRTY_STEPS:Ljava/lang/String; = "activity_google_fit_dirty"

.field public static final ACTIVITY_OFFSET:Ljava/lang/String; = "offset"

.field public static final ACTIVITY_STEPS:Ljava/lang/String; = "value"

.field public static final ACTIVITY_TIMEZONE:Ljava/lang/String; = "timezone"

.field public static final ACTIVITY_TYPE:Ljava/lang/String; = "type"

.field public static final ALARM_ANGLE:Ljava/lang/String; = "alarm_angle"

.field public static final ALARM_ENABLED:Ljava/lang/String; = "alarm_enabled"

.field public static final ALARM_HOUR:Ljava/lang/String; = "alarm_hour"

.field public static final ALARM_IS_ENABLED:Ljava/lang/String; = "alarm_status"

.field public static final ALARM_MINUTE:Ljava/lang/String; = "alarm_minute"

.field public static final ALARM_NAME:Ljava/lang/String; = "alarm_name"

.field public static final ALARM_TIME:Ljava/lang/String; = "alarm_time"

.field public static final ALARM_TIME_OF_DAY:Ljava/lang/String; = "alarm_time_of_day"

.field public static final APPS_RESOURCES_APP_ID:Ljava/lang/String; = "apps_resources_app_id"

.field public static final APPS_RESOURCES_RES_ID:Ljava/lang/String; = "apps_resources_res_id"

.field public static final AWAKE_COUNT:Ljava/lang/String; = "awake_count"

.field public static final CLOUD_APP_AUTH_INVALIDATED:Ljava/lang/String; = "auth_invalidated"

.field public static final CLOUD_APP_CONTENT:Ljava/lang/String; = "app_watchfaces"

.field public static final CLOUD_APP_DESCRIPTION:Ljava/lang/String; = "app_description"

.field public static final CLOUD_APP_EDIT_IMAGE:Ljava/lang/String; = "app_edit_image"

.field public static final CLOUD_APP_GLOBAL_ID:Ljava/lang/String; = "app_global_id"

.field public static final CLOUD_APP_HAS_DYNAMIC_SETTINGS:Ljava/lang/String; = "dynamic_settings"

.field public static final CLOUD_APP_IMAGE:Ljava/lang/String; = "app_image"

.field public static final CLOUD_APP_MODIFIED:Ljava/lang/String; = "cloud_app_modified"

.field public static final CLOUD_APP_NAME:Ljava/lang/String; = "app_name"

.field public static final CLOUD_APP_NEEDS_AUTH:Ljava/lang/String; = "needs_auth"

.field public static final CLOUD_APP_NEEDS_LOCATION:Ljava/lang/String; = "needs_location"

.field public static final CLOUD_APP_ORDER_INDEX:Ljava/lang/String; = "cloud_app_order_index"

.field public static final CLOUD_APP_RATING:Ljava/lang/String; = "app_rating"

.field public static final CLOUD_APP_STATE:Ljava/lang/String; = "app_state"

.field public static final CLOUD_APP_TYPE:Ljava/lang/String; = "app_type"

.field public static final CLOUD_APP_USER_SETTINGS:Ljava/lang/String; = "user_settings"

.field public static final CLOUD_APP_UUID:Ljava/lang/String; = "app_uuid"

.field public static final CONTEXT_AREA:Ljava/lang/String; = "area"

.field public static final CONTEXT_PERIOD_OF_DAY:Ljava/lang/String; = "period_of_day"

.field public static final CONTEXT_TYPE:Ljava/lang/String; = "type"

.field private static final CREATEFILE:Ljava/lang/String; = "create.sql"

.field private static final DATABASE_NAME:Ljava/lang/String; = "vectorwatch.db"

.field private static final DATABASE_VERSION:I = 0x16

.field public static final DEFAULT_OFFSET_MINUTES:I = 0xf

.field public static final DEFAULT_OFFSET_MINUTES_ACTIVITY_SYNC:I = 0xf

.field public static final DEFAULT_OFFSET_MINUTES_LOGS_SYNC:I = 0x3c

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DIRTY_FALSE:I = 0x0

.field public static final DIRTY_FIELD:Ljava/lang/String; = "dirty_field"

.field public static final DIRTY_TRUE:I = 0x1

.field public static final EVENT_START_TIME:Ljava/lang/String; = "start_time"

.field public static final EVENT_STOP_TIME:Ljava/lang/String; = "stop_time"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LOG_BACKLIGHT_COUNT:Ljava/lang/String; = "log_backlight_count"

.field public static final LOG_BATTERY_VOLTAGE:Ljava/lang/String; = "log_battery_voltage"

.field public static final LOG_BUTTON_PRESS_COUNT:Ljava/lang/String; = "log_button_presses"

.field public static final LOG_CONNECT_COUNT:Ljava/lang/String; = "log_connects"

.field public static final LOG_DISCONNECT_COUNT:Ljava/lang/String; = "log_disconnects"

.field public static final LOG_GLANCE_COUNT:Ljava/lang/String; = "log_glance_count"

.field public static final LOG_NOTIFICATION_COUNT:Ljava/lang/String; = "log_notification"

.field public static final LOG_OFFSET:Ljava/lang/String; = "log_offset"

.field public static final LOG_RX_PACKETS:Ljava/lang/String; = "log_received_packets"

.field public static final LOG_SECS_DISCONNECTED_COUNT:Ljava/lang/String; = "log_secs_disconnected"

.field public static final LOG_SHAKER_COUNT:Ljava/lang/String; = "log_shaker_count"

.field public static final LOG_TX_PACKETS:Ljava/lang/String; = "log_transmitted_packets"

.field public static final PHONE_STEP_COUNT:Ljava/lang/String; = "phone_steps"

.field public static final RESOURCE_COMPRESSED:Ljava/lang/String; = "resource_compressed"

.field public static final RESOURCE_COMPRESSED_SIZE:Ljava/lang/String; = "resource_compressed_size"

.field public static final RESOURCE_CONTENT:Ljava/lang/String; = "resource_content"

.field public static final RESOURCE_GLOBAL_ID:Ljava/lang/String; = "resource_global_id"

.field public static final RESOURCE_SIZE:Ljava/lang/String; = "resource_size"

.field public static final RESTLESS_COUNT:Ljava/lang/String; = "restless_count"

.field public static final SLEEP_START_TIME:Ljava/lang/String; = "start_time"

.field public static final SLEEP_STOP_TIME:Ljava/lang/String; = "stop_time"

.field private static final SQL_DIR:Ljava/lang/String; = "db"

.field public static final START_COUNT_TIMESTAMP:Ljava/lang/String; = "start_count_timestamp"

.field public static final STOP_COUNT_TIMESTAMP:Ljava/lang/String; = "stop_count_timestamp"

.field public static final STREAM_ACTIVE_APP:Ljava/lang/String; = "stream_active_app"

.field public static final STREAM_ACTIVE_FACE:Ljava/lang/String; = "stream_active_face"

.field public static final STREAM_ACTIVE_FIELD:Ljava/lang/String; = "stream_active_field"

.field public static final STREAM_AUTH_INVALIDATED:Ljava/lang/String; = "auth_invalidated"

.field public static final STREAM_CHANNEL_LABEL:Ljava/lang/String; = "stream_channel_label"

.field public static final STREAM_CONTENT_VERSION:Ljava/lang/String; = "stream_cont_version"

.field public static final STREAM_DESCRIPTION:Ljava/lang/String; = "stream_description"

.field public static final STREAM_DIRTY:Ljava/lang/String; = "stream_dirty"

.field public static final STREAM_DYNAMIC:Ljava/lang/String; = "stream_dynamic"

.field public static final STREAM_ID:Ljava/lang/String; = "stream_id"

.field public static final STREAM_IMG:Ljava/lang/String; = "stream_img"

.field public static final STREAM_LABEL:Ljava/lang/String; = "stream_label"

.field public static final STREAM_NAME:Ljava/lang/String; = "stream_name"

.field public static final STREAM_NEEDS_AUTH:Ljava/lang/String; = "needs_auth"

.field public static final STREAM_POSSIBLE_SETTINGS:Ljava/lang/String; = "stream_possible_settings"

.field public static final STREAM_SECONDS_TO_LIVE:Ljava/lang/String; = "stream_seconds_to_live"

.field public static final STREAM_SETTINGS:Ljava/lang/String; = "stream_settings"

.field public static final STREAM_STATE:Ljava/lang/String; = "stream_state"

.field public static final STREAM_SUPPORTED_TYPE:Ljava/lang/String; = "stream_supported_type"

.field public static final STREAM_TIMESTAMP:Ljava/lang/String; = "stream_timestamp"

.field public static final STREAM_TYPE:Ljava/lang/String; = "stream_type"

.field public static final STREAM_UPDATE_VALUE:Ljava/lang/String; = "stream_update_value"

.field public static final STREAM_UUID:Ljava/lang/String; = "stream_uuid"

.field public static final STREAM_VERSION:Ljava/lang/String; = "stream_version"

.field public static final TABLE_ACTIVITY_DATA:Ljava/lang/String; = "activity_data"

.field public static final TABLE_ALARM_DATA:Ljava/lang/String; = "alarm_data"

.field public static final TABLE_APPS_RESOURCES:Ljava/lang/String; = "apps_resources"

.field public static final TABLE_CALENDAR_DATA:Ljava/lang/String; = "calendar_data"

.field public static final TABLE_CLOUD_APP:Ljava/lang/String; = "cloud_app"

.field public static final TABLE_CONTEXTUAL_DATA:Ljava/lang/String; = "contextual_data"

.field public static final TABLE_LOG_DATA:Ljava/lang/String; = "log_data"

.field public static final TABLE_RESOURCES:Ljava/lang/String; = "resources"

.field public static final TABLE_SLEEP_DATA:Ljava/lang/String; = "sleep_data"

.field public static final TABLE_STEP_LOG:Ljava/lang/String; = "step_log"

.field public static final TABLE_STREAM_DATA:Ljava/lang/String; = "stream_data"

.field private static final TAG:Ljava/lang/String;

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final TIMEZONE:Ljava/lang/String; = "timezone"

.field public static final TITLE:Ljava/lang/String; = "title"

.field private static final UPGRADEFILE_PREFIX:Ljava/lang/String; = "upgrade-"

.field private static final UPGRADEFILE_SUFFIX:Ljava/lang/String; = ".sql"

.field public static final WATCH_STEP_COUNT:Ljava/lang/String; = "watch_steps"

.field public static final _ID:Ljava/lang/String; = "_id"


# instance fields
.field private contextReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 169
    const-string v0, "vectorwatch.db"

    const/4 v1, 0x0

    const/16 v2, 0x16

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 22
    const-class v0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->log:Lorg/slf4j/Logger;

    .line 170
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->contextReference:Ljava/lang/ref/WeakReference;

    .line 172
    invoke-virtual {p0}, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 173
    return-void
.end method


# virtual methods
.method protected execSqlFile(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "sqlFile"    # Ljava/lang/String;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "db/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->contextReference:Ljava/lang/ref/WeakReference;

    .line 222
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 221
    invoke-static {v2, v1}, Lcom/vectorwatch/android/utils/SqlParser;->parseSqlFile(Ljava/lang/String;Landroid/content/res/AssetManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 223
    .local v0, "sqlInstruction":Ljava/lang/String;
    invoke-virtual {p2, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0

    .line 225
    .end local v0    # "sqlInstruction":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 180
    iget-object v1, p0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Create database"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 183
    :try_start_0
    const-string v1, "create.sql"

    invoke-virtual {p0, v1, p1}, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->execSqlFile(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    .line 191
    :goto_0
    const/4 v1, 0x0

    const/16 v2, 0x16

    invoke-virtual {p0, p1, v1, v2}, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 192
    return-void

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 186
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 187
    .local v0, "e":Landroid/database/SQLException;
    iget-object v1, p0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/SQLException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 9
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 200
    iget-object v3, p0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Upgrade database"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 205
    :try_start_0
    const-string v4, "db"

    iget-object v3, p0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->contextReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/vectorwatch/android/utils/AssetUtils;->list(Ljava/lang/String;Landroid/content/res/AssetManager;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v2, v4, v3

    .line 206
    .local v2, "sqlFile":Ljava/lang/String;
    const-string v6, "upgrade-"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 207
    const-string v6, "upgrade-"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    const-string v8, ".sql"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 208
    .local v1, "fileVersion":I
    if-le v1, p2, :cond_0

    if-gt v1, p3, :cond_0

    .line 209
    iget-object v6, p0, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SQL: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 210
    invoke-virtual {p0, v2, p1}, Lcom/vectorwatch/android/database/VectorDatabaseHelper;->execSqlFile(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    .end local v1    # "fileVersion":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 214
    .end local v2    # "sqlFile":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 215
    .local v0, "exception":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Database upgrade failed"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 217
    .end local v0    # "exception":Ljava/io/IOException;
    :cond_1
    return-void
.end method
