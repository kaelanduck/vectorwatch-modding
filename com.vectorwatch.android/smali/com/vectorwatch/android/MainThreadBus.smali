.class public Lcom/vectorwatch/android/MainThreadBus;
.super Lcom/squareup/otto/Bus;
.source "MainThreadBus.java"


# instance fields
.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 13
    sget-object v0, Lcom/squareup/otto/ThreadEnforcer;->ANY:Lcom/squareup/otto/ThreadEnforcer;

    invoke-direct {p0, v0}, Lcom/squareup/otto/Bus;-><init>(Lcom/squareup/otto/ThreadEnforcer;)V

    .line 10
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vectorwatch/android/MainThreadBus;->mHandler:Landroid/os/Handler;

    .line 14
    return-void
.end method

.method static synthetic access$001(Lcom/vectorwatch/android/MainThreadBus;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/MainThreadBus;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 9
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public post(Ljava/lang/Object;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/Object;

    .prologue
    .line 18
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 19
    invoke-super {p0, p1}, Lcom/squareup/otto/Bus;->post(Ljava/lang/Object;)V

    .line 28
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/MainThreadBus;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/vectorwatch/android/MainThreadBus$1;

    invoke-direct {v1, p0, p1}, Lcom/vectorwatch/android/MainThreadBus$1;-><init>(Lcom/vectorwatch/android/MainThreadBus;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
