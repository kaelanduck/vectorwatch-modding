.class public Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;
.super Ljava/lang/Object;
.source "SuccessMessageFromCloudEvent.java"


# instance fields
.field private cloudMessageModel:Lcom/vectorwatch/android/events/CloudMessageModel;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/events/CloudMessageModel;)V
    .locals 0
    .param p1, "cloudMessage"    # Lcom/vectorwatch/android/events/CloudMessageModel;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;->cloudMessageModel:Lcom/vectorwatch/android/events/CloudMessageModel;

    .line 12
    return-void
.end method


# virtual methods
.method public getCloudMessageModel()Lcom/vectorwatch/android/events/CloudMessageModel;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/vectorwatch/android/events/SuccessMessageFromCloudEvent;->cloudMessageModel:Lcom/vectorwatch/android/events/CloudMessageModel;

    return-object v0
.end method
