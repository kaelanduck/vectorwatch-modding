.class public Lcom/vectorwatch/android/events/FoundBleDeviceEvent;
.super Ljava/lang/Object;
.source "FoundBleDeviceEvent.java"


# instance fields
.field private device:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/vectorwatch/android/events/FoundBleDeviceEvent;->device:Landroid/bluetooth/BluetoothDevice;

    .line 13
    return-void
.end method


# virtual methods
.method public getDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/events/FoundBleDeviceEvent;->device:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method
