.class public Lcom/vectorwatch/android/events/VerifyUpdateEvent;
.super Ljava/lang/Object;
.source "VerifyUpdateEvent.java"


# instance fields
.field private crtPart:I

.field private totalParts:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "crtPart"    # I
    .param p2, "totalParts"    # I

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/vectorwatch/android/events/VerifyUpdateEvent;->crtPart:I

    .line 13
    iput p2, p0, Lcom/vectorwatch/android/events/VerifyUpdateEvent;->totalParts:I

    .line 14
    return-void
.end method


# virtual methods
.method public getCrtPart()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/vectorwatch/android/events/VerifyUpdateEvent;->crtPart:I

    return v0
.end method

.method public getTotalParts()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/vectorwatch/android/events/VerifyUpdateEvent;->totalParts:I

    return v0
.end method
