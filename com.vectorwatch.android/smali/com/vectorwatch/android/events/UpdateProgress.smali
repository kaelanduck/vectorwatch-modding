.class public Lcom/vectorwatch/android/events/UpdateProgress;
.super Ljava/lang/Object;
.source "UpdateProgress.java"


# instance fields
.field private status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/com/android/vos/update/UpdateStatus;)V
    .locals 0
    .param p1, "status"    # Lcom/vectorwatch/com/android/vos/update/UpdateStatus;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/events/UpdateProgress;->status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus;

    .line 14
    return-void
.end method


# virtual methods
.method public getUpdateStatus()Lcom/vectorwatch/com/android/vos/update/UpdateStatus;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/events/UpdateProgress;->status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus;

    return-object v0
.end method

.method public setUpdateStatus(Lcom/vectorwatch/com/android/vos/update/UpdateStatus;)V
    .locals 0
    .param p1, "status"    # Lcom/vectorwatch/com/android/vos/update/UpdateStatus;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/vectorwatch/android/events/UpdateProgress;->status:Lcom/vectorwatch/com/android/vos/update/UpdateStatus;

    .line 22
    return-void
.end method
