.class public Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;
.super Ljava/lang/Object;
.source "CheckCompatibilityResultEvent.java"


# instance fields
.field compatibilityStatus:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

.field serverMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;Ljava/lang/String;)V
    .locals 0
    .param p1, "compatibilityStatus"    # Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;
    .param p2, "serverMessage"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;->compatibilityStatus:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    .line 14
    iput-object p2, p0, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;->serverMessage:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getCompatibilityStatus()Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;->compatibilityStatus:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    return-object v0
.end method

.method public getServerMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;->serverMessage:Ljava/lang/String;

    return-object v0
.end method
