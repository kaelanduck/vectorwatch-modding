.class public Lcom/vectorwatch/android/events/LanguageFileProgress;
.super Ljava/lang/Object;
.source "LanguageFileProgress.java"


# instance fields
.field private mCurrentPackage:I

.field private mTotalPackets:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "currentPackage"    # I
    .param p2, "totalPackets"    # I

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/vectorwatch/android/events/LanguageFileProgress;->mCurrentPackage:I

    .line 13
    iput p2, p0, Lcom/vectorwatch/android/events/LanguageFileProgress;->mTotalPackets:I

    .line 14
    return-void
.end method


# virtual methods
.method public getCurrentPackage()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/vectorwatch/android/events/LanguageFileProgress;->mCurrentPackage:I

    return v0
.end method

.method public getTotalPackets()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/vectorwatch/android/events/LanguageFileProgress;->mTotalPackets:I

    return v0
.end method
