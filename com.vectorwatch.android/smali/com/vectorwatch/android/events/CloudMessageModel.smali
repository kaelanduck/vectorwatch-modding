.class public Lcom/vectorwatch/android/events/CloudMessageModel;
.super Ljava/lang/Object;
.source "CloudMessageModel.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;
    }
.end annotation


# instance fields
.field private code:Ljava/lang/Integer;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method


# virtual methods
.method public getCode()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/vectorwatch/android/events/CloudMessageModel;->code:Ljava/lang/Integer;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/events/CloudMessageModel;->text:Ljava/lang/String;

    return-object v0
.end method

.method public setCode(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "code"    # Ljava/lang/Integer;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/vectorwatch/android/events/CloudMessageModel;->code:Ljava/lang/Integer;

    .line 44
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/vectorwatch/android/events/CloudMessageModel;->text:Ljava/lang/String;

    .line 40
    return-void
.end method
