.class public Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;
.super Ljava/lang/Object;
.source "CloudSystemUpdateEvent.java"


# instance fields
.field softwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/SoftwareUpdateModel;)V
    .locals 0
    .param p1, "systemUpdate"    # Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;->softwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 14
    return-void
.end method


# virtual methods
.method public getSoftwareUpdate()Lcom/vectorwatch/android/models/SoftwareUpdateModel;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/vectorwatch/android/events/CloudSystemUpdateEvent;->softwareUpdateModel:Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    return-object v0
.end method
