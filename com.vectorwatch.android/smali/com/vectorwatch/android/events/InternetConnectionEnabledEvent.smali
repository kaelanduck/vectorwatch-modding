.class public Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;
.super Ljava/lang/Object;
.source "InternetConnectionEnabledEvent.java"


# instance fields
.field private enabled:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-boolean p1, p0, Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;->enabled:Z

    .line 11
    return-void
.end method


# virtual methods
.method public isEnabled()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;->enabled:Z

    return v0
.end method
