.class public Lcom/vectorwatch/android/events/LoginStateChangedEvent;
.super Ljava/lang/Object;
.source "LoginStateChangedEvent.java"


# instance fields
.field private mIsLoggedIn:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0
    .param p1, "isLoggedIn"    # Z

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-boolean p1, p0, Lcom/vectorwatch/android/events/LoginStateChangedEvent;->mIsLoggedIn:Z

    .line 11
    return-void
.end method


# virtual methods
.method public isLoggedIn()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/vectorwatch/android/events/LoginStateChangedEvent;->mIsLoggedIn:Z

    return v0
.end method
