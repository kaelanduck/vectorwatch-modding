.class public Lcom/vectorwatch/android/events/InstallAppEvent;
.super Ljava/lang/Object;
.source "InstallAppEvent.java"


# static fields
.field public static final INSTALL_EVENT_ERROR:I = 0x2

.field public static final INSTALL_EVENT_SKIP:I = 0x3

.field public static final INSTALL_EVENT_START:I = 0x0

.field public static final INSTALL_EVENT_UPDATE:I = 0x1


# instance fields
.field private errorMessage:Ljava/lang/String;

.field private type:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/vectorwatch/android/events/InstallAppEvent;->type:I

    .line 17
    iput-object p2, p0, Lcom/vectorwatch/android/events/InstallAppEvent;->errorMessage:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getInstallAppType()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/vectorwatch/android/events/InstallAppEvent;->type:I

    return v0
.end method

.method public getInstallErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/events/InstallAppEvent;->errorMessage:Ljava/lang/String;

    return-object v0
.end method
