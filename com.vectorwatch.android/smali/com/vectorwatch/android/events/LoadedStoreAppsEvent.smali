.class public Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;
.super Ljava/lang/Object;
.source "LoadedStoreAppsEvent.java"


# instance fields
.field option:Lcom/vectorwatch/android/models/cloud/AppsOption;

.field storeElements:Lcom/vectorwatch/android/models/StoreQuery;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/models/cloud/AppsOption;Lcom/vectorwatch/android/models/StoreQuery;)V
    .locals 0
    .param p1, "appType"    # Lcom/vectorwatch/android/models/cloud/AppsOption;
    .param p2, "storeElements"    # Lcom/vectorwatch/android/models/StoreQuery;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p2, p0, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->storeElements:Lcom/vectorwatch/android/models/StoreQuery;

    .line 14
    iput-object p1, p0, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->option:Lcom/vectorwatch/android/models/cloud/AppsOption;

    .line 15
    return-void
.end method


# virtual methods
.method public getOption()Lcom/vectorwatch/android/models/cloud/AppsOption;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->option:Lcom/vectorwatch/android/models/cloud/AppsOption;

    return-object v0
.end method

.method public getStoreElements()Lcom/vectorwatch/android/models/StoreQuery;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/vectorwatch/android/events/LoadedStoreAppsEvent;->storeElements:Lcom/vectorwatch/android/models/StoreQuery;

    return-object v0
.end method
