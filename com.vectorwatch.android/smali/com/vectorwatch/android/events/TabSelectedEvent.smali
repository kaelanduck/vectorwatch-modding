.class public Lcom/vectorwatch/android/events/TabSelectedEvent;
.super Ljava/lang/Object;
.source "TabSelectedEvent.java"


# instance fields
.field private mPrevTabIndex:I

.field private mTabIndex:I


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "tabIndex"    # I
    .param p2, "prevTabIndex"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/events/TabSelectedEvent;->mPrevTabIndex:I

    .line 11
    iput p1, p0, Lcom/vectorwatch/android/events/TabSelectedEvent;->mTabIndex:I

    .line 12
    iput p2, p0, Lcom/vectorwatch/android/events/TabSelectedEvent;->mPrevTabIndex:I

    .line 13
    return-void
.end method


# virtual methods
.method public getPrevTabIndex()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/vectorwatch/android/events/TabSelectedEvent;->mPrevTabIndex:I

    return v0
.end method

.method public getTabIndex()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/events/TabSelectedEvent;->mTabIndex:I

    return v0
.end method
