.class public Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;
.super Ljava/lang/Object;
.source "WatchUpdateProgressIncreasedEvent.java"


# instance fields
.field private progressPercentage:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "updateProgress"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;->progressPercentage:I

    .line 11
    return-void
.end method


# virtual methods
.method public getProgressPercentage()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/vectorwatch/android/events/WatchUpdateProgressIncreasedEvent;->progressPercentage:I

    return v0
.end method
