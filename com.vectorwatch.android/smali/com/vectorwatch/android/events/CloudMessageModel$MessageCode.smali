.class public final enum Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;
.super Ljava/lang/Enum;
.source "CloudMessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/events/CloudMessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MessageCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

.field public static final enum APP_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

.field public static final enum WATCH_ALREADY_UP_TO_DATE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

.field public static final enum WATCH_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 12
    new-instance v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    const-string v1, "APP_UPDATE_AVAILABLE"

    invoke-direct {v0, v1, v4, v2}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->APP_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    .line 13
    new-instance v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    const-string v1, "WATCH_UPDATE_AVAILABLE"

    invoke-direct {v0, v1, v2, v3}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    .line 14
    new-instance v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    const-string v1, "WATCH_ALREADY_UP_TO_DATE"

    invoke-direct {v0, v1, v3, v5}, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_ALREADY_UP_TO_DATE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    .line 11
    new-array v0, v5, [Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    sget-object v1, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->APP_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_UPDATE_AVAILABLE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->WATCH_ALREADY_UP_TO_DATE:Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->$VALUES:[Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput p3, p0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->val:I

    .line 20
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->$VALUES:[Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/vectorwatch/android/events/CloudMessageModel$MessageCode;->val:I

    return v0
.end method
