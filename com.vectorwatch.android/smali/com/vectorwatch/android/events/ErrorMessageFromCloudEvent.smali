.class public Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;
.super Ljava/lang/Object;
.source "ErrorMessageFromCloudEvent.java"


# instance fields
.field private errorBody:Ljava/lang/Object;

.field private updateType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0
    .param p1, "errorBody"    # Ljava/lang/Object;
    .param p2, "updateType"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;->errorBody:Ljava/lang/Object;

    .line 12
    iput-object p2, p0, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;->updateType:Ljava/lang/String;

    .line 13
    return-void
.end method


# virtual methods
.method public getErrorBody()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;->errorBody:Ljava/lang/Object;

    return-object v0
.end method

.method public getUpdateType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;->updateType:Ljava/lang/String;

    return-object v0
.end method
