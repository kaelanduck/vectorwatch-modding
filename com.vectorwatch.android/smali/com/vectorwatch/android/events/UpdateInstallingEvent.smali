.class public Lcom/vectorwatch/android/events/UpdateInstallingEvent;
.super Ljava/lang/Object;
.source "UpdateInstallingEvent.java"


# instance fields
.field private crtPart:I

.field private totalParts:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "crtPart"    # I
    .param p2, "totalParts"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/vectorwatch/android/events/UpdateInstallingEvent;->crtPart:I

    .line 12
    iput p2, p0, Lcom/vectorwatch/android/events/UpdateInstallingEvent;->totalParts:I

    .line 13
    return-void
.end method


# virtual methods
.method public getCrtPart()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/events/UpdateInstallingEvent;->crtPart:I

    return v0
.end method

.method public getTotalParts()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/vectorwatch/android/events/UpdateInstallingEvent;->totalParts:I

    return v0
.end method
