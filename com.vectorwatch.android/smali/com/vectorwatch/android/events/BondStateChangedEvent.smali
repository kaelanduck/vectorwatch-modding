.class public Lcom/vectorwatch/android/events/BondStateChangedEvent;
.super Ljava/lang/Object;
.source "BondStateChangedEvent.java"


# instance fields
.field bondState:I

.field deviceAddress:Ljava/lang/String;

.field deviceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "bondState"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput p1, p0, Lcom/vectorwatch/android/events/BondStateChangedEvent;->bondState:I

    .line 13
    iput-object p3, p0, Lcom/vectorwatch/android/events/BondStateChangedEvent;->deviceAddress:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/vectorwatch/android/events/BondStateChangedEvent;->deviceName:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getBondState()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/vectorwatch/android/events/BondStateChangedEvent;->bondState:I

    return v0
.end method

.method public getDeviceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/events/BondStateChangedEvent;->deviceAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/events/BondStateChangedEvent;->deviceName:Ljava/lang/String;

    return-object v0
.end method
