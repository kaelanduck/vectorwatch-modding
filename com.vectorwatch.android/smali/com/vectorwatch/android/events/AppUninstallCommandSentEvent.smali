.class public Lcom/vectorwatch/android/events/AppUninstallCommandSentEvent;
.super Ljava/lang/Object;
.source "AppUninstallCommandSentEvent.java"


# instance fields
.field private removedAppId:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "removedAppId"    # I

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Lcom/vectorwatch/android/events/AppUninstallCommandSentEvent;->removedAppId:I

    .line 11
    return-void
.end method


# virtual methods
.method public getRemovedAppId()I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/vectorwatch/android/events/AppUninstallCommandSentEvent;->removedAppId:I

    return v0
.end method
