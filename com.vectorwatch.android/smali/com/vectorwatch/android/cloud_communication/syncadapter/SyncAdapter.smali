.class public Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SyncAdapter.java"


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.vectorwatch.android.database.contentprovider"

.field public static final CREATE_ACCOUNT:I = 0x2

.field public static final LOGIN_USER:I = 0x1

.field public static final REGSITER_WATCH:I = 0x3

.field public static final REQ_TO_SERVER:Ljava/lang/String; = "req_type"

.field public static final SYNC_DATA:I = 0x5

.field public static final SYNC_LOGS:I = 0x6

.field public static final UNREGISTER_WATCH:I = 0x4


# instance fields
.field private log:Lorg/slf4j/Logger;

.field private mAccountManager:Landroid/accounts/AccountManager;

.field private mContext:Landroid/content/Context;

.field private mResolver:Landroid/content/ContentResolver;

.field private mRestAdapter:Lretrofit/RestAdapter;

.field private mSyncService:Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "autoInitialize"    # Z

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 66
    const-class v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    .line 77
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mContext:Landroid/content/Context;

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mResolver:Landroid/content/ContentResolver;

    .line 79
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mAccountManager:Landroid/accounts/AccountManager;

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "autoInitialize"    # Z
    .param p3, "allowParallelSyncs"    # Z

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;ZZ)V

    .line 66
    const-class v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    .line 85
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mContext:Landroid/content/Context;

    .line 86
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mResolver:Landroid/content/ContentResolver;

    .line 87
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mAccountManager:Landroid/accounts/AccountManager;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)Lorg/slf4j/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)Landroid/accounts/AccountManager;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mAccountManager:Landroid/accounts/AccountManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)Lretrofit/RestAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mRestAdapter:Lretrofit/RestAdapter;

    return-object v0
.end method

.method static synthetic access$202(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;Lretrofit/RestAdapter;)Lretrofit/RestAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;
    .param p1, "x1"    # Lretrofit/RestAdapter;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mRestAdapter:Lretrofit/RestAdapter;

    return-object p1
.end method

.method static synthetic access$302(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;)Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;
    .param p1, "x1"    # Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mSyncService:Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;

    return-object p1
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->syncToGoogleFit()V

    return-void
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->syncActivities()V

    return-void
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->syncLogs()V

    return-void
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->syncSleep()V

    return-void
.end method

.method private syncActivities()V
    .locals 23

    .prologue
    .line 203
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    const-string v5, "Trying to sync activity to cloud."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 204
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v20

    .line 205
    .local v20, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/vectorwatch/android/database/DatabaseManager;->getAllActivityForCloudSync(Lio/realm/Realm;)Ljava/util/List;

    move-result-object v17

    .line 207
    .local v17, "activities":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;>;"
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 208
    .local v19, "listActivities":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UserActivityDataModel;>;"
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 209
    .local v21, "timestampsToClear":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;

    .line 210
    .local v18, "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    new-instance v3, Lcom/vectorwatch/android/models/UserActivityDataModel;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getVal()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getEffTime()I

    move-result v5

    .line 211
    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getAvgAmpl()I

    move-result v6

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getAvgPeriod()I

    move-result v7

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getCal()I

    move-result v8

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getDist()I

    move-result v9

    .line 212
    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimezone()I

    move-result v10

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getActivityType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v12

    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getOffset()I

    move-result v14

    invoke-direct/range {v3 .. v14}, Lcom/vectorwatch/android/models/UserActivityDataModel;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;JI)V

    .line 213
    .local v3, "vectorActivity":Lcom/vectorwatch/android/models/UserActivityDataModel;
    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    invoke-virtual/range {v18 .. v18}, Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;->getTimestamp()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    .end local v3    # "vectorActivity":Lcom/vectorwatch/android/models/UserActivityDataModel;
    .end local v18    # "activity":Lcom/vectorwatch/android/ui/chart/model/CloudActivityDay;
    :cond_0
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 218
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v15

    .line 219
    .local v15, "accountManager":Landroid/accounts/AccountManager;
    const-string v4, "com.vectorwatch.android"

    invoke-virtual {v15, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v16

    .line 221
    .local v16, "accounts":[Landroid/accounts/Account;
    move-object/from16 v0, v16

    array-length v4, v0

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 222
    const-string v4, "Sync-Defaults:"

    const-string v5, "There should be at least one account in Accounts & sync"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-virtual/range {v20 .. v20}, Lio/realm/Realm;->close()V

    .line 243
    .end local v15    # "accountManager":Landroid/accounts/AccountManager;
    .end local v16    # "accounts":[Landroid/accounts/Account;
    :goto_1
    return-void

    .line 227
    .restart local v15    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v16    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const/4 v4, 0x0

    aget-object v2, v16, v4

    .line 229
    .local v2, "account":Landroid/accounts/Account;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$2;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v5, v0, v1}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$2;-><init>(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;Ljava/util/List;)V

    move-object/from16 v0, v19

    invoke-static {v4, v2, v0, v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->syncActivity(Landroid/content/Context;Landroid/accounts/Account;Ljava/util/List;Lretrofit/Callback;)V

    .line 242
    .end local v2    # "account":Landroid/accounts/Account;
    .end local v15    # "accountManager":Landroid/accounts/AccountManager;
    .end local v16    # "accounts":[Landroid/accounts/Account;
    :cond_2
    invoke-virtual/range {v20 .. v20}, Lio/realm/Realm;->close()V

    goto :goto_1
.end method

.method private syncLogs()V
    .locals 26

    .prologue
    .line 249
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    const-string v3, "Trying to sync logs to cloud."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 252
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_LOGS:Landroid/net/Uri;

    const/16 v4, 0xd

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "timestamp"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "log_battery_voltage"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "log_shaker_count"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "log_backlight_count"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "log_connects"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "log_button_presses"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "log_disconnects"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "log_secs_disconnected"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "log_received_packets"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "log_notification"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "log_transmitted_packets"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const-string v6, "log_glance_count"

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "log_offset"

    aput-object v6, v4, v5

    const-string v5, "dirty_field = 1"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 262
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 264
    .local v12, "listLogs":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/WatchLogDataModel;>;"
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 279
    :cond_0
    const-string v2, "log_offset"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 280
    .local v22, "offset":I
    const-string v2, "log_notification"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 281
    .local v18, "noNot":I
    const-string v2, "log_glance_count"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 282
    .local v17, "noGlances":I
    const-string v2, "log_received_packets"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 283
    .local v19, "noRxBLE":I
    const-string v2, "log_transmitted_packets"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 284
    .local v21, "noTxBLE":I
    const-string v2, "log_disconnects"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 285
    .local v16, "noDisconn":I
    const-string v2, "log_connects"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 286
    .local v15, "noConn":I
    const-string v2, "log_button_presses"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 287
    .local v14, "noButtonPress":I
    const-string v2, "log_backlight_count"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 288
    .local v13, "noBacklightActivations":I
    const-string v2, "log_shaker_count"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 289
    .local v20, "noShakerActivations":I
    const-string v2, "log_battery_voltage"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 290
    .local v8, "batteryLevel":I
    const-string v2, "log_secs_disconnected"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 291
    .local v10, "disconnectPeriod":I
    const-string v2, "timestamp"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 294
    .local v24, "ts":I
    new-instance v25, Lcom/vectorwatch/android/models/WatchLogDataModel;

    invoke-direct/range {v25 .. v25}, Lcom/vectorwatch/android/models/WatchLogDataModel;-><init>()V

    .line 295
    .local v25, "vectorLog":Lcom/vectorwatch/android/models/WatchLogDataModel;
    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setOffset(I)V

    .line 296
    move-object/from16 v0, v25

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setNoNot(I)V

    .line 297
    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setNoGlances(I)V

    .line 298
    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setNoRxBLE(I)V

    .line 299
    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setNoTxBLE(I)V

    .line 300
    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setNoDisconn(I)V

    .line 301
    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setNoConn(I)V

    .line 302
    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setNoButtonPress(I)V

    .line 303
    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setNoBacklightActivations(I)V

    .line 304
    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setNoShakerActivations(I)V

    .line 305
    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setBatteryLevel(I)V

    .line 306
    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setDisconnectPeriod(I)V

    .line 307
    move-object/from16 v0, v25

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/WatchLogDataModel;->setId(I)V

    .line 309
    move-object/from16 v0, v25

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 312
    .end local v8    # "batteryLevel":I
    .end local v10    # "disconnectPeriod":I
    .end local v13    # "noBacklightActivations":I
    .end local v14    # "noButtonPress":I
    .end local v15    # "noConn":I
    .end local v16    # "noDisconn":I
    .end local v17    # "noGlances":I
    .end local v18    # "noNot":I
    .end local v19    # "noRxBLE":I
    .end local v20    # "noShakerActivations":I
    .end local v21    # "noTxBLE":I
    .end local v22    # "offset":I
    .end local v24    # "ts":I
    .end local v25    # "vectorLog":Lcom/vectorwatch/android/models/WatchLogDataModel;
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 315
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 318
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Logs to sync: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mSyncService:Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;

    invoke-interface {v2, v12}, Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;->syncLogs(Ljava/util/List;)Lcom/vectorwatch/android/models/ServerResponseModel;

    move-result-object v23

    .line 320
    .local v23, "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Logs sync successfully. Received message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v23 .. v23}, Lcom/vectorwatch/android/models/ServerResponseModel;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 321
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    const-string v3, "CLOUD CALLS: sync_logs"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 323
    sget-object v2, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_LOGS:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->updateDirtyFieldInTable(Landroid/net/Uri;I)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    .end local v23    # "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    :cond_2
    :goto_0
    return-void

    .line 325
    :catch_0
    move-exception v11

    .line 326
    .local v11, "error":Lretrofit/RetrofitError;
    invoke-static {v11}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    goto :goto_0
.end method

.method private syncSleep()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 166
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    const-string v1, "Trying to sync sleep to cloud."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "start_time"

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v5, "stop_time"

    aput-object v5, v2, v3

    const-string v3, "dirty_field = 1"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 172
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 173
    .local v9, "listSleep":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepCloud;>;"
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 175
    :cond_0
    new-instance v8, Lcom/vectorwatch/android/models/SleepIds;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "start_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/vectorwatch/android/models/SleepIds;-><init>(Ljava/lang/String;)V

    .line 177
    .local v8, "ids":Lcom/vectorwatch/android/models/SleepIds;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "stop_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 179
    .local v11, "stopTs":Ljava/lang/String;
    new-instance v0, Lcom/vectorwatch/android/models/SleepCloud;

    invoke-direct {v0, v8, v11}, Lcom/vectorwatch/android/models/SleepCloud;-><init>(Lcom/vectorwatch/android/models/SleepIds;Ljava/lang/String;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    .end local v8    # "ids":Lcom/vectorwatch/android/models/SleepIds;
    .end local v11    # "stopTs":Ljava/lang/String;
    :cond_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 186
    :try_start_0
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sleep to sync: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mSyncService:Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;

    invoke-interface {v0, v9}, Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;->syncActivitiesSleep(Ljava/util/List;)Lcom/vectorwatch/android/models/ServerResponseModel;

    move-result-object v10

    .line 188
    .local v10, "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sleep sync successfully. Received message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Lcom/vectorwatch/android/models/ServerResponseModel;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    const-string v1, "CLOUD CALLS: sync_sleep"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 191
    sget-object v0, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_SLEEP:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->updateDirtyFieldInTable(Landroid/net/Uri;I)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    .end local v10    # "resp":Lcom/vectorwatch/android/models/ServerResponseModel;
    :cond_2
    :goto_0
    return-void

    .line 192
    :catch_0
    move-exception v7

    .line 193
    .local v7, "error":Lretrofit/RetrofitError;
    invoke-static {v7}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    goto :goto_0
.end method

.method private syncToGoogleFit()V
    .locals 5

    .prologue
    .line 153
    const-string v2, "flag_sync_to_google_fit"

    const/4 v3, 0x0

    .line 154
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 155
    .local v1, "isSyncToGoogleFitEnabled":Z
    if-eqz v1, :cond_0

    .line 156
    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    const-string v3, "FITNESS_API: Triggered by sync adapter"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 157
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/vectorwatch/android/service/FitnessClientService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 160
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private updateDirtyFieldInTable(Landroid/net/Uri;I)V
    .locals 4
    .param p1, "tableUri"    # Landroid/net/Uri;
    .param p2, "dirtyValue"    # I

    .prologue
    .line 338
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 340
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "dirty_field"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 341
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "dirty_field = 1"

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v0, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 343
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 93
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;

    const-string v2, "SyncAdapter - Stop sync, we are in offline mode"

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 150
    :goto_0
    return-void

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;

    invoke-direct {v1, p0, p1}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;-><init>(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;Landroid/accounts/Account;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 149
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
