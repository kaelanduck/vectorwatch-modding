.class Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;
.super Ljava/lang/Object;
.source "SyncAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

.field final synthetic val$account:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;Landroid/accounts/Account;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 102
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->val$account:Landroid/accounts/Account;

    if-nez v3, :cond_0

    .line 103
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    # getter for: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$000(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "No account to perform sync to cloud."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 146
    :goto_0
    return-void

    .line 111
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    # getter for: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mAccountManager:Landroid/accounts/AccountManager;
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$100(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)Landroid/accounts/AccountManager;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->val$account:Landroid/accounts/Account;

    const-string v5, "Full access"

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "authToken":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 114
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    # getter for: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$000(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "The user should be authenticated before data is being synced"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 118
    :cond_1
    new-instance v3, Lretrofit/RestAdapter$Builder;

    invoke-direct {v3}, Lretrofit/RestAdapter$Builder;-><init>()V

    new-instance v4, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;

    invoke-direct {v4, v0}, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lretrofit/RestAdapter$Builder;->setRequestInterceptor(Lretrofit/RequestInterceptor;)Lretrofit/RestAdapter$Builder;

    move-result-object v1

    .line 120
    .local v1, "builder":Lretrofit/RestAdapter$Builder;
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v4

    # setter for: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mRestAdapter:Lretrofit/RestAdapter;
    invoke-static {v3, v4}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$202(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;Lretrofit/RestAdapter;)Lretrofit/RestAdapter;

    .line 121
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    # getter for: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mRestAdapter:Lretrofit/RestAdapter;
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$200(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)Lretrofit/RestAdapter;

    move-result-object v3

    const-class v5, Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;

    invoke-virtual {v3, v5}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;

    # setter for: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->mSyncService:Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
    invoke-static {v4, v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$302(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;)Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;

    .line 125
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    # getter for: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->log:Lorg/slf4j/Logger;
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$000(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "CLOUD CALLS: try_sync_activities_&_logs"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 128
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    # invokes: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->syncToGoogleFit()V
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$400(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)V

    .line 130
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    # invokes: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->syncActivities()V
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$500(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)V

    .line 132
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    # invokes: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->syncLogs()V
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$600(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)V

    .line 134
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$1;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    # invokes: Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->syncSleep()V
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->access$700(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 136
    .end local v0    # "authToken":Ljava/lang/String;
    .end local v1    # "builder":Lretrofit/RestAdapter$Builder;
    :catch_0
    move-exception v2

    .line 138
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v2}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 139
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v2

    .line 141
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 142
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 144
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v2}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto/16 :goto_0
.end method
