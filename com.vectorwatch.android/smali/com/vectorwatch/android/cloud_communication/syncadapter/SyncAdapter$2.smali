.class Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$2;
.super Ljava/lang/Object;
.source "SyncAdapter.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->syncActivities()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/ServerResponseModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

.field final synthetic val$timestampsToClear:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$2;->this$0:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$2;->val$timestampsToClear:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 0
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 239
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V
    .locals 2
    .param p1, "serverResponse"    # Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 233
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$2;->val$timestampsToClear:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/database/DatabaseManager;->updateCloudDirty(Ljava/util/List;)V

    .line 234
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 229
    check-cast p1, Lcom/vectorwatch/android/models/ServerResponseModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter$2;->success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V

    return-void
.end method
