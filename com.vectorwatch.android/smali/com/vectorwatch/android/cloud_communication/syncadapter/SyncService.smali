.class public Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncService;
.super Landroid/app/Service;
.source "SyncService.java"


# static fields
.field private static mSyncAdapter:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

.field private static final mSyncAdapterLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncService;->mSyncAdapterLock:Ljava/lang/Object;

    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncService;->mSyncAdapter:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncService;->mSyncAdapter:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 15
    sget-object v1, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncService;->mSyncAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    .line 16
    :try_start_0
    const-string v0, "SERVICE SYN"

    const-string v2, "In SyncService"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 17
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncService;->mSyncAdapter:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncService;->mSyncAdapter:Lcom/vectorwatch/android/cloud_communication/syncadapter/SyncAdapter;

    .line 19
    :cond_0
    monitor-exit v1

    .line 20
    return-void

    .line 19
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
