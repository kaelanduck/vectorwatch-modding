.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;
.super Ljava/lang/Object;
.source "SyncUpdatesService.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncStreamStateChangesIfNeeded(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$stream:Lcom/vectorwatch/android/models/Stream;

.field final synthetic val$streamUnsModel:Lcom/vectorwatch/android/models/StreamUnsubscribeModel;

.field final synthetic val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->val$streamUnsModel:Lcom/vectorwatch/android/models/StreamUnsubscribeModel;

    iput-object p5, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 0
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 251
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V
    .locals 4
    .param p1, "streamDownloadResponse"    # Lcom/vectorwatch/android/models/StreamDownloadResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->val$stream:Lcom/vectorwatch/android/models/Stream;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->val$streamUnsModel:Lcom/vectorwatch/android/models/StreamUnsubscribeModel;

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->val$subscriptionModel:Lcom/vectorwatch/android/models/StreamPlacementModel;

    # invokes: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->onStreamUnsubscribeChannelSuccess(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    invoke-static {v0, v1, v2, v3}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$100(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    .line 246
    :goto_0
    return-void

    .line 244
    :cond_0
    const-string v0, "Unexpected"

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 238
    check-cast p1, Lcom/vectorwatch/android/models/StreamDownloadResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;->success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V

    return-void
.end method
