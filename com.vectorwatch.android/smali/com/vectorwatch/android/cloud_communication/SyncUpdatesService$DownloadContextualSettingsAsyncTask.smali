.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;
.super Landroid/os/AsyncTask;
.source "SyncUpdatesService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DownloadContextualSettingsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field pService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Service;",
            ">;"
        }
    .end annotation
.end field

.field syncedOk:Z


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 1
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    .line 991
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 989
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->syncedOk:Z

    .line 992
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    .line 993
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/content/Intent;
    .locals 13
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v12, 0x1

    .line 999
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    const-string v11, "AT_RetrieveUserSettingsThread"

    invoke-virtual {v10, v11}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1000
    iget-object v10, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v10}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/Service;

    .line 1001
    .local v9, "service":Landroid/app/Service;
    if-nez v9, :cond_0

    .line 1002
    const/4 v8, 0x0

    .line 1052
    :goto_0
    return-object v8

    .line 1004
    :cond_0
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v10

    const-string v11, "Started downloading user settings"

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1009
    :try_start_0
    iget-object v10, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v10}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {v10}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1010
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v10, "com.vectorwatch.android"

    invoke-virtual {v1, v10}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 1012
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v10, v2

    if-ge v10, v12, :cond_1

    .line 1013
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v10

    const-string v11, "User settings download - There should be at least one account in Accounts & sync"

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1015
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1016
    .local v8, "res":Landroid/content/Intent;
    const-string v10, "ERR_MSG"

    const v11, 0x7f09010d

    invoke-virtual {v8, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 1040
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v8    # "res":Landroid/content/Intent;
    :catch_0
    move-exception v6

    .line 1041
    .local v6, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v6}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 1051
    .end local v6    # "e":Landroid/accounts/OperationCanceledException;
    :goto_1
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1052
    .restart local v8    # "res":Landroid/content/Intent;
    goto :goto_0

    .line 1021
    .end local v8    # "res":Landroid/content/Intent;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const/4 v10, 0x0

    :try_start_1
    aget-object v0, v2, v10

    .line 1023
    .local v0, "account":Landroid/accounts/Account;
    const-string v10, "Full access"

    const/4 v11, 0x1

    invoke-virtual {v1, v0, v10, v11}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 1026
    .local v3, "authToken":Ljava/lang/String;
    # invokes: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->retrieveAppStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$400(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v4

    .line 1030
    .local v4, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    :try_start_2
    invoke-interface {v4}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->getContextualFromCloud()Lcom/vectorwatch/android/models/ResponseContextual;

    move-result-object v5

    .line 1031
    .local v5, "contextualFromCloud":Lcom/vectorwatch/android/models/ResponseContextual;
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v10

    const-string v11, "CLOUD CALLS: get_settings_from_cloud"

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1033
    invoke-virtual {v9}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v5, v10}, Lcom/vectorwatch/android/utils/Helpers;->updateContextualSettings(Lcom/vectorwatch/android/models/ResponseContextual;Landroid/content/Context;)V

    .line 1035
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->syncedOk:Z
    :try_end_2
    .catch Lretrofit/RetrofitError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 1036
    .end local v5    # "contextualFromCloud":Lcom/vectorwatch/android/models/ResponseContextual;
    :catch_1
    move-exception v7

    .line 1037
    .local v7, "error":Lretrofit/RetrofitError;
    :try_start_3
    invoke-static {v7}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_1

    .line 1042
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .end local v7    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v6

    .line 1043
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1044
    .end local v6    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v6

    .line 1045
    .local v6, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v6}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1

    .line 1046
    .end local v6    # "e":Landroid/accounts/AuthenticatorException;
    :catch_4
    move-exception v6

    .line 1048
    .local v6, "e":Ljava/lang/NullPointerException;
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v10

    const-string v11, "There seem to be internet problems."

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 986
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->doInBackground([Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 1062
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Service;

    .line 1063
    .local v0, "service":Landroid/app/Service;
    if-eqz v0, :cond_0

    .line 1064
    const-string v1, "ERR_MSG"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1065
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error at downloading user settings: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ERR_MSG"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1080
    :cond_0
    :goto_0
    return-void

    .line 1068
    :cond_1
    iget-boolean v1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->syncedOk:Z

    if-eqz v1, :cond_0

    .line 1069
    const-string v1, "flag_sync_settings_from_cloud"

    invoke-static {v1, v2, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1071
    const-string v1, "flag_sync_settings_to_cloud"

    invoke-static {v1, v2, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1074
    invoke-virtual {v0}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->syncChangesFromCloudToWatch(Landroid/content/Context;)V

    .line 1076
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/UserSettingsUpdateEvent;

    invoke-direct {v2}, Lcom/vectorwatch/android/events/UserSettingsUpdateEvent;-><init>()V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 986
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1058
    return-void
.end method
