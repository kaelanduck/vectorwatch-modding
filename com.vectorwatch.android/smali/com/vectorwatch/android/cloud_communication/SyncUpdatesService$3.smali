.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;
.super Ljava/lang/Object;
.source "SyncUpdatesService.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncStreamStateChangesIfNeeded(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$stream:Lcom/vectorwatch/android/models/Stream;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;->val$stream:Lcom/vectorwatch/android/models/Stream;

    iput-object p3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 3
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 207
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fail uninstall stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V
    .locals 4
    .param p1, "streamDownloadResponse"    # Lcom/vectorwatch/android/models/StreamDownloadResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 200
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uninstall stream: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;->val$context:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->hardDeleteStream(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    .line 202
    .local v0, "deleted":I
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/StreamDeletedEvent;

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;->val$stream:Lcom/vectorwatch/android/models/Stream;

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/events/StreamDeletedEvent;-><init>(Lcom/vectorwatch/android/models/Stream;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 203
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 197
    check-cast p1, Lcom/vectorwatch/android/models/StreamDownloadResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;->success(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lretrofit/client/Response;)V

    return-void
.end method
