.class public interface abstract Lcom/vectorwatch/android/cloud_communication/SyncServiceInterface;
.super Ljava/lang/Object;
.source "SyncServiceInterface.java"


# virtual methods
.method public abstract createAccount(Lcom/vectorwatch/android/models/UserModel;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Lcom/vectorwatch/android/models/UserModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/account"
    .end annotation
.end method

.method public abstract registerWatch(Ljava/lang/String;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "watchSerialNo"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/watch/register_UsingCpuID/{watchSerialNo}"
    .end annotation
.end method

.method public abstract syncActivities(Ljava/util/List;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/UserActivityDataModel;",
            ">;)",
            "Lcom/vectorwatch/android/models/ServerResponseModel;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/activity"
    .end annotation
.end method

.method public abstract syncActivitiesSleep(Ljava/util/List;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SleepCloud;",
            ">;)",
            "Lcom/vectorwatch/android/models/ServerResponseModel;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/activity/sleep"
    .end annotation
.end method

.method public abstract syncLogs(Ljava/util/List;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/WatchLogDataModel;",
            ">;)",
            "Lcom/vectorwatch/android/models/ServerResponseModel;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/logdata"
    .end annotation
.end method

.method public abstract unregisterWatch(Ljava/lang/String;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "watchSerialNo"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/watch/unregister_UsingCpuID/{watchSerialNo}"
    .end annotation
.end method

.method public abstract userLogin(Lcom/vectorwatch/android/models/UserModel;)Lcom/vectorwatch/android/models/LoginResponseModel;
    .param p1    # Lcom/vectorwatch/android/models/UserModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/account/login"
    .end annotation
.end method
