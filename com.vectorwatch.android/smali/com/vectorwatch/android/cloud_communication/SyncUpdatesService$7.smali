.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;
.super Ljava/lang/Object;
.source "SyncUpdatesService.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncSettingsFromCloud()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/NewsletterResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .prologue
    .line 663
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 2
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 693
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    const-string v1, "SyncUpdatesService - Newsletter get settings error"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 694
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/NewsletterResponse;Lretrofit/client/Response;)V
    .locals 6
    .param p1, "newsletterResponse"    # Lcom/vectorwatch/android/models/NewsletterResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    const/4 v5, 0x0

    .line 666
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/NewsletterResponse;->getData()Lcom/vectorwatch/android/models/NewsletterGetData;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 667
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/NewsletterResponse;->getData()Lcom/vectorwatch/android/models/NewsletterGetData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/NewsletterGetData;->getNewsletters()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 668
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/NewsletterResponse;->getData()Lcom/vectorwatch/android/models/NewsletterGetData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/NewsletterGetData;->getNewsletters()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 669
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/NewsletterResponse;->getData()Lcom/vectorwatch/android/models/NewsletterGetData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/NewsletterGetData;->getNewsletters()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/NewsletterItem;

    .line 670
    .local v1, "item":Lcom/vectorwatch/android/models/NewsletterItem;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/NewsletterItem;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MOBILE_APP_NEWSLETTER_LIST"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 671
    const-string v2, "pref_news_app"

    .line 672
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/NewsletterItem;->isSubscribed()Z

    move-result v3

    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .line 671
    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 674
    :cond_0
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/NewsletterItem;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MOBILE_APP_MARKETING_NEWSLETTER_LIST"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 675
    const-string v2, "pref_news_news"

    .line 676
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/NewsletterItem;->isSubscribed()Z

    move-result v3

    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .line 675
    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 668
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 680
    .end local v1    # "item":Lcom/vectorwatch/android/models/NewsletterItem;
    :cond_2
    const-string v2, "flag_sync_settings_from_cloud"

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-static {v2, v5, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 682
    const-string v2, "flag_sync_settings_to_cloud"

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-static {v2, v5, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 685
    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-virtual {v2}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/Helpers;->syncChangesFromCloudToWatch(Landroid/content/Context;)V

    .line 687
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/events/UserSettingsUpdateEvent;

    invoke-direct {v3}, Lcom/vectorwatch/android/events/UserSettingsUpdateEvent;-><init>()V

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 689
    .end local v0    # "i":I
    :cond_3
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 663
    check-cast p1, Lcom/vectorwatch/android/models/NewsletterResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;->success(Lcom/vectorwatch/android/models/NewsletterResponse;Lretrofit/client/Response;)V

    return-void
.end method
