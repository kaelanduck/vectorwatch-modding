.class public Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;
.super Ljava/lang/Object;
.source "VectorRequestInterceptor.java"

# interfaces
.implements Lretrofit/RequestInterceptor;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private authToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;->authToken:Ljava/lang/String;

    .line 18
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;->authToken:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public intercept(Lretrofit/RequestInterceptor$RequestFacade;)V
    .locals 2
    .param p1, "request"    # Lretrofit/RequestInterceptor$RequestFacade;

    .prologue
    .line 24
    const-string v0, "Content-Type"

    const-string v1, "application/json"

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;->authToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;->authToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;->log:Lorg/slf4j/Logger;

    const-string v1, "No token saved in shared preferences"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 32
    :cond_1
    const-string v0, "Authorization"

    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;->authToken:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v0, "Timezone"

    invoke-static {}, Lcom/vectorwatch/android/utils/DateTimeUtils;->getUserTimezone()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lretrofit/RequestInterceptor$RequestFacade;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    return-void
.end method
