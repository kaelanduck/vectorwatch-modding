.class public Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;
.super Ljava/lang/Object;
.source "CloudCommunicationHandler.java"


# static fields
.field public static STREAM_APP_AUTH_ERROR_STATUS_CODE:I

.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/16 v0, 0x385

    sput v0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->STREAM_APP_AUTH_ERROR_STATUS_CODE:I

    .line 61
    const-class v0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method public static downloadAppFromLoadedAppList(Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/DownloadedAppContainer;
    .locals 7
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 158
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Auth token: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 159
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: download_app_from_loaded_app_list"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 161
    const/4 v4, 0x1

    invoke-static {p1, v4}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v0

    .line 163
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    const/4 v1, 0x0

    .line 166
    .local v1, "downloadedAppContainer":Lcom/vectorwatch/android/models/DownloadedAppContainer;
    new-instance v3, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 168
    .local v3, "extraInfo":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 171
    :try_start_0
    invoke-interface {v0, p0, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->getApp(Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Lcom/vectorwatch/android/models/DownloadedAppContainer;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 176
    :goto_0
    return-object v1

    .line 172
    :catch_0
    move-exception v2

    .line 173
    .local v2, "error":Lretrofit/RetrofitError;
    invoke-static {v2}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    goto :goto_0
.end method

.method public static getAppAdditionalData(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;Ljava/lang/String;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    .locals 6
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "version"    # Ljava/lang/String;
    .param p2, "model"    # Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;
    .param p3, "authToken"    # Ljava/lang/String;

    .prologue
    .line 130
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Auth token: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 131
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: download_app_from_loaded_app_list"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 133
    const/4 v3, 0x1

    invoke-static {p3, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v0

    .line 135
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    const/4 v2, 0x0

    .line 136
    .local v2, "response":Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    invoke-static {p2}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 138
    :try_start_0
    invoke-interface {v0, p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->callAppCallback(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 144
    return-object v2

    .line 139
    :catch_0
    move-exception v1

    .line 140
    .local v1, "error":Lretrofit/RetrofitError;
    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    .line 141
    throw v1
.end method

.method public static getAppAuthMethod(Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/AuthMethod;
    .locals 6
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 275
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: getAppAuthMethod"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 276
    invoke-static {p1}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v1

    .line 277
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    new-instance v3, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 278
    .local v3, "model":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 279
    const/4 v0, 0x0

    .line 281
    .local v0, "authMethod":Lcom/vectorwatch/android/models/AuthMethodResponse;
    :try_start_0
    invoke-interface {v1, p0, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->getAppAuthMethod(Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Lcom/vectorwatch/android/models/AuthMethodResponse;

    move-result-object v0

    .line 282
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/AuthMethodResponse;->getAuthMethod()Lcom/vectorwatch/android/models/AuthMethod;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 286
    :goto_0
    return-object v4

    .line 283
    :catch_0
    move-exception v2

    .line 284
    .local v2, "error":Lretrofit/RetrofitError;
    invoke-static {v2}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    .line 286
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static getAppPushData(Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponseRoot;
    .locals 7
    .param p0, "authToken"    # Ljava/lang/String;
    .param p1, "pushKey"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 382
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "CLOUD CALLS: get app push data"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 383
    const/4 v5, 0x1

    invoke-static {p0, v5}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v0

    .line 385
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 386
    .local v3, "options":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "key"

    invoke-virtual {v3, v5, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    :try_start_0
    invoke-interface {v0, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->getAppPushData(Ljava/util/HashMap;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponseRoot;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 397
    :goto_0
    return-object v4

    .line 391
    :catch_0
    move-exception v2

    .line 392
    .local v2, "error":Lretrofit/RetrofitError;
    invoke-static {v2}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    goto :goto_0

    .line 393
    .end local v2    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v1

    .line 394
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static getAppSettingOptions(Ljava/lang/String;Lcom/vectorwatch/android/models/AppOptionsRequest;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "request"    # Lcom/vectorwatch/android/models/AppOptionsRequest;
    .param p2, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/AppOptionsRequest;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339
    const/4 v3, 0x1

    invoke-static {p2, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v0

    .line 341
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    const/4 v2, 0x0

    .line 343
    .local v2, "streamSettingOptionsResponse":Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;
    :try_start_0
    invoke-static {p1}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 344
    invoke-interface {v0, p0, p1}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->getAppSettingOptions(Ljava/lang/String;Lcom/vectorwatch/android/models/AppOptionsRequest;)Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 348
    :goto_0
    if-eqz v2, :cond_0

    .line 349
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;->getOptions()Ljava/util/List;

    move-result-object v3

    .line 352
    :goto_1
    return-object v3

    .line 345
    :catch_0
    move-exception v1

    .line 346
    .local v1, "error":Lretrofit/RetrofitError;
    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    goto :goto_0

    .line 352
    .end local v1    # "error":Lretrofit/RetrofitError;
    :cond_0
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static getAppSettings(Ljava/lang/String;Lcom/vectorwatch/android/models/AuthCredentialsRequest;Ljava/lang/String;)Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .locals 5
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "request"    # Lcom/vectorwatch/android/models/AuthCredentialsRequest;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 290
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: getAppSettings"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 291
    invoke-static {p2}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v0

    .line 293
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    const/4 v2, 0x0

    .line 295
    .local v2, "streamSettingsResponse":Lcom/vectorwatch/android/models/PossibleSettingsResponse;
    invoke-static {p1}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 298
    :try_start_0
    invoke-interface {v0, p0, p1}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->getAppSettings(Ljava/lang/String;Lcom/vectorwatch/android/models/AuthCredentialsRequest;)Lcom/vectorwatch/android/models/PossibleSettingsResponse;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 303
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/PossibleSettingsResponse;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v3

    :goto_0
    return-object v3

    .line 299
    :catch_0
    move-exception v1

    .line 300
    .local v1, "error":Lretrofit/RetrofitError;
    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    .line 301
    throw v1

    .line 303
    .end local v1    # "error":Lretrofit/RetrofitError;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getListOfDefaultsFromStore(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppDetails;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 64
    sget-object v9, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v10, "CLOUD CALLS: get_list_of_defaults_from_store"

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 66
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 69
    .local v7, "shape":Ljava/lang/String;
    invoke-static {v7}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->getShapeFilters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 71
    .local v0, "andFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Filter;>;"
    new-instance v4, Lcom/vectorwatch/android/models/CloudRequestModel;

    invoke-direct {v4, v11, v11, v11, v0}, Lcom/vectorwatch/android/models/CloudRequestModel;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;)V

    .line 72
    .local v4, "model":Lcom/vectorwatch/android/models/CloudRequestModel;
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    .line 74
    .local v3, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v3, v4}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 75
    .local v8, "stuff":Ljava/lang/String;
    sget-object v9, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Default filters sent to cloud: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 78
    const/4 v9, 0x1

    invoke-static {p1, v9}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v1

    .line 80
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DownloadedAppDetails;>;"
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 86
    :try_start_0
    sget-object v9, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v10, "Getting default apps."

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 87
    invoke-interface {v1, v4}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->getDefaults(Lcom/vectorwatch/android/models/CloudRequestModel;)Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;

    move-result-object v6

    .line 89
    .local v6, "retrievedFullAppsContainer":Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;
    iget-object v9, v6, Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;->data:Lcom/vectorwatch/android/models/RetrievedFullAppsContainer$FullCloudAppsList;

    iget-object v5, v9, Lcom/vectorwatch/android/models/RetrievedFullAppsContainer$FullCloudAppsList;->apps:Ljava/util/List;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .end local v6    # "retrievedFullAppsContainer":Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;
    :goto_0
    return-object v5

    .line 90
    :catch_0
    move-exception v2

    .line 91
    .local v2, "error":Lretrofit/RetrofitError;
    invoke-static {v2}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    goto :goto_0
.end method

.method private static getShapeFilters(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p0, "shape"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Filter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: get_shape_filters"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 105
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v2, "shapesSupported":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "round"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    :goto_0
    new-instance v1, Lcom/vectorwatch/android/models/Filter;

    const-string v3, "deviceCompatibility"

    invoke-direct {v1, v3, v2}, Lcom/vectorwatch/android/models/Filter;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 112
    .local v1, "shapeFilter":Lcom/vectorwatch/android/models/Filter;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v0, "andFilterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/Filter;>;"
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    return-object v0

    .line 109
    .end local v0    # "andFilterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/Filter;>;"
    .end local v1    # "shapeFilter":Lcom/vectorwatch/android/models/Filter;
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getStreamSettingOptions(Lcom/vectorwatch/android/models/Stream;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "settingName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p4, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/Stream;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 365
    .local p3, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: getStreamSettingOptions"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 366
    const/4 v4, 0x1

    invoke-static {p4, v4}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v0

    .line 369
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    :try_start_0
    new-instance v2, Lcom/vectorwatch/android/models/StreamOptionsRequest;

    invoke-direct {v2, p1, p2, p3}, Lcom/vectorwatch/android/models/StreamOptionsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 370
    .local v2, "request":Lcom/vectorwatch/android/models/StreamOptionsRequest;
    invoke-static {v2}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 371
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v4, v5, v2}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->getStreamSettingOptions(Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/StreamOptionsRequest;)Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;

    move-result-object v3

    .line 372
    .local v3, "streamSettingOptionsResponse":Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;->getOptions()Ljava/util/List;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    return-object v4

    .line 374
    .end local v2    # "request":Lcom/vectorwatch/android/models/StreamOptionsRequest;
    .end local v3    # "streamSettingOptionsResponse":Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;
    :catch_0
    move-exception v1

    .line 375
    .local v1, "error":Lretrofit/RetrofitError;
    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    .line 376
    throw v1
.end method

.method public static getStreamSettings(Lcom/vectorwatch/android/models/Stream;Ljava/lang/String;)Lcom/vectorwatch/android/models/settings/PossibleSettings;
    .locals 6
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 315
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: getStreamAuthMethod"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 316
    invoke-static {p1}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v0

    .line 318
    .local v0, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    const/4 v3, 0x0

    .line 319
    .local v3, "streamSettingsResponse":Lcom/vectorwatch/android/models/PossibleSettingsResponse;
    new-instance v2, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v2}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 320
    .local v2, "request":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v2}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 322
    :try_start_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getVersion()Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v4, v5, v2}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->getStreamSettings(Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Lcom/vectorwatch/android/models/PossibleSettingsResponse;
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 328
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/PossibleSettingsResponse;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v4

    :goto_0
    return-object v4

    .line 323
    :catch_0
    move-exception v1

    .line 324
    .local v1, "error":Lretrofit/RetrofitError;
    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    .line 325
    throw v1

    .line 328
    .end local v1    # "error":Lretrofit/RetrofitError;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static notifyCloudAppStateChange(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 11
    .param p0, "appId"    # I
    .param p1, "appUuid"    # Ljava/lang/String;
    .param p2, "newState"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    .line 187
    invoke-static {p3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 188
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v0, "com.vectorwatch.android"

    invoke-virtual {v1, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .line 192
    .local v8, "accounts":[Landroid/accounts/Account;
    if-eqz p2, :cond_0

    sget-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    invoke-virtual {v0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->UNINSTALL:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    .line 193
    invoke-virtual {v0}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    if-eqz v8, :cond_2

    array-length v0, v8

    const/4 v3, 0x1

    if-ge v0, v3, :cond_3

    .line 198
    :cond_2
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v3, "Should not attempt to sync data to cloud while no account is logged in."

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 202
    :cond_3
    aget-object v0, v8, v10

    if-nez v0, :cond_4

    .line 203
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v3, "Should not attempt to sync data to cloud while the only account identified is null."

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :cond_4
    aget-object v2, v8, v10

    .line 208
    .local v2, "account":Landroid/accounts/Account;
    move-object v4, p1

    .line 209
    .local v4, "uuid":Ljava/lang/String;
    move v7, p0

    .line 210
    .local v7, "appIdCopy":I
    move-object v6, p3

    .line 213
    .local v6, "contextCopy":Landroid/content/Context;
    new-instance v5, Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    invoke-direct {v5}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;-><init>()V

    .line 214
    .local v5, "extraInfo":Lcom/vectorwatch/android/models/BaseCloudRequestModel;
    invoke-static {v5}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    .line 216
    new-instance v9, Ljava/lang/Thread;

    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler$1;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Landroid/content/Context;I)V

    invoke-direct {v9, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 264
    .local v9, "thread":Ljava/lang/Thread;
    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private static populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V
    .locals 6
    .param p0, "model"    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    .prologue
    .line 482
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 485
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "last_known_system_info"

    const-class v5, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 486
    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 488
    .local v1, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-eqz v1, :cond_0

    .line 489
    const-string v3, "kernel"

    invoke-static {v1, v3}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setKernelVersion(Ljava/lang/String;)V

    .line 492
    :cond_0
    const-string v3, "android"

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setDeviceType(Ljava/lang/String;)V

    .line 493
    const-string v3, "2.0.2"

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setAppVersion(Ljava/lang/String;)V

    .line 494
    invoke-static {v0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->getEndpointArn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setEndpointArn(Ljava/lang/String;)V

    .line 496
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 498
    .local v2, "shape":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 499
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "populateSystemData: Shape is null!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 506
    :goto_0
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 507
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setCpuId(Ljava/lang/String;)V

    .line 509
    :cond_1
    return-void

    .line 500
    :cond_2
    const-string v3, "round"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 501
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setDeviceCompatibility(Ljava/lang/String;)V

    goto :goto_0

    .line 503
    :cond_3
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setDeviceCompatibility(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static retrieveStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .locals 1
    .param p0, "authToken"    # Ljava/lang/String;

    .prologue
    .line 436
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v0

    return-object v0
.end method

.method public static retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .locals 5
    .param p0, "authToken"    # Ljava/lang/String;
    .param p1, "version"    # I

    .prologue
    .line 448
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: retrieve_store_interface"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 450
    new-instance v3, Lretrofit/RestAdapter$Builder;

    invoke-direct {v3}, Lretrofit/RestAdapter$Builder;-><init>()V

    new-instance v4, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lretrofit/RestAdapter$Builder;->setRequestInterceptor(Lretrofit/RequestInterceptor;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/cloud_communication/syncadapter/CloudErrorHandler;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/syncadapter/CloudErrorHandler;-><init>()V

    .line 451
    invoke-virtual {v3, v4}, Lretrofit/RestAdapter$Builder;->setErrorHandler(Lretrofit/ErrorHandler;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 454
    .local v0, "builder":Lretrofit/RestAdapter$Builder;
    packed-switch p1, :pswitch_data_0

    .line 462
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 466
    .local v2, "restAdapter":Lretrofit/RestAdapter;
    :goto_0
    const-class v3, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    invoke-virtual {v2, v3}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    .line 468
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    return-object v1

    .line 456
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_0
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 457
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 459
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_1
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV2Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 460
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 454
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static syncDefaultsToCloud(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p0, "authToken"    # Ljava/lang/String;
    .param p1, "appContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407
    const/4 v6, 0x1

    invoke-static {p0, v6}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v2

    .line 409
    .local v2, "cloudInterface":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->getCurrentWatchAppsState(Landroid/content/Context;)Lcom/vectorwatch/android/models/UpdateDefaultsModel;

    move-result-object v5

    .line 411
    .local v5, "updateDefaultsModel":Lcom/vectorwatch/android/models/UpdateDefaultsModel;
    invoke-static {p1}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppIdsInRunningOrder(Landroid/content/Context;)Ljava/util/List;

    move-result-object v4

    .line 412
    .local v4, "runningApps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 413
    .local v0, "appId":Ljava/lang/Integer;
    sget-object v7, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SETUP: running apps = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 415
    .end local v0    # "appId":Ljava/lang/Integer;
    :cond_0
    sget-object v6, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SETUP: What gets synced as defaults = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/UpdateDefaultsModel;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 417
    const/4 v1, 0x0

    .line 419
    .local v1, "apps":Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;
    :try_start_0
    sget-object v6, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v7, "SETUP: Trying to sync defaults to cloud."

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 420
    invoke-interface {v2, v5}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->updateDefaults(Lcom/vectorwatch/android/models/UpdateDefaultsModel;)Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;

    move-result-object v1

    .line 421
    sget-object v6, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v7, "SETUP: Managed to sync defaults to cloud."

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lretrofit/RetrofitError; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    :goto_1
    if-eqz v1, :cond_1

    iget-object v6, v1, Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;->data:Lcom/vectorwatch/android/models/RetrievedFullAppsContainer$FullCloudAppsList;

    if-eqz v6, :cond_1

    .line 428
    sget-object v6, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    const-string v7, "SETUP: Returning apps."

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 429
    iget-object v6, v1, Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;->data:Lcom/vectorwatch/android/models/RetrievedFullAppsContainer$FullCloudAppsList;

    iget-object v6, v6, Lcom/vectorwatch/android/models/RetrievedFullAppsContainer$FullCloudAppsList;->apps:Ljava/util/List;

    .line 432
    :goto_2
    return-object v6

    .line 422
    :catch_0
    move-exception v3

    .line 423
    .local v3, "error":Lretrofit/RetrofitError;
    sget-object v6, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SETUP: error at syncing defaults to cloud: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 424
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;

    goto :goto_1

    .line 432
    .end local v3    # "error":Lretrofit/RetrofitError;
    :cond_1
    const/4 v6, 0x0

    goto :goto_2
.end method
