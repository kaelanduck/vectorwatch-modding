.class public Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;
.super Ljava/lang/Object;
.source "PushNotificationHandler.java"


# static fields
.field public static final PAYLOAD:Ljava/lang/String; = "p"

.field public static final PUSH_ACTIVITY_ALERTS:I = 0xa

.field public static final PUSH_APP_AUTH_EXPIRED:I = 0x6

.field public static final PUSH_APP_UPDATE:I = 0x4

.field public static final PUSH_CONFIG:I = 0x2

.field public static final PUSH_DATA:I = 0x3

.field public static final PUSH_MULTICHANNEL:I = 0xb

.field public static final PUSH_OS_AUTO_UPDATE:I = 0x0

.field public static final PUSH_OS_UPDATE:I = 0x1

.field public static final PUSH_STORE_NEW:I = 0x7

.field public static final PUSH_STREAM_AUTH_EXPIRED:I = 0x5

.field public static final STORE_COUNT:Ljava/lang/String; = "count"

.field public static final STREAM_UUID:Ljava/lang/String; = "streamUUID"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final VERSION:Ljava/lang/String; = "v"

.field private static final log:Lorg/slf4j/Logger;

.field private static refreshAlert:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-class v0, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "x2"    # Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .param p3, "x3"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 60
    invoke-static {p0, p1, p2, p3}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->onStreamUnsubscribeChannelSuccess(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    return-void
.end method

.method private static getChannelSettingsChange(Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)Lcom/vectorwatch/android/models/StreamChannelSettings;
    .locals 3
    .param p0, "streamUnsModel"    # Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .param p1, "subscriptionModel"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 351
    .line 352
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/StreamChannelSettings;-><init>()V

    .line 355
    .local v0, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 357
    .local v1, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setSubscriptions(Ljava/util/List;)V

    .line 360
    return-object v0

    .line 353
    .end local v0    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v1    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v0

    goto :goto_0
.end method

.method private static getPayload(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 7
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 370
    const/4 v2, 0x0

    .line 373
    .local v2, "payload":Lorg/json/JSONArray;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 375
    .local v1, "obj":Lorg/json/JSONObject;
    const-string v4, "v"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 376
    const-string v4, "v"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 378
    .local v3, "version":I
    packed-switch v3, :pswitch_data_0

    .line 384
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "JSON needs to have a version field. Should not get here."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 391
    .end local v1    # "obj":Lorg/json/JSONObject;
    .end local v3    # "version":I
    :cond_0
    :goto_0
    return-object v2

    .line 381
    .restart local v1    # "obj":Lorg/json/JSONObject;
    .restart local v3    # "version":I
    :pswitch_0
    const-string v4, "p"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 382
    goto :goto_0

    .line 387
    .end local v1    # "obj":Lorg/json/JSONObject;
    .end local v3    # "version":I
    :catch_0
    move-exception v0

    .line 388
    .local v0, "e":Lorg/json/JSONException;
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not parse malformed JSON: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 389
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 378
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static handleActivityAlert(Lorg/json/JSONObject;)V
    .locals 10
    .param p0, "obj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 177
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;

    invoke-direct {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;-><init>()V

    .line 178
    .local v0, "alert":Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;
    const-string v7, "activityType"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->setActivityType(Ljava/lang/String;)V

    .line 179
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->setTimestamp(J)V

    .line 180
    const-string v7, "type"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->setType(I)V

    .line 181
    const-string v7, "message"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->setMessage(Ljava/lang/String;)V

    .line 182
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->setUuid(Ljava/lang/String;)V

    .line 184
    const-string v7, "longValues"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 185
    .local v3, "longValueArray":Lorg/json/JSONArray;
    new-instance v2, Lio/realm/RealmList;

    invoke-direct {v2}, Lio/realm/RealmList;-><init>()V

    .line 186
    .local v2, "longList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 187
    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/LongObject;-><init>(Ljava/lang/Long;)V

    invoke-virtual {v2, v7}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 186
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 189
    :cond_0
    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;->setLongValues(Lio/realm/RealmList;)V

    .line 191
    const-string v7, "stringValues"

    invoke-virtual {p0, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 192
    .local v6, "stringValueArray":Lorg/json/JSONArray;
    new-instance v5, Lio/realm/RealmList;

    invoke-direct {v5}, Lio/realm/RealmList;-><init>()V

    .line 193
    .local v5, "stringList":Lio/realm/RealmList;, "Lio/realm/RealmList<Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;>;"
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v1, v7, :cond_1

    .line 194
    new-instance v7, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;

    invoke-virtual {v6, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/StringObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Lio/realm/RealmList;->add(Lio/realm/RealmModel;)Z

    .line 193
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 197
    :cond_1
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v4

    .line 198
    .local v4, "realm":Lio/realm/Realm;
    invoke-static {}, Lcom/vectorwatch/android/database/DatabaseManager;->getInstance()Lcom/vectorwatch/android/database/DatabaseManager;

    move-result-object v7

    invoke-virtual {v7, v4, v0}, Lcom/vectorwatch/android/database/DatabaseManager;->createActivityAlert(Lio/realm/Realm;Lcom/vectorwatch/android/ui/tabmenufragments/activity/models/ActivityAlert;)V

    .line 199
    invoke-virtual {v4}, Lio/realm/Realm;->close()V

    .line 200
    return-void
.end method

.method private static handleIncomingStreamDataVersion2(Lcom/vectorwatch/android/models/StreamValueModel;Landroid/content/Context;)V
    .locals 30
    .param p0, "streamValueModel"    # Lcom/vectorwatch/android/models/StreamValueModel;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 203
    if-nez p0, :cond_1

    .line 290
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 207
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/StreamValueModel;->getV()Ljava/lang/Integer;

    move-result-object v21

    .line 209
    .local v21, "version":Ljava/lang/Integer;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/StreamValueModel;->getV()Ljava/lang/Integer;

    move-result-object v22

    if-eqz v22, :cond_0

    .line 210
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/models/StreamValueModel;->getPayload()Ljava/util/List;

    move-result-object v18

    .line 212
    .local v18, "updatesList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamValueModel$Payload;>;"
    if-eqz v18, :cond_0

    .line 215
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v22

    packed-switch v22, :pswitch_data_0

    goto :goto_0

    .line 219
    :pswitch_1
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_6

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/vectorwatch/android/models/StreamValueModel$Payload;

    .line 220
    .local v16, "update":Lcom/vectorwatch/android/models/StreamValueModel$Payload;
    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->getChannelLabel()Ljava/lang/String;

    move-result-object v4

    .line 221
    .local v4, "channelLabel":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->getStreamUUID()Ljava/lang/String;

    move-result-object v13

    .line 222
    .local v13, "streamUuid":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->getData()Ljava/lang/String;

    move-result-object v20

    .line 223
    .local v20, "value":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->getSecondsToLive()Ljava/lang/Integer;

    move-result-object v9

    .line 224
    .local v9, "secondsToLive":Ljava/lang/Integer;
    invoke-virtual/range {v16 .. v16}, Lcom/vectorwatch/android/models/StreamValueModel$Payload;->getTtl()Ljava/lang/Integer;

    move-result-object v15

    .line 226
    .local v15, "ttl":Ljava/lang/Integer;
    if-eqz v4, :cond_0

    if-eqz p1, :cond_0

    if-eqz v20, :cond_0

    .line 230
    const/16 v19, -0x1

    .line 231
    .local v19, "validity":I
    if-eqz v9, :cond_4

    .line 232
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 237
    :cond_3
    :goto_1
    move-object/from16 v0, v20

    move/from16 v1, v19

    move-object/from16 v2, p1

    invoke-static {v4, v0, v1, v2}, Lcom/vectorwatch/android/managers/StreamsManager;->setUpdateData(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)I

    move-result v17

    .line 238
    .local v17, "updatedRows":I
    sget-object v23, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Updated rows count = "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " value for update = "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-interface/range {v23 .. v24}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 240
    const/16 v23, 0x1

    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_2

    .line 244
    new-instance v11, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-direct {v11}, Lcom/vectorwatch/android/models/StreamChannelSettings;-><init>()V

    .line 245
    .local v11, "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    invoke-virtual {v11, v4}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setUniqueLabel(Ljava/lang/String;)V

    .line 246
    sget-object v23, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v24, "Received update for non-existing stream"

    invoke-interface/range {v23 .. v24}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 248
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 250
    .local v6, "detailsForStreamsToUnsubscribe":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamUnsubscribeModel;>;"
    invoke-static {v13, v11}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->setStreamUnsubscribeModel(Ljava/lang/String;Lcom/vectorwatch/android/models/StreamChannelSettings;)Lcom/vectorwatch/android/models/StreamUnsubscribeModel;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_2

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;

    .line 254
    .local v12, "streamUnsModel":Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getSubscriptionModel()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v24

    if-nez v24, :cond_5

    new-instance v14, Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-direct {v14}, Lcom/vectorwatch/android/models/StreamPlacementModel;-><init>()V

    .line 257
    .local v14, "subscriptionModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    :goto_3
    new-instance v5, Lcom/vectorwatch/android/models/ChannelSettingsChange;

    invoke-direct {v5}, Lcom/vectorwatch/android/models/ChannelSettingsChange;-><init>()V

    .line 259
    .local v5, "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    invoke-static {v12, v14}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->getChannelSettingsChange(Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v24

    .line 258
    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/vectorwatch/android/models/ChannelSettingsChange;->setOldChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 261
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getStreamUuid()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v10

    .line 263
    .local v10, "stream":Lcom/vectorwatch/android/models/Stream;
    new-instance v24, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler$1;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-direct {v0, v10, v1, v12, v14}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler$1;-><init>(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v10, v5, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->unsubscribeChannel(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V

    goto :goto_2

    .line 233
    .end local v5    # "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    .end local v6    # "detailsForStreamsToUnsubscribe":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamUnsubscribeModel;>;"
    .end local v10    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v11    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v12    # "streamUnsModel":Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .end local v14    # "subscriptionModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    .end local v17    # "updatedRows":I
    :cond_4
    if-eqz v15, :cond_3

    .line 234
    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    const-wide/16 v28, 0x3e8

    div-long v26, v26, v28

    sub-long v24, v24, v26

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v19, v0

    goto/16 :goto_1

    .line 255
    .restart local v6    # "detailsForStreamsToUnsubscribe":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamUnsubscribeModel;>;"
    .restart local v11    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .restart local v12    # "streamUnsModel":Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .restart local v17    # "updatedRows":I
    :cond_5
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getSubscriptionModel()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v14

    goto :goto_3

    .line 283
    .end local v4    # "channelLabel":Ljava/lang/String;
    .end local v6    # "detailsForStreamsToUnsubscribe":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamUnsubscribeModel;>;"
    .end local v9    # "secondsToLive":Ljava/lang/Integer;
    .end local v11    # "streamChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v12    # "streamUnsModel":Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .end local v13    # "streamUuid":Ljava/lang/String;
    .end local v15    # "ttl":Ljava/lang/Integer;
    .end local v16    # "update":Lcom/vectorwatch/android/models/StreamValueModel$Payload;
    .end local v17    # "updatedRows":I
    .end local v19    # "validity":I
    .end local v20    # "value":Ljava/lang/String;
    :cond_6
    invoke-static/range {p1 .. p1}, Lcom/vectorwatch/android/managers/StreamsManager;->getUpdates(Landroid/content/Context;)Ljava/util/List;

    move-result-object v8

    .line 284
    .local v8, "routeList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/PushUpdateRoute;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_4
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/models/PushUpdateRoute;

    .line 285
    .local v7, "route":Lcom/vectorwatch/android/models/PushUpdateRoute;
    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->sendStreamValue(Landroid/content/Context;Lcom/vectorwatch/android/models/PushUpdateRoute;)Ljava/util/UUID;

    goto :goto_4

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static handleOsUpdate(Lcom/vectorwatch/android/service/ble/messages/PushOsUpdate;Landroid/content/Context;)V
    .locals 5
    .param p0, "osUpdate"    # Lcom/vectorwatch/android/service/ble/messages/PushOsUpdate;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 395
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v3, "handleOsUpdate(..)"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 396
    invoke-virtual {p0}, Lcom/vectorwatch/android/service/ble/messages/PushOsUpdate;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "receivedUrl":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 398
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "URL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 400
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v3, "TEST - push notif handler."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 401
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/vectorwatch/com/android/vos/update/UpdateWatchActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 403
    .local v1, "updateOsActivity":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 404
    const-string v2, "update_kernel_file_url.vos"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 408
    .end local v1    # "updateOsActivity":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static handleStreamAuthExpired(Ljava/lang/String;Landroid/content/Context;)V
    .locals 14
    .param p0, "streamUuid"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v13, 0x8000000

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 416
    invoke-static {p0, p1}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v6

    .line 417
    .local v6, "stream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v6, :cond_0

    .line 418
    new-instance v7, Landroid/support/v7/app/NotificationCompat$Builder;

    invoke-direct {v7, p1}, Landroid/support/v7/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v8, 0x7f0200f0

    .line 419
    invoke-virtual {v7, v8}, Landroid/support/v7/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const v8, 0x7f09016f

    .line 420
    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const v8, 0x7f09016e

    new-array v9, v12, [Ljava/lang/Object;

    .line 421
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {p1, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const/4 v8, 0x3

    .line 422
    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    .line 423
    invoke-virtual {v7, v12}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/NotificationCompat$Builder;

    .line 425
    .local v1, "builder":Landroid/support/v7/app/NotificationCompat$Builder;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    long-to-float v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Float;->intValue()I

    move-result v3

    .line 427
    .local v3, "notification_id":I
    new-instance v5, Landroid/content/Intent;

    const-class v7, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v5, p1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 428
    .local v5, "resultIntent":Landroid/content/Intent;
    const-string v7, "action"

    invoke-virtual {v5, v7, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 429
    const-string v7, "notification_id"

    invoke-virtual {v5, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 430
    const-string v7, "streamUUID"

    invoke-virtual {v5, v7, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 432
    invoke-static {p1, v11, v5, v13}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 433
    .local v4, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v1, v4}, Landroid/support/v7/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 435
    new-instance v0, Landroid/content/Intent;

    const-class v7, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 436
    .local v0, "actionIntent":Landroid/content/Intent;
    const-string v7, "action"

    invoke-virtual {v0, v7, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 437
    const-string v7, "notification_id"

    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 438
    const-string v7, "streamUUID"

    invoke-virtual {v0, v7, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    const v7, 0x7f09016d

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v11, v0, v13}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v1, v11, v7, v8}, Landroid/support/v7/app/NotificationCompat$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 441
    const-string v7, "notification"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 442
    .local v2, "mNotificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v1}, Landroid/support/v7/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 444
    .end local v0    # "actionIntent":Landroid/content/Intent;
    .end local v1    # "builder":Landroid/support/v7/app/NotificationCompat$Builder;
    .end local v2    # "mNotificationManager":Landroid/app/NotificationManager;
    .end local v3    # "notification_id":I
    .end local v4    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v5    # "resultIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private static onStreamUnsubscribeChannelSuccess(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "streamUnsModel"    # Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .param p3, "subscriptionModel"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 304
    invoke-virtual {p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 305
    invoke-virtual {p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 306
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stream remove from face - stream name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 308
    invoke-virtual {p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 307
    invoke-static {p1, v0, v1, v2, p0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->deactivateStream(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)V

    .line 311
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PRIVATE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 312
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    .line 311
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PUBLIC:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    :cond_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/vectorwatch/android/managers/StreamsManager;->deleteStreamFromDatabase(Ljava/lang/String;Landroid/content/Context;)Z

    .line 316
    :cond_1
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getStreamUuid()Ljava/lang/String;

    move-result-object v0

    const-string v1, "560e9eb0e48057d1360d3700b9c77777"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    const-string v0, "update_phone_battery"

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 323
    :cond_2
    :goto_0
    return-void

    .line 321
    :cond_3
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsubscribe with no info for stream: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static pushNotification(Ljava/lang/String;Landroid/content/Context;)V
    .locals 11
    .param p0, "data"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    if-nez p0, :cond_1

    .line 84
    sget-object v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v9, "PushNotification: Push data is null"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    sget-object v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "PushNotification: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 91
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 93
    .local v1, "dataJSON":Lorg/json/JSONObject;
    const-string v8, "type"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "type"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    const/4 v9, 0x7

    if-ne v8, v9, :cond_2

    .line 94
    const-string v8, "count"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 95
    const-string v8, "count"

    invoke-virtual {v1, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/vectorwatch/android/VectorApplication;->sStoreCount:I

    .line 96
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v8

    new-instance v9, Lcom/vectorwatch/android/events/StoreCountBadgeEvent;

    invoke-direct {v9}, Lcom/vectorwatch/android/events/StoreCountBadgeEvent;-><init>()V

    invoke-virtual {v8, v9}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 97
    sget v8, Lcom/vectorwatch/android/VectorApplication;->sStoreCount:I

    invoke-static {p1, v8}, Lme/leolin/shortcutbadger/ShortcutBadger;->applyCount(Landroid/content/Context;I)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 162
    .end local v1    # "dataJSON":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 163
    .local v2, "e":Lorg/json/JSONException;
    sget-object v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v9, "Push not a json object"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 164
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 100
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v1    # "dataJSON":Lorg/json/JSONObject;
    :cond_2
    :try_start_1
    invoke-static {p0}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->getPayload(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 101
    .local v6, "payload":Lorg/json/JSONArray;
    if-nez v6, :cond_3

    .line 102
    sget-object v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v9, "Payload of push notification is null."

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 165
    .end local v1    # "dataJSON":Lorg/json/JSONObject;
    .end local v6    # "payload":Lorg/json/JSONArray;
    :catch_1
    move-exception v2

    .line 166
    .local v2, "e":Ljava/lang/NullPointerException;
    sget-object v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v9, "Push must not be null"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 167
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 106
    .end local v2    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "dataJSON":Lorg/json/JSONObject;
    .restart local v6    # "payload":Lorg/json/JSONArray;
    :cond_3
    :try_start_2
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    .line 107
    .local v3, "gson":Lcom/google/gson/Gson;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v8

    if-ge v4, v8, :cond_4

    .line 109
    :try_start_3
    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 110
    .local v5, "obj":Lorg/json/JSONObject;
    const-string v8, "type"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 146
    :pswitch_0
    sget-object v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v9, "Push notification should have a predefined type. Should not get here."

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 107
    .end local v5    # "obj":Lorg/json/JSONObject;
    :goto_2
    :pswitch_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 115
    .restart local v5    # "obj":Lorg/json/JSONObject;
    :pswitch_2
    new-instance v8, Lcom/vectorwatch/android/service/ble/messages/PushOsUpdate;

    invoke-direct {v8, v5}, Lcom/vectorwatch/android/service/ble/messages/PushOsUpdate;-><init>(Lorg/json/JSONObject;)V

    invoke-static {v8, p1}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->handleOsUpdate(Lcom/vectorwatch/android/service/ble/messages/PushOsUpdate;Landroid/content/Context;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 148
    .end local v5    # "obj":Lorg/json/JSONObject;
    :catch_2
    move-exception v2

    .line 149
    .local v2, "e":Lorg/json/JSONException;
    :try_start_4
    sget-object v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Could not parse malformed JSON: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 121
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v5    # "obj":Lorg/json/JSONObject;
    :pswitch_3
    :try_start_5
    const-class v8, Lcom/vectorwatch/android/models/StreamValueModel;

    invoke-virtual {v3, p0, v8}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/models/StreamValueModel;

    .line 122
    .local v7, "streamValueModel":Lcom/vectorwatch/android/models/StreamValueModel;
    invoke-static {v7, p1}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->handleIncomingStreamDataVersion2(Lcom/vectorwatch/android/models/StreamValueModel;Landroid/content/Context;)V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 151
    .end local v5    # "obj":Lorg/json/JSONObject;
    .end local v7    # "streamValueModel":Lcom/vectorwatch/android/models/StreamValueModel;
    :catch_3
    move-exception v2

    .line 152
    .local v2, "e":Ljava/lang/Exception;
    :try_start_6
    sget-object v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->log:Lorg/slf4j/Logger;

    const-string v9, "Push notification general exception. This handler is here in order to avoid app crash. Should not get here."

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 153
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_2

    .line 125
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v5    # "obj":Lorg/json/JSONObject;
    :pswitch_4
    :try_start_7
    new-instance v0, Landroid/content/Intent;

    const-class v8, Lcom/vectorwatch/android/service/AppPushService;

    invoke-direct {v0, p1, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    .local v0, "appPushService":Landroid/content/Intent;
    const-string v8, "key"

    const-string v9, "cacheKey"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    const-string v8, "appUuid"

    const-string v9, "appUuid"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_2

    .line 131
    .end local v0    # "appPushService":Landroid/content/Intent;
    :pswitch_5
    const-string v8, "streamUUID"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->handleStreamAuthExpired(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_2

    .line 142
    :pswitch_6
    const/4 v8, 0x1

    sput-boolean v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->refreshAlert:Z

    .line 143
    invoke-static {v5}, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->handleActivityAlert(Lorg/json/JSONObject;)V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    .line 157
    .end local v5    # "obj":Lorg/json/JSONObject;
    :cond_4
    :try_start_8
    sget-boolean v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->refreshAlert:Z

    if-eqz v8, :cond_0

    .line 158
    const/4 v8, 0x0

    sput-boolean v8, Lcom/vectorwatch/android/cloud_communication/PushNotificationHandler;->refreshAlert:Z

    .line 159
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v8

    new-instance v9, Lcom/vectorwatch/android/events/RefreshActivityAlertsEvent;

    invoke-direct {v9}, Lcom/vectorwatch/android/events/RefreshActivityAlertsEvent;-><init>()V

    invoke-virtual {v8, v9}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method private static setStreamUnsubscribeModel(Ljava/lang/String;Lcom/vectorwatch/android/models/StreamChannelSettings;)Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .locals 2
    .param p0, "streamUuid"    # Ljava/lang/String;
    .param p1, "streamChannelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;

    .prologue
    const/4 v1, 0x0

    .line 333
    new-instance v0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;-><init>()V

    .line 334
    .local v0, "streamUnsubscribeModel":Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->setStream(Lcom/vectorwatch/android/models/Stream;)V

    .line 335
    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->setStreamUuid(Ljava/lang/String;)V

    .line 337
    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->setSubscriptionModel(Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    .line 338
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 340
    return-object v0
.end method
