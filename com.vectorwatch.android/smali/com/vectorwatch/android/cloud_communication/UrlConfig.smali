.class public Lcom/vectorwatch/android/cloud_communication/UrlConfig;
.super Ljava/lang/Object;
.source "UrlConfig.java"


# static fields
.field public static final API_DEV:Ljava/lang/String; = "http://52.18.220.232:8080"

.field public static final API_PRODUCTION:Ljava/lang/String; = "https://endpoint.vector.watch"

.field public static final API_STAGE:Ljava/lang/String; = "https://api.vector.watch"

.field private static API_URI:Ljava/lang/String; = null

.field private static API_URI_V1:Ljava/lang/String; = null

.field private static API_URI_V2:Ljava/lang/String; = null

.field public static final LEGAL_TERMS_URL:Ljava/lang/String; = "http://www.vectorwatch.com/terms-of-use"

.field public static final PATH_DETAILS:Ljava/lang/String; = "/VectorCloud/rest"

.field public static final PATH_DETAILS_V1:Ljava/lang/String; = "/VectorCloud/rest/v1"

.field public static final PATH_DETAILS_V2:Ljava/lang/String; = "/VectorCloud/rest/v2"

.field public static final RECOVER_PASS_URL:Ljava/lang/String; = "/VectorCloud/forgotPassword.html"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "https://endpoint.vector.watch/VectorCloud/rest/"

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI:Ljava/lang/String;

    .line 14
    const-string v0, "https://endpoint.vector.watch/VectorCloud/rest/v1"

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI_V1:Ljava/lang/String;

    .line 15
    const-string v0, "https://endpoint.vector.watch/VectorCloud/rest/v2"

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI_V2:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getApiUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI:Ljava/lang/String;

    return-object v0
.end method

.method public static getApiV1Uri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI_V1:Ljava/lang/String;

    return-object v0
.end method

.method public static getApiV2Uri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI_V2:Ljava/lang/String;

    return-object v0
.end method

.method public static getLegalTermsUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const-string v0, "http://www.vectorwatch.com/terms-of-use"

    return-object v0
.end method

.method public static getRecoverPassUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI_V1:Ljava/lang/String;

    const-string v1, "https://endpoint.vector.watch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const-string v0, "https://endpoint.vector.watch/VectorCloud/forgotPassword.html"

    .line 31
    :goto_0
    return-object v0

    .line 28
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI_V1:Ljava/lang/String;

    const-string v1, "http://52.18.220.232:8080"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    const-string v0, "http://52.18.220.232:8080/VectorCloud/forgotPassword.html"

    goto :goto_0

    .line 31
    :cond_1
    const-string v0, "https://api.vector.watch/VectorCloud/forgotPassword.html"

    goto :goto_0
.end method

.method public static setApiUri(Ljava/lang/String;)V
    .locals 2
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/VectorCloud/rest/v1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI_V1:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/VectorCloud/rest/v2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI_V2:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/VectorCloud/rest"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->API_URI:Ljava/lang/String;

    .line 56
    return-void
.end method
