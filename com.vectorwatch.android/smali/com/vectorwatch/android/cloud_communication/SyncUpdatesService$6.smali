.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$6;
.super Ljava/lang/Object;
.source "SyncUpdatesService.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncSettingsToCloud()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/ServerResponseModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .prologue
    .line 641
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$6;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 0
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 651
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V
    .locals 3
    .param p1, "serverResponse"    # Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    const/4 v2, 0x0

    .line 644
    const-string v0, "flag_sync_settings_to_cloud"

    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$6;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 645
    const-string v0, "flag_sync_settings_from_cloud"

    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$6;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 646
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 641
    check-cast p1, Lcom/vectorwatch/android/models/ServerResponseModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$6;->success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V

    return-void
.end method
