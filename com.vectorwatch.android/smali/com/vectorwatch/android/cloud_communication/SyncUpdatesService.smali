.class public Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;
.super Landroid/app/Service;
.source "SyncUpdatesService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserContextualSettingsAsyncTask;,
        Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;,
        Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;,
        Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadUserSettingsAsyncTask;,
        Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const-class v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 1180
    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "x2"    # Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .param p3, "x3"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 81
    invoke-static {p0, p1, p2, p3}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->onStreamUnsubscribeChannelSuccess(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    return-void
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Lretrofit/RetrofitError;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;
    .param p1, "x1"    # Lretrofit/RetrofitError;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->handleRetrieveAppDefaultsFailure(Lretrofit/RetrofitError;)V

    return-void
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Lretrofit/RetrofitError;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;
    .param p1, "x1"    # Lretrofit/RetrofitError;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->handleRetrieveStreamDefaultsFailure(Lretrofit/RetrofitError;)V

    return-void
.end method

.method static synthetic access$400(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-static {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->retrieveAppStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 81
    invoke-static {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->instantiateSupportedAppsList(Landroid/content/Context;)V

    return-void
.end method

.method private confirmSystemUpdateIfNeeded(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const-wide/16 v10, -0x1

    .line 335
    const-string v4, "flag_confirm_update"

    invoke-static {v4, v5, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 337
    .local v1, "needsConfirmKernel":Z
    const-string v4, "flag_confirm_update"

    invoke-static {v4, v5, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 340
    .local v0, "needsConfirmBootloader":Z
    if-eqz v1, :cond_0

    .line 341
    const-string v4, "update_id"

    invoke-static {v4, v10, v11, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getLongPreference(Ljava/lang/String;JLandroid/content/Context;)J

    move-result-wide v2

    .line 344
    .local v2, "updateId":J
    cmp-long v4, v2, v10

    if-eqz v4, :cond_0

    .line 345
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->KERNEL:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    new-instance v6, Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

    .line 347
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchOldCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 348
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    invoke-static {p0, v4, v5, v6}, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;->confirmUpdateDoneOnWatchWithCpuId(Landroid/content/Context;Ljava/lang/Long;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Z

    .line 352
    .end local v2    # "updateId":J
    :cond_0
    if-eqz v0, :cond_1

    .line 353
    const-string v4, "update_id_bootloader"

    invoke-static {v4, v10, v11, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getLongPreference(Ljava/lang/String;JLandroid/content/Context;)J

    move-result-wide v2

    .line 356
    .restart local v2    # "updateId":J
    cmp-long v4, v2, v10

    if-eqz v4, :cond_1

    .line 357
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;->BOOTLOADER:Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;

    new-instance v6, Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;

    .line 359
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchOldCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 360
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9}, Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    invoke-static {p0, v4, v5, v6}, Lcom/vectorwatch/com/android/vos/update/WatchCloudUpdateEndpoints;->confirmUpdateDoneOnWatchWithCpuId(Landroid/content/Context;Ljava/lang/Long;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Z

    .line 363
    .end local v2    # "updateId":J
    :cond_1
    return-void
.end method

.method private static getChannelSettingsChange(Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)Lcom/vectorwatch/android/models/StreamChannelSettings;
    .locals 3
    .param p0, "streamUnsModel"    # Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .param p1, "subscriptionModel"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 292
    .line 293
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/StreamChannelSettings;-><init>()V

    .line 296
    .local v0, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 298
    .local v1, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setSubscriptions(Ljava/util/List;)V

    .line 301
    return-object v0

    .line 294
    .end local v0    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v1    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v0

    goto :goto_0
.end method

.method private handleRetrieveAppDefaultsFailure(Lretrofit/RetrofitError;)V
    .locals 9
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    const v8, 0x7f090101

    const/4 v7, 0x2

    .line 527
    const/4 v4, 0x1

    sput-boolean v4, Lcom/vectorwatch/android/VectorApplication;->hasError:Z

    .line 529
    if-nez p1, :cond_0

    .line 531
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    .line 532
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 531
    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 563
    :goto_0
    return-void

    .line 537
    :cond_0
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v4

    sget-object v5, Lretrofit/RetrofitError$Kind;->UNEXPECTED:Lretrofit/RetrofitError$Kind;

    if-ne v4, v5, :cond_1

    .line 538
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 539
    .local v0, "errorMessage":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v5, v7, v0}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 544
    .end local v0    # "errorMessage":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v1

    .line 545
    .local v1, "response":Lretrofit/client/Response;
    if-eqz v1, :cond_3

    .line 546
    invoke-virtual {v1}, Lretrofit/client/Response;->getStatus()I

    move-result v2

    .line 547
    .local v2, "status":I
    invoke-static {v2}, Lcom/vectorwatch/android/utils/CloudStatusCodes;->getDefaultStringIdForStatus(I)I

    move-result v3

    .line 548
    .local v3, "statusCodeMessageResId":I
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DEFAULT APPS: error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 550
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 551
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    .line 552
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 551
    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 555
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    .line 556
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 555
    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 560
    .end local v2    # "status":I
    .end local v3    # "statusCodeMessageResId":I
    :cond_3
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    .line 561
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 560
    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private handleRetrieveStreamDefaultsFailure(Lretrofit/RetrofitError;)V
    .locals 9
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    const v8, 0x7f0900ff

    const/4 v7, 0x2

    .line 571
    const/4 v4, 0x1

    sput-boolean v4, Lcom/vectorwatch/android/VectorApplication;->hasError:Z

    .line 573
    if-nez p1, :cond_0

    .line 575
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    .line 576
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 575
    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 607
    :goto_0
    return-void

    .line 581
    :cond_0
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v4

    sget-object v5, Lretrofit/RetrofitError$Kind;->UNEXPECTED:Lretrofit/RetrofitError$Kind;

    if-ne v4, v5, :cond_1

    .line 582
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 583
    .local v0, "errorMessage":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v5, v7, v0}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 588
    .end local v0    # "errorMessage":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v1

    .line 589
    .local v1, "response":Lretrofit/client/Response;
    if-eqz v1, :cond_3

    .line 590
    invoke-virtual {v1}, Lretrofit/client/Response;->getStatus()I

    move-result v2

    .line 591
    .local v2, "status":I
    invoke-static {v2}, Lcom/vectorwatch/android/utils/CloudStatusCodes;->getDefaultStringIdForStatus(I)I

    move-result v3

    .line 592
    .local v3, "statusCodeMessageResId":I
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DEFAULT STREAM: error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 594
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 595
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    .line 596
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 595
    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 599
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    .line 600
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 599
    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 604
    .end local v2    # "status":I
    .end local v3    # "statusCodeMessageResId":I
    :cond_3
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v4

    new-instance v5, Lcom/vectorwatch/android/events/InstallAppEvent;

    .line 605
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 604
    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private handleSyncDefaults(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 429
    const-string v0, "flag_sync_system_apps"

    invoke-static {v0, v3, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "flag_sync_defaults_to_cloud"

    .line 430
    invoke-static {v0, v4, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 431
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v1, "SETUP: Sync with cloud - Default apps - Sync apps flag not set. Sync to cloud flag not set either."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 437
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v1, "Sync with cloud - Default apps - No paired device."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 441
    :cond_2
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->isAccountLoggedIn()Z

    move-result v0

    if-nez v0, :cond_3

    .line 442
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v1, "Sync with cloud - Default apps - No account found."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 446
    :cond_3
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SETUP: SYNC WITH CLOUD - Retrieving defaults. Flag set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "flag_sync_system_apps"

    .line 447
    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 446
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 448
    const-string v0, "flag_sync_system_apps"

    invoke-static {v0, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 450
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->resetDefaultAppsTables(Landroid/content/Context;)V

    .line 452
    sput-boolean v3, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    .line 454
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;-><init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;)V

    invoke-static {p0, v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getDefaultsStreams(Landroid/content/Context;Lretrofit/Callback;)V

    goto :goto_0

    .line 512
    :cond_4
    const-string v0, "flag_sync_defaults_to_cloud"

    invoke-static {v0, v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SETUP: SYNC WITH CLOUD - Storing defaults to cloud. Flag set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "flag_sync_defaults_to_cloud"

    .line 515
    invoke-static {v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 514
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 516
    sput-boolean v3, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    .line 517
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;

    invoke-direct {v0, p0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;-><init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Landroid/app/Service;)V

    new-array v1, v4, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method private static instantiateSupportedAppsList(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 952
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 982
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 983
    return-void
.end method

.method private isAccountLoggedIn()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 713
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 714
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 716
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v3, v1

    if-ge v3, v2, :cond_0

    .line 717
    const/4 v2, 0x0

    .line 720
    :cond_0
    return v2
.end method

.method private notifyCloudAppChangesToCloud(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 372
    sget-object v7, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v8, "CLOUD CALL - service to notify app changes."

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 373
    move-object v3, p1

    .line 376
    .local v3, "contextCopy":Landroid/content/Context;
    invoke-static {v3}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getModifiedCloudAppsList(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v5

    .line 378
    .local v5, "modifiedAppsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/vectorwatch/android/models/AppIdentification;Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 379
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vectorwatch/android/models/AppIdentification;Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 380
    .local v6, "state":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/AppIdentification;

    .line 382
    .local v1, "appIdentification":Lcom/vectorwatch/android/models/AppIdentification;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AppIdentification;->getAppId()Ljava/lang/Integer;

    move-result-object v0

    .line 383
    .local v0, "appId":Ljava/lang/Integer;
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/AppIdentification;->getAppUuid()Ljava/lang/String;

    move-result-object v2

    .line 384
    .local v2, "appUuid":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 385
    sget-object v8, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CLOUD CALL - trying to notify about: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 387
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8, v2, v6, v3}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->notifyCloudAppStateChange(ILjava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 391
    .end local v0    # "appId":Ljava/lang/Integer;
    .end local v1    # "appIdentification":Lcom/vectorwatch/android/models/AppIdentification;
    .end local v2    # "appUuid":Ljava/lang/String;
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/vectorwatch/android/models/AppIdentification;Ljava/lang/String;>;"
    .end local v6    # "state":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private static onStreamUnsubscribeChannelSuccess(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "streamUnsModel"    # Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .param p3, "subscriptionModel"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 316
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stream remove from face - stream name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 317
    invoke-virtual {p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 318
    invoke-virtual {p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 317
    invoke-static {p1, v0, v1, v2, p0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->deactivateStream(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)V

    .line 321
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PRIVATE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 322
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    .line 321
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 322
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->APP_PUBLIC:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 323
    :cond_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/vectorwatch/android/managers/StreamsManager;->deleteStreamFromDatabase(Ljava/lang/String;Landroid/content/Context;)Z

    .line 326
    :cond_1
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getStreamUuid()Ljava/lang/String;

    move-result-object v0

    const-string v1, "560e9eb0e48057d1360d3700b9c77777"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 327
    const-string v0, "update_phone_battery"

    const-wide/16 v2, -0x1

    invoke-static {v0, v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 330
    :cond_2
    return-void
.end method

.method private registerWatchIfNeeded(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 401
    const-string v3, "bonded_watch_dirty"

    invoke-static {v3, v5, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    .line 403
    .local v1, "needToRegister":Z
    if-eqz v1, :cond_0

    .line 404
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.vectorwatch.android"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 405
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v3, v0

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 406
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v4, "No account found in account manager. Needed for completing registration."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 420
    .end local v0    # "accounts":[Landroid/accounts/Account;
    :cond_0
    :goto_0
    return-void

    .line 411
    .restart local v0    # "accounts":[Landroid/accounts/Account;
    :cond_1
    invoke-static {p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 413
    .local v2, "watchUniqueName":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 414
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v4, "No paired watch was found in order to complete registration."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 418
    :cond_2
    aget-object v3, v0, v5

    invoke-static {v2, v3, p1}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->registerWatch(Ljava/lang/String;Landroid/accounts/Account;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static retrieveAppStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .locals 6
    .param p0, "authToken"    # Ljava/lang/String;

    .prologue
    .line 85
    new-instance v4, Lretrofit/RestAdapter$Builder;

    invoke-direct {v4}, Lretrofit/RestAdapter$Builder;-><init>()V

    new-instance v5, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/cloud_communication/VectorRequestInterceptor;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lretrofit/RestAdapter$Builder;->setRequestInterceptor(Lretrofit/RequestInterceptor;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 86
    .local v0, "builder":Lretrofit/RestAdapter$Builder;
    new-instance v4, Lcom/google/gson/GsonBuilder;

    invoke-direct {v4}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v2

    .line 87
    .local v2, "gson":Lcom/google/gson/Gson;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v4

    new-instance v5, Lretrofit/converter/GsonConverter;

    invoke-direct {v5, v2}, Lretrofit/converter/GsonConverter;-><init>(Lcom/google/gson/Gson;)V

    .line 88
    invoke-virtual {v4, v5}, Lretrofit/RestAdapter$Builder;->setConverter(Lretrofit/converter/Converter;)Lretrofit/RestAdapter$Builder;

    move-result-object v4

    .line 89
    invoke-virtual {v4}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v3

    .line 90
    .local v3, "restAdapter":Lretrofit/RestAdapter;
    const-class v4, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    invoke-virtual {v3, v4}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;

    .line 92
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    return-object v1
.end method

.method private static setStreamUnsubscribeModel(Lcom/vectorwatch/android/models/Stream;)Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .locals 2
    .param p0, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 274
    new-instance v0, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;-><init>()V

    .line 276
    .local v0, "streamUnsubscribeModel":Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->setStream(Lcom/vectorwatch/android/models/Stream;)V

    .line 277
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->setStreamUuid(Ljava/lang/String;)V

    .line 278
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->setSubscriptionModel(Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    .line 279
    invoke-virtual {p0}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 281
    return-object v0
.end method

.method private syncParseInstallationId(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 162
    const-string v1, "flag_send_parse_installation_id"

    const/4 v2, 0x0

    invoke-static {v1, v2, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 165
    .local v0, "syncInstallationId":Z
    if-eqz v0, :cond_0

    .line 166
    new-instance v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$2;

    invoke-direct {v1, p0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$2;-><init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Landroid/content/Context;)V

    invoke-static {p0, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->updateAccountInfo(Landroid/content/Context;Lretrofit/Callback;)V

    .line 180
    :cond_0
    return-void
.end method

.method private syncSettingsFromCloud()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 660
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->isAccountLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadUserSettingsAsyncTask;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadUserSettingsAsyncTask;-><init>(Landroid/app/Service;)V

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadUserSettingsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 662
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;-><init>(Landroid/app/Service;)V

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$DownloadContextualSettingsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 663
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$7;-><init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;)V

    invoke-static {p0, v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getNewsletterSettings(Landroid/content/Context;Lretrofit/Callback;)V

    .line 699
    :goto_0
    return-void

    .line 697
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v1, "SyncUpdatesService - Trying to sync settings from cloud but user is not logged in."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncSettingsIfNeeded(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 610
    const-string v0, "flag_sync_settings_from_cloud"

    invoke-static {v0, v1, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "flag_sync_settings_to_cloud"

    .line 611
    invoke-static {v0, v1, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 619
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v1, "ALARMS - SYNC TO CLOUD"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 620
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncSettingsToCloud()V

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    const-string v0, "flag_sync_settings_from_cloud"

    invoke-static {v0, v1, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 626
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v1, "ALARMS - SYNC FROM CLOUD"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 627
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncSettingsFromCloud()V

    goto :goto_0

    .line 631
    :cond_2
    const-string v0, "flag_sync_settings_to_cloud"

    invoke-static {v0, v1, p1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v1, "ALARMS - SYNC TO CLOUD"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 634
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncSettingsToCloud()V

    goto :goto_0
.end method

.method private syncSettingsToCloud()V
    .locals 2

    .prologue
    .line 640
    invoke-direct {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->isAccountLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 641
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$6;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$6;-><init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;)V

    invoke-static {p0, v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->uploadUserSettings(Landroid/content/Context;Lretrofit/Callback;)V

    .line 653
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserContextualSettingsAsyncTask;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserContextualSettingsAsyncTask;-><init>(Landroid/app/Service;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserContextualSettingsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 657
    :goto_0
    return-void

    .line 655
    :cond_0
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v1, "SyncUpdatesService - Trying to sync settings to cloud but user is not logged in."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private syncStreamStateChangesIfNeeded(Landroid/content/Context;)V
    .locals 23
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 189
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v3, "SYNC STREAM STATE"

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 191
    sget-object v2, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_DELETE:Lcom/vectorwatch/android/models/Stream$State;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamsWithGivenState(Lcom/vectorwatch/android/models/Stream$State;Landroid/content/Context;)Ljava/util/List;

    move-result-object v21

    .line 194
    .local v21, "streamsMarkedForDelete":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    if-eqz v21, :cond_0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 196
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/Stream;

    .line 197
    .local v4, "stream":Lcom/vectorwatch/android/models/Stream;
    new-instance v3, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v4, v1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$3;-><init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;)V

    move-object/from16 v0, p1

    invoke-static {v0, v4, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->uninstallStream(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lretrofit/Callback;)V

    goto :goto_0

    .line 214
    .end local v4    # "stream":Lcom/vectorwatch/android/models/Stream;
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_UNSUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamsWithGivenState(Lcom/vectorwatch/android/models/Stream$State;Landroid/content/Context;)Ljava/util/List;

    move-result-object v22

    .line 218
    .local v22, "streamsMarkedForUnsubscribe":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .local v17, "detailsForUnsubscribe":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamUnsubscribeModel;>;"
    if-eqz v22, :cond_3

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 221
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/Stream;

    .line 222
    .restart local v4    # "stream":Lcom/vectorwatch/android/models/Stream;
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "UNSUBSCRIBE REQUESTED for "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 223
    invoke-static {v4}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->setStreamUnsubscribeModel(Lcom/vectorwatch/android/models/Stream;)Lcom/vectorwatch/android/models/StreamUnsubscribeModel;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 226
    .end local v4    # "stream":Lcom/vectorwatch/android/models/Stream;
    :cond_1
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;

    .line 227
    .local v6, "streamUnsModel":Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getStreamUuid()Ljava/lang/String;

    move-result-object v20

    .line 229
    .local v20, "streamToUnsUuid":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getSubscriptionModel()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v7, Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-direct {v7}, Lcom/vectorwatch/android/models/StreamPlacementModel;-><init>()V

    .line 232
    .local v7, "subscriptionModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    :goto_3
    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v4

    .line 234
    .restart local v4    # "stream":Lcom/vectorwatch/android/models/Stream;
    new-instance v16, Lcom/vectorwatch/android/models/ChannelSettingsChange;

    invoke-direct/range {v16 .. v16}, Lcom/vectorwatch/android/models/ChannelSettingsChange;-><init>()V

    .line 235
    .local v16, "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    invoke-static {v6, v7}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getChannelSettingsChange(Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/ChannelSettingsChange;->setOldChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 237
    new-instance v2, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    invoke-direct/range {v2 .. v7}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$4;-><init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;Lcom/vectorwatch/android/models/StreamUnsubscribeModel;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v4, v1, v2}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->unsubscribeChannel(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V

    goto :goto_2

    .line 230
    .end local v4    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v7    # "subscriptionModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    .end local v16    # "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    :cond_2
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/StreamUnsubscribeModel;->getSubscriptionModel()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v7

    goto :goto_3

    .line 256
    .end local v6    # "streamUnsModel":Lcom/vectorwatch/android/models/StreamUnsubscribeModel;
    .end local v20    # "streamToUnsUuid":Ljava/lang/String;
    :cond_3
    sget-object v2, Lcom/vectorwatch/android/models/Stream$State;->MARKED_FOR_SUBSCRIBE:Lcom/vectorwatch/android/models/Stream$State;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getStreamsWithGivenState(Lcom/vectorwatch/android/models/Stream$State;Landroid/content/Context;)Ljava/util/List;

    move-result-object v19

    .line 257
    .local v19, "markedForSubscribe":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    if-eqz v19, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 258
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v18

    if-ge v0, v2, :cond_4

    .line 259
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/Stream;

    .line 260
    .restart local v4    # "stream":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v9

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppUuid()Ljava/lang/String;

    move-result-object v10

    .line 261
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v2

    .line 260
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 261
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 262
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream;->getStreamPlacement()Lcom/vectorwatch/android/models/StreamPlacementModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v2

    .line 261
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    const/4 v14, 0x0

    move-object v8, v4

    move-object/from16 v15, p1

    .line 260
    invoke-static/range {v8 .. v15}, Lcom/vectorwatch/android/managers/StreamsManager;->placeStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIIZLandroid/content/Context;)V

    .line 258
    add-int/lit8 v18, v18, 0x1

    goto :goto_4

    .line 265
    .end local v4    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v18    # "i":I
    :cond_4
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 703
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 98
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 99
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 106
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->needsLogin(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    new-instance v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$1;-><init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;)V

    invoke-static {v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->checkCloudStatus(Lretrofit/Callback;)V

    .line 120
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->stopSelf()V

    .line 128
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/NetworkUtils;->getConnectivityStatusString(Landroid/content/Context;)I

    move-result v0

    .line 131
    .local v0, "status":I
    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 132
    invoke-direct {p0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncParseInstallationId(Landroid/content/Context;)V

    .line 134
    invoke-direct {p0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->registerWatchIfNeeded(Landroid/content/Context;)V

    .line 136
    invoke-direct {p0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->confirmSystemUpdateIfNeeded(Landroid/content/Context;)V

    .line 139
    invoke-direct {p0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncStreamStateChangesIfNeeded(Landroid/content/Context;)V

    .line 141
    sget-object v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v2, "SETUP: need to check compatibility."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 142
    const-string v1, "check_compatibility"

    const/4 v2, 0x0

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 144
    sget-object v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v2, "SETUP: no need to check compatibility."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 145
    sget-object v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SETUP: sSetupWatchActive = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 146
    sget-object v1, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;

    const-string v2, "SYNC_APPS: sync from cloud - check compat not needed."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 150
    sget-boolean v1, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    if-nez v1, :cond_1

    .line 151
    invoke-direct {p0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->handleSyncDefaults(Landroid/content/Context;)V

    .line 153
    :cond_1
    invoke-direct {p0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncSettingsIfNeeded(Landroid/content/Context;)V

    .line 156
    :cond_2
    invoke-direct {p0, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->notifyCloudAppChangesToCloud(Landroid/content/Context;)V

    .line 158
    :cond_3
    const/4 v1, 0x1

    return v1
.end method
