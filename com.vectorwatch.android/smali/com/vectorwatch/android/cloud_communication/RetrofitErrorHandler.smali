.class public Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;
.super Ljava/lang/Object;
.source "RetrofitErrorHandler.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method public static handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;
    .locals 6
    .param p0, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 25
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RetrofitError "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 27
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler$1;->$SwitchMap$retrofit$RetrofitError$Kind:[I

    invoke-virtual {p0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v4

    invoke-virtual {v4}, Lretrofit/RetrofitError$Kind;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 59
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v3

    :goto_0
    return-object v3

    .line 29
    :pswitch_0
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Network error - check network connectivity"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lretrofit/RetrofitError;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 30
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/NoNetworkConnectionEvent;

    const-string v5, "Check network connection"

    invoke-direct {v4, v5}, Lcom/vectorwatch/android/events/NoNetworkConnectionEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 31
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v3

    goto :goto_0

    .line 34
    :pswitch_1
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Conversion error - check response expeced format"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v3

    goto :goto_0

    .line 38
    :pswitch_2
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Unexpected error - internal error while attempting to execute request - application should crash: "

    .line 39
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    .line 38
    invoke-interface {v3, v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 40
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v3

    goto :goto_0

    .line 43
    :pswitch_3
    :try_start_0
    const-class v3, Lcom/vectorwatch/android/models/ErrorMessageModel;

    invoke-virtual {p0, v3}, Lretrofit/RetrofitError;->getBodyAs(Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/ErrorMessageModel;

    .line 44
    .local v0, "body":Lcom/vectorwatch/android/models/ErrorMessageModel;
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->log:Lorg/slf4j/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HTTP error message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/vectorwatch/android/models/ErrorMessageModel;->error:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " | error status code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v5

    invoke-virtual {v5}, Lretrofit/client/Response;->getStatus()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/client/Response;->getStatus()I

    move-result v2

    .line 47
    .local v2, "status":I
    const/16 v3, 0x199

    if-ne v2, v3, :cond_0

    .line 48
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/DuplicateAccountEvent;

    iget-object v5, v0, Lcom/vectorwatch/android/models/ErrorMessageModel;->error:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/vectorwatch/android/events/DuplicateAccountEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 51
    :cond_0
    const/16 v3, 0x191

    if-ne v2, v3, :cond_1

    .line 52
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/IncorrectLoginEvent;

    iget-object v5, v0, Lcom/vectorwatch/android/models/ErrorMessageModel;->error:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/vectorwatch/android/events/IncorrectLoginEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .end local v0    # "body":Lcom/vectorwatch/android/models/ErrorMessageModel;
    .end local v2    # "status":I
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v3

    goto/16 :goto_0

    .line 54
    :catch_0
    move-exception v1

    .line 55
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Retrofit intercepted a bad response from cloud."

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_1

    .line 27
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
