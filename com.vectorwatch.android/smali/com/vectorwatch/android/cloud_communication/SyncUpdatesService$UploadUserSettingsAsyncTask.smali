.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;
.super Landroid/os/AsyncTask;
.source "SyncUpdatesService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UploadUserSettingsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field pService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Service;",
            ">;"
        }
    .end annotation
.end field

.field syncedOk:Z


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 1
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    .line 1088
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1086
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;->syncedOk:Z

    .line 1089
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    .line 1090
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/content/Intent;
    .locals 13
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v12, 0x1

    .line 1096
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    const-string v11, "AT_RetrieveUserSettingsThread"

    invoke-virtual {v10, v11}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1097
    iget-object v10, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v10}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/Service;

    .line 1098
    .local v9, "service":Landroid/app/Service;
    if-nez v9, :cond_0

    .line 1099
    const/4 v8, 0x0

    .line 1153
    :goto_0
    return-object v8

    .line 1101
    :cond_0
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v10

    const-string v11, "Started uploading user settings"

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1106
    :try_start_0
    iget-object v10, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v10}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {v10}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1107
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v10, "com.vectorwatch.android"

    invoke-virtual {v1, v10}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 1109
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v10, v2

    if-ge v10, v12, :cond_1

    .line 1110
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v10

    const-string v11, "User settings upload - There should be at least one account in Accounts & sync"

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1112
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1113
    .local v8, "res":Landroid/content/Intent;
    const-string v10, "ERR_MSG"

    const v11, 0x7f09010d

    invoke-virtual {v8, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 1140
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v8    # "res":Landroid/content/Intent;
    :catch_0
    move-exception v5

    .line 1141
    .local v5, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v5}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 1152
    .end local v5    # "e":Landroid/accounts/OperationCanceledException;
    :goto_1
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 1153
    .restart local v8    # "res":Landroid/content/Intent;
    goto :goto_0

    .line 1118
    .end local v8    # "res":Landroid/content/Intent;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const/4 v10, 0x0

    :try_start_1
    aget-object v0, v2, v10

    .line 1120
    .local v0, "account":Landroid/accounts/Account;
    const-string v10, "Full access"

    const/4 v11, 0x1

    invoke-virtual {v1, v0, v10, v11}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 1123
    .local v3, "authToken":Ljava/lang/String;
    # invokes: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->retrieveAppStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    invoke-static {v3}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$400(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v4

    .line 1128
    .local v4, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    :try_start_2
    invoke-virtual {v9}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    .line 1127
    invoke-static {v10}, Lcom/vectorwatch/android/utils/Helpers;->packUserSettingsForUpload(Landroid/content/Context;)Lcom/vectorwatch/android/models/LocalSettingsModel;

    move-result-object v7

    .line 1129
    .local v7, "localSettingsModel":Lcom/vectorwatch/android/models/LocalSettingsModel;
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "UPDATE SETTINGS: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    new-instance v12, Lcom/google/gson/Gson;

    invoke-direct {v12}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v12, v7}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1130
    invoke-interface {v4, v7}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;->putSettingsInCloud(Lcom/vectorwatch/android/models/LocalSettingsModel;)Lcom/vectorwatch/android/models/ServerResponseModel;

    .line 1131
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v10

    const-string v11, "CLOUD CALLS: put_settings_in_cloud"

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1133
    invoke-static {v9}, Lcom/vectorwatch/android/utils/Helpers;->resetFlagsForChangedSettings(Landroid/content/Context;)V

    .line 1135
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;->syncedOk:Z
    :try_end_2
    .catch Lretrofit/RetrofitError; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 1136
    .end local v7    # "localSettingsModel":Lcom/vectorwatch/android/models/LocalSettingsModel;
    :catch_1
    move-exception v6

    .line 1137
    .local v6, "error":Lretrofit/RetrofitError;
    :try_start_3
    invoke-static {v6}, Lcom/vectorwatch/android/cloud_communication/RetrofitErrorHandler;->handleRetrofitError(Lretrofit/RetrofitError;)Lretrofit/RetrofitError$Kind;
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_1

    .line 1142
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
    .end local v6    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v5

    .line 1143
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1144
    .end local v5    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 1145
    .local v5, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v5}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1

    .line 1146
    .end local v5    # "e":Landroid/accounts/AuthenticatorException;
    :catch_4
    move-exception v5

    .line 1148
    .local v5, "e":Ljava/lang/NullPointerException;
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v10

    const-string v11, "There seem to be internet problems."

    invoke-interface {v10, v11}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1083
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;->doInBackground([Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 1163
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Service;

    .line 1164
    .local v0, "service":Landroid/app/Service;
    if-eqz v0, :cond_0

    .line 1165
    const-string v1, "ERR_MSG"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1166
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error at downloading user settings: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ERR_MSG"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1177
    :cond_0
    :goto_0
    return-void

    .line 1169
    :cond_1
    iget-boolean v1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;->syncedOk:Z

    if-eqz v1, :cond_0

    .line 1170
    const-string v1, "flag_sync_settings_to_cloud"

    invoke-static {v1, v2, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 1172
    const-string v1, "flag_sync_settings_from_cloud"

    invoke-static {v1, v2, v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1083
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$UploadUserSettingsAsyncTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1159
    return-void
.end method
