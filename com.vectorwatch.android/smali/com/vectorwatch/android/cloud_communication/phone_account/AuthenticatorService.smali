.class public Lcom/vectorwatch/android/cloud_communication/phone_account/AuthenticatorService;
.super Landroid/app/Service;
.source "AuthenticatorService.java"


# instance fields
.field private authenticator:Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;

.field private log:Lorg/slf4j/Logger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 11
    const-class v0, Lcom/vectorwatch/android/cloud_communication/phone_account/AuthenticatorService;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/AuthenticatorService;->log:Lorg/slf4j/Logger;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/AuthenticatorService;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getBinder()...  returning the AccountAuthenticator binder for intent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/AuthenticatorService;->authenticator:Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;

    invoke-virtual {v0}, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/AuthenticatorService;->log:Lorg/slf4j/Logger;

    const-string v1, "SampleSyncAdapter Authentication Service started."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/AuthenticatorService;->authenticator:Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;

    .line 18
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/AuthenticatorService;->log:Lorg/slf4j/Logger;

    const-string v1, "SampleSyncAdapter Authentication Service stopped."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 23
    return-void
.end method
