.class public Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "Authenticator.java"


# static fields
.field private static sAuthToken:Ljava/lang/String;

.field private static sIsLoginSuccess:Z


# instance fields
.field private log:Lorg/slf4j/Logger;

.field mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->sIsLoginSuccess:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    .line 29
    const-class v0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->log:Lorg/slf4j/Logger;

    .line 34
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->mContext:Landroid/content/Context;

    .line 35
    return-void
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 26
    sput-object p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->sAuthToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;)Lorg/slf4j/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 26
    sput-boolean p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->sIsLoginSuccess:Z

    return p0
.end method

.method private confirmPassword(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 162
    .local v0, "userInfo":Lcom/vectorwatch/android/models/UserModel;
    iget-object v1, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->prepareUserDataForLogin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/UserModel;

    move-result-object v0

    .line 164
    new-instance v1, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator$2;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator$2;-><init>(Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;)V

    invoke-static {v0, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->accountLogin(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V

    .line 176
    sget-boolean v1, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->sIsLoginSuccess:Z

    return v1
.end method


# virtual methods
.method public addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "requiredFeatures"    # [Ljava/lang/String;
    .param p5, "options"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 45
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->mContext:Landroid/content/Context;

    const-class v3, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 46
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "ACCOUNT_TYPE"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const-string v2, "AUTH_TYPE"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 48
    const-string v2, "IS_ADDING_ACCOUNT"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 49
    const-string v2, "accountAuthenticatorResponse"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 50
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "intent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 52
    return-object v0
.end method

.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 7
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "options"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 58
    if-eqz p3, :cond_0

    const-string v5, "password"

    invoke-virtual {p3, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 59
    const-string v5, "password"

    invoke-virtual {p3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "password":Ljava/lang/String;
    iget-object v5, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v5, v2}, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->confirmPassword(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 61
    .local v4, "verified":Z
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 62
    .local v3, "result":Landroid/os/Bundle;
    const-string v5, "booleanResult"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 74
    .end local v2    # "password":Ljava/lang/String;
    .end local v3    # "result":Landroid/os/Bundle;
    .end local v4    # "verified":Z
    :goto_0
    return-object v3

    .line 67
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->mContext:Landroid/content/Context;

    const-class v6, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "ACCOUNT_NAME"

    iget-object v6, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v5, "CONFIRM_CREDENTIALS"

    const/4 v6, 0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 70
    const-string v5, "accountAuthenticatorResponse"

    invoke-virtual {v1, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 72
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 73
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "intent"

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v3, v0

    .line 74
    goto :goto_0
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 39
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 8
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "options"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 84
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    invoke-virtual {v0, p2, p3}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->sAuthToken:Ljava/lang/String;

    .line 87
    sget-object v6, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->sAuthToken:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 88
    invoke-virtual {v0, p2}, Landroid/accounts/AccountManager;->getPassword(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    .line 90
    .local v3, "password":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 92
    const/4 v5, 0x0

    .line 93
    .local v5, "userInfo":Lcom/vectorwatch/android/models/UserModel;
    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->mContext:Landroid/content/Context;

    iget-object v7, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v6, v7, v3}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->prepareUserDataForLogin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/UserModel;

    move-result-object v5

    .line 95
    new-instance v6, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator$1;

    invoke-direct {v6, p0}, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator$1;-><init>(Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;)V

    invoke-static {v5, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->accountLogin(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V

    .line 110
    .end local v3    # "password":Ljava/lang/String;
    .end local v5    # "userInfo":Lcom/vectorwatch/android/models/UserModel;
    :cond_0
    sget-object v6, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->sAuthToken:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 111
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 112
    .local v4, "result":Landroid/os/Bundle;
    const-string v6, "authAccount"

    iget-object v7, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v6, "accountType"

    iget-object v7, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v6, "authtoken"

    sget-object v7, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->sAuthToken:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .end local v4    # "result":Landroid/os/Bundle;
    :goto_0
    return-object v4

    .line 119
    :cond_1
    new-instance v2, Landroid/content/Intent;

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->mContext:Landroid/content/Context;

    const-class v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 120
    .local v2, "intent":Landroid/content/Intent;
    const-string v6, "accountAuthenticatorResponse"

    invoke-virtual {v2, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 121
    const-string v6, "ACCOUNT_TYPE"

    iget-object v7, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const-string v6, "AUTH_TYPE"

    invoke-virtual {v2, v6, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 124
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v6, "intent"

    invoke-virtual {v1, v6, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v4, v1

    .line 125
    goto :goto_0
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "authTokenType"    # Ljava/lang/String;

    .prologue
    .line 132
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "features"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 152
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 153
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "booleanResult"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 154
    return-object v0
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .param p1, "response"    # Landroid/accounts/AccountAuthenticatorResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authTokenType"    # Ljava/lang/String;
    .param p4, "options"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 139
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/phone_account/Authenticator;->mContext:Landroid/content/Context;

    const-class v3, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "ACCOUNT_NAME"

    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v2, "AUTH_TYPE"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    const-string v2, "CONFIRM_CREDENTIALS"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 144
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 145
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "intent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 146
    return-object v0
.end method
