.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;
.super Landroid/os/AsyncTask;
.source "SyncUpdatesService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncDefaultsToCloudAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field pService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Service;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

.field updatedDefaultApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Landroid/app/Service;)V
    .locals 1
    .param p2, "service"    # Landroid/app/Service;

    .prologue
    .line 728
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 726
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    .line 729
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    .line 730
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/content/Intent;
    .locals 12
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 736
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    const-string v9, "AT_SyncDefaults"

    invoke-virtual {v8, v9}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 737
    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/Service;

    .line 738
    .local v7, "service":Landroid/app/Service;
    if-nez v7, :cond_0

    .line 739
    const/4 v6, 0x0

    .line 785
    :goto_0
    return-object v6

    .line 741
    :cond_0
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v8

    const-string v9, "Sync defaults to cloud"

    invoke-interface {v8, v9}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 746
    :try_start_0
    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v8}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 747
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v8, "com.vectorwatch.android"

    invoke-virtual {v1, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 749
    .local v2, "accounts":[Landroid/accounts/Account;
    array-length v8, v2

    if-ge v8, v10, :cond_2

    .line 750
    const-string v8, "Sync-Defaults:"

    const-string v9, "There should be at least one account in Accounts & sync"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 753
    .local v6, "res":Landroid/content/Intent;
    const-string v8, "ERR_MSG"

    const v9, 0x7f09010d

    invoke-virtual {v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 776
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v6    # "res":Landroid/content/Intent;
    :catch_0
    move-exception v5

    .line 777
    .local v5, "e":Landroid/accounts/OperationCanceledException;
    invoke-virtual {v5}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    .line 784
    .end local v5    # "e":Landroid/accounts/OperationCanceledException;
    :cond_1
    :goto_1
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 785
    .restart local v6    # "res":Landroid/content/Intent;
    goto :goto_0

    .line 758
    .end local v6    # "res":Landroid/content/Intent;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    :cond_2
    const/4 v8, 0x0

    :try_start_1
    aget-object v0, v2, v8

    .line 760
    .local v0, "account":Landroid/accounts/Account;
    const-string v8, "Full access"

    const/4 v9, 0x1

    invoke-virtual {v1, v0, v8, v9}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 763
    .local v4, "authToken":Ljava/lang/String;
    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v4, v8}, Lcom/vectorwatch/android/cloud_communication/CloudCommunicationHandler;->syncDefaultsToCloud(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v8

    iput-object v8, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    .line 765
    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    if-eqz v8, :cond_3

    .line 766
    iget-object v8, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/DownloadedAppDetails;

    .line 767
    .local v3, "app":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "SETUP: Returned app. "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v3, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    .line 778
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "app":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .end local v4    # "authToken":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 779
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 771
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountManager":Landroid/accounts/AccountManager;
    .restart local v2    # "accounts":[Landroid/accounts/Account;
    .restart local v4    # "authToken":Ljava/lang/String;
    :cond_3
    :try_start_2
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 772
    .restart local v6    # "res":Landroid/content/Intent;
    const-string v8, "ERR_MSG"

    const v9, 0x7f090115

    invoke-virtual {v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 780
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountManager":Landroid/accounts/AccountManager;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v4    # "authToken":Ljava/lang/String;
    .end local v6    # "res":Landroid/content/Intent;
    :catch_2
    move-exception v5

    .line 781
    .local v5, "e":Landroid/accounts/AuthenticatorException;
    invoke-virtual {v5}, Landroid/accounts/AuthenticatorException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 723
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->doInBackground([Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 795
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->pService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Service;

    .line 796
    .local v1, "service":Landroid/app/Service;
    if-eqz v1, :cond_4

    .line 797
    const-string v3, "ERR_MSG"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 798
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error at downloading system apps: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ERR_MSG"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 802
    sput-boolean v7, Lcom/vectorwatch/android/VectorApplication;->hasError:Z

    .line 803
    const-string v3, "ERR_MSG"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 804
    .local v0, "errorMessage":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v4, v8, v0}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 847
    .end local v0    # "errorMessage":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 807
    :cond_1
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 808
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v3

    const-string v4, "SETUP: Synced apps back from cloud. "

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 813
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-virtual {v4}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vectorwatch/android/managers/CloudAppsManager;->updateAppsDatabase(Ljava/util/List;Landroid/content/Context;)V

    .line 816
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_2

    .line 817
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 818
    .local v2, "tempElements":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DownloadedAppDetails;>;"
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 819
    iput-object v2, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    .line 823
    .end local v2    # "tempElements":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/DownloadedAppDetails;>;"
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->updatedDefaultApps:Ljava/util/List;

    sput-object v3, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    .line 825
    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    if-eqz v3, :cond_3

    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_3

    .line 826
    sget-object v3, Lcom/vectorwatch/android/VectorApplication;->sAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sput v3, Lcom/vectorwatch/android/VectorApplication;->sTotalInstalledApps:I

    .line 827
    sput v6, Lcom/vectorwatch/android/VectorApplication;->sCorrectlyInstalledApps:I

    .line 828
    sput v6, Lcom/vectorwatch/android/VectorApplication;->sInstalledCount:I

    .line 831
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/InstallAppEvent;

    const/4 v5, 0x0

    invoke-direct {v4, v6, v5}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 833
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/RefreshWatchmakerUi;

    invoke-direct {v4}, Lcom/vectorwatch/android/events/RefreshWatchmakerUi;-><init>()V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto :goto_0

    .line 835
    :cond_3
    sput-boolean v7, Lcom/vectorwatch/android/VectorApplication;->hasError:Z

    .line 836
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-virtual {v3}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090116

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 837
    .restart local v0    # "errorMessage":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v4, v8, v0}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 843
    .end local v0    # "errorMessage":Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-virtual {v3}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090101

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 844
    .restart local v0    # "errorMessage":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/InstallAppEvent;

    invoke-direct {v4, v8, v0}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 845
    sput-boolean v7, Lcom/vectorwatch/android/VectorApplication;->hasError:Z

    goto/16 :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 723
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$SyncDefaultsToCloudAsyncTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 791
    return-void
.end method
