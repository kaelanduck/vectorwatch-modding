.class final Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;
.super Ljava/lang/Object;
.source "SyncUpdatesService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->instantiateSupportedAppsList(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 952
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 955
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "app_not_all"

    const-class v5, Ljava/util/List;

    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 956
    .local v1, "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v1, :cond_3

    .line 957
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 959
    .restart local v1    # "notifList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;->val$context:Landroid/content/Context;

    invoke-static {v6, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSystemNotification(ZLandroid/content/Context;)V

    .line 960
    sget-object v3, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->USER_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;->val$context:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/vectorwatch/android/utils/AppSignatureRetriever;->getInstalledApps(Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    .line 961
    .local v2, "supportedAppsList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AppInfoModel;>;"
    sget-object v3, Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;->SYSTEM_APP:Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;

    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;->val$context:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/vectorwatch/android/utils/AppSignatureRetriever;->getInstalledApps(Lcom/vectorwatch/android/utils/AppSignatureRetriever$AppType;Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 962
    if-eqz v2, :cond_1

    .line 963
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 964
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/AppInfoModel;

    iget-object v3, v3, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 965
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/models/AppInfoModel;

    iget-object v3, v3, Lcom/vectorwatch/android/models/AppInfoModel;->packetName:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;->val$context:Landroid/content/Context;

    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 963
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 967
    :cond_0
    const-string v3, "pref_notif_first_time"

    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;->val$context:Landroid/content/Context;

    invoke-static {v3, v7, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 969
    .end local v0    # "i":I
    :cond_1
    const-string v3, "phone_call"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;->val$context:Landroid/content/Context;

    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setSupportedAppNotification(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 970
    const-string v3, "phone_call"

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 971
    const-string v3, "phone_call"

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 973
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "app_not_all"

    invoke-virtual {v3, v4, v1}, Lcom/vectorwatch/android/utils/ComplexPreferences;->putObject(Ljava/lang/String;Ljava/lang/Object;)V

    .line 975
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/vectorwatch/android/utils/Helpers;->triggerUserSettingsSyncToCloud(Landroid/content/Context;)V

    .line 979
    .end local v2    # "supportedAppsList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/AppInfoModel;>;"
    :goto_1
    return-void

    .line 977
    :cond_3
    const-string v3, "pref_notif_first_time"

    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$8;->val$context:Landroid/content/Context;

    invoke-static {v3, v7, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    goto :goto_1
.end method
