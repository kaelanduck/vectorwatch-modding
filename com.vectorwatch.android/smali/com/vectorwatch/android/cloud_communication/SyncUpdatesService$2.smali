.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$2;
.super Ljava/lang/Object;
.source "SyncUpdatesService.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->syncParseInstallationId(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/SyncResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$2;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    iput-object p2, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 3
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 176
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to update user account: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lretrofit/RetrofitError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/SyncResponse;Lretrofit/client/Response;)V
    .locals 3
    .param p1, "syncResponse"    # Lcom/vectorwatch/android/models/SyncResponse;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 170
    const-string v0, "flag_send_parse_installation_id"

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$2;->val$context:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 172
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 166
    check-cast p1, Lcom/vectorwatch/android/models/SyncResponse;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$2;->success(Lcom/vectorwatch/android/models/SyncResponse;Lretrofit/client/Response;)V

    return-void
.end method
