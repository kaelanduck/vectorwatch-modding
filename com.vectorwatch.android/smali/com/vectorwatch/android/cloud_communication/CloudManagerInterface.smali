.class public interface abstract Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
.super Ljava/lang/Object;
.source "CloudManagerInterface.java"


# virtual methods
.method public abstract addRating(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;Ljava/lang/String;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "type"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "versionInfo"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/store/rate/{type}/{uuid}"
    .end annotation
.end method

.method public abstract callAppCallback(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "version"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/AppCallbackProxyResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/app/proxy/{uuid}/{version}"
    .end annotation
.end method

.method public abstract changeStreamState(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "streamUUID"
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lretrofit/http/Path;
            value = "streamVersion"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "state"
        .end annotation
    .end param
    .param p4    # Lcom/vectorwatch/android/models/ChannelSettingsChange;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/ChannelSettingsChange;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/stream/change_stream_state/{streamUUID}/{streamVersion}/{state}/"
    .end annotation
.end method

.method public abstract checkCloudStatus(Lretrofit/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/details"
    .end annotation
.end method

.method public abstract confirmUpdateWithCpuId(Ljava/lang/Long;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lretrofit/http/Path;
            value = "osId"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/watch/confirm_update/{osId}/"
    .end annotation
.end method

.method public abstract createAccount(Lcom/vectorwatch/android/models/UserModel;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Lcom/vectorwatch/android/models/UserModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/account"
    .end annotation
.end method

.method public abstract createAccount(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V
    .param p1    # Lcom/vectorwatch/android/models/UserModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/UserModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/account"
    .end annotation
.end method

.method public abstract downloadStream(Ljava/lang/String;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "streamUUID"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/ChannelSettingsChange;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/ChannelSettingsChange;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/stream/change_stream_state/{streamUUID}/DOWNLOADED/"
    .end annotation
.end method

.method public abstract getActivity(Ljava/lang/String;JILretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "type"
        .end annotation
    .end param
    .param p2    # J
        .annotation runtime Lretrofit/http/Path;
            value = "tsEnd"
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lretrofit/http/Path;
            value = "tsTimezoneOffset"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "JI",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/activity/{type}/{tsEnd}/{tsTimezoneOffset}"
    .end annotation
.end method

.method public abstract getApp(Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/BaseCloudRequestModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppContainer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/app/change_app_state/{uuid}/RUNNING"
    .end annotation
.end method

.method public abstract getAppAuthMethod(Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/BaseCloudRequestModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/AuthMethodResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/app/{uuid}/authMethod"
    .end annotation
.end method

.method public abstract getAppDefaults(Lcom/vectorwatch/android/models/CloudRequestModel;Lretrofit/Callback;)V
    .param p1    # Lcom/vectorwatch/android/models/CloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/CloudRequestModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/app/apps/default"
    .end annotation
.end method

.method public abstract getAppSettingOptions(Ljava/lang/String;Lcom/vectorwatch/android/models/AppOptionsRequest;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/AppOptionsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/AppOptionsRequest;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/app/{uuid}/getOptions"
    .end annotation
.end method

.method public abstract getAppSettings(Ljava/lang/String;Lcom/vectorwatch/android/models/AuthCredentialsRequest;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/AuthCredentialsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/AuthCredentialsRequest;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/PossibleSettingsResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/app/{uuid}/getSettings"
    .end annotation
.end method

.method public abstract getContextualFromCloud(Lretrofit/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ResponseContextual;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/account/contextual"
    .end annotation
.end method

.method public abstract getDefaultStreams(Lcom/vectorwatch/android/models/CloudRequestModel;Lretrofit/Callback;)V
    .param p1    # Lcom/vectorwatch/android/models/CloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/CloudRequestModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/DefaultStreamsModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/stream/streams/default"
    .end annotation
.end method

.method public abstract getNewsletter(Lretrofit/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/account/newsletters"
    .end annotation
.end method

.method public abstract getRating(Ljava/lang/String;Ljava/lang/String;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "type"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/store/rate/{type}/{uuid}"
    .end annotation
.end method

.method public abstract getSettingsFromCloud(Lretrofit/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/RemoteSettingsModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/account/me"
    .end annotation
.end method

.method public abstract getShareLink(Ljava/lang/String;Lcom/vectorwatch/android/models/ShareValuesItem;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "type"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/ShareValuesItem;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/ShareValuesItem;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ShareServerResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/activity/share/{type}"
    .end annotation
.end method

.method public abstract getSoftwareUpdate(Ljava/lang/String;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "updateType"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/WatchOsDetailsModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/WatchOsDetailsModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/SoftwareUpdateModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/watch/get_update/{updateType}/"
    .end annotation
.end method

.method public abstract getStoreSlideshow(Lretrofit/Callback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/store/slideshow"
    .end annotation
.end method

.method public abstract getStreamAuthMethod(Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "streamUUID"
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lretrofit/http/Path;
            value = "streamVersion"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/vectorwatch/android/models/BaseCloudRequestModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/AuthMethodResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/stream/{streamUUID}/{streamVersion}/authMethod"
    .end annotation
.end method

.method public abstract getStreamSettingOptions(Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/StreamOptionsRequest;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "streamUUID"
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lretrofit/http/Path;
            value = "streamVersion"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/StreamOptionsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/vectorwatch/android/models/StreamOptionsRequest;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/stream/{streamUUID}/{streamVersion}/getOptions"
    .end annotation
.end method

.method public abstract getStreamSettings(Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "streamUUID"
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lretrofit/http/Path;
            value = "streamVersion"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/vectorwatch/android/models/BaseCloudRequestModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/PossibleSettingsResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/stream/{streamUUID}/{streamVersion}/getSettings"
    .end annotation
.end method

.method public abstract notifyAppStateChange(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "newAppState"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/BaseCloudRequestModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppContainer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/app/change_app_state/{uuid}/{newAppState}"
    .end annotation
.end method

.method public abstract putSettingsInCloud(Lcom/vectorwatch/android/models/LocalSettingsModel;Lretrofit/Callback;)V
    .param p1    # Lcom/vectorwatch/android/models/LocalSettingsModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "alarmSettings"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/LocalSettingsModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/PUT;
        value = "/account"
    .end annotation
.end method

.method public abstract queryStore(Ljava/lang/String;Ljava/util/HashMap;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "versionInfo"
        .end annotation
    .end param
    .param p2    # Ljava/util/HashMap;
        .annotation runtime Lretrofit/http/QueryMap;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StoreQuery;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/store"
    .end annotation
.end method

.method public abstract registerWatch(Ljava/lang/String;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "watchSerialNo"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/watch/register_UsingCpuID/{watchSerialNo}"
    .end annotation
.end method

.method public abstract setAuthCredetentialsForStream(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "storeElement"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "itemUUID"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;",
            "Lretrofit/Callback",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/{storeElement}/{itemUUID}/setAuth"
    .end annotation
.end method

.method public abstract setNewsletter(Ljava/util/List;Lretrofit/Callback;)V
    .param p1    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterItem;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/PUT;
        value = "/account/newsletters"
    .end annotation
.end method

.method public abstract syncActivities(Ljava/util/List;Lretrofit/Callback;)V
    .param p1    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/UserActivityDataModel;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/activity"
    .end annotation
.end method

.method public abstract syncActivitiesSleep(Ljava/util/List;Lretrofit/Callback;)V
    .param p1    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SleepCloud;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/activity/sleep"
    .end annotation
.end method

.method public abstract syncContextualInCloud(Ljava/util/List;Lretrofit/Callback;)V
    .param p1    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/ContextualItem;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/account/contextual"
    .end annotation
.end method

.method public abstract syncLogs(Ljava/util/List;Lretrofit/Callback;)V
    .param p1    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/WatchLogDataModel;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/logdata"
    .end annotation
.end method

.method public abstract syncOrderToCloud(Ljava/lang/String;Ljava/util/ArrayList;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "device"
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/account/sync/{device}"
    .end annotation
.end method

.method public abstract syncStreamLogExpired(Ljava/lang/String;Ljava/util/List;Lretrofit/Callback;)V
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "versionInfo"
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamLogItem;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/stream/expired"
    .end annotation
.end method

.method public abstract updateDefaults(Lcom/vectorwatch/android/models/UpdateDefaultsModel;Lretrofit/Callback;)V
    .param p1    # Lcom/vectorwatch/android/models/UpdateDefaultsModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/UpdateDefaultsModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/app/updateDefaults/"
    .end annotation
.end method

.method public abstract updateInfo(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V
    .param p1    # Lcom/vectorwatch/android/models/UserModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/UserModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/SyncResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/PUT;
        value = "/account/deviceInfo"
    .end annotation
.end method

.method public abstract uploadWatchFile(Lretrofit/mime/TypedFile;Ljava/lang/String;Lretrofit/Callback;)V
    .param p1    # Lretrofit/mime/TypedFile;
        .annotation runtime Lretrofit/http/Part;
            value = "file"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "versionInfo"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit/mime/TypedFile;",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Multipart;
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/upload/watchFile"
    .end annotation
.end method

.method public abstract userLogin(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V
    .param p1    # Lcom/vectorwatch/android/models/UserModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/UserModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/LoginResponseModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/account/login"
    .end annotation
.end method
