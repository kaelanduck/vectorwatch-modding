.class public Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;
.super Ljava/lang/Object;
.source "CloudManagerHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;
    }
.end annotation


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    const-class v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    .prologue
    .line 98
    invoke-static {p0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V

    return-void
.end method

.method static synthetic access$100()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    .prologue
    .line 98
    invoke-static {p0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getJSONFromSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 98
    invoke-static {p0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getShapeFilters(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static accountLogin(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V
    .locals 4
    .param p0, "userInfo"    # Lcom/vectorwatch/android/models/UserModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/UserModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/LoginResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1959
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/LoginResponseModel;>;"
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v3, "Account login."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1961
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1962
    .local v0, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/LoginResponseModel;>;"
    iput-object p1, v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1964
    new-instance v1, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$37;

    invoke-direct {v1, p0, v0, p1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$37;-><init>(Lcom/vectorwatch/android/models/UserModel;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1977
    .local v1, "t":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1978
    return-void
.end method

.method public static accountSignUp(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V
    .locals 4
    .param p0, "userInfo"    # Lcom/vectorwatch/android/models/UserModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/vectorwatch/android/models/UserModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1987
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v3, "Account sign up."

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1989
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1990
    .local v0, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p1, v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1992
    new-instance v1, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$38;

    invoke-direct {v1, p0, v0, p1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$38;-><init>(Lcom/vectorwatch/android/models/UserModel;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 2005
    .local v1, "t":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 2006
    return-void
.end method

.method public static addRating(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;Lretrofit/Callback;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "ratingInfo"    # Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 531
    .local p4, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: download_stream"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 533
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 534
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .line 536
    .local v8, "accounts":[Landroid/accounts/Account;
    new-instance v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 537
    .local v6, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemPostResponse;>;"
    iput-object p4, v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 538
    iput-object p0, v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 540
    array-length v3, v8

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 541
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 543
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 574
    :goto_0
    return-void

    .line 548
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v8, v3

    .line 549
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$9;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingCloudItem;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 573
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static checkCloudStatus(Lretrofit/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    .local p0, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$1;-><init>(Lretrofit/Callback;)V

    .line 142
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 143
    return-void
.end method

.method public static confirmUpdateDoneOnWatchWithCpuId(Landroid/content/Context;Ljava/lang/Long;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;Lretrofit/Callback;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "versionId"    # Ljava/lang/Long;
    .param p2, "type"    # Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;
    .param p3, "model"    # Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Long;",
            "Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;",
            "Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1594
    .local p4, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1595
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .line 1597
    .local v8, "accounts":[Landroid/accounts/Account;
    new-instance v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1598
    .local v6, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p4, v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1599
    iput-object p0, v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1601
    array-length v3, v8

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 1602
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1604
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1633
    :goto_0
    return-void

    .line 1609
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v8, v3

    .line 1610
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;

    move-object v3, p2

    move-object v4, p1

    move-object v5, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$29;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/com/android/vos/update/UpdateService$UpdateType;Ljava/lang/Long;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1632
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static downloadAppFromLoadedAppList(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/DownloadedAppContainer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 263
    .local p2, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/DownloadedAppContainer;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "download_app_from_loaded_app_list"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 265
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 266
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 268
    .local v7, "accounts":[Landroid/accounts/Account;
    new-instance v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 269
    .local v5, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/DownloadedAppContainer;>;"
    iput-object p2, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 270
    iput-object p0, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 272
    array-length v3, v7

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 273
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 275
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p2, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 317
    :goto_0
    return-void

    .line 280
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v7, v3

    .line 281
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;

    move-object v3, p0

    move-object v4, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$4;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 316
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static downloadContextualSettings(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ResponseContextual;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1679
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ResponseContextual;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1680
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v5, "com.vectorwatch.android"

    invoke-virtual {v1, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 1682
    .local v2, "accounts":[Landroid/accounts/Account;
    new-instance v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1683
    .local v3, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ResponseContextual;>;"
    iput-object p1, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1684
    iput-object p0, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1686
    array-length v5, v2

    const/4 v6, 0x1

    if-ge v5, v6, :cond_0

    .line 1687
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1689
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09010d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v6}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v5

    invoke-interface {p1, v5}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1718
    :goto_0
    return-void

    .line 1694
    :cond_0
    const/4 v5, 0x0

    aget-object v0, v2, v5

    .line 1695
    .local v0, "account":Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$31;

    invoke-direct {v4, v1, v0, v3, p1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$31;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1717
    .local v4, "t":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static downloadStream(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 326
    .local p2, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/StreamDownloadResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: download_stream"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 328
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 329
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 331
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 332
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/StreamDownloadResponse;>;"
    iput-object p2, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 333
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 335
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 336
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 338
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p2, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 368
    :goto_0
    return-void

    .line 343
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 344
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$5;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 367
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static downloadUserSettings(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/RemoteSettingsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1806
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/RemoteSettingsModel;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1807
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v5, "com.vectorwatch.android"

    invoke-virtual {v1, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 1809
    .local v2, "accounts":[Landroid/accounts/Account;
    new-instance v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1810
    .local v3, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/RemoteSettingsModel;>;"
    iput-object p1, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1811
    iput-object p0, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1813
    array-length v5, v2

    const/4 v6, 0x1

    if-ge v5, v6, :cond_0

    .line 1814
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "Download-settings: There should be at least one account in Accounts & sync"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1816
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09010d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v6}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v5

    invoke-interface {p1, v5}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1842
    :goto_0
    return-void

    .line 1821
    :cond_0
    const/4 v5, 0x0

    aget-object v0, v2, v5

    .line 1822
    .local v0, "account":Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$34;

    invoke-direct {v4, v1, v0, v3, p1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$34;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1841
    .local v4, "t":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getActivityFromCloud(Landroid/content/Context;Ljava/lang/String;JILretrofit/Callback;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "tsEnd"    # J
    .param p4, "tzOffset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "JI",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 580
    .local p5, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: get_activity_from_cloud"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 582
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v9

    .line 583
    .local v9, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v9, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v10

    .line 585
    .local v10, "accounts":[Landroid/accounts/Account;
    new-instance v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 586
    .local v7, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/ui/chart/model/CloudActivityResponse;>;"
    move-object/from16 v0, p5

    iput-object v0, v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 587
    iput-object p0, v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 589
    array-length v3, v10

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 590
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 592
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-interface {v0, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 614
    :goto_0
    return-void

    .line 597
    :cond_0
    const/4 v3, 0x0

    aget-object v8, v10, v3

    .line 599
    .local v8, "account":Landroid/accounts/Account;
    :try_start_0
    const-string v3, "Full access"

    const/4 v4, 0x1

    invoke-virtual {v9, v8, v3, v4}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    .line 601
    .local v11, "authToken":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-static {v11, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v2

    .local v2, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    move-object v3, p1

    move-wide/from16 v4, p2

    move/from16 v6, p4

    .line 603
    invoke-interface/range {v2 .. v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;->getActivity(Ljava/lang/String;JILretrofit/Callback;)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 604
    .end local v2    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v11    # "authToken":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 605
    .local v12, "e":Landroid/accounts/AuthenticatorException;
    const-string v3, "Account"

    invoke-static {v3, v12}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v13

    .line 606
    .local v13, "error":Lretrofit/RetrofitError;
    move-object/from16 v0, p5

    invoke-interface {v0, v13}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 607
    .end local v12    # "e":Landroid/accounts/AuthenticatorException;
    .end local v13    # "error":Lretrofit/RetrofitError;
    :catch_1
    move-exception v12

    .line 608
    .local v12, "e":Landroid/accounts/OperationCanceledException;
    const-string v3, "Canceled"

    invoke-static {v3, v12}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v13

    .line 609
    .restart local v13    # "error":Lretrofit/RetrofitError;
    move-object/from16 v0, p5

    invoke-interface {v0, v13}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    .line 610
    .end local v12    # "e":Landroid/accounts/OperationCanceledException;
    .end local v13    # "error":Lretrofit/RetrofitError;
    :catch_2
    move-exception v12

    .line 611
    .local v12, "e":Ljava/lang/Exception;
    const-string v3, "Unexpected"

    invoke-static {v3, v12}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v13

    .line 612
    .restart local v13    # "error":Lretrofit/RetrofitError;
    move-object/from16 v0, p5

    invoke-interface {v0, v13}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0
.end method

.method public static getAppAdditionalData(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;Lretrofit/Callback;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "version"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/AppCallbackProxyResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p4, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/AppCallbackProxyResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: download_app_from_loaded_app_list"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 157
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 158
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .line 160
    .local v8, "accounts":[Landroid/accounts/Account;
    new-instance v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 161
    .local v6, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/AppCallbackProxyResponse;>;"
    iput-object p4, v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 162
    iput-object p0, v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 164
    array-length v3, v8

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 165
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 196
    :goto_0
    return-void

    .line 172
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v8, v3

    .line 173
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$2;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$2;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 195
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getAppAuthMethod(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/AuthMethodResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 623
    .local p2, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/AuthMethodResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: getAppAuthMethod"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 624
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 625
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 627
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 628
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/AuthMethodResponse;>;"
    iput-object p2, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 629
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 631
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 632
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 634
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p2, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 663
    :goto_0
    return-void

    .line 639
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 640
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$10;

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$10;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 662
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getAppSettingOptions(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Lretrofit/Callback;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "settingName"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 821
    .local p4, "authCredentials":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p5, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    .local p6, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;>;"
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: getStreamSettings"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 824
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 825
    .local v2, "accountManager":Landroid/accounts/AccountManager;
    const-string v4, "com.vectorwatch.android"

    invoke-virtual {v2, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v10

    .line 827
    .local v10, "accounts":[Landroid/accounts/Account;
    new-instance v8, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v8}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 828
    .local v8, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;>;"
    move-object/from16 v0, p6

    iput-object v0, v8, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 829
    iput-object p0, v8, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 831
    array-length v4, v10

    const/4 v5, 0x1

    if-ge v4, v5, :cond_0

    .line 832
    sget-object v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 834
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09010d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v4}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 864
    :goto_0
    return-void

    .line 839
    :cond_0
    const/4 v4, 0x0

    aget-object v3, v10, v4

    .line 840
    .local v3, "account":Landroid/accounts/Account;
    new-instance v1, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$14;

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v6, p5

    move-object v7, p1

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$14;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 863
    .local v1, "t":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getAppSettings(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/PossibleSettingsResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 719
    .local p2, "authCredentials":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p3, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/PossibleSettingsResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: getAppSettings"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 720
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 721
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 723
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 724
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/PossibleSettingsResponse;>;"
    iput-object p3, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 725
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 727
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 728
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 730
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p3, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 757
    :goto_0
    return-void

    .line 735
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 736
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$12;

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$12;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 756
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getDefaultApps(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1155
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: get_list_of_defaults_from_store"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1156
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1157
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 1159
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1160
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;>;"
    iput-object p1, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1161
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1163
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 1164
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1166
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p1, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1207
    :goto_0
    return-void

    .line 1171
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 1172
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;

    move-object v3, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$20;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1206
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getDefaultsStreams(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/DefaultStreamsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1216
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/DefaultStreamsModel;>;"
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "CLOUD CALLS: get_defaults_streams"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1217
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1218
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v5, "com.vectorwatch.android"

    invoke-virtual {v1, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 1220
    .local v2, "accounts":[Landroid/accounts/Account;
    new-instance v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1221
    .local v3, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/DefaultStreamsModel;>;"
    iput-object p1, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1222
    iput-object p0, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1224
    array-length v5, v2

    const/4 v6, 0x1

    if-ge v5, v6, :cond_0

    .line 1225
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1227
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09010d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v6}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v5

    invoke-interface {p1, v5}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1259
    :goto_0
    return-void

    .line 1232
    :cond_0
    const/4 v5, 0x0

    aget-object v0, v2, v5

    .line 1233
    .local v0, "account":Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$21;

    invoke-direct {v4, v1, v0, v3, p1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$21;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1258
    .local v4, "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private static getJSONFromSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Ljava/lang/String;
    .locals 5
    .param p0, "model"    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    .prologue
    .line 2071
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 2073
    .local v1, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "deviceType"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2074
    const-string v2, "appVersion"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getAppVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2075
    const-string v2, "kernelVersion"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getKernelVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2076
    const-string v2, "endpointArn"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getEndpointArn()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2077
    const-string v2, "deviceCompatibility"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getDeviceCompatibility()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2078
    const-string v2, "cpuId"

    invoke-virtual {p0}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->getCpuId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2080
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2083
    :goto_0
    return-object v2

    .line 2081
    :catch_0
    move-exception v0

    .line 2082
    .local v0, "exception":Lorg/json/JSONException;
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CloudManagerHandler: Exception while formatting the JSON "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 2083
    const-string v2, ""

    goto :goto_0
.end method

.method public static getNewsletterSettings(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 428
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/NewsletterResponse;>;"
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "CLOUD CALLS: set_newsletter_settings"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 430
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 431
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v5, "com.vectorwatch.android"

    invoke-virtual {v1, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 433
    .local v2, "accounts":[Landroid/accounts/Account;
    new-instance v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 434
    .local v3, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/NewsletterResponse;>;"
    iput-object p1, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 435
    iput-object p0, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 437
    array-length v5, v2

    const/4 v6, 0x1

    if-ge v5, v6, :cond_0

    .line 438
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 440
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09010d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v6}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v5

    invoke-interface {p1, v5}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 468
    :goto_0
    return-void

    .line 445
    :cond_0
    const/4 v5, 0x0

    aget-object v0, v2, v5

    .line 446
    .local v0, "account":Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$7;

    invoke-direct {v4, v1, v0, v3, p1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$7;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 467
    .local v4, "t":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getRating(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lretrofit/Callback;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 379
    .local p3, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: download_stream"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 381
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 382
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 384
    .local v7, "accounts":[Landroid/accounts/Account;
    new-instance v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 385
    .local v5, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RatingItemGetResponse;>;"
    iput-object p3, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 386
    iput-object p0, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 388
    array-length v3, v7

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 389
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 391
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p3, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 419
    :goto_0
    return-void

    .line 396
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v7, v3

    .line 397
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$6;

    move-object v3, p2

    move-object v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$6;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 418
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private static getShapeFilters(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p0, "shape"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Filter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2260
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: get_shape_filters"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2261
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2262
    .local v2, "shapesSupported":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "round"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2263
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2267
    :goto_0
    new-instance v1, Lcom/vectorwatch/android/models/Filter;

    const-string v3, "deviceCompatibility"

    invoke-direct {v1, v3, v2}, Lcom/vectorwatch/android/models/Filter;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 2268
    .local v1, "shapeFilter":Lcom/vectorwatch/android/models/Filter;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2269
    .local v0, "andFilterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/Filter;>;"
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2271
    return-object v0

    .line 2265
    .end local v0    # "andFilterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/Filter;>;"
    .end local v1    # "shapeFilter":Lcom/vectorwatch/android/models/Filter;
    :cond_0
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getShareLink(Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/models/ShareValuesItem;Lretrofit/Callback;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "shareValuesItem"    # Lcom/vectorwatch/android/models/ShareValuesItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/ShareValuesItem;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ShareServerResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 876
    .local p3, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ShareServerResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: getShareLink"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 879
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 880
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 882
    .local v7, "accounts":[Landroid/accounts/Account;
    new-instance v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 883
    .local v5, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ShareServerResponse;>;"
    iput-object p3, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 884
    iput-object p0, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 886
    array-length v3, v7

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 887
    const-string v3, "Sync-Defaults:"

    const-string v4, "There should be at least one account in Accounts & sync"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    const-string v3, "No account"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p3, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 916
    :goto_0
    return-void

    .line 893
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v7, v3

    .line 894
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$15;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Lcom/vectorwatch/android/models/ShareValuesItem;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 915
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getStoreSlideshow(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1911
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;>;"
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "Get store slideshow"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1913
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1914
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v5, "com.vectorwatch.android"

    invoke-virtual {v1, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 1916
    .local v2, "accounts":[Landroid/accounts/Account;
    new-instance v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1917
    .local v3, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/ui/tabmenufragments/store/cloud/model/RecommendQuery;>;"
    iput-object p1, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1918
    iput-object p0, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1920
    array-length v5, v2

    const/4 v6, 0x1

    if-ge v5, v6, :cond_0

    .line 1921
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "Download-settings: There should be at least one account in Accounts & sync"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1923
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09010d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v6}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v5

    invoke-interface {p1, v5}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1950
    :goto_0
    return-void

    .line 1928
    :cond_0
    const/4 v5, 0x0

    aget-object v0, v2, v5

    .line 1929
    .local v0, "account":Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$36;

    invoke-direct {v4, v1, v0, v3, p1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$36;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1949
    .local v4, "t":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getStreamAuthMethod(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/AuthMethodResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 672
    .local p2, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/AuthMethodResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: getStreamAuthMethod"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 673
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 674
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 676
    .local v7, "accounts":[Landroid/accounts/Account;
    new-instance v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 677
    .local v5, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/AuthMethodResponse;>;"
    iput-object p2, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 678
    iput-object p0, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 680
    array-length v3, v7

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 681
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 683
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p2, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 715
    :goto_0
    return-void

    .line 688
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v7, v3

    .line 689
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;

    move-object v3, p1

    move-object v4, p0

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$11;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 714
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getStreamSettings(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vectorwatch/android/models/Stream;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/PossibleSettingsResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 767
    .local p2, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/PossibleSettingsResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: getStreamSettings"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 770
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 771
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 773
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 774
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/PossibleSettingsResponse;>;"
    iput-object p2, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 775
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 777
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 778
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 780
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p2, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 809
    :goto_0
    return-void

    .line 785
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 786
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$13;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 808
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static getStreamSettingsOptions(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lretrofit/Callback;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "currentValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vectorwatch/android/models/Stream;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/Setting;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1264
    .local p4, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    .local p5, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1265
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    .line 1267
    .local v9, "accounts":[Landroid/accounts/Account;
    new-instance v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1268
    .local v7, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;>;"
    iput-object p5, v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1269
    iput-object p0, v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1271
    array-length v3, v9

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 1272
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1274
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p5, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1305
    :goto_0
    return-void

    .line 1279
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v9, v3

    .line 1280
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$22;

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p1

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$22;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1304
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private static populateSystemData(Lcom/vectorwatch/android/models/BaseCloudRequestModel;)V
    .locals 6
    .param p0, "model"    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;

    .prologue
    .line 2093
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 2096
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v3

    const-string v4, "last_known_system_info"

    const-class v5, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 2097
    invoke-virtual {v3, v4, v5}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 2099
    .local v1, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-eqz v1, :cond_0

    .line 2100
    const-string v3, "kernel"

    invoke-static {v1, v3}, Lcom/vectorwatch/android/utils/Helpers;->getSystemInfoAsString(Lcom/vectorwatch/com/android/vos/update/SystemInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setKernelVersion(Ljava/lang/String;)V

    .line 2103
    :cond_0
    const-string v3, "android"

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setDeviceType(Ljava/lang/String;)V

    .line 2104
    const-string v3, "2.0.2"

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setAppVersion(Ljava/lang/String;)V

    .line 2105
    invoke-static {v0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->getEndpointArn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setEndpointArn(Ljava/lang/String;)V

    .line 2107
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 2109
    .local v2, "shape":Ljava/lang/String;
    if-nez v2, :cond_2

    .line 2110
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "populateSystemData: Shape is null!"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 2117
    :goto_0
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2118
    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchNewCpuId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setCpuId(Ljava/lang/String;)V

    .line 2120
    :cond_1
    return-void

    .line 2111
    :cond_2
    const-string v3, "round"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2112
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->ROUND_240:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setDeviceCompatibility(Ljava/lang/String;)V

    goto :goto_0

    .line 2114
    :cond_3
    sget-object v3, Lcom/vectorwatch/android/models/WatchType;->SQUARE_144:Lcom/vectorwatch/android/models/WatchType;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/WatchType;->asString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/models/BaseCloudRequestModel;->setDeviceCompatibility(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static queryStore(Landroid/content/Context;Lcom/vectorwatch/android/models/cloud/AppsOption;ILjava/lang/String;Lretrofit/Callback;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appOption"    # Lcom/vectorwatch/android/models/cloud/AppsOption;
    .param p2, "from"    # I
    .param p3, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vectorwatch/android/models/cloud/AppsOption;",
            "I",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StoreQuery;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1847
    .local p4, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/StoreQuery;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1848
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    .line 1850
    .local v9, "accounts":[Landroid/accounts/Account;
    new-instance v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1851
    .local v7, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/StoreQuery;>;"
    iput-object p4, v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1852
    iput-object p0, v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1854
    array-length v3, v9

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 1855
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Download-settings: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1857
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1907
    :goto_0
    return-void

    .line 1862
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v9, v3

    .line 1863
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;

    move v3, p2

    move-object v4, p1

    move-object v5, p0

    move-object v6, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$35;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;ILcom/vectorwatch/android/models/cloud/AppsOption;Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1906
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static retrieveStoreInterface(I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .locals 5
    .param p0, "version"    # I

    .prologue
    .line 2132
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: retrieve_store_interface"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2134
    new-instance v3, Lretrofit/RestAdapter$Builder;

    invoke-direct {v3}, Lretrofit/RestAdapter$Builder;-><init>()V

    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$40;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$40;-><init>()V

    invoke-virtual {v3, v4}, Lretrofit/RestAdapter$Builder;->setRequestInterceptor(Lretrofit/RequestInterceptor;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 2143
    .local v0, "builder":Lretrofit/RestAdapter$Builder;
    packed-switch p0, :pswitch_data_0

    .line 2151
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2155
    .local v2, "restAdapter":Lretrofit/RestAdapter;
    :goto_0
    const-class v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    invoke-virtual {v2, v3}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    .line 2157
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    return-object v1

    .line 2145
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_0
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2146
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 2148
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_1
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV2Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2149
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 2143
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static retrieveStoreInterface(Ljava/lang/String;)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .locals 1
    .param p0, "authToken"    # Ljava/lang/String;

    .prologue
    .line 2123
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    move-result-object v0

    return-object v0
.end method

.method public static retrieveStoreInterface(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .locals 5
    .param p0, "authToken"    # Ljava/lang/String;
    .param p1, "version"    # I

    .prologue
    .line 2167
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "[CLOUD CALLS]: retrieve_store_interface"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2169
    new-instance v3, Lretrofit/RestAdapter$Builder;

    invoke-direct {v3}, Lretrofit/RestAdapter$Builder;-><init>()V

    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$41;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$41;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lretrofit/RestAdapter$Builder;->setRequestInterceptor(Lretrofit/RequestInterceptor;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/cloud_communication/syncadapter/CloudErrorHandler;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/syncadapter/CloudErrorHandler;-><init>()V

    .line 2183
    invoke-virtual {v3, v4}, Lretrofit/RestAdapter$Builder;->setErrorHandler(Lretrofit/ErrorHandler;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 2186
    .local v0, "builder":Lretrofit/RestAdapter$Builder;
    packed-switch p1, :pswitch_data_0

    .line 2197
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2203
    .local v2, "restAdapter":Lretrofit/RestAdapter;
    :goto_0
    const-class v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    invoke-virtual {v2, v3}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    .line 2205
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    return-object v1

    .line 2188
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_0
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2189
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 2191
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_1
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2192
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 2194
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_2
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV2Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2195
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 2186
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static retrieveStoreInterfaceForMultipart(Ljava/lang/String;I)Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .locals 5
    .param p0, "authToken"    # Ljava/lang/String;
    .param p1, "version"    # I

    .prologue
    .line 2215
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: retrieve_store_interface"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2217
    new-instance v3, Lretrofit/RestAdapter$Builder;

    invoke-direct {v3}, Lretrofit/RestAdapter$Builder;-><init>()V

    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$42;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$42;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lretrofit/RestAdapter$Builder;->setRequestInterceptor(Lretrofit/RequestInterceptor;)Lretrofit/RestAdapter$Builder;

    move-result-object v0

    .line 2231
    .local v0, "builder":Lretrofit/RestAdapter$Builder;
    packed-switch p1, :pswitch_data_0

    .line 2242
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2248
    .local v2, "restAdapter":Lretrofit/RestAdapter;
    :goto_0
    const-class v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    invoke-virtual {v2, v3}, Lretrofit/RestAdapter;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;

    .line 2250
    .local v1, "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    return-object v1

    .line 2233
    .end local v1    # "cloudService":Lcom/vectorwatch/android/cloud_communication/CloudManagerInterface;
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_0
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2234
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 2236
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_1
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV1Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2237
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 2239
    .end local v2    # "restAdapter":Lretrofit/RestAdapter;
    :pswitch_2
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getApiV2Uri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lretrofit/RestAdapter$Builder;->setEndpoint(Ljava/lang/String;)Lretrofit/RestAdapter$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lretrofit/RestAdapter$Builder;->build()Lretrofit/RestAdapter;

    move-result-object v2

    .line 2240
    .restart local v2    # "restAdapter":Lretrofit/RestAdapter;
    goto :goto_0

    .line 2231
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static retrieveUpdateDetails(Landroid/content/Context;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Ljava/lang/String;Lretrofit/Callback;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "watchOsDetailsModel"    # Lcom/vectorwatch/android/models/WatchOsDetailsModel;
    .param p2, "updateType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vectorwatch/android/models/WatchOsDetailsModel;",
            "Ljava/lang/String;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/SoftwareUpdateModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1637
    .local p3, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/SoftwareUpdateModel;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "COMPATIBILITY - Started retrieval of system update details for compatibility"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1638
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1639
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 1641
    .local v7, "accounts":[Landroid/accounts/Account;
    new-instance v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1642
    .local v5, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/SoftwareUpdateModel;>;"
    iput-object p3, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1643
    iput-object p0, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1645
    array-length v3, v7

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 1646
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1648
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p3, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1676
    :goto_0
    return-void

    .line 1653
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v7, v3

    .line 1654
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$30;

    move-object v3, p2

    move-object v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$30;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Lcom/vectorwatch/android/models/WatchOsDetailsModel;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1675
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static setAuthCredentials(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;Lretrofit/Callback;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "element"    # Ljava/lang/String;
    .param p2, "uuid"    # Ljava/lang/String;
    .param p3, "credentials"    # Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;",
            "Lretrofit/Callback",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1318
    .local p4, "callback":Lretrofit/Callback;, "Lretrofit/Callback<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 1319
    .local v2, "accountManager":Landroid/accounts/AccountManager;
    const-string v1, "com.vectorwatch.android"

    invoke-virtual {v2, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v8

    .line 1321
    .local v8, "accounts":[Landroid/accounts/Account;
    new-instance v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1322
    .local v6, "cb":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    iput-object p4, v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1323
    iput-object p0, v6, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1325
    array-length v1, v8

    const/4 v4, 0x1

    if-ge v1, v4, :cond_0

    .line 1326
    sget-object v1, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v1, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1328
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f09010d

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v1

    invoke-interface {p4, v1}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1361
    :goto_0
    return-void

    .line 1333
    :cond_0
    const/4 v1, 0x0

    aget-object v3, v8, v1

    .line 1334
    .local v3, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;

    move-object v1, p3

    move-object v4, p1

    move-object v5, p2

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$23;-><init>(Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1360
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static setNewsletterSettings(Landroid/content/Context;Ljava/util/List;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterItem;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/NewsletterResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 479
    .local p1, "newsletterItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/NewsletterItem;>;"
    .local p2, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/NewsletterResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: set_newsletter_settings"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 481
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 482
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 484
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 485
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/NewsletterResponse;>;"
    iput-object p2, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 486
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 488
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 489
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 491
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p2, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 519
    :goto_0
    return-void

    .line 496
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 497
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$8;

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$8;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/util/List;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 518
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static subscribeStream(Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPlacementModel;Lcom/vectorwatch/android/models/StreamChannelSettings;Lretrofit/Callback;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "subscriptionModel"    # Lcom/vectorwatch/android/models/StreamPlacementModel;
    .param p3, "channelSettings"    # Lcom/vectorwatch/android/models/StreamChannelSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/StreamPlacementModel;",
            "Lcom/vectorwatch/android/models/StreamChannelSettings;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 976
    .local p4, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/StreamDownloadResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: subscribe_stream"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 978
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 979
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    .line 981
    .local v9, "accounts":[Landroid/accounts/Account;
    new-instance v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 982
    .local v7, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/StreamDownloadResponse;>;"
    iput-object p4, v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 983
    iput-object p0, v7, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 985
    array-length v3, v9

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 986
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 988
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p4, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1038
    :goto_0
    return-void

    .line 992
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v9, v3

    .line 993
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;

    move-object v3, p2

    move-object v4, p3

    move-object v5, p1

    move-object v6, p0

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$17;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/models/StreamPlacementModel;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1037
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static syncActivity(Landroid/content/Context;Landroid/accounts/Account;Ljava/util/List;Lretrofit/Callback;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/UserActivityDataModel;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1464
    .local p2, "listActivity":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/UserActivityDataModel;>;"
    .local p3, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Activity to sync: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1466
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1467
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p3, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1468
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1470
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1471
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$26;

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$26;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/util/List;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1495
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1496
    return-void
.end method

.method public static syncDefaultsToCloud(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1367
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1368
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v5, "com.vectorwatch.android"

    invoke-virtual {v1, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 1370
    .local v2, "accounts":[Landroid/accounts/Account;
    new-instance v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1371
    .local v3, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;>;"
    iput-object p1, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1372
    iput-object p0, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1374
    array-length v5, v2

    const/4 v6, 0x1

    if-ge v5, v6, :cond_0

    .line 1375
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1377
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09010d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v6}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v5

    invoke-interface {p1, v5}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1411
    :goto_0
    return-void

    .line 1382
    :cond_0
    const/4 v5, 0x0

    aget-object v0, v2, v5

    .line 1383
    .local v0, "account":Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;

    invoke-direct {v4, v1, v0, p0, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$24;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;)V

    .line 1410
    .local v4, "t":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static syncLogs(Landroid/content/Context;Landroid/accounts/Account;Ljava/util/List;Lretrofit/Callback;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/WatchLogDataModel;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1506
    .local p2, "listLogs":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/WatchLogDataModel;>;"
    .local p3, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Activity to sync: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1508
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1509
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p3, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1510
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1512
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1513
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$27;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/util/List;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1537
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1538
    return-void
.end method

.method public static syncOrder(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 200
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD_CALLS: sync_order"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 202
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 203
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 205
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 206
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p1, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 207
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 210
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 211
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Order: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p1, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 251
    :goto_0
    return-void

    .line 218
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 219
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;

    move-object v3, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$3;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 250
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static syncSleep(Landroid/content/Context;Landroid/accounts/Account;Ljava/util/List;Lretrofit/Callback;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/SleepCloud;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1422
    .local p2, "listSleep":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/SleepCloud;>;"
    .local p3, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    sget-object v2, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sleep to sync: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1424
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1425
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p3, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1426
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1428
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1429
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$25;

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$25;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/util/List;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1453
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1454
    return-void
.end method

.method public static syncStreamTtlExpiredToCloud(Landroid/content/Context;Ljava/util/List;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamLogItem;",
            ">;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1548
    .local p1, "streamLogItemList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamLogItem;>;"
    .local p2, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1549
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v5, "com.vectorwatch.android"

    invoke-virtual {v1, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 1551
    .local v2, "accounts":[Landroid/accounts/Account;
    new-instance v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1552
    .local v3, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p2, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1553
    iput-object p0, v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1555
    array-length v5, v2

    const/4 v6, 0x1

    if-ge v5, v6, :cond_0

    .line 1556
    sget-object v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v6, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1558
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09010d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v5, v6}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v5

    invoke-interface {p2, v5}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1587
    :goto_0
    return-void

    .line 1563
    :cond_0
    const/4 v5, 0x0

    aget-object v0, v2, v5

    .line 1564
    .local v0, "account":Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;

    invoke-direct {v4, v1, v0, p1, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$28;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/util/List;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;)V

    .line 1586
    .local v4, "t":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static uninstallStream(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vectorwatch/android/models/Stream;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1106
    .local p2, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/StreamDownloadResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CLOUD CALLS: uninstall_stream"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1108
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1109
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 1111
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1112
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/StreamDownloadResponse;>;"
    iput-object p2, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1113
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1115
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 1116
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1118
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p2, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1152
    :goto_0
    return-void

    .line 1122
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 1123
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$19;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1151
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static unsubscribeChannel(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "channelSettingsChange"    # Lcom/vectorwatch/android/models/ChannelSettingsChange;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/vectorwatch/android/models/Stream;",
            "Lcom/vectorwatch/android/models/ChannelSettingsChange;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/StreamDownloadResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1051
    .local p3, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/StreamDownloadResponse;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "CLOUD CALLS: unsubscribe_channel"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1052
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1053
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    .line 1055
    .local v7, "accounts":[Landroid/accounts/Account;
    new-instance v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1056
    .local v5, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/StreamDownloadResponse;>;"
    iput-object p3, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1057
    iput-object p0, v5, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1059
    array-length v3, v7

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    .line 1060
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v4, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v4}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1062
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v4}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p3, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1096
    :goto_0
    return-void

    .line 1066
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v7, v3

    .line 1067
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$18;

    move-object v3, p2

    move-object v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$18;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1095
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static updateAccountInfo(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/SyncResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 922
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/SyncResponse;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 923
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 925
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 926
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/SyncResponse;>;"
    iput-object p1, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 927
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 929
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 930
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Sync-Defaults: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 932
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p1, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 962
    :goto_0
    return-void

    .line 936
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 937
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;

    move-object v3, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$16;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 961
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static uploadUserContextualSettings(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1721
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1722
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 1724
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1725
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p1, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1726
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1728
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 1729
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Upload-contextual: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1731
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p1, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1761
    :goto_0
    return-void

    .line 1736
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 1737
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;

    move-object v3, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$32;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1760
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static uploadUserSettings(Landroid/content/Context;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1764
    .local p1, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 1765
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 1767
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 1768
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p1, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 1769
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 1771
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 1772
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "Upload-contextual: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1774
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p1, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 1803
    :goto_0
    return-void

    .line 1779
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 1780
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;

    move-object v3, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$33;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Landroid/content/Context;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 1802
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static uploadWatchFile(Landroid/content/Context;Ljava/io/File;Lretrofit/Callback;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/io/File;",
            "Lretrofit/Callback",
            "<",
            "Lcom/vectorwatch/android/models/ServerResponseModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2016
    .local p2, "cb":Lretrofit/Callback;, "Lretrofit/Callback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CRASH LOGS: Send watch logs."

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2018
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 2019
    .local v1, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 2021
    .local v6, "accounts":[Landroid/accounts/Account;
    new-instance v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;

    invoke-direct {v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;-><init>()V

    .line 2022
    .local v4, "callback":Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;, "Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback<Lcom/vectorwatch/android/models/ServerResponseModel;>;"
    iput-object p2, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->callback:Lretrofit/Callback;

    .line 2023
    iput-object p0, v4, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;->context:Landroid/content/Context;

    .line 2025
    array-length v3, v6

    const/4 v5, 0x1

    if-ge v3, v5, :cond_0

    .line 2026
    sget-object v3, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->log:Lorg/slf4j/Logger;

    const-string v5, "CRASH LOGS: There should be at least one account in Accounts & sync"

    invoke-interface {v3, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2028
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f09010d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/Throwable;

    invoke-direct {v5}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v5}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object v3

    invoke-interface {p2, v3}, Lretrofit/Callback;->failure(Lretrofit/RetrofitError;)V

    .line 2060
    :goto_0
    return-void

    .line 2033
    :cond_0
    const/4 v3, 0x0

    aget-object v2, v6, v3

    .line 2034
    .local v2, "account":Landroid/accounts/Account;
    new-instance v0, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$39;-><init>(Landroid/accounts/AccountManager;Landroid/accounts/Account;Ljava/io/File;Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler$GenericCloudCallback;Lretrofit/Callback;)V

    .line 2059
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
