.class Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;
.super Ljava/lang/Object;
.source "SyncUpdatesService.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->handleSyncDefaults(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/DefaultStreamsModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .prologue
    .line 454
    iput-object p1, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 1
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 509
    iget-object v0, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    # invokes: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->handleRetrieveStreamDefaultsFailure(Lretrofit/RetrofitError;)V
    invoke-static {v0, p1}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$300(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;Lretrofit/RetrofitError;)V

    .line 510
    return-void
.end method

.method public success(Lcom/vectorwatch/android/models/DefaultStreamsModel;Lretrofit/client/Response;)V
    .locals 8
    .param p1, "defaultStreamsModel"    # Lcom/vectorwatch/android/models/DefaultStreamsModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 457
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/DefaultStreamsModel;->getData()Lcom/vectorwatch/android/models/DefaultStreamsModel$DownloadedDefaultStreamsList;

    move-result-object v3

    if-nez v3, :cond_1

    .line 458
    :cond_0
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    new-instance v4, Lcom/vectorwatch/android/events/InstallAppEvent;

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    .line 459
    invoke-virtual {v6}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900ff

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/vectorwatch/android/events/InstallAppEvent;-><init>(ILjava/lang/String;)V

    .line 458
    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 505
    :goto_0
    return-void

    .line 463
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/DefaultStreamsModel;->getData()Lcom/vectorwatch/android/models/DefaultStreamsModel$DownloadedDefaultStreamsList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/DefaultStreamsModel$DownloadedDefaultStreamsList;->getStreams()Ljava/util/List;

    move-result-object v0

    .line 465
    .local v0, "defaultStreams":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/Stream;

    .line 466
    .local v2, "stream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v2, :cond_2

    .line 467
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 469
    .local v1, "gson":Lcom/google/gson/Gson;
    # getter for: Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;->access$000()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DEFAULT STREAM - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 472
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    if-eqz v4, :cond_2

    .line 473
    iget-object v4, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    invoke-static {v2, v4}, Lcom/vectorwatch/android/managers/StreamsManager;->saveStreamToDatabase(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;)Z

    goto :goto_1

    .line 479
    .end local v1    # "gson":Lcom/google/gson/Gson;
    .end local v2    # "stream":Lcom/vectorwatch/android/models/Stream;
    :cond_3
    iget-object v3, p0, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->this$0:Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService;

    new-instance v4, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5$1;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5$1;-><init>(Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;)V

    invoke-static {v3, v4}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getDefaultApps(Landroid/content/Context;Lretrofit/Callback;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 454
    check-cast p1, Lcom/vectorwatch/android/models/DefaultStreamsModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/cloud_communication/SyncUpdatesService$5;->success(Lcom/vectorwatch/android/models/DefaultStreamsModel;Lretrofit/client/Response;)V

    return-void
.end method
