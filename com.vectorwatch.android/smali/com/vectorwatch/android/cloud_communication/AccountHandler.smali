.class public Lcom/vectorwatch/android/cloud_communication/AccountHandler;
.super Ljava/lang/Object;
.source "AccountHandler.java"


# static fields
.field private static final log:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/vectorwatch/android/cloud_communication/AccountHandler;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method public static prepareUserDataForLogin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/UserModel;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 146
    new-instance v0, Lcom/vectorwatch/android/models/UserModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/UserModel;-><init>()V

    .line 147
    .local v0, "userInfo":Lcom/vectorwatch/android/models/UserModel;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/UserModel;->setUsername(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v0, p2}, Lcom/vectorwatch/android/models/UserModel;->setPassword(Ljava/lang/String;)V

    .line 149
    const-string v1, "android"

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserModel;->setDeviceType(Ljava/lang/String;)V

    .line 150
    invoke-static {p0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->getEndpointArn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/UserModel;->setEndpointArn(Ljava/lang/String;)V

    .line 152
    return-object v0
.end method

.method public static prepareUserDataForSignUp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/vectorwatch/android/models/UserModel;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "registeredForUpdates"    # Z

    .prologue
    .line 166
    new-instance v0, Lcom/vectorwatch/android/models/LocalSettingsModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/LocalSettingsModel;-><init>()V

    .line 167
    .local v0, "localSettingsModel":Lcom/vectorwatch/android/models/LocalSettingsModel;
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SettingsHelper;->getPhoneTimeFormat(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setTimeFormat(Ljava/lang/String;)V

    .line 168
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SettingsHelper;->getPhoneUnitSystem(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/models/LocalSettingsModel;->setUnitSystem(Ljava/lang/String;)V

    .line 170
    new-instance v1, Lcom/vectorwatch/android/models/UserModel;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/UserModel;-><init>()V

    .line 171
    .local v1, "userInfo":Lcom/vectorwatch/android/models/UserModel;
    invoke-virtual {v1, p1}, Lcom/vectorwatch/android/models/UserModel;->setUsername(Ljava/lang/String;)V

    .line 172
    invoke-virtual {v1, p2}, Lcom/vectorwatch/android/models/UserModel;->setPassword(Ljava/lang/String;)V

    .line 173
    invoke-virtual {v1, p3}, Lcom/vectorwatch/android/models/UserModel;->setName(Ljava/lang/String;)V

    .line 174
    const-string v2, "android"

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/UserModel;->setDeviceType(Ljava/lang/String;)V

    .line 175
    invoke-virtual {v1, p4}, Lcom/vectorwatch/android/models/UserModel;->setRegisterForUpdates(Z)V

    .line 176
    invoke-static {p0}, Lcom/vectorwatch/android/RemotePushNotificationsInitializer;->getEndpointArn(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/UserModel;->setEndpointArn(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/LocalSettingsModel;->getPreferences()Lcom/vectorwatch/android/models/UserSettingsModel;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/models/UserModel;->setPreferences(Lcom/vectorwatch/android/models/UserSettingsModel;)V

    .line 179
    return-object v1
.end method

.method public static registerWatch(Ljava/lang/String;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 2
    .param p0, "nameOfBondedWatch"    # Ljava/lang/String;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;

    invoke-direct {v1, p2, p1, p0}, Lcom/vectorwatch/android/cloud_communication/AccountHandler$1;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 114
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 115
    return-void
.end method

.method public static userLogOut(Ljava/lang/String;Landroid/accounts/AccountManager;)Z
    .locals 4
    .param p0, "username"    # Ljava/lang/String;
    .param p1, "accountManager"    # Landroid/accounts/AccountManager;

    .prologue
    const/4 v3, 0x0

    .line 128
    const-string v2, "com.vectorwatch.android"

    invoke-virtual {p1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 129
    .local v0, "accounts":[Landroid/accounts/Account;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 130
    aget-object v2, v0, v1

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    aget-object v2, v0, v1

    invoke-virtual {p1, v2, v3, v3}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 132
    const/4 v2, 0x1

    .line 135
    :goto_1
    return v2

    .line 129
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 135
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
