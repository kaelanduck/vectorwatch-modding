.class public interface abstract Lcom/vectorwatch/android/cloud_communication/CloudCommunicationInterface;
.super Ljava/lang/Object;
.source "CloudCommunicationInterface.java"


# virtual methods
.method public abstract callAppCallback(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "version"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/AppAdditionalDataRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/app/proxy/{uuid}/{version}"
    .end annotation
.end method

.method public abstract confirmUpdateWithCpuId(Ljava/lang/Long;Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lretrofit/http/Path;
            value = "osId"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/CpuRegisterUnregisterModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/watch/confirm_update/{osId}/"
    .end annotation
.end method

.method public abstract getApp(Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Lcom/vectorwatch/android/models/DownloadedAppContainer;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/app/change_app_state/{uuid}/RUNNING"
    .end annotation
.end method

.method public abstract getAppAuthMethod(Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Lcom/vectorwatch/android/models/AuthMethodResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/app/{uuid}/authMethod"
    .end annotation
.end method

.method public abstract getAppPushData(Ljava/util/HashMap;)Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponseRoot;
    .param p1    # Ljava/util/HashMap;
        .annotation runtime Lretrofit/http/QueryMap;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vectorwatch/android/models/AppCallbackProxyResponse$PushResponseRoot;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/app/getPushData"
    .end annotation
.end method

.method public abstract getAppSettingOptions(Ljava/lang/String;Lcom/vectorwatch/android/models/AppOptionsRequest;)Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/AppOptionsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/app/{uuid}/getOptions"
    .end annotation
.end method

.method public abstract getAppSettings(Ljava/lang/String;Lcom/vectorwatch/android/models/AuthCredentialsRequest;)Lcom/vectorwatch/android/models/PossibleSettingsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Lcom/vectorwatch/android/models/AuthCredentialsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/app/{uuid}/getSettings"
    .end annotation
.end method

.method public abstract getContextualFromCloud()Lcom/vectorwatch/android/models/ResponseContextual;
    .annotation runtime Lretrofit/http/GET;
        value = "/account/contextual"
    .end annotation
.end method

.method public abstract getDefaultStreams(Lcom/vectorwatch/android/models/CloudRequestModel;)Lcom/vectorwatch/android/models/DefaultStreamsModel;
    .param p1    # Lcom/vectorwatch/android/models/CloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/stream/streams/default"
    .end annotation
.end method

.method public abstract getDefaults(Lcom/vectorwatch/android/models/CloudRequestModel;)Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;
    .param p1    # Lcom/vectorwatch/android/models/CloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/app/apps/default"
    .end annotation
.end method

.method public abstract getFilteredApps(Lcom/vectorwatch/android/models/CloudRequestModel;)Lcom/vectorwatch/android/models/RetrievedCloudAppsContainer;
    .param p1    # Lcom/vectorwatch/android/models/CloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/app/apps"
    .end annotation
.end method

.method public abstract getFilteredStreams(Lcom/vectorwatch/android/models/CloudRequestModel;)Lcom/vectorwatch/android/models/RetrievedStreamsContainer;
    .param p1    # Lcom/vectorwatch/android/models/CloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/stream/streams"
    .end annotation
.end method

.method public abstract getSettingsFromCloud()Lcom/vectorwatch/android/models/RemoteSettingsModel;
    .annotation runtime Lretrofit/http/GET;
        value = "/account/me"
    .end annotation
.end method

.method public abstract getStreamSettingOptions(Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/StreamOptionsRequest;)Lcom/vectorwatch/android/models/StreamSettingOptionsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "streamUUID"
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lretrofit/http/Path;
            value = "streamVersion"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/StreamOptionsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/stream/{streamUUID}/{streamVersion}/getOptions"
    .end annotation
.end method

.method public abstract getStreamSettings(Ljava/lang/String;Ljava/lang/Integer;Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Lcom/vectorwatch/android/models/PossibleSettingsResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "streamUUID"
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lretrofit/http/Path;
            value = "streamVersion"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/stream/{streamUUID}/{streamVersion}/getSettings"
    .end annotation
.end method

.method public abstract notifyAppStateChange(Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/BaseCloudRequestModel;)Lcom/vectorwatch/android/models/DownloadedAppContainer;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "uuid"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Path;
            value = "newAppState"
        .end annotation
    .end param
    .param p3    # Lcom/vectorwatch/android/models/BaseCloudRequestModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/app/change_app_state/{uuid}/{newAppState}"
    .end annotation
.end method

.method public abstract putSettingsInCloud(Lcom/vectorwatch/android/models/LocalSettingsModel;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Lcom/vectorwatch/android/models/LocalSettingsModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "alarmSettings"
    .end annotation

    .annotation runtime Lretrofit/http/PUT;
        value = "/account"
    .end annotation
.end method

.method public abstract queryStore(Ljava/lang/String;Ljava/util/HashMap;)Lcom/vectorwatch/android/models/StoreQuery;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "versionInfo"
        .end annotation
    .end param
    .param p2    # Ljava/util/HashMap;
        .annotation runtime Lretrofit/http/QueryMap;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/vectorwatch/android/models/StoreQuery;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/GET;
        value = "/store"
    .end annotation
.end method

.method public abstract syncContextualInCloud(Ljava/util/List;)Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p1    # Ljava/util/List;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/ContextualItem;",
            ">;)",
            "Lcom/vectorwatch/android/models/ServerResponseModel;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/account/contextual"
    .end annotation
.end method

.method public abstract updateDefaults(Lcom/vectorwatch/android/models/UpdateDefaultsModel;)Lcom/vectorwatch/android/models/RetrievedFullAppsContainer;
    .param p1    # Lcom/vectorwatch/android/models/UpdateDefaultsModel;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/app/updateDefaults/"
    .end annotation
.end method
