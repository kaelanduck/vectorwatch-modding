.class Lcom/vectorwatch/android/ui/webview/OauthActivity$MyJavaScriptInterface;
.super Ljava/lang/Object;
.source "OauthActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/webview/OauthActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyJavaScriptInterface"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;


# direct methods
.method private constructor <init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyJavaScriptInterface;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;Lcom/vectorwatch/android/ui/webview/OauthActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vectorwatch/android/ui/webview/OauthActivity;
    .param p2, "x1"    # Lcom/vectorwatch/android/ui/webview/OauthActivity$1;

    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyJavaScriptInterface;-><init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;)V

    return-void
.end method


# virtual methods
.method public processContent(Ljava/lang/String;)V
    .locals 4
    .param p1, "content"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 184
    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyJavaScriptInterface;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    # getter for: Lcom/vectorwatch/android/ui/webview/OauthActivity;->isVisibile:Z
    invoke-static {v2}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->access$400(Lcom/vectorwatch/android/ui/webview/OauthActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 185
    const-string v2, "Cannot GET "

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 186
    .local v1, "regex":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 187
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 189
    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyJavaScriptInterface;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    const-string v3, "User canceled"

    # invokes: Lcom/vectorwatch/android/ui/webview/OauthActivity;->returnErrorResult(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->access$500(Lcom/vectorwatch/android/ui/webview/OauthActivity;Ljava/lang/String;)V

    .line 192
    .end local v0    # "matcher":Ljava/util/regex/Matcher;
    .end local v1    # "regex":Ljava/util/regex/Pattern;
    :cond_0
    return-void
.end method
