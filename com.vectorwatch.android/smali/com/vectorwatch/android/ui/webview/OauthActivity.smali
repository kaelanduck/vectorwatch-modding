.class public Lcom/vectorwatch/android/ui/webview/OauthActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "OauthActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;,
        Lcom/vectorwatch/android/ui/webview/OauthActivity$MyJavaScriptInterface;,
        Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebChromeClient;
    }
.end annotation


# static fields
.field public static final ELEMENT_TYPE_APPLICATION:Ljava/lang/String; = "app"

.field public static final ELEMENT_TYPE_STREAM:Ljava/lang/String; = "stream"

.field public static final ERROR_REGEX_PATTERN:Ljava/lang/String; = "Cannot GET "

.field public static final EXTRA_ELEMENT_TYPE:Ljava/lang/String; = "element_type"

.field public static final EXTRA_ELEMENT_UUID:Ljava/lang/String; = "element_uuid"

.field public static final EXTRA_KEY_ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final EXTRA_KEY_CODE:Ljava/lang/String; = "code"

.field public static final EXTRA_KEY_ERROR_MSG:Ljava/lang/String; = "error_msg"

.field public static final EXTRA_KEY_LOGIN_URL:Ljava/lang/String; = "login_url"

.field public static final EXTRA_KEY_OAUTH_TOKEN:Ljava/lang/String; = "oauth_token"

.field public static final EXTRA_KEY_OAUTH_VERIFIER:Ljava/lang/String; = "oauth_verifier"

.field public static final EXTRA_KEY_OAUTH_VERSION:Ljava/lang/String; = "oauth_version"

.field public static final EXTRA_KEY_STATE:Ljava/lang/String; = "state"

.field public static final EXTRA_KEY_TITLE:Ljava/lang/String; = "title"

.field public static final EXTRA_KEY_TOKEN:Ljava/lang/String; = "token"

.field public static final EXTRA_KEY_VERIFIER:Ljava/lang/String; = "verifier"

.field public static final OAUTH_VERSION_1:Ljava/lang/String; = "oauth1"

.field public static final OAUTH_VERSION_2:Ljava/lang/String; = "oauth2"

.field public static final OAUTH_VERSION_3:Ljava/lang/String; = "oauth3"

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private elementType:Ljava/lang/String;

.field private elementUuid:Ljava/lang/String;

.field private isVisibile:Z

.field private loginUrl:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;

.field private progressBar:Landroid/view/View;

.field private streamElemId:Ljava/lang/Integer;

.field private streamPosId:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/vectorwatch/android/ui/webview/OauthActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->isVisibile:Z

    .line 54
    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    .line 55
    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->loginUrl:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->progressBar:Landroid/view/View;

    .line 198
    return-void
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/webview/OauthActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/webview/OauthActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->progressBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/webview/OauthActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/webview/OauthActivity;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->isVisibile:Z

    return v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/webview/OauthActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/webview/OauthActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->returnErrorResult(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/webview/OauthActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/webview/OauthActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->returnSuccessResult()V

    return-void
.end method

.method private initWebView()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 118
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->clearCookies()V

    .line 119
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 120
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 121
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 122
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebChromeClient;

    invoke-direct {v1, p0, v2}, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebChromeClient;-><init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;Lcom/vectorwatch/android/ui/webview/OauthActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 123
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;

    invoke-direct {v1, p0, v2}, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;-><init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;Lcom/vectorwatch/android/ui/webview/OauthActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 124
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyJavaScriptInterface;

    invoke-direct {v1, p0, v2}, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyJavaScriptInterface;-><init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;Lcom/vectorwatch/android/ui/webview/OauthActivity$1;)V

    const-string v2, "VectorWatchInterface"

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->loginUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method private returnErrorResult(Ljava/lang/String;)V
    .locals 4
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 129
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 130
    .local v0, "returnIntent":Landroid/content/Intent;
    const-string v1, "error_msg"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    const-string v1, "stream_pos"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->streamPosId:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 132
    const-string v1, "elem_pos"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->streamElemId:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 133
    const-string v1, "element_uuid"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/events/StreamAuthCanceledEvent;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/vectorwatch/android/events/StreamAuthCanceledEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 139
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->setResult(ILandroid/content/Intent;)V

    .line 140
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->finish()V

    .line 141
    return-void
.end method

.method private returnSuccessResult()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 144
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 145
    .local v0, "returnIntent":Landroid/content/Intent;
    const-string v1, "oauth_version"

    const-string v2, "oauth1"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    const-string v1, "stream_pos"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->streamPosId:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 147
    const-string v1, "elem_pos"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->streamElemId:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 148
    const-string v1, "element_uuid"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementType:Ljava/lang/String;

    const-string v2, "app"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    invoke-static {p0, v1, v3}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->setAuthInvalidated(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 156
    :cond_0
    :goto_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->setResult(ILandroid/content/Intent;)V

    .line 157
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->finish()V

    .line 158
    return-void

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementType:Ljava/lang/String;

    const-string v2, "stream"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    invoke-static {p0, v1, v3}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->setAuthInvalidated(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private splitQuery(Ljava/net/URL;)Ljava/util/Map;
    .locals 12
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URL;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 325
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    .line 326
    .local v5, "queryPairs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v4

    .line 327
    .local v4, "query":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 328
    const-string v6, "&"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 329
    .local v3, "pairs":[Ljava/lang/String;
    array-length v8, v3

    move v6, v7

    :goto_0
    if-ge v6, v8, :cond_3

    aget-object v2, v3, v6

    .line 330
    .local v2, "pair":Ljava/lang/String;
    const-string v9, "="

    invoke-virtual {v2, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 331
    .local v1, "idx":I
    if-lez v1, :cond_0

    .line 332
    invoke-virtual {v2, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const-string v10, "UTF-8"

    invoke-static {v9, v10}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v10, v1, 0x1

    invoke-virtual {v2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "UTF-8"

    invoke-static {v10, v11}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 334
    :cond_0
    const-string v9, "UTF-8"

    invoke-static {v2, v9}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-interface {v5, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 337
    .end local v1    # "idx":I
    .end local v2    # "pair":Ljava/lang/String;
    .end local v3    # "pairs":[Ljava/lang/String;
    :cond_1
    if-eqz p1, :cond_3

    .line 338
    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v4

    .line 339
    if-eqz v4, :cond_3

    .line 340
    sget-object v6, Lcom/vectorwatch/android/ui/webview/OauthActivity;->log:Lorg/slf4j/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "AUTH: query = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 344
    const-string v6, "#"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 345
    .local v0, "firstSplit":[Ljava/lang/String;
    array-length v6, v0

    if-le v6, v10, :cond_3

    .line 346
    aget-object v6, v0, v10

    const-string v8, "&"

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 348
    .restart local v3    # "pairs":[Ljava/lang/String;
    array-length v8, v3

    move v6, v7

    :goto_2
    if-ge v6, v8, :cond_3

    aget-object v2, v3, v6

    .line 349
    .restart local v2    # "pair":Ljava/lang/String;
    const-string v9, "="

    invoke-virtual {v2, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 350
    .restart local v1    # "idx":I
    if-lez v1, :cond_2

    .line 351
    invoke-virtual {v2, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const-string v10, "UTF-8"

    invoke-static {v9, v10}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v10, v1, 0x1

    invoke-virtual {v2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "UTF-8"

    invoke-static {v10, v11}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 353
    :cond_2
    const-string v9, "UTF-8"

    invoke-static {v2, v9}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    invoke-interface {v5, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 359
    .end local v0    # "firstSplit":[Ljava/lang/String;
    .end local v1    # "idx":I
    .end local v2    # "pair":Ljava/lang/String;
    .end local v3    # "pairs":[Ljava/lang/String;
    :cond_3
    return-object v5
.end method

.method private syncTokensToServer(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 297
    .local p1, "tokens":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;

    invoke-direct {v0}, Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;-><init>()V

    .line 298
    .local v0, "credentials":Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;
    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;->setAuthInfo(Ljava/util/HashMap;)V

    .line 299
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementType:Ljava/lang/String;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    new-instance v3, Lcom/vectorwatch/android/ui/webview/OauthActivity$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity$1;-><init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;)V

    invoke-static {p0, v1, v2, v0, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->setAuthCredentials(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/cloud/AuthCredentialsCloudModel;Lretrofit/Callback;)V

    .line 315
    return-void
.end method


# virtual methods
.method public clearCookies()V
    .locals 4

    .prologue
    .line 235
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x16

    if-lt v2, v3, :cond_0

    .line 236
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/webkit/CookieManager;->removeAllCookies(Landroid/webkit/ValueCallback;)V

    .line 237
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/webkit/CookieManager;->flush()V

    .line 247
    :goto_0
    return-void

    .line 239
    :cond_0
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v1

    .line 240
    .local v1, "cookieSyncMngr":Landroid/webkit/CookieSyncManager;
    invoke-virtual {v1}, Landroid/webkit/CookieSyncManager;->startSync()V

    .line 241
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 242
    .local v0, "cookieManager":Landroid/webkit/CookieManager;
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 243
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeSessionCookie()V

    .line 244
    invoke-virtual {v1}, Landroid/webkit/CookieSyncManager;->stopSync()V

    .line 245
    invoke-virtual {v1}, Landroid/webkit/CookieSyncManager;->sync()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 365
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 373
    :goto_0
    return-void

    .line 368
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 369
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/StreamAuthCanceledEvent;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/vectorwatch/android/events/StreamAuthCanceledEvent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 371
    :cond_1
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, -0x1

    .line 90
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const v1, 0x7f03002a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->setContentView(I)V

    .line 93
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "login_url"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->loginUrl:Ljava/lang/String;

    .line 94
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->loginUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->loginUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    :cond_0
    const v1, 0x7f090113

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->returnErrorResult(Ljava/lang/String;)V

    .line 115
    :goto_0
    return-void

    .line 99
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "stream_place_id"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->streamPosId:Ljava/lang/Integer;

    .line 100
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "watchface_elem_id"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->streamElemId:Ljava/lang/Integer;

    .line 101
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "element_uuid"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementUuid:Ljava/lang/String;

    .line 102
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "element_type"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->elementType:Ljava/lang/String;

    .line 104
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "title":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 107
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 110
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 111
    const v1, 0x7f090168

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 112
    const v1, 0x7f100111

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->mWebView:Landroid/webkit/WebView;

    .line 113
    const v1, 0x7f1000f8

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->progressBar:Landroid/view/View;

    .line 114
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->initWebView()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 383
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onPause()V

    .line 384
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->isVisibile:Z

    .line 385
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 377
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onResume()V

    .line 378
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity;->isVisibile:Z

    .line 379
    return-void
.end method

.method public parseLoadedUrl(Ljava/lang/String;)Z
    .locals 7
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 256
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 259
    .local v0, "authParams":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 260
    .local v3, "urlResource":Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v5

    const-string v6, "http"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 261
    invoke-direct {p0, v3}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->splitQuery(Ljava/net/URL;)Ljava/util/Map;

    move-result-object v2

    .line 262
    .local v2, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "oauth_token"

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "oauth_verifier"

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 264
    const-string v5, "token"

    const-string v6, "oauth_token"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    const-string v5, "verifier"

    const-string v6, "oauth_verifier"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->syncTokensToServer(Ljava/util/HashMap;)V

    .line 277
    :cond_0
    :goto_0
    const/4 v4, 0x0

    .line 287
    .end local v2    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "urlResource":Ljava/net/URL;
    :cond_1
    :goto_1
    return v4

    .line 267
    .restart local v2    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v3    # "urlResource":Ljava/net/URL;
    :cond_2
    const-string v5, "code"

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "state"

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 269
    const-string v5, "code"

    const-string v6, "code"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    const-string v5, "state"

    const-string v6, "state"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->syncTokensToServer(Ljava/util/HashMap;)V

    goto :goto_0

    .line 281
    .end local v2    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "urlResource":Ljava/net/URL;
    :catch_0
    move-exception v1

    .line 283
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_1

    .line 272
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v3    # "urlResource":Ljava/net/URL;
    :cond_3
    const-string v5, "access_token"

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 273
    const-string v5, "access_token"

    const-string v6, "access_token"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    const-string v5, "state"

    const-string v6, "state"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->syncTokensToServer(Ljava/util/HashMap;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 284
    .end local v2    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "urlResource":Ljava/net/URL;
    :catch_1
    move-exception v1

    .line 286
    .local v1, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_1
.end method
