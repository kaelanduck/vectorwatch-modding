.class Lcom/vectorwatch/android/ui/webview/OauthActivity$1;
.super Ljava/lang/Object;
.source "OauthActivity.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/webview/OauthActivity;->syncTokensToServer(Ljava/util/HashMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/webview/OauthActivity;

    .prologue
    .line 300
    iput-object p1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$1;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 3
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    .line 312
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$1;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$1;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    const v2, 0x7f090114

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/vectorwatch/android/ui/webview/OauthActivity;->returnErrorResult(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->access$500(Lcom/vectorwatch/android/ui/webview/OauthActivity;Ljava/lang/String;)V

    .line 313
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 300
    check-cast p1, Ljava/util/HashMap;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/webview/OauthActivity$1;->success(Ljava/util/HashMap;Lretrofit/client/Response;)V

    return-void
.end method

.method public success(Ljava/util/HashMap;Lretrofit/client/Response;)V
    .locals 3
    .param p2, "response"    # Lretrofit/client/Response;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lretrofit/client/Response;",
            ")V"
        }
    .end annotation

    .prologue
    .line 303
    .local p1, "stringStringHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p2}, Lretrofit/client/Response;->getStatus()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 304
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$1;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    # invokes: Lcom/vectorwatch/android/ui/webview/OauthActivity;->returnSuccessResult()V
    invoke-static {v0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->access$600(Lcom/vectorwatch/android/ui/webview/OauthActivity;)V

    .line 308
    :goto_0
    return-void

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$1;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$1;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    const v2, 0x7f090114

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/vectorwatch/android/ui/webview/OauthActivity;->returnErrorResult(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->access$500(Lcom/vectorwatch/android/ui/webview/OauthActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
