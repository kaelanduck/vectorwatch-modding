.class Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "OauthActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/webview/OauthActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;


# direct methods
.method private constructor <init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;Lcom/vectorwatch/android/ui/webview/OauthActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vectorwatch/android/ui/webview/OauthActivity;
    .param p2, "x1"    # Lcom/vectorwatch/android/ui/webview/OauthActivity$1;

    .prologue
    .line 198
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;-><init>(Lcom/vectorwatch/android/ui/webview/OauthActivity;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    # getter for: Lcom/vectorwatch/android/ui/webview/OauthActivity;->isVisibile:Z
    invoke-static {v0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->access$400(Lcom/vectorwatch/android/ui/webview/OauthActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    :cond_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 206
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    # getter for: Lcom/vectorwatch/android/ui/webview/OauthActivity;->isVisibile:Z
    invoke-static {v1}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->access$400(Lcom/vectorwatch/android/ui/webview/OauthActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 208
    .local v0, "uriResource":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->startActivity(Landroid/content/Intent;)V

    .line 210
    invoke-virtual {p1}, Landroid/webkit/WebView;->stopLoading()V

    .line 211
    invoke-virtual {p1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    invoke-virtual {p1}, Landroid/webkit/WebView;->goBack()V

    .line 216
    .end local v0    # "uriResource":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    # getter for: Lcom/vectorwatch/android/ui/webview/OauthActivity;->isVisibile:Z
    invoke-static {v0}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->access$400(Lcom/vectorwatch/android/ui/webview/OauthActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    const/16 v0, 0x5dc

    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    invoke-static {p3, v0, v1}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 203
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 225
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    invoke-virtual {v1, p2}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->parseLoadedUrl(Ljava/lang/String;)Z

    move-result v0

    .line 226
    .local v0, "shouldOverrideUrlLoading":Z
    if-eqz v0, :cond_0

    .line 227
    iget-object v1, p0, Lcom/vectorwatch/android/ui/webview/OauthActivity$MyWebViewClient;->this$0:Lcom/vectorwatch/android/ui/webview/OauthActivity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/webview/OauthActivity;->startActivity(Landroid/content/Intent;)V

    .line 229
    :cond_0
    return v0
.end method
