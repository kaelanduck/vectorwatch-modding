.class public Lcom/vectorwatch/android/ui/onboarding_experience/RecoverPassActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "RecoverPassActivity.java"


# instance fields
.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    const v0, 0x7f03009b

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/RecoverPassActivity;->setContentView(I)V

    .line 21
    const v0, 0x7f10025b

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/RecoverPassActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/RecoverPassActivity;->webView:Landroid/webkit/WebView;

    .line 22
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/RecoverPassActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 23
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/RecoverPassActivity;->webView:Landroid/webkit/WebView;

    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getRecoverPassUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 24
    return-void
.end method
