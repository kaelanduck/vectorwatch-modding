.class public Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
.super Landroid/app/Activity;
.source "ConnectWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;,
        Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;,
        Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;,
        Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;
    }
.end annotation


# static fields
.field private static final DELAY_FOR_SHOWING_NOTIFICATION_SCREEN:I = 0x1770

.field private static final DELAY_SHOW_CONNECTED_SCREEN:J = 0x9c4L

.field public static final EXTRA_IS_PAIRED:Ljava/lang/String; = "EXTRA_IS_PAIRED"

.field private static final SCAN_PERIOD:J = 0x1770L

.field private static final TROUBLESHOOTING_LINK:Ljava/lang/String; = "http://support.vectorwatch.com/go/connection-troubleshooting"

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private deviceAdapter:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;

.field deviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;",
            ">;"
        }
    .end annotation
.end field

.field final handler:Landroid/os/Handler;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothManager:Landroid/bluetooth/BluetoothManager;

.field private mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mBtConnect:Landroid/widget/Button;

.field private mBtTblstLink:Landroid/widget/TextView;

.field private mBtTour:Landroid/widget/TextView;

.field private mCurrentState:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

.field private mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

.field private mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mHandler:Landroid/os/Handler;

.field private mImgConnectBackground:Landroid/widget/ImageView;

.field private mLbConnect:Landroid/widget/TextView;

.field private mLbFound:Landroid/widget/TextView;

.field private mLbInfo:Landroid/widget/TextView;

.field private mLbSearching:Landroid/widget/TextView;

.field private mLbWatchConnected:Landroid/widget/TextView;

.field private mLvDevices:Landroid/widget/ListView;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRunnable:Ljava/lang/Runnable;

.field private mScanning:Z

.field private mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

.field private scanCallback:Lcom/vectorwatch/android/utils/scanner/VectorScanner;

.field private txtSelectWatch:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->handler:Landroid/os/Handler;

    .line 86
    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 89
    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 90
    sget-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Initial:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentState:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    .line 92
    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    .line 93
    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 826
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanLeDevice(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mScanning:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Lcom/vectorwatch/android/utils/scanner/VectorScanner;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanCallback:Lcom/vectorwatch/android/utils/scanner/VectorScanner;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentState:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->showEnableBluetoothDialog()V

    return-void
.end method

.method static synthetic access$1700(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->redirectUserToAllowNotifications()V

    return-void
.end method

.method static synthetic access$1800(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->goToMain()V

    return-void
.end method

.method static synthetic access$1900(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onNotificationsAllowed()V

    return-void
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$302(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;)Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
    .param p1, "x1"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    return-object p1
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onDeviceConnecting()V

    return-void
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$502(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
    .param p1, "x1"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->saveDeviceToSharedPreferences(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->triggerConnect()V

    return-void
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    return-object v0
.end method

.method private addDevice(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V
    .locals 6
    .param p1, "discovDev"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "bondedState"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 758
    const/4 v1, 0x0

    .line 759
    .local v1, "found":Z
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 760
    .local v2, "localIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 761
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 764
    const/4 v1, 0x1

    goto :goto_0

    .line 766
    :cond_1
    if-nez v1, :cond_4

    .line 767
    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    .line 768
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, p0, v3, v4, p1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Ljava/lang/String;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    .line 769
    .local v0, "device":Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 770
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_2

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    .line 771
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_4

    :cond_2
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x53

    if-eq v3, v4, :cond_3

    .line 772
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x52

    if-ne v3, v4, :cond_4

    .line 773
    :cond_3
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 774
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceAdapter:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;->notifyDataSetChanged()V

    .line 777
    .end local v0    # "device":Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;
    :cond_4
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onNewDeviceAdded()V

    .line 778
    return-void
.end method

.method private goToLogin()V
    .locals 2

    .prologue
    .line 687
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 688
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 689
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 690
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->finish()V

    .line 691
    return-void
.end method

.method private goToMain()V
    .locals 2

    .prologue
    .line 680
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 681
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 682
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->startActivity(Landroid/content/Intent;)V

    .line 683
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->finish()V

    .line 684
    return-void
.end method

.method private needsLogin()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 695
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 696
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v4, "com.vectorwatch.android"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 698
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v4, v1

    if-ge v4, v2, :cond_0

    .line 699
    const-string v3, "Store-Apps:"

    const-string v4, "There should be at least one account in Accounts & sync"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    :goto_0
    return v2

    .line 702
    :cond_0
    aget-object v4, v1, v3

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAccountAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 703
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLoggedInStatus(ZLandroid/content/Context;)V

    move v2, v3

    .line 704
    goto :goto_0
.end method

.method private onDeviceConnected()V
    .locals 4

    .prologue
    .line 636
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 637
    sget-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connected:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    .line 640
    const-string v0, "flag_show_onboarding"

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 643
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$11;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$11;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 654
    return-void
.end method

.method private onDeviceConnecting()V
    .locals 2

    .prologue
    .line 630
    sget-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connecting:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    .line 631
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 632
    return-void
.end method

.method private onNewDeviceAdded()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 515
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 518
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 519
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 520
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 521
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->txtSelectWatch:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 522
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 523
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    const v0, 0x7f09015b

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 527
    :cond_0
    return-void

    .line 523
    :cond_1
    const v0, 0x7f09015c

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private onNotificationsAllowed()V
    .locals 3

    .prologue
    .line 719
    const-string v1, "flag_notifications_allowed_from_os"

    const/4 v2, 0x1

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    .line 721
    .local v0, "enableAllNoifications":Z
    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 722
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->enableAllByDefault(Landroid/app/Activity;)V

    .line 725
    const-string v1, "flag_notifications_allowed_from_os"

    const/4 v2, 0x0

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 729
    :cond_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->needsLogin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 730
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->goToLogin()V

    .line 733
    :goto_0
    return-void

    .line 732
    :cond_1
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->goToMain()V

    goto :goto_0
.end method

.method private populateList()V
    .locals 4

    .prologue
    .line 615
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    .line 616
    new-instance v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-direct {v2, p0, p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Landroid/app/Activity;Ljava/util/List;)V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceAdapter:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;

    .line 618
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceAdapter:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 619
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 621
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 622
    .local v1, "localIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 623
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 624
    .local v0, "localBluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    const-string v2, "bonded"

    invoke-direct {p0, v0, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->addDevice(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V

    goto :goto_0

    .line 627
    .end local v0    # "localBluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    :cond_0
    return-void
.end method

.method private redirectUserToAllowNotifications()V
    .locals 4

    .prologue
    .line 708
    const-string v2, "AllowNotif"

    const-string v3, "redirect to allow notif screen"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/vectorwatch/android/service/nls/NLService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 710
    .local v1, "service":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 712
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 714
    .local v0, "intent":Landroid/content/Intent;
    const/16 v2, 0x4d2

    invoke-virtual {p0, v0, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 715
    return-void
.end method

.method private saveDeviceToSharedPreferences(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 658
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchOldCpuId(Ljava/lang/String;Landroid/content/Context;)V

    .line 660
    const-string v0, "Pair:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Address: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    invoke-static {p1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBondedDeviceName(Ljava/lang/String;Landroid/content/Context;)V

    .line 662
    invoke-static {p2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBondedDeviceAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 665
    const-string v0, "bonded_watch_dirty"

    const/4 v1, 0x1

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 667
    const-string v0, "pref_pair_time"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLongPreference(Ljava/lang/String;JLandroid/content/Context;)V

    .line 669
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->notifyCloud(Landroid/content/Context;)V

    .line 670
    return-void
.end method

.method private scanLeDevice(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 737
    if-eqz p1, :cond_0

    .line 739
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 741
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->populateList()V

    .line 743
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 745
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mScanning:Z

    .line 746
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanCallback:Lcom/vectorwatch/android/utils/scanner/VectorScanner;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/utils/scanner/VectorScanner;->startScan(Landroid/bluetooth/BluetoothAdapter;)V

    .line 755
    :goto_0
    return-void

    .line 749
    :cond_0
    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mScanning:Z

    .line 750
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanCallback:Lcom/vectorwatch/android/utils/scanner/VectorScanner;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/utils/scanner/VectorScanner;->stopScan(Landroid/bluetooth/BluetoothAdapter;)V

    .line 752
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private showEnableBluetoothDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 588
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 589
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0900a6

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900a8

    .line 590
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    .line 591
    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    new-instance v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$10;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$10;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    .line 592
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009

    .line 609
    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 610
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 612
    return-void
.end method

.method private triggerConnect()V
    .locals 0

    .prologue
    .line 676
    invoke-static {p0}, Lcom/vectorwatch/android/service/ble/BluetoothManager;->connect(Landroid/content/Context;)V

    .line 677
    return-void
.end method


# virtual methods
.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 876
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 877
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 879
    :cond_0
    return-void
.end method

.method public handleFoundBleDeviceEvent(Lcom/vectorwatch/android/events/FoundBleDeviceEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/FoundBleDeviceEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 866
    :try_start_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 867
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/FoundBleDeviceEvent;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    const-string v2, "in range"

    invoke-direct {p0, v1, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->addDevice(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 872
    :cond_0
    :goto_0
    return-void

    .line 869
    :catch_0
    move-exception v0

    .line 870
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->log:Lorg/slf4j/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connect watch - error in handleFoundDevice "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 369
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onDeviceConnected()V

    .line 372
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 582
    const/16 v0, 0x4d2

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 583
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onNotificationsAllowed()V

    .line 585
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 531
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mScanning:Z

    if-eqz v0, :cond_0

    .line 532
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanLeDevice(Z)V

    .line 539
    :goto_0
    return-void

    .line 533
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 534
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 535
    sget-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Initial:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    goto :goto_0

    .line 537
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 184
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 185
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 187
    const-string v3, "bluetooth"

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothManager;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothManager:Landroid/bluetooth/BluetoothManager;

    .line 188
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothManager:Landroid/bluetooth/BluetoothManager;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 189
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v3, :cond_0

    .line 190
    const v3, 0x7f090076

    invoke-static {p0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 194
    :cond_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_4

    .line 195
    const-string v3, "SCANNER"

    const-string v4, "Scan callback instantiated for API_LEVEL >= 21"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    new-instance v3, Lcom/vectorwatch/android/utils/scanner/ScannerApi21;

    invoke-direct {v3}, Lcom/vectorwatch/android/utils/scanner/ScannerApi21;-><init>()V

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanCallback:Lcom/vectorwatch/android/utils/scanner/VectorScanner;

    .line 201
    :goto_0
    const v3, 0x7f030021

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->setContentView(I)V

    .line 203
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 205
    const v3, 0x7f1000c0

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTour:Landroid/widget/TextView;

    .line 206
    const v3, 0x7f1000c1

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTblstLink:Landroid/widget/TextView;

    .line 207
    const v3, 0x7f1000be

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    .line 208
    const v3, 0x7f1000bb

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    .line 209
    const v3, 0x7f1000ba

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    .line 210
    const v3, 0x7f1000bd

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    .line 211
    const v3, 0x7f1000bc

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbWatchConnected:Landroid/widget/TextView;

    .line 212
    const v3, 0x7f1000b9

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mImgConnectBackground:Landroid/widget/ImageView;

    .line 213
    const v3, 0x7f1000c4

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    .line 214
    const v3, 0x7f1000c2

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    .line 215
    const v3, 0x7f1000bf

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    .line 216
    const v3, 0x7f1000c3

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->txtSelectWatch:Landroid/widget/TextView;

    .line 218
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTour:Landroid/widget/TextView;

    new-instance v4, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$2;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$2;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    new-instance v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mRunnable:Ljava/lang/Runnable;

    .line 259
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v3, v4, :cond_1

    .line 260
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    const v4, 0x7f0901dc

    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    new-instance v4, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$4;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$4;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 268
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    const/4 v4, 0x2

    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 269
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00b2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 270
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    const-string v4, "fonts/Reader-Regular.otf"

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 271
    .local v1, "font":Landroid/graphics/Typeface;
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 274
    .end local v1    # "font":Landroid/graphics/Typeface;
    :cond_1
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTblstLink:Landroid/widget/TextView;

    new-instance v4, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$5;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$5;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    new-instance v4, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;

    invoke-direct {v4, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    new-instance v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$7;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$7;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 339
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mHandler:Landroid/os/Handler;

    .line 341
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 342
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_2

    .line 343
    const-string v3, "EXTRA_IS_PAIRED"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 344
    .local v2, "state":I
    const-string v3, "Connect"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IS PAIRED = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 348
    sget-object v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connecting:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    .line 349
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->triggerConnect()V

    .line 356
    .end local v2    # "state":I
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "allow_screen"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 357
    sget-object v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->AllowNotifications:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    .line 359
    :cond_3
    return-void

    .line 198
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_4
    new-instance v3, Lcom/vectorwatch/android/utils/scanner/ScannerApi18;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/utils/scanner/ScannerApi18;-><init>(Landroid/app/Activity;)V

    iput-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanCallback:Lcom/vectorwatch/android/utils/scanner/VectorScanner;

    goto/16 :goto_0

    .line 350
    .restart local v0    # "extras":Landroid/os/Bundle;
    .restart local v2    # "state":I
    :cond_5
    if-nez v2, :cond_2

    .line 352
    sget-object v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Initial:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 363
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 364
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 365
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    .line 543
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 545
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.device.action.FOUND"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 546
    .local v1, "localIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 547
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 548
    .local v0, "localBroadcastReceiver":Landroid/content/BroadcastReceiver;
    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 550
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentState:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    sget-object v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->AllowNotifications:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    if-ne v2, v3, :cond_0

    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 551
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onNotificationsAllowed()V

    .line 554
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    .line 558
    iget-boolean v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mScanning:Z

    if-eqz v2, :cond_0

    .line 559
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanLeDevice(Z)V

    .line 561
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 562
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 564
    :try_start_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 571
    .local v1, "localBroadcastReceiver":Landroid/content/BroadcastReceiver;
    if-eqz v1, :cond_2

    .line 573
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 578
    :cond_2
    :goto_1
    return-void

    .line 565
    .end local v1    # "localBroadcastReceiver":Landroid/content/BroadcastReceiver;
    :catch_0
    move-exception v0

    .line 566
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Unregister"

    const-string v3, "Tried to unregister non-null bonding broadcast receiver and it failed."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 574
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "localBroadcastReceiver":Landroid/content/BroadcastReceiver;
    :catch_1
    move-exception v0

    .line 575
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v2, "Unregister"

    const-string v3, "Tried to unregister non-null local broadcast receiver and it failed."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V
    .locals 10
    .param p1, "state"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    .prologue
    const v9, 0x7f0200f5

    const v8, 0x7f0200f4

    const/4 v7, 0x0

    const/4 v6, 0x4

    const/16 v3, 0x8

    .line 375
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentState:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    .line 376
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 377
    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->UnpairedFromSettings:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    if-ne p1, v1, :cond_0

    .line 378
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 379
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 380
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTour:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 381
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTblstLink:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 382
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 383
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 384
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 385
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbWatchConnected:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 386
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    const v2, 0x7f09017e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 387
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 388
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    const v2, 0x7f090151

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 389
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 390
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->txtSelectWatch:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 392
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$8;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$8;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    const-wide/16 v4, 0x1770

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 404
    :cond_0
    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Scanning:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    if-ne p1, v1, :cond_3

    .line 405
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 406
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 407
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    const v2, 0x7f090195

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 408
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTour:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 409
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 410
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbWatchConnected:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 411
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 412
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->txtSelectWatch:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 413
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 414
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 415
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 424
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 425
    .local v0, "mLbConnectLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 512
    .end local v0    # "mLbConnectLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    :goto_1
    return-void

    .line 417
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 418
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    invoke-virtual {v1, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 419
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 420
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 421
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 426
    :cond_3
    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Initial:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    if-ne p1, v1, :cond_5

    .line 427
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    new-instance v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$9;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$9;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 434
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 435
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 436
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTour:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 437
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTblstLink:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 438
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v1, v3}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 439
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 440
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 441
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbWatchConnected:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 442
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_4

    .line 443
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    const v2, 0x7f0901dc

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    :goto_2
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 448
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    const v2, 0x7f090195

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 449
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->txtSelectWatch:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 451
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    const v2, 0x7f090157

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 452
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 453
    .restart local v0    # "mLbConnectLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 454
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mImgConnectBackground:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto/16 :goto_1

    .line 445
    .end local v0    # "mLbConnectLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    const v2, 0x7f090154

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 456
    :cond_5
    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connecting:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    if-ne p1, v1, :cond_8

    .line 457
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    if-eqz v1, :cond_7

    .line 458
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    sget-object v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Round:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    if-ne v1, v2, :cond_6

    .line 459
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mImgConnectBackground:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 460
    :cond_6
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    sget-object v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Square:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    if-ne v1, v2, :cond_7

    .line 461
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mImgConnectBackground:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 463
    :cond_7
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 464
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 465
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    const v2, 0x7f090152

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 466
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTour:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 467
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v1, v7}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 468
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 469
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 470
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 471
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->txtSelectWatch:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 473
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbWatchConnected:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 474
    :cond_8
    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connected:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    if-ne p1, v1, :cond_b

    .line 475
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    if-eqz v1, :cond_a

    .line 476
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    sget-object v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Round:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    if-ne v1, v2, :cond_9

    .line 477
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mImgConnectBackground:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 478
    :cond_9
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    sget-object v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Square:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    if-ne v1, v2, :cond_a

    .line 479
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mImgConnectBackground:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 481
    :cond_a
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 482
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 483
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 484
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTour:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 485
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v1, v6}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 486
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 487
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 488
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 489
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->txtSelectWatch:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 491
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbWatchConnected:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 492
    :cond_b
    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->AllowNotifications:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    if-ne p1, v1, :cond_1

    .line 493
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 494
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;

    const v2, 0x7f090079

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 495
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 496
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    const v2, 0x7f090150

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 497
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbConnect:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 498
    .restart local v0    # "mLbConnectLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 499
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mImgConnectBackground:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 501
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 502
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtTour:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 503
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mSpvProgress:Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;

    invoke-virtual {v1, v6}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 504
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbFound:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 505
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 506
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbWatchConnected:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 507
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 508
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbInfo:Landroid/widget/TextView;

    const v2, 0x7f09014f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 509
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->txtSelectWatch:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method
