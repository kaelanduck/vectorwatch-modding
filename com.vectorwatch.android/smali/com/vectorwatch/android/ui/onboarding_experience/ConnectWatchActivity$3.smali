.class Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;
.super Ljava/lang/Object;
.source "ConnectWatchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 227
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 230
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # setter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mScanning:Z
    invoke-static {v0, v4}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$1002(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Z)Z

    .line 231
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanCallback:Lcom/vectorwatch/android/utils/scanner/VectorScanner;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$1200(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Lcom/vectorwatch/android/utils/scanner/VectorScanner;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$1100(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/utils/scanner/VectorScanner;->stopScan(Landroid/bluetooth/BluetoothAdapter;)V

    .line 233
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 235
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLvDevices:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$1300(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 250
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3$1;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;)V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 256
    return-void

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$800(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 240
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_2

    .line 241
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$800(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    :goto_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$900(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090083

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$900(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$800(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
