.class Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;
.super Ljava/lang/Object;
.source "ConnectWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothDeviceCustom"
.end annotation


# instance fields
.field private address:Ljava/lang/String;

.field private bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private name:Ljava/lang/String;

.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Ljava/lang/String;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 831
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .line 832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 833
    iput-object p2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->name:Ljava/lang/String;

    .line 834
    iput-object p3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->address:Ljava/lang/String;

    .line 835
    iput-object p4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 836
    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 847
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 859
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->address:Ljava/lang/String;

    .line 860
    return-void
.end method

.method public setBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 843
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 844
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 851
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->name:Ljava/lang/String;

    .line 852
    return-void
.end method
