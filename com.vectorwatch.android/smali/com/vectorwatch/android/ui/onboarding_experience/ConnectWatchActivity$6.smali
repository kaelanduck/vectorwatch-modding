.class Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;
.super Ljava/lang/Object;
.source "ConnectWatchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 288
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 291
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentState:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$1500(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->AllowNotifications:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    if-eq v1, v2, :cond_2

    .line 292
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$1100(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->showEnableBluetoothDialog()V
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$1600(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    .line 322
    :goto_0
    return-void

    .line 295
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_1

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$900(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, "RETRY"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 300
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    const v2, 0x7f0c00c5

    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 304
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0901d8

    new-instance v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6$1;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;)V

    .line 305
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 313
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 315
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    const/4 v2, 0x1

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanLeDevice(Z)V
    invoke-static {v1, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$000(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Z)V

    .line 316
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    sget-object v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Scanning:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    goto :goto_0

    .line 320
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$6;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->redirectUserToAllowNotifications()V
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$1700(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    goto :goto_0
.end method
