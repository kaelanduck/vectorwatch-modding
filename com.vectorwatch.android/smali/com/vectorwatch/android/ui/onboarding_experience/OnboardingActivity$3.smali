.class Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;
.super Ljava/lang/Object;
.source "OnboardingActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->access$000(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 95
    :goto_0
    return-void

    .line 86
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->start()V

    goto :goto_0

    .line 89
    :pswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->start()V

    goto :goto_0

    .line 92
    :pswitch_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->start()V

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 100
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->access$000(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 80
    :goto_0
    return-void

    .line 65
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    .line 67
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setAlpha(F)V

    goto :goto_0

    .line 70
    :pswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    .line 72
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setAlpha(F)V

    goto :goto_0

    .line 75
    :pswitch_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    .line 77
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setAlpha(F)V

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
