.class Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$8;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->initControls()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$8;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 262
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$8;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/NetworkUtils;->isOnline(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$8;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    const-class v2, Lcom/vectorwatch/android/ui/onboarding_experience/RecoverPassActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 264
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$8;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 268
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$8;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    const v2, 0x7f09020a

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$8;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0
.end method
