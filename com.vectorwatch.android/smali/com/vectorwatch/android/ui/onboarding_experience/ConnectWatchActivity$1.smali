.class Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;
.super Ljava/lang/Object;
.source "ConnectWatchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "paramView"    # Landroid/view/View;
    .param p3, "paramInt"    # I
    .param p4, "paramLong"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "paramAdapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 97
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    sget-object v5, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connecting:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    .line 99
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->scanLeDevice(Z)V
    invoke-static {v4, v6}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$000(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Z)V

    .line 101
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$200(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mRunnable:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$100(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 103
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    iget-object v4, v4, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v4, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;

    .line 105
    .local v0, "device":Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    .line 107
    .local v3, "name":Ljava/lang/String;
    if-eqz v3, :cond_1

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x53

    if-ne v4, v5, :cond_1

    .line 108
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    sget-object v5, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Square:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    # setter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;
    invoke-static {v4, v5}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$302(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;)Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    .line 109
    const-string v4, "square"

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-static {v4, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchShape(Ljava/lang/String;Landroid/content/Context;)V

    .line 116
    :goto_0
    const-string v4, "BOND"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bond state "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    new-instance v5, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$1;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$1;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;)V

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 124
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_2

    .line 128
    :try_start_0
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-static {v4}, Lcom/vectorwatch/android/utils/Helpers;->createBond(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_1
    new-instance v2, Landroid/content/IntentFilter;

    const-string v4, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v2, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 134
    .local v2, "filter":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    new-instance v5, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;

    invoke-direct {v5, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;)V

    # setter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v4, v5}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$502(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    .line 168
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v5}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$500(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/content/BroadcastReceiver;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 175
    .end local v2    # "filter":Landroid/content/IntentFilter;
    :cond_0
    :goto_2
    return-void

    .line 111
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    sget-object v5, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Round:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    # setter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mCurrentWatchShape:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;
    invoke-static {v4, v5}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$302(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;)Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    .line 112
    const-string v4, "round"

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-static {v4, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchShape(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 129
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 170
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v4

    const/16 v5, 0xc

    if-ne v4, v5, :cond_0

    .line 172
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->saveDeviceToSharedPreferences(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$600(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->triggerConnect()V
    invoke-static {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$700(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    goto :goto_2
.end method
