.class Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$5;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->initControls()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$5;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$5;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mLbInfo:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$100(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 242
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$5;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$200(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)Landroid/widget/ScrollView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$5;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerButtons:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$400(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$5;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerRegister:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$600(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$5;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mShortAnimationDuration:I
    invoke-static {v2}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$500(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/UIUtils;->crossFadePanels(Landroid/view/View;Landroid/view/View;I)V

    .line 244
    return-void
.end method
