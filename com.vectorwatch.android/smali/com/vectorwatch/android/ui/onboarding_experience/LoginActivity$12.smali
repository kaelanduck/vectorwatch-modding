.class Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->submitLogin()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/LoginResponseModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

.field final synthetic val$accountType:Ljava/lang/String;

.field final synthetic val$password:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 451
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->val$accountType:Ljava/lang/String;

    iput-object p3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->val$password:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 9
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    const v8, 0x7f090114

    const/16 v7, 0xbb8

    .line 477
    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$900()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "AUTHENTICATOR: Error at login."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 478
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 480
    .local v0, "data":Landroid/os/Bundle;
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v1

    .line 481
    .local v1, "response":Lretrofit/client/Response;
    if-eqz v1, :cond_1

    .line 482
    invoke-virtual {v1}, Lretrofit/client/Response;->getStatus()I

    move-result v2

    .line 483
    .local v2, "status":I
    invoke-static {v2}, Lcom/vectorwatch/android/utils/CloudStatusCodes;->getDefaultStringIdForStatus(I)I

    move-result v3

    .line 484
    .local v3, "statusCodeMessageResId":I
    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$900()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LOGIN: error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 485
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 486
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v4, v7, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 499
    .end local v2    # "status":I
    .end local v3    # "statusCodeMessageResId":I
    :goto_0
    return-void

    .line 490
    .restart local v2    # "status":I
    .restart local v3    # "statusCodeMessageResId":I
    :cond_0
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v4, v7, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 495
    .end local v2    # "status":I
    .end local v3    # "statusCodeMessageResId":I
    :cond_1
    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$900()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LOGIN: undef error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 496
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v4, v7, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0
.end method

.method public success(Lcom/vectorwatch/android/models/LoginResponseModel;Lretrofit/client/Response;)V
    .locals 5
    .param p1, "loginResponseModel"    # Lcom/vectorwatch/android/models/LoginResponseModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    const/4 v4, 0x0

    .line 455
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v2, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setOfflineModeStatus(Landroid/content/Context;Z)V

    .line 458
    const-string v2, "flag_send_parse_installation_id"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .line 459
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 458
    invoke-static {v2, v4, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 460
    const-string v2, "account_update_type"

    .line 461
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/LoginResponseModel;->getData()Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->getUpdateType()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .line 460
    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 463
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 465
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/LoginResponseModel;->getData()Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/LoginResponseModel$LoginResponseData;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const-string v2, "accountType"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->val$accountType:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v2, "authtoken"

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/LoginResponseModel;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-string v2, "USER_PASSWORD"

    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->val$password:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 470
    .local v1, "res":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 471
    const-string v2, "action_type"

    sget-object v3, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->LOGIN:Ljava/lang/Integer;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 472
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->finishLogin(Landroid/content/Intent;)V
    invoke-static {v2, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$800(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;Landroid/content/Intent;)V

    .line 473
    return-void
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 451
    check-cast p1, Lcom/vectorwatch/android/models/LoginResponseModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;->success(Lcom/vectorwatch/android/models/LoginResponseModel;Lretrofit/client/Response;)V

    return-void
.end method
