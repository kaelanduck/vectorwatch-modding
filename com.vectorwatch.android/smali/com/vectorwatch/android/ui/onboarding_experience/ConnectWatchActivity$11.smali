.class Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$11;
.super Ljava/lang/Object;
.source "ConnectWatchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onDeviceConnected()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 643
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->isNotificationListenerPermissionsEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->AllowNotifications:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    .line 651
    :goto_0
    return-void

    .line 649
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onNotificationsAllowed()V
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$1900(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    goto :goto_0
.end method
