.class Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$7;
.super Landroid/content/BroadcastReceiver;
.source "ConnectWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$7;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "paramContext"    # Landroid/content/Context;
    .param p2, "paramIntent"    # Landroid/content/Intent;

    .prologue
    .line 327
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$7;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 331
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$7;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    const v2, 0x7f09021d

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->setTitle(I)V

    .line 332
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$7;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$7;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 336
    :cond_0
    return-void
.end method
