.class Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$5;
.super Ljava/lang/Object;
.source "ConnectWatchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 279
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "http://support.vectorwatch.com/go/connection-troubleshooting"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 280
    .local v1, "myIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v2, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    .end local v1    # "myIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0901bf

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 283
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
