.class public Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "LoginActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final ACTION_TYPE:Ljava/lang/String; = "action_type"

.field public static final ARG_ACCOUNT_NAME:Ljava/lang/String; = "ACCOUNT_NAME"

.field public static final ARG_ACCOUNT_TYPE:Ljava/lang/String; = "ACCOUNT_TYPE"

.field public static final ARG_AUTH_TYPE:Ljava/lang/String; = "AUTH_TYPE"

.field public static final ARG_CONFIRM_CREDENTIALS:Ljava/lang/String; = "CONFIRM_CREDENTIALS"

.field public static final ARG_IS_ADDING_ACCOUNT:Ljava/lang/String; = "IS_ADDING_ACCOUNT"

.field public static final KEY_ERROR_MESSAGE:Ljava/lang/String; = "ERR_MSG"

.field public static final LOGIN:Ljava/lang/Integer;

.field public static final PARAM_USER_PASSWORD:Ljava/lang/String; = "USER_PASSWORD"

.field public static final SIGN_UP:Ljava/lang/Integer;

.field private static log:Lorg/slf4j/Logger;


# instance fields
.field loadingDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

.field private mAccountManager:Landroid/accounts/AccountManager;

.field private mAgreeToTerms:Landroid/widget/CheckBox;

.field private mAuthTokenType:Ljava/lang/String;

.field private mBtFbLogin:Landroid/widget/TextView;

.field private mBtLogin:Landroid/widget/TextView;

.field private mBtLoginCancel:Landroid/widget/TextView;

.field private mBtRecoverPass:Landroid/widget/TextView;

.field private mBtRegister:Landroid/widget/TextView;

.field private mBtRegisterCancel:Landroid/widget/TextView;

.field private mBtSkip:Landroid/widget/TextView;

.field private mBtVectorLogin:Landroid/widget/TextView;

.field private mBtVectorRegister:Landroid/widget/TextView;

.field private mContainerButtons:Landroid/widget/RelativeLayout;

.field private mContainerLogin:Landroid/widget/RelativeLayout;

.field private mContainerRegister:Landroid/widget/RelativeLayout;

.field private mIgnoreBoard:Z

.field private mImgFbLogin:Landroid/widget/ImageView;

.field private mLbInfo:Landroid/widget/TextView;

.field private mLegalTermsLink:Landroid/widget/TextView;

.field mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private mRegisterForUpdates:Landroid/widget/CheckBox;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mShortAnimationDuration:I

.field private mTxLoginPassword:Landroid/widget/EditText;

.field private mTxLoginUsername:Landroid/widget/EditText;

.field private mTxRegisterName:Landroid/widget/EditText;

.field private mTxRegisterPassword:Landroid/widget/EditText;

.field private mTxRegisterUsername:Landroid/widget/EditText;

.field private mVisiblePanel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->SIGN_UP:Ljava/lang/Integer;

    .line 59
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->LOGIN:Ljava/lang/Integer;

    .line 62
    const-class v0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->loadingDialog:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;

    .line 93
    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$1;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    return-void
.end method

.method static synthetic access$002(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mVisiblePanel:I

    return p1
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mLbInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerLogin:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerButtons:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 49
    iget v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mShortAnimationDuration:I

    return v0
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerRegister:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->finishLoginOffline()V

    return-void
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->finishLogin(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$900()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method private finishLogin(Landroid/content/Intent;)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    const/4 v9, -0x1

    const/4 v10, 0x1

    .line 590
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "finishLogin()"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 593
    const-string v7, "check_compatibility"

    invoke-static {v7, v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 595
    sput-boolean v11, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    .line 597
    const-string v7, "action_type"

    invoke-virtual {p1, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 599
    .local v3, "action":Ljava/lang/Integer;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v7, v9, :cond_1

    .line 600
    :cond_0
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "FinishLogin called with something different than login or sign up."

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 660
    :goto_0
    return-void

    .line 604
    :cond_1
    const-string v7, "authAccount"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 606
    .local v2, "accountUsername":Ljava/lang/String;
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->SIGN_UP:Ljava/lang/Integer;

    if-ne v3, v7, :cond_3

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 607
    :cond_2
    const v7, 0x7f090231

    invoke-virtual {p0, v7}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x7d0

    invoke-static {v7, v8, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 612
    :cond_3
    const-string v7, "USER_PASSWORD"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 613
    .local v1, "accountPassword":Ljava/lang/String;
    new-instance v0, Landroid/accounts/Account;

    const-string v7, "accountType"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v2, v7}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    .local v0, "account":Landroid/accounts/Account;
    const/4 v7, 0x5

    invoke-static {p0, v7}, Lcom/vectorwatch/android/utils/Helpers;->setCurrentUpdateState(Landroid/content/Context;I)V

    .line 618
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "IS_ADDING_ACCOUNT"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 619
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "finishLogin - addAccountExplicitly"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 620
    const-string v7, "authtoken"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 621
    .local v4, "authToken":Ljava/lang/String;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAuthTokenType:Ljava/lang/String;

    .line 624
    .local v5, "authTokenType":Ljava/lang/String;
    iget-object v7, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAccountManager:Landroid/accounts/AccountManager;

    const/4 v8, 0x0

    invoke-virtual {v7, v0, v1, v8}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v6

    .line 625
    .local v6, "success":Z
    if-eqz v6, :cond_4

    .line 626
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "Account added successfully"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 631
    :goto_1
    iget-object v7, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAccountManager:Landroid/accounts/AccountManager;

    invoke-virtual {v7, v0, v5, v4}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    .end local v4    # "authToken":Ljava/lang/String;
    .end local v5    # "authTokenType":Ljava/lang/String;
    .end local v6    # "success":Z
    :goto_2
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAccountAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 640
    invoke-static {v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLoggedInStatus(ZLandroid/content/Context;)V

    .line 641
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v7

    new-instance v8, Lcom/vectorwatch/android/events/LoginStateChangedEvent;

    invoke-direct {v8, v10}, Lcom/vectorwatch/android/events/LoginStateChangedEvent;-><init>(Z)V

    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 643
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f09019c

    invoke-virtual {p0, v9}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "authAccount"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 644
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->setAccountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 647
    const-string v7, "flag_sync_settings_from_cloud"

    invoke-static {v7, v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 651
    const-string v7, "flag_sync_system_apps"

    invoke-static {v7, v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 654
    const-string v7, "bonded_watch_dirty"

    invoke-static {v7, v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 657
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->mandatoryRequestsAtConnect(Landroid/content/Context;)V

    .line 659
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->goToMain()V

    goto/16 :goto_0

    .line 628
    .restart local v4    # "authToken":Ljava/lang/String;
    .restart local v5    # "authTokenType":Ljava/lang/String;
    .restart local v6    # "success":Z
    :cond_4
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "Account was not added"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_1

    .line 633
    .end local v4    # "authToken":Ljava/lang/String;
    .end local v5    # "authTokenType":Ljava/lang/String;
    .end local v6    # "success":Z
    :cond_5
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "finishLogin - setPassword"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 635
    iget-object v7, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAccountManager:Landroid/accounts/AccountManager;

    invoke-virtual {v7, v0, v1}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private finishLoginOffline()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 525
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "finishLogin()"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 526
    invoke-static {p0, v10}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setOfflineModeStatus(Landroid/content/Context;Z)V

    .line 529
    const-string v7, "check_compatibility"

    invoke-static {v7, v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 531
    sput-boolean v11, Lcom/vectorwatch/android/VectorApplication;->sSetupWatchActive:Z

    .line 533
    const-string v2, "offline@vectorwatch.com"

    .line 534
    .local v2, "accountUsername":Ljava/lang/String;
    const-string v1, "1234"

    .line 535
    .local v1, "accountPassword":Ljava/lang/String;
    new-instance v0, Landroid/accounts/Account;

    const-string v7, "com.vectorwatch.android"

    invoke-direct {v0, v2, v7}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    .local v0, "account":Landroid/accounts/Account;
    const/4 v7, 0x5

    invoke-static {p0, v7}, Lcom/vectorwatch/android/utils/Helpers;->setCurrentUpdateState(Landroid/content/Context;I)V

    .line 540
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "IS_ADDING_ACCOUNT"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 541
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "finishLogin - addAccountExplicitly"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 542
    const-string v3, ""

    .line 543
    .local v3, "authToken":Ljava/lang/String;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAuthTokenType:Ljava/lang/String;

    .line 546
    .local v4, "authTokenType":Ljava/lang/String;
    iget-object v7, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAccountManager:Landroid/accounts/AccountManager;

    const/4 v8, 0x0

    invoke-virtual {v7, v0, v1, v8}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v6

    .line 547
    .local v6, "success":Z
    if-eqz v6, :cond_0

    .line 548
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "Account added successfully"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 553
    :goto_0
    iget-object v7, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAccountManager:Landroid/accounts/AccountManager;

    invoke-virtual {v7, v0, v4, v3}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "authTokenType":Ljava/lang/String;
    .end local v6    # "success":Z
    :goto_1
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAccountAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 562
    invoke-static {v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLoggedInStatus(ZLandroid/content/Context;)V

    .line 563
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v7

    new-instance v8, Lcom/vectorwatch/android/events/LoginStateChangedEvent;

    invoke-direct {v8, v10}, Lcom/vectorwatch/android/events/LoginStateChangedEvent;-><init>(Z)V

    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 565
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 566
    .local v5, "offlineBundle":Landroid/os/Bundle;
    const-string v7, "accountType"

    const-string v8, "com.vectorwatch.android"

    invoke-virtual {v5, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    const-string v7, "authAccount"

    invoke-virtual {v5, v7, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    const-string v7, "authtoken"

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->setAccountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 571
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f09019c

    invoke-virtual {p0, v9}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const v9, 0x7f0901d5

    invoke-virtual {p0, v9}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 574
    const-string v7, "flag_sync_settings_from_cloud"

    invoke-static {v7, v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 578
    const-string v7, "flag_sync_system_apps"

    invoke-static {v7, v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 581
    const-string v7, "bonded_watch_dirty"

    invoke-static {v7, v10, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 584
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->mandatoryRequestsAtConnect(Landroid/content/Context;)V

    .line 586
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->goToMain()V

    .line 587
    return-void

    .line 550
    .end local v5    # "offlineBundle":Landroid/os/Bundle;
    .restart local v3    # "authToken":Ljava/lang/String;
    .restart local v4    # "authTokenType":Ljava/lang/String;
    .restart local v6    # "success":Z
    :cond_0
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "Account was not added"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 555
    .end local v3    # "authToken":Ljava/lang/String;
    .end local v4    # "authTokenType":Ljava/lang/String;
    .end local v6    # "success":Z
    :cond_1
    sget-object v7, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;

    const-string v8, "finishLogin - setPassword"

    invoke-interface {v7, v8}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 557
    iget-object v7, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAccountManager:Landroid/accounts/AccountManager;

    invoke-virtual {v7, v0, v1}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private goToMain()V
    .locals 3

    .prologue
    .line 663
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 664
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 665
    const-string v1, "ignore_board"

    iget-boolean v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mIgnoreBoard:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 666
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 667
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->finish()V

    .line 668
    return-void
.end method

.method private initControls()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 208
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerButtons:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerLogin:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 210
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerRegister:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtVectorLogin:Landroid/widget/TextView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$2;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$2;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtVectorRegister:Landroid/widget/TextView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$3;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$3;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtLoginCancel:Landroid/widget/TextView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$4;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$4;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegisterCancel:Landroid/widget/TextView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$5;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$5;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtLogin:Landroid/widget/TextView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$6;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$6;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegister:Landroid/widget/TextView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$7;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$7;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRecoverPass:Landroid/widget/TextView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$8;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$8;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mLegalTermsLink:Landroid/widget/TextView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$9;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$9;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtSkip:Landroid/widget/TextView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$10;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$10;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterPassword:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 290
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxLoginPassword:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 292
    return-void
.end method

.method private singUpCall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "accountType"    # Ljava/lang/String;

    .prologue
    .line 356
    const-string v1, "flag_send_parse_installation_id"

    const/4 v2, 0x1

    invoke-static {v1, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 359
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mRegisterForUpdates:Landroid/widget/CheckBox;

    .line 360
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 359
    invoke-static {p0, p1, p2, p3, v1}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->prepareUserDataForSignUp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/vectorwatch/android/models/UserModel;

    move-result-object v0

    .line 362
    .local v0, "userModel":Lcom/vectorwatch/android/models/UserModel;
    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;

    invoke-direct {v1, p0, p1, p4, p2}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->accountSignUp(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V

    .line 419
    return-void
.end method

.method private switchToOfflineDialog()V
    .locals 3

    .prologue
    .line 504
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090248

    .line 505
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    new-instance v2, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$14;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$14;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    .line 506
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    new-instance v2, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$13;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$13;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;)V

    .line 513
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 520
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 521
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 522
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 296
    iget v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mVisiblePanel:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 297
    iput v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mVisiblePanel:I

    .line 298
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerButtons:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerLogin:Landroid/widget/RelativeLayout;

    iget v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mShortAnimationDuration:I

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/UIUtils;->crossFadePanels(Landroid/view/View;Landroid/view/View;I)V

    .line 309
    :goto_0
    return-void

    .line 301
    :cond_0
    iget v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mVisiblePanel:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 302
    iput v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mVisiblePanel:I

    .line 303
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mLbInfo:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 304
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerButtons:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerRegister:Landroid/widget/RelativeLayout;

    iget v2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mShortAnimationDuration:I

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/UIUtils;->crossFadePanels(Landroid/view/View;Landroid/view/View;I)V

    goto :goto_0

    .line 307
    :cond_1
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 672
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 683
    :goto_0
    return-void

    .line 674
    :pswitch_0
    if-eqz p2, :cond_0

    .line 675
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegister:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 676
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegister:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegister:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 679
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegister:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 672
    :pswitch_data_0
    .packed-switch 0x7f100130
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 124
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 125
    const v1, 0x7f03002c

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->setContentView(I)V

    .line 127
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    const-string v1, "unpaired_from_phone_settings"

    invoke-static {v1, v3, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->goToMain()V

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ignore_board"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mIgnoreBoard:Z

    .line 135
    iput v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mVisiblePanel:I

    .line 137
    const v1, 0x7f100123

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtSkip:Landroid/widget/TextView;

    .line 138
    invoke-static {p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 139
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtSkip:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    :goto_0
    const v1, 0x7f100129

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRecoverPass:Landroid/widget/TextView;

    .line 146
    const v1, 0x7f1000bf

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mLbInfo:Landroid/widget/TextView;

    .line 148
    const v1, 0x7f10011c

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerButtons:Landroid/widget/RelativeLayout;

    .line 149
    const v1, 0x7f10011e

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtFbLogin:Landroid/widget/TextView;

    .line 150
    const v1, 0x7f100122

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtVectorLogin:Landroid/widget/TextView;

    .line 151
    const v1, 0x7f100120

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtVectorRegister:Landroid/widget/TextView;

    .line 152
    const v1, 0x7f10011d

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mImgFbLogin:Landroid/widget/ImageView;

    .line 154
    const v1, 0x7f100124

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerLogin:Landroid/widget/RelativeLayout;

    .line 155
    const v1, 0x7f100125

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxLoginUsername:Landroid/widget/EditText;

    .line 156
    const v1, 0x7f100126

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxLoginPassword:Landroid/widget/EditText;

    .line 158
    const v1, 0x7f100128

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtLogin:Landroid/widget/TextView;

    .line 159
    const v1, 0x7f100127

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtLoginCancel:Landroid/widget/TextView;

    .line 161
    const v1, 0x7f10012a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mContainerRegister:Landroid/widget/RelativeLayout;

    .line 162
    const v1, 0x7f10012b

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterName:Landroid/widget/EditText;

    .line 163
    const v1, 0x7f10012c

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterUsername:Landroid/widget/EditText;

    .line 164
    const v1, 0x7f10012d

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterPassword:Landroid/widget/EditText;

    .line 165
    const v1, 0x7f10012f

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegister:Landroid/widget/TextView;

    .line 166
    const v1, 0x7f10012e

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegisterCancel:Landroid/widget/TextView;

    .line 167
    const v1, 0x7f100073

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mScrollView:Landroid/widget/ScrollView;

    .line 168
    const v1, 0x7f100131

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mLegalTermsLink:Landroid/widget/TextView;

    .line 169
    const v1, 0x7f100130

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAgreeToTerms:Landroid/widget/CheckBox;

    .line 170
    const v1, 0x7f100132

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mRegisterForUpdates:Landroid/widget/CheckBox;

    .line 172
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mShortAnimationDuration:I

    .line 175
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAccountManager:Landroid/accounts/AccountManager;

    .line 177
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mRegisterForUpdates:Landroid/widget/CheckBox;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 179
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ACCOUNT_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "username":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "AUTH_TYPE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAuthTokenType:Ljava/lang/String;

    .line 181
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAuthTokenType:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 182
    const-string v1, "Full access"

    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAuthTokenType:Ljava/lang/String;

    .line 185
    :cond_1
    if-eqz v0, :cond_2

    .line 186
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxLoginUsername:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 190
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtFbLogin:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 191
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mImgFbLogin:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 192
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAgreeToTerms:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 193
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegister:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 194
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtRegister:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 196
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->initControls()V

    .line 197
    return-void

    .line 141
    .end local v0    # "username":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mBtSkip:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 201
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onDestroy()V

    .line 202
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxLoginPassword:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 203
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterPassword:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 204
    iput-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 205
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0}, Landroid/accounts/AccountAuthenticatorActivity;->onResume()V

    .line 116
    const-string v0, "unpaired_from_phone_settings"

    const/4 v1, 0x0

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->goToMain()V

    .line 120
    :cond_0
    return-void
.end method

.method public submitLogin()V
    .locals 9

    .prologue
    .line 422
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v6

    const-string v7, "prefs_cloud_status"

    const-class v8, Lcom/vectorwatch/android/models/CloudStatus;

    invoke-virtual {v6, v7, v8}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/CloudStatus;

    .line 425
    .local v2, "cloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    if-eqz v2, :cond_0

    .line 426
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->switchToOfflineDialog()V

    .line 501
    :goto_0
    return-void

    .line 430
    :cond_0
    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxLoginUsername:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxLoginPassword:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    if-nez v6, :cond_2

    .line 431
    :cond_1
    const v6, 0x7f0901ae

    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x5dc

    invoke-static {v6, v7, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 436
    :cond_2
    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxLoginUsername:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 438
    .local v5, "username":Ljava/lang/String;
    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxLoginPassword:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 440
    .local v3, "password":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "ACCOUNT_TYPE"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 441
    .local v0, "accType":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 442
    const-string v0, "com.vectorwatch.android"

    .line 445
    :cond_3
    move-object v1, v0

    .line 449
    .local v1, "accountType":Ljava/lang/String;
    invoke-static {p0, v5, v3}, Lcom/vectorwatch/android/cloud_communication/AccountHandler;->prepareUserDataForLogin(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/vectorwatch/android/models/UserModel;

    move-result-object v4

    .line 451
    .local v4, "userInfo":Lcom/vectorwatch/android/models/UserModel;
    new-instance v6, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;

    invoke-direct {v6, p0, v1, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$12;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v4, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->accountLogin(Lcom/vectorwatch/android/models/UserModel;Lretrofit/Callback;)V

    goto :goto_0
.end method

.method public submitRegister()V
    .locals 10

    .prologue
    const/16 v9, 0x5dc

    .line 312
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v6

    const-string v7, "prefs_cloud_status"

    const-class v8, Lcom/vectorwatch/android/models/CloudStatus;

    invoke-virtual {v6, v7, v8}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/CloudStatus;

    .line 315
    .local v2, "cloudStatus":Lcom/vectorwatch/android/models/CloudStatus;
    if-eqz v2, :cond_0

    .line 316
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->switchToOfflineDialog()V

    .line 352
    :goto_0
    return-void

    .line 320
    :cond_0
    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterName:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterUsername:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterPassword:Landroid/widget/EditText;

    .line 321
    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    if-nez v6, :cond_2

    .line 322
    :cond_1
    const v6, 0x7f0901af

    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v9, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 327
    :cond_2
    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterName:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 329
    .local v3, "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterUsername:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 331
    .local v5, "username":Ljava/lang/String;
    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mTxRegisterPassword:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 333
    .local v4, "password":Ljava/lang/String;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x6

    if-ge v6, v7, :cond_4

    .line 334
    :cond_3
    const v6, 0x7f0901df

    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v9, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 339
    :cond_4
    iget-object v6, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->mAgreeToTerms:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-nez v6, :cond_5

    .line 340
    const v6, 0x7f0900bf

    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v9, p0}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 345
    :cond_5
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "ACCOUNT_TYPE"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 346
    .local v0, "accType":Ljava/lang/String;
    if-nez v0, :cond_6

    .line 347
    const-string v0, "com.vectorwatch.android"

    .line 349
    :cond_6
    move-object v1, v0

    .line 351
    .local v1, "accountType":Ljava/lang/String;
    invoke-direct {p0, v5, v4, v3, v1}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->singUpCall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
