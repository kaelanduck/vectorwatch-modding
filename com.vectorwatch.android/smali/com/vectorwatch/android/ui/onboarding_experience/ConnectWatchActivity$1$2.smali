.class Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;
.super Landroid/content/BroadcastReceiver;
.source "ConnectWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, -0x1

    .line 137
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 138
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v3, "android.bluetooth.device.extra.BOND_STATE"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 139
    .local v0, "bondState":I
    const-string v3, "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 141
    .local v2, "previousBondState":I
    const-string v3, "BOND"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bond state changed for: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " new state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " previous: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/16 v3, 0xc

    if-ne v0, v3, :cond_0

    .line 145
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 150
    invoke-static {p1}, Lcom/vectorwatch/android/utils/Helpers;->unpairDrivenReset(Landroid/content/Context;)V

    .line 152
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->saveDeviceToSharedPreferences(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$600(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->triggerConnect()V
    invoke-static {v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$700(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)V

    .line 155
    :cond_0
    const/16 v3, 0xb

    if-ne v2, v3, :cond_1

    const/16 v3, 0xa

    if-ne v0, v3, :cond_1

    .line 157
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    sget-object v4, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Initial:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {v3, v4}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->refreshControls(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;)V

    .line 158
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-ge v3, v4, :cond_2

    .line 159
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$800(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    iget-object v4, v4, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09005e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    :goto_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mBtConnect:Landroid/widget/Button;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$900(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    iget-object v4, v4, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090083

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 165
    :cond_1
    return-void

    .line 161
    :cond_2
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    iget-object v3, v3, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->mLbSearching:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->access$800(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1$2;->this$1:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;

    iget-object v4, v4, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09005f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
