.class final enum Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;
.super Ljava/lang/Enum;
.source "ConnectWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "WatchShape"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

.field public static final enum Round:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

.field public static final enum Square:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 785
    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    const-string v1, "Round"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Round:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    const-string v1, "Square"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Square:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    .line 784
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Round:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->Square:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    aput-object v1, v0, v3

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->$VALUES:[Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 784
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 784
    const-class v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;
    .locals 1

    .prologue
    .line 784
    sget-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->$VALUES:[Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$WatchShape;

    return-object v0
.end method
