.class public Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "OnboardingActivity.java"


# instance fields
.field private animLogo:Landroid/view/animation/Animation;

.field private animStarted:Landroid/view/animation/Animation;

.field private animVideoIn:Landroid/view/animation/Animation;

.field private animVideoOut:Landroid/view/animation/Animation;

.field private animWelcome:Landroid/view/animation/Animation;

.field private animationListenerIn:Landroid/view/animation/Animation$AnimationListener;

.field private animationListenerOut:Landroid/view/animation/Animation$AnimationListener;

.field mBtnGetStarted:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100116
        }
    .end annotation
.end field

.field mFirstLayout:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100112
        }
    .end annotation
.end field

.field mImgLogo:Landroid/widget/ImageView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100114
        }
    .end annotation
.end field

.field private mSimpleExit:Z

.field mTxtWelcome:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100115
        }
    .end annotation
.end field

.field mVideoLayout:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100117
        }
    .end annotation
.end field

.field private mVideoState:I

.field mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100118
        }
    .end annotation
.end field

.field private onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private videoOut:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 47
    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$1;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 54
    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$2;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$2;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 60
    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$3;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animationListenerIn:Landroid/view/animation/Animation$AnimationListener;

    .line 102
    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animationListenerOut:Landroid/view/animation/Animation$AnimationListener;

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    .prologue
    .line 33
    iget v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    return v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    .prologue
    .line 33
    iget v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->videoOut:I

    return v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoIn:Landroid/view/animation/Animation;

    return-object v0
.end method

.method private goToLogin()V
    .locals 3

    .prologue
    .line 319
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 320
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 321
    const-string v1, "ignore_board"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 322
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->startActivity(Landroid/content/Intent;)V

    .line 323
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->finish()V

    .line 324
    return-void
.end method

.method private goToMain()V
    .locals 3

    .prologue
    .line 327
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/vectorwatch/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 328
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 329
    const-string v1, "ignore_board"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 330
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->startActivity(Landroid/content/Intent;)V

    .line 331
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->finish()V

    .line 332
    return-void
.end method

.method private hideSystemUI(Landroid/view/View;)V
    .locals 4
    .param p1, "mDecorView"    # Landroid/view/View;

    .prologue
    .line 276
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 278
    .local v0, "currentApiVersion":I
    const/16 v1, 0x1706

    .line 286
    .local v1, "flags":I
    const/16 v2, 0x13

    if-lt v0, v2, :cond_0

    .line 288
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x1706

    invoke-virtual {v2, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 293
    new-instance v2, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$5;

    invoke-direct {v2, p0, p1}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$5;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;Landroid/view/View;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 302
    :cond_0
    return-void
.end method

.method private needsLogin()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 306
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 307
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v4, "com.vectorwatch.android"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 309
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v4, v1

    if-ge v4, v2, :cond_0

    .line 310
    const-string v3, "Store-Apps:"

    const-string v4, "There should be at least one account in Accounts & sync"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :goto_0
    return v2

    .line 313
    :cond_0
    aget-object v4, v1, v3

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v4, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setAccountAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 314
    invoke-static {v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setLoggedInStatus(ZLandroid/content/Context;)V

    move v2, v3

    .line 315
    goto :goto_0
.end method


# virtual methods
.method public getStarted()V
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100116
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mFirstLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x10e0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/UIUtils;->crossFadePanels(Landroid/view/View;Landroid/view/View;I)V

    .line 206
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    .line 207
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->invalidate()V

    .line 208
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 209
    return-void
.end method

.method public next()V
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10011b
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 239
    iget v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    packed-switch v0, :pswitch_data_0

    .line 263
    :goto_0
    return-void

    .line 241
    :pswitch_0
    iput v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->videoOut:I

    .line 242
    iput v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    .line 243
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.resource://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0701de

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->setVideo(Landroid/net/Uri;I)V

    .line 244
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->clearAnimation()V

    .line 245
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 248
    :pswitch_1
    iput v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->videoOut:I

    .line 249
    const/4 v0, 0x2

    iput v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    .line 250
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.resource://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0701e0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->setVideo(Landroid/net/Uri;I)V

    .line 251
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->clearAnimation()V

    .line 252
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 255
    :pswitch_2
    const-string v0, "flag_show_onboarding"

    invoke-static {v0, v1, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 256
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mSimpleExit:Z

    if-nez v0, :cond_0

    .line 257
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->goToMain()V

    goto/16 :goto_0

    .line 259
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->finish()V

    goto/16 :goto_0

    .line 239
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 267
    iget v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 268
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onBackPressed()V

    .line 272
    :goto_0
    return-void

    .line 270
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->previous()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 161
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 163
    const v0, 0x7f03002b

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->setContentView(I)V

    .line 164
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 166
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "simple_exit"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mSimpleExit:Z

    .line 167
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 168
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->onErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 170
    const/4 v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    .line 172
    const v0, 0x7f040020

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animLogo:Landroid/view/animation/Animation;

    .line 173
    const v0, 0x7f040024

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animWelcome:Landroid/view/animation/Animation;

    .line 174
    const v0, 0x7f040021

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animStarted:Landroid/view/animation/Animation;

    .line 175
    const v0, 0x7f040022

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoIn:Landroid/view/animation/Animation;

    .line 176
    const v0, 0x7f040023

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoOut:Landroid/view/animation/Animation;

    .line 178
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoIn:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animationListenerIn:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 179
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoOut:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animationListenerOut:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 181
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.resource://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0701df

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->setVideo(Landroid/net/Uri;I)V

    .line 183
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mImgLogo:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animLogo:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 184
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mBtnGetStarted:Landroid/widget/Button;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animStarted:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->startAnimation(Landroid/view/animation/Animation;)V

    .line 185
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mTxtWelcome:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animWelcome:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 187
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setAlpha(F)V

    .line 189
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setBackgroundResource(I)V

    .line 191
    sget-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_ONBOARDING:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {p0, v0, v1, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    .line 194
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onResume()V

    .line 199
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->hideSystemUI(Landroid/view/View;)V

    .line 200
    return-void
.end method

.method public previous()V
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10011a
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 213
    iget v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    packed-switch v0, :pswitch_data_0

    .line 235
    :goto_0
    return-void

    .line 215
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mFirstLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x10e0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/UIUtils;->crossFadePanels(Landroid/view/View;Landroid/view/View;I)V

    .line 217
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->pause()V

    .line 218
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v4}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    .line 219
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->setVisibility(I)V

    .line 220
    const/4 v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    goto :goto_0

    .line 223
    :pswitch_1
    iput v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->videoOut:I

    .line 224
    iput v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    .line 225
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 226
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.resource://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0701df

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/malmstein/fenster/view/FensterVideoView;->setVideo(Landroid/net/Uri;I)V

    goto :goto_0

    .line 229
    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->videoOut:I

    .line 230
    iput v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I

    .line 231
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.resource://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0701de

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/malmstein/fenster/view/FensterVideoView;->setVideo(Landroid/net/Uri;I)V

    .line 232
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
