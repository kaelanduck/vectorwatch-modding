.class public Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "LegalTermsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$WebViewClient;
    }
.end annotation


# instance fields
.field private webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 31
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const v0, 0x7f030081

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;->setContentView(I)V

    .line 24
    const v0, 0x7f100234

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;->webView:Landroid/webkit/WebView;

    .line 25
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 26
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;->webView:Landroid/webkit/WebView;

    new-instance v1, Landroid/webkit/WebChromeClient;

    invoke-direct {v1}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 27
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;->webView:Landroid/webkit/WebView;

    new-instance v1, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$WebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$WebViewClient;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 28
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;->webView:Landroid/webkit/WebView;

    invoke-static {}, Lcom/vectorwatch/android/cloud_communication/UrlConfig;->getLegalTermsUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 29
    return-void
.end method
