.class Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;
.super Landroid/widget/BaseAdapter;
.source "ConnectWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceAdapter"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field devices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;",
            ">;"
        }
    .end annotation
.end field

.field inflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;Landroid/app/Activity;Ljava/util/List;)V
    .locals 1
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
    .param p2, "deviceListActivity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 793
    .local p3, "devices":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 794
    iput-object p2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;->context:Landroid/content/Context;

    .line 795
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 796
    iput-object p3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;->devices:Ljava/util/List;

    .line 797
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;->devices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "paramInt"    # I

    .prologue
    .line 804
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;->devices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "paramInt"    # I

    .prologue
    .line 808
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "paramInt"    # I
    .param p2, "paramView"    # Landroid/view/View;
    .param p3, "paramViewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 812
    const/4 v1, 0x0

    .line 813
    .local v1, "localViewGroup":Landroid/view/ViewGroup;
    if-eqz p2, :cond_0

    move-object v1, p2

    .line 814
    check-cast v1, Landroid/view/ViewGroup;

    .line 818
    :goto_0
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;->devices:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;

    .line 819
    .local v0, "localBluetoothDevice":Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;
    const v4, 0x7f100133

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 820
    .local v3, "textViewName":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;->getName()Ljava/lang/String;

    move-result-object v2

    .line 821
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 822
    return-object v1

    .line 816
    .end local v0    # "localBluetoothDevice":Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$BluetoothDeviceCustom;
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "textViewName":Landroid/widget/TextView;
    :cond_0
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$DeviceAdapter;->inflater:Landroid/view/LayoutInflater;

    const v5, 0x7f030082

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .end local v1    # "localViewGroup":Landroid/view/ViewGroup;
    check-cast v1, Landroid/view/ViewGroup;

    .restart local v1    # "localViewGroup":Landroid/view/ViewGroup;
    goto :goto_0
.end method
