.class Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Lretrofit/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->singUpCall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lretrofit/Callback",
        "<",
        "Lcom/vectorwatch/android/models/ServerResponseModel;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

.field final synthetic val$accountType:Ljava/lang/String;

.field final synthetic val$password:Ljava/lang/String;

.field final synthetic val$username:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .prologue
    .line 362
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->val$username:Ljava/lang/String;

    iput-object p3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->val$accountType:Ljava/lang/String;

    iput-object p4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->val$password:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failure(Lretrofit/RetrofitError;)V
    .locals 9
    .param p1, "error"    # Lretrofit/RetrofitError;

    .prologue
    const v8, 0x7f090114

    const/16 v7, 0xbb8

    .line 395
    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$900()Lorg/slf4j/Logger;

    move-result-object v4

    const-string v5, "SIGNUP: Error at signup."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 396
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 398
    .local v0, "data":Landroid/os/Bundle;
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getResponse()Lretrofit/client/Response;

    move-result-object v1

    .line 399
    .local v1, "response":Lretrofit/client/Response;
    if-eqz v1, :cond_1

    .line 400
    invoke-virtual {v1}, Lretrofit/client/Response;->getStatus()I

    move-result v2

    .line 401
    .local v2, "status":I
    invoke-static {v2}, Lcom/vectorwatch/android/utils/CloudStatusCodes;->getDefaultStringIdForStatus(I)I

    move-result v3

    .line 402
    .local v3, "statusCodeMessageResId":I
    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$900()Lorg/slf4j/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SIGNUP: error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 404
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 405
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v4, v7, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 417
    .end local v2    # "status":I
    .end local v3    # "statusCodeMessageResId":I
    :goto_0
    return-void

    .line 409
    .restart local v2    # "status":I
    .restart local v3    # "statusCodeMessageResId":I
    :cond_0
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v4, v7, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 414
    .end local v2    # "status":I
    .end local v3    # "statusCodeMessageResId":I
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v4, v7, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0
.end method

.method public success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V
    .locals 6
    .param p1, "serverResponseModel"    # Lcom/vectorwatch/android/models/ServerResponseModel;
    .param p2, "response"    # Lretrofit/client/Response;

    .prologue
    .line 365
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/ServerResponseModel;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 367
    .local v0, "authToken":Ljava/lang/String;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 369
    .local v1, "data":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 370
    const-string v3, "authAccount"

    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->val$username:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v3, "accountType"

    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->val$accountType:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const-string v3, "authtoken"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v3, "USER_PASSWORD"

    iget-object v4, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->val$password:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v3, "flag_send_parse_installation_id"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .line 377
    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 376
    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 380
    const-string v3, "flag_sync_system_apps"

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    .line 381
    invoke-virtual {v5}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 380
    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 383
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 384
    .local v2, "res":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 385
    const-string v3, "action_type"

    sget-object v4, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->SIGN_UP:Ljava/lang/Integer;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 386
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    # invokes: Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->finishLogin(Landroid/content/Intent;)V
    invoke-static {v3, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->access$800(Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;Landroid/content/Intent;)V

    .line 391
    .end local v2    # "res":Landroid/content/Intent;
    :goto_0
    return-void

    .line 388
    :cond_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090144

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xbb8

    iget-object v5, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity;

    invoke-static {v3, v4, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0
.end method

.method public bridge synthetic success(Ljava/lang/Object;Lretrofit/client/Response;)V
    .locals 0

    .prologue
    .line 362
    check-cast p1, Lcom/vectorwatch/android/models/ServerResponseModel;

    invoke-virtual {p0, p1, p2}, Lcom/vectorwatch/android/ui/onboarding_experience/LoginActivity$11;->success(Lcom/vectorwatch/android/models/ServerResponseModel;Lretrofit/client/Response;)V

    return-void
.end method
