.class Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;
.super Ljava/lang/Object;
.source "OnboardingActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 5
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 120
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoState:I
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->access$000(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 134
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->videoOut:I
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->access$100(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 151
    :goto_1
    return-void

    .line 122
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->clearAnimation()V

    .line 123
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoIn:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->access$200(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 126
    :pswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->clearAnimation()V

    .line 127
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoIn:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->access$200(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 130
    :pswitch_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->clearAnimation()V

    .line 131
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->animVideoIn:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->access$200(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/malmstein/fenster/view/FensterVideoView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 136
    :pswitch_3
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v4}, Lcom/malmstein/fenster/view/FensterVideoView;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setAlpha(F)V

    .line 138
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    goto :goto_1

    .line 141
    :pswitch_4
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v4}, Lcom/malmstein/fenster/view/FensterVideoView;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setAlpha(F)V

    .line 143
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    goto :goto_1

    .line 146
    :pswitch_5
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v4}, Lcom/malmstein/fenster/view/FensterVideoView;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v2}, Lcom/malmstein/fenster/view/FensterVideoView;->setAlpha(F)V

    .line 148
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0, v3}, Lcom/malmstein/fenster/view/FensterVideoView;->seekTo(I)V

    goto/16 :goto_1

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 134
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 156
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    # getter for: Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->videoOut:I
    invoke-static {v0}, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->access$100(Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 116
    :goto_0
    return-void

    .line 107
    :pswitch_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->pause()V

    goto :goto_0

    .line 110
    :pswitch_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->pause()V

    goto :goto_0

    .line 113
    :pswitch_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity$4;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/onboarding_experience/OnboardingActivity;->mVideoView:Lcom/malmstein/fenster/view/FensterVideoView;

    invoke-virtual {v0}, Lcom/malmstein/fenster/view/FensterVideoView;->pause()V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
