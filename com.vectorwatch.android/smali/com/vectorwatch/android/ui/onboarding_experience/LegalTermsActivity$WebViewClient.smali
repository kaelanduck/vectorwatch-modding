.class Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$WebViewClient;
.super Landroid/webkit/WebViewClient;
.source "LegalTermsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;


# direct methods
.method private constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$WebViewClient;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;
    .param p2, "x1"    # Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$1;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$WebViewClient;-><init>(Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;)V

    return-void
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity$WebViewClient;->this$0:Lcom/vectorwatch/android/ui/onboarding_experience/LegalTermsActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Oh no! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 35
    return-void
.end method
