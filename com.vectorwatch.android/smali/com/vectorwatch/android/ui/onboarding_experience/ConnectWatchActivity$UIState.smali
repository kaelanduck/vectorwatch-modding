.class final enum Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;
.super Ljava/lang/Enum;
.source "ConnectWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "UIState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

.field public static final enum AllowNotifications:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

.field public static final enum Connected:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

.field public static final enum Connecting:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

.field public static final enum Initial:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

.field public static final enum Scanning:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

.field public static final enum UnpairedFromSettings:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 781
    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    const-string v1, "Initial"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Initial:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    const-string v1, "Scanning"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Scanning:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    const-string v1, "Connecting"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connecting:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    const-string v1, "Connected"

    invoke-direct {v0, v1, v6}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connected:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    const-string v1, "AllowNotifications"

    invoke-direct {v0, v1, v7}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->AllowNotifications:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    new-instance v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    const-string v1, "UnpairedFromSettings"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->UnpairedFromSettings:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    .line 780
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Initial:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Scanning:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connecting:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->Connected:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->AllowNotifications:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->UnpairedFromSettings:Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->$VALUES:[Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 780
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 780
    const-class v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;
    .locals 1

    .prologue
    .line 780
    sget-object v0, Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->$VALUES:[Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/ui/onboarding_experience/ConnectWatchActivity$UIState;

    return-object v0
.end method
