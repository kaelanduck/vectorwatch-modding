.class Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;
.super Landroid/widget/BaseAdapter;
.source "SearchForWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/SearchForWatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceAdapter"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field devices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;",
            ">;"
        }
    .end annotation
.end field

.field inflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Landroid/app/Activity;Ljava/util/List;)V
    .locals 1
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;
    .param p2, "deviceListActivity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 304
    .local p3, "devices":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 305
    iput-object p2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;->context:Landroid/content/Context;

    .line 306
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 307
    iput-object p3, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;->devices:Ljava/util/List;

    .line 308
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;->devices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "paramInt"    # I

    .prologue
    .line 315
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;->devices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "paramInt"    # I

    .prologue
    .line 319
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "paramInt"    # I
    .param p2, "paramView"    # Landroid/view/View;
    .param p3, "paramViewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 323
    const/4 v2, 0x0

    .line 324
    .local v2, "localViewGroup":Landroid/view/ViewGroup;
    if-eqz p2, :cond_0

    move-object v2, p2

    .line 325
    check-cast v2, Landroid/view/ViewGroup;

    .line 329
    :goto_0
    iget-object v6, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;->devices:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;

    .line 330
    .local v1, "localBluetoothDevice":Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;
    const v6, 0x7f1001a6

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 331
    .local v4, "textViewAddress":Landroid/widget/TextView;
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 332
    .local v0, "address":Ljava/lang/String;
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    const v6, 0x7f100133

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 334
    .local v5, "textViewName":Landroid/widget/TextView;
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getName()Ljava/lang/String;

    move-result-object v3

    .line 335
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    return-object v2

    .line 327
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "localBluetoothDevice":Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "textViewAddress":Landroid/widget/TextView;
    .end local v5    # "textViewName":Landroid/widget/TextView;
    :cond_0
    iget-object v6, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;->inflater:Landroid/view/LayoutInflater;

    const v7, 0x7f03004e

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .end local v2    # "localViewGroup":Landroid/view/ViewGroup;
    check-cast v2, Landroid/view/ViewGroup;

    .restart local v2    # "localViewGroup":Landroid/view/ViewGroup;
    goto :goto_0
.end method
