.class public Lcom/vectorwatch/android/ui/helper/OrientationManager;
.super Landroid/view/OrientationEventListener;
.source "OrientationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;,
        Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    }
.end annotation


# instance fields
.field private listener:Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;

.field public screenOrientation:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rate"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rate"    # I
    .param p3, "listener"    # Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    .line 22
    invoke-virtual {p0, p3}, Lcom/vectorwatch/android/ui/helper/OrientationManager;->setListener(Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;)V

    .line 23
    return-void
.end method


# virtual methods
.method public getScreenOrientation()Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vectorwatch/android/ui/helper/OrientationManager;->screenOrientation:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    return-object v0
.end method

.method public onOrientationChanged(I)V
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    const/16 v3, 0xdc

    const/16 v2, 0x8c

    .line 35
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    const/16 v1, 0x3c

    if-lt p1, v1, :cond_2

    if-gt p1, v2, :cond_2

    .line 40
    sget-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->REVERSED_LANDSCAPE:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    .line 48
    .local v0, "newOrientation":Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    :goto_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/helper/OrientationManager;->screenOrientation:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    if-eq v0, v1, :cond_0

    .line 49
    iput-object v0, p0, Lcom/vectorwatch/android/ui/helper/OrientationManager;->screenOrientation:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    .line 50
    iget-object v1, p0, Lcom/vectorwatch/android/ui/helper/OrientationManager;->listener:Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/vectorwatch/android/ui/helper/OrientationManager;->listener:Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/helper/OrientationManager;->screenOrientation:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    invoke-interface {v1, v2}, Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;->onOrientationChange(Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;)V

    goto :goto_0

    .line 41
    .end local v0    # "newOrientation":Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    :cond_2
    if-lt p1, v2, :cond_3

    if-gt p1, v3, :cond_3

    .line 42
    sget-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->REVERSED_PORTRAIT:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    .restart local v0    # "newOrientation":Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    goto :goto_1

    .line 43
    .end local v0    # "newOrientation":Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    :cond_3
    if-lt p1, v3, :cond_4

    const/16 v1, 0x12c

    if-gt p1, v1, :cond_4

    .line 44
    sget-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->LANDSCAPE:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    .restart local v0    # "newOrientation":Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    goto :goto_1

    .line 46
    .end local v0    # "newOrientation":Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    :cond_4
    sget-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->PORTRAIT:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    .restart local v0    # "newOrientation":Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    goto :goto_1
.end method

.method public setListener(Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/vectorwatch/android/ui/helper/OrientationManager;->listener:Lcom/vectorwatch/android/ui/helper/OrientationManager$OrientationListener;

    .line 58
    return-void
.end method
