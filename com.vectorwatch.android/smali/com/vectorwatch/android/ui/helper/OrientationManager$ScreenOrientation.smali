.class public final enum Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
.super Ljava/lang/Enum;
.source "OrientationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/helper/OrientationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScreenOrientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

.field public static final enum LANDSCAPE:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

.field public static final enum PORTRAIT:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

.field public static final enum REVERSED_LANDSCAPE:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

.field public static final enum REVERSED_PORTRAIT:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    const-string v1, "REVERSED_LANDSCAPE"

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->REVERSED_LANDSCAPE:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    new-instance v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    const-string v1, "LANDSCAPE"

    invoke-direct {v0, v1, v3}, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->LANDSCAPE:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    new-instance v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    const-string v1, "PORTRAIT"

    invoke-direct {v0, v1, v4}, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->PORTRAIT:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    new-instance v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    const-string v1, "REVERSED_PORTRAIT"

    invoke-direct {v0, v1, v5}, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->REVERSED_PORTRAIT:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    .line 13
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    sget-object v1, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->REVERSED_LANDSCAPE:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->LANDSCAPE:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->PORTRAIT:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->REVERSED_PORTRAIT:Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    aput-object v1, v0, v5

    sput-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->$VALUES:[Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->$VALUES:[Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/ui/helper/OrientationManager$ScreenOrientation;

    return-object v0
.end method
