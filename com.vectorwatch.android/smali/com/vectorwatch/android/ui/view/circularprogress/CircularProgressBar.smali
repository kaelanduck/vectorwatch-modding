.class public Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
.super Landroid/widget/ProgressBar;
.source "CircularProgressBar.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    const v0, 0x7f010059

    invoke-direct {p0, p1, p2, v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct/range {p0 .. p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->isInEditMode()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 29
    new-instance v17, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;-><init>(Landroid/content/Context;Z)V

    invoke-virtual/range {v17 .. v17}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->build()Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    :goto_0
    return-void

    .line 33
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 34
    .local v13, "res":Landroid/content/res/Resources;
    sget-object v17, Lcom/vectorwatch/android/R$styleable;->CircularProgressBar:[I

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    move/from16 v3, p3

    move/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 37
    .local v5, "a":Landroid/content/res/TypedArray;
    const/16 v17, 0x1

    const v18, 0x7f0f0032

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    .line 38
    .local v7, "color":I
    const/16 v17, 0x3

    const v18, 0x7f0b0065

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v15

    .line 39
    .local v15, "strokeWidth":F
    const/16 v17, 0x6

    const v18, 0x7f0902a0

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v16

    .line 40
    .local v16, "sweepSpeed":F
    const/16 v17, 0x7

    const v18, 0x7f09029f

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v14

    .line 41
    .local v14, "rotationSpeed":F
    const/16 v17, 0x2

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    .line 42
    .local v9, "colorsId":I
    const/16 v17, 0x4

    const v18, 0x7f0e0006

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v12

    .line 43
    .local v12, "minSweepAngle":I
    const/16 v17, 0x5

    const v18, 0x7f0e0005

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v11

    .line 44
    .local v11, "maxSweepAngle":I
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 46
    const/4 v8, 0x0

    .line 48
    .local v8, "colors":[I
    if-eqz v9, :cond_1

    .line 49
    invoke-virtual {v13, v9}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v8

    .line 53
    :cond_1
    new-instance v17, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;-><init>(Landroid/content/Context;)V

    .line 54
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->sweepSpeed(F)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;

    move-result-object v17

    .line 55
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->rotationSpeed(F)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;

    move-result-object v17

    .line 56
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->strokeWidth(F)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;

    move-result-object v17

    .line 57
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->minSweepAngle(I)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;

    move-result-object v17

    .line 58
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->maxSweepAngle(I)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;

    move-result-object v6

    .line 60
    .local v6, "builder":Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;
    if-eqz v8, :cond_2

    array-length v0, v8

    move/from16 v17, v0

    if-lez v17, :cond_2

    .line 61
    invoke-virtual {v6, v8}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->colors([I)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;

    .line 65
    :goto_1
    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->build()Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    move-result-object v10

    .line 66
    .local v10, "indeterminateDrawable":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 63
    .end local v10    # "indeterminateDrawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    invoke-virtual {v6, v7}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;->color(I)Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$Builder;

    goto :goto_1
.end method

.method private checkIndeterminateDrawable()Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;
    .locals 3

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 71
    .local v0, "ret":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    if-nez v1, :cond_1

    .line 72
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "The drawable is not a CircularProgressDrawable"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 73
    :cond_1
    check-cast v0, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    .end local v0    # "ret":Landroid/graphics/drawable/Drawable;
    return-object v0
.end method


# virtual methods
.method public progressiveStop()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->checkIndeterminateDrawable()Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->progressiveStop()V

    .line 78
    return-void
.end method

.method public progressiveStop(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->checkIndeterminateDrawable()Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable;->progressiveStop(Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressDrawable$OnEndListener;)V

    .line 82
    return-void
.end method
