.class public Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;
.super Landroid/widget/LinearLayout;
.source "ProperRatingBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;
    }
.end annotation


# static fields
.field private static final DF_CLICKABLE:Z = false

.field private static final DF_DEFAULT_TICKS:I = 0x3

.field private static final DF_SYMBOLIC_TEXT_NORMAL_COLOR:I = -0x1000000

.field private static final DF_SYMBOLIC_TEXT_SELECTED_COLOR:I = -0x777778

.field private static final DF_SYMBOLIC_TEXT_SIZE_RES:I = 0x7f0b00af

.field private static final DF_SYMBOLIC_TEXT_STYLE:I = 0x0

.field private static final DF_SYMBOLIC_TICK_RES:I = 0x7f0901ea

.field private static final DF_TICK_SPACING_RES:I = 0x7f0b00ae

.field private static final DF_TOTAL_TICKS:I = 0x5


# instance fields
.field private customTextNormalColor:I

.field private customTextSelectedColor:I

.field private customTextSize:I

.field private customTextStyle:I

.field private isClickable:Z

.field private lastSelectedTickIndex:I

.field private listener:Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;

.field private mTickClickedListener:Landroid/view/View$OnClickListener;

.field private rating:I

.field private symbolicTick:Ljava/lang/String;

.field private tickNormalDrawable:Landroid/graphics/drawable/Drawable;

.field private tickSelectedDrawable:Landroid/graphics/drawable/Drawable;

.field private tickSpacing:I

.field private totalTicks:I

.field private useSymbolicTick:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->useSymbolicTick:Z

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->listener:Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;

    .line 157
    new-instance v0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$1;-><init>(Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->mTickClickedListener:Landroid/view/View$OnClickListener;

    .line 74
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    .prologue
    .line 44
    iget v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->lastSelectedTickIndex:I

    return v0
.end method

.method static synthetic access$002(Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->lastSelectedTickIndex:I

    return p1
.end method

.method static synthetic access$102(Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->rating:I

    return p1
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->redrawChildren()V

    return-void
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;)Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->listener:Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;

    return-object v0
.end method

.method private addChild(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "position"    # I

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->useSymbolicTick:Z

    if-eqz v0, :cond_0

    .line 127
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->addSymbolicChild(Landroid/content/Context;I)V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->addDrawableChild(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private addChildren(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->removeAllViews()V

    .line 119
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->totalTicks:I

    if-ge v0, v1, :cond_0

    .line 120
    invoke-direct {p0, p1, v0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->addChild(Landroid/content/Context;I)V

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    :cond_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->redrawChildren()V

    .line 123
    return-void
.end method

.method private addDrawableChild(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "position"    # I

    .prologue
    .line 148
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 149
    .local v0, "iv":Landroid/widget/ImageView;
    iget v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickSpacing:I

    iget v2, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickSpacing:I

    iget v3, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickSpacing:I

    iget v4, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickSpacing:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 150
    iget-boolean v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->isClickable:Z

    if-eqz v1, :cond_0

    .line 151
    const v1, 0x7f100008

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 152
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->mTickClickedListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    :cond_0
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->addView(Landroid/view/View;)V

    .line 155
    return-void
.end method

.method private addSymbolicChild(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "position"    # I

    .prologue
    .line 134
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 135
    .local v0, "tv":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->symbolicTick:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    const/4 v1, 0x0

    iget v2, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->customTextSize:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 137
    iget v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->customTextStyle:I

    if-eqz v1, :cond_0

    .line 138
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iget v2, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->customTextStyle:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 140
    :cond_0
    iget-boolean v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->isClickable:Z

    if-eqz v1, :cond_1

    .line 141
    const v1, 0x7f100008

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 142
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->mTickClickedListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    :cond_1
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->addView(Landroid/view/View;)V

    .line 145
    return-void
.end method

.method private afterInit()V
    .locals 2

    .prologue
    .line 107
    iget v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->rating:I

    iget v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->totalTicks:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->totalTicks:I

    iput v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->rating:I

    .line 108
    :cond_0
    iget v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->rating:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->lastSelectedTickIndex:I

    .line 110
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickNormalDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickSelectedDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 111
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->useSymbolicTick:Z

    .line 114
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->addChildren(Landroid/content/Context;)V

    .line 115
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x5

    const/4 v2, 0x3

    const/4 v3, 0x0

    .line 78
    sget-object v1, Lcom/vectorwatch/android/R$styleable;->ProperRatingBar:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 80
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->totalTicks:I

    .line 81
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->rating:I

    .line 83
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->isClickable:Z

    .line 85
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->symbolicTick:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->symbolicTick:Ljava/lang/String;

    if-nez v1, :cond_0

    const v1, 0x7f0901ea

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->symbolicTick:Ljava/lang/String;

    .line 89
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00af

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 88
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->customTextSize:I

    .line 90
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->customTextStyle:I

    .line 91
    const/4 v1, 0x6

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->customTextNormalColor:I

    .line 93
    const/4 v1, 0x7

    const v2, -0x777778

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->customTextSelectedColor:I

    .line 96
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickNormalDrawable:Landroid/graphics/drawable/Drawable;

    .line 97
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickSelectedDrawable:Landroid/graphics/drawable/Drawable;

    .line 98
    const/16 v1, 0xa

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 98
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickSpacing:I

    .line 101
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->afterInit()V

    .line 103
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 104
    return-void
.end method

.method private redrawChildSelection(Landroid/widget/ImageView;Z)V
    .locals 1
    .param p1, "child"    # Landroid/widget/ImageView;
    .param p2, "isSelected"    # Z

    .prologue
    .line 178
    if-eqz p2, :cond_0

    .line 179
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickSelectedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->tickNormalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private redrawChildSelection(Landroid/widget/TextView;Z)V
    .locals 1
    .param p1, "child"    # Landroid/widget/TextView;
    .param p2, "isSelected"    # Z

    .prologue
    .line 186
    if-eqz p2, :cond_0

    .line 187
    iget v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->customTextSelectedColor:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 191
    :goto_0
    return-void

    .line 189
    :cond_0
    iget v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->customTextNormalColor:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private redrawChildren()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 168
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->totalTicks:I

    if-ge v0, v1, :cond_3

    .line 169
    iget-boolean v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->useSymbolicTick:Z

    if-eqz v1, :cond_1

    .line 170
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v2, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->lastSelectedTickIndex:I

    if-gt v0, v2, :cond_0

    move v2, v3

    :goto_1
    invoke-direct {p0, v1, v2}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->redrawChildSelection(Landroid/widget/TextView;Z)V

    .line 168
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v4

    .line 170
    goto :goto_1

    .line 172
    :cond_1
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v2, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->lastSelectedTickIndex:I

    if-gt v0, v2, :cond_2

    move v2, v3

    :goto_3
    invoke-direct {p0, v1, v2}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->redrawChildSelection(Landroid/widget/ImageView;Z)V

    goto :goto_2

    :cond_2
    move v2, v4

    goto :goto_3

    .line 175
    :cond_3
    return-void
.end method


# virtual methods
.method public getListener()Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->listener:Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;

    return-object v0
.end method

.method public getRating()I
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->rating:I

    return v0
.end method

.method public getSymbolicTick()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->symbolicTick:Ljava/lang/String;

    return-object v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 207
    instance-of v1, p1, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;

    if-nez v1, :cond_0

    .line 208
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 216
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 212
    check-cast v0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;

    .line 213
    .local v0, "savedState":Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 215
    iget v1, v0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;->rating:I

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->setRating(I)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 199
    new-instance v0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 200
    .local v0, "savedState":Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;
    iget v1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->rating:I

    iput v1, v0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar$SavedState;->rating:I

    .line 202
    return-object v0
.end method

.method public removeRatingListener()V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->listener:Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;

    .line 279
    return-void
.end method

.method public setListener(Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;

    .prologue
    .line 268
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :cond_0
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->listener:Lcom/vectorwatch/android/ui/view/ratingbar/RatingListener;

    .line 271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->isClickable:Z

    .line 272
    return-void
.end method

.method public setRating(I)V
    .locals 1
    .param p1, "rating"    # I

    .prologue
    .line 294
    iget v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->totalTicks:I

    if-le p1, v0, :cond_0

    iget p1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->totalTicks:I

    .line 295
    :cond_0
    iput p1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->rating:I

    .line 296
    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->lastSelectedTickIndex:I

    .line 297
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->redrawChildren()V

    .line 298
    return-void
.end method

.method public setSymbolicTick(Ljava/lang/String;)V
    .locals 0
    .param p1, "tick"    # Ljava/lang/String;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->symbolicTick:Ljava/lang/String;

    .line 302
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/ratingbar/ProperRatingBar;->afterInit()V

    .line 303
    return-void
.end method
