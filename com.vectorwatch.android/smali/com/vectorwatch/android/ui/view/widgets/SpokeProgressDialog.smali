.class public Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;
.super Landroid/app/Dialog;
.source "SpokeProgressDialog.java"


# instance fields
.field private final mProgressView:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->requestWindowFeature(I)Z

    .line 22
    const v0, 0x7f030099

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->setContentView(I)V

    .line 23
    const v0, 0x7f1000c2

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->mProgressView:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;

    .line 25
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 26
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->mProgressView:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->dismiss()V

    .line 37
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 38
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 31
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressDialog;->mProgressView:Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->show()V

    .line 32
    return-void
.end method
