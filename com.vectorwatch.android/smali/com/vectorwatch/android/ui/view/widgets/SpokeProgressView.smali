.class public Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;
.super Lcom/vectorwatch/android/ui/view/widgets/SpokeView;
.source "SpokeProgressView.java"


# static fields
.field private static final PROGRESS_ONE_ROUND:J = 0x3e8L


# instance fields
.field private animateRunnable:Ljava/lang/Runnable;

.field final handler:Landroid/os/Handler;

.field private mProgressStep:F

.field private progressStartTime:J

.field private running:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;-><init>(Landroid/content/Context;)V

    .line 87
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->handler:Landroid/os/Handler;

    .line 88
    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView$1;-><init>(Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->animateRunnable:Ljava/lang/Runnable;

    .line 23
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->init()V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->handler:Landroid/os/Handler;

    .line 88
    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView$1;-><init>(Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->animateRunnable:Ljava/lang/Runnable;

    .line 28
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->init()V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->handler:Landroid/os/Handler;

    .line 88
    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView$1;-><init>(Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->animateRunnable:Ljava/lang/Runnable;

    .line 33
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->init()V

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->animateRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method private init()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 38
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v8, v7, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 39
    .local v1, "dip":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 41
    .local v0, "batteryColor":I
    const/16 v2, 0x18

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokesNo(I)V

    .line 42
    float-to-double v2, v1

    const-wide/high16 v4, 0x400c000000000000L    # 3.5

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeBStrokeWidth(I)V

    .line 43
    mul-float v2, v1, v7

    float-to-int v2, v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeAStrokeWidth(I)V

    .line 44
    const/16 v2, 0x64

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeAPercent(I)V

    .line 45
    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeBPercent(I)V

    .line 46
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeBColor(I)V

    .line 47
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeAColor(I)V

    .line 48
    invoke-virtual {p0, v8}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setDrawAllBSpokes(Z)V

    .line 49
    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setClickable(Z)V

    .line 50
    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeBPadding(I)V

    .line 51
    invoke-virtual {p0, v6}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeAPadding(I)V

    .line 52
    const/16 v2, 0x28

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeARadiusPercent(I)V

    .line 53
    const/16 v2, 0x1b

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeBRadiusPercent(I)V

    .line 54
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->running:Z

    .line 69
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->animateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 71
    :cond_0
    return-void
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->running:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const-wide/16 v6, 0x3e8

    .line 75
    iget-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->running:Z

    if-eqz v2, :cond_0

    .line 76
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->progressStartTime:J

    sub-long/2addr v2, v4

    rem-long v0, v2, v6

    .line 77
    .local v0, "animationTime":J
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->getSpokeAPercent()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v2, v0

    div-long/2addr v2, v6

    long-to-int v2, v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->setSpokeBPercent(I)V

    .line 80
    .end local v0    # "animationTime":J
    :cond_0
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->onDraw(Landroid/graphics/Canvas;)V

    .line 81
    return-void
.end method

.method public show()V
    .locals 6

    .prologue
    .line 57
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->running:Z

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->progressStartTime:J

    .line 60
    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->animateRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 62
    const/16 v1, 0x168

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->getSpokesNo()I

    move-result v2

    div-int/2addr v1, v2

    int-to-float v0, v1

    .line 63
    .local v0, "spokeAngle":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->getSpokeAPercent()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v0

    iput v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeProgressView;->mProgressStep:F

    .line 65
    return-void
.end method
