.class public Lcom/vectorwatch/android/ui/view/widgets/SpokeView;
.super Landroid/view/View;
.source "SpokeView.java"


# static fields
.field public static final DEFAULT_COLOR:I


# instance fields
.field private animCurrentStep:I

.field private animFinalPercent:I

.field private animTotalRunTime:I

.field private animateRunnable:Ljava/lang/Runnable;

.field private clickable:Z

.field private clicked:Z

.field private debugTouch:Z

.field private drawAllBSpokes:Z

.field final handler:Landroid/os/Handler;

.field private runningAnimation:Z

.field private spokeAColor:I

.field private spokeAPadding:I

.field private spokeAPaint:Landroid/graphics/Paint;

.field private spokeAPercent:I

.field private spokeARadiusPercent:I

.field private spokeAStrokeWidth:I

.field private spokeBColor:I

.field private spokeBPadding:I

.field private spokeBPaint:Landroid/graphics/Paint;

.field private spokeBPercent:I

.field private spokeBRadiusPercent:I

.field private spokeBStrokeWidth:I

.field private spokesNo:I

.field private touchAngle:D

.field private touch_x:F

.field private touch_y:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const/16 v0, 0xc4

    const/16 v1, 0xa5

    const/16 v2, 0x85

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->DEFAULT_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 18
    sget v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->DEFAULT_COLOR:I

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAColor:I

    .line 19
    sget v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->DEFAULT_COLOR:I

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBColor:I

    .line 20
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->handler:Landroid/os/Handler;

    .line 21
    const/16 v0, 0x27

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokesNo:I

    .line 22
    const/16 v0, 0x1e

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeARadiusPercent:I

    .line 23
    const/16 v0, 0x3c

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBRadiusPercent:I

    .line 24
    const/16 v0, 0x46

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPercent:I

    .line 25
    const/16 v0, 0x23

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPercent:I

    .line 26
    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clickable:Z

    .line 27
    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->drawAllBSpokes:Z

    .line 28
    const/4 v0, 0x4

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAStrokeWidth:I

    .line 29
    const/16 v0, 0x8

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBStrokeWidth:I

    .line 30
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPadding:I

    .line 31
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    .line 32
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPaint:Landroid/graphics/Paint;

    .line 33
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touchAngle:D

    .line 35
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clicked:Z

    .line 38
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->debugTouch:Z

    .line 39
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animCurrentStep:I

    .line 40
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animFinalPercent:I

    .line 41
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animTotalRunTime:I

    .line 42
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->runningAnimation:Z

    .line 199
    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;-><init>(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animateRunnable:Ljava/lang/Runnable;

    .line 46
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->init()V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    sget v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->DEFAULT_COLOR:I

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAColor:I

    .line 19
    sget v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->DEFAULT_COLOR:I

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBColor:I

    .line 20
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->handler:Landroid/os/Handler;

    .line 21
    const/16 v0, 0x27

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokesNo:I

    .line 22
    const/16 v0, 0x1e

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeARadiusPercent:I

    .line 23
    const/16 v0, 0x3c

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBRadiusPercent:I

    .line 24
    const/16 v0, 0x46

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPercent:I

    .line 25
    const/16 v0, 0x23

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPercent:I

    .line 26
    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clickable:Z

    .line 27
    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->drawAllBSpokes:Z

    .line 28
    const/4 v0, 0x4

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAStrokeWidth:I

    .line 29
    const/16 v0, 0x8

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBStrokeWidth:I

    .line 30
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPadding:I

    .line 31
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    .line 32
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPaint:Landroid/graphics/Paint;

    .line 33
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touchAngle:D

    .line 35
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clicked:Z

    .line 38
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->debugTouch:Z

    .line 39
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animCurrentStep:I

    .line 40
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animFinalPercent:I

    .line 41
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animTotalRunTime:I

    .line 42
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->runningAnimation:Z

    .line 199
    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;-><init>(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animateRunnable:Ljava/lang/Runnable;

    .line 51
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->init()V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    sget v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->DEFAULT_COLOR:I

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAColor:I

    .line 19
    sget v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->DEFAULT_COLOR:I

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBColor:I

    .line 20
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->handler:Landroid/os/Handler;

    .line 21
    const/16 v0, 0x27

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokesNo:I

    .line 22
    const/16 v0, 0x1e

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeARadiusPercent:I

    .line 23
    const/16 v0, 0x3c

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBRadiusPercent:I

    .line 24
    const/16 v0, 0x46

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPercent:I

    .line 25
    const/16 v0, 0x23

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPercent:I

    .line 26
    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clickable:Z

    .line 27
    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->drawAllBSpokes:Z

    .line 28
    const/4 v0, 0x4

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAStrokeWidth:I

    .line 29
    const/16 v0, 0x8

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBStrokeWidth:I

    .line 30
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPadding:I

    .line 31
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    .line 32
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPaint:Landroid/graphics/Paint;

    .line 33
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touchAngle:D

    .line 35
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clicked:Z

    .line 38
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->debugTouch:Z

    .line 39
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animCurrentStep:I

    .line 40
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animFinalPercent:I

    .line 41
    iput v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animTotalRunTime:I

    .line 42
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->runningAnimation:Z

    .line 199
    new-instance v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView$1;-><init>(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animateRunnable:Ljava/lang/Runnable;

    .line 56
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->init()V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->runningAnimation:Z

    return v0
.end method

.method static synthetic access$002(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;
    .param p1, "x1"    # Z

    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->runningAnimation:Z

    return p1
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokesNo:I

    return v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animFinalPercent:I

    return v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animTotalRunTime:I

    return v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animCurrentStep:I

    return v0
.end method

.method static synthetic access$408(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)I
    .locals 2
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .prologue
    .line 16
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animCurrentStep:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animCurrentStep:I

    return v0
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/view/widgets/SpokeView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animateRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static calcRotationAngleInDegrees(Landroid/graphics/PointF;Landroid/graphics/PointF;)D
    .locals 8
    .param p0, "centerPt"    # Landroid/graphics/PointF;
    .param p1, "targetPt"    # Landroid/graphics/PointF;

    .prologue
    .line 77
    iget v4, p1, Landroid/graphics/PointF;->y:F

    iget v5, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    iget v6, p1, Landroid/graphics/PointF;->x:F

    iget v7, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 83
    .local v2, "theta":D
    const-wide v4, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double/2addr v2, v4

    .line 87
    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    .line 93
    .local v0, "angle":D
    const-wide/16 v4, 0x0

    cmpg-double v4, v0, v4

    if-gez v4, :cond_0

    .line 94
    const-wide v4, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v4

    .line 96
    :cond_0
    return-wide v0
.end method

.method private init()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 115
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 116
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 117
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 118
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 119
    return-void
.end method


# virtual methods
.method public getSpokeAColor()I
    .locals 1

    .prologue
    .line 284
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAColor:I

    return v0
.end method

.method public getSpokeAPadding()I
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPadding:I

    return v0
.end method

.method public getSpokeAPercent()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPercent:I

    return v0
.end method

.method public getSpokeARadiusPercent()I
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeARadiusPercent:I

    return v0
.end method

.method public getSpokeAStrokeWidth()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAStrokeWidth:I

    return v0
.end method

.method public getSpokeBColor()I
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBColor:I

    return v0
.end method

.method public getSpokeBPadding()I
    .locals 1

    .prologue
    .line 364
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    return v0
.end method

.method public getSpokeBPercent()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPercent:I

    return v0
.end method

.method public getSpokeBRadiusPercent()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBRadiusPercent:I

    return v0
.end method

.method public getSpokeBStrokeWidth()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBStrokeWidth:I

    return v0
.end method

.method public getSpokesNo()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokesNo:I

    return v0
.end method

.method public isClickable()Z
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clickable:Z

    return v0
.end method

.method public isDebugTouch()Z
    .locals 1

    .prologue
    .line 340
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->debugTouch:Z

    return v0
.end method

.method public isDrawAllBSpokes()Z
    .locals 1

    .prologue
    .line 348
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->drawAllBSpokes:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 20
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 139
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->getWidth()I

    move-result v19

    .line 140
    .local v19, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->getHeight()I

    move-result v10

    .line 142
    .local v10, "height":I
    if-lez v19, :cond_0

    if-gtz v10, :cond_1

    .line 189
    :cond_0
    return-void

    .line 145
    :cond_1
    move/from16 v0, v19

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v12, v2

    .line 146
    .local v12, "radius":F
    div-int/lit8 v2, v19, 0x2

    int-to-float v3, v2

    .line 147
    .local v3, "centerX":F
    div-int/lit8 v2, v10, 0x2

    int-to-float v9, v2

    .line 148
    .local v9, "centerY":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPadding:I

    int-to-float v2, v2

    sub-float v2, v12, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeARadiusPercent:I

    int-to-float v4, v4

    mul-float/2addr v2, v4

    const/high16 v4, 0x42c80000    # 100.0f

    div-float v13, v2, v4

    .line 149
    .local v13, "spokeASize":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    int-to-float v2, v2

    sub-float v2, v12, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBRadiusPercent:I

    int-to-float v4, v4

    mul-float/2addr v2, v4

    const/high16 v4, 0x42c80000    # 100.0f

    div-float v18, v2, v4

    .line 150
    .local v18, "spokeBSize":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPercent:I

    mul-int/lit16 v2, v2, 0x168

    div-int/lit8 v2, v2, 0x64

    int-to-double v14, v2

    .line 151
    .local v14, "spokeAAngle":D
    move-object/from16 v0, p0

    iget v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPercent:I

    mul-int/lit16 v2, v2, 0x168

    div-int/lit8 v2, v2, 0x64

    int-to-double v0, v2

    move-wide/from16 v16, v0

    .line 153
    .local v16, "spokeBAngle":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAStrokeWidth:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 156
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBStrokeWidth:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 158
    const/16 v2, 0x168

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokesNo:I

    div-int/2addr v2, v4

    int-to-float v8, v2

    .line 160
    .local v8, "angle":F
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->debugTouch:Z

    if-eqz v2, :cond_2

    .line 161
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touch_x:F

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touch_y:F

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/high16 v5, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 164
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clickable:Z

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->runningAnimation:Z

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_0
    and-int/2addr v2, v4

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clicked:Z

    if-eqz v2, :cond_3

    .line 165
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touchAngle:D

    move-wide/from16 v16, v0

    .line 166
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clicked:Z

    .line 171
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->debugTouch:Z

    if-eqz v2, :cond_4

    .line 172
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "B"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v16

    double-to-int v4, v0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v9, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 174
    :cond_4
    const/4 v11, 0x0

    .local v11, "i":F
    :goto_1
    const/high16 v2, 0x43b40000    # 360.0f

    cmpg-float v2, v11, v2

    if-gez v2, :cond_0

    .line 176
    float-to-double v4, v11

    cmpg-double v2, v4, v14

    if-gez v2, :cond_5

    .line 177
    sub-float v2, v9, v12

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPadding:I

    int-to-float v4, v4

    add-float/2addr v4, v2

    sub-float v2, v9, v12

    add-float/2addr v2, v13

    move-object/from16 v0, p0

    iget v5, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPadding:I

    int-to-float v5, v5

    add-float v6, v2, v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v5, v3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 181
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->drawAllBSpokes:Z

    if-eqz v2, :cond_6

    float-to-double v4, v11

    cmpg-double v2, v4, v16

    if-gtz v2, :cond_6

    .line 182
    sub-float v2, v9, v12

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    int-to-float v4, v4

    add-float/2addr v4, v2

    sub-float v2, v9, v12

    add-float v2, v2, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    int-to-float v5, v5

    add-float v6, v2, v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v5, v3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 184
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->drawAllBSpokes:Z

    if-nez v2, :cond_7

    float-to-double v4, v11

    cmpg-double v2, v4, v16

    if-gtz v2, :cond_7

    add-float v2, v11, v8

    float-to-double v4, v2

    cmpl-double v2, v4, v16

    if-lez v2, :cond_7

    .line 185
    sub-float v2, v9, v12

    move-object/from16 v0, p0

    iget v4, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    int-to-float v4, v4

    add-float/2addr v4, v2

    sub-float v2, v9, v12

    add-float v2, v2, v18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    int-to-float v5, v5

    add-float v6, v2, v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v5, v3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 187
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v3, v9}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 174
    add-float/2addr v11, v8

    goto/16 :goto_1

    .line 164
    .end local v11    # "i":F
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 123
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 124
    const/4 v1, 0x0

    .line 125
    .local v1, "size":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->getMeasuredWidth()I

    move-result v2

    .line 126
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->getMeasuredHeight()I

    move-result v0

    .line 128
    .local v0, "height":I
    if-le v2, v0, :cond_0

    .line 129
    move v1, v0

    .line 133
    :goto_0
    invoke-virtual {p0, v1, v1}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setMeasuredDimension(II)V

    .line 134
    return-void

    .line 131
    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 101
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touch_x:F

    .line 102
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touch_y:F

    .line 104
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v1, Landroid/graphics/PointF;

    iget v2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touch_x:F

    iget v3, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touch_y:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-static {v0, v1}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->calcRotationAngleInDegrees(Landroid/graphics/PointF;Landroid/graphics/PointF;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->touchAngle:D

    .line 107
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->invalidate()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->runningAnimation:Z

    .line 109
    iput-boolean v4, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clicked:Z

    .line 111
    return v4
.end method

.method public setClickable(Z)V
    .locals 0
    .param p1, "clickable"    # Z

    .prologue
    .line 336
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->clickable:Z

    .line 337
    return-void
.end method

.method public setDebugTouch(Z)V
    .locals 0
    .param p1, "debugTouch"    # Z

    .prologue
    .line 344
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->debugTouch:Z

    .line 345
    return-void
.end method

.method public setDrawAllBSpokes(Z)V
    .locals 0
    .param p1, "drawAllBSpokes"    # Z

    .prologue
    .line 352
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->drawAllBSpokes:Z

    .line 353
    return-void
.end method

.method public setSpokeAColor(I)V
    .locals 0
    .param p1, "spokeAColor"    # I

    .prologue
    .line 288
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAColor:I

    .line 289
    return-void
.end method

.method public setSpokeAPadding(I)V
    .locals 0
    .param p1, "spokeAPadding"    # I

    .prologue
    .line 360
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPadding:I

    .line 361
    return-void
.end method

.method public setSpokeAPercent(I)V
    .locals 1
    .param p1, "spokeAPercent"    # I

    .prologue
    .line 309
    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    .line 310
    const/16 p1, 0x64

    .line 311
    :cond_0
    if-gez p1, :cond_1

    .line 312
    const/4 p1, 0x0

    .line 313
    :cond_1
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAPercent:I

    .line 314
    return-void
.end method

.method public setSpokeARadiusPercent(I)V
    .locals 1
    .param p1, "spokeARadiusPercent"    # I

    .prologue
    .line 248
    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    .line 249
    const/16 p1, 0x64

    .line 250
    :cond_0
    if-gez p1, :cond_1

    .line 251
    const/4 p1, 0x0

    .line 252
    :cond_1
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeARadiusPercent:I

    .line 253
    return-void
.end method

.method public setSpokeAStrokeWidth(I)V
    .locals 0
    .param p1, "spokeAStrokeWidth"    # I

    .prologue
    .line 272
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAStrokeWidth:I

    .line 273
    return-void
.end method

.method public setSpokeBColor(I)V
    .locals 0
    .param p1, "spokeBColor"    # I

    .prologue
    .line 296
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBColor:I

    .line 297
    return-void
.end method

.method public setSpokeBPadding(I)V
    .locals 0
    .param p1, "spokeBPadding"    # I

    .prologue
    .line 368
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPadding:I

    .line 369
    return-void
.end method

.method public setSpokeBPercent(I)V
    .locals 1
    .param p1, "spokeBPercent"    # I

    .prologue
    .line 321
    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    .line 322
    const/16 p1, 0x64

    .line 323
    :cond_0
    if-gez p1, :cond_1

    .line 324
    const/4 p1, 0x0

    .line 326
    :cond_1
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBPercent:I

    .line 327
    return-void
.end method

.method public setSpokeBRadiusPercent(I)V
    .locals 1
    .param p1, "spokeBRadiusPercent"    # I

    .prologue
    .line 260
    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    .line 261
    const/16 p1, 0x64

    .line 262
    :cond_0
    if-gez p1, :cond_1

    .line 263
    const/4 p1, 0x0

    .line 264
    :cond_1
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBRadiusPercent:I

    .line 265
    return-void
.end method

.method public setSpokeBStrokeWidth(I)V
    .locals 0
    .param p1, "spokeBStrokeWidth"    # I

    .prologue
    .line 280
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBStrokeWidth:I

    .line 281
    return-void
.end method

.method public setSpokeColor(I)V
    .locals 0
    .param p1, "spokeColor"    # I

    .prologue
    .line 300
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeBColor:I

    .line 301
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokeAColor:I

    .line 302
    return-void
.end method

.method public setSpokesNo(I)V
    .locals 1
    .param p1, "spokesNo"    # I

    .prologue
    .line 238
    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    .line 239
    const/4 p1, 0x4

    .line 240
    :cond_0
    rem-int/lit8 v0, p1, 0x4

    sub-int v0, p1, v0

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->spokesNo:I

    .line 241
    return-void
.end method

.method public startAnimation(III)V
    .locals 4
    .param p1, "percent"    # I
    .param p2, "runTimeMs"    # I
    .param p3, "delayedStartMs"    # I

    .prologue
    .line 192
    iput p1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animFinalPercent:I

    .line 193
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animCurrentStep:I

    .line 194
    iput p2, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animTotalRunTime:I

    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->runningAnimation:Z

    .line 196
    iget-object v0, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->animateRunnable:Ljava/lang/Runnable;

    int-to-long v2, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 197
    return-void
.end method
