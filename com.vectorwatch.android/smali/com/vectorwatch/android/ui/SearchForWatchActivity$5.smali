.class Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;
.super Ljava/lang/Object;
.source "SearchForWatchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/SearchForWatchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "paramView"    # Landroid/view/View;
    .param p3, "paramInt"    # I
    .param p4, "paramLong"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "paramAdapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v8, 0x0

    .line 153
    iget-object v7, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    # invokes: Lcom/vectorwatch/android/ui/SearchForWatchActivity;->scanLeDevice(Z)V
    invoke-static {v7, v8}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->access$200(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Z)V

    .line 155
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 156
    .local v4, "localBundle":Landroid/os/Bundle;
    iget-object v7, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    iget-object v7, v7, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v7, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;

    .line 157
    .local v1, "device":Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "address":Ljava/lang/String;
    const-string v7, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v4, v7, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 161
    .local v5, "localIntent":Landroid/content/Intent;
    invoke-virtual {v5, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 164
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    .line 166
    .local v6, "name":Ljava/lang/String;
    if-eqz v6, :cond_1

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x53

    if-ne v7, v8, :cond_1

    .line 167
    const-string v7, "square"

    iget-object v8, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->access$300(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchShape(Ljava/lang/String;Landroid/content/Context;)V

    .line 172
    :goto_0
    const-string v7, "BOND"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bond state "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v9

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v7

    const/16 v8, 0xa

    if-ne v7, v8, :cond_2

    .line 175
    :try_start_0
    iget-object v7, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->createBond(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :goto_1
    new-instance v3, Landroid/content/IntentFilter;

    const-string v7, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-direct {v3, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 180
    .local v3, "filter":Landroid/content/IntentFilter;
    iget-object v7, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->access$300(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v8}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->access$400(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)Landroid/content/BroadcastReceiver;

    move-result-object v8

    invoke-virtual {v7, v8, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 181
    iget-object v7, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    const/4 v8, 0x1

    # setter for: Lcom/vectorwatch/android/ui/SearchForWatchActivity;->registeredBondingReceiver:Z
    invoke-static {v7, v8}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->access$102(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Z)Z

    .line 187
    .end local v3    # "filter":Landroid/content/IntentFilter;
    :cond_0
    :goto_2
    return-void

    .line 169
    :cond_1
    const-string v7, "round"

    iget-object v8, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    # getter for: Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->access$300(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setWatchShape(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 176
    :catch_0
    move-exception v2

    .line 177
    .local v2, "e":Ljava/lang/Exception;
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 182
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v7

    invoke-virtual {v7}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v7

    const/16 v8, 0xc

    if-ne v7, v8, :cond_0

    .line 183
    iget-object v7, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->registerToServer(Landroid/bluetooth/BluetoothDevice;)V

    .line 184
    iget-object v7, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    const/4 v8, -0x1

    invoke-virtual {v7, v8, v5}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_2
.end method
