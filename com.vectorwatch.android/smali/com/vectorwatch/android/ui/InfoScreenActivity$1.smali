.class Lcom/vectorwatch/android/ui/InfoScreenActivity$1;
.super Ljava/lang/Object;
.source "InfoScreenActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/InfoScreenActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/InfoScreenActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/InfoScreenActivity;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 83
    # getter for: Lcom/vectorwatch/android/ui/InfoScreenActivity;->log:Lorg/slf4j/Logger;
    invoke-static {}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->access$000()Lorg/slf4j/Logger;

    move-result-object v1

    const-string v2, "COMPATIBILITY: timeout received."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 84
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v1

    const-string v2, "last_known_system_info"

    const-class v3, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 87
    invoke-virtual {v1, v2, v3}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 88
    .local v0, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    if-eqz v0, :cond_0

    .line 89
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090102

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    .end local v0    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 98
    return-void

    .line 91
    .restart local v0    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 94
    .end local v0    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    iget-object v1, v1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
