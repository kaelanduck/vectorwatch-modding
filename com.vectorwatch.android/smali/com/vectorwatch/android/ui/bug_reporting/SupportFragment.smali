.class public Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;
.super Landroid/app/Fragment;
.source "SupportFragment.java"


# static fields
.field private static final ALPHA_MAILING_LIST:Ljava/lang/String; = "androidalpha@vectorwatch.com"

.field private static final BETA_MAILING_LIST:Ljava/lang/String; = "androidbeta@vectorwatch.com"

.field private static final PICK_FROM_GALLERY:I = 0x65

.field private static final PRODUCTION_MAILING_LIST:Ljava/lang/String; = "support@vectorwatch.com"

.field private static final RESULT_CODE_SEND_REPORT:I = 0x3f2

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private mAccountManager:Landroid/accounts/AccountManager;

.field private mAttachFile:Landroid/widget/ImageButton;

.field private mAttachedFilesPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mBugDetails:Landroid/widget/EditText;

.field private mBugSummary:Landroid/widget/EditText;

.field private mContactEmail:Ljava/lang/String;

.field private mIsUsingLoginEmail:Landroid/widget/CheckBox;

.field private mOtherEmail:Landroid/widget/EditText;

.field private mSendBugReport:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->handleFileAttachAction()V

    return-void
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->handleSendingBugReport()V

    return-void
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->checkEmailUsed(Z)V

    return-void
.end method

.method private attachLogFiles()V
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    invoke-static {}, Lcom/vectorwatch/android/utils/FileUtils;->getActiveLogFile()Ljava/io/File;

    move-result-object v21

    .line 133
    .local v21, "logFile":Ljava/io/File;
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->read(Ljava/io/File;)[B

    move-result-object v11

    .line 134
    .local v11, "dataBase64":[B
    const/4 v2, 0x0

    invoke-static {v11, v2}, Lorg/apache/commons/codec/binary/Base64;->encodeBase64([BZ)[B

    move-result-object v2

    invoke-static {v2}, Lorg/apache/commons/codec/binary/StringUtils;->newStringUtf8([B)Ljava/lang/String;

    move-result-object v20

    .line 135
    .local v20, "logEncoded":Ljava/lang/String;
    invoke-static {}, Lcom/vectorwatch/android/utils/FileUtils;->getActiveLogBaseFile()Ljava/io/File;

    move-result-object v19

    .line 136
    .local v19, "logCacheDir":Ljava/io/File;
    new-instance v24, Ljava/io/PrintWriter;

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    .line 137
    .local v24, "writer":Ljava/io/PrintWriter;
    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 138
    invoke-virtual/range {v24 .. v24}, Ljava/io/PrintWriter;->close()V

    .line 140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mAttachedFilesPaths:Ljava/util/ArrayList;

    invoke-static/range {v19 .. v19}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    invoke-static {}, Lcom/vectorwatch/android/utils/FileUtils;->getDatabaseFile()Ljava/io/File;

    move-result-object v13

    .line 143
    .local v13, "databaseFile":Ljava/io/File;
    const/16 v18, 0x0

    .line 145
    .local v18, "fw":Ljava/io/FileWriter;
    new-instance v18, Ljava/io/FileWriter;

    .end local v18    # "fw":Ljava/io/FileWriter;
    invoke-virtual {v13}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 147
    .restart local v18    # "fw":Ljava/io/FileWriter;
    new-instance v8, Ljava/io/BufferedWriter;

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 150
    .local v8, "bw":Ljava/io/BufferedWriter;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/vectorwatch/android/database/VectorContentProvider;->CONTENT_URI_ACTIVITY:Landroid/net/Uri;

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "calories"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "distance"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "value"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "timestamp"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "avg_period"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "activity_google_fit_dirty"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "activity_google_fit_dirty_dist"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "activity_google_fit_dirty_cal"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 166
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 168
    :cond_0
    const-string v2, "calories"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 169
    .local v9, "calories":I
    const-string v2, "distance"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 170
    .local v17, "distance":I
    const-string v2, "value"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 171
    .local v22, "steps":I
    const-string v2, "timestamp"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 172
    .local v23, "timestamp":I
    const-string v2, "activity_google_fit_dirty"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 173
    .local v16, "dirtyFit":I
    const-string v2, "activity_google_fit_dirty_dist"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 174
    .local v15, "dirtyDist":I
    const-string v2, "activity_google_fit_dirty_cal"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 177
    .local v14, "dirtyCal":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Activity ts = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " calories = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | distance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "| steps ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " | google fit dirty (s, d, c) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 180
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 183
    .end local v9    # "calories":I
    .end local v14    # "dirtyCal":I
    .end local v15    # "dirtyDist":I
    .end local v16    # "dirtyFit":I
    .end local v17    # "distance":I
    .end local v22    # "steps":I
    .end local v23    # "timestamp":I
    :cond_1
    invoke-virtual {v8}, Ljava/io/BufferedWriter;->close()V

    .line 184
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 186
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->read(Ljava/io/File;)[B

    move-result-object v11

    .line 187
    const/4 v2, 0x0

    invoke-static {v11, v2}, Lorg/apache/commons/codec/binary/Base64;->encodeBase64([BZ)[B

    move-result-object v2

    invoke-static {v2}, Lorg/apache/commons/codec/binary/StringUtils;->newStringUtf8([B)Ljava/lang/String;

    move-result-object v20

    .line 188
    invoke-static {}, Lcom/vectorwatch/android/utils/FileUtils;->getDatabaseBaseFile()Ljava/io/File;

    move-result-object v12

    .line 189
    .local v12, "databaseBase":Ljava/io/File;
    new-instance v24, Ljava/io/PrintWriter;

    .end local v24    # "writer":Ljava/io/PrintWriter;
    move-object/from16 v0, v24

    invoke-direct {v0, v12}, Ljava/io/PrintWriter;-><init>(Ljava/io/File;)V

    .line 190
    .restart local v24    # "writer":Ljava/io/PrintWriter;
    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 191
    invoke-virtual/range {v24 .. v24}, Ljava/io/PrintWriter;->close()V

    .line 193
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mAttachedFilesPaths:Ljava/util/ArrayList;

    invoke-static {v12}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    return-void
.end method

.method private checkEmailUsed(Z)V
    .locals 2
    .param p1, "isChecked"    # Z

    .prologue
    .line 124
    if-eqz p1, :cond_0

    .line 125
    iget-object v0, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mOtherEmail:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mOtherEmail:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_0
.end method

.method private handleFileAttachAction()V
    .locals 3

    .prologue
    .line 349
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 350
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    const-string v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 353
    const-string v1, "Complete action using"

    .line 354
    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x65

    .line 353
    invoke-virtual {p0, v1, v2}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 356
    return-void
.end method

.method private handleSendingBugReport()V
    .locals 18

    .prologue
    .line 203
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->hasBugSummary()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->hasEmail()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->hasBugDetails()Z

    move-result v13

    if-nez v13, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isAlphaBuild()Z

    move-result v13

    if-nez v13, :cond_2

    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isDevBuild()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 208
    :cond_2
    const-string v13, "androidalpha@vectorwatch.com"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mContactEmail:Ljava/lang/String;

    .line 215
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mContactEmail:Ljava/lang/String;

    if-nez v13, :cond_3

    .line 216
    sget-object v13, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->log:Lorg/slf4j/Logger;

    const-string v14, "Bug report - There should be a sending email."

    invoke-interface {v13, v14}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 219
    :cond_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "[Android] "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mBugSummary:Landroid/widget/EditText;

    invoke-virtual {v14}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 220
    .local v7, "mailTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mBugDetails:Landroid/widget/EditText;

    invoke-virtual {v13}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 222
    .local v6, "mailText":Ljava/lang/String;
    sget-object v13, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->log:Lorg/slf4j/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Bug report - mail title, text, contact address, dest address: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mContactEmail:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mContactEmail:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 225
    new-instance v5, Landroid/content/Intent;

    const-string v13, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v5, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 226
    .local v5, "intent":Landroid/content/Intent;
    const-string v13, "message/rfc822"

    invoke-virtual {v5, v13}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    const-string v13, "android.intent.extra.SUBJECT"

    invoke-virtual {v5, v13, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 229
    .local v8, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v9

    .line 231
    .local v9, "packageName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 234
    .local v1, "appVersion":Ljava/lang/String;
    const/4 v13, 0x0

    :try_start_0
    invoke-virtual {v8, v9, v13}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v13

    iget-object v1, v13, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 239
    :goto_2
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v13

    const-string v14, "last_known_system_info"

    const-class v15, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 240
    invoke-virtual {v13, v14, v15}, Lcom/vectorwatch/android/utils/ComplexPreferences;->getObject(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/com/android/vos/update/SystemInfo;

    .line 242
    .local v4, "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    const-string v12, ""

    .line 243
    .local v12, "watchOS":Ljava/lang/String;
    const-string v11, ""

    .line 244
    .local v11, "userLoginMail":Ljava/lang/String;
    const-string v10, ""

    .line 245
    .local v10, "timezone":Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 246
    invoke-virtual {v4}, Lcom/vectorwatch/com/android/vos/update/SystemInfo;->toString()Ljava/lang/String;

    move-result-object v12

    .line 247
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    invoke-static {v13}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getAccountAddress(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 248
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    .line 251
    :cond_4
    const-string v14, "android.intent.extra.TEXT"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "\n Android app version: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, " "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n Android API Level: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget v15, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Device: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v15, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Manufacturer: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v15, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Model: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v15, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, " ("

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v15, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, ") "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    if-eqz v12, :cond_7

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Watch system info: "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v16, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    :goto_3
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Watch ID: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 258
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    invoke-static {v15}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBondedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Watch SN: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "pref_serial_number"

    const/16 v16, 0x0

    .line 259
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "User login: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "User type: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "account_update_type"

    const/16 v16, 0x0

    .line 261
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "Local timezone: "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n\n------- Bug report "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "starts"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, " "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "below "

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "-------\n\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 251
    invoke-virtual {v5, v14, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    const-string v13, "android.intent.extra.EMAIL"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mContactEmail:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    invoke-virtual {v5, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    const-string v13, "android.intent.extra.STREAM"

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mAttachedFilesPaths:Ljava/util/ArrayList;

    invoke-virtual {v5, v13, v14}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 267
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    sget-object v15, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SUPPORT_TICKET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v16, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SENT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v13, 0x0

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v13}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 271
    :try_start_1
    const-string v13, "Send mail..."

    invoke-static {v5, v13}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v13

    const/16 v14, 0x3f2

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 272
    :catch_0
    move-exception v3

    .line 273
    .local v3, "ex":Landroid/content/ActivityNotFoundException;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v13

    const v14, 0x7f0901c1

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    .line 274
    sget-object v13, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->log:Lorg/slf4j/Logger;

    const-string v14, "Support - Start activity for results: stack = "

    invoke-interface {v13, v14, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 209
    .end local v1    # "appVersion":Ljava/lang/String;
    .end local v3    # "ex":Landroid/content/ActivityNotFoundException;
    .end local v4    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v6    # "mailText":Ljava/lang/String;
    .end local v7    # "mailTitle":Ljava/lang/String;
    .end local v8    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "timezone":Ljava/lang/String;
    .end local v11    # "userLoginMail":Ljava/lang/String;
    .end local v12    # "watchOS":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isBetaBuild()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 210
    const-string v13, "androidbeta@vectorwatch.com"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mContactEmail:Ljava/lang/String;

    goto/16 :goto_1

    .line 212
    :cond_6
    const-string v13, "support@vectorwatch.com"

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mContactEmail:Ljava/lang/String;

    goto/16 :goto_1

    .line 235
    .restart local v1    # "appVersion":Ljava/lang/String;
    .restart local v5    # "intent":Landroid/content/Intent;
    .restart local v6    # "mailText":Ljava/lang/String;
    .restart local v7    # "mailTitle":Ljava/lang/String;
    .restart local v8    # "packageManager":Landroid/content/pm/PackageManager;
    .restart local v9    # "packageName":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 236
    .local v2, "e":Ljava/lang/Exception;
    sget-object v13, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->log:Lorg/slf4j/Logger;

    const-string v14, "Support - Can\'t retrieve appVersion: stack = "

    invoke-interface {v13, v14, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 251
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v4    # "info":Lcom/vectorwatch/com/android/vos/update/SystemInfo;
    .restart local v10    # "timezone":Ljava/lang/String;
    .restart local v11    # "userLoginMail":Ljava/lang/String;
    .restart local v12    # "watchOS":Ljava/lang/String;
    :cond_7
    const-string v13, "No watch system info"

    goto/16 :goto_3
.end method

.method private hasBugDetails()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 297
    iget-object v1, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mBugDetails:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 298
    const/4 v0, 0x1

    .line 301
    :goto_0
    return v0

    .line 300
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f090077

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private hasBugSummary()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 284
    iget-object v1, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mBugSummary:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 285
    const/4 v0, 0x1

    .line 288
    :goto_0
    return v0

    .line 287
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f090078

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private hasEmail()Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v3, 0x1

    const v8, 0x7f0900bd

    const/4 v4, 0x0

    .line 310
    iget-object v5, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mIsUsingLoginEmail:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 342
    :cond_0
    :goto_0
    return v3

    .line 315
    :cond_1
    iget-object v5, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mOtherEmail:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 316
    .local v2, "otherEmail":Ljava/lang/String;
    sget-object v5, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Not using login email. Other email is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 317
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 319
    const-string v5, "@"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 320
    const-string v5, "@"

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 321
    .local v0, "afterAt":Ljava/lang/String;
    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 322
    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 325
    .local v1, "afterPoint":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    if-ge v5, v9, :cond_2

    .line 326
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v8, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    move v3, v4

    .line 327
    goto :goto_0

    .line 330
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v5, v9, :cond_0

    .line 331
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v8, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    move v3, v4

    .line 332
    goto :goto_0

    .line 336
    .end local v1    # "afterPoint":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v8, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    move v3, v4

    .line 337
    goto/16 :goto_0

    .line 341
    .end local v0    # "afterAt":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v8, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    move v3, v4

    .line 342
    goto/16 :goto_0
.end method


# virtual methods
.method public handleBondStateChangedEvent(Lcom/vectorwatch/android/events/BondStateChangedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BondStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 410
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BondStateChangedEvent;->getBondState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->goToMain(Landroid/app/Activity;)V

    .line 413
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 360
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 361
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 362
    .local v1, "selectedImage":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_data"

    aput-object v3, v2, v0

    .line 364
    .local v2, "filePathColumn":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 365
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 366
    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 367
    .local v7, "columnIndex":I
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 368
    .local v6, "attachmentFile":Ljava/lang/String;
    sget-object v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Attachment Path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 369
    const/4 v10, 0x0

    .line 370
    .local v10, "uri":Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 371
    iget-object v0, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mAttachedFilesPaths:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 375
    .end local v1    # "selectedImage":Landroid/net/Uri;
    .end local v2    # "filePathColumn":[Ljava/lang/String;
    .end local v6    # "attachmentFile":Ljava/lang/String;
    .end local v7    # "columnIndex":I
    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v10    # "uri":Landroid/net/Uri;
    :cond_0
    const/16 v0, 0x3f2

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 376
    invoke-static {}, Lcom/vectorwatch/android/utils/FileUtils;->getDatabaseFile()Ljava/io/File;

    move-result-object v9

    .line 377
    .local v9, "databaseFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 379
    .end local v9    # "databaseFile":Ljava/io/File;
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mAccountManager:Landroid/accounts/AccountManager;

    .line 77
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 81
    const v2, 0x7f03002e

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 83
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f100135

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mBugSummary:Landroid/widget/EditText;

    .line 84
    const v2, 0x7f10013a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mBugDetails:Landroid/widget/EditText;

    .line 85
    const v2, 0x7f100139

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mOtherEmail:Landroid/widget/EditText;

    .line 86
    const v2, 0x7f100137

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mIsUsingLoginEmail:Landroid/widget/CheckBox;

    .line 87
    const v2, 0x7f10013c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mAttachFile:Landroid/widget/ImageButton;

    .line 88
    const v2, 0x7f10013e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mSendBugReport:Landroid/widget/Button;

    .line 89
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mAttachedFilesPaths:Ljava/util/ArrayList;

    .line 91
    :try_start_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->attachLogFiles()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mAttachFile:Landroid/widget/ImageButton;

    new-instance v3, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment$1;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment$1;-><init>(Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mSendBugReport:Landroid/widget/Button;

    new-instance v3, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment$2;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment$2;-><init>(Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mIsUsingLoginEmail:Landroid/widget/CheckBox;

    new-instance v3, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment$3;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment$3;-><init>(Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 118
    iget-object v2, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;->mIsUsingLoginEmail:Landroid/widget/CheckBox;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 120
    return-object v1

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public read(Ljava/io/File;)[B
    .locals 8
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 382
    const/4 v3, 0x0

    .line 383
    .local v3, "ous":Ljava/io/ByteArrayOutputStream;
    const/4 v1, 0x0

    .line 385
    .local v1, "ios":Ljava/io/InputStream;
    const/16 v6, 0x1000

    :try_start_0
    new-array v0, v6, [B

    .line 386
    .local v0, "buffer":[B
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 387
    .end local v3    # "ous":Ljava/io/ByteArrayOutputStream;
    .local v4, "ous":Ljava/io/ByteArrayOutputStream;
    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 388
    .end local v1    # "ios":Ljava/io/InputStream;
    .local v2, "ios":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 389
    .local v5, "read":I
    :goto_0
    :try_start_2
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    .line 390
    const/4 v6, 0x0

    invoke-virtual {v4, v0, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 393
    :catchall_0
    move-exception v6

    move-object v1, v2

    .end local v2    # "ios":Ljava/io/InputStream;
    .restart local v1    # "ios":Ljava/io/InputStream;
    move-object v3, v4

    .line 394
    .end local v0    # "buffer":[B
    .end local v4    # "ous":Ljava/io/ByteArrayOutputStream;
    .end local v5    # "read":I
    .restart local v3    # "ous":Ljava/io/ByteArrayOutputStream;
    :goto_1
    if-eqz v3, :cond_0

    .line 395
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 400
    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    .line 401
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 403
    :cond_1
    :goto_3
    throw v6

    .line 394
    .end local v1    # "ios":Ljava/io/InputStream;
    .end local v3    # "ous":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "ios":Ljava/io/InputStream;
    .restart local v4    # "ous":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "read":I
    :cond_2
    if-eqz v4, :cond_3

    .line 395
    :try_start_5
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 400
    :cond_3
    :goto_4
    if-eqz v2, :cond_4

    .line 401
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 405
    :cond_4
    :goto_5
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    return-object v6

    .line 396
    :catch_0
    move-exception v6

    goto :goto_4

    .line 402
    :catch_1
    move-exception v6

    goto :goto_5

    .line 396
    .end local v0    # "buffer":[B
    .end local v2    # "ios":Ljava/io/InputStream;
    .end local v4    # "ous":Ljava/io/ByteArrayOutputStream;
    .end local v5    # "read":I
    .restart local v1    # "ios":Ljava/io/InputStream;
    .restart local v3    # "ous":Ljava/io/ByteArrayOutputStream;
    :catch_2
    move-exception v7

    goto :goto_2

    .line 402
    :catch_3
    move-exception v7

    goto :goto_3

    .line 393
    :catchall_1
    move-exception v6

    goto :goto_1

    .end local v3    # "ous":Ljava/io/ByteArrayOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v4    # "ous":Ljava/io/ByteArrayOutputStream;
    :catchall_2
    move-exception v6

    move-object v3, v4

    .end local v4    # "ous":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "ous":Ljava/io/ByteArrayOutputStream;
    goto :goto_1
.end method
