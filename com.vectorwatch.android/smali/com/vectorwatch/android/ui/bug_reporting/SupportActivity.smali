.class public Lcom/vectorwatch/android/ui/bug_reporting/SupportActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "SupportActivity.java"


# static fields
.field private static final FILE_SELECT_CODE:I

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field private URI:Landroid/net/Uri;

.field private mAttachFile:Landroid/widget/ImageButton;

.field private mAttachedFiles:Landroid/widget/ListView;

.field private mBugDetails:Landroid/widget/EditText;

.field private mBugSummary:Landroid/widget/EditText;

.field private mContext:Landroid/content/Context;

.field private mIsUsingLoginEmail:Landroid/widget/CheckBox;

.field private mOtherEmail:Landroid/widget/EditText;

.field private mSendBugReport:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/bug_reporting/SupportActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/bug_reporting/SupportActivity;->URI:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 40
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/bug_reporting/SupportActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;

    invoke-direct {v2}, Lcom/vectorwatch/android/ui/bug_reporting/SupportFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 43
    sget-object v0, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_SCREEN:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v1, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_SET:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;->ANALYTICS_LABEL_SUPPORT:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    invoke-static {p0, v0, v1, v2}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    .line 46
    return-void
.end method
