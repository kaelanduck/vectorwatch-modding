.class public Lcom/vectorwatch/android/ui/InfoScreenActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "InfoScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;
    }
.end annotation


# static fields
.field private static final DELAY_SHOW_CHECK_COMPATIBILITY_SCREEN:J = 0x9c4L

.field public static final KEY_INFO_TYPE:Ljava/lang/String; = "key_info_type"

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field final fHandler:Landroid/os/Handler;

.field layout:Landroid/widget/RelativeLayout;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f100142
        }
    .end annotation
.end field

.field private mHandlerCheckComp:Landroid/os/Handler;

.field mInfoText:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001fe
        }
    .end annotation
.end field

.field mRetryButton:Landroid/widget/Button;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001ff
        }
    .end annotation
.end field

.field private mRunnableCheckComp:Ljava/lang/Runnable;

.field mVectorLogo:Landroid/widget/ImageView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001fc
        }
    .end annotation
.end field

.field mVectorTextLogo:Landroid/widget/ImageView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001fd
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 67
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->fHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/InfoScreenActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/InfoScreenActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRunnableCheckComp:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/InfoScreenActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/InfoScreenActivity;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mHandlerCheckComp:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public handleCompatibilityResultEvent(Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 183
    iget-object v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mHandlerCheckComp:Landroid/os/Handler;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRunnableCheckComp:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 184
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;->getCompatibilityStatus()Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->COMPATIBLE:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    if-ne v0, v1, :cond_1

    .line 185
    sget-object v0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "COMPATIBILITY RESPONSE - ALREADY UP TO DATE"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 186
    invoke-static {p0}, Lcom/vectorwatch/android/service/VersionMonitor;->handleCompatibleVersions(Landroid/content/Context;)V

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;->getCompatibilityStatus()Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->INCOMPATIBLE_OLD_APP_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    if-ne v0, v1, :cond_2

    .line 191
    sget-object v0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "COMPATIBILITY RESPONSE - APP UPDATE AVAILABLE"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 193
    const-string v0, "flag_is_force_ota"

    invoke-static {v0, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 194
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;->getServerMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/vectorwatch/android/service/VersionMonitor;->handleIncompatibleOlderAppVersion(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 198
    :cond_2
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/CheckCompatibilityResultEvent;->getCompatibilityStatus()Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;->INCOMPATIBLE_OLD_WATCH_VERSION:Lcom/vectorwatch/android/service/VersionMonitor$CompatibilityStatus;

    if-ne v0, v1, :cond_0

    .line 199
    sget-object v0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->log:Lorg/slf4j/Logger;

    const-string v1, "COMPATIBILITY RESPONSE - WATCH UPDATE AVAILABLE"

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 201
    const-string v0, "flag_is_force_ota"

    invoke-static {v0, v2, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 202
    invoke-static {p0}, Lcom/vectorwatch/android/service/VersionMonitor;->handleIncompatibleOlderWatchVersion(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public handleErrorMessageFromCloudEvent(Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;)V
    .locals 10
    .param p1, "event"    # Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const v9, 0x7f090141

    const/4 v8, 0x0

    .line 209
    iget-object v5, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mHandlerCheckComp:Landroid/os/Handler;

    iget-object v6, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRunnableCheckComp:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 210
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/ErrorMessageFromCloudEvent;->getErrorBody()Ljava/lang/Object;

    move-result-object v0

    .line 212
    .local v0, "errorBody":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 216
    :cond_0
    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    .line 217
    .local v2, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v2, v0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 218
    .local v1, "errorBodyJson":Ljava/lang/String;
    sget-object v5, Lcom/vectorwatch/android/ui/InfoScreenActivity;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "COMPATIBILITY: Json body for error message received from cloud: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 219
    const-class v5, Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    invoke-virtual {v2, v1, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/SoftwareUpdateModel;

    .line 221
    .local v4, "model":Lcom/vectorwatch/android/models/SoftwareUpdateModel;
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/SoftwareUpdateModel;->getErrorMessage()Lcom/vectorwatch/android/events/CloudMessageModel;

    move-result-object v3

    .line 222
    .local v3, "messageModel":Lcom/vectorwatch/android/events/CloudMessageModel;
    if-eqz v3, :cond_2

    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isInternetEnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 225
    invoke-virtual {v3}, Lcom/vectorwatch/android/events/CloudMessageModel;->getText()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 226
    sget-object v5, Lcom/vectorwatch/android/ui/InfoScreenActivity;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "COMPATIBILITY: received error check. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/vectorwatch/android/events/CloudMessageModel;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 227
    iget-object v5, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/vectorwatch/android/events/CloudMessageModel;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v5, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 230
    :cond_1
    iget-object v5, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v5, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 234
    :cond_2
    iget-object v5, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    iget-object v5, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public handleInternetConnectionEnabledEvent(Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/InternetConnectionEnabledEvent;->isEnabled()Z

    move-result v0

    .line 173
    .local v0, "isInternetEnabled":Z
    if-nez v0, :cond_0

    .line 174
    sget-object v1, Lcom/vectorwatch/android/ui/InfoScreenActivity;->log:Lorg/slf4j/Logger;

    const-string v2, "COMPATIBILITY: received internet disabled event."

    invoke-interface {v1, v2}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 176
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090140

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 179
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x4

    .line 74
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v1, 0x7f030068

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->setContentView(I)V

    .line 79
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mHandlerCheckComp:Landroid/os/Handler;

    .line 80
    new-instance v1, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity$1;-><init>(Lcom/vectorwatch/android/ui/InfoScreenActivity;)V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRunnableCheckComp:Ljava/lang/Runnable;

    .line 102
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v1

    const-string v2, "last_known_system_info"

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/utils/ComplexPreferences;->remove(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mHandlerCheckComp:Landroid/os/Handler;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRunnableCheckComp:Ljava/lang/Runnable;

    const-wide/32 v4, 0xea60

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 106
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)V

    .line 107
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 110
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 113
    invoke-static {p0}, Lcom/vectorwatch/android/utils/Helpers;->isInternetEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 115
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090140

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 137
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    new-instance v2, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;-><init>(Lcom/vectorwatch/android/ui/InfoScreenActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    return-void

    .line 118
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "key_info_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 122
    .local v0, "infoType":I
    sget-object v1, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->CHECK_COMPATIBILITY:Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->getVal()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09013f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 127
    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->fHandler:Landroid/os/Handler;

    new-instance v2, Lcom/vectorwatch/android/ui/InfoScreenActivity$2;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity$2;-><init>(Lcom/vectorwatch/android/ui/InfoScreenActivity;)V

    const-wide/16 v4, 0x9c4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 165
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onDestroy()V

    .line 166
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 167
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 160
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onStop()V

    .line 161
    return-void
.end method
