.class public Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "ListViewAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;
    }
.end annotation


# instance fields
.field public final INSTALL_APP:Ljava/lang/String;

.field public final UNINSTALL_APP:Ljava/lang/String;

.field private allStreams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation
.end field

.field private appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

.field public currentPosition:I

.field private listType:Ljava/lang/String;

.field private mActivity:Landroid/app/Activity;

.field private mCloudAppActionsListener:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;

.field private mDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;"
        }
    .end annotation
.end field

.field private mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public mResourceId:I

.field mRunningOperations:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedPosition:I

.field private mShape:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;ILcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;Ljava/lang/String;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p3, "resourceId"    # I
    .param p4, "actionsListener"    # Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;
    .param p5, "listType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;I",
            "Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "cloudElementSummaries":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    const/4 v3, -0x1

    .line 59
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 40
    const-string v2, "install"

    iput-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->INSTALL_APP:Ljava/lang/String;

    .line 41
    const-string v2, "uninstall"

    iput-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->UNINSTALL_APP:Ljava/lang/String;

    .line 44
    iput v3, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->currentPosition:I

    .line 47
    iput v3, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mSelectedPosition:I

    .line 56
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mRunningOperations:Ljava/util/HashMap;

    .line 61
    iput-object p1, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    .line 62
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    .line 63
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mDataList:Ljava/util/List;

    .line 64
    iput p3, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mResourceId:I

    .line 65
    iput-object p4, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mCloudAppActionsListener:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;

    .line 66
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mShape:Ljava/lang/String;

    .line 67
    invoke-static {p1}, Lcom/vectorwatch/android/managers/StreamsManager;->getSavedStreams(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->allStreams:Ljava/util/List;

    .line 69
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200ef

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 71
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 72
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v2

    mul-int/lit16 v2, v2, 0x400

    mul-int/lit16 v1, v2, 0x400

    .line 73
    .local v1, "availableMemoryInBytes":I
    new-instance v2, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$1;

    div-int/lit8 v3, v1, 0xa

    invoke-direct {v2, p0, v3}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$1;-><init>(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;I)V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mMemoryCache:Landroid/util/LruCache;

    .line 80
    iput-object p5, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->listType:Ljava/lang/String;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Lcom/vectorwatch/android/utils/AppInstallManager;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->appInstallManager:Lcom/vectorwatch/android/utils/AppInstallManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->allStreams:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;)Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mCloudAppActionsListener:Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$OnCloudAppActionsListener;

    return-object v0
.end method


# virtual methods
.method public addItems(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StoreElement;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 175
    .local p1, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StoreElement;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 177
    .local v1, "uuidSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mDataList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 178
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mDataList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 182
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 183
    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mDataList:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 187
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->notifyDataSetChanged()V

    .line 188
    return-void
.end method

.method public getAppOperation(Lcom/vectorwatch/android/models/StoreElement;)Ljava/lang/String;
    .locals 3
    .param p1, "app"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    const/4 v0, 0x0

    .line 199
    if-nez p1, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-object v0

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mRunningOperations:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 202
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mRunningOperations:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/vectorwatch/android/models/StoreElement;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mDataList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->getItem(I)Lcom/vectorwatch/android/models/StoreElement;

    move-result-object v0

    return-object v0
.end method

.method public getItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 0

    .prologue
    .line 148
    return-object p0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mDataList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/StoreElement;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StoreElement;->getId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 102
    if-nez p2, :cond_0

    .line 103
    new-instance p2, Lcom/vectorwatch/android/ui/view/CloudAppView;

    .end local p2    # "convertView":Landroid/view/View;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    iget v1, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mResourceId:I

    new-instance v2, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;

    invoke-direct {v2, p0, p1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter$2;-><init>(Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;I)V

    invoke-direct {p2, v0, v1, v2}, Lcom/vectorwatch/android/ui/view/CloudAppView;-><init>(Landroid/app/Activity;ILcom/vectorwatch/android/ui/view/CloudAppView$Callbacks;)V

    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    move-object v0, p2

    .line 141
    check-cast v0, Lcom/vectorwatch/android/ui/view/CloudAppView;

    invoke-virtual {p0, p1}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->getItem(I)Lcom/vectorwatch/android/models/StoreElement;

    move-result-object v1

    iget v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mSelectedPosition:I

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    :goto_0
    iget-object v3, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->listType:Ljava/lang/String;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mShape:Ljava/lang/String;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->allStreams:Ljava/util/List;

    iget-object v6, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual/range {v0 .. v6}, Lcom/vectorwatch/android/ui/view/CloudAppView;->bind(Lcom/vectorwatch/android/models/StoreElement;ZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Landroid/util/LruCache;)V

    .line 144
    return-object p2

    .line 141
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mSelectedPosition:I

    if-eq p3, v0, :cond_0

    .line 154
    iput p3, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mSelectedPosition:I

    .line 158
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->notifyDataSetChanged()V

    .line 159
    return-void

    .line 156
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mSelectedPosition:I

    goto :goto_0
.end method

.method public onOperationFinished(Ljava/lang/String;)V
    .locals 1
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mRunningOperations:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    return-void
.end method

.method public setAllStreams(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 210
    .local p1, "streamList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Stream;>;"
    iput-object p1, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->allStreams:Ljava/util/List;

    .line 211
    return-void
.end method

.method public setCurrentPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->currentPosition:I

    .line 85
    return-void
.end method

.method public startInstallApp(Lcom/vectorwatch/android/models/StoreElement;Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;Landroid/widget/ImageView;)V
    .locals 3
    .param p1, "app"    # Lcom/vectorwatch/android/models/StoreElement;
    .param p2, "progressBar"    # Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;
    .param p3, "actionButton"    # Landroid/widget/ImageView;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/NetworkUtils;->getConnectivityStatus(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mRunningOperations:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    const-string v2, "install"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    :goto_0
    return-void

    .line 166
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lcom/vectorwatch/android/ui/view/circularprogress/CircularProgressBar;->setVisibility(I)V

    .line 167
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09010f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5dc

    iget-object v2, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mActivity:Landroid/app/Activity;

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0
.end method

.method public startUninstallApp(Lcom/vectorwatch/android/models/StoreElement;)V
    .locals 3
    .param p1, "app"    # Lcom/vectorwatch/android/models/StoreElement;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/vectorwatch/android/ui/adapter/ListViewAdapter;->mRunningOperations:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StoreElement;->getUuid()Ljava/lang/String;

    move-result-object v1

    const-string v2, "uninstall"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    return-void
.end method
