.class Lcom/vectorwatch/android/ui/InfoScreenActivity$3;
.super Ljava/lang/Object;
.source "InfoScreenActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/InfoScreenActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/InfoScreenActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/InfoScreenActivity;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 141
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getPrefInstance()Lcom/vectorwatch/android/utils/ComplexPreferences;

    move-result-object v0

    const-string v1, "last_known_system_info"

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/ComplexPreferences;->remove(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->isInternetEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mInfoText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09013f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRetryButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    # getter for: Lcom/vectorwatch/android/ui/InfoScreenActivity;->mHandlerCheckComp:Landroid/os/Handler;
    invoke-static {v0}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->access$200(Lcom/vectorwatch/android/ui/InfoScreenActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    # getter for: Lcom/vectorwatch/android/ui/InfoScreenActivity;->mRunnableCheckComp:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/vectorwatch/android/ui/InfoScreenActivity;->access$100(Lcom/vectorwatch/android/ui/InfoScreenActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 147
    iget-object v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$3;->this$0:Lcom/vectorwatch/android/ui/InfoScreenActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/InfoScreenActivity;->fHandler:Landroid/os/Handler;

    new-instance v1, Lcom/vectorwatch/android/ui/InfoScreenActivity$3$1;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/InfoScreenActivity$3$1;-><init>(Lcom/vectorwatch/android/ui/InfoScreenActivity$3;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    :cond_0
    return-void
.end method
