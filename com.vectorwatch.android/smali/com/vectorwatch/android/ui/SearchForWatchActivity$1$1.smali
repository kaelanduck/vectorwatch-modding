.class Lcom/vectorwatch/android/ui/SearchForWatchActivity$1$1;
.super Ljava/lang/Object;
.source "SearchForWatchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/SearchForWatchActivity$1;->onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/vectorwatch/android/ui/SearchForWatchActivity$1;

.field final synthetic val$device:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity$1;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "this$1"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity$1;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1$1;->this$1:Lcom/vectorwatch/android/ui/SearchForWatchActivity$1;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 64
    const-string v0, "BLE"

    const-string v1, "Device"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1$1;->this$1:Lcom/vectorwatch/android/ui/SearchForWatchActivity$1;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    const-string v2, "in range"

    # invokes: Lcom/vectorwatch/android/ui/SearchForWatchActivity;->addDevice(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->access$000(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V

    .line 67
    const-string v0, "BLE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1$1;->val$device:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    return-void
.end method
