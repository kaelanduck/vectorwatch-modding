.class public Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;
.super Ljava/lang/Object;
.source "EndlessScrollListener.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field private currentPage:I

.field private loading:Z

.field private onEndListCallback:Lcom/vectorwatch/android/ui/store/OnEndListCallback;

.field private previousTotal:I

.field private visibleThreshold:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x5

    iput v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->visibleThreshold:I

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->currentPage:I

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->previousTotal:I

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->loading:Z

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/vectorwatch/android/ui/store/OnEndListCallback;I)V
    .locals 1
    .param p1, "onEndListCallback"    # Lcom/vectorwatch/android/ui/store/OnEndListCallback;
    .param p2, "visibleThreshold"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x5

    iput v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->visibleThreshold:I

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->currentPage:I

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->previousTotal:I

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->loading:Z

    .line 21
    iput p2, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->visibleThreshold:I

    .line 22
    iput-object p1, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->onEndListCallback:Lcom/vectorwatch/android/ui/store/OnEndListCallback;

    .line 23
    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->loading:Z

    if-eqz v0, :cond_0

    .line 34
    iget v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->previousTotal:I

    if-le p4, v0, :cond_0

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->loading:Z

    .line 36
    iput p4, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->previousTotal:I

    .line 37
    iget v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->currentPage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->currentPage:I

    .line 40
    :cond_0
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->loading:Z

    if-nez v0, :cond_1

    sub-int v0, p4, p3

    iget v1, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->visibleThreshold:I

    add-int/2addr v1, p2

    if-gt v0, v1, :cond_1

    .line 42
    iget-object v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->onEndListCallback:Lcom/vectorwatch/android/ui/store/OnEndListCallback;

    iget v1, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->currentPage:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/store/OnEndListCallback;->execute(I)V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/listeners/EndlessScrollListener;->loading:Z

    .line 45
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 28
    return-void
.end method
