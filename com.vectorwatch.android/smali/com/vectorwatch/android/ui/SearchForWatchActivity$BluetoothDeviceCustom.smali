.class Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;
.super Ljava/lang/Object;
.source "SearchForWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/SearchForWatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothDeviceCustom"
.end annotation


# instance fields
.field private address:Ljava/lang/String;

.field private bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private name:Ljava/lang/String;

.field final synthetic this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Ljava/lang/String;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "address"    # Ljava/lang/String;
    .param p4, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 345
    iput-object p1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->this$0:Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    .line 346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347
    iput-object p2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->name:Ljava/lang/String;

    .line 348
    iput-object p3, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->address:Ljava/lang/String;

    .line 349
    iput-object p4, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 350
    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getBluetoothDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 373
    iput-object p1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->address:Ljava/lang/String;

    .line 374
    return-void
.end method

.method public setBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "bluetoothDevice"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 357
    iput-object p1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 358
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 365
    iput-object p1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->name:Ljava/lang/String;

    .line 366
    return-void
.end method
