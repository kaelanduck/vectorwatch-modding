.class public Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
.super Landroid/app/Fragment;
.source "WatchMakerFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/view/View$OnDragListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;,
        Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;
    }
.end annotation


# static fields
.field public static final ACTION_SHOW_GPS_DISABLED_ALERT_DIALOG:I = 0x3

.field public static final ACTION_SHOW_STREAM_DROPPED:Ljava/lang/String; = "show_stream_dropped"

.field public static final ACTION_SHOW_STREAM_TUTORIAL:Ljava/lang/String; = "show_stream_tutorial"

.field public static final ACTION_SHOW_WATCHFACE_TUTORIAL:Ljava/lang/String; = "show_watchface_tutorial"

.field public static final ACTION_UPDATE_APP_AUTH_CREDENTIALS:I = 0x2

.field public static final ACTION_UPDATE_STREAM_AUTH_CREDENTIALS:I = 0x1

.field private static final DELAY_CHANGE_PAGE:J = 0x2bcL

.field private static final DELAY_SWAP:J = 0x64L

.field public static final EXTRA_KEY_ACTION:Ljava/lang/String; = "action"

.field public static final EXTRA_KEY_APP_ID:Ljava/lang/String; = "app_id"

.field public static final EXTRA_KEY_NOTIFICATION_ID:Ljava/lang/String; = "notification_id"

.field public static final EXTRA_KEY_STREAM_NAME:Ljava/lang/String; = "stream_name"

.field private static final FRAGMENT_INDEX:I = 0x0

.field private static final MESSAGE_ADD_ITEM:I = 0x1

.field private static final MESSAGE_REMOVE_ITEM:I = 0x2

.field public static final MIN_NUMBER_OF_VIEWS:I = 0x1

.field private static final REQUEST_CODE_STREAM_AUTHENTICATION:I = 0xca

.field private static final REQUEST_CODE_STREAM_SETTINGS:I = 0xc9

.field public static final TAG:Ljava/lang/String; = "WatchMakerFragment"

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field goOnePageLeft:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

.field goOnePageRight:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

.field private isPaused:Z

.field private isUpdatingStreamCredentialsUuid:Ljava/lang/String;

.field mActionMode:Landroid/support/v7/view/ActionMode;

.field public mActiveWatchFaces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/vectorwatch/android/models/CloudElementSummary;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityTopLayout:Landroid/widget/RelativeLayout;

.field mBatteryFab:Lcom/software/shell/fab/ActionButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001e3
        }
    .end annotation
.end field

.field private mBatteryImage:Landroid/widget/ImageView;

.field private mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

.field private mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

.field private mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

.field mChangePageHandler:Landroid/os/Handler;

.field private mContainerFacePlaceholders:Landroid/widget/RelativeLayout;

.field private mContext:Landroid/content/Context;

.field mContextModeCallback:Landroid/support/v7/view/ActionMode$Callback;

.field private mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

.field mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

.field private mDraggedItemPosition:I

.field private mDraggedWatchFace:Lcom/vectorwatch/android/models/CloudElementSummary;

.field private mElementsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Element;",
            ">;"
        }
    .end annotation
.end field

.field private mEtPlaceholder:Landroid/widget/EditText;

.field private mFaceReferenceHeight:I

.field private mFaceReferenceWidth:I

.field private mImgWatchFaceDetails:Landroid/widget/ImageView;

.field private mIsUpdatingStreamCredentials:Z

.field private mLandedSomewhere:Z

.field private mLandingCentralLeft:Landroid/view/View;

.field private mLandingCentralRight:Landroid/view/View;

.field private mLandingDelete:Landroid/widget/ImageView;

.field private mLandingLeftPage:Landroid/view/View;

.field private mLandingRightPage:Landroid/view/View;

.field private mLinkStateImage:Landroid/widget/TextView;

.field private mListener:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;

.field private mMediumAnimationDuration:I

.field private mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mMovedFromOriginalPosition:Z

.field private mNewCenterAfterDelete:I

.field private mPerformAction:Z

.field private mReplaceStream:Z

.field mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

.field private mStreamDroppedOnFace:Z

.field private mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

.field private mStreamToBeDeletedPosition:I

.field mSwappingHandler:Landroid/os/Handler;

.field mVibrationFAB:Lcom/software/shell/fab/ActionButton;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001e7
        }
    .end annotation
.end field

.field private mView:Landroid/view/View;

.field private mViewOverlay:Landroid/view/View;

.field private progressBar:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    const-class v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 276
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 179
    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    .line 180
    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    .line 185
    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 199
    iput v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    .line 200
    iput v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mNewCenterAfterDelete:I

    .line 201
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    .line 202
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandedSomewhere:Z

    .line 203
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mIsUpdatingStreamCredentials:Z

    .line 204
    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isUpdatingStreamCredentialsUuid:Ljava/lang/String;

    .line 205
    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedWatchFace:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 226
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mChangePageHandler:Landroid/os/Handler;

    .line 227
    new-instance v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    invoke-direct {v0, p0, v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;I)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageLeft:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    .line 228
    new-instance v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;I)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageRight:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    .line 230
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isPaused:Z

    .line 253
    new-instance v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$1;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    .line 1799
    new-instance v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$21;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$21;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContextModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    .line 278
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/StreamDownloadResponse;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/StreamDownloadResponse;
    .param p2, "x2"    # Lcom/vectorwatch/android/models/Stream;
    .param p3, "x3"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 145
    invoke-direct {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->onStreamSubscribeSuccess(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    return-void
.end method

.method static synthetic access$1102(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;)Lcom/vectorwatch/android/models/Stream;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

    return-object p1
.end method

.method static synthetic access$1302(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # I

    .prologue
    .line 145
    iput p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeletedPosition:I

    return p1
.end method

.method static synthetic access$1402(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # Landroid/widget/EditText;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;ILcom/vectorwatch/android/models/Element;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/vectorwatch/android/models/Element;
    .param p4, "x4"    # Landroid/view/View;

    .prologue
    .line 145
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->startStreamConfiguration(Lcom/vectorwatch/android/models/Stream;ILcom/vectorwatch/android/models/Element;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->closeStreamsSetupMode()V

    return-void
.end method

.method static synthetic access$1700(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mViewOverlay:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContainerFacePlaceholders:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mImgWatchFaceDetails:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setViewPagerCenter(IZ)V

    return-void
.end method

.method static synthetic access$2000(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->streamClearedFromFace(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    iget v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    return v0
.end method

.method static synthetic access$300()Lorg/slf4j/Logger;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->triggerSwitchToOnlineDialog()V

    return-void
.end method

.method static synthetic access$500(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->triggerSwitchToOfflineDialog()V

    return-void
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->watchFaceDroppedOnDelete()V

    return-void
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "x2"    # Lcom/vectorwatch/android/models/Stream;
    .param p3, "x3"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 145
    invoke-direct {p0, p1, p2, p3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->replaceActiveStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    return-void
.end method

.method static synthetic access$900(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isUpdatingStreamCredentialsUuid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isUpdatingStreamCredentialsUuid:Ljava/lang/String;

    return-object p1
.end method

.method private addFacePlaceholders(Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 18
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 1953
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContainerFacePlaceholders:Landroid/widget/RelativeLayout;

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1954
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b00c5

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1955
    .local v4, "containerWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b00c5

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1957
    .local v2, "containerHeight":I
    int-to-float v13, v4

    move-object/from16 v0, p0

    iget v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mFaceReferenceWidth:I

    int-to-float v14, v14

    div-float v11, v13, v14

    .line 1958
    .local v11, "scaleX":F
    int-to-float v13, v2

    move-object/from16 v0, p0

    iget v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mFaceReferenceHeight:I

    int-to-float v14, v14

    div-float v12, v13, v14

    .line 1961
    .local v12, "scaleY":F
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getElementsAssociatedToApp(ILandroid/content/Context;)Ljava/util/List;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mElementsList:Ljava/util/List;

    .line 1963
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mElementsList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vectorwatch/android/models/Element;

    .line 1964
    .local v7, "element":Lcom/vectorwatch/android/models/Element;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    const v15, 0x7f030078

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1967
    .local v3, "containerPlaceHolder":Landroid/widget/LinearLayout;
    const v14, 0x7f100225

    invoke-virtual {v3, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    .line 1968
    .local v6, "editText":Landroid/widget/EditText;
    const/4 v14, 0x0

    invoke-virtual {v6, v14}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1970
    const-string v14, ""

    invoke-virtual {v6, v14}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1972
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v14

    invoke-virtual {v6, v14}, Landroid/widget/EditText;->setId(I)V

    .line 1973
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getMaxWidth()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v11

    float-to-int v14, v14

    invoke-virtual {v6, v14}, Landroid/widget/EditText;->setWidth(I)V

    .line 1975
    const v14, 0x7f10000c

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/widget/EditText;->setTag(ILjava/lang/Object;)V

    .line 1977
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v14, -0x2

    const/4 v15, -0x2

    invoke-direct {v8, v14, v15}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1983
    .local v8, "etParams":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-static {v14}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "round"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1984
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getPlacement()Lcom/vectorwatch/android/models/PlacementPosition;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/PlacementPosition;->getPosY()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    const/4 v15, -0x1

    if-eq v14, v15, :cond_3

    .line 1985
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getPlacement()Lcom/vectorwatch/android/models/PlacementPosition;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/PlacementPosition;->getPosY()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v12

    float-to-int v14, v14

    iput v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1997
    :goto_1
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getPlacement()Lcom/vectorwatch/android/models/PlacementPosition;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/PlacementPosition;->getPosX()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    const/4 v15, -0x1

    if-eq v14, v15, :cond_6

    .line 1998
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getPlacement()Lcom/vectorwatch/android/models/PlacementPosition;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/PlacementPosition;->getPosX()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v11

    float-to-int v14, v14

    .line 1999
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0b00b6

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    sub-int/2addr v14, v15

    add-int/lit8 v14, v14, -0x14

    iput v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2009
    :cond_1
    :goto_2
    iget v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/16 v15, 0x12c

    if-le v14, v15, :cond_2

    .line 2010
    const/4 v10, 0x4

    .line 2011
    .local v10, "maxLength":I
    const/4 v14, 0x1

    new-array v9, v14, [Landroid/text/InputFilter;

    .line 2012
    .local v9, "fArray":[Landroid/text/InputFilter;
    const/4 v14, 0x0

    new-instance v15, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v15, v10}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v15, v9, v14

    .line 2013
    invoke-virtual {v6, v9}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 2015
    .end local v9    # "fArray":[Landroid/text/InputFilter;
    .end local v10    # "maxLength":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContainerFacePlaceholders:Landroid/widget/RelativeLayout;

    invoke-virtual {v14, v3, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2017
    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/widget/EditText;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2019
    const v14, 0x7f100224

    invoke-virtual {v3, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 2020
    .local v5, "deleteButton":Landroid/widget/ImageView;
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v14

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setId(I)V

    .line 2021
    const v14, 0x7f02016d

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2022
    const v14, 0x7f10000d

    invoke-virtual {v6}, Landroid/widget/EditText;->getId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v5, v14, v15}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 2023
    const/4 v14, 0x4

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2025
    const v14, 0x7f10000e

    invoke-virtual {v5}, Landroid/widget/ImageView;->getId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/widget/EditText;->setTag(ILjava/lang/Object;)V

    .line 2027
    new-instance v14, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$25;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$25;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2040
    invoke-virtual/range {p1 .. p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v14

    const/4 v15, 0x0

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v17

    invoke-static/range {v14 .. v17}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamActiveOnPlaceholder(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v1

    .line 2041
    .local v1, "activeStream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v1, :cond_0

    .line 2042
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v1, v14}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setUpStreamPlaceholder(Landroid/widget/EditText;Lcom/vectorwatch/android/models/Stream;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 1987
    .end local v1    # "activeStream":Lcom/vectorwatch/android/models/Stream;
    .end local v5    # "deleteButton":Landroid/widget/ImageView;
    :cond_3
    div-int/lit8 v14, v2, 0x2

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getMaxHeight()Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    int-to-float v15, v15

    mul-float/2addr v15, v12

    float-to-int v15, v15

    sub-int/2addr v14, v15

    iput v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_1

    .line 1990
    :cond_4
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getPlacement()Lcom/vectorwatch/android/models/PlacementPosition;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/PlacementPosition;->getPosY()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    const/4 v15, -0x1

    if-eq v14, v15, :cond_5

    .line 1991
    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getPlacement()Lcom/vectorwatch/android/models/PlacementPosition;

    move-result-object v14

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/PlacementPosition;->getPosY()Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    int-to-float v14, v14

    mul-float/2addr v14, v12

    float-to-int v14, v14

    iput v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_1

    .line 1993
    :cond_5
    div-int/lit8 v14, v2, 0x2

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getMaxHeight()Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    int-to-float v15, v15

    mul-float/2addr v15, v12

    float-to-int v15, v15

    sub-int/2addr v14, v15

    iput v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_1

    .line 2002
    :cond_6
    div-int/lit8 v14, v4, 0x2

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/Element;->getMaxWidth()Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    int-to-float v15, v15

    mul-float/2addr v15, v11

    float-to-int v15, v15

    sub-int/2addr v14, v15

    .line 2003
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0b00b6

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    sub-int/2addr v14, v15

    iput v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2004
    iget v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v14, :cond_1

    .line 2005
    const/4 v14, 0x0

    iput v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2006
    iget v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iput v14, v8, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_2

    .line 2045
    .end local v3    # "containerPlaceHolder":Landroid/widget/LinearLayout;
    .end local v6    # "editText":Landroid/widget/EditText;
    .end local v7    # "element":Lcom/vectorwatch/android/models/Element;
    .end local v8    # "etParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_7
    return-void
.end method

.method private closeStreamsSetupMode()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1900
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    .line 1901
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 1902
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setLandingZonesForDrag(Z)V

    .line 1903
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 1904
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mImgWatchFaceDetails:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1906
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mViewOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1907
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContainerFacePlaceholders:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1908
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContainerFacePlaceholders:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1910
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mImgWatchFaceDetails:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    .line 1911
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 1914
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 1915
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    .line 1916
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$23;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$23;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 1917
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1925
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f04000a

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1927
    .local v0, "bottomDown":Landroid/view/animation/Animation;
    new-instance v1, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$24;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$24;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1943
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1945
    .end local v0    # "bottomDown":Landroid/view/animation/Animation;
    :cond_0
    return-void
.end method

.method private createStreamsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/Stream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2095
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/managers/StreamsManager;->getSavedStreams(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getElement(I)Lcom/vectorwatch/android/models/Element;
    .locals 3
    .param p1, "elementId"    # I

    .prologue
    .line 2054
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mElementsList:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 2055
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mElementsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/Element;

    .line 2056
    .local v0, "element":Lcom/vectorwatch/android/models/Element;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 2061
    .end local v0    # "element":Lcom/vectorwatch/android/models/Element;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private needsLogin()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1711
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 1712
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    const-string v3, "com.vectorwatch.android"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 1714
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v3, v1

    if-ge v3, v2, :cond_0

    .line 1715
    const-string v3, "Store-Apps:"

    const-string v4, "There should be at least one account in Accounts & sync"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1719
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static newInstance()Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    .locals 2

    .prologue
    .line 287
    new-instance v1, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;

    invoke-direct {v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;-><init>()V

    .line 288
    .local v1, "fragment":Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 289
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 290
    return-object v1
.end method

.method private onStreamSubscribeSuccess(Lcom/vectorwatch/android/models/StreamDownloadResponse;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 13
    .param p1, "streamDownloadResponse"    # Lcom/vectorwatch/android/models/StreamDownloadResponse;
    .param p2, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p3, "subscriptionModel"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 1522
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/StreamDownloadResponse;->getData()Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/StreamDownloadResponse$DownloadedStreamData;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v12

    .line 1523
    .local v12, "responseStream":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getStreamData()Lcom/vectorwatch/android/models/StreamValueModel;

    move-result-object v5

    .line 1524
    .local v5, "modelData":Lcom/vectorwatch/android/models/StreamValueModel;
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v7

    .line 1525
    .local v7, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Stream - standalone - channel label - "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vectorwatch/android/models/StreamChannelSettings;->getUniqueLabel()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1529
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/vectorwatch/android/models/Stream;->setChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 1531
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1532
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->MOBILE_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1537
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1538
    .local v2, "appId":I
    :goto_0
    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1539
    .local v3, "faceId":I
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1540
    .local v4, "elementId":I
    :goto_2
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    iget-object v6, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    move-object v0, p2

    invoke-static/range {v0 .. v6}, Lcom/vectorwatch/android/managers/StreamsManager;->onPostExecuteSubscribeStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILcom/vectorwatch/android/models/StreamValueModel;Landroid/content/Context;)V

    .line 1552
    .end local v2    # "appId":I
    .end local v3    # "faceId":I
    .end local v4    # "elementId":I
    :cond_1
    :goto_3
    return-void

    .line 1537
    :cond_2
    const/4 v2, -0x1

    goto :goto_0

    .line 1538
    .restart local v2    # "appId":I
    :cond_3
    const/4 v3, -0x1

    goto :goto_1

    .line 1539
    .restart local v3    # "faceId":I
    :cond_4
    const/4 v4, -0x1

    goto :goto_2

    .line 1542
    .end local v2    # "appId":I
    .end local v3    # "faceId":I
    :cond_5
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/vectorwatch/android/models/Stream$StreamTypes;->WATCH_SOURCE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1545
    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getAppId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1546
    .restart local v2    # "appId":I
    :goto_4
    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getWatchFaceId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1547
    .restart local v3    # "faceId":I
    :goto_5
    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual/range {p3 .. p3}, Lcom/vectorwatch/android/models/StreamPlacementModel;->getElementId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1549
    .restart local v4    # "elementId":I
    :goto_6
    iget-object v11, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    move-object v6, p2

    move v8, v2

    move v9, v3

    move v10, v4

    invoke-static/range {v6 .. v11}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->activateStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;IIILandroid/content/Context;)V

    goto :goto_3

    .line 1545
    .end local v2    # "appId":I
    .end local v3    # "faceId":I
    .end local v4    # "elementId":I
    :cond_6
    const/4 v2, -0x1

    goto :goto_4

    .line 1546
    .restart local v2    # "appId":I
    :cond_7
    const/4 v3, -0x1

    goto :goto_5

    .line 1547
    .restart local v3    # "faceId":I
    :cond_8
    const/4 v4, -0x1

    goto :goto_6
.end method

.method private replaceActiveStream(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V
    .locals 4
    .param p1, "activeStream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p3, "subscriptionModel"    # Lcom/vectorwatch/android/models/StreamPlacementModel;

    .prologue
    .line 1470
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v0

    .line 1471
    .local v0, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setUniqueLabel(Ljava/lang/String;)V

    .line 1473
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;

    invoke-direct {v3, p0, p2, p3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$19;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    invoke-static {v1, v2, p3, v0, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->subscribeStream(Landroid/content/Context;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPlacementModel;Lcom/vectorwatch/android/models/StreamChannelSettings;Lretrofit/Callback;)V

    .line 1512
    return-void
.end method

.method private requestAdditionalStreamDataFromUser(Lcom/vectorwatch/android/models/Stream;Ljava/lang/Integer;Lcom/vectorwatch/android/models/Element;)V
    .locals 7
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "streamPosInList"    # Ljava/lang/Integer;
    .param p3, "elem"    # Lcom/vectorwatch/android/models/Element;

    .prologue
    .line 1145
    new-instance v6, Lcom/google/gson/Gson;

    invoke-direct {v6}, Lcom/google/gson/Gson;-><init>()V

    .line 1146
    .local v6, "gson":Lcom/google/gson/Gson;
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Settings object stream."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1148
    if-nez p1, :cond_0

    .line 1149
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    const-string v1, "stream is null."

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1154
    :goto_0
    return-void

    .line 1153
    :cond_0
    new-instance v0, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p3}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->progressBar:Landroid/view/View;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;-><init>(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;IILandroid/view/View;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private setBatteryIndicator(Landroid/view/View;I)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "progress"    # I

    .prologue
    const/16 v8, 0x55

    const/16 v7, 0x3c

    const/16 v6, 0x23

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1724
    const v2, 0x7f1001e4

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    .line 1726
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v5, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 1727
    .local v1, "dip":F
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1729
    .local v0, "batteryColor":I
    if-lt p2, v8, :cond_1

    .line 1730
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryImage:Landroid/widget/ImageView;

    const v3, 0x7f020069

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1741
    :goto_0
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    if-eqz v2, :cond_0

    .line 1742
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    const/16 v3, 0x18

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokesNo(I)V

    .line 1743
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v1

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBStrokeWidth(I)V

    .line 1744
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    const/high16 v3, -0x40000000    # -2.0f

    mul-float/2addr v3, v1

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeAStrokeWidth(I)V

    .line 1745
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeAPercent(I)V

    .line 1746
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBPercent(I)V

    .line 1747
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {v2, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBColor(I)V

    .line 1748
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {v2, v0}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeAColor(I)V

    .line 1749
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {v2, v5}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setDrawAllBSpokes(Z)V

    .line 1750
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {v2, v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setClickable(Z)V

    .line 1751
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {v2, v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBPadding(I)V

    .line 1752
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {v2, v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeAPadding(I)V

    .line 1753
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    invoke-virtual {v2, v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeARadiusPercent(I)V

    .line 1754
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    const/16 v3, 0x28

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->setSpokeBRadiusPercent(I)V

    .line 1756
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryProgress:Lcom/vectorwatch/android/ui/view/widgets/SpokeView;

    const/16 v3, 0xc8

    const/16 v4, 0x1f4

    invoke-virtual {v2, p2, v3, v4}, Lcom/vectorwatch/android/ui/view/widgets/SpokeView;->startAnimation(III)V

    .line 1758
    :cond_0
    return-void

    .line 1731
    :cond_1
    if-lt p2, v7, :cond_2

    if-ge p2, v8, :cond_2

    .line 1732
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryImage:Landroid/widget/ImageView;

    const v3, 0x7f020066

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1733
    :cond_2
    if-lt p2, v6, :cond_3

    if-ge p2, v7, :cond_3

    .line 1734
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryImage:Landroid/widget/ImageView;

    const v3, 0x7f020065

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1735
    :cond_3
    const/16 v2, 0xa

    if-lt p2, v2, :cond_4

    if-ge p2, v6, :cond_4

    .line 1736
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryImage:Landroid/widget/ImageView;

    const v3, 0x7f020064

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 1738
    :cond_4
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryImage:Landroid/widget/ImageView;

    const v3, 0x7f020068

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method

.method private setLandingZonesForDrag(Z)V
    .locals 3
    .param p1, "on"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1781
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingLeftPage:Landroid/view/View;

    if-eqz p1, :cond_0

    move-object v0, p0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1782
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingRightPage:Landroid/view/View;

    if-eqz p1, :cond_1

    move-object v0, p0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1783
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    move-object v0, p0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1784
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralLeft:Landroid/view/View;

    if-eqz p1, :cond_3

    move-object v0, p0

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1785
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralRight:Landroid/view/View;

    if-eqz p1, :cond_4

    .end local p0    # "this":Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    :goto_4
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1786
    return-void

    .restart local p0    # "this":Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;
    :cond_0
    move-object v0, v1

    .line 1781
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 1782
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1783
    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 1784
    goto :goto_3

    :cond_4
    move-object p0, v1

    .line 1785
    goto :goto_4
.end method

.method private setUpLinkLostState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2102
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 2103
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2105
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_offline_badge"

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    .line 2106
    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2107
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2108
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f0901d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2114
    :cond_0
    :goto_0
    return-void

    .line 2111
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2112
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setUpStreamPlaceholder(Landroid/widget/EditText;Lcom/vectorwatch/android/models/Stream;Ljava/lang/Integer;)V
    .locals 11
    .param p1, "etPlaceholder"    # Landroid/widget/EditText;
    .param p2, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p3, "streamPosition"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x0

    .line 1611
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "flag_show_stream_tutorial"

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getFlagTutorial(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v10

    .line 1613
    .local v10, "needsStreamTutorial":Z
    if-eqz v10, :cond_0

    .line 1614
    const v0, 0x7f0900b2

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xbb8

    .line 1615
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 1614
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1617
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "flag_show_stream_tutorial"

    invoke-static {v0, v3, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setFlagTutorial(Landroid/content/Context;ZLjava/lang/String;)V

    .line 1621
    :cond_0
    const v0, 0x7f10000e

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 1622
    .local v7, "deleteButtonId":Ljava/lang/Integer;
    if-nez v7, :cond_2

    .line 1647
    :cond_1
    :goto_0
    return-void

    .line 1625
    :cond_2
    invoke-virtual {p1}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 1626
    .local v6, "deleteButton":Landroid/widget/ImageView;
    if-eqz v6, :cond_1

    .line 1629
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Stream;->getLabelText()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const v0, 0x7f0901c5

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1631
    .local v9, "label":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1632
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setSelected(Z)V

    .line 1633
    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setGravity(I)V

    .line 1634
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    .line 1635
    const v0, 0x7f100010

    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/EditText;->setTag(ILjava/lang/Object;)V

    .line 1636
    const v0, 0x7f10000f

    invoke-virtual {p1, v0, p2}, Landroid/widget/EditText;->setTag(ILjava/lang/Object;)V

    .line 1637
    invoke-virtual {p1, p3}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 1638
    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1639
    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setSoundEffectsEnabled(Z)V

    .line 1641
    const v0, 0x7f10000c

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 1643
    .local v8, "elementId":Ljava/lang/Integer;
    if-eqz v8, :cond_1

    .line 1645
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getElement(I)Lcom/vectorwatch/android/models/Element;

    move-result-object v4

    .line 1646
    .local v4, "element":Lcom/vectorwatch/android/models/Element;
    new-instance v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v1, p0

    move-object v2, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$OnClickStreamListener;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;ILcom/vectorwatch/android/models/Element;Landroid/widget/EditText;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1629
    .end local v4    # "element":Lcom/vectorwatch/android/models/Element;
    .end local v8    # "elementId":Ljava/lang/Integer;
    .end local v9    # "label":Ljava/lang/String;
    :cond_3
    invoke-virtual {p2}, Lcom/vectorwatch/android/models/Stream;->getLabelText()Ljava/lang/String;

    move-result-object v9

    goto :goto_1
.end method

.method private setViewPagerCenter(IZ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "animated"    # Z

    .prologue
    .line 1790
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$20;

    invoke-direct {v1, p0, p1, p2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$20;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;IZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1796
    return-void
.end method

.method private startStreamConfiguration(Lcom/vectorwatch/android/models/Stream;ILcom/vectorwatch/android/models/Element;Landroid/view/View;)V
    .locals 11
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "streamPosition"    # I
    .param p3, "element"    # Lcom/vectorwatch/android/models/Element;
    .param p4, "streamView"    # Landroid/view/View;

    .prologue
    .line 1085
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getDynamic()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1086
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamDroppedOnFace:Z

    .line 1087
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->requestAdditionalStreamDataFromUser(Lcom/vectorwatch/android/models/Stream;Ljava/lang/Integer;Lcom/vectorwatch/android/models/Element;)V

    .line 1142
    :goto_0
    return-void

    .line 1088
    :cond_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1089
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getRenderOptions()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1090
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamDroppedOnFace:Z

    .line 1091
    invoke-virtual {p3}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->startStreamSettingsActivity(Lcom/vectorwatch/android/models/Stream;II)V

    goto :goto_0

    .line 1093
    :cond_2
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v0, :cond_7

    .line 1095
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    if-eqz v0, :cond_3

    .line 1096
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p3}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v0, v2, v3, v4, v5}, Lcom/vectorwatch/android/managers/StreamsManager;->removeStreamFromFace(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Z

    .line 1097
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v0

    const/4 v2, 0x0

    .line 1098
    invoke-virtual {p3}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v3

    .line 1097
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/vectorwatch/android/managers/StreamsManager;->getWatchDisableStreamData(III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v10

    .line 1100
    .local v10, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    .line 1104
    .end local v10    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    :cond_3
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1105
    new-instance v1, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-direct {v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;-><init>()V

    .line 1110
    .local v1, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    :goto_1
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getDynamic()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    if-nez v0, :cond_5

    .line 1111
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    const-string v2, "Stream defaults null on stream dropped on face"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1113
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    const v2, 0x7f10000e

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 1114
    .local v9, "deleteButtonId":Ljava/lang/Integer;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 1115
    .local v8, "deleteButton":Landroid/widget/ImageView;
    invoke-virtual {v8}, Landroid/widget/ImageView;->performClick()Z

    .line 1117
    const v0, 0x7f09023b

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x5dc

    .line 1118
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 1117
    invoke-static {v0, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1140
    .end local v1    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v8    # "deleteButton":Landroid/widget/ImageView;
    .end local v9    # "deleteButtonId":Ljava/lang/Integer;
    :goto_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    goto/16 :goto_0

    .line 1107
    :cond_4
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getChannelSettings()Lcom/vectorwatch/android/models/StreamChannelSettings;

    move-result-object v1

    .restart local v1    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    goto :goto_1

    .line 1119
    :cond_5
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getDefaults()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1120
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getDefaults()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setUserSettings(Ljava/util/Map;)V

    .line 1121
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v3

    const/4 v4, 0x0

    .line 1122
    invoke-virtual {p3}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    move-object v0, p1

    .line 1121
    invoke-static/range {v0 .. v7}, Lcom/vectorwatch/android/managers/StreamsManager;->placeStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIIZLandroid/content/Context;)V

    goto :goto_2

    .line 1124
    :cond_6
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    const-string v2, "Stream defaults null on stream dropped on face"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1126
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    const v2, 0x7f10000e

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 1127
    .restart local v9    # "deleteButtonId":Ljava/lang/Integer;
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 1128
    .restart local v8    # "deleteButton":Landroid/widget/ImageView;
    invoke-virtual {v8}, Landroid/widget/ImageView;->performClick()Z

    .line 1130
    const v0, 0x7f09023b

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x5dc

    .line 1131
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 1130
    invoke-static {v0, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_2

    .line 1134
    .end local v1    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v8    # "deleteButton":Landroid/widget/ImageView;
    .end local v9    # "deleteButtonId":Ljava/lang/Integer;
    :cond_7
    const v0, 0x7f09023b

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x5dc

    .line 1135
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 1134
    invoke-static {v0, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1137
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    const-string v2, "mCrtApp is null! Should not happen!"

    invoke-interface {v0, v2}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private startStreamSettingsActivity(Lcom/vectorwatch/android/models/Stream;II)V
    .locals 6
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;
    .param p2, "streamPosInList"    # I
    .param p3, "elementId"    # I

    .prologue
    .line 1157
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getStreamPossibleSettings()Lcom/vectorwatch/android/models/settings/PossibleSettings;

    move-result-object v2

    .line 1158
    .local v2, "streamPossibleSettings":Lcom/vectorwatch/android/models/settings/PossibleSettings;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getSettings()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1159
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/settings/PossibleSettings;->getRenderOptions()Ljava/util/Map;

    move-result-object v4

    if-nez v4, :cond_1

    .line 1160
    :cond_0
    sget-object v4, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    const-string v5, "Settings object is null for this stream."

    invoke-interface {v4, v5}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1172
    :goto_0
    return-void

    .line 1163
    :cond_1
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 1164
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v0, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1165
    .local v1, "json":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1166
    .local v3, "streamSettings":Landroid/content/Intent;
    const-string v4, "stream_json"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1167
    const-string v4, "stream_place_id"

    invoke-virtual {v3, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1168
    const-string v4, "watchface_elem_id"

    invoke-virtual {v3, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1169
    const-string v4, "stream_uuid"

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1170
    const-string v4, "STREAM_DROPPED"

    iget-boolean v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamDroppedOnFace:Z

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1171
    const/16 v4, 0xc9

    invoke-virtual {p0, v3, v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private startStreamSetupMode(Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 10
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    const/4 v9, 0x0

    .line 1835
    if-nez p1, :cond_1

    .line 1836
    sget-object v5, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    const-string v6, "Trying to enter stream setup mode for a null app."

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 1894
    :cond_0
    :goto_0
    return-void

    .line 1840
    :cond_1
    invoke-direct {p0, v9}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setLandingZonesForDrag(Z)V

    .line 1842
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v7, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_TAPPED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 1843
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v8

    .line 1842
    invoke-static {v5, v6, v7, v8}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 1845
    const/4 v4, 0x0

    .line 1846
    .local v4, "image":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getEditImg()Ljava/lang/String;

    move-result-object v4

    .line 1848
    if-nez v4, :cond_2

    .line 1849
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getImage()Ljava/lang/String;

    move-result-object v4

    .line 1852
    :cond_2
    if-eqz v4, :cond_0

    .line 1856
    invoke-static {v4, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 1857
    .local v2, "decodedString":[B
    array-length v5, v2

    invoke-static {v2, v9, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1859
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mListener:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;

    iget-object v6, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v6}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentWatchFace()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v6

    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v7}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentItem()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;->appSelected(Lcom/vectorwatch/android/models/CloudElementSummary;I)V

    .line 1861
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mImgWatchFaceDetails:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1862
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mImgWatchFaceDetails:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1863
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mImgWatchFaceDetails:Landroid/widget/ImageView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1865
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContainerFacePlaceholders:Landroid/widget/RelativeLayout;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1866
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->addFacePlaceholders(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 1868
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00c5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    .line 1869
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b00c7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    .line 1871
    .local v3, "detailsSizeFactor":F
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mImgWatchFaceDetails:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    const-wide/16 v6, 0x1f4

    .line 1872
    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    .line 1874
    invoke-static {}, Lcom/vectorwatch/android/utils/UIUtils;->hasLollipop()Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v5, Landroid/view/animation/AnimationUtils;

    invoke-direct {v5}, Landroid/view/animation/AnimationUtils;-><init>()V

    .line 1875
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v7, 0x10c000d

    invoke-static {v5, v7}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v5

    .line 1873
    :goto_1
    invoke-virtual {v6, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 1877
    invoke-virtual {v5, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    .line 1878
    invoke-virtual {v5, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    .line 1879
    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    new-instance v6, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$22;

    invoke-direct {v6, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$22;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 1880
    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1889
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v6, 0x7f04000b

    invoke-static {v5, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1891
    .local v1, "bottomUp":Landroid/view/animation/Animation;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    invoke-virtual {v5, v1}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1893
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    invoke-virtual {v5, v9}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1875
    .end local v1    # "bottomUp":Landroid/view/animation/Animation;
    :cond_3
    new-instance v5, Landroid/view/animation/AnimationUtils;

    invoke-direct {v5}, Landroid/view/animation/AnimationUtils;-><init>()V

    .line 1876
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v7, 0x10c000b

    invoke-static {v5, v7}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v5

    goto :goto_1
.end method

.method private streamClearedFromFace(Landroid/view/View;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x0

    .line 1574
    move-object v2, p1

    check-cast v2, Landroid/widget/EditText;

    .line 1575
    .local v2, "etPlaceholder":Landroid/widget/EditText;
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1576
    const v7, 0x7f10000c

    invoke-virtual {v2, v7}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1577
    .local v1, "elementId":Ljava/lang/Integer;
    if-nez v1, :cond_1

    .line 1608
    :cond_0
    :goto_0
    return-void

    .line 1581
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-direct {p0, v7}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getElement(I)Lcom/vectorwatch/android/models/Element;

    move-result-object v0

    .line 1583
    .local v0, "element":Lcom/vectorwatch/android/models/Element;
    const v7, 0x7f100010

    invoke-virtual {v2, v7}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1584
    .local v5, "streamName":Ljava/lang/String;
    invoke-virtual {v2, v10}, Landroid/widget/EditText;->setSelected(Z)V

    .line 1587
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 1588
    .local v6, "streamPosition":Ljava/lang/Integer;
    if-eqz v6, :cond_0

    .line 1593
    const v7, 0x7f10000f

    invoke-virtual {p1, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/Stream;

    .line 1595
    .local v4, "stream":Lcom/vectorwatch/android/models/Stream;
    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v7, :cond_2

    .line 1596
    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v7

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v4, v7, v10, v8, v9}, Lcom/vectorwatch/android/managers/StreamsManager;->removeStreamFromFace(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Z

    .line 1597
    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v7

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v7, v10, v8}, Lcom/vectorwatch/android/managers/StreamsManager;->getWatchDisableStreamData(III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v3

    .line 1599
    .local v3, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    goto :goto_0

    .line 1601
    .end local v3    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    :cond_2
    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_0

    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1602
    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v7

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v4, v7, v10, v8, v9}, Lcom/vectorwatch/android/managers/StreamsManager;->removeStreamFromFace(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Z

    .line 1603
    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v7}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v7

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v7, v10, v8}, Lcom/vectorwatch/android/managers/StreamsManager;->getWatchDisableStreamData(III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v3

    .line 1605
    .restart local v3    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method private streamDroppedOnFace(Landroid/view/View;Landroid/view/View;)V
    .locals 10
    .param p1, "streamView"    # Landroid/view/View;
    .param p2, "placeholderView"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1024
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 1025
    .local v4, "streamPosition":Ljava/lang/Integer;
    if-nez v4, :cond_1

    .line 1073
    .end local p2    # "placeholderView":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 1027
    .restart local p2    # "placeholderView":Landroid/view/View;
    :cond_1
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->getItem(I)Lcom/vectorwatch/android/models/Stream;

    move-result-object v3

    .line 1029
    .local v3, "stream":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/vectorwatch/android/utils/NetworkUtils;->isOnline(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getStreamType()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/vectorwatch/android/models/Stream$StreamTypes;->STANDALONE:Lcom/vectorwatch/android/models/Stream$StreamTypes;

    .line 1030
    invoke-virtual {v6}, Lcom/vectorwatch/android/models/Stream$StreamTypes;->getVal()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1031
    const v5, 0x7f09023c

    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x5dc

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto :goto_0

    .line 1034
    :cond_2
    check-cast p2, Landroid/widget/EditText;

    .end local p2    # "placeholderView":Landroid/view/View;
    iput-object p2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    .line 1036
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    const v6, 0x7f10000c

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1038
    .local v2, "elementId":Ljava/lang/Integer;
    if-eqz v2, :cond_0

    .line 1041
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v5}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getElement(I)Lcom/vectorwatch/android/models/Element;

    move-result-object v1

    .line 1042
    .local v1, "element":Lcom/vectorwatch/android/models/Element;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    invoke-virtual {v5, v9}, Landroid/widget/EditText;->setSelected(Z)V

    .line 1044
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v5, :cond_4

    .line 1045
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v5

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1046
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    .line 1045
    invoke-static {v5, v8, v6, v7}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamActiveOnPlaceholder(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    .line 1047
    .local v0, "activeStream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v0, :cond_3

    .line 1048
    iput-boolean v9, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    .line 1049
    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

    .line 1050
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeletedPosition:I

    .line 1065
    :cond_3
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    invoke-direct {p0, v5, v3, v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setUpStreamPlaceholder(Landroid/widget/EditText;Lcom/vectorwatch/android/models/Stream;Ljava/lang/Integer;)V

    .line 1067
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v7, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CUSTOMIZED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    .line 1068
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v8

    .line 1067
    invoke-static {v5, v6, v7, v8}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 1072
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {p0, v3, v5, v1, p1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->startStreamConfiguration(Lcom/vectorwatch/android/models/Stream;ILcom/vectorwatch/android/models/Element;Landroid/view/View;)V

    goto/16 :goto_0

    .line 1053
    .end local v0    # "activeStream":Lcom/vectorwatch/android/models/Stream;
    :cond_4
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_5

    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 1054
    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v5

    .line 1055
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    .line 1054
    invoke-static {v5, v8, v6, v7}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamActiveOnPlaceholder(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    .line 1056
    .restart local v0    # "activeStream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v0, :cond_3

    goto/16 :goto_0

    .line 1060
    .end local v0    # "activeStream":Lcom/vectorwatch/android/models/Stream;
    :cond_5
    sget-object v5, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    const-string v6, "Should never get here. Face should exist and it should be active in order to drop a stream"

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private toogleDeleteLanding(Z)V
    .locals 3
    .param p1, "active"    # Z

    .prologue
    .line 1678
    if-eqz p1, :cond_0

    .line 1679
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    const v1, 0x7f0200e8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1680
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    const/4 v1, 0x1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->performHapticFeedback(II)Z

    .line 1687
    :goto_0
    return-void

    .line 1685
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    const v1, 0x7f0200e9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private triggerSwitchToOfflineDialog()V
    .locals 3

    .prologue
    .line 487
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090248

    .line 488
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    new-instance v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$8;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$8;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 489
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    new-instance v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$7;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$7;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 497
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 504
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 505
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 506
    return-void
.end method

.method private triggerSwitchToOnlineDialog()V
    .locals 8

    .prologue
    .line 433
    const-string v4, "prefs_show_online"

    const-wide/16 v6, -0x1

    .line 434
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 433
    invoke-static {v4, v6, v7, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getLongPreference(Ljava/lang/String;JLandroid/content/Context;)J

    move-result-wide v2

    .line 435
    .local v2, "showOnlineTimestamp":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 437
    .local v0, "currentTimestamp":J
    sub-long v4, v0, v2

    const-wide/32 v6, 0x5265c00

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 438
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f090249

    .line 439
    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1040013

    new-instance v6, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;

    invoke-direct {v6, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$6;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 440
    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1040009

    new-instance v6, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$5;

    invoke-direct {v6, p0, v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$5;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;J)V

    .line 469
    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x0

    .line 478
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 479
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 481
    :cond_0
    return-void
.end method

.method private updateAppsLists()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2069
    sget-object v0, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    .line 2070
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 2069
    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getAppsWithGivenState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    .line 2072
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v0, :cond_1

    .line 2073
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getElementsAssociatedToApp(ILandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mElementsList:Ljava/util/List;

    .line 2079
    :cond_0
    :goto_0
    return-void

    .line 2075
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2076
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getElementsAssociatedToApp(ILandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mElementsList:Ljava/util/List;

    goto :goto_0
.end method

.method private watchFaceDroppedAfterPosition(ILandroid/view/View;)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1762
    iget v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 1778
    :goto_0
    return-void

    .line 1765
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedWatchFace:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v1, p2, v2, p1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->addViewAt(Landroid/view/View;Lcom/vectorwatch/android/models/CloudElementSummary;I)I

    .line 1766
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->cleanEmptyItems()V

    .line 1767
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->notifyDataSetChanged()V

    .line 1768
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 1772
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setViewPagerCenter(IZ)V

    .line 1774
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getAppsList()Ljava/util/List;

    move-result-object v0

    .line 1776
    .local v0, "cloudElementSummaryList":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mListener:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;->appPositionChangedInCarousel(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private watchFaceDroppedOnDelete()V
    .locals 5

    .prologue
    .line 1690
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/vectorwatch/android/VectorApplication;

    check-cast v2, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->isInstalling()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1691
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09005d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x5dc

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 1708
    :goto_0
    return-void

    .line 1695
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedWatchFace:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1696
    .local v1, "watchFace":Lcom/vectorwatch/android/models/CloudElementSummary;
    iget v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    .line 1698
    .local v0, "position":I
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->notifyDataSetChanged()V

    .line 1699
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 1700
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mListener:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;

    invoke-virtual {v2, v1, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;->appRemovedFromCarousel(Lcom/vectorwatch/android/models/CloudElementSummary;I)V

    .line 1702
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    iget v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mNewCenterAfterDelete:I

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getWatchFace(I)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1705
    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v2

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vectorwatch/android/managers/StreamsManager;->clearStreamsFromUninstalledApp(ILandroid/content/Context;)V

    .line 1707
    iget v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mNewCenterAfterDelete:I

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setViewPagerCenter(IZ)V

    goto :goto_0
.end method


# virtual methods
.method protected cancelCurrentStream()V
    .locals 5

    .prologue
    .line 1555
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    if-eqz v2, :cond_0

    .line 1556
    iget-boolean v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    if-nez v2, :cond_2

    .line 1557
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    const v3, 0x7f10000e

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1558
    .local v1, "deleteButtonId":Ljava/lang/Integer;
    if-nez v1, :cond_1

    .line 1571
    .end local v1    # "deleteButtonId":Ljava/lang/Integer;
    :cond_0
    :goto_0
    return-void

    .line 1561
    .restart local v1    # "deleteButtonId":Ljava/lang/Integer;
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1562
    .local v0, "deleteButton":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 1565
    invoke-virtual {v0}, Landroid/widget/ImageView;->performClick()Z

    goto :goto_0

    .line 1567
    .end local v0    # "deleteButton":Landroid/widget/ImageView;
    .end local v1    # "deleteButtonId":Ljava/lang/Integer;
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    .line 1568
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mEtPlaceholder:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

    iget v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeletedPosition:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setUpStreamPlaceholder(Landroid/widget/EditText;Lcom/vectorwatch/android/models/Stream;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public handleAppAuthExpiredEvent(Lcom/vectorwatch/android/events/AppAuthExpiredEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppAuthExpiredEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2463
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isPaused:Z

    if-nez v0, :cond_0

    .line 2464
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2465
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->setAuthInvalidated(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 2466
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/events/AppListReloadEvent;

    invoke-direct {v1}, Lcom/vectorwatch/android/events/AppListReloadEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/MainThreadBus;->post(Ljava/lang/Object;)V

    .line 2467
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppAuthExpiredEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->triggerAuthCredentialsChangeForApp(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 2470
    :cond_0
    return-void
.end method

.method public handleAppListReload(Lcom/vectorwatch/android/events/AppListReloadEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppListReloadEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2083
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMemoryCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 2084
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppListReloadEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2085
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppListReloadEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/UIUtils;->getCloudAppFaceCacheKey(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2086
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppListReloadEvent;->getApp()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    sget-object v2, Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;->WATCHMAKER_EDIT_IMAGE:Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;

    invoke-static {v1, v2}, Lcom/vectorwatch/android/utils/UIUtils;->getCloudAppFaceCacheKey(Lcom/vectorwatch/android/models/CloudElementSummary;Lcom/vectorwatch/android/utils/UIUtils$WatchfaceImageType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2089
    :cond_0
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->updateAppsLists()V

    .line 2090
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->updateList(Ljava/util/ArrayList;)V

    .line 2091
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 2092
    return-void
.end method

.method public handleAppUninstalledFromWatchEvent(Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2254
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;->getElementSummary()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2255
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;->getElementSummary()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getAppPosition(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2257
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppUninstalledFromWatchEvent;->getElementSummary()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getAppPosition(I)I

    move-result v0

    .line 2259
    .local v0, "appPosition":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v1, v2, v0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->removeView(Landroid/support/v4/view/ViewPager;I)I

    .line 2260
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->notifyDataSetChanged()V

    .line 2261
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 2262
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->invalidate()V

    .line 2264
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2266
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setViewPagerCenter(IZ)V

    .line 2268
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentWatchFace()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 2271
    .end local v0    # "appPosition":I
    :cond_0
    return-void
.end method

.method public handleBatteryInformationUpdateEvent(Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2198
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/BatteryInformationUpdateEvent;->getPercentage()B

    move-result v0

    .line 2199
    .local v0, "batteryPercentage":I
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 2200
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mView:Landroid/view/View;

    invoke-direct {p0, v1, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setBatteryIndicator(Landroid/view/View;I)V

    .line 2202
    :cond_0
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2236
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 2237
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2238
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2240
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefs_offline_badge"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    .line 2241
    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2242
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2243
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2250
    :cond_0
    :goto_0
    return-void

    .line 2246
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2247
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 2248
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2176
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 2177
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setUpLinkLostState()V

    .line 2178
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 2180
    :cond_0
    return-void
.end method

.method public declared-synchronized handleOnAppInstalledOnWatchEvent(Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;)V
    .locals 14
    .param p1, "event"    # Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2120
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;->getAppId()I

    move-result v8

    .line 2122
    .local v8, "appId":I
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getFullAppDetails(ILandroid/content/Context;)Lcom/vectorwatch/android/models/DownloadedAppDetails;

    move-result-object v10

    .line 2124
    .local v10, "fullApp":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/AppInstalledOnWatchEvent;->getPosition()I

    move-result v9

    .line 2125
    .local v9, "appPosition":I
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT INSTALL - WatchMakerFragment - handleOnAppInstalledOnWatchEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v10, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2128
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->updateAppsLists()V

    .line 2130
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 2131
    .local v11, "s":Ljava/lang/String;
    const-string v0, "round"

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2132
    const v12, 0x7f020103

    .line 2136
    .local v12, "watchImage":I
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v12}, Lcom/vectorwatch/android/utils/UIUtils;->getImageSize(Landroid/content/Context;I)[I

    move-result-object v13

    .line 2137
    .local v13, "watchImageSize":[I
    const/4 v0, 0x0

    aget v6, v13, v0

    .line 2138
    .local v6, "pageSizePx":I
    new-instance v0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMemoryCache:Landroid/util/LruCache;

    move-object v3, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;-><init>(Landroid/support/v4/view/ViewPager;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Landroid/content/Context;ILandroid/util/LruCache;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    .line 2141
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 2142
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setCurrentItem(I)V

    .line 2143
    invoke-virtual {p0, v9}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->onPageSelected(I)V

    .line 2145
    const/4 v0, 0x0

    invoke-direct {p0, v9, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setViewPagerCenter(IZ)V

    .line 2146
    sget-object v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT INSTALL - handleOnAppInstalledEvent: Set current item - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " element name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v10, Lcom/vectorwatch/android/models/DownloadedAppDetails;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    .line 2147
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2146
    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 2148
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingLeftPage:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2149
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingRightPage:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2150
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 2152
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 2155
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/vectorwatch/android/managers/CloudAppsManager;->appContainsDefaultStreams(Lcom/vectorwatch/android/models/DownloadedAppDetails;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2157
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->createStreamsList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->updateList(Ljava/util/List;)V

    .line 2158
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->setAdapter(Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2160
    :cond_0
    monitor-exit p0

    return-void

    .line 2134
    .end local v6    # "pageSizePx":I
    .end local v12    # "watchImage":I
    .end local v13    # "watchImageSize":[I
    :cond_1
    const v12, 0x7f020104

    .restart local v12    # "watchImage":I
    goto/16 :goto_0

    .line 2120
    .end local v8    # "appId":I
    .end local v9    # "appPosition":I
    .end local v10    # "fullApp":Lcom/vectorwatch/android/models/DownloadedAppDetails;
    .end local v11    # "s":Ljava/lang/String;
    .end local v12    # "watchImage":I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public handleRefreshWatchmakerUi(Lcom/vectorwatch/android/events/RefreshWatchmakerUi;)V
    .locals 12
    .param p1, "event"    # Lcom/vectorwatch/android/events/RefreshWatchmakerUi;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 2207
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->updateAppsLists()V

    .line 2210
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 2212
    .local v8, "s":Ljava/lang/String;
    const-string v0, "round"

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2213
    const v9, 0x7f020103

    .line 2218
    .local v9, "watchImage":I
    :goto_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v9}, Lcom/vectorwatch/android/utils/UIUtils;->getImageSize(Landroid/content/Context;I)[I

    move-result-object v10

    .line 2219
    .local v10, "watchImageSize":[I
    aget v6, v10, v11

    .line 2221
    .local v6, "pageSizePx":I
    new-instance v0, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMemoryCache:Landroid/util/LruCache;

    move-object v3, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;-><init>(Landroid/support/v4/view/ViewPager;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Landroid/content/Context;ILandroid/util/LruCache;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    .line 2224
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 2226
    const-string v0, "flag_first_paired_watch"

    .line 2227
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 2226
    invoke-static {v0, v11, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2228
    const-string v0, "flag_first_paired_watch"

    const/4 v1, 0x1

    .line 2229
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 2228
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 2230
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->recreate()V

    .line 2232
    :cond_0
    return-void

    .line 2215
    .end local v6    # "pageSizePx":I
    .end local v9    # "watchImage":I
    .end local v10    # "watchImageSize":[I
    :cond_1
    const v9, 0x7f020104

    .restart local v9    # "watchImage":I
    goto :goto_0
.end method

.method public handleStreamAuthExpiredEvent(Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2164
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamAuthExpiredEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->handleStreamCredentialsExpired(Lcom/vectorwatch/android/models/Stream;)V

    .line 2165
    return-void
.end method

.method protected handleStreamCredentialsExpired(Lcom/vectorwatch/android/models/Stream;)V
    .locals 6
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    const/4 v3, 0x1

    .line 2386
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->setAuthInvalidated(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 2387
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2388
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 2389
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f09016c

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090170

    new-instance v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$28;

    invoke-direct {v2, p0, p1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$28;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;)V

    .line 2390
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09016b

    new-instance v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$27;

    invoke-direct {v2, p0, p1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$27;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;)V

    .line 2395
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$26;

    invoke-direct {v1, p0, p1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$26;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;)V

    .line 2400
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027

    .line 2406
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 2407
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 2410
    return-void
.end method

.method protected handleStreamCredentialsExpired(Ljava/lang/String;)V
    .locals 2
    .param p1, "streamUuid"    # Ljava/lang/String;

    .prologue
    .line 2381
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    .line 2382
    .local v0, "stream":Lcom/vectorwatch/android/models/Stream;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->handleStreamCredentialsExpired(Lcom/vectorwatch/android/models/Stream;)V

    .line 2383
    return-void
.end method

.method public handleStreamDeletedEvent(Lcom/vectorwatch/android/events/StreamDeletedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamDeletedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2191
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->createStreamsList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->updateList(Ljava/util/List;)V

    .line 2192
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->setAdapter(Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;)V

    .line 2193
    return-void
.end method

.method public handleStreamDownloadCompletedEvent(Lcom/vectorwatch/android/events/StreamDownloadCompletedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamDownloadCompletedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2185
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->createStreamsList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;->updateList(Ljava/util/List;)V

    .line 2186
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->setAdapter(Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;)V

    .line 2187
    return-void
.end method

.method public handleStreamPossibleSettingsReceivedEvent(Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2169
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;->getStreamPosInList()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;->getElemId()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2170
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;->getStream()Lcom/vectorwatch/android/models/Stream;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;->getStreamPosInList()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/StreamPossibleSettingsReceivedEvent;->getElemId()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->startStreamSettingsActivity(Lcom/vectorwatch/android/models/Stream;II)V

    .line 2172
    :cond_0
    return-void
.end method

.method public handleTabSelectedEvent(Lcom/vectorwatch/android/events/TabSelectedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/vectorwatch/android/events/TabSelectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 2275
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/TabSelectedEvent;->getTabIndex()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    if-eqz v0, :cond_0

    .line 2276
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 2277
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    invoke-virtual {v0}, Landroid/support/v7/view/ActionMode;->finish()V

    .line 2279
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 34
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1176
    invoke-super/range {p0 .. p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1177
    const/4 v4, -0x1

    move/from16 v0, p2

    if-ne v0, v4, :cond_12

    if-eqz p3, :cond_12

    .line 1178
    const-string v4, "stream_pos"

    const/4 v5, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    .line 1179
    .local v28, "streamPosition":Ljava/lang/Integer;
    const-string v4, "elem_pos"

    const/4 v5, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 1181
    .local v17, "elementId":Ljava/lang/Integer;
    const-string v4, "result_map"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v24

    .line 1182
    .local v24, "result":Landroid/os/Bundle;
    const-string v4, "STREAM_DROPPED"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamDroppedOnFace:Z

    .line 1185
    packed-switch p1, :pswitch_data_0

    .line 1458
    .end local v17    # "elementId":Ljava/lang/Integer;
    .end local v24    # "result":Landroid/os/Bundle;
    .end local v28    # "streamPosition":Ljava/lang/Integer;
    :cond_0
    :goto_0
    return-void

    .line 1187
    .restart local v17    # "elementId":Ljava/lang/Integer;
    .restart local v24    # "result":Landroid/os/Bundle;
    .restart local v28    # "streamPosition":Ljava/lang/Integer;
    :pswitch_0
    if-eqz p3, :cond_0

    const-string v4, "result_map"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1190
    const-string v4, "stream_uuid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 1192
    .local v29, "streamUuid":Ljava/lang/String;
    invoke-virtual/range {v24 .. v24}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v25

    .line 1194
    .local v25, "settingNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v26, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-direct/range {v26 .. v26}, Lcom/vectorwatch/android/models/StreamChannelSettings;-><init>()V

    .line 1196
    .local v26, "settingsForChannel":Lcom/vectorwatch/android/models/StreamChannelSettings;
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 1197
    .local v15, "channelSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_c

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_c

    .line 1198
    if-eqz v25, :cond_4

    .line 1199
    invoke-interface/range {v25 .. v25}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 1201
    .local v19, "key":Ljava/lang/String;
    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v33

    .line 1202
    .local v33, "values":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 1204
    .local v2, "set":Lcom/vectorwatch/android/models/settings/Setting;
    move-object/from16 v0, v33

    array-length v4, v0

    const/4 v5, 0x4

    if-le v4, v5, :cond_2

    .line 1208
    new-instance v18, Lcom/google/gson/Gson;

    invoke-direct/range {v18 .. v18}, Lcom/google/gson/Gson;-><init>()V

    .line 1209
    .local v18, "gson":Lcom/google/gson/Gson;
    const/4 v4, 0x4

    aget-object v4, v33, v4

    const-class v5, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    .line 1212
    .local v23, "propertyValueModel":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    if-eqz v23, :cond_1

    .line 1213
    new-instance v2, Lcom/vectorwatch/android/models/settings/Setting;

    .end local v2    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    const/4 v4, 0x0

    aget-object v3, v33, v4

    const/4 v4, 0x1

    aget-object v4, v33, v4

    const/4 v5, 0x2

    aget-object v5, v33, v5

    const/4 v6, 0x3

    aget-object v6, v33, v6

    .line 1214
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual/range {v23 .. v23}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getPermissions()Ljava/util/List;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/vectorwatch/android/models/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V

    .line 1232
    .restart local v2    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    :goto_2
    move-object/from16 v0, v19

    invoke-interface {v15, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1216
    :cond_1
    new-instance v2, Lcom/vectorwatch/android/models/settings/Setting;

    .end local v2    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    const/4 v4, 0x0

    aget-object v4, v33, v4

    const/4 v5, 0x1

    aget-object v5, v33, v5

    const/4 v6, 0x2

    aget-object v6, v33, v6

    const/4 v7, 0x3

    aget-object v7, v33, v7

    .line 1217
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/vectorwatch/android/models/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .restart local v2    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    goto :goto_2

    .line 1222
    .end local v18    # "gson":Lcom/google/gson/Gson;
    .end local v23    # "propertyValueModel":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    :cond_2
    new-instance v18, Lcom/google/gson/Gson;

    invoke-direct/range {v18 .. v18}, Lcom/google/gson/Gson;-><init>()V

    .line 1223
    .restart local v18    # "gson":Lcom/google/gson/Gson;
    const/4 v4, 0x3

    aget-object v4, v33, v4

    const-class v5, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    .line 1226
    .restart local v23    # "propertyValueModel":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    if-eqz v23, :cond_3

    .line 1227
    new-instance v2, Lcom/vectorwatch/android/models/settings/Setting;

    .end local v2    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    const/4 v4, 0x0

    aget-object v4, v33, v4

    const/4 v5, 0x1

    aget-object v5, v33, v5

    const/4 v6, 0x2

    aget-object v6, v33, v6

    invoke-virtual/range {v23 .. v23}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getPermissions()Ljava/util/List;

    move-result-object v7

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/vectorwatch/android/models/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .restart local v2    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    goto :goto_2

    .line 1229
    :cond_3
    new-instance v2, Lcom/vectorwatch/android/models/settings/Setting;

    .end local v2    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    const/4 v4, 0x0

    aget-object v4, v33, v4

    const/4 v5, 0x1

    aget-object v5, v33, v5

    const/4 v6, 0x2

    aget-object v6, v33, v6

    invoke-direct {v2, v4, v5, v6}, Lcom/vectorwatch/android/models/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v2    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    goto :goto_2

    .line 1237
    .end local v2    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    .end local v18    # "gson":Lcom/google/gson/Gson;
    .end local v19    # "key":Ljava/lang/String;
    .end local v23    # "propertyValueModel":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    .end local v33    # "values":[Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamDroppedOnFace:Z

    if-eqz v4, :cond_6

    .line 1238
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-static {v0, v4}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v3

    .line 1247
    .local v3, "stream":Lcom/vectorwatch/android/models/Stream;
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    if-eqz v4, :cond_7

    .line 1249
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    .line 1250
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    if-eqz v4, :cond_5

    .line 1251
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v4, v5, v6, v7, v8}, Lcom/vectorwatch/android/managers/StreamsManager;->removeStreamFromFace(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Z

    .line 1252
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1253
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1252
    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/managers/StreamsManager;->getWatchDisableStreamData(III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v20

    .line 1255
    .local v20, "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-static {v0, v4}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    .line 1258
    new-instance v32, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-direct/range {v32 .. v32}, Lcom/vectorwatch/android/models/StreamChannelSettings;-><init>()V

    .line 1260
    .local v32, "unsubscribeChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1261
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v5

    const/4 v6, 0x0

    .line 1262
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    .line 1261
    invoke-static {v4, v5, v6, v7, v8}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getActiveStreamChannelLabel(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Ljava/lang/String;

    move-result-object v27

    .line 1263
    .local v27, "streamChannelLabel":Ljava/lang/String;
    move-object/from16 v0, v32

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setUniqueLabel(Ljava/lang/String;)V

    .line 1264
    new-instance v30, Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-direct/range {v30 .. v30}, Lcom/vectorwatch/android/models/StreamPlacementModel;-><init>()V

    .line 1265
    .local v30, "subscriptionModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setAppId(Ljava/lang/Integer;)V

    .line 1266
    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setElementId(Ljava/lang/Integer;)V

    .line 1267
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setWatchFaceId(Ljava/lang/Integer;)V

    .line 1269
    new-instance v31, Ljava/util/ArrayList;

    invoke-direct/range {v31 .. v31}, Ljava/util/ArrayList;-><init>()V

    .line 1271
    .local v31, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1273
    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setSubscriptions(Ljava/util/List;)V

    .line 1275
    new-instance v16, Lcom/vectorwatch/android/models/ChannelSettingsChange;

    invoke-direct/range {v16 .. v16}, Lcom/vectorwatch/android/models/ChannelSettingsChange;-><init>()V

    .line 1276
    .local v16, "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    move-object/from16 v0, v16

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/models/ChannelSettingsChange;->setOldChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 1278
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

    new-instance v6, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$16;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$16;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    move-object/from16 v0, v16

    invoke-static {v4, v5, v0, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->unsubscribeChannel(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V

    .line 1292
    .end local v16    # "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    .end local v20    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    .end local v27    # "streamChannelLabel":Ljava/lang/String;
    .end local v30    # "subscriptionModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    .end local v31    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    .end local v32    # "unsubscribeChannelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    :cond_5
    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setUserSettings(Ljava/util/Map;)V

    .line 1294
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1295
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    move-object/from16 v4, v26

    .line 1294
    invoke-static/range {v3 .. v10}, Lcom/vectorwatch/android/managers/StreamsManager;->placeStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIIZLandroid/content/Context;)V

    goto/16 :goto_0

    .line 1240
    .end local v3    # "stream":Lcom/vectorwatch/android/models/Stream;
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1241
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    .line 1240
    invoke-static {v4, v5, v6, v7}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamActiveOnPlaceholder(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v3

    .restart local v3    # "stream":Lcom/vectorwatch/android/models/Stream;
    goto/16 :goto_3

    .line 1299
    :cond_7
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_9

    .line 1300
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 1302
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    if-eqz v4, :cond_8

    .line 1303
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v4, v5, v6, v7, v8}, Lcom/vectorwatch/android/managers/StreamsManager;->removeStreamFromFace(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Z

    .line 1304
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1305
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/managers/StreamsManager;->getWatchDisableStreamData(III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v20

    .line 1307
    .restart local v20    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-static {v0, v4}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    .line 1309
    .end local v20    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    :cond_8
    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setUserSettings(Ljava/util/Map;)V

    .line 1310
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1311
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    move-object/from16 v4, v26

    .line 1310
    invoke-static/range {v3 .. v10}, Lcom/vectorwatch/android/managers/StreamsManager;->placeStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIIZLandroid/content/Context;)V

    .line 1329
    :cond_9
    :goto_4
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    goto/16 :goto_0

    .line 1314
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 1316
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mReplaceStream:Z

    if-eqz v4, :cond_b

    .line 1317
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamToBeDeleted:Lcom/vectorwatch/android/models/Stream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v4

    const/4 v6, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v5, v4, v6, v7, v8}, Lcom/vectorwatch/android/managers/StreamsManager;->removeStreamFromFace(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Z

    .line 1318
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1319
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/managers/StreamsManager;->getWatchDisableStreamData(III)Lcom/vectorwatch/android/models/ChangeComplicationModel;

    move-result-object v20

    .line 1321
    .restart local v20    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-static {v0, v4}, Lcom/vectorwatch/android/managers/StreamsManager;->sendStreamUpdate(Lcom/vectorwatch/android/models/ChangeComplicationModel;Landroid/content/Context;)V

    .line 1323
    .end local v20    # "model":Lcom/vectorwatch/android/models/ChangeComplicationModel;
    :cond_b
    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setUserSettings(Ljava/util/Map;)V

    .line 1324
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1325
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getUuid()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    move-object/from16 v4, v26

    .line 1324
    invoke-static/range {v3 .. v10}, Lcom/vectorwatch/android/managers/StreamsManager;->placeStreamOnFace(Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamChannelSettings;Ljava/lang/String;IIIZLandroid/content/Context;)V

    goto/16 :goto_4

    .line 1332
    .end local v3    # "stream":Lcom/vectorwatch/android/models/Stream;
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->cancelCurrentStream()V

    goto/16 :goto_0

    .line 1338
    .end local v15    # "channelSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    .end local v25    # "settingNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v26    # "settingsForChannel":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v29    # "streamUuid":Ljava/lang/String;
    :pswitch_1
    const-string v4, "oauth_version"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 1339
    const-string v4, "element_uuid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 1343
    .restart local v29    # "streamUuid":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-static {v0, v4}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v3

    .line 1345
    .restart local v3    # "stream":Lcom/vectorwatch/android/models/Stream;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamDroppedOnFace:Z

    if-eqz v4, :cond_d

    .line 1346
    new-instance v4, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->progressBar:Landroid/view/View;

    move-object v5, v3

    invoke-direct/range {v4 .. v9}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;-><init>(Lcom/vectorwatch/android/models/Stream;Landroid/content/Context;IILandroid/view/View;)V

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamPossibleSettingsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 1348
    :cond_d
    sget-object v4, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;->RUNNING:Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    .line 1349
    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 1348
    invoke-static {v4, v5}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getAppsWithGivenState(Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler$AppState;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v13

    .line 1350
    .local v13, "appsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 1351
    .local v12, "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    invoke-static {v5, v6}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getElementsAssociatedToApp(ILandroid/content/Context;)Ljava/util/List;

    move-result-object v22

    .line 1352
    .local v22, "placeholders":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_f
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/vectorwatch/android/models/Element;

    .line 1354
    .local v21, "placeholder":Lcom/vectorwatch/android/models/Element;
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    invoke-static {v6, v7, v8, v9}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamActiveOnPlaceholder(IIILandroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v11

    .line 1355
    .local v11, "activeStream":Lcom/vectorwatch/android/models/Stream;
    if-eqz v11, :cond_f

    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1356
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v6

    const/4 v7, 0x0

    .line 1357
    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    .line 1356
    invoke-static {v11, v6, v7, v8, v9}, Lcom/vectorwatch/android/database/StreamsDatabaseHandler;->getActiveStreamChannelLabel(Lcom/vectorwatch/android/models/Stream;IIILandroid/content/Context;)Ljava/lang/String;

    move-result-object v27

    .line 1359
    .restart local v27    # "streamChannelLabel":Ljava/lang/String;
    sget-object v6, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Clearing streams for uninstall - stream name = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v11}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " app = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 1360
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " element = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1359
    invoke-interface {v6, v7}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 1363
    new-instance v14, Lcom/vectorwatch/android/models/StreamChannelSettings;

    invoke-direct {v14}, Lcom/vectorwatch/android/models/StreamChannelSettings;-><init>()V

    .line 1364
    .local v14, "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    move-object/from16 v0, v27

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setUniqueLabel(Ljava/lang/String;)V

    .line 1365
    new-instance v30, Lcom/vectorwatch/android/models/StreamPlacementModel;

    invoke-direct/range {v30 .. v30}, Lcom/vectorwatch/android/models/StreamPlacementModel;-><init>()V

    .line 1366
    .restart local v30    # "subscriptionModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    invoke-virtual {v12}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v30

    invoke-virtual {v0, v6}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setAppId(Ljava/lang/Integer;)V

    .line 1367
    invoke-virtual/range {v21 .. v21}, Lcom/vectorwatch/android/models/Element;->getId()Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v30

    invoke-virtual {v0, v6}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setElementId(Ljava/lang/Integer;)V

    .line 1368
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v30

    invoke-virtual {v0, v6}, Lcom/vectorwatch/android/models/StreamPlacementModel;->setWatchFaceId(Ljava/lang/Integer;)V

    .line 1370
    new-instance v31, Ljava/util/ArrayList;

    invoke-direct/range {v31 .. v31}, Ljava/util/ArrayList;-><init>()V

    .line 1372
    .restart local v31    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1374
    move-object/from16 v0, v31

    invoke-virtual {v14, v0}, Lcom/vectorwatch/android/models/StreamChannelSettings;->setSubscriptions(Ljava/util/List;)V

    .line 1376
    new-instance v16, Lcom/vectorwatch/android/models/ChannelSettingsChange;

    invoke-direct/range {v16 .. v16}, Lcom/vectorwatch/android/models/ChannelSettingsChange;-><init>()V

    .line 1377
    .restart local v16    # "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lcom/vectorwatch/android/models/ChannelSettingsChange;->setOldChannelSettings(Lcom/vectorwatch/android/models/StreamChannelSettings;)V

    .line 1379
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    new-instance v7, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$17;

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v7, v0, v3, v11, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$17;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/StreamPlacementModel;)V

    move-object/from16 v0, v16

    invoke-static {v6, v11, v0, v7}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->unsubscribeChannel(Landroid/content/Context;Lcom/vectorwatch/android/models/Stream;Lcom/vectorwatch/android/models/ChannelSettingsChange;Lretrofit/Callback;)V

    goto/16 :goto_5

    .line 1400
    .end local v11    # "activeStream":Lcom/vectorwatch/android/models/Stream;
    .end local v12    # "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    .end local v14    # "channelSettings":Lcom/vectorwatch/android/models/StreamChannelSettings;
    .end local v16    # "channelSettingsChange":Lcom/vectorwatch/android/models/ChannelSettingsChange;
    .end local v21    # "placeholder":Lcom/vectorwatch/android/models/Element;
    .end local v22    # "placeholders":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/Element;>;"
    .end local v27    # "streamChannelLabel":Ljava/lang/String;
    .end local v30    # "subscriptionModel":Lcom/vectorwatch/android/models/StreamPlacementModel;
    .end local v31    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPlacementModel;>;"
    :cond_10
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mIsUpdatingStreamCredentials:Z

    goto/16 :goto_0

    .line 1403
    .end local v3    # "stream":Lcom/vectorwatch/android/models/Stream;
    .end local v13    # "appsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/vectorwatch/android/models/CloudElementSummary;>;"
    .end local v29    # "streamUuid":Ljava/lang/String;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->cancelCurrentStream()V

    .line 1404
    const-string v4, "error_msg"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1405
    const-string v4, "error_msg"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x5dc

    .line 1406
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 1405
    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto/16 :goto_0

    .line 1412
    .end local v17    # "elementId":Ljava/lang/Integer;
    .end local v24    # "result":Landroid/os/Bundle;
    .end local v28    # "streamPosition":Ljava/lang/Integer;
    :cond_12
    packed-switch p1, :pswitch_data_1

    goto/16 :goto_0

    .line 1415
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mIsUpdatingStreamCredentials:Z

    if-eqz v4, :cond_14

    .line 1416
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mIsUpdatingStreamCredentials:Z

    .line 1418
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isUpdatingStreamCredentialsUuid:Ljava/lang/String;

    if-eqz v4, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isUpdatingStreamCredentialsUuid:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_13

    .line 1419
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isUpdatingStreamCredentialsUuid:Ljava/lang/String;

    new-instance v6, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$18;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$18;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->downloadStream(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V

    .line 1454
    :cond_13
    :goto_6
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mStreamDroppedOnFace:Z

    goto/16 :goto_0

    .line 1452
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->cancelCurrentStream()V

    goto :goto_6

    .line 1185
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1412
    :pswitch_data_1
    .packed-switch 0xc9
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 633
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 635
    new-instance v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mListener:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;

    .line 637
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 641
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v6, 0x7f090062

    const/16 v9, 0x5dc

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 681
    iget-boolean v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    if-nez v4, :cond_0

    .line 682
    iput-boolean v8, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    .line 683
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v7}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 684
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->removeClickListeners()V

    .line 686
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v4, p1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v1

    .line 689
    .local v1, "centerIndex":I
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->getCurrentItem()I

    move-result v4

    if-eq v1, v4, :cond_1

    .line 690
    invoke-direct {p0, v1, v7}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setViewPagerCenter(IZ)V

    .line 691
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v8}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 692
    iput-boolean v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    .line 693
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 773
    .end local v1    # "centerIndex":I
    :cond_0
    :goto_0
    return-void

    .line 695
    .restart local v1    # "centerIndex":I
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentWatchFace()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    .line 696
    .local v2, "currentApp":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getType()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 697
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getType()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->SYSTEM_WATCHFACE:Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/DownloadedAppContainer$StoreAppType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 699
    :cond_2
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v4

    if-nez v4, :cond_3

    .line 701
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090063

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 702
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 701
    invoke-static {v4, v9, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 703
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v8}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 704
    iput-boolean v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    .line 705
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    goto :goto_0

    .line 708
    :cond_3
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v4

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/vectorwatch/android/managers/CloudAppsManager;->getElementsAssociatedToApp(ILandroid/content/Context;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 709
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/BaseActivity;

    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContextModeCallback:Landroid/support/v7/view/ActionMode$Callback;

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/ui/BaseActivity;->startSupportActionMode(Landroid/support/v7/view/ActionMode$Callback;)Landroid/support/v7/view/ActionMode;

    move-result-object v4

    iput-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    .line 710
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActionMode:Landroid/support/v7/view/ActionMode;

    const v5, 0x7f0900ba

    invoke-virtual {p0, v5}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v7/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 711
    invoke-direct {p0, v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->startStreamSetupMode(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    goto/16 :goto_0

    .line 713
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 714
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 713
    invoke-static {v4, v9, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 715
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v8}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 716
    iput-boolean v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    .line 717
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    goto/16 :goto_0

    .line 720
    :cond_5
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getRequireUserAuth()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getAuthInvalidated()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 722
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v8}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 723
    iput-boolean v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    .line 724
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 725
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->triggerAuthCredentialsChangeForApp(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    goto/16 :goto_0

    .line 726
    :cond_6
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->hasDynamicSettings()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 728
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v8}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 729
    iput-boolean v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    .line 730
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 732
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/VectorApplication;

    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getAppInstallManager()Lcom/vectorwatch/android/utils/AppInstallManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/vectorwatch/android/utils/AppInstallManager;->startConfiguringAppSettings(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    goto/16 :goto_0

    .line 734
    :cond_7
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getType()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/vectorwatch/android/models/cloud/AppsOption;->PLACEHOLDER:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v5}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 735
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v8}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 736
    iput-boolean v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    .line 737
    new-instance v3, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 738
    .local v3, "input":Landroid/widget/EditText;
    new-instance v4, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x104000a

    new-instance v6, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;

    invoke-direct {v6, p0, v2, v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$10;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/widget/EditText;)V

    .line 739
    invoke-virtual {v4, v5, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v4

    const/high16 v5, 0x1040000

    new-instance v6, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$9;

    invoke-direct {v6, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$9;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 751
    invoke-virtual {v4, v5, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v4

    .line 757
    invoke-virtual {v4}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 758
    .local v0, "alertDialog":Landroid/support/v7/app/AlertDialog;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 759
    invoke-virtual {v2}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    .line 758
    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getStringPreference(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 760
    const v4, 0x7f0900b5

    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 761
    invoke-virtual {v0, v3}, Landroid/support/v7/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 762
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 764
    .end local v0    # "alertDialog":Landroid/support/v7/app/AlertDialog;
    .end local v3    # "input":Landroid/widget/EditText;
    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 765
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 764
    invoke-static {v4, v9, v5}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 766
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v8}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 767
    iput-boolean v7, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mPerformAction:Z

    .line 768
    iget-object v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 297
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 298
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    .line 299
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 301
    invoke-virtual {p0, v4}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setRetainInstance(Z)V

    .line 303
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMemoryCache:Landroid/util/LruCache;

    if-nez v2, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 305
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v2

    mul-int/lit16 v2, v2, 0x400

    mul-int/lit16 v1, v2, 0x400

    .line 306
    .local v1, "availableMemoryInBytes":I
    new-instance v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$2;

    div-int/lit8 v3, v1, 0xa

    invoke-direct {v2, p0, v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$2;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;I)V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMemoryCache:Landroid/util/LruCache;

    .line 314
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "availableMemoryInBytes":I
    :cond_0
    const-string v2, "flag_show_onboarding"

    .line 315
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 314
    invoke-static {v2, v4, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)V

    .line 316
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 18
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 512
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030064

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    .line 514
    .local v15, "view":Landroid/view/View;
    const v1, 0x7f1001b5

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 515
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setUpLinkLostState()V

    .line 517
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10e0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMediumAnimationDuration:I

    .line 520
    const v1, 0x7f1001e1

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActivityTopLayout:Landroid/widget/RelativeLayout;

    .line 521
    const v1, 0x7f1001ea

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    .line 522
    const v1, 0x7f1001eb

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingLeftPage:Landroid/view/View;

    .line 523
    const v1, 0x7f1001f2

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingRightPage:Landroid/view/View;

    .line 524
    const v1, 0x7f1001f0

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralLeft:Landroid/view/View;

    .line 525
    const v1, 0x7f1001f1

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralRight:Landroid/view/View;

    .line 526
    const v1, 0x7f1001e5

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryImage:Landroid/widget/ImageView;

    .line 528
    const v1, 0x7f1001f4

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mViewOverlay:Landroid/view/View;

    .line 529
    const v1, 0x7f1001f5

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mImgWatchFaceDetails:Landroid/widget/ImageView;

    .line 530
    const v1, 0x7f1001f6

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContainerFacePlaceholders:Landroid/widget/RelativeLayout;

    .line 531
    const v1, 0x7f1000f8

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->progressBar:Landroid/view/View;

    .line 533
    const v1, 0x7f1001ec

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 534
    .local v9, "background":Landroid/widget/ImageView;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getWatchShape(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    .line 537
    .local v13, "s":Ljava/lang/String;
    const-string v1, "round"

    invoke-virtual {v13, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 538
    const v16, 0x7f020103

    .line 539
    .local v16, "watchImage":I
    const/16 v1, 0xf0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mFaceReferenceWidth:I

    .line 540
    const/16 v1, 0xf0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mFaceReferenceHeight:I

    .line 546
    :goto_0
    move/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 548
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    move/from16 v0, v16

    invoke-static {v1, v0}, Lcom/vectorwatch/android/utils/UIUtils;->getImageSize(Landroid/content/Context;I)[I

    move-result-object v17

    .line 549
    .local v17, "watchImageSize":[I
    const/4 v1, 0x0

    aget v7, v17, v1

    .line 552
    .local v7, "pageSizePx":I
    const-string v1, "last_synced_battery_level"

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIntPreference(Ljava/lang/String;ILandroid/content/Context;)I

    move-result v10

    .line 554
    .local v10, "batteryLevel":I
    if-ltz v10, :cond_4

    .line 555
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v10}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setBatteryIndicator(Landroid/view/View;I)V

    .line 560
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->updateAppsLists()V

    .line 562
    const v1, 0x7f1001e2

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    .line 563
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 564
    new-instance v1, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    .line 565
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f030077

    .line 567
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->createStreamsList()Ljava/util/List;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMemoryCache:Landroid/util/LruCache;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;-><init>(Landroid/content/Context;ILjava/util/List;Landroid/util/LruCache;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    .line 569
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCustomScrollView:Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mScrollViewAdapter:Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;->setAdapter(Lcom/vectorwatch/android/ui/adapter/CustomScrollViewAdapter;)V

    .line 572
    const v1, 0x7f1001f3

    invoke-virtual {v15, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    .line 573
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPagingEnabled(Z)V

    .line 575
    new-instance v1, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMemoryCache:Landroid/util/LruCache;

    move-object/from16 v4, p0

    move-object/from16 v5, p0

    invoke-direct/range {v1 .. v8}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;-><init>(Landroid/support/v4/view/ViewPager;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Landroid/content/Context;ILandroid/util/LruCache;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    .line 580
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setLandingZonesForDrag(Z)V

    .line 582
    new-instance v11, Landroid/util/DisplayMetrics;

    invoke-direct {v11}, Landroid/util/DisplayMetrics;-><init>()V

    .line 583
    .local v11, "displaymetrics":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 584
    iget v14, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 585
    .local v14, "screenWidth":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    sub-int v2, v14, v7

    div-int/lit8 v2, v2, 0x2

    const/4 v3, 0x0

    sub-int v4, v14, v7

    div-int/lit8 v4, v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setPadding(IIII)V

    .line 588
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setOffscreenPageLimit(I)V

    .line 591
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 593
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mView:Landroid/view/View;

    .line 596
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v12, v1, :cond_0

    .line 597
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getPrefsWatchCurrent(Landroid/content/Context;)I

    move-result v2

    if-ne v1, v2, :cond_5

    .line 598
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v12, v2}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setCurrentItem(IZ)V

    .line 603
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActiveWatchFaces:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 604
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentWatchFace()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 608
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 610
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/Helpers;->isBluetoothEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 611
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5dc

    .line 612
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 611
    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 615
    :cond_2
    return-object v15

    .line 542
    .end local v7    # "pageSizePx":I
    .end local v10    # "batteryLevel":I
    .end local v11    # "displaymetrics":Landroid/util/DisplayMetrics;
    .end local v12    # "i":I
    .end local v14    # "screenWidth":I
    .end local v16    # "watchImage":I
    .end local v17    # "watchImageSize":[I
    :cond_3
    const v16, 0x7f020104

    .line 543
    .restart local v16    # "watchImage":I
    const/16 v1, 0x90

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mFaceReferenceWidth:I

    .line 544
    const/16 v1, 0xa8

    move-object/from16 v0, p0

    iput v1, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mFaceReferenceHeight:I

    goto/16 :goto_0

    .line 557
    .restart local v7    # "pageSizePx":I
    .restart local v10    # "batteryLevel":I
    .restart local v17    # "watchImageSize":[I
    :cond_4
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setBatteryIndicator(Landroid/view/View;I)V

    goto/16 :goto_1

    .line 596
    .restart local v11    # "displaymetrics":Landroid/util/DisplayMetrics;
    .restart local v12    # "i":I
    .restart local v14    # "screenWidth":I
    :cond_5
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 645
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 646
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 647
    return-void
.end method

.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 18
    .param p1, "targetView"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 838
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/View;

    .line 839
    .local v12, "view":Landroid/view/View;
    invoke-virtual {v12}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 840
    .local v4, "containerLayout":Landroid/view/ViewGroup;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    move-object v10, v13

    .line 841
    .local v10, "originalOwner":Landroid/view/ViewGroup;
    :goto_0
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    const-class v14, Lcom/vectorwatch/android/ui/watchmaker/views/CustomScrollView;

    invoke-virtual {v13, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 843
    .local v7, "isStream":Z
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v2

    .line 844
    .local v2, "action":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselViewPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->getCurrentItem()I

    move-result v6

    .line 845
    .local v6, "currentItem":I
    packed-switch v2, :pswitch_data_0

    .line 1019
    :cond_0
    :goto_2
    :pswitch_0
    const/4 v13, 0x1

    :goto_3
    return v13

    .line 840
    .end local v2    # "action":I
    .end local v6    # "currentItem":I
    .end local v7    # "isStream":Z
    .end local v10    # "originalOwner":Landroid/view/ViewGroup;
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    .line 841
    .restart local v10    # "originalOwner":Landroid/view/ViewGroup;
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 847
    .restart local v2    # "action":I
    .restart local v6    # "currentItem":I
    .restart local v7    # "isStream":Z
    :pswitch_1
    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 850
    :pswitch_2
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v13

    if-nez v13, :cond_3

    .line 852
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090063

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0x5dc

    .line 853
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    .line 852
    invoke-static {v13, v14, v15}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 854
    const/4 v13, 0x1

    goto :goto_3

    .line 857
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    sget-object v15, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_WATCH_FACE:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v16, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_REORDERED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    const/4 v13, 0x0

    check-cast v13, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v13}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsLabel;)V

    .line 860
    if-nez v7, :cond_a

    .line 861
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingLeftPage:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 862
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mChangePageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageLeft:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    const-wide/16 v16, 0x2bc

    move-wide/from16 v0, v16

    invoke-virtual {v13, v14, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 864
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingRightPage:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 865
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mChangePageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageRight:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    const-wide/16 v16, 0x2bc

    move-wide/from16 v0, v16

    invoke-virtual {v13, v14, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 867
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralLeft:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    if-eqz v13, :cond_6

    .line 868
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v8

    .line 869
    .local v8, "message":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 870
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeMessages(I)V

    .line 874
    .end local v8    # "message":Landroid/os/Message;
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralRight:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    if-eqz v13, :cond_7

    .line 875
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const/4 v14, 0x1

    const/4 v15, 0x1

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v8

    .line 876
    .restart local v8    # "message":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 877
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeMessages(I)V

    .line 881
    .end local v8    # "message":Landroid/os/Message;
    :cond_7
    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 882
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->toogleDeleteLanding(Z)V

    goto/16 :goto_2

    .line 872
    .restart local v8    # "message":Landroid/os/Message;
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const-wide/16 v14, 0x64

    invoke-virtual {v13, v8, v14, v15}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_4

    .line 879
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const-wide/16 v14, 0x64

    invoke-virtual {v13, v8, v14, v15}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_5

    .line 885
    .end local v8    # "message":Landroid/os/Message;
    :cond_a
    move-object/from16 v0, p1

    instance-of v13, v0, Landroid/widget/EditText;

    if-eqz v13, :cond_0

    move-object/from16 v11, p1

    .line 886
    check-cast v11, Landroid/widget/EditText;

    .line 887
    .local v11, "txPlaceHolder":Landroid/widget/EditText;
    const/4 v13, 0x1

    invoke-virtual {v11, v13}, Landroid/widget/EditText;->setSelected(Z)V

    goto/16 :goto_2

    .line 893
    .end local v11    # "txPlaceHolder":Landroid/widget/EditText;
    :pswitch_3
    if-nez v7, :cond_f

    .line 894
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingLeftPage:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 895
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mChangePageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageLeft:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 897
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingRightPage:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 898
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mChangePageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageRight:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 900
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralLeft:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_d

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v13, v6}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->isEmptyItemAtIndex(I)Z

    move-result v13

    if-eqz v13, :cond_d

    .line 901
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const/4 v14, 0x2

    const/4 v15, 0x1

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v8

    .line 902
    .restart local v8    # "message":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const-wide/16 v14, 0x64

    invoke-virtual {v13, v8, v14, v15}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 904
    .end local v8    # "message":Landroid/os/Message;
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralRight:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_e

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v13, v6}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->isEmptyItemAtIndex(I)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 905
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const/4 v14, 0x2

    const/4 v15, -0x1

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v8

    .line 906
    .restart local v8    # "message":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mSwappingHandler:Landroid/os/Handler;

    const-wide/16 v14, 0x64

    invoke-virtual {v13, v8, v14, v15}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 908
    .end local v8    # "message":Landroid/os/Message;
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 909
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->toogleDeleteLanding(Z)V

    goto/16 :goto_2

    .line 912
    :cond_f
    move-object/from16 v0, p1

    instance-of v13, v0, Landroid/widget/EditText;

    if-eqz v13, :cond_0

    move-object/from16 v11, p1

    .line 913
    check-cast v11, Landroid/widget/EditText;

    .line 914
    .restart local v11    # "txPlaceHolder":Landroid/widget/EditText;
    invoke-virtual {v11}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 915
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Landroid/widget/EditText;->setSelected(Z)V

    goto/16 :goto_2

    .line 926
    .end local v11    # "txPlaceHolder":Landroid/widget/EditText;
    :pswitch_4
    if-nez v7, :cond_1e

    .line 927
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingLeftPage:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_16

    .line 928
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandedSomewhere:Z

    .line 929
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    if-eqz v13, :cond_10

    .line 930
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v12}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->watchFaceDroppedAfterPosition(ILandroid/view/View;)V

    .line 931
    :cond_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageLeft:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    if-eqz v13, :cond_11

    .line 932
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mChangePageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageLeft:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 987
    :cond_11
    :goto_6
    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    .line 992
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActivityTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v13

    if-nez v13, :cond_12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActivityTopLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v13

    if-nez v13, :cond_13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    .line 993
    invoke-virtual {v13}, Landroid/widget/ImageView;->getVisibility()I

    move-result v13

    if-nez v13, :cond_13

    .line 994
    :cond_12
    sget-object v13, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    const-string v14, "buttons invisible, delete visible"

    invoke-interface {v13, v14}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 995
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActivityTopLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMediumAnimationDuration:I

    invoke-static {v13, v14, v15}, Lcom/vectorwatch/android/utils/UIUtils;->crossFadePanels(Landroid/view/View;Landroid/view/View;I)V

    .line 998
    :cond_13
    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    .line 999
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    if-eqz v13, :cond_15

    .line 1000
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    .line 1001
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->cleanEmptyItems()V

    .line 1003
    move v9, v6

    .line 1004
    .local v9, "newCenter":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    const/4 v14, -0x1

    if-eq v13, v14, :cond_14

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandedSomewhere:Z

    if-nez v13, :cond_14

    .line 1005
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedWatchFace:Lcom/vectorwatch/android/models/CloudElementSummary;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    invoke-virtual {v13, v12, v14, v15}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->addViewAt(Landroid/view/View;Lcom/vectorwatch/android/models/CloudElementSummary;I)I

    .line 1006
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandedSomewhere:Z

    .line 1007
    move-object/from16 v0, p0

    iget v9, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    .line 1010
    :cond_14
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->notifyDataSetChanged()V

    .line 1011
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 1012
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v13}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setViewPagerCenter(IZ)V

    .line 1014
    .end local v9    # "newCenter":I
    :cond_15
    const/4 v13, -0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    goto/16 :goto_2

    .line 933
    :cond_16
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingRightPage:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_18

    .line 934
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandedSomewhere:Z

    .line 935
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    if-eqz v13, :cond_17

    .line 936
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v12}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->watchFaceDroppedAfterPosition(ILandroid/view/View;)V

    .line 937
    :cond_17
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageRight:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    if-eqz v13, :cond_11

    .line 938
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mChangePageHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->goOnePageRight:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$ChangePageThread;

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/16 :goto_6

    .line 939
    :cond_18
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralRight:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_19

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingCentralLeft:Landroid/view/View;

    .line 940
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1a

    .line 941
    :cond_19
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandedSomewhere:Z

    .line 942
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    if-eqz v13, :cond_11

    .line 943
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v12}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->watchFaceDroppedAfterPosition(ILandroid/view/View;)V

    goto/16 :goto_6

    .line 944
    :cond_1a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_11

    .line 945
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandedSomewhere:Z

    .line 946
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCount()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1b

    .line 947
    sget-object v13, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->log:Lorg/slf4j/Logger;

    const-string v14, "Trying to remove last watchface. Not possible."

    invoke-interface {v13, v14}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 948
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090118

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0x5dc

    .line 949
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    .line 948
    invoke-static {v13, v14, v15}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 950
    const/4 v13, 0x0

    goto/16 :goto_3

    .line 952
    :cond_1b
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v13

    if-eqz v13, :cond_1d

    .line 953
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v13}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentWatchFace()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v5

    .line 954
    .local v5, "currentApp":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {v5}, Lcom/vectorwatch/android/models/CloudElementSummary;->getType()Ljava/lang/String;

    move-result-object v13

    sget-object v14, Lcom/vectorwatch/android/models/cloud/AppsOption;->PLACEHOLDER:Lcom/vectorwatch/android/models/cloud/AppsOption;

    invoke-virtual {v14}, Lcom/vectorwatch/android/models/cloud/AppsOption;->name()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1c

    .line 955
    new-instance v13, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-direct {v13, v14}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v14, 0x1040013

    new-instance v15, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$15;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$15;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 956
    invoke-virtual {v13, v14, v15}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v13

    const/high16 v14, 0x1040000

    new-instance v15, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$14;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$14;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 963
    invoke-virtual {v13, v14, v15}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v13

    .line 968
    invoke-virtual {v13}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v3

    .line 969
    .local v3, "alertDialog":Landroid/support/v7/app/AlertDialog;
    const v13, 0x7f0900a2

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/support/v7/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 970
    const v13, 0x7f0900a3

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/support/v7/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 971
    invoke-virtual {v3}, Landroid/support/v7/app/AlertDialog;->show()V

    goto/16 :goto_6

    .line 973
    .end local v3    # "alertDialog":Landroid/support/v7/app/AlertDialog;
    :cond_1c
    invoke-direct/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->watchFaceDroppedOnDelete()V

    goto/16 :goto_6

    .line 977
    .end local v5    # "currentApp":Lcom/vectorwatch/android/models/CloudElementSummary;
    :cond_1d
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090063

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/16 v14, 0x5dc

    .line 978
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v15

    .line 977
    invoke-static {v13, v14, v15}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    goto/16 :goto_6

    .line 981
    :cond_1e
    if-eqz v7, :cond_11

    move-object/from16 v0, p1

    instance-of v13, v0, Landroid/widget/EditText;

    if-eqz v13, :cond_11

    .line 984
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v12, v1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->streamDroppedOnFace(Landroid/view/View;Landroid/view/View;)V

    goto/16 :goto_6

    .line 845
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 653
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 655
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090063

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x5dc

    .line 656
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 655
    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 676
    :goto_0
    return v5

    .line 660
    :cond_0
    const-string v2, ""

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 661
    .local v0, "data":Landroid/content/ClipData;
    new-instance v1, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v1, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 662
    .local v1, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    invoke-virtual {p1, v0, v1, p1, v5}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 664
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandingDelete:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mActivityTopLayout:Landroid/widget/RelativeLayout;

    iget v4, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMediumAnimationDuration:I

    invoke-static {v2, v3, v4}, Lcom/vectorwatch/android/utils/UIUtils;->crossFadePanels(Landroid/view/View;Landroid/view/View;I)V

    .line 666
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentItem()I

    move-result v2

    iput v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    .line 667
    iget v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    iget-object v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_1

    iget v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    .line 669
    :goto_1
    iput v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mNewCenterAfterDelete:I

    .line 671
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    iget v3, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedItemPosition:I

    invoke-virtual {v2, v3}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getWatchFace(I)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mDraggedWatchFace:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 673
    iput-boolean v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mMovedFromOriginalPosition:Z

    .line 674
    iput-boolean v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mLandedSomewhere:Z

    goto :goto_0

    .line 667
    :cond_1
    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    .line 669
    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    goto :goto_1
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 834
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 809
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 825
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->resetClickListeners()V

    .line 826
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mListener:Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v1}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentWatchFace()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v1

    iget-object v2, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v2}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentItem()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerInteractionListener;->appSelected(Lcom/vectorwatch/android/models/CloudElementSummary;I)V

    .line 827
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentWatchFace()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCrtApp:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 829
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mCarouselPagerAdapter:Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/adapter/CarouselViewPagerAdapter;->getCurrentWatchFace()Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vectorwatch/android/models/CloudElementSummary;->getId()I

    move-result v0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setPrefsWatchCurrent(ILandroid/content/Context;)V

    .line 830
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 238
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isPaused:Z

    .line 240
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vectorwatch/android/VectorApplication;->sShowStreamDropped:Z

    .line 241
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 620
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 621
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isPaused:Z

    .line 622
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/Helpers;->isBluetoothEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 623
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5dc

    .line 624
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 623
    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 627
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->testIntentForAdditionalActions()V

    .line 628
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->setUpLinkLostState()V

    .line 629
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 245
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 246
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 250
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 251
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f0f00ab

    const v6, 0x106000c

    const/high16 v5, 0x40600000    # 3.5f

    const/high16 v4, 0x3fc00000    # 1.5f

    const/4 v3, 0x0

    .line 320
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 321
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)V

    .line 323
    new-instance v0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$3;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    invoke-static {v0}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->checkCloudStatus(Lretrofit/Callback;)V

    .line 355
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    new-instance v1, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$4;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 397
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    sget-object v1, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setType(Lcom/software/shell/fab/ActionButton$Type;)V

    .line 398
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    const v1, 0x7f02012b

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setImageResource(I)V

    .line 399
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setButtonColor(I)V

    .line 400
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v0, v3}, Lcom/software/shell/fab/ActionButton;->setStrokeWidth(F)V

    .line 401
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setButtonColorPressed(I)V

    .line 402
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v0, v5}, Lcom/software/shell/fab/ActionButton;->setShadowYOffset(F)V

    .line 403
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v0, v4}, Lcom/software/shell/fab/ActionButton;->setShadowXOffset(F)V

    .line 404
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShadowColor(I)V

    .line 405
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x11

    invoke-static {v1, v2}, Lcom/github/lzyzsd/circleprogress/Utils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setImageSize(F)V

    .line 411
    :goto_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryFab:Lcom/software/shell/fab/ActionButton;

    sget-object v1, Lcom/software/shell/fab/ActionButton$Type;->DEFAULT:Lcom/software/shell/fab/ActionButton$Type;

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setType(Lcom/software/shell/fab/ActionButton$Type;)V

    .line 412
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryFab:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setButtonColor(I)V

    .line 413
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryFab:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v0, v3}, Lcom/software/shell/fab/ActionButton;->setStrokeWidth(F)V

    .line 414
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryFab:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v0, v5}, Lcom/software/shell/fab/ActionButton;->setShadowYOffset(F)V

    .line 415
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryFab:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {v0, v4}, Lcom/software/shell/fab/ActionButton;->setShadowXOffset(F)V

    .line 416
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryFab:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setShadowColor(I)V

    .line 417
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mBatteryFab:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/high16 v2, 0x41d80000    # 27.0f

    invoke-static {v1, v2}, Lcom/github/lzyzsd/circleprogress/Utils;->pxFromDp(Landroid/content/Context;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setImageSize(F)V

    .line 419
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getDndButtonState(Landroid/content/Context;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    const v1, 0x7f020180

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setImageResource(I)V

    .line 422
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setSelected(Z)V

    .line 427
    :goto_1
    return-void

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x18

    invoke-static {v1, v2}, Lcom/github/lzyzsd/circleprogress/Utils;->dpToPx(Landroid/content/Context;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setImageSize(F)V

    goto :goto_0

    .line 424
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    const v1, 0x7f020181

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setImageResource(I)V

    .line 425
    iget-object v0, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mVibrationFAB:Lcom/software/shell/fab/ActionButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/software/shell/fab/ActionButton;->setSelected(Z)V

    goto :goto_1
.end method

.method public removeStreamFromAllFaces(Lcom/vectorwatch/android/models/Stream;)V
    .locals 3
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 2413
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;

    invoke-direct {v2, p0, p1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$29;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/Stream;)V

    invoke-static {v0, v1, v2}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->downloadStream(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V

    .line 2440
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 2
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 813
    invoke-super {p0, p1}, Landroid/app/Fragment;->setUserVisibleHint(Z)V

    .line 814
    if-eqz p1, :cond_0

    .line 815
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 816
    .local v0, "a":Landroid/app/Activity;
    if-eqz v0, :cond_0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 818
    .end local v0    # "a":Landroid/app/Activity;
    :cond_0
    return-void
.end method

.method protected testIntentForAdditionalActions()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2331
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2332
    .local v2, "i":Landroid/content/Intent;
    const-string v4, "action"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 2333
    const-string v4, "streamUUID"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2335
    .local v3, "streamUuid":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->handleStreamCredentialsExpired(Ljava/lang/String;)V

    .line 2337
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "notification"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    const-string v5, "notification_id"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 2338
    const-string v4, "action"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2339
    const-string v4, "streamUUID"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2340
    const-string v4, "notification_id"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2378
    .end local v3    # "streamUuid":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 2341
    :cond_1
    const-string v4, "action"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 2342
    sput-boolean v7, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_AUTH_EXPIRED_NOTIFICATION:Z

    .line 2343
    const-string v4, "app_id"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2344
    .local v1, "appId":I
    const-string v4, "action"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2345
    const-string v4, "app_id"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2346
    if-lez v1, :cond_0

    .line 2347
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppSummaryWithId(ILandroid/content/Context;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    .line 2348
    .local v0, "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->triggerAuthCredentialsChangeForApp(Lcom/vectorwatch/android/models/CloudElementSummary;)V

    goto :goto_0

    .line 2350
    .end local v0    # "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    .end local v1    # "appId":I
    :cond_2
    const-string v4, "action"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    .line 2351
    const-string v4, "app_id"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2352
    .restart local v1    # "appId":I
    const-string v4, "action"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2353
    const-string v4, "app_id"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2354
    sput-boolean v7, Lcom/vectorwatch/android/utils/AppDataRequesterHelper;->HAS_LOCATION_NOTIFICATION:Z

    .line 2355
    if-lez v1, :cond_0

    .line 2356
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppSummaryWithId(ILandroid/content/Context;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v0

    .line 2357
    .restart local v0    # "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/vectorwatch/android/utils/LocationHelper;->triggerGPSDisabledDialogWithApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/app/Activity;)V

    goto :goto_0

    .line 2360
    .end local v0    # "app":Lcom/vectorwatch/android/models/CloudElementSummary;
    .end local v1    # "appId":I
    :cond_3
    const-string v4, "show_watchface_tutorial"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2361
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090255

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xbb8

    .line 2362
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 2361
    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 2363
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "flag_show_watchface_tutorial"

    invoke-static {v4, v7, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setFlagTutorial(Landroid/content/Context;ZLjava/lang/String;)V

    .line 2365
    const-string v4, "show_watchface_tutorial"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2366
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/MainActivity;

    iget-object v4, v4, Lcom/vectorwatch/android/MainActivity;->mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v7}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setCurrentItem(I)V

    .line 2368
    :cond_4
    const-string v4, "show_stream_tutorial"

    invoke-virtual {v2, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2369
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090254

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "stream_name"

    .line 2370
    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mContext:Landroid/content/Context;

    const v6, 0x7f09023a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x7d0

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 2369
    invoke-static {v4, v5, v6}, Lcom/vectorwatch/android/utils/Helpers;->displayNotificationAlertDialog(Ljava/lang/String;ILandroid/content/Context;)V

    .line 2371
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "flag_show_watchface_stream_tutorial"

    invoke-static {v4, v7, v5}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setFlagTutorial(Landroid/content/Context;ZLjava/lang/String;)V

    .line 2373
    const-string v4, "show_stream_tutorial"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2374
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/MainActivity;

    iget-object v4, v4, Lcom/vectorwatch/android/MainActivity;->mPager:Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;

    invoke-virtual {v4, v7}, Lcom/vectorwatch/android/ui/view/NonSwipeableViewPager;->setCurrentItem(I)V

    goto/16 :goto_0
.end method

.method public triggerAuthCredentialsChangeForApp(Lcom/vectorwatch/android/models/CloudElementSummary;)V
    .locals 6
    .param p1, "app"    # Lcom/vectorwatch/android/models/CloudElementSummary;

    .prologue
    .line 777
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 778
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v0

    .line 782
    .local v0, "showText":Ljava/lang/String;
    :goto_0
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 783
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 784
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f090167

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090170

    new-instance v3, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$13;

    invoke-direct {v3, p0, p1}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$13;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/models/CloudElementSummary;)V

    .line 785
    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0900ab

    new-instance v3, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$12;

    invoke-direct {v3, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$12;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 791
    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$11;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$11;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;)V

    .line 796
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1080027

    .line 802
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 803
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 804
    return-void

    .line 780
    .end local v0    # "showText":Ljava/lang/String;
    :cond_0
    const-string v0, "-"

    .restart local v0    # "showText":Ljava/lang/String;
    goto :goto_0
.end method

.method public updateStreamCredentials(Lcom/vectorwatch/android/models/Stream;)V
    .locals 4
    .param p1, "stream"    # Lcom/vectorwatch/android/models/Stream;

    .prologue
    .line 2443
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->mIsUpdatingStreamCredentials:Z

    .line 2444
    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->isUpdatingStreamCredentialsUuid:Ljava/lang/String;

    .line 2445
    new-instance v0, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;-><init>(Landroid/app/Activity;Lcom/vectorwatch/android/models/Stream;)V

    .line 2446
    .local v0, "listener":Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/models/Stream;->getUuid()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;

    invoke-direct {v3, p0, v0}, Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment$30;-><init>(Lcom/vectorwatch/android/ui/watchmaker/WatchMakerFragment;Lcom/vectorwatch/android/utils/StreamAuthCredentialsStatusListener;)V

    invoke-static {v1, v2, v3}, Lcom/vectorwatch/android/cloud_communication/CloudManagerHandler;->getStreamAuthMethod(Landroid/content/Context;Ljava/lang/String;Lretrofit/Callback;)V

    .line 2459
    return-void
.end method
