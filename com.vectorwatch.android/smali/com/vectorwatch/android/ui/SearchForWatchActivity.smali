.class public Lcom/vectorwatch/android/ui/SearchForWatchActivity;
.super Landroid/app/Activity;
.source "SearchForWatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;,
        Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;
    }
.end annotation


# static fields
.field private static final SCAN_PERIOD:J = 0x1770L


# instance fields
.field private final REQUEST_ENABLE_BT:I

.field private deviceAdapter:Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;

.field deviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;",
            ">;"
        }
    .end annotation
.end field

.field private deviceListView:Landroid/widget/ListView;

.field private deviceNameTextView:Landroid/widget/TextView;

.field private mAccountManager:Landroid/accounts/AccountManager;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothManager:Landroid/bluetooth/BluetoothManager;

.field private mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mHandler:Landroid/os/Handler;

.field private mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mScanning:Z

.field private pDialog:Landroid/widget/ProgressBar;

.field private registeredBondingReceiver:Z

.field private watchImageView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    const/4 v0, 0x3

    iput v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->REQUEST_ENABLE_BT:I

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 57
    new-instance v0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$1;-><init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->registeredBondingReceiver:Z

    .line 74
    new-instance v0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$2;

    invoke-direct {v0, p0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$2;-><init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 340
    return-void
.end method

.method static synthetic access$000(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->addDevice(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$102(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->registeredBondingReceiver:Z

    return p1
.end method

.method static synthetic access$200(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->scanLeDevice(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$502(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mScanning:Z

    return p1
.end method

.method static synthetic access$600(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)Landroid/bluetooth/BluetoothAdapter$LeScanCallback;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    return-object v0
.end method

.method static synthetic access$700(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$800(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/vectorwatch/android/ui/SearchForWatchActivity;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->pDialog:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private addDevice(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V
    .locals 5
    .param p1, "discovDev"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "bondedState"    # Ljava/lang/String;

    .prologue
    .line 252
    const/4 v1, 0x0

    .line 253
    .local v1, "found":Z
    iget-object v3, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 254
    .local v2, "localIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 255
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 258
    const/4 v1, 0x1

    goto :goto_0

    .line 260
    :cond_1
    if-nez v1, :cond_2

    .line 261
    new-instance v0, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 262
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, p0, v3, v4, p1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;-><init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Ljava/lang/String;Ljava/lang/String;Landroid/bluetooth/BluetoothDevice;)V

    .line 263
    .local v0, "device":Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;
    iget-object v3, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    iget-object v3, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceAdapter:Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;

    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;->notifyDataSetChanged()V

    .line 266
    .end local v0    # "device":Lcom/vectorwatch/android/ui/SearchForWatchActivity$BluetoothDeviceCustom;
    :cond_2
    return-void
.end method

.method private populateList()V
    .locals 4

    .prologue
    .line 197
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceList:Ljava/util/List;

    .line 198
    new-instance v2, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceList:Ljava/util/List;

    invoke-direct {v2, p0, p0, v3}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;-><init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;Landroid/app/Activity;Ljava/util/List;)V

    iput-object v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceAdapter:Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;

    .line 200
    iget-object v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceAdapter:Lcom/vectorwatch/android/ui/SearchForWatchActivity$DeviceAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 201
    iget-object v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 203
    iget-object v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 204
    .local v1, "localIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 206
    .local v0, "localBluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    const-string v2, "bonded"

    invoke-direct {p0, v0, v2}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->addDevice(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)V

    goto :goto_0

    .line 209
    .end local v0    # "localBluetoothDevice":Landroid/bluetooth/BluetoothDevice;
    :cond_0
    return-void
.end method

.method private saveDeviceToSharedPerefernces(Landroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 93
    const-string v0, "Pair:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Address: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBondedDeviceName(Ljava/lang/String;Landroid/content/Context;)V

    .line 95
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->setBondedDeviceAddress(Ljava/lang/String;Landroid/content/Context;)V

    .line 96
    return-void
.end method

.method private scanLeDevice(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    const/4 v2, 0x0

    .line 213
    if-eqz p1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->pDialog:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 216
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->populateList()V

    .line 218
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/vectorwatch/android/ui/SearchForWatchActivity$6;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$6;-><init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)V

    const-wide/16 v4, 0x1770

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 233
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mScanning:Z

    .line 234
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    move-result v0

    .line 235
    .local v0, "startedScan":Z
    const-string v1, "iii"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "started le "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    .end local v0    # "startedScan":Z
    :goto_0
    return-void

    .line 238
    :cond_0
    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mScanning:Z

    .line 239
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 240
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->pDialog:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public createBond(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 6
    .param p1, "btDevice"    # Landroid/bluetooth/BluetoothDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 245
    const-string v3, "android.bluetooth.BluetoothDevice"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 246
    .local v0, "class1":Ljava/lang/Class;
    const-string v3, "createBond"

    new-array v4, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 247
    .local v1, "createBondMethod":Ljava/lang/reflect/Method;
    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 248
    .local v2, "returnValue":Ljava/lang/Boolean;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    return v3
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f100141

    .line 101
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 102
    const v1, 0x7f03002f

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->setContentView(I)V

    .line 104
    const v1, 0x7f10013f

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->watchImageView:Landroid/widget/ImageView;

    .line 105
    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceListView:Landroid/widget/ListView;

    .line 106
    const v1, 0x7f100140

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->pDialog:Landroid/widget/ProgressBar;

    .line 108
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mAccountManager:Landroid/accounts/AccountManager;

    .line 110
    iput-object p0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mContext:Landroid/content/Context;

    .line 111
    new-instance v1, Lcom/vectorwatch/android/ui/SearchForWatchActivity$3;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$3;-><init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 125
    const-string v1, "bluetooth"

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothManager;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothManager:Landroid/bluetooth/BluetoothManager;

    .line 126
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothManager:Landroid/bluetooth/BluetoothManager;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 127
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v1, :cond_0

    .line 128
    const v1, 0x7f090076

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 134
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.bluetooth.adapter.action.REQUEST_ENABLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    .local v0, "enableBtIntent":Landroid/content/Intent;
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 138
    .end local v0    # "enableBtIntent":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->deviceListView:Landroid/widget/ListView;

    .line 139
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mHandler:Landroid/os/Handler;

    .line 141
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->watchImageView:Landroid/widget/ImageView;

    new-instance v2, Lcom/vectorwatch/android/ui/SearchForWatchActivity$4;

    invoke-direct {v2, p0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$4;-><init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    new-instance v1, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;

    invoke-direct {v1, p0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity$5;-><init>(Lcom/vectorwatch/android/ui/SearchForWatchActivity;)V

    iput-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mDeviceClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 189
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 274
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 275
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->pDialog:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->pDialog:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 278
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 269
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->scanLeDevice(Z)V

    .line 270
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 271
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 281
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 283
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.bluetooth.device.action.FOUND"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 284
    .local v1, "localIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 286
    .local v0, "localBroadcastReceiver":Landroid/content/BroadcastReceiver;
    invoke-virtual {p0, v0, v1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 288
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 291
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 292
    iget-boolean v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->registeredBondingReceiver:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 293
    iget-object v1, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mBondingBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 296
    .local v0, "localBroadcastReceiver":Landroid/content/BroadcastReceiver;
    invoke-virtual {p0, v0}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 297
    return-void
.end method

.method public registerToServer(Landroid/bluetooth/BluetoothDevice;)V
    .locals 0
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 193
    invoke-direct {p0, p1}, Lcom/vectorwatch/android/ui/SearchForWatchActivity;->saveDeviceToSharedPerefernces(Landroid/bluetooth/BluetoothDevice;)V

    .line 194
    return-void
.end method
