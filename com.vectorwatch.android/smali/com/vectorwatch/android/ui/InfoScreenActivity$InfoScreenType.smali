.class public final enum Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;
.super Ljava/lang/Enum;
.source "InfoScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/InfoScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InfoScreenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

.field public static final enum CHECK_COMPATIBILITY:Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    new-instance v0, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    const-string v1, "CHECK_COMPATIBILITY"

    invoke-direct {v0, v1, v2, v2}, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->CHECK_COMPATIBILITY:Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    sget-object v1, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->CHECK_COMPATIBILITY:Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->$VALUES:[Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "val"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput p3, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->val:I

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    const-class v0, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    return-object v0
.end method

.method public static values()[Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->$VALUES:[Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    invoke-virtual {v0}, [Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/vectorwatch/android/ui/InfoScreenActivity$InfoScreenType;->val:I

    return v0
.end method
