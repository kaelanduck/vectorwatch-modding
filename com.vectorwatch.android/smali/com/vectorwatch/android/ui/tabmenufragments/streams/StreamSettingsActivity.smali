.class public Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "StreamSettingsActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    }
.end annotation


# static fields
.field public static final EXTRA_ELEMENT_ID:Ljava/lang/String; = "watchface_elem_id"

.field public static final EXTRA_JSON:Ljava/lang/String; = "stream_json"

.field public static final EXTRA_STREAM:Ljava/lang/String; = "stream"

.field public static final EXTRA_STREAM_POS:Ljava/lang/String; = "stream_place_id"

.field public static final EXTRA_STREAM_UUID:Ljava/lang/String; = "stream_uuid"

.field public static final RETURN_EXTRA_ELEM_POS:Ljava/lang/String; = "elem_pos"

.field public static final RETURN_EXTRA_SELECTED_VALUES:Ljava/lang/String; = "result_map"

.field public static final RETURN_EXTRA_STREAM_POS:Ljava/lang/String; = "stream_pos"

.field private static final SPECIAL_CASE_CITY:Ljava/lang/String; = "City"

.field private static final SPECIAL_CASE_TIMEZONE:Ljava/lang/String; = "Timezone"

.field public static final STREAM_DROPPED_ON_FACE:Ljava/lang/String; = "STREAM_DROPPED"

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field final GRID:Ljava/lang/String;

.field final INPUT_LIST:Ljava/lang/String;

.field final INPUT_LIST_STRICT:Ljava/lang/String;

.field breadcrumbsTextView:Landroid/widget/TextView;

.field mCurrentIndex:I

.field public mRenderOptions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;",
            ">;"
        }
    .end annotation
.end field

.field mSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private mStreamDroppedOnFace:Z

.field properties:[Ljava/lang/String;

.field result:Landroid/os/Bundle;

.field private stream:Lcom/vectorwatch/android/models/Stream;

.field private streamElemId:Ljava/lang/Integer;

.field private streamPosId:Ljava/lang/Integer;

.field private streamUuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 44
    const-string v0, "INPUT_LIST"

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->INPUT_LIST:Ljava/lang/String;

    .line 45
    const-string v0, "INPUT_LIST_STRICT"

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->INPUT_LIST_STRICT:Ljava/lang/String;

    .line 46
    const-string v0, "GRID_LAYOUT"

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->GRID:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    .line 73
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->result:Landroid/os/Bundle;

    .line 370
    return-void
.end method


# virtual methods
.method public continueOnEmptyOption()V
    .locals 0

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->finishAndReturnSettings()V

    .line 310
    return-void
.end method

.method public finishAndReturnSettings()V
    .locals 3

    .prologue
    .line 313
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 314
    .local v0, "returnIntent":Landroid/content/Intent;
    const-string v1, "result_map"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 315
    const-string v1, "stream_pos"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->streamPosId:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 316
    const-string v1, "elem_pos"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->streamElemId:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 317
    const-string v1, "STREAM_DROPPED"

    iget-boolean v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mStreamDroppedOnFace:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 318
    const-string v1, "stream_uuid"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->streamUuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 321
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->finish()V

    .line 322
    return-void
.end method

.method public getHint(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 222
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 223
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;

    .line 224
    .local v0, "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->getHint()Ljava/lang/String;

    move-result-object v1

    .line 226
    .end local v0    # "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRenderType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 214
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;

    .line 216
    .local v0, "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->getType()Ljava/lang/String;

    move-result-object v1

    .line 218
    .end local v0    # "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSpecialSuggestions(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 364
    .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    const-string v1, "City"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 367
    :cond_0
    return-object v0
.end method

.method public getValues(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 278
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 281
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadFragment()V
    .locals 10

    .prologue
    const v9, 0x7f100159

    const/4 v8, 0x0

    .line 134
    const-string v5, "SreamSettings"

    const-string v6, "Load fragment method"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 138
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->properties:[Ljava/lang/String;

    array-length v5, v5

    if-nez v5, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->finishAndReturnSettings()V

    .line 194
    :goto_0
    return-void

    .line 143
    :cond_0
    iget v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    iget-object v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->properties:[Ljava/lang/String;

    array-length v6, v6

    if-ge v5, v6, :cond_9

    .line 144
    const/4 v3, 0x0

    .line 145
    .local v3, "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->properties:[Ljava/lang/String;

    iget v6, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    aget-object v2, v5, v6

    .line 146
    .local v2, "property":Ljava/lang/String;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    if-eqz v5, :cond_1

    .line 147
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    check-cast v3, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;

    .line 149
    .restart local v3    # "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    :cond_1
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getRenderType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 152
    .local v4, "type":Ljava/lang/String;
    sget-object v5, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->log:Lorg/slf4j/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RENDER OPTION: type stream customize = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 154
    if-eqz v4, :cond_6

    .line 155
    const-string v5, "INPUT_LIST"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 156
    const/4 v5, 0x1

    invoke-static {v2, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->newInstance(Ljava/lang/String;Z)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    move-result-object v0

    .line 171
    .local v0, "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    :goto_1
    if-eqz v3, :cond_2

    .line 172
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->getDataType()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_7

    .line 173
    invoke-virtual {v0, v8}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->setIsDynamic(Z)V

    .line 177
    :goto_2
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->getAsYouType()Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->setAsYouType(Z)V

    .line 178
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->getMinChars()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->setMinChars(I)V

    .line 180
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 181
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    if-lez v5, :cond_8

    .line 182
    invoke-virtual {v1, v9, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 183
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 188
    :goto_3
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 193
    .end local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    .end local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    .end local v2    # "property":Ljava/lang/String;
    .end local v3    # "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    .end local v4    # "type":Ljava/lang/String;
    :goto_4
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->renderBreadCrumbs()V

    goto :goto_0

    .line 157
    .restart local v2    # "property":Ljava/lang/String;
    .restart local v3    # "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    .restart local v4    # "type":Ljava/lang/String;
    :cond_3
    const-string v5, "INPUT_LIST_STRICT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 158
    invoke-static {v2, v8}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->newInstance(Ljava/lang/String;Z)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    move-result-object v0

    .restart local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    goto :goto_1

    .line 159
    .end local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    :cond_4
    const-string v5, "GRID_LAYOUT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 160
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->newInstance(Ljava/lang/String;)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;

    move-result-object v0

    .restart local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    goto :goto_1

    .line 163
    .end local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    :cond_5
    invoke-static {v2, v8}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->newInstance(Ljava/lang/String;Z)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    move-result-object v0

    .restart local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    goto :goto_1

    .line 167
    .end local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    :cond_6
    sget-object v5, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->log:Lorg/slf4j/Logger;

    const-string v6, "STREAM OPTION: Null type received from cloud for rendering stream options screen."

    invoke-interface {v5, v6}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 168
    invoke-static {v2, v8}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->newInstance(Ljava/lang/String;Z)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    move-result-object v0

    .restart local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    goto :goto_1

    .line 175
    :cond_7
    invoke-virtual {v3}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->getDataType()Ljava/lang/String;

    move-result-object v5

    const-string v6, "dynamic"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->setIsDynamic(Z)V

    goto :goto_2

    .line 185
    .restart local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_8
    invoke-virtual {v1, v9, v0}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_3

    .line 191
    .end local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    .end local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    .end local v2    # "property":Ljava/lang/String;
    .end local v3    # "renderOption":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    .end local v4    # "type":Ljava/lang/String;
    :cond_9
    iget v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    goto :goto_4
.end method

.method public loadNextFragment()V
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    .line 326
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->loadFragment()V

    .line 327
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 121
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 122
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->result:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->properties:[Ljava/lang/String;

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->result:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->properties:[Ljava/lang/String;

    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 129
    :cond_0
    :goto_0
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    .line 130
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->renderBreadCrumbs()V

    .line 131
    return-void

    .line 127
    :cond_1
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, -0x1

    .line 84
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    const v2, 0x7f030037

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->setContentView(I)V

    .line 87
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 89
    const v2, 0x7f10015a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->breadcrumbsTextView:Landroid/widget/TextView;

    .line 91
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "stream_json"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "json":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "stream_place_id"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->streamPosId:Ljava/lang/Integer;

    .line 93
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "watchface_elem_id"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->streamElemId:Ljava/lang/Integer;

    .line 94
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "stream_uuid"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->streamUuid:Ljava/lang/String;

    .line 95
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "STREAM_DROPPED"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mStreamDroppedOnFace:Z

    .line 96
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->streamUuid:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vectorwatch/android/managers/StreamsManager;->getStreamFromDb(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/Stream;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 98
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->stream:Lcom/vectorwatch/android/models/Stream;

    if-eqz v2, :cond_0

    .line 99
    sget-object v2, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;->ANALYTICS_CATEGORY_STREAM:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;

    sget-object v3, Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;->ANALYTICS_ACTION_CUSTOMIZED:Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->stream:Lcom/vectorwatch/android/models/Stream;

    .line 100
    invoke-virtual {v4}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v4

    .line 99
    invoke-static {p0, v2, v3, v4}, Lcom/vectorwatch/android/utils/AnalyticsHelper;->logEvent(Landroid/content/Context;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsCategory;Lcom/vectorwatch/android/utils/AnalyticsHelper$AnalyticsAction;Ljava/lang/String;)V

    .line 102
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->stream:Lcom/vectorwatch/android/models/Stream;

    invoke-virtual {v3}, Lcom/vectorwatch/android/models/Stream;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f090047

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 109
    :goto_0
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->parseJson(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->loadFragment()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_1
    return-void

    .line 104
    :cond_0
    sget-object v2, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->log:Lorg/slf4j/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "STREAM: setting up stream - null. Stream UUID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->streamUuid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " stream dropped on face = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mStreamDroppedOnFace:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "Error"

    const-string v3, "Can\'t parse json for settings screen."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->finish()V

    goto :goto_1
.end method

.method public parseJson(Ljava/lang/String;)V
    .locals 14
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 330
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 332
    .local v5, "rootJso":Lorg/json/JSONObject;
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 333
    .local v0, "gson":Lcom/google/gson/Gson;
    new-instance v11, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$2;

    invoke-direct {v11, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;)V

    .line 334
    invoke-virtual {v11}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v7

    .line 335
    .local v7, "type":Ljava/lang/reflect/Type;
    const-string v11, "renderOptions"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11, v7}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map;

    iput-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mRenderOptions:Ljava/util/Map;

    .line 337
    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 338
    .local v3, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v11

    new-array v11, v11, [Ljava/lang/String;

    iput-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->properties:[Ljava/lang/String;

    .line 339
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 340
    .local v2, "key":Ljava/lang/String;
    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v12, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;

    .line 341
    .local v4, "option":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->properties:[Ljava/lang/String;

    invoke-virtual {v4}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->getOrder()I

    move-result v13

    aput-object v2, v12, v13

    goto :goto_0

    .line 344
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "option":Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
    :cond_0
    const-string v11, "settings"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 345
    .local v6, "settingsJson":Lorg/json/JSONObject;
    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 346
    .restart local v2    # "key":Ljava/lang/String;
    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 347
    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 348
    .local v10, "valuesList":Lorg/json/JSONArray;
    new-instance v12, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$3;

    invoke-direct {v12, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;)V

    .line 349
    invoke-virtual {v12}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$3;->getType()Ljava/lang/reflect/Type;

    move-result-object v8

    .line 351
    .local v8, "typeKeyValues":Ljava/lang/reflect/Type;
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getSpecialSuggestions(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 352
    .local v1, "initializedValues":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    invoke-virtual {v10}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12, v8}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 353
    .local v9, "values":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    invoke-interface {v1, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 355
    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    invoke-interface {v12, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 357
    .end local v1    # "initializedValues":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    .end local v8    # "typeKeyValues":Ljava/lang/reflect/Type;
    .end local v9    # "values":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    .end local v10    # "valuesList":Lorg/json/JSONArray;
    :cond_1
    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v12, v2, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 360
    .end local v2    # "key":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public renderBreadCrumbs()V
    .locals 7

    .prologue
    .line 197
    const-string v2, ""

    .line 198
    .local v2, "text":Ljava/lang/String;
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 199
    .local v1, "settingNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v1, :cond_1

    .line 200
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 202
    .local v0, "settingName":Ljava/lang/String;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 203
    .local v3, "values":[Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 204
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " > "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 206
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v6, v3, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 207
    goto :goto_0

    .line 209
    .end local v0    # "settingName":Ljava/lang/String;
    .end local v3    # "values":[Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->breadcrumbsTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    return-void
.end method

.method public requestOptions(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;Ljava/lang/String;)V
    .locals 18
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "fragment"    # Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    .param p3, "currentValue"    # Ljava/lang/String;

    .prologue
    .line 231
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 232
    .local v16, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v14

    .line 233
    .local v14, "settingNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v14, :cond_3

    .line 234
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 236
    .local v13, "settingName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v4, v13}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 237
    .local v17, "values":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 239
    .local v3, "set":Lcom/vectorwatch/android/models/settings/Setting;
    move-object/from16 v0, v17

    array-length v4, v0

    const/4 v5, 0x4

    if-le v4, v5, :cond_1

    .line 241
    new-instance v12, Lcom/google/gson/Gson;

    invoke-direct {v12}, Lcom/google/gson/Gson;-><init>()V

    .line 242
    .local v12, "gson":Lcom/google/gson/Gson;
    const/4 v4, 0x4

    aget-object v4, v17, v4

    const-class v5, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    invoke-virtual {v12, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    .line 244
    .local v15, "streamPropertyValueModel":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    if-eqz v15, :cond_0

    .line 245
    new-instance v3, Lcom/vectorwatch/android/models/settings/Setting;

    .end local v3    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    const/4 v4, 0x0

    aget-object v4, v17, v4

    const/4 v5, 0x1

    aget-object v5, v17, v5

    const/4 v6, 0x2

    aget-object v6, v17, v6

    const/4 v7, 0x3

    aget-object v7, v17, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 246
    invoke-virtual {v15}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getPermissions()Ljava/util/List;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/vectorwatch/android/models/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V

    .line 258
    .restart local v3    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    :cond_0
    :goto_1
    move-object/from16 v0, v16

    invoke-interface {v0, v13, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 248
    .end local v12    # "gson":Lcom/google/gson/Gson;
    .end local v15    # "streamPropertyValueModel":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    :cond_1
    new-instance v12, Lcom/google/gson/Gson;

    invoke-direct {v12}, Lcom/google/gson/Gson;-><init>()V

    .line 249
    .restart local v12    # "gson":Lcom/google/gson/Gson;
    const/4 v4, 0x3

    aget-object v4, v17, v4

    const-class v5, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    invoke-virtual {v12, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    .line 252
    .restart local v15    # "streamPropertyValueModel":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    if-eqz v15, :cond_2

    .line 253
    new-instance v3, Lcom/vectorwatch/android/models/settings/Setting;

    .end local v3    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    const/4 v4, 0x0

    aget-object v4, v17, v4

    const/4 v5, 0x1

    aget-object v5, v17, v5

    const/4 v6, 0x2

    aget-object v6, v17, v6

    invoke-virtual {v15}, Lcom/vectorwatch/android/models/StreamPropertyValueModel;->getPermissions()Ljava/util/List;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/vectorwatch/android/models/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .restart local v3    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    goto :goto_1

    .line 255
    :cond_2
    new-instance v3, Lcom/vectorwatch/android/models/settings/Setting;

    .end local v3    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    const/4 v4, 0x0

    aget-object v4, v17, v4

    const/4 v5, 0x1

    aget-object v5, v17, v5

    const/4 v6, 0x2

    aget-object v6, v17, v6

    invoke-direct {v3, v4, v5, v6}, Lcom/vectorwatch/android/models/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v3    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    goto :goto_1

    .line 261
    .end local v3    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    .end local v12    # "gson":Lcom/google/gson/Gson;
    .end local v13    # "settingName":Ljava/lang/String;
    .end local v15    # "streamPropertyValueModel":Lcom/vectorwatch/android/models/StreamPropertyValueModel;
    .end local v17    # "values":[Ljava/lang/String;
    :cond_3
    new-instance v4, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->stream:Lcom/vectorwatch/android/models/Stream;

    new-instance v9, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v9, v0, v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;)V

    .line 273
    invoke-virtual/range {p0 .. p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v6, p1

    move-object/from16 v7, p3

    move-object/from16 v8, v16

    invoke-direct/range {v4 .. v11}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;-><init>(Lcom/vectorwatch/android/models/Stream;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;Landroid/content/Context;Landroid/view/View;)V

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 274
    return-void
.end method

.method public select(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPropertyValueModel;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "optionName"    # Ljava/lang/String;
    .param p3, "optionValue"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "model"    # Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    .prologue
    const/4 v5, 0x4

    .line 288
    const-string v2, "debug"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Added values "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const v2, 0x7f0902c4

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 305
    :goto_0
    return-void

    .line 295
    :cond_0
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 296
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v0, p5}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "streamValueModelJson":Ljava/lang/String;
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->result:Landroid/os/Bundle;

    new-array v2, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v2, v4

    const/4 v4, 0x1

    aput-object p3, v2, v4

    const/4 v4, 0x2

    aput-object p4, v2, v4

    const/4 v4, 0x3

    aput-object v1, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-array v4, v5, [Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-virtual {v3, p1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 300
    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mCurrentIndex:I

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->properties:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_1

    .line 301
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->finishAndReturnSettings()V

    goto :goto_0

    .line 303
    :cond_1
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->loadNextFragment()V

    goto :goto_0
.end method
