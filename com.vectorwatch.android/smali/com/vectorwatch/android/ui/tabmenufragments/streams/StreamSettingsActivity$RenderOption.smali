.class Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;
.super Ljava/lang/Object;
.source "StreamSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RenderOption"
.end annotation


# instance fields
.field private asYouType:Z

.field private dataType:Ljava/lang/String;

.field private hint:Ljava/lang/String;

.field private minChars:I

.field private order:I

.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

.field private type:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

    .prologue
    .line 370
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAsYouType()Z
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->asYouType:Z

    return v0
.end method

.method public getDataType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->dataType:Ljava/lang/String;

    return-object v0
.end method

.method public getHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->hint:Ljava/lang/String;

    return-object v0
.end method

.method public getMinChars()I
    .locals 1

    .prologue
    .line 387
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->minChars:I

    return v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 399
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->order:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 0
    .param p1, "hint"    # Ljava/lang/String;

    .prologue
    .line 407
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->hint:Ljava/lang/String;

    .line 408
    return-void
.end method

.method public setOrder(I)V
    .locals 0
    .param p1, "order"    # I

    .prologue
    .line 403
    iput p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->order:I

    .line 404
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 395
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$RenderOption;->type:Ljava/lang/String;

    .line 396
    return-void
.end method
