.class Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;
.super Ljava/lang/Object;
.source "StreamSettingsActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/utils/asyncTasks/GetStreamSettingOptionsAsyncTask$StreamSettingOptionsCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->requestOptions(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

.field final synthetic val$fragment:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

.field final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;->val$key:Ljava/lang/String;

    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;->val$fragment:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOptionsReceivedError()V
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;->val$key:Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;->val$fragment:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onOptionsReceived()V

    .line 272
    return-void
.end method

.method public onOptionsReceivedSuccess(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 264
    .local p1, "options":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity;->mSettings:Ljava/util/Map;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;->val$key:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/streams/StreamSettingsActivity$1;->val$fragment:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onOptionsReceived()V

    .line 266
    return-void
.end method
