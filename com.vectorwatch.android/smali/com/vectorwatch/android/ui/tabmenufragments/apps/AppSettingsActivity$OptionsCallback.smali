.class Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;
.super Ljava/lang/Object;
.source "AppSettingsActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OptionsCallback"
.end annotation


# instance fields
.field fragment:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

.field key:Ljava/lang/String;

.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;)V
    .locals 0
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "fragment"    # Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;->key:Ljava/lang/String;

    .line 230
    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;->fragment:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

    .line 231
    return-void
.end method


# virtual methods
.method public onOptionsReceivedError()V
    .locals 3

    .prologue
    .line 241
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mSettings:Ljava/util/Map;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;->key:Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;->fragment:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onOptionsReceived()V

    .line 243
    return-void
.end method

.method public onOptionsReceivedSuccess(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "options":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;

    iget-object v0, v0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mSettings:Ljava/util/Map;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;->key:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;->fragment:Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->onOptionsReceived()V

    .line 237
    return-void
.end method
