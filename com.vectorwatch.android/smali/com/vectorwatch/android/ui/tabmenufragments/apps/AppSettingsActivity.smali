.class public Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;
.super Lcom/vectorwatch/android/ui/BaseActivity;
.source "AppSettingsActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment$Callbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;
    }
.end annotation


# static fields
.field public static final EXTRA_APP_UUID:Ljava/lang/String; = "app_uuid"

.field public static final EXTRA_JSON:Ljava/lang/String; = "settings_json"

.field public static final RETURN_EXTRA_SELECTED_VALUES:Ljava/lang/String; = "result_map"

.field private static final SPECIAL_CASE_CITY:Ljava/lang/String; = "City"

.field private static final SPECIAL_CASE_TIMEZONE:Ljava/lang/String; = "Timezone"

.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field final GRID:Ljava/lang/String;

.field final INPUT_LIST:Ljava/lang/String;

.field final INPUT_LIST_STRICT:Ljava/lang/String;

.field private app:Lcom/vectorwatch/android/models/CloudElementSummary;

.field private appUuid:Ljava/lang/String;

.field mCurrentIndex:I

.field public mRenderOptions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/vectorwatch/android/models/settings/RenderOption;",
            ">;"
        }
    .end annotation
.end field

.field mSettings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;>;"
        }
    .end annotation
.end field

.field properties:[Ljava/lang/String;

.field result:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/BaseActivity;-><init>()V

    .line 45
    const-string v0, "INPUT_LIST"

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->INPUT_LIST:Ljava/lang/String;

    .line 46
    const-string v0, "INPUT_LIST_STRICT"

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->INPUT_LIST_STRICT:Ljava/lang/String;

    .line 47
    const-string v0, "GRID_LAYOUT"

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->GRID:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mSettings:Ljava/util/Map;

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    .line 66
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->result:Landroid/os/Bundle;

    .line 224
    return-void
.end method


# virtual methods
.method public continueOnEmptyOption()V
    .locals 0

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->finishAndReturnSettings()V

    .line 273
    return-void
.end method

.method public finishAndReturnSettings()V
    .locals 3

    .prologue
    .line 276
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 277
    .local v0, "returnIntent":Landroid/content/Intent;
    const-string v1, "result_map"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 278
    const-string v1, "app_uuid"

    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->appUuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->setResult(ILandroid/content/Intent;)V

    .line 281
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->finish()V

    .line 282
    return-void
.end method

.method public getHint(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 166
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mSettings:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 167
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/settings/RenderOption;

    .line 168
    .local v0, "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/settings/RenderOption;->getHint()Ljava/lang/String;

    move-result-object v1

    .line 170
    .end local v0    # "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRenderType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 158
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mSettings:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/models/settings/RenderOption;

    .line 160
    .local v0, "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    invoke-virtual {v0}, Lcom/vectorwatch/android/models/settings/RenderOption;->getType()Ljava/lang/String;

    move-result-object v1

    .line 162
    .end local v0    # "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSpecialSuggestions(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 320
    .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    const-string v1, "City"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    :cond_0
    return-object v0
.end method

.method public getValues(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/vectorwatch/android/models/StreamPropertyValueModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mSettings:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mSettings:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 251
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadFragment()V
    .locals 10

    .prologue
    const v9, 0x7f100159

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 103
    const-string v5, "SreamSettings"

    const-string v8, "Load fragment method"

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 107
    iget v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    iget-object v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->properties:[Ljava/lang/String;

    array-length v8, v8

    if-ge v5, v8, :cond_5

    .line 108
    const/4 v3, 0x0

    .line 109
    .local v3, "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->properties:[Ljava/lang/String;

    iget v8, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    aget-object v2, v5, v8

    .line 111
    .local v2, "property":Ljava/lang/String;
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mSettings:Ljava/util/Map;

    if-eqz v5, :cond_0

    .line 112
    iget-object v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    check-cast v3, Lcom/vectorwatch/android/models/settings/RenderOption;

    .line 115
    .restart local v3    # "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    :cond_0
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getRenderType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 118
    .local v4, "type":Ljava/lang/String;
    const/4 v5, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v5, :pswitch_data_0

    .line 129
    invoke-static {v2, v6}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->newInstance(Ljava/lang/String;Z)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    move-result-object v0

    .line 133
    .local v0, "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    :goto_1
    if-eqz v3, :cond_2

    .line 134
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/settings/RenderOption;->getDataType()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    .line 135
    invoke-virtual {v0, v6}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->setIsDynamic(Z)V

    .line 139
    :goto_2
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/settings/RenderOption;->getAsYouType()Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->setAsYouType(Z)V

    .line 140
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/settings/RenderOption;->getMinChars()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->setMinChars(I)V

    .line 142
    :cond_2
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 143
    .local v1, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    if-lez v5, :cond_4

    .line 144
    invoke-virtual {v1, v9, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 145
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 150
    :goto_3
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 155
    .end local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    .end local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    .end local v2    # "property":Ljava/lang/String;
    .end local v3    # "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    .end local v4    # "type":Ljava/lang/String;
    :goto_4
    return-void

    .line 118
    .restart local v2    # "property":Ljava/lang/String;
    .restart local v3    # "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    .restart local v4    # "type":Ljava/lang/String;
    :sswitch_0
    const-string v8, "INPUT_LIST"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v5, v6

    goto :goto_0

    :sswitch_1
    const-string v8, "INPUT_LIST_STRICT"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v5, v7

    goto :goto_0

    :sswitch_2
    const-string v8, "GRID_LAYOUT"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v5, 0x2

    goto :goto_0

    .line 120
    :pswitch_0
    invoke-static {v2, v7}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->newInstance(Ljava/lang/String;Z)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    move-result-object v0

    .line 121
    .restart local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    goto :goto_1

    .line 123
    .end local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    :pswitch_1
    invoke-static {v2, v6}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;->newInstance(Ljava/lang/String;Z)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsAutocompleteFragment;

    move-result-object v0

    .line 124
    .restart local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    goto :goto_1

    .line 126
    .end local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    :pswitch_2
    invoke-static {v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;->newInstance(Ljava/lang/String;)Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsGridFragment;

    move-result-object v0

    .line 127
    .restart local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    goto :goto_1

    .line 137
    :cond_3
    invoke-virtual {v3}, Lcom/vectorwatch/android/models/settings/RenderOption;->getDataType()Ljava/lang/String;

    move-result-object v5

    const-string v6, "dynamic"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;->setIsDynamic(Z)V

    goto :goto_2

    .line 147
    .restart local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_4
    invoke-virtual {v1, v9, v0}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_3

    .line 153
    .end local v0    # "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    .end local v1    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    .end local v2    # "property":Ljava/lang/String;
    .end local v3    # "renderOption":Lcom/vectorwatch/android/models/settings/RenderOption;
    .end local v4    # "type":Ljava/lang/String;
    :cond_5
    iget v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    goto :goto_4

    .line 118
    :sswitch_data_0
    .sparse-switch
        -0x6edab1bd -> :sswitch_2
        0x24c2e875 -> :sswitch_1
        0x71503c13 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public loadNextFragment()V
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    .line 286
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->loadFragment()V

    .line 287
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 99
    :goto_0
    iget v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    .line 100
    return-void

    .line 97
    :cond_0
    invoke-super {p0}, Lcom/vectorwatch/android/ui/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/vectorwatch/android/ui/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const v2, 0x7f030037

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->setContentView(I)V

    .line 77
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 79
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "settings_json"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "json":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "app_uuid"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->appUuid:Ljava/lang/String;

    .line 81
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->appUuid:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vectorwatch/android/database/CloudAppsDatabaseHandler;->getAppSummaryWithUuid(Ljava/lang/String;Landroid/content/Context;)Lcom/vectorwatch/android/models/CloudElementSummary;

    move-result-object v2

    iput-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    .line 83
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->parseJson(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->loadFragment()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "Error"

    const-string v3, "Can\'t parse json for settings screen."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->finish()V

    goto :goto_0
.end method

.method public parseJson(Ljava/lang/String;)V
    .locals 14
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 290
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 292
    .local v5, "rootJso":Lorg/json/JSONObject;
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 293
    .local v0, "gson":Lcom/google/gson/Gson;
    new-instance v11, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$2;

    invoke-direct {v11, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$2;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;)V

    .line 294
    invoke-virtual {v11}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v7

    .line 295
    .local v7, "type":Ljava/lang/reflect/Type;
    const-string v11, "renderOptions"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11, v7}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map;

    iput-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mRenderOptions:Ljava/util/Map;

    .line 297
    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 298
    .local v3, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v11

    new-array v11, v11, [Ljava/lang/String;

    iput-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->properties:[Ljava/lang/String;

    .line 299
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 300
    .local v2, "key":Ljava/lang/String;
    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v12, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vectorwatch/android/models/settings/RenderOption;

    .line 301
    .local v4, "option":Lcom/vectorwatch/android/models/settings/RenderOption;
    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->properties:[Ljava/lang/String;

    invoke-virtual {v4}, Lcom/vectorwatch/android/models/settings/RenderOption;->getOrder()I

    move-result v13

    aput-object v2, v12, v13

    goto :goto_0

    .line 304
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "option":Lcom/vectorwatch/android/models/settings/RenderOption;
    :cond_0
    const-string v11, "settings"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 305
    .local v6, "settingsJson":Lorg/json/JSONObject;
    iget-object v11, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mRenderOptions:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 306
    .restart local v2    # "key":Ljava/lang/String;
    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 307
    .local v10, "valuesList":Lorg/json/JSONArray;
    new-instance v12, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$3;

    invoke-direct {v12, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$3;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;)V

    .line 308
    invoke-virtual {v12}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$3;->getType()Ljava/lang/reflect/Type;

    move-result-object v8

    .line 310
    .local v8, "typeKeyValues":Ljava/lang/reflect/Type;
    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getSpecialSuggestions(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 311
    .local v1, "initializedValues":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    invoke-virtual {v10}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12, v8}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 312
    .local v9, "values":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    invoke-interface {v1, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 314
    iget-object v12, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mSettings:Ljava/util/Map;

    invoke-interface {v12, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 316
    .end local v1    # "initializedValues":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    .end local v2    # "key":Ljava/lang/String;
    .end local v8    # "typeKeyValues":Ljava/lang/reflect/Type;
    .end local v9    # "values":Ljava/util/List;, "Ljava/util/List<Lcom/vectorwatch/android/models/StreamPropertyValueModel;>;"
    .end local v10    # "valuesList":Lorg/json/JSONArray;
    :cond_1
    return-void
.end method

.method public requestOptions(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;Ljava/lang/String;)V
    .locals 13
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "fragment"    # Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;
    .param p3, "currentValue"    # Ljava/lang/String;

    .prologue
    .line 175
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 176
    .local v4, "userSettings":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/vectorwatch/android/models/settings/Setting;>;"
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v11

    .line 177
    .local v11, "settingNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v11, :cond_1

    .line 178
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 180
    .local v10, "settingName":Ljava/lang/String;
    iget-object v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->result:Landroid/os/Bundle;

    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 182
    .local v12, "values":[Ljava/lang/String;
    array-length v2, v12

    const/4 v3, 0x3

    if-le v2, v3, :cond_0

    .line 183
    new-instance v9, Lcom/vectorwatch/android/models/settings/Setting;

    const/4 v2, 0x0

    aget-object v2, v12, v2

    const/4 v3, 0x1

    aget-object v3, v12, v3

    const/4 v5, 0x2

    aget-object v5, v12, v5

    const/4 v6, 0x3

    aget-object v6, v12, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct {v9, v2, v3, v5, v6}, Lcom/vectorwatch/android/models/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 188
    .local v9, "set":Lcom/vectorwatch/android/models/settings/Setting;
    :goto_1
    invoke-interface {v4, v10, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 185
    .end local v9    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    :cond_0
    new-instance v9, Lcom/vectorwatch/android/models/settings/Setting;

    const/4 v2, 0x0

    aget-object v2, v12, v2

    const/4 v3, 0x1

    aget-object v3, v12, v3

    const/4 v5, 0x2

    aget-object v5, v12, v5

    invoke-direct {v9, v2, v3, v5}, Lcom/vectorwatch/android/models/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v9    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    goto :goto_1

    .line 191
    .end local v9    # "set":Lcom/vectorwatch/android/models/settings/Setting;
    .end local v10    # "settingName":Ljava/lang/String;
    .end local v12    # "values":[Ljava/lang/String;
    :cond_1
    new-instance v0, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    new-instance v5, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;

    invoke-direct {v5, p0, p1, p2}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$OptionsCallback;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;)V

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    move-object v2, p1

    move-object/from16 v3, p3

    invoke-direct/range {v0 .. v7}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;-><init>(Lcom/vectorwatch/android/models/CloudElementSummary;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask$AppSettingOptionsCallback;Landroid/content/Context;Landroid/view/View;)V

    .line 192
    .local v0, "task":Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-virtual {v1}, Lcom/vectorwatch/android/models/CloudElementSummary;->getRequireLocation()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 193
    new-instance v8, Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v8, v1}, Lcom/vectorwatch/android/utils/LocationHelper;-><init>(Landroid/content/Context;)V

    .line 194
    .local v8, "locationHelper":Lcom/vectorwatch/android/utils/LocationHelper;
    invoke-virtual {v8}, Lcom/vectorwatch/android/utils/LocationHelper;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 195
    new-instance v1, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;

    invoke-direct {v1, p0, v8, v0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;-><init>(Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;Lcom/vectorwatch/android/utils/LocationHelper;Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;)V

    invoke-virtual {v8, v1}, Lcom/vectorwatch/android/utils/LocationHelper;->startLocationUpdates(Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;)Lcom/vectorwatch/android/utils/LocationHelper;

    .line 222
    .end local v8    # "locationHelper":Lcom/vectorwatch/android/utils/LocationHelper;
    :goto_2
    return-void

    .line 217
    .restart local v8    # "locationHelper":Lcom/vectorwatch/android/utils/LocationHelper;
    :cond_2
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->app:Lcom/vectorwatch/android/models/CloudElementSummary;

    invoke-static {v1, p0}, Lcom/vectorwatch/android/utils/LocationHelper;->triggerGPSDisabledDialogWithApp(Lcom/vectorwatch/android/models/CloudElementSummary;Landroid/app/Activity;)V

    goto :goto_2

    .line 220
    .end local v8    # "locationHelper":Lcom/vectorwatch/android/utils/LocationHelper;
    :cond_3
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_2
.end method

.method public select(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/vectorwatch/android/models/StreamPropertyValueModel;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "optionName"    # Ljava/lang/String;
    .param p3, "optionValue"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "dataModel"    # Lcom/vectorwatch/android/models/StreamPropertyValueModel;

    .prologue
    const/4 v5, 0x4

    .line 258
    const-string v2, "debug"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Added values "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 260
    .local v1, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v1, p5}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "dataModelJson":Ljava/lang/String;
    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->result:Landroid/os/Bundle;

    new-array v2, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v2, v4

    const/4 v4, 0x1

    aput-object p3, v2, v4

    const/4 v4, 0x2

    aput-object p4, v2, v4

    const/4 v4, 0x3

    aput-object v0, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-array v4, v5, [Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-virtual {v3, p1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 263
    iget v2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->mCurrentIndex:I

    iget-object v3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->properties:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_0

    .line 264
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->finishAndReturnSettings()V

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->loadNextFragment()V

    goto :goto_0
.end method
