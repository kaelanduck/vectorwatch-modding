.class Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;
.super Ljava/lang/Object;
.source "AppSettingsActivity.java"

# interfaces
.implements Lcom/vectorwatch/android/utils/LocationHelper$VectorLocationCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;->requestOptions(Ljava/lang/String;Lcom/vectorwatch/android/ui/tabmenufragments/settingfragments/SettingsPropertyFragment;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;

.field final synthetic val$locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

.field final synthetic val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;


# direct methods
.method constructor <init>(Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;Lcom/vectorwatch/android/utils/LocationHelper;Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;)V
    .locals 0
    .param p1, "this$0"    # Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->this$0:Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity;

    iput-object p2, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->val$locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    iput-object p3, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccurateLocation(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->val$locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 199
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    invoke-static {p1}, Lcom/vectorwatch/android/models/VectorLocation;->fromLocation(Landroid/location/Location;)Lcom/vectorwatch/android/models/VectorLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    .line 200
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 201
    return-void
.end method

.method public onLocation(Landroid/location/Location;)V
    .locals 2
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->val$locationHelper:Lcom/vectorwatch/android/utils/LocationHelper;

    invoke-virtual {v0}, Lcom/vectorwatch/android/utils/LocationHelper;->stopLocationUpdates()Lcom/vectorwatch/android/utils/LocationHelper;

    .line 206
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    invoke-static {p1}, Lcom/vectorwatch/android/models/VectorLocation;->fromLocation(Landroid/location/Location;)Lcom/vectorwatch/android/models/VectorLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->setLocation(Lcom/vectorwatch/android/models/VectorLocation;)Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    .line 207
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 208
    return-void
.end method

.method public onTimeoutError()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/apps/AppSettingsActivity$1;->val$task:Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/utils/asyncTasks/GetAppSettingOptionsAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 214
    return-void
.end method
