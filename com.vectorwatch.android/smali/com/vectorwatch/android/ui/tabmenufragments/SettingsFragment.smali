.class public Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;
.super Landroid/app/Fragment;
.source "SettingsFragment.java"

# interfaces
.implements Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/SettingsView;


# static fields
.field private static final log:Lorg/slf4j/Logger;


# instance fields
.field mAccountProfileView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001c3
        }
    .end annotation
.end field

.field mActivityInfoView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001c4
        }
    .end annotation
.end field

.field mAlarmsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001c9
        }
    .end annotation
.end field

.field mAlertsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001c6
        }
    .end annotation
.end field

.field mContextualView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001c7
        }
    .end annotation
.end field

.field mLinkStateImage:Landroid/widget/TextView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001b5
        }
    .end annotation
.end field

.field mLogoutView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001d1
        }
    .end annotation
.end field

.field mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001cc
        }
    .end annotation
.end field

.field private mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

.field mResetView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001d0
        }
    .end annotation
.end field

.field mSection1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001c2
        }
    .end annotation
.end field

.field mSection2:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001c5
        }
    .end annotation
.end field

.field mSection3:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001ca
        }
    .end annotation
.end field

.field mSection4:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001ce
        }
    .end annotation
.end field

.field mSupportView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001cb
        }
    .end annotation
.end field

.field mTourView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001cd
        }
    .end annotation
.end field

.field mUpdateWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001cf
        }
    .end annotation
.end field

.field mVersionView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001d2
        }
    .end annotation
.end field

.field mWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;
    .annotation build Lbutterknife/Bind;
        value = {
            0x7f1001c8
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/Class;)Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->log:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 100
    return-void
.end method

.method public static newInstance()Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;

    invoke-direct {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;-><init>()V

    .line 95
    .local v0, "fragment":Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;
    return-object v0
.end method

.method private screenSetup()V
    .locals 4

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 288
    .local v0, "res":Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAccountProfileView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f090149

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 289
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAccountProfileView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f0200f7

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setImageResource(I)V

    .line 291
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mActivityInfoView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f09024a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 292
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mActivityInfoView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f0200de

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setImageResource(I)V

    .line 294
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAlertsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f090166

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 295
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAlertsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f0200e0

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setImageResource(I)V

    .line 297
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mContextualView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f09015a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 298
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mContextualView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f0200e7

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setImageResource(I)V

    .line 300
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f090189

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 301
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f020102

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setImageResource(I)V

    .line 303
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAlarmsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f09014d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 304
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mAlarmsView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f0200df

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setImageResource(I)V

    .line 306
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSupportView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f09017b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 307
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSupportView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f0200f8

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setImageResource(I)V

    .line 311
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mTourView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f09017d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 313
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mUpdateWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f09017f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 315
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mResetView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f090173

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 317
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLogoutView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f090163

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 319
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mVersionView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f090180

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "2.0.2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " build "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x165

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "b5957a0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 323
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection1:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    const v2, 0x7f090177

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->setLabel(Ljava/lang/String;)V

    .line 324
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection2:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    const v2, 0x7f090178

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->setLabel(Ljava/lang/String;)V

    .line 325
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection3:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    const v2, 0x7f090179

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->setLabel(Ljava/lang/String;)V

    .line 326
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSection4:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;

    const v2, 0x7f09017a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsSectionView;->setLabel(Ljava/lang/String;)V

    .line 328
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f090164

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setLabel(Ljava/lang/String;)V

    .line 329
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    const v2, 0x7f0200f6

    invoke-virtual {v1, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setImageResource(I)V

    .line 330
    return-void
.end method

.method private setUpLinkLostState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 268
    invoke-static {}, Lcom/vectorwatch/android/utils/Helpers;->isWatchConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "prefs_offline_badge"

    .line 272
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f0901d6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public OnAboutClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001d2
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ABOUT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 233
    return-void
.end method

.method public OnAlarmsClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001c9
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ALARMS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 198
    return-void
.end method

.method public OnAlertsClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001c6
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ALERTS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 183
    return-void
.end method

.method public OnContextualClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001c7
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->CONTEXTUAL:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 188
    return-void
.end method

.method public OnLogoutClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001d1
        }
    .end annotation

    .prologue
    .line 227
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->LOGOUT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 228
    return-void
.end method

.method public OnNewsletterClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001cc
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->NEWSLETTER:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 208
    return-void
.end method

.method public OnResetSettingsClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001d0
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->RESET_SETTINGS:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 223
    return-void
.end method

.method public OnSupportClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001cb
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->SUPPORT:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 203
    return-void
.end method

.method public OnTourClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001cd
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->TOUR:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 213
    return-void
.end method

.method public OnUpdateWatchClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001cf
        }
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->UPDATE_WATCH:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 218
    return-void
.end method

.method public OnWatchClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001c8
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->WATCH:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 193
    return-void
.end method

.method public handleLanguageFileProgress(Lcom/vectorwatch/android/events/LanguageFileProgress;)V
    .locals 4
    .param p1, "event"    # Lcom/vectorwatch/android/events/LanguageFileProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 246
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_1

    .line 247
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 248
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f09019a

    invoke-virtual {p0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "prefs_offline_badge"

    .line 251
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v3, v2}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getBooleanPreference(Ljava/lang/String;ZLandroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 252
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 257
    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getCurrentPackage()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/vectorwatch/android/events/LanguageFileProgress;->getTotalPackets()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float v0, v1, v2

    .line 258
    .local v0, "progress":F
    iget-object v1, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f09027e

    invoke-virtual {p0, v3}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public handleLinkStateChangedEvent(Lcom/vectorwatch/android/events/LinkStateChangedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/vectorwatch/android/events/LinkStateChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 239
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->setUpLinkLostState()V

    .line 240
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 242
    :cond_0
    return-void
.end method

.method public onAccountProfileClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001c3
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ACCOUNT_PROFILE:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 173
    return-void
.end method

.method public onActivityInfoClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1001c4
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    sget-object v1, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;->ACTIVITY_INFO:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;

    invoke-interface {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onOptionClicked(Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter$SettingsOption;)V

    .line 178
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 106
    new-instance v0, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;

    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenterImpl;-><init>(Landroid/app/Activity;Lcom/vectorwatch/android/ui/tabmenufragments/settings/views/SettingsView;)V

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    .line 107
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->register(Ljava/lang/Object;)V

    .line 108
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 114
    const v1, 0x7f03005c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 115
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)V

    .line 116
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->screenSetup()V

    .line 118
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 153
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 154
    invoke-static {}, Lcom/vectorwatch/android/VectorApplication;->getEventBus()Lcom/vectorwatch/android/MainThreadBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/vectorwatch/android/MainThreadBus;->unregister(Ljava/lang/Object;)V

    .line 155
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 159
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 160
    invoke-static {p0}, Lbutterknife/ButterKnife;->unbind(Ljava/lang/Object;)V

    .line 161
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 148
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mPresenter:Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;

    invoke-interface {v0}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/presenters/SettingsPresenter;->onPause()V

    .line 149
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 141
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 142
    invoke-direct {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->setUpLinkLostState()V

    .line 143
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 123
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 124
    const v0, 0x7f1001b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mLinkStateImage:Landroid/widget/TextView;

    .line 126
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/vectorwatch/android/utils/SharedPreferencesHelper;->getIsOfflineMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSupportView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mUpdateWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    invoke-virtual {v0, v2}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setVisibility(I)V

    .line 137
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mSupportView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mNewsletterView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->mUpdateWatchView:Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;

    invoke-virtual {v0, v1}, Lcom/vectorwatch/android/ui/tabmenufragments/settings/models/SettingsMainElementView;->setVisibility(I)V

    goto :goto_0
.end method

.method public triggerPairScreen()V
    .locals 1

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/vectorwatch/android/ui/tabmenufragments/SettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/vectorwatch/android/ui/BaseActivity;

    invoke-virtual {v0}, Lcom/vectorwatch/android/ui/BaseActivity;->triggerDisplayOfPairingScreen()V

    .line 167
    return-void
.end method
